﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoManager<AudioManager> {
	
	public List<AudioClip> m_listClips;

	// Use this for initialization
	protected override void Awake () {

		AudioPooler.Instance.CreatePool (this.gameObject);
	}

	public void PlayMusic(string p_soundName, bool p_bisLooping)
	{
			AudioSource audioS = AudioPooler.Instance.GetAudioSource (this.gameObject, AudioPooler.SOUNDTYPE.BGM);
			audioS.loop = p_bisLooping;
			AudioPooler.Instance.SearchMusic (p_soundName, audioS, p_bisLooping);
	}

	public void PlaySound(string p_soundName, bool p_bisLooping)
	{
			AudioSource audioS = AudioPooler.Instance.GetAudioSource (this.gameObject, AudioPooler.SOUNDTYPE.SFX);
			audioS.loop = p_bisLooping;
			AudioPooler.Instance.SearchSound (p_soundName, audioS, p_bisLooping);
	}

	public void StopAudio()
	{
		Debug.Log ("Stopping audio");
		AudioPooler.Instance.ClearAudioSource (this.gameObject);
	}

	public void StopMusic()
	{
		//Debug.Log ("Stopping Music");
		AudioPooler.Instance.ClearMusic (this.gameObject);
	}
	
	public void StopSoundEffects()
	{
		Debug.Log ("Stopping Sound");
		AudioPooler.Instance.ClearSoundEffects (this.gameObject);
	}
}
