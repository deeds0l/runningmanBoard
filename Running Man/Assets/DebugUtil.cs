﻿#define DEBUG_LEVEL_LOG
#define DEBUG_LEVEL_WARN
#define DEBUG_LEVEL_ERROR


using UnityEngine;
using System;
using System.Collections;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

// setting the conditional to the platform of choice will only compile the method for that platform
// alternatively, use the #defines at the top of this file
public class DebugUtil
{
	private static bool	  	ENABLE_DEBUG = false;
	private const string 	DUTIL_UNITY = "<Unity> : ";
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_WARN" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_ERROR" )]
	public static void log( object format, params object[] paramList )
	{
		if( format is string )
			Debug.Log( string.Format( format as string, paramList ) );
		else
			Debug.Log( format );
	}
	
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_WARN" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_ERROR" )]
	public static void warn( object format, params object[] paramList )
	{
		if( format is string )
			Debug.LogWarning( string.Format( format as string, paramList ) );
		else
			Debug.LogWarning( format );
	}
	
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_ERROR" )]
	public static void error( object format, params object[] paramList )
	{
		if( format is string )
			Debug.LogError( string.Format( format as string, paramList ) );
		else
			Debug.LogError( format );
	}
	
	
	[System.Diagnostics.Conditional( "UNITY_EDITOR" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	public static void assert( bool condition )
	{
		assert( condition, string.Empty, true );
	}
	
	
	[System.Diagnostics.Conditional( "UNITY_EDITOR" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	public static void assert( bool condition, string assertString )
	{
		assert( condition, assertString, false );
	}
	
	
	[System.Diagnostics.Conditional( "UNITY_EDITOR" )]
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	public static void assert( bool condition, string assertString, bool pauseOnFail )
	{
		if( !condition )
		{
			Debug.LogError( "assert failed! " + assertString );
			
			if( pauseOnFail )
				Debug.Break();
		}
	}

	public static void Log(string p_s)
	{
		LogMessage(p_s,LogType.Log);
	}

	private static void LogMessage(string p_s, LogType p_logType)
	{
		if (!ENABLE_DEBUG)
			return;
		
		//		StackTrace trace = new StackTrace(true);
		//		StackFrame sf = trace.GetFrame(2);			
		//		string strLogType = "Test";
		switch(p_logType)
		{
		case LogType.Log :
		{
			//			strLogType = "LOG";
			Debug.Log(DUTIL_UNITY+p_s);
		}
			break;
		case LogType.Warning :
		{
			//			strLogType = "WARNING";
			Debug.LogWarning(DUTIL_UNITY+p_s);
		}
			break;
		case LogType.Error :
		case LogType.Exception :
		{
			StackTrace st = new StackTrace (true);
			//			strLogType = "ERROR";
			//			strLogType = "CAUGHT AN EXCEPTION";
			Debug.LogError(DUTIL_UNITY+ " Message: \" "+ p_s + " \" at \" "+ st.GetFrame (2).GetFileName ()+ " \" : Method: \" " + st.GetFrame(2).GetMethod().Name + " \".");
			//+arric: Removed, just want the error logs. Don't want to stop the application.
			//throw new Exception(); 		
			//-arric
		}
			break;
		default:
		{
			// none
		}
			break;
		}
		//m_sLogs = m_sLogs + "\n" + DateTime.Now.ToString() +/* " " + sf.GetFileName().ToString() + " : [" + sf.GetFileLineNumber().ToString() + "|" + sf.GetFileColumnNumber().ToString() + "]" +*/ " <UNITY:" + strLogType + "> : "  + p_s; 
	}
}