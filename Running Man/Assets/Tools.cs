﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public static class Tools {

	public static System.Collections.IEnumerator WaitForRealTime( float p_delay )
	{
		while( true ){
			float pauseEndTime = Time.realtimeSinceStartup + p_delay;
			while ( Time.realtimeSinceStartup < pauseEndTime ){
				yield return 0;
			}
			break;
		}
	}

	public static T InstantiateCanvas<T>( string p_name, Transform p_parent = null ) where T:MonoBehaviour
	{
		return InstantiateResource<T>( "Prefabs/Canvas/" + p_name, p_parent );
	}

	public static GameObject InstantiateResource( string p_path, Transform p_parent = null )
	{
		Object resourceObj = Resources.Load( p_path );
		if( resourceObj == null ) {
			DebugUtil.assert( resourceObj != null );
			return null;
		}
		
		GameObject gameObj = GameObject.Instantiate( resourceObj ) as GameObject;
		if( gameObj == null ) {
			DebugUtil.assert( gameObj != null );
			return null;
		}
		
		gameObj.name = Tools.RemoveSubstring( gameObj.name, "(Clone)" );
		
		if( p_parent != null ) {
			gameObj.transform.SetParent( p_parent );
		}
		
		return gameObj;
	}

	public static T InstantiateManager<T>( string p_name, Transform p_parent = null ) where T:MonoBehaviour
	{
		return InstantiateResource<T>( "Prefabs/Managers/" + p_name, p_parent );
	}
	
	public static T InstantiateResource<T>( string p_path, Transform p_parent = null ) where T:MonoBehaviour
	{
		T comp = InstantiateResource( p_path, p_parent ).GetComponent<T>();
		if( comp == null ) {
			DebugUtil.assert( comp != null );
			return null;
		}
		
		return comp;
	}

	public static string RemoveSubstring (string p_mainStr, string p_subStr)
	{
		p_mainStr = p_mainStr.Replace(p_subStr, "");
		p_mainStr = p_mainStr.Trim();
		return p_mainStr;
	}

	public static bool IsMultipleTouch()
	{
		return ( Input.touchCount > 1 );
	}
}
