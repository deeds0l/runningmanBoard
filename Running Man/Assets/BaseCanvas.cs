﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof( DefiniteCasinoCanvasSetup ))]
public abstract class BaseCanvas<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T s_instance = null;
	public static T Instance { get { return s_instance; } }
	
	protected Canvas m_canvas = null;
	protected CanvasGroup m_canvasGroup = null;
	
	public static void Create( Transform p_parent)
	{
		if( s_instance == null ) {
			s_instance = Tools.InstantiateCanvas<T>( typeof( T ).Name, p_parent );

			//Debug.Log ("Created a Canvas: " + typeof( T ).Name);
			//DebugUtil.Log( "Created a Canvas: " + typeof( T ).Name );
		}
	}

	protected virtual void Awake()
	{
		m_canvas = GetComponent<Canvas>();
		m_canvasGroup = GetComponent<CanvasGroup>();

		//Debug.Log ("Canvas Awake: " + typeof(T).Name);
		if( m_canvasGroup != null ) {
			m_canvasGroup.ignoreParentGroups = true;
		}
	}
	
	protected virtual void OnDestroy()
	{
		s_instance = null;
		//Debug.Log ("Destroyed a Base: " + typeof(T).Name);
		//DebugUtil.Log( "Destroyed a Base: " + gameObject.name );
	}

	public virtual void Open()
	{
		gameObject.SetActive(true);
	}

	public virtual void Close()
	{
		gameObject.SetActive(false);
	}
}