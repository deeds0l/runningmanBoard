﻿using UnityEngine;
using System.Collections;

public class MonoManager<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T s_instance = null;
	public static T Instance { get { return s_instance; } }
	
	public static void Create( Transform p_parent = null)
	{
		if( s_instance == null ) {
			s_instance = Tools.InstantiateManager<T>( typeof( T ).Name, p_parent );
			
			DebugUtil.Log( "Created a Mono Manager: " + typeof( T ).Name );
		}
	}
	
	protected virtual void Awake()
	{
		
	}
	
	protected virtual void OnDestroy()
	{
		s_instance = null;
		
		DebugUtil.Log( "Destroyed a Mono Manager: " + gameObject.name );
	}
	
	
	public void Destroy()
	{
		Destroy( gameObject );
	}
}