﻿using UnityEngine;
using System.Collections;

public class NonMonoManager<T> where T: class, new()
{
	protected static T s_instance = null;
	public static T Instance { get { return s_instance; } }
	
	public static void Create()
	{
		if( s_instance == null )
		{
			s_instance = new T();

			DebugUtil.Log( "Created a Mono Manager: " + typeof(T).Name );
		}
	}
	
	public virtual void Destroy()
	{
		s_instance = null;

		DebugUtil.Log( "Destroyed a Mono Manager: " + GetType().Name );
	}
}
