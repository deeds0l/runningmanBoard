﻿using UnityEngine;
using System.Collections;

public interface ICanvasInteractable
{
	void Open();
	void Close();
	void SetSortingOrder(int p_order);
}

public abstract class BaseOverlay<T> : BaseCanvas<T>, ICanvasInteractable where T : MonoBehaviour
{
	private Coroutine m_coroutine = null;
	
	protected override void Awake()
	{
		base.Awake();
		gameObject.SetActive( false );
	}

	public override void Open()
	{	
		base.Open();

		FadeIn();
	}

	public override void Close()
	{
		FadeOut();
	}

	protected void OnClose()
	{
		gameObject.SetActive(false);
	}

	protected void CancelClose()
	{
		if (m_coroutine != null) { StopCoroutine( m_coroutine ); }
	}
	
	public void CloseWithDelay (float p_delay)
	{
		m_coroutine = StartCoroutine(CloseWithDelayCoroutine(p_delay));
	}
	
	private IEnumerator CloseWithDelayCoroutine( float p_delay )
	{
		yield return StartCoroutine (Tools.WaitForRealTime (p_delay));
		Close();
	}

	public virtual void SetSortingOrder( int p_order )
	{
		// Enable/Disable must be done to refresh ordering.
		m_canvas.enabled = false;
		m_canvas.sortingOrder = p_order;
		m_canvas.enabled = true;
	}

	protected virtual void FadeIn ()
	{
		StartCoroutine(FadeInCoroutine(0.5f));
	}

	protected virtual void FadeOut ()
	{
		StartCoroutine(FadeOutCoroutine(0.25f));
	}

	protected IEnumerator FadeInCoroutine (float duration)
	{
		for (float t = 0; t < 1; t += Time.deltaTime/duration) {
			GetComponent<CanvasGroup>().alpha = t;
			yield return null;
		}
		GetComponent<CanvasGroup>().alpha = 1;
	}

	protected IEnumerator FadeOutCoroutine (float duration)
	{
		for (float t = 1; t > 0; t -= Time.deltaTime/duration) {
			GetComponent<CanvasGroup>().alpha = t;
			yield return null;
		}
		GetComponent<CanvasGroup>().alpha = 0;
		
		OnClose();
	}
}