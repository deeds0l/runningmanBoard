﻿using UnityEngine;
using System.Collections;

public class BoardController : MonoBehaviour {

	void Awake()
	{
		AudioManager.Create ();
	}

	public void Stop()
	{
		AudioManager.Instance.StopAudio ();
	}

	public void PlaySound(int sound)
	{
		switch(sound)
		{
		case 1: AudioManager.Instance.PlaySound ("metalgear",false);
			break;
		case 2: AudioManager.Instance.PlaySound ("sparta",false);
			break;
		case 3: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("cena",false);
			break;
		case 4: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("bitsam",false);
			break;
		case 5: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("bingbing",false);
			break;
		case 6: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("bling",false);
			break;
		case 7: AudioManager.Instance.PlaySound ("wrong",false);
			break;
		case 8: AudioManager.Instance.PlaySound ("wenk",false);
			break;
		case 9: AudioManager.Instance.PlaySound ("leeroy",false);
			break;
		case 10: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("forever",false);
			break;
		case 11:
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlaySound ("lying",false);
			break;
		case 12: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("fup",false);
			break;
		case 13: 
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("jaws",false);
			break;
		case 14:
			//AudioManager.Instance.StopMusic();
			AudioManager.Instance.PlayMusic ("pretty",false);
			break;
		}


	}
}
