﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DefiniteCasinoCanvasSetup), true)]
public class DefiniteCasinoCanvasSetupEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		//DrawDefaultInspector();
		DefiniteCasinoCanvasSetup baseCanvas = target as DefiniteCasinoCanvasSetup;

		if( GUILayout.Button( "Reset to Definite Casino Setup", GUILayout.Height( 30 ) ) )
		{
			baseCanvas.ResetToDefiniteCasinoSetup();
		}

		EditorGUILayout.HelpBox( "Resets every Canvas component suitable for Battle Magik usage.\nSet Graphic Raycaster Blocking Mask to Everything.", MessageType.Info );
	}
}
