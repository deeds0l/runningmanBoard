﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

class MyEditorScript {

	static void GenerateXcodeProject ()
	{
		string pathname = "AutoBuild/iOS";
		string[] levels = { "Assets/Scene/Casino.unity" };
		EditorUserBuildSettings.SwitchActiveBuildTarget( BuildTarget.iOS );
		string result = BuildPipeline.BuildPlayer( levels, pathname, BuildTarget.iOS, BuildOptions.None );
		if (result.Length > 0) {
			throw new Exception("BuildPlayer failure: " + result);
		}
	}
}