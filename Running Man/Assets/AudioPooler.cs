﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AudioPooler : MonoBehaviour {

	private static AudioPooler instance;
	
	public static AudioPooler Instance
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType<AudioPooler>();
			}
			
			return instance;
		}
	}

	public enum SOUNDTYPE
	{
		BGM,
		SFX
	}

//	Queue<AudioSource> m_currentMusicSource;
//	Queue<AudioSource> m_currentSoundSource;
	AudioSource m_currentAudioSource;
	AudioClip m_currentAudioClip;
	objectSource newObjectSource;

	[SerializeField]
	private int m_audioPreloadCount;

	bool m_bstoppedMusic;
	bool m_bstoppedSound;

	void Awake()
	{
		instance = this;
	}

	public class objectSource
	{
		public GameObject m_audioContainer;
		public List<AudioSource> m_spawnedMusicSource;
		public Queue<AudioSource> m_musicSourceQueue;
		public List<AudioSource> m_spawnedSoundSource;
		public Queue<AudioSource> m_soundSourceQueue;
	}

	List<objectSource> m_objectSourceList = new List<objectSource>();

	public void CreatePool(GameObject p_audioContainer)
	{
		m_objectSourceList.Add (new objectSource());

		newObjectSource = m_objectSourceList.Last();
		newObjectSource.m_audioContainer = p_audioContainer;
		newObjectSource.m_spawnedMusicSource = new List<AudioSource>();
		newObjectSource.m_musicSourceQueue = new Queue<AudioSource>();
		newObjectSource.m_spawnedSoundSource = new List<AudioSource>();
		newObjectSource.m_soundSourceQueue = new Queue<AudioSource>();
		//m_currentAudioClip = new List<AudioClip>();
//		m_currentMusicSource = new Queue<AudioSource>();
//		m_currentSoundSource = new Queue<AudioSource>();

		for (int i = 0; i < m_audioPreloadCount; i++) 
		{
			AudioSource audioSource = p_audioContainer.AddComponent<AudioSource>(); //create component here
			newObjectSource.m_musicSourceQueue.Enqueue(audioSource); //enque audio source
			newObjectSource.m_soundSourceQueue.Enqueue(audioSource);
			audioSource.enabled = false; //make component disabled
		}
	}

	public AudioSource GetAudioSource(GameObject p_audioContainer, SOUNDTYPE p_soundType)
	{
		AudioSource objectAudioSource;
		newObjectSource = m_objectSourceList.FirstOrDefault(audio => audio.m_audioContainer == p_audioContainer);

		if (p_soundType == SOUNDTYPE.BGM) 
		{
			//Debug.Log ("soundType: " + p_soundType.ToString() + " expected bgm");

			if (newObjectSource.m_musicSourceQueue.Count <= 0) {
				AudioSource audioSource = p_audioContainer.AddComponent<AudioSource> (); //create component here
				newObjectSource.m_musicSourceQueue.Enqueue (audioSource); //enque audio source
				audioSource.enabled = false; //make component disabled();
			}

			objectAudioSource = newObjectSource.m_musicSourceQueue.Dequeue ();
			objectAudioSource.enabled = true;
			
			newObjectSource.m_spawnedMusicSource.Add (objectAudioSource);
		} 
		else
		{
			//Debug.Log ("Dequeue: " + newObjectSource.m_soundSourceQueue.Count);

			if (newObjectSource.m_soundSourceQueue.Count <= 0) {
				AudioSource audioSource = p_audioContainer.AddComponent<AudioSource> (); //create component here
				newObjectSource.m_soundSourceQueue.Enqueue (audioSource); //enque audio source
				audioSource.enabled = false; //make component disabled();
			}

			objectAudioSource = newObjectSource.m_soundSourceQueue.Dequeue ();
			//if(objectAudioSource.clip != null){Debug.Log ("Dequeued Clip: " + objectAudioSource.clip.name);}
			objectAudioSource.enabled = true;
			
			newObjectSource.m_spawnedSoundSource.Add (objectAudioSource);
		}

		return objectAudioSource;
	}

	public void ReturnAudioSource (AudioSource p_currentAudioSource, SOUNDTYPE p_soundType)
	{
		newObjectSource = m_objectSourceList.FirstOrDefault(audio => audio.m_audioContainer == p_currentAudioSource.transform.gameObject);

		p_currentAudioSource.enabled = false;

		Debug.Log ("Returned Clip: " + p_currentAudioSource.clip.name);

		if (p_soundType == SOUNDTYPE.BGM) 
		{
			newObjectSource.m_musicSourceQueue.Enqueue (p_currentAudioSource);
		} 
		else
		{
			newObjectSource.m_soundSourceQueue.Enqueue (p_currentAudioSource);
		}

	}//remove the source from list

	public void ClearAudioSource(GameObject p_audioContainer)
	{

		newObjectSource = m_objectSourceList.FirstOrDefault(audio => audio.m_audioContainer == p_audioContainer);

		foreach(var currentAudioSource in newObjectSource.m_spawnedMusicSource)
		{
			ReturnAudioSource (currentAudioSource, SOUNDTYPE.BGM);
		}

		foreach(var currentAudioSource in newObjectSource.m_spawnedSoundSource)
		{
			ReturnAudioSource (currentAudioSource, SOUNDTYPE.SFX);
		}

		newObjectSource.m_spawnedMusicSource.Clear ();
		newObjectSource.m_spawnedSoundSource.Clear ();
	}

	public void ClearMusic(GameObject p_audioContainer)
	{
		newObjectSource = m_objectSourceList.FirstOrDefault(audio => audio.m_audioContainer == p_audioContainer);
		
		foreach(var currentAudioSource in newObjectSource.m_spawnedMusicSource)
		{
			//Debug.Log ("Audio Source: " + currentAudioSource.clip.name);
			ReturnAudioSource (currentAudioSource, SOUNDTYPE.BGM);
		}
		
		newObjectSource.m_spawnedMusicSource.Clear ();

		//Debug.Log ("End Clear");
	}
	
	public void ClearSoundEffects(GameObject p_audioContainer)
	{
		newObjectSource = m_objectSourceList.FirstOrDefault(audio => audio.m_audioContainer == p_audioContainer);
		
		foreach(var currentAudioSource in newObjectSource.m_spawnedSoundSource)
		{
			//Debug.Log ("Cleared: " + currentAudioSource.clip.name);
			ReturnAudioSource (currentAudioSource, SOUNDTYPE.SFX);
		}

		//Debug.Log ("Clear Finished");
		
		newObjectSource.m_spawnedSoundSource.Clear ();
	}

	public void SearchMusic(string key, AudioSource p_audioSource, bool p_bisLooping)
	{
		//LocalizationManager.Instance.GetLocalizedItem (key).ToString();
		
		//Debug.Log ("key: " + key);
		//m_currentAudioClip.Add (AudioManager.Instance.m_listClips.FirstOrDefault(clip => clip.name == key));
		
		m_currentAudioClip = AudioManager.Instance.m_listClips.FirstOrDefault(clip => clip.name == key); //Resources.Load<AudioClip>("Music/" + m_currentAudio) as AudioClip;
		LookForMusicSource (p_audioSource, p_bisLooping);
	}
	
	public void SearchSound(string key, AudioSource p_audioSource, bool p_bisLooping)
	{
		//LocalizationManager.Instance.GetLocalizedItem (key).ToString();
		
		//m_currentAudioClip.Add (AudioManager.Instance.m_listClips.FirstOrDefault(clip => clip.name == key));
		
		m_currentAudioClip = AudioManager.Instance.m_listClips.FirstOrDefault(clip => clip.name == key); //Resources.Load<AudioClip>("Music/" + m_currentAudio) as AudioClip;
		LookForSoundSource (p_audioSource, p_bisLooping);
	}
	
	void LookForMusicSource(AudioSource p_audioSource, bool p_bisLooping)
	{
//		m_currentMusicSource.Enqueue (p_audioSource);
//		m_currentMusicSource.LastOrDefault ().clip = m_currentAudioClip;

		m_currentAudioSource = p_audioSource;
		m_currentAudioSource.clip = m_currentAudioClip;

		StartCoroutine(DisableMusic(p_audioSource, p_bisLooping));
	}

	void LookForSoundSource(AudioSource p_audioSource, bool p_bisLooping)
	{
//		m_currentSoundSource.Enqueue (p_audioSource);
//		m_currentSoundSource.LastOrDefault ().clip = m_currentAudioClip;

		m_currentAudioSource = p_audioSource;
		m_currentAudioSource.clip = m_currentAudioClip;
		
		StartCoroutine(DisableSound(p_audioSource, p_bisLooping));
	}

	IEnumerator DisableMusic(AudioSource p_audioSource, bool p_bisLooping)
	{
		p_audioSource.Play();
		//Debug.Log ("Clip Length: " + m_currentAudioClip.length);
		yield return new WaitForSeconds (m_currentAudioClip.length);

		if (p_bisLooping == false && p_audioSource.enabled == true) {
			ReturnAudioSource (p_audioSource, SOUNDTYPE.BGM);
			newObjectSource.m_spawnedMusicSource.Remove(p_audioSource);
		}
		Debug.Log ("Terminate Music");
	}

	IEnumerator DisableSound(AudioSource p_audioSource, bool p_bisLooping)
	{
		p_audioSource.Play();
		//Debug.Log ("Clip Length: " + m_currentAudioClip.length);
		yield return new WaitForSeconds (m_currentAudioClip.length);
		
		if (p_bisLooping == false && p_audioSource.enabled == true) {
			//Debug.Log ("Disabled: " + p_audioSource.clip.name);
			ReturnAudioSource (p_audioSource, SOUNDTYPE.SFX);
			newObjectSource.m_spawnedSoundSource.Remove(p_audioSource);
			Debug.Log ("Terminate Sound");
		}
	}

}
