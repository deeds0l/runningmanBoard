﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof( Canvas ))]
[RequireComponent (typeof ( CanvasScaler ))]
[RequireComponent (typeof ( GraphicRaycaster ))]
[RequireComponent (typeof ( CanvasGroup ))]
public class DefiniteCasinoCanvasSetup : MonoBehaviour
{
	protected void Awake()
	{
		//ResetToMlbSetup();
	}

	public void ResetToDefiniteCasinoSetup()
	{
		Canvas canvas = GetComponent<Canvas>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		canvas.pixelPerfect = false;

		CanvasScaler canvasScaler = GetComponent<CanvasScaler>();
		canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvasScaler.referenceResolution = new Vector2( 2048, 1536 );
		canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
		canvasScaler.matchWidthOrHeight = 0.75f;
		canvasScaler.referencePixelsPerUnit = 100;

		GraphicRaycaster graphicRaycaster = GetComponent<GraphicRaycaster>();
		graphicRaycaster.ignoreReversedGraphics = true;
		graphicRaycaster.blockingObjects = GraphicRaycaster.BlockingObjects.None;

		CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
		canvasGroup.alpha = 1;
		canvasGroup.interactable = true;
		canvasGroup.blocksRaycasts = true;
		canvasGroup.ignoreParentGroups = false;
	}
}
