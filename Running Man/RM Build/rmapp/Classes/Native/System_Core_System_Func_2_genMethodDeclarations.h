﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<AudioPooler/objectSource,System.Boolean>
struct Func_2_t37;
// System.Object
struct Object_t;
// AudioPooler/objectSource
struct objectSource_t8;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<AudioPooler/objectSource,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Byte>
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
#define Func_2__ctor_m105(__this, ___object, ___method, method) (( void (*) (Func_2_t37 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m11608_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<AudioPooler/objectSource,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m11737(__this, ___arg1, method) (( bool (*) (Func_2_t37 *, objectSource_t8 *, const MethodInfo*))Func_2_Invoke_m11610_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<AudioPooler/objectSource,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m11738(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t37 *, objectSource_t8 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m11612_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<AudioPooler/objectSource,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m11739(__this, ___result, method) (( bool (*) (Func_2_t37 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m11614_gshared)(__this, ___result, method)
