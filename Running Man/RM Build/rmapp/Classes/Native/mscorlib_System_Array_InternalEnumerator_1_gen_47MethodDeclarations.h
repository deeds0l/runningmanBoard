﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>
struct InternalEnumerator_1_t2885;
// System.Object
struct Object_t;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t417;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17525(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2885 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17526(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m17527(__this, method) (( void (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17528(__this, method) (( bool (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m17529(__this, method) (( DisallowMultipleComponent_t417 * (*) (InternalEnumerator_1_t2885 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
