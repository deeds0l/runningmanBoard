﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DivideByZeroException
struct DivideByZeroException_t1834;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.DivideByZeroException::.ctor()
extern "C" void DivideByZeroException__ctor_m9956 (DivideByZeroException_t1834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DivideByZeroException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DivideByZeroException__ctor_m9957 (DivideByZeroException_t1834 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
