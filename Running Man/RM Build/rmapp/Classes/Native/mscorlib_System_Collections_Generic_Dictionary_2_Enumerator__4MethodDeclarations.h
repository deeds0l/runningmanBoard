﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t2597;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2592;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13505_gshared (Enumerator_t2597 * __this, Dictionary_2_t2592 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m13505(__this, ___dictionary, method) (( void (*) (Enumerator_t2597 *, Dictionary_2_t2592 *, const MethodInfo*))Enumerator__ctor_m13505_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13506(__this, method) (( Object_t * (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1147  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508(__this, method) (( Object_t * (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509(__this, method) (( Object_t * (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13510_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13510(__this, method) (( bool (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_MoveNext_m13510_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2593  Enumerator_get_Current_m13511_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13511(__this, method) (( KeyValuePair_2_t2593  (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_get_Current_m13511_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m13512_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m13512(__this, method) (( Object_t * (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_get_CurrentKey_m13512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m13513_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m13513(__this, method) (( Object_t * (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_get_CurrentValue_m13513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m13514_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13514(__this, method) (( void (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_VerifyState_m13514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m13515_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m13515(__this, method) (( void (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_VerifyCurrent_m13515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13516_gshared (Enumerator_t2597 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13516(__this, method) (( void (*) (Enumerator_t2597 *, const MethodInfo*))Enumerator_Dispose_m13516_gshared)(__this, method)
