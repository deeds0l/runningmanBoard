﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t651;
struct GcScoreData_t651_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t661;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern "C" Score_t661 * GcScoreData_ToScore_m3208 (GcScoreData_t651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t651_marshal(const GcScoreData_t651& unmarshaled, GcScoreData_t651_marshaled& marshaled);
void GcScoreData_t651_marshal_back(const GcScoreData_t651_marshaled& marshaled, GcScoreData_t651& unmarshaled);
void GcScoreData_t651_marshal_cleanup(GcScoreData_t651_marshaled& marshaled);
