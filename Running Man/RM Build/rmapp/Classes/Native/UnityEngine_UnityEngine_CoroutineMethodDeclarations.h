﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t228;
struct Coroutine_t228_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m2179 (Coroutine_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m2180 (Coroutine_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m2181 (Coroutine_t228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t228_marshal(const Coroutine_t228& unmarshaled, Coroutine_t228_marshaled& marshaled);
void Coroutine_t228_marshal_back(const Coroutine_t228_marshaled& marshaled, Coroutine_t228& unmarshaled);
void Coroutine_t228_marshal_cleanup(Coroutine_t228_marshaled& marshaled);
