﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t2648;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14275(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2648 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11445_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14276(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2648 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11446_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
#define InternalEnumerator_1_Dispose_m14277(__this, method) (( void (*) (InternalEnumerator_1_t2648 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11447_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14278(__this, method) (( bool (*) (InternalEnumerator_1_t2648 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11448_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
#define InternalEnumerator_1_get_Current_m14279(__this, method) (( int32_t (*) (InternalEnumerator_1_t2648 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11449_gshared)(__this, method)
