﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t183;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t286;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t287;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t288 
{
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t183 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;
};
struct LayoutRebuilder_t288_StaticFields{
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t286 * ___U3CU3Ef__amU24cache2_2;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t286 * ___U3CU3Ef__amU24cache3_3;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t286 * ___U3CU3Ef__amU24cache4_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache5
	UnityAction_1_t286 * ___U3CU3Ef__amU24cache5_5;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache6
	Predicate_1_t287 * ___U3CU3Ef__amU24cache6_6;
};
