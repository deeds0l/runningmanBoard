﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t49;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t287  : public MulticastDelegate_t216
{
};
