﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t381;

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m1841 (WaitForEndOfFrame_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
