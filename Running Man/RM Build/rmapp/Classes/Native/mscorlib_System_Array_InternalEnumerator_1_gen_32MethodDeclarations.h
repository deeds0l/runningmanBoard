﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Byte>
struct InternalEnumerator_1_t2737;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15518_gshared (InternalEnumerator_1_t2737 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15518(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2737 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15518_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15519_gshared (InternalEnumerator_1_t2737 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15519(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2737 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15519_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15520_gshared (InternalEnumerator_1_t2737 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15520(__this, method) (( void (*) (InternalEnumerator_1_t2737 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15520_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15521_gshared (InternalEnumerator_1_t2737 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15521(__this, method) (( bool (*) (InternalEnumerator_1_t2737 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15521_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m15522_gshared (InternalEnumerator_1_t2737 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15522(__this, method) (( uint8_t (*) (InternalEnumerator_1_t2737 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15522_gshared)(__this, method)
