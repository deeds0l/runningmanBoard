﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t546;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.IO.FileStream/ReadDelegate
struct  ReadDelegate_t1517  : public MulticastDelegate_t216
{
};
