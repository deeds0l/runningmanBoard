﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1335;
struct U24ArrayTypeU2412_t1335_marshaled;

void U24ArrayTypeU2412_t1335_marshal(const U24ArrayTypeU2412_t1335& unmarshaled, U24ArrayTypeU2412_t1335_marshaled& marshaled);
void U24ArrayTypeU2412_t1335_marshal_back(const U24ArrayTypeU2412_t1335_marshaled& marshaled, U24ArrayTypeU2412_t1335& unmarshaled);
void U24ArrayTypeU2412_t1335_marshal_cleanup(U24ArrayTypeU2412_t1335_marshaled& marshaled);
