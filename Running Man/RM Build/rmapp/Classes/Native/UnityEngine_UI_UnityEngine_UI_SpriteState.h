﻿#pragma once
#include <stdint.h>
// UnityEngine.Sprite
struct Sprite_t201;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.UI.SpriteState
struct  SpriteState_t252 
{
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t201 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t201 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t201 * ___m_DisabledSprite_2;
};
