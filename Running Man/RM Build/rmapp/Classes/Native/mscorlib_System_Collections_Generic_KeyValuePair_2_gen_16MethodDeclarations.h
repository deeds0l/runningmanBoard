﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
struct KeyValuePair_2_t2818;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m16646_gshared (KeyValuePair_2_t2818 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m16646(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2818 *, uint64_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m16646_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Key()
extern "C" uint64_t KeyValuePair_2_get_Key_m16647_gshared (KeyValuePair_2_t2818 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m16647(__this, method) (( uint64_t (*) (KeyValuePair_2_t2818 *, const MethodInfo*))KeyValuePair_2_get_Key_m16647_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m16648_gshared (KeyValuePair_2_t2818 * __this, uint64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m16648(__this, ___value, method) (( void (*) (KeyValuePair_2_t2818 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m16648_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m16649_gshared (KeyValuePair_2_t2818 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m16649(__this, method) (( Object_t * (*) (KeyValuePair_2_t2818 *, const MethodInfo*))KeyValuePair_2_get_Value_m16649_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m16650_gshared (KeyValuePair_2_t2818 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m16650(__this, ___value, method) (( void (*) (KeyValuePair_2_t2818 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m16650_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m16651_gshared (KeyValuePair_2_t2818 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m16651(__this, method) (( String_t* (*) (KeyValuePair_2_t2818 *, const MethodInfo*))KeyValuePair_2_ToString_m16651_gshared)(__this, method)
