﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t679;
// System.Collections.Generic.ICollection`1<UnityEngine.Event>
struct ICollection_1_t3254;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t3255;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t229;
struct Event_t229_marshaled;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t2901;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t2902;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
struct IEqualityComparer_1_t2898;
// System.Collections.Generic.IDictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t3256;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3257;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t3258;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_15MethodDeclarations.h"
#define Dictionary_2__ctor_m17654(__this, method) (( void (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2__ctor_m13094_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m17655(__this, ___comparer, method) (( void (*) (Dictionary_2_t679 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13095_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m17656(__this, ___dictionary, method) (( void (*) (Dictionary_2_t679 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13096_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m17657(__this, ___capacity, method) (( void (*) (Dictionary_2_t679 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m17658(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t679 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13098_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m17659(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t679 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m13099_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17660(__this, method) (( Object_t* (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17661(__this, method) (( Object_t* (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17662(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13102_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17663(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t679 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13103_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m17664(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t679 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13104_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m17665(__this, ___key, method) (( bool (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13105_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m17666(__this, ___key, method) (( void (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13106_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17667(__this, method) (( bool (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17668(__this, method) (( Object_t * (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17669(__this, method) (( bool (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17670(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t679 *, KeyValuePair_2_t2900 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17671(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t679 *, KeyValuePair_2_t2900 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17672(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t679 *, KeyValuePair_2U5BU5D_t3257*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17673(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t679 *, KeyValuePair_2_t2900 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17674(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t679 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13114_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17675(__this, method) (( Object_t * (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17676(__this, method) (( Object_t* (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17677(__this, method) (( Object_t * (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Count()
#define Dictionary_2_get_Count_m17678(__this, method) (( int32_t (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_get_Count_m13118_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
#define Dictionary_2_get_Item_m17679(__this, ___key, method) (( int32_t (*) (Dictionary_2_t679 *, Event_t229 *, const MethodInfo*))Dictionary_2_get_Item_m13119_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m17680(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t679 *, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m13120_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m17681(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t679 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13121_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m17682(__this, ___size, method) (( void (*) (Dictionary_2_t679 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13122_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m17683(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t679 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13123_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m17684(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2900  (*) (Object_t * /* static, unused */, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m13124_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m17685(__this /* static, unused */, ___key, ___value, method) (( Event_t229 * (*) (Object_t * /* static, unused */, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m13125_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m17686(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m13126_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m17687(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t679 *, KeyValuePair_2U5BU5D_t3257*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13127_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Resize()
#define Dictionary_2_Resize_m17688(__this, method) (( void (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_Resize_m13128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
#define Dictionary_2_Add_m17689(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t679 *, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_Add_m13129_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Clear()
#define Dictionary_2_Clear_m17690(__this, method) (( void (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_Clear_m13130_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m17691(__this, ___key, method) (( bool (*) (Dictionary_2_t679 *, Event_t229 *, const MethodInfo*))Dictionary_2_ContainsKey_m13131_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m17692(__this, ___value, method) (( bool (*) (Dictionary_2_t679 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m13132_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m17693(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t679 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m13133_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m17694(__this, ___sender, method) (( void (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13134_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
#define Dictionary_2_Remove_m17695(__this, ___key, method) (( bool (*) (Dictionary_2_t679 *, Event_t229 *, const MethodInfo*))Dictionary_2_Remove_m13135_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m17696(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t679 *, Event_t229 *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m13136_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Keys()
#define Dictionary_2_get_Keys_m17697(__this, method) (( KeyCollection_t2901 * (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_get_Keys_m13137_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Values()
#define Dictionary_2_get_Values_m17698(__this, method) (( ValueCollection_t2902 * (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_get_Values_m13138_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m17699(__this, ___key, method) (( Event_t229 * (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13139_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m17700(__this, ___value, method) (( int32_t (*) (Dictionary_2_t679 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13140_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m17701(__this, ___pair, method) (( bool (*) (Dictionary_2_t679 *, KeyValuePair_2_t2900 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13141_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m17702(__this, method) (( Enumerator_t2903  (*) (Dictionary_2_t679 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13142_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m17703(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, Event_t229 *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13143_gshared)(__this /* static, unused */, ___key, ___value, method)
