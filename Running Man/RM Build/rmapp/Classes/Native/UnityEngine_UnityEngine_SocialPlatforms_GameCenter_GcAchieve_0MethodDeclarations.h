﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t650;
struct GcAchievementData_t650_marshaled;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t659;

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t659 * GcAchievementData_ToAchievement_m3207 (GcAchievementData_t650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcAchievementData_t650_marshal(const GcAchievementData_t650& unmarshaled, GcAchievementData_t650_marshaled& marshaled);
void GcAchievementData_t650_marshal_back(const GcAchievementData_t650_marshaled& marshaled, GcAchievementData_t650& unmarshaled);
void GcAchievementData_t650_marshal_cleanup(GcAchievementData_t650_marshaled& marshaled);
