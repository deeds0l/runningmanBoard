﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// Metadata Definition System.ExecutionEngineException
extern TypeInfo ExecutionEngineException_t1846_il2cpp_TypeInfo;
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineExceptionMethodDeclarations.h"
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor()
extern const MethodInfo ExecutionEngineException__ctor_m9991_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m9991/* method */
	, &ExecutionEngineException_t1846_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ExecutionEngineException_t1846_ExecutionEngineException__ctor_m9992_ParameterInfos[] = 
{
	{"info", 0, 134224105, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224106, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ExecutionEngineException__ctor_m9992_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m9992/* method */
	, &ExecutionEngineException_t1846_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ExecutionEngineException_t1846_ExecutionEngineException__ctor_m9992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecutionEngineException_t1846_MethodInfos[] =
{
	&ExecutionEngineException__ctor_m9991_MethodInfo,
	&ExecutionEngineException__ctor_m9992_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo Exception_ToString_m3652_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m3653_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m3654_MethodInfo;
extern const MethodInfo Exception_get_Message_m3655_MethodInfo;
extern const MethodInfo Exception_get_Source_m3656_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m3657_MethodInfo;
extern const MethodInfo Exception_GetType_m3658_MethodInfo;
static const Il2CppMethodReference ExecutionEngineException_t1846_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool ExecutionEngineException_t1846_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t428_0_0_0;
extern const Il2CppType _Exception_t824_0_0_0;
static Il2CppInterfaceOffsetPair ExecutionEngineException_t1846_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionEngineException_t1846_0_0_0;
extern const Il2CppType ExecutionEngineException_t1846_1_0_0;
extern const Il2CppType SystemException_t1181_0_0_0;
struct ExecutionEngineException_t1846;
const Il2CppTypeDefinitionMetadata ExecutionEngineException_t1846_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecutionEngineException_t1846_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, ExecutionEngineException_t1846_VTable/* vtableMethods */
	, ExecutionEngineException_t1846_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecutionEngineException_t1846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionEngineException"/* name */
	, "System"/* namespaze */
	, ExecutionEngineException_t1846_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecutionEngineException_t1846_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 876/* custom_attributes_cache */
	, &ExecutionEngineException_t1846_0_0_0/* byval_arg */
	, &ExecutionEngineException_t1846_1_0_0/* this_arg */
	, &ExecutionEngineException_t1846_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionEngineException_t1846)/* instance_size */
	, sizeof (ExecutionEngineException_t1846)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// Metadata Definition System.FieldAccessException
extern TypeInfo FieldAccessException_t1847_il2cpp_TypeInfo;
// System.FieldAccessException
#include "mscorlib_System_FieldAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor()
extern const MethodInfo FieldAccessException__ctor_m9993_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m9993/* method */
	, &FieldAccessException_t1847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FieldAccessException_t1847_FieldAccessException__ctor_m9994_ParameterInfos[] = 
{
	{"message", 0, 134224107, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.String)
extern const MethodInfo FieldAccessException__ctor_m9994_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m9994/* method */
	, &FieldAccessException_t1847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, FieldAccessException_t1847_FieldAccessException__ctor_m9994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo FieldAccessException_t1847_FieldAccessException__ctor_m9995_ParameterInfos[] = 
{
	{"info", 0, 134224108, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224109, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FieldAccessException__ctor_m9995_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m9995/* method */
	, &FieldAccessException_t1847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, FieldAccessException_t1847_FieldAccessException__ctor_m9995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FieldAccessException_t1847_MethodInfos[] =
{
	&FieldAccessException__ctor_m9993_MethodInfo,
	&FieldAccessException__ctor_m9994_MethodInfo,
	&FieldAccessException__ctor_m9995_MethodInfo,
	NULL
};
static const Il2CppMethodReference FieldAccessException_t1847_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool FieldAccessException_t1847_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FieldAccessException_t1847_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAccessException_t1847_0_0_0;
extern const Il2CppType FieldAccessException_t1847_1_0_0;
extern const Il2CppType MemberAccessException_t1848_0_0_0;
struct FieldAccessException_t1847;
const Il2CppTypeDefinitionMetadata FieldAccessException_t1847_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAccessException_t1847_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1848_0_0_0/* parent */
	, FieldAccessException_t1847_VTable/* vtableMethods */
	, FieldAccessException_t1847_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FieldAccessException_t1847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAccessException"/* name */
	, "System"/* namespaze */
	, FieldAccessException_t1847_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FieldAccessException_t1847_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 877/* custom_attributes_cache */
	, &FieldAccessException_t1847_0_0_0/* byval_arg */
	, &FieldAccessException_t1847_1_0_0/* this_arg */
	, &FieldAccessException_t1847_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAccessException_t1847)/* instance_size */
	, sizeof (FieldAccessException_t1847)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// Metadata Definition System.FlagsAttribute
extern TypeInfo FlagsAttribute_t406_il2cpp_TypeInfo;
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FlagsAttribute::.ctor()
extern const MethodInfo FlagsAttribute__ctor_m1988_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FlagsAttribute__ctor_m1988/* method */
	, &FlagsAttribute_t406_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FlagsAttribute_t406_MethodInfos[] =
{
	&FlagsAttribute__ctor_m1988_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m3659_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m3530_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference FlagsAttribute_t406_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool FlagsAttribute_t406_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t832_0_0_0;
static Il2CppInterfaceOffsetPair FlagsAttribute_t406_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FlagsAttribute_t406_0_0_0;
extern const Il2CppType FlagsAttribute_t406_1_0_0;
extern const Il2CppType Attribute_t539_0_0_0;
struct FlagsAttribute_t406;
const Il2CppTypeDefinitionMetadata FlagsAttribute_t406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FlagsAttribute_t406_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, FlagsAttribute_t406_VTable/* vtableMethods */
	, FlagsAttribute_t406_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FlagsAttribute_t406_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FlagsAttribute"/* name */
	, "System"/* namespaze */
	, FlagsAttribute_t406_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FlagsAttribute_t406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 878/* custom_attributes_cache */
	, &FlagsAttribute_t406_0_0_0/* byval_arg */
	, &FlagsAttribute_t406_1_0_0/* this_arg */
	, &FlagsAttribute_t406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FlagsAttribute_t406)/* instance_size */
	, sizeof (FlagsAttribute_t406)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.FormatException
#include "mscorlib_System_FormatException.h"
// Metadata Definition System.FormatException
extern TypeInfo FormatException_t743_il2cpp_TypeInfo;
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor()
extern const MethodInfo FormatException__ctor_m9996_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m9996/* method */
	, &FormatException_t743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormatException_t743_FormatException__ctor_m3427_ParameterInfos[] = 
{
	{"message", 0, 134224110, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.String)
extern const MethodInfo FormatException__ctor_m3427_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m3427/* method */
	, &FormatException_t743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, FormatException_t743_FormatException__ctor_m3427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo FormatException_t743_FormatException__ctor_m4847_ParameterInfos[] = 
{
	{"info", 0, 134224111, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224112, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FormatException__ctor_m4847_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m4847/* method */
	, &FormatException_t743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, FormatException_t743_FormatException__ctor_m4847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatException_t743_MethodInfos[] =
{
	&FormatException__ctor_m9996_MethodInfo,
	&FormatException__ctor_m3427_MethodInfo,
	&FormatException__ctor_m4847_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatException_t743_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool FormatException_t743_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatException_t743_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatException_t743_0_0_0;
extern const Il2CppType FormatException_t743_1_0_0;
struct FormatException_t743;
const Il2CppTypeDefinitionMetadata FormatException_t743_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatException_t743_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, FormatException_t743_VTable/* vtableMethods */
	, FormatException_t743_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2374/* fieldStart */

};
TypeInfo FormatException_t743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatException"/* name */
	, "System"/* namespaze */
	, FormatException_t743_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatException_t743_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 879/* custom_attributes_cache */
	, &FormatException_t743_0_0_0/* byval_arg */
	, &FormatException_t743_1_0_0/* this_arg */
	, &FormatException_t743_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatException_t743)/* instance_size */
	, sizeof (FormatException_t743)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.GC
#include "mscorlib_System_GC.h"
// Metadata Definition System.GC
extern TypeInfo GC_t1849_il2cpp_TypeInfo;
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GC_t1849_GC_SuppressFinalize_m5812_ParameterInfos[] = 
{
	{"obj", 0, 134224113, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.GC::SuppressFinalize(System.Object)
extern const MethodInfo GC_SuppressFinalize_m5812_MethodInfo = 
{
	"SuppressFinalize"/* name */
	, (methodPointerType)&GC_SuppressFinalize_m5812/* method */
	, &GC_t1849_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, GC_t1849_GC_SuppressFinalize_m5812_ParameterInfos/* parameters */
	, 880/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GC_t1849_MethodInfos[] =
{
	&GC_SuppressFinalize_m5812_MethodInfo,
	NULL
};
static const Il2CppMethodReference GC_t1849_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool GC_t1849_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GC_t1849_0_0_0;
extern const Il2CppType GC_t1849_1_0_0;
struct GC_t1849;
const Il2CppTypeDefinitionMetadata GC_t1849_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GC_t1849_VTable/* vtableMethods */
	, GC_t1849_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GC_t1849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GC"/* name */
	, "System"/* namespaze */
	, GC_t1849_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GC_t1849_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GC_t1849_0_0_0/* byval_arg */
	, &GC_t1849_1_0_0/* this_arg */
	, &GC_t1849_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GC_t1849)/* instance_size */
	, sizeof (GC_t1849)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Guid
#include "mscorlib_System_Guid.h"
// Metadata Definition System.Guid
extern TypeInfo Guid_t769_il2cpp_TypeInfo;
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo Guid_t769_Guid__ctor_m9997_ParameterInfos[] = 
{
	{"b", 0, 134224114, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Byte[])
extern const MethodInfo Guid__ctor_m9997_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m9997/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Guid_t769_Guid__ctor_m9997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
static const ParameterInfo Guid_t769_Guid__ctor_m9998_ParameterInfos[] = 
{
	{"a", 0, 134224115, 0, &Int32_t54_0_0_0},
	{"b", 1, 134224116, 0, &Int16_t448_0_0_0},
	{"c", 2, 134224117, 0, &Int16_t448_0_0_0},
	{"d", 3, 134224118, 0, &Byte_t367_0_0_0},
	{"e", 4, 134224119, 0, &Byte_t367_0_0_0},
	{"f", 5, 134224120, 0, &Byte_t367_0_0_0},
	{"g", 6, 134224121, 0, &Byte_t367_0_0_0},
	{"h", 7, 134224122, 0, &Byte_t367_0_0_0},
	{"i", 8, 134224123, 0, &Byte_t367_0_0_0},
	{"j", 9, 134224124, 0, &Byte_t367_0_0_0},
	{"k", 10, 134224125, 0, &Byte_t367_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Int32,System.Int16,System.Int16,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
extern const MethodInfo Guid__ctor_m9998_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m9998/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, Guid_t769_Guid__ctor_m9998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.cctor()
extern const MethodInfo Guid__cctor_m9999_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Guid__cctor_m9999/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t769_Guid_CheckNull_m10000_ParameterInfos[] = 
{
	{"o", 0, 134224126, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckNull(System.Object)
extern const MethodInfo Guid_CheckNull_m10000_MethodInfo = 
{
	"CheckNull"/* name */
	, (methodPointerType)&Guid_CheckNull_m10000/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Guid_t769_Guid_CheckNull_m10000_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Guid_t769_Guid_CheckLength_m10001_ParameterInfos[] = 
{
	{"o", 0, 134224127, 0, &ByteU5BU5D_t546_0_0_0},
	{"l", 1, 134224128, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckLength(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckLength_m10001_MethodInfo = 
{
	"CheckLength"/* name */
	, (methodPointerType)&Guid_CheckLength_m10001/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, Guid_t769_Guid_CheckLength_m10001_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Guid_t769_Guid_CheckArray_m10002_ParameterInfos[] = 
{
	{"o", 0, 134224129, 0, &ByteU5BU5D_t546_0_0_0},
	{"l", 1, 134224130, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckArray(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckArray_m10002_MethodInfo = 
{
	"CheckArray"/* name */
	, (methodPointerType)&Guid_CheckArray_m10002/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, Guid_t769_Guid_CheckArray_m10002_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Guid_t769_Guid_Compare_m10003_ParameterInfos[] = 
{
	{"x", 0, 134224131, 0, &Int32_t54_0_0_0},
	{"y", 1, 134224132, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::Compare(System.Int32,System.Int32)
extern const MethodInfo Guid_Compare_m10003_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&Guid_Compare_m10003/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Guid_t769_Guid_Compare_m10003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t769_Guid_CompareTo_m10004_ParameterInfos[] = 
{
	{"value", 0, 134224133, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Object)
extern const MethodInfo Guid_CompareTo_m10004_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m10004/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Guid_t769_Guid_CompareTo_m10004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t769_Guid_Equals_m10005_ParameterInfos[] = 
{
	{"o", 0, 134224134, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Object)
extern const MethodInfo Guid_Equals_m10005_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m10005/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Guid_t769_Guid_Equals_m10005_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t769_0_0_0;
extern const Il2CppType Guid_t769_0_0_0;
static const ParameterInfo Guid_t769_Guid_CompareTo_m10006_ParameterInfos[] = 
{
	{"value", 0, 134224135, 0, &Guid_t769_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Guid_t769 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Guid)
extern const MethodInfo Guid_CompareTo_m10006_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m10006/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Guid_t769/* invoker_method */
	, Guid_t769_Guid_CompareTo_m10006_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t769_0_0_0;
static const ParameterInfo Guid_t769_Guid_Equals_m10007_ParameterInfos[] = 
{
	{"g", 0, 134224136, 0, &Guid_t769_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Guid_t769 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Guid)
extern const MethodInfo Guid_Equals_m10007_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m10007/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Guid_t769/* invoker_method */
	, Guid_t769_Guid_Equals_m10007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::GetHashCode()
extern const MethodInfo Guid_GetHashCode_m10008_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Guid_GetHashCode_m10008/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Guid_t769_Guid_ToHex_m10009_ParameterInfos[] = 
{
	{"b", 0, 134224137, 0, &Int32_t54_0_0_0},
};
extern const Il2CppType Char_t369_0_0_0;
extern void* RuntimeInvoker_Char_t369_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Guid::ToHex(System.Int32)
extern const MethodInfo Guid_ToHex_m10009_MethodInfo = 
{
	"ToHex"/* name */
	, (methodPointerType)&Guid_ToHex_m10009/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Char_t369_0_0_0/* return_type */
	, RuntimeInvoker_Char_t369_Int32_t54/* invoker_method */
	, Guid_t769_Guid_ToHex_m10009_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Guid_t769 (const MethodInfo* method, void* obj, void** args);
// System.Guid System.Guid::NewGuid()
extern const MethodInfo Guid_NewGuid_m10010_MethodInfo = 
{
	"NewGuid"/* name */
	, (methodPointerType)&Guid_NewGuid_m10010/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Guid_t769_0_0_0/* return_type */
	, RuntimeInvoker_Guid_t769/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Guid_t769_Guid_AppendInt_m10011_ParameterInfos[] = 
{
	{"builder", 0, 134224138, 0, &StringBuilder_t338_0_0_0},
	{"value", 1, 134224139, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendInt(System.Text.StringBuilder,System.Int32)
extern const MethodInfo Guid_AppendInt_m10011_MethodInfo = 
{
	"AppendInt"/* name */
	, (methodPointerType)&Guid_AppendInt_m10011/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, Guid_t769_Guid_AppendInt_m10011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
static const ParameterInfo Guid_t769_Guid_AppendShort_m10012_ParameterInfos[] = 
{
	{"builder", 0, 134224140, 0, &StringBuilder_t338_0_0_0},
	{"value", 1, 134224141, 0, &Int16_t448_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendShort(System.Text.StringBuilder,System.Int16)
extern const MethodInfo Guid_AppendShort_m10012_MethodInfo = 
{
	"AppendShort"/* name */
	, (methodPointerType)&Guid_AppendShort_m10012/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int16_t448/* invoker_method */
	, Guid_t769_Guid_AppendShort_m10012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
static const ParameterInfo Guid_t769_Guid_AppendByte_m10013_ParameterInfos[] = 
{
	{"builder", 0, 134224142, 0, &StringBuilder_t338_0_0_0},
	{"value", 1, 134224143, 0, &Byte_t367_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendByte(System.Text.StringBuilder,System.Byte)
extern const MethodInfo Guid_AppendByte_m10013_MethodInfo = 
{
	"AppendByte"/* name */
	, (methodPointerType)&Guid_AppendByte_m10013/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Guid_t769_Guid_AppendByte_m10013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Guid_t769_Guid_BaseToString_m10014_ParameterInfos[] = 
{
	{"h", 0, 134224144, 0, &Boolean_t72_0_0_0},
	{"p", 1, 134224145, 0, &Boolean_t72_0_0_0},
	{"b", 2, 134224146, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::BaseToString(System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Guid_BaseToString_m10014_MethodInfo = 
{
	"BaseToString"/* name */
	, (methodPointerType)&Guid_BaseToString_m10014/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, Guid_t769_Guid_BaseToString_m10014_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString()
extern const MethodInfo Guid_ToString_m10015_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m10015/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Guid_t769_Guid_ToString_m3488_ParameterInfos[] = 
{
	{"format", 0, 134224147, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String)
extern const MethodInfo Guid_ToString_m3488_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m3488/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Guid_t769_Guid_ToString_m3488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo Guid_t769_Guid_ToString_m10016_ParameterInfos[] = 
{
	{"format", 0, 134224148, 0, &String_t_0_0_0},
	{"provider", 1, 134224149, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String,System.IFormatProvider)
extern const MethodInfo Guid_ToString_m10016_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m10016/* method */
	, &Guid_t769_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Guid_t769_Guid_ToString_m10016_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Guid_t769_MethodInfos[] =
{
	&Guid__ctor_m9997_MethodInfo,
	&Guid__ctor_m9998_MethodInfo,
	&Guid__cctor_m9999_MethodInfo,
	&Guid_CheckNull_m10000_MethodInfo,
	&Guid_CheckLength_m10001_MethodInfo,
	&Guid_CheckArray_m10002_MethodInfo,
	&Guid_Compare_m10003_MethodInfo,
	&Guid_CompareTo_m10004_MethodInfo,
	&Guid_Equals_m10005_MethodInfo,
	&Guid_CompareTo_m10006_MethodInfo,
	&Guid_Equals_m10007_MethodInfo,
	&Guid_GetHashCode_m10008_MethodInfo,
	&Guid_ToHex_m10009_MethodInfo,
	&Guid_NewGuid_m10010_MethodInfo,
	&Guid_AppendInt_m10011_MethodInfo,
	&Guid_AppendShort_m10012_MethodInfo,
	&Guid_AppendByte_m10013_MethodInfo,
	&Guid_BaseToString_m10014_MethodInfo,
	&Guid_ToString_m10015_MethodInfo,
	&Guid_ToString_m3488_MethodInfo,
	&Guid_ToString_m10016_MethodInfo,
	NULL
};
extern const MethodInfo Guid_Equals_m10005_MethodInfo;
extern const MethodInfo Guid_GetHashCode_m10008_MethodInfo;
extern const MethodInfo Guid_ToString_m10015_MethodInfo;
extern const MethodInfo Guid_ToString_m10016_MethodInfo;
extern const MethodInfo Guid_CompareTo_m10004_MethodInfo;
extern const MethodInfo Guid_CompareTo_m10006_MethodInfo;
extern const MethodInfo Guid_Equals_m10007_MethodInfo;
static const Il2CppMethodReference Guid_t769_VTable[] =
{
	&Guid_Equals_m10005_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Guid_GetHashCode_m10008_MethodInfo,
	&Guid_ToString_m10015_MethodInfo,
	&Guid_ToString_m10016_MethodInfo,
	&Guid_CompareTo_m10004_MethodInfo,
	&Guid_CompareTo_m10006_MethodInfo,
	&Guid_Equals_m10007_MethodInfo,
};
static bool Guid_t769_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
extern const Il2CppType IComparable_1_t2403_0_0_0;
extern const Il2CppType IEquatable_1_t2404_0_0_0;
static const Il2CppType* Guid_t769_InterfacesTypeInfos[] = 
{
	&IFormattable_t74_0_0_0,
	&IComparable_t76_0_0_0,
	&IComparable_1_t2403_0_0_0,
	&IEquatable_1_t2404_0_0_0,
};
static Il2CppInterfaceOffsetPair Guid_t769_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IComparable_t76_0_0_0, 5},
	{ &IComparable_1_t2403_0_0_0, 6},
	{ &IEquatable_1_t2404_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Guid_t769_1_0_0;
extern const Il2CppType ValueType_t439_0_0_0;
const Il2CppTypeDefinitionMetadata Guid_t769_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Guid_t769_InterfacesTypeInfos/* implementedInterfaces */
	, Guid_t769_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Guid_t769_VTable/* vtableMethods */
	, Guid_t769_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2375/* fieldStart */

};
TypeInfo Guid_t769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Guid"/* name */
	, "System"/* namespaze */
	, Guid_t769_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Guid_t769_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 881/* custom_attributes_cache */
	, &Guid_t769_0_0_0/* byval_arg */
	, &Guid_t769_1_0_0/* this_arg */
	, &Guid_t769_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Guid_t769)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Guid_t769)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Guid_t769 )/* native_size */
	, sizeof(Guid_t769_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.ICustomFormatter
extern TypeInfo ICustomFormatter_t1926_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo ICustomFormatter_t1926_ICustomFormatter_Format_m11045_ParameterInfos[] = 
{
	{"format", 0, 134224150, 0, &String_t_0_0_0},
	{"arg", 1, 134224151, 0, &Object_t_0_0_0},
	{"formatProvider", 2, 134224152, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ICustomFormatter::Format(System.String,System.Object,System.IFormatProvider)
extern const MethodInfo ICustomFormatter_Format_m11045_MethodInfo = 
{
	"Format"/* name */
	, NULL/* method */
	, &ICustomFormatter_t1926_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ICustomFormatter_t1926_ICustomFormatter_Format_m11045_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICustomFormatter_t1926_MethodInfos[] =
{
	&ICustomFormatter_Format_m11045_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomFormatter_t1926_0_0_0;
extern const Il2CppType ICustomFormatter_t1926_1_0_0;
struct ICustomFormatter_t1926;
const Il2CppTypeDefinitionMetadata ICustomFormatter_t1926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICustomFormatter_t1926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomFormatter"/* name */
	, "System"/* namespaze */
	, ICustomFormatter_t1926_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICustomFormatter_t1926_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 882/* custom_attributes_cache */
	, &ICustomFormatter_t1926_0_0_0/* byval_arg */
	, &ICustomFormatter_t1926_1_0_0/* this_arg */
	, &ICustomFormatter_t1926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IFormatProvider
extern TypeInfo IFormatProvider_t1909_il2cpp_TypeInfo;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatProvider_t1909_IFormatProvider_GetFormat_m11046_ParameterInfos[] = 
{
	{"formatType", 0, 134224153, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.IFormatProvider::GetFormat(System.Type)
extern const MethodInfo IFormatProvider_GetFormat_m11046_MethodInfo = 
{
	"GetFormat"/* name */
	, NULL/* method */
	, &IFormatProvider_t1909_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatProvider_t1909_IFormatProvider_GetFormat_m11046_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatProvider_t1909_MethodInfos[] =
{
	&IFormatProvider_GetFormat_m11046_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatProvider_t1909_1_0_0;
struct IFormatProvider_t1909;
const Il2CppTypeDefinitionMetadata IFormatProvider_t1909_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatProvider_t1909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatProvider"/* name */
	, "System"/* namespaze */
	, IFormatProvider_t1909_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatProvider_t1909_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 883/* custom_attributes_cache */
	, &IFormatProvider_t1909_0_0_0/* byval_arg */
	, &IFormatProvider_t1909_1_0_0/* this_arg */
	, &IFormatProvider_t1909_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// Metadata Definition System.IndexOutOfRangeException
extern TypeInfo IndexOutOfRangeException_t738_il2cpp_TypeInfo;
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor()
extern const MethodInfo IndexOutOfRangeException__ctor_m10017_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m10017/* method */
	, &IndexOutOfRangeException_t738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t738_IndexOutOfRangeException__ctor_m3395_ParameterInfos[] = 
{
	{"message", 0, 134224154, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern const MethodInfo IndexOutOfRangeException__ctor_m3395_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m3395/* method */
	, &IndexOutOfRangeException_t738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IndexOutOfRangeException_t738_IndexOutOfRangeException__ctor_m3395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t738_IndexOutOfRangeException__ctor_m10018_ParameterInfos[] = 
{
	{"info", 0, 134224155, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224156, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IndexOutOfRangeException__ctor_m10018_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m10018/* method */
	, &IndexOutOfRangeException_t738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, IndexOutOfRangeException_t738_IndexOutOfRangeException__ctor_m10018_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexOutOfRangeException_t738_MethodInfos[] =
{
	&IndexOutOfRangeException__ctor_m10017_MethodInfo,
	&IndexOutOfRangeException__ctor_m3395_MethodInfo,
	&IndexOutOfRangeException__ctor_m10018_MethodInfo,
	NULL
};
static const Il2CppMethodReference IndexOutOfRangeException_t738_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool IndexOutOfRangeException_t738_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IndexOutOfRangeException_t738_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IndexOutOfRangeException_t738_0_0_0;
extern const Il2CppType IndexOutOfRangeException_t738_1_0_0;
struct IndexOutOfRangeException_t738;
const Il2CppTypeDefinitionMetadata IndexOutOfRangeException_t738_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IndexOutOfRangeException_t738_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, IndexOutOfRangeException_t738_VTable/* vtableMethods */
	, IndexOutOfRangeException_t738_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IndexOutOfRangeException_t738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexOutOfRangeException"/* name */
	, "System"/* namespaze */
	, IndexOutOfRangeException_t738_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IndexOutOfRangeException_t738_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 884/* custom_attributes_cache */
	, &IndexOutOfRangeException_t738_0_0_0/* byval_arg */
	, &IndexOutOfRangeException_t738_1_0_0/* this_arg */
	, &IndexOutOfRangeException_t738_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IndexOutOfRangeException_t738)/* instance_size */
	, sizeof (IndexOutOfRangeException_t738)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// Metadata Definition System.InvalidCastException
extern TypeInfo InvalidCastException_t1850_il2cpp_TypeInfo;
// System.InvalidCastException
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor()
extern const MethodInfo InvalidCastException__ctor_m10019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m10019/* method */
	, &InvalidCastException_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidCastException_t1850_InvalidCastException__ctor_m10020_ParameterInfos[] = 
{
	{"message", 0, 134224157, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.String)
extern const MethodInfo InvalidCastException__ctor_m10020_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m10020/* method */
	, &InvalidCastException_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvalidCastException_t1850_InvalidCastException__ctor_m10020_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo InvalidCastException_t1850_InvalidCastException__ctor_m10021_ParameterInfos[] = 
{
	{"info", 0, 134224158, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224159, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidCastException__ctor_m10021_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m10021/* method */
	, &InvalidCastException_t1850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, InvalidCastException_t1850_InvalidCastException__ctor_m10021_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidCastException_t1850_MethodInfos[] =
{
	&InvalidCastException__ctor_m10019_MethodInfo,
	&InvalidCastException__ctor_m10020_MethodInfo,
	&InvalidCastException__ctor_m10021_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidCastException_t1850_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool InvalidCastException_t1850_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidCastException_t1850_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidCastException_t1850_0_0_0;
extern const Il2CppType InvalidCastException_t1850_1_0_0;
struct InvalidCastException_t1850;
const Il2CppTypeDefinitionMetadata InvalidCastException_t1850_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidCastException_t1850_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, InvalidCastException_t1850_VTable/* vtableMethods */
	, InvalidCastException_t1850_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2389/* fieldStart */

};
TypeInfo InvalidCastException_t1850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidCastException"/* name */
	, "System"/* namespaze */
	, InvalidCastException_t1850_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidCastException_t1850_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 885/* custom_attributes_cache */
	, &InvalidCastException_t1850_0_0_0/* byval_arg */
	, &InvalidCastException_t1850_1_0_0/* this_arg */
	, &InvalidCastException_t1850_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidCastException_t1850)/* instance_size */
	, sizeof (InvalidCastException_t1850)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// Metadata Definition System.InvalidOperationException
extern TypeInfo InvalidOperationException_t1159_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor()
extern const MethodInfo InvalidOperationException__ctor_m4688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m4688/* method */
	, &InvalidOperationException_t1159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidOperationException_t1159_InvalidOperationException__ctor_m4682_ParameterInfos[] = 
{
	{"message", 0, 134224160, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern const MethodInfo InvalidOperationException__ctor_m4682_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m4682/* method */
	, &InvalidOperationException_t1159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvalidOperationException_t1159_InvalidOperationException__ctor_m4682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t42_0_0_0;
extern const Il2CppType Exception_t42_0_0_0;
static const ParameterInfo InvalidOperationException_t1159_InvalidOperationException__ctor_m10022_ParameterInfos[] = 
{
	{"message", 0, 134224161, 0, &String_t_0_0_0},
	{"innerException", 1, 134224162, 0, &Exception_t42_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
extern const MethodInfo InvalidOperationException__ctor_m10022_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m10022/* method */
	, &InvalidOperationException_t1159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, InvalidOperationException_t1159_InvalidOperationException__ctor_m10022_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo InvalidOperationException_t1159_InvalidOperationException__ctor_m10023_ParameterInfos[] = 
{
	{"info", 0, 134224163, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224164, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidOperationException__ctor_m10023_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m10023/* method */
	, &InvalidOperationException_t1159_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, InvalidOperationException_t1159_InvalidOperationException__ctor_m10023_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidOperationException_t1159_MethodInfos[] =
{
	&InvalidOperationException__ctor_m4688_MethodInfo,
	&InvalidOperationException__ctor_m4682_MethodInfo,
	&InvalidOperationException__ctor_m10022_MethodInfo,
	&InvalidOperationException__ctor_m10023_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidOperationException_t1159_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool InvalidOperationException_t1159_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidOperationException_t1159_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidOperationException_t1159_0_0_0;
extern const Il2CppType InvalidOperationException_t1159_1_0_0;
struct InvalidOperationException_t1159;
const Il2CppTypeDefinitionMetadata InvalidOperationException_t1159_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidOperationException_t1159_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, InvalidOperationException_t1159_VTable/* vtableMethods */
	, InvalidOperationException_t1159_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2390/* fieldStart */

};
TypeInfo InvalidOperationException_t1159_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidOperationException"/* name */
	, "System"/* namespaze */
	, InvalidOperationException_t1159_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidOperationException_t1159_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 886/* custom_attributes_cache */
	, &InvalidOperationException_t1159_0_0_0/* byval_arg */
	, &InvalidOperationException_t1159_1_0_0/* this_arg */
	, &InvalidOperationException_t1159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidOperationException_t1159)/* instance_size */
	, sizeof (InvalidOperationException_t1159)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// Metadata Definition System.LoaderOptimization
extern TypeInfo LoaderOptimization_t1851_il2cpp_TypeInfo;
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimizationMethodDeclarations.h"
static const MethodInfo* LoaderOptimization_t1851_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference LoaderOptimization_t1851_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool LoaderOptimization_t1851_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IConvertible_t75_0_0_0;
static Il2CppInterfaceOffsetPair LoaderOptimization_t1851_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoaderOptimization_t1851_0_0_0;
extern const Il2CppType LoaderOptimization_t1851_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata LoaderOptimization_t1851_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoaderOptimization_t1851_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, LoaderOptimization_t1851_VTable/* vtableMethods */
	, LoaderOptimization_t1851_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2391/* fieldStart */

};
TypeInfo LoaderOptimization_t1851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoaderOptimization"/* name */
	, "System"/* namespaze */
	, LoaderOptimization_t1851_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 887/* custom_attributes_cache */
	, &LoaderOptimization_t1851_0_0_0/* byval_arg */
	, &LoaderOptimization_t1851_1_0_0/* this_arg */
	, &LoaderOptimization_t1851_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoaderOptimization_t1851)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoaderOptimization_t1851)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Math
#include "mscorlib_System_Math.h"
// Metadata Definition System.Math
extern TypeInfo Math_t1852_il2cpp_TypeInfo;
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Math_t1852_Math_Abs_m10024_ParameterInfos[] = 
{
	{"value", 0, 134224165, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single System.Math::Abs(System.Single)
extern const MethodInfo Math_Abs_m10024_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m10024/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Single_t85/* invoker_method */
	, Math_t1852_Math_Abs_m10024_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Math_t1852_Math_Abs_m10025_ParameterInfos[] = 
{
	{"value", 0, 134224166, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Abs(System.Int32)
extern const MethodInfo Math_Abs_m10025_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m10025/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, Math_t1852_Math_Abs_m10025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo Math_t1852_Math_Abs_m10026_ParameterInfos[] = 
{
	{"value", 0, 134224167, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Math::Abs(System.Int64)
extern const MethodInfo Math_Abs_m10026_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m10026/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753_Int64_t753/* invoker_method */
	, Math_t1852_Math_Abs_m10026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Ceiling_m10027_ParameterInfos[] = 
{
	{"a", 0, 134224168, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Ceiling(System.Double)
extern const MethodInfo Math_Ceiling_m10027_MethodInfo = 
{
	"Ceiling"/* name */
	, (methodPointerType)&Math_Ceiling_m10027/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Ceiling_m10027_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Floor_m10028_ParameterInfos[] = 
{
	{"d", 0, 134224169, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Floor(System.Double)
extern const MethodInfo Math_Floor_m10028_MethodInfo = 
{
	"Floor"/* name */
	, (methodPointerType)&Math_Floor_m10028/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Floor_m10028_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Log_m3398_ParameterInfos[] = 
{
	{"a", 0, 134224170, 0, &Double_t752_0_0_0},
	{"newBase", 1, 134224171, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double,System.Double)
extern const MethodInfo Math_Log_m3398_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m3398/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Log_m3398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Math_t1852_Math_Max_m5816_ParameterInfos[] = 
{
	{"val1", 0, 134224172, 0, &Int32_t54_0_0_0},
	{"val2", 1, 134224173, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern const MethodInfo Math_Max_m5816_MethodInfo = 
{
	"Max"/* name */
	, (methodPointerType)&Math_Max_m5816/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Math_t1852_Math_Max_m5816_ParameterInfos/* parameters */
	, 890/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Math_t1852_Math_Min_m10029_ParameterInfos[] = 
{
	{"val1", 0, 134224174, 0, &Int32_t54_0_0_0},
	{"val2", 1, 134224175, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern const MethodInfo Math_Min_m10029_MethodInfo = 
{
	"Min"/* name */
	, (methodPointerType)&Math_Min_m10029/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Math_t1852_Math_Min_m10029_ParameterInfos/* parameters */
	, 891/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Decimal_t755_0_0_0;
extern const Il2CppType Decimal_t755_0_0_0;
static const ParameterInfo Math_t1852_Math_Round_m10030_ParameterInfos[] = 
{
	{"d", 0, 134224176, 0, &Decimal_t755_0_0_0},
};
extern void* RuntimeInvoker_Decimal_t755_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
// System.Decimal System.Math::Round(System.Decimal)
extern const MethodInfo Math_Round_m10030_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m10030/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Decimal_t755_0_0_0/* return_type */
	, RuntimeInvoker_Decimal_t755_Decimal_t755/* invoker_method */
	, Math_t1852_Math_Round_m10030_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Round_m10031_ParameterInfos[] = 
{
	{"a", 0, 134224177, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Round(System.Double)
extern const MethodInfo Math_Round_m10031_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m10031/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Round_m10031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Sin_m10032_ParameterInfos[] = 
{
	{"a", 0, 134224178, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sin(System.Double)
extern const MethodInfo Math_Sin_m10032_MethodInfo = 
{
	"Sin"/* name */
	, (methodPointerType)&Math_Sin_m10032/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Sin_m10032_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Cos_m10033_ParameterInfos[] = 
{
	{"d", 0, 134224179, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Cos(System.Double)
extern const MethodInfo Math_Cos_m10033_MethodInfo = 
{
	"Cos"/* name */
	, (methodPointerType)&Math_Cos_m10033/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Cos_m10033_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Log_m10034_ParameterInfos[] = 
{
	{"d", 0, 134224180, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double)
extern const MethodInfo Math_Log_m10034_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m10034/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Log_m10034_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Pow_m10035_ParameterInfos[] = 
{
	{"x", 0, 134224181, 0, &Double_t752_0_0_0},
	{"y", 1, 134224182, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Pow(System.Double,System.Double)
extern const MethodInfo Math_Pow_m10035_MethodInfo = 
{
	"Pow"/* name */
	, (methodPointerType)&Math_Pow_m10035/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Pow_m10035_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Math_t1852_Math_Sqrt_m10036_ParameterInfos[] = 
{
	{"d", 0, 134224183, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sqrt(System.Double)
extern const MethodInfo Math_Sqrt_m10036_MethodInfo = 
{
	"Sqrt"/* name */
	, (methodPointerType)&Math_Sqrt_m10036/* method */
	, &Math_t1852_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Double_t752/* invoker_method */
	, Math_t1852_Math_Sqrt_m10036_ParameterInfos/* parameters */
	, 892/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Math_t1852_MethodInfos[] =
{
	&Math_Abs_m10024_MethodInfo,
	&Math_Abs_m10025_MethodInfo,
	&Math_Abs_m10026_MethodInfo,
	&Math_Ceiling_m10027_MethodInfo,
	&Math_Floor_m10028_MethodInfo,
	&Math_Log_m3398_MethodInfo,
	&Math_Max_m5816_MethodInfo,
	&Math_Min_m10029_MethodInfo,
	&Math_Round_m10030_MethodInfo,
	&Math_Round_m10031_MethodInfo,
	&Math_Sin_m10032_MethodInfo,
	&Math_Cos_m10033_MethodInfo,
	&Math_Log_m10034_MethodInfo,
	&Math_Pow_m10035_MethodInfo,
	&Math_Sqrt_m10036_MethodInfo,
	NULL
};
static const Il2CppMethodReference Math_t1852_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Math_t1852_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Math_t1852_0_0_0;
extern const Il2CppType Math_t1852_1_0_0;
struct Math_t1852;
const Il2CppTypeDefinitionMetadata Math_t1852_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Math_t1852_VTable/* vtableMethods */
	, Math_t1852_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Math_t1852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Math"/* name */
	, "System"/* namespaze */
	, Math_t1852_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Math_t1852_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Math_t1852_0_0_0/* byval_arg */
	, &Math_t1852_1_0_0/* this_arg */
	, &Math_t1852_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Math_t1852)/* instance_size */
	, sizeof (Math_t1852)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// Metadata Definition System.MemberAccessException
extern TypeInfo MemberAccessException_t1848_il2cpp_TypeInfo;
// System.MemberAccessException
#include "mscorlib_System_MemberAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor()
extern const MethodInfo MemberAccessException__ctor_m10037_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m10037/* method */
	, &MemberAccessException_t1848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MemberAccessException_t1848_MemberAccessException__ctor_m10038_ParameterInfos[] = 
{
	{"message", 0, 134224184, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.String)
extern const MethodInfo MemberAccessException__ctor_m10038_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m10038/* method */
	, &MemberAccessException_t1848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MemberAccessException_t1848_MemberAccessException__ctor_m10038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MemberAccessException_t1848_MemberAccessException__ctor_m10039_ParameterInfos[] = 
{
	{"info", 0, 134224185, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224186, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberAccessException__ctor_m10039_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m10039/* method */
	, &MemberAccessException_t1848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MemberAccessException_t1848_MemberAccessException__ctor_m10039_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberAccessException_t1848_MethodInfos[] =
{
	&MemberAccessException__ctor_m10037_MethodInfo,
	&MemberAccessException__ctor_m10038_MethodInfo,
	&MemberAccessException__ctor_m10039_MethodInfo,
	NULL
};
static const Il2CppMethodReference MemberAccessException_t1848_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MemberAccessException_t1848_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberAccessException_t1848_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberAccessException_t1848_1_0_0;
struct MemberAccessException_t1848;
const Il2CppTypeDefinitionMetadata MemberAccessException_t1848_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberAccessException_t1848_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, MemberAccessException_t1848_VTable/* vtableMethods */
	, MemberAccessException_t1848_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberAccessException_t1848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberAccessException"/* name */
	, "System"/* namespaze */
	, MemberAccessException_t1848_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberAccessException_t1848_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 893/* custom_attributes_cache */
	, &MemberAccessException_t1848_0_0_0/* byval_arg */
	, &MemberAccessException_t1848_1_0_0/* this_arg */
	, &MemberAccessException_t1848_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberAccessException_t1848)/* instance_size */
	, sizeof (MemberAccessException_t1848)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// Metadata Definition System.MethodAccessException
extern TypeInfo MethodAccessException_t1853_il2cpp_TypeInfo;
// System.MethodAccessException
#include "mscorlib_System_MethodAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor()
extern const MethodInfo MethodAccessException__ctor_m10040_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m10040/* method */
	, &MethodAccessException_t1853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MethodAccessException_t1853_MethodAccessException__ctor_m10041_ParameterInfos[] = 
{
	{"info", 0, 134224187, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224188, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodAccessException__ctor_m10041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m10041/* method */
	, &MethodAccessException_t1853_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MethodAccessException_t1853_MethodAccessException__ctor_m10041_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodAccessException_t1853_MethodInfos[] =
{
	&MethodAccessException__ctor_m10040_MethodInfo,
	&MethodAccessException__ctor_m10041_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodAccessException_t1853_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MethodAccessException_t1853_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodAccessException_t1853_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAccessException_t1853_0_0_0;
extern const Il2CppType MethodAccessException_t1853_1_0_0;
struct MethodAccessException_t1853;
const Il2CppTypeDefinitionMetadata MethodAccessException_t1853_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAccessException_t1853_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1848_0_0_0/* parent */
	, MethodAccessException_t1853_VTable/* vtableMethods */
	, MethodAccessException_t1853_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodAccessException_t1853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAccessException"/* name */
	, "System"/* namespaze */
	, MethodAccessException_t1853_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodAccessException_t1853_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 894/* custom_attributes_cache */
	, &MethodAccessException_t1853_0_0_0/* byval_arg */
	, &MethodAccessException_t1853_1_0_0/* this_arg */
	, &MethodAccessException_t1853_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAccessException_t1853)/* instance_size */
	, sizeof (MethodAccessException_t1853)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// Metadata Definition System.MissingFieldException
extern TypeInfo MissingFieldException_t1854_il2cpp_TypeInfo;
// System.MissingFieldException
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor()
extern const MethodInfo MissingFieldException__ctor_m10042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m10042/* method */
	, &MissingFieldException_t1854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingFieldException_t1854_MissingFieldException__ctor_m10043_ParameterInfos[] = 
{
	{"message", 0, 134224189, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.String)
extern const MethodInfo MissingFieldException__ctor_m10043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m10043/* method */
	, &MissingFieldException_t1854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MissingFieldException_t1854_MissingFieldException__ctor_m10043_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MissingFieldException_t1854_MissingFieldException__ctor_m10044_ParameterInfos[] = 
{
	{"info", 0, 134224190, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224191, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingFieldException__ctor_m10044_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m10044/* method */
	, &MissingFieldException_t1854_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MissingFieldException_t1854_MissingFieldException__ctor_m10044_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingFieldException::get_Message()
extern const MethodInfo MissingFieldException_get_Message_m10045_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingFieldException_get_Message_m10045/* method */
	, &MissingFieldException_t1854_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingFieldException_t1854_MethodInfos[] =
{
	&MissingFieldException__ctor_m10042_MethodInfo,
	&MissingFieldException__ctor_m10043_MethodInfo,
	&MissingFieldException__ctor_m10044_MethodInfo,
	&MissingFieldException_get_Message_m10045_MethodInfo,
	NULL
};
extern const MethodInfo MissingFieldException_get_Message_m10045_MethodInfo;
static const PropertyInfo MissingFieldException_t1854____Message_PropertyInfo = 
{
	&MissingFieldException_t1854_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingFieldException_get_Message_m10045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingFieldException_t1854_PropertyInfos[] =
{
	&MissingFieldException_t1854____Message_PropertyInfo,
	NULL
};
extern const MethodInfo MissingMemberException_GetObjectData_m10050_MethodInfo;
static const Il2CppMethodReference MissingFieldException_t1854_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&MissingFieldException_get_Message_m10045_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MissingFieldException_t1854_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingFieldException_t1854_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingFieldException_t1854_0_0_0;
extern const Il2CppType MissingFieldException_t1854_1_0_0;
extern const Il2CppType MissingMemberException_t1855_0_0_0;
struct MissingFieldException_t1854;
const Il2CppTypeDefinitionMetadata MissingFieldException_t1854_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingFieldException_t1854_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t1855_0_0_0/* parent */
	, MissingFieldException_t1854_VTable/* vtableMethods */
	, MissingFieldException_t1854_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MissingFieldException_t1854_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingFieldException"/* name */
	, "System"/* namespaze */
	, MissingFieldException_t1854_MethodInfos/* methods */
	, MissingFieldException_t1854_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingFieldException_t1854_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 895/* custom_attributes_cache */
	, &MissingFieldException_t1854_0_0_0/* byval_arg */
	, &MissingFieldException_t1854_1_0_0/* this_arg */
	, &MissingFieldException_t1854_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingFieldException_t1854)/* instance_size */
	, sizeof (MissingFieldException_t1854)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// Metadata Definition System.MissingMemberException
extern TypeInfo MissingMemberException_t1855_il2cpp_TypeInfo;
// System.MissingMemberException
#include "mscorlib_System_MissingMemberExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor()
extern const MethodInfo MissingMemberException__ctor_m10046_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m10046/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t1855_MissingMemberException__ctor_m10047_ParameterInfos[] = 
{
	{"message", 0, 134224192, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String)
extern const MethodInfo MissingMemberException__ctor_m10047_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m10047/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MissingMemberException_t1855_MissingMemberException__ctor_m10047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MissingMemberException_t1855_MissingMemberException__ctor_m10048_ParameterInfos[] = 
{
	{"info", 0, 134224193, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224194, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException__ctor_m10048_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m10048/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MissingMemberException_t1855_MissingMemberException__ctor_m10048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t1855_MissingMemberException__ctor_m10049_ParameterInfos[] = 
{
	{"className", 0, 134224195, 0, &String_t_0_0_0},
	{"memberName", 1, 134224196, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String,System.String)
extern const MethodInfo MissingMemberException__ctor_m10049_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m10049/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MissingMemberException_t1855_MissingMemberException__ctor_m10049_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MissingMemberException_t1855_MissingMemberException_GetObjectData_m10050_ParameterInfos[] = 
{
	{"info", 0, 134224197, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224198, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException_GetObjectData_m10050_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MissingMemberException_GetObjectData_m10050/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MissingMemberException_t1855_MissingMemberException_GetObjectData_m10050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMemberException::get_Message()
extern const MethodInfo MissingMemberException_get_Message_m10051_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMemberException_get_Message_m10051/* method */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMemberException_t1855_MethodInfos[] =
{
	&MissingMemberException__ctor_m10046_MethodInfo,
	&MissingMemberException__ctor_m10047_MethodInfo,
	&MissingMemberException__ctor_m10048_MethodInfo,
	&MissingMemberException__ctor_m10049_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&MissingMemberException_get_Message_m10051_MethodInfo,
	NULL
};
extern const MethodInfo MissingMemberException_get_Message_m10051_MethodInfo;
static const PropertyInfo MissingMemberException_t1855____Message_PropertyInfo = 
{
	&MissingMemberException_t1855_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMemberException_get_Message_m10051_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMemberException_t1855_PropertyInfos[] =
{
	&MissingMemberException_t1855____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMemberException_t1855_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&MissingMemberException_get_Message_m10051_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MissingMemberException_t1855_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMemberException_t1855_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMemberException_t1855_1_0_0;
struct MissingMemberException_t1855;
const Il2CppTypeDefinitionMetadata MissingMemberException_t1855_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMemberException_t1855_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t1848_0_0_0/* parent */
	, MissingMemberException_t1855_VTable/* vtableMethods */
	, MissingMemberException_t1855_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2398/* fieldStart */

};
TypeInfo MissingMemberException_t1855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMemberException"/* name */
	, "System"/* namespaze */
	, MissingMemberException_t1855_MethodInfos/* methods */
	, MissingMemberException_t1855_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMemberException_t1855_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 896/* custom_attributes_cache */
	, &MissingMemberException_t1855_0_0_0/* byval_arg */
	, &MissingMemberException_t1855_1_0_0/* this_arg */
	, &MissingMemberException_t1855_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMemberException_t1855)/* instance_size */
	, sizeof (MissingMemberException_t1855)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// Metadata Definition System.MissingMethodException
extern TypeInfo MissingMethodException_t1856_il2cpp_TypeInfo;
// System.MissingMethodException
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor()
extern const MethodInfo MissingMethodException__ctor_m10052_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m10052/* method */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t1856_MissingMethodException__ctor_m10053_ParameterInfos[] = 
{
	{"message", 0, 134224199, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String)
extern const MethodInfo MissingMethodException__ctor_m10053_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m10053/* method */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MissingMethodException_t1856_MissingMethodException__ctor_m10053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MissingMethodException_t1856_MissingMethodException__ctor_m10054_ParameterInfos[] = 
{
	{"info", 0, 134224200, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224201, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMethodException__ctor_m10054_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m10054/* method */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MissingMethodException_t1856_MissingMethodException__ctor_m10054_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t1856_MissingMethodException__ctor_m10055_ParameterInfos[] = 
{
	{"className", 0, 134224202, 0, &String_t_0_0_0},
	{"methodName", 1, 134224203, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String,System.String)
extern const MethodInfo MissingMethodException__ctor_m10055_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m10055/* method */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MissingMethodException_t1856_MissingMethodException__ctor_m10055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMethodException::get_Message()
extern const MethodInfo MissingMethodException_get_Message_m10056_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMethodException_get_Message_m10056/* method */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMethodException_t1856_MethodInfos[] =
{
	&MissingMethodException__ctor_m10052_MethodInfo,
	&MissingMethodException__ctor_m10053_MethodInfo,
	&MissingMethodException__ctor_m10054_MethodInfo,
	&MissingMethodException__ctor_m10055_MethodInfo,
	&MissingMethodException_get_Message_m10056_MethodInfo,
	NULL
};
extern const MethodInfo MissingMethodException_get_Message_m10056_MethodInfo;
static const PropertyInfo MissingMethodException_t1856____Message_PropertyInfo = 
{
	&MissingMethodException_t1856_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMethodException_get_Message_m10056_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMethodException_t1856_PropertyInfos[] =
{
	&MissingMethodException_t1856____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMethodException_t1856_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&MissingMethodException_get_Message_m10056_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&MissingMemberException_GetObjectData_m10050_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MissingMethodException_t1856_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMethodException_t1856_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMethodException_t1856_0_0_0;
extern const Il2CppType MissingMethodException_t1856_1_0_0;
struct MissingMethodException_t1856;
const Il2CppTypeDefinitionMetadata MissingMethodException_t1856_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMethodException_t1856_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t1855_0_0_0/* parent */
	, MissingMethodException_t1856_VTable/* vtableMethods */
	, MissingMethodException_t1856_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2401/* fieldStart */

};
TypeInfo MissingMethodException_t1856_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMethodException"/* name */
	, "System"/* namespaze */
	, MissingMethodException_t1856_MethodInfos/* methods */
	, MissingMethodException_t1856_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMethodException_t1856_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 897/* custom_attributes_cache */
	, &MissingMethodException_t1856_0_0_0/* byval_arg */
	, &MissingMethodException_t1856_1_0_0/* this_arg */
	, &MissingMethodException_t1856_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMethodException_t1856)/* instance_size */
	, sizeof (MissingMethodException_t1856)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// Metadata Definition System.MonoAsyncCall
extern TypeInfo MonoAsyncCall_t1857_il2cpp_TypeInfo;
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoAsyncCall::.ctor()
extern const MethodInfo MonoAsyncCall__ctor_m10057_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoAsyncCall__ctor_m10057/* method */
	, &MonoAsyncCall_t1857_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoAsyncCall_t1857_MethodInfos[] =
{
	&MonoAsyncCall__ctor_m10057_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoAsyncCall_t1857_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MonoAsyncCall_t1857_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoAsyncCall_t1857_0_0_0;
extern const Il2CppType MonoAsyncCall_t1857_1_0_0;
struct MonoAsyncCall_t1857;
const Il2CppTypeDefinitionMetadata MonoAsyncCall_t1857_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoAsyncCall_t1857_VTable/* vtableMethods */
	, MonoAsyncCall_t1857_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2402/* fieldStart */

};
TypeInfo MonoAsyncCall_t1857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoAsyncCall"/* name */
	, "System"/* namespaze */
	, MonoAsyncCall_t1857_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoAsyncCall_t1857_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoAsyncCall_t1857_0_0_0/* byval_arg */
	, &MonoAsyncCall_t1857_1_0_0/* this_arg */
	, &MonoAsyncCall_t1857_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoAsyncCall_t1857)/* instance_size */
	, sizeof (MonoAsyncCall_t1857)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern TypeInfo AttributeInfo_t1858_il2cpp_TypeInfo;
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfoMethodDeclarations.h"
extern const Il2CppType AttributeUsageAttribute_t803_0_0_0;
extern const Il2CppType AttributeUsageAttribute_t803_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AttributeInfo_t1858_AttributeInfo__ctor_m10058_ParameterInfos[] = 
{
	{"usage", 0, 134224228, 0, &AttributeUsageAttribute_t803_0_0_0},
	{"inheritanceLevel", 1, 134224229, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs/AttributeInfo::.ctor(System.AttributeUsageAttribute,System.Int32)
extern const MethodInfo AttributeInfo__ctor_m10058_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AttributeInfo__ctor_m10058/* method */
	, &AttributeInfo_t1858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, AttributeInfo_t1858_AttributeInfo__ctor_m10058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs/AttributeInfo::get_Usage()
extern const MethodInfo AttributeInfo_get_Usage_m10059_MethodInfo = 
{
	"get_Usage"/* name */
	, (methodPointerType)&AttributeInfo_get_Usage_m10059/* method */
	, &AttributeInfo_t1858_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t803_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.MonoCustomAttrs/AttributeInfo::get_InheritanceLevel()
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m10060_MethodInfo = 
{
	"get_InheritanceLevel"/* name */
	, (methodPointerType)&AttributeInfo_get_InheritanceLevel_m10060/* method */
	, &AttributeInfo_t1858_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeInfo_t1858_MethodInfos[] =
{
	&AttributeInfo__ctor_m10058_MethodInfo,
	&AttributeInfo_get_Usage_m10059_MethodInfo,
	&AttributeInfo_get_InheritanceLevel_m10060_MethodInfo,
	NULL
};
extern const MethodInfo AttributeInfo_get_Usage_m10059_MethodInfo;
static const PropertyInfo AttributeInfo_t1858____Usage_PropertyInfo = 
{
	&AttributeInfo_t1858_il2cpp_TypeInfo/* parent */
	, "Usage"/* name */
	, &AttributeInfo_get_Usage_m10059_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m10060_MethodInfo;
static const PropertyInfo AttributeInfo_t1858____InheritanceLevel_PropertyInfo = 
{
	&AttributeInfo_t1858_il2cpp_TypeInfo/* parent */
	, "InheritanceLevel"/* name */
	, &AttributeInfo_get_InheritanceLevel_m10060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AttributeInfo_t1858_PropertyInfos[] =
{
	&AttributeInfo_t1858____Usage_PropertyInfo,
	&AttributeInfo_t1858____InheritanceLevel_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AttributeInfo_t1858_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AttributeInfo_t1858_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeInfo_t1858_0_0_0;
extern const Il2CppType AttributeInfo_t1858_1_0_0;
extern TypeInfo MonoCustomAttrs_t1859_il2cpp_TypeInfo;
extern const Il2CppType MonoCustomAttrs_t1859_0_0_0;
struct AttributeInfo_t1858;
const Il2CppTypeDefinitionMetadata AttributeInfo_t1858_DefinitionMetadata = 
{
	&MonoCustomAttrs_t1859_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeInfo_t1858_VTable/* vtableMethods */
	, AttributeInfo_t1858_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2409/* fieldStart */

};
TypeInfo AttributeInfo_t1858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeInfo"/* name */
	, ""/* namespaze */
	, AttributeInfo_t1858_MethodInfos/* methods */
	, AttributeInfo_t1858_PropertyInfos/* properties */
	, NULL/* events */
	, &AttributeInfo_t1858_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeInfo_t1858_0_0_0/* byval_arg */
	, &AttributeInfo_t1858_1_0_0/* this_arg */
	, &AttributeInfo_t1858_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeInfo_t1858)/* instance_size */
	, sizeof (AttributeInfo_t1858)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// Metadata Definition System.MonoCustomAttrs
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs::.cctor()
extern const MethodInfo MonoCustomAttrs__cctor_m10061_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoCustomAttrs__cctor_m10061/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_IsUserCattrProvider_m10062_ParameterInfos[] = 
{
	{"obj", 0, 134224204, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsUserCattrProvider(System.Object)
extern const MethodInfo MonoCustomAttrs_IsUserCattrProvider_m10062_MethodInfo = 
{
	"IsUserCattrProvider"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsUserCattrProvider_m10062/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_IsUserCattrProvider_m10062_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributesInternal_m10063_ParameterInfos[] = 
{
	{"obj", 0, 134224205, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224206, 0, &Type_t_0_0_0},
	{"pseudoAttrs", 2, 134224207, 0, &Boolean_t72_0_0_0},
};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesInternal(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesInternal_m10063_MethodInfo = 
{
	"GetCustomAttributesInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesInternal_m10063/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributesInternal_m10063_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetPseudoCustomAttributes_m10064_ParameterInfos[] = 
{
	{"obj", 0, 134224208, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224209, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetPseudoCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetPseudoCustomAttributes_m10064_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetPseudoCustomAttributes_m10064/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetPseudoCustomAttributes_m10064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributesBase_m10065_ParameterInfos[] = 
{
	{"obj", 0, 134224210, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224211, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesBase(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesBase_m10065_MethodInfo = 
{
	"GetCustomAttributesBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesBase_m10065/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributesBase_m10065_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttribute_m10066_ParameterInfos[] = 
{
	{"obj", 0, 134224212, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224213, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224214, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Attribute System.MonoCustomAttrs::GetCustomAttribute(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttribute_m10066_MethodInfo = 
{
	"GetCustomAttribute"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttribute_m10066/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &Attribute_t539_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttribute_m10066_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributes_m10067_ParameterInfos[] = 
{
	{"obj", 0, 134224215, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224216, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224217, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m10067_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m10067/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributes_m10067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributes_m10068_ParameterInfos[] = 
{
	{"obj", 0, 134224218, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"inherit", 1, 134224219, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m10068_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m10068/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetCustomAttributes_m10068_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_IsDefined_m10069_ParameterInfos[] = 
{
	{"obj", 0, 134224220, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"attributeType", 1, 134224221, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224222, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefined(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_IsDefined_m10069_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefined_m10069/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_IsDefined_m10069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_IsDefinedInternal_m10070_ParameterInfos[] = 
{
	{"obj", 0, 134224223, 0, &ICustomAttributeProvider_t1925_0_0_0},
	{"AttributeType", 1, 134224224, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefinedInternal(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_IsDefinedInternal_m10070_MethodInfo = 
{
	"IsDefinedInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefinedInternal_m10070/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_IsDefinedInternal_m10070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetBasePropertyDefinition_m10071_ParameterInfos[] = 
{
	{"property", 0, 134224225, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoCustomAttrs::GetBasePropertyDefinition(System.Reflection.PropertyInfo)
extern const MethodInfo MonoCustomAttrs_GetBasePropertyDefinition_m10071_MethodInfo = 
{
	"GetBasePropertyDefinition"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBasePropertyDefinition_m10071/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetBasePropertyDefinition_m10071_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_GetBase_m10072_ParameterInfos[] = 
{
	{"obj", 0, 134224226, 0, &ICustomAttributeProvider_t1925_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ICustomAttributeProvider System.MonoCustomAttrs::GetBase(System.Reflection.ICustomAttributeProvider)
extern const MethodInfo MonoCustomAttrs_GetBase_m10072_MethodInfo = 
{
	"GetBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBase_m10072/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &ICustomAttributeProvider_t1925_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_GetBase_m10072_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t1859_MonoCustomAttrs_RetrieveAttributeUsage_m10073_ParameterInfos[] = 
{
	{"attributeType", 0, 134224227, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs::RetrieveAttributeUsage(System.Type)
extern const MethodInfo MonoCustomAttrs_RetrieveAttributeUsage_m10073_MethodInfo = 
{
	"RetrieveAttributeUsage"/* name */
	, (methodPointerType)&MonoCustomAttrs_RetrieveAttributeUsage_m10073/* method */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t803_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t1859_MonoCustomAttrs_RetrieveAttributeUsage_m10073_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoCustomAttrs_t1859_MethodInfos[] =
{
	&MonoCustomAttrs__cctor_m10061_MethodInfo,
	&MonoCustomAttrs_IsUserCattrProvider_m10062_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesInternal_m10063_MethodInfo,
	&MonoCustomAttrs_GetPseudoCustomAttributes_m10064_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesBase_m10065_MethodInfo,
	&MonoCustomAttrs_GetCustomAttribute_m10066_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m10067_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m10068_MethodInfo,
	&MonoCustomAttrs_IsDefined_m10069_MethodInfo,
	&MonoCustomAttrs_IsDefinedInternal_m10070_MethodInfo,
	&MonoCustomAttrs_GetBasePropertyDefinition_m10071_MethodInfo,
	&MonoCustomAttrs_GetBase_m10072_MethodInfo,
	&MonoCustomAttrs_RetrieveAttributeUsage_m10073_MethodInfo,
	NULL
};
static const Il2CppType* MonoCustomAttrs_t1859_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AttributeInfo_t1858_0_0_0,
};
static const Il2CppMethodReference MonoCustomAttrs_t1859_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MonoCustomAttrs_t1859_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCustomAttrs_t1859_1_0_0;
struct MonoCustomAttrs_t1859;
const Il2CppTypeDefinitionMetadata MonoCustomAttrs_t1859_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoCustomAttrs_t1859_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoCustomAttrs_t1859_VTable/* vtableMethods */
	, MonoCustomAttrs_t1859_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2411/* fieldStart */

};
TypeInfo MonoCustomAttrs_t1859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCustomAttrs"/* name */
	, "System"/* namespaze */
	, MonoCustomAttrs_t1859_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoCustomAttrs_t1859_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCustomAttrs_t1859_0_0_0/* byval_arg */
	, &MonoCustomAttrs_t1859_1_0_0/* this_arg */
	, &MonoCustomAttrs_t1859_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCustomAttrs_t1859)/* instance_size */
	, sizeof (MonoCustomAttrs_t1859)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoCustomAttrs_t1859_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// Metadata Definition System.MonoTouchAOTHelper
extern TypeInfo MonoTouchAOTHelper_t1860_il2cpp_TypeInfo;
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTouchAOTHelper::.cctor()
extern const MethodInfo MonoTouchAOTHelper__cctor_m10074_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoTouchAOTHelper__cctor_m10074/* method */
	, &MonoTouchAOTHelper_t1860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTouchAOTHelper_t1860_MethodInfos[] =
{
	&MonoTouchAOTHelper__cctor_m10074_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTouchAOTHelper_t1860_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MonoTouchAOTHelper_t1860_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTouchAOTHelper_t1860_0_0_0;
extern const Il2CppType MonoTouchAOTHelper_t1860_1_0_0;
struct MonoTouchAOTHelper_t1860;
const Il2CppTypeDefinitionMetadata MonoTouchAOTHelper_t1860_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTouchAOTHelper_t1860_VTable/* vtableMethods */
	, MonoTouchAOTHelper_t1860_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2414/* fieldStart */

};
TypeInfo MonoTouchAOTHelper_t1860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTouchAOTHelper"/* name */
	, "System"/* namespaze */
	, MonoTouchAOTHelper_t1860_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTouchAOTHelper_t1860_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTouchAOTHelper_t1860_0_0_0/* byval_arg */
	, &MonoTouchAOTHelper_t1860_1_0_0/* this_arg */
	, &MonoTouchAOTHelper_t1860_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTouchAOTHelper_t1860)/* instance_size */
	, sizeof (MonoTouchAOTHelper_t1860)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoTouchAOTHelper_t1860_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// Metadata Definition System.MonoTypeInfo
extern TypeInfo MonoTypeInfo_t1861_il2cpp_TypeInfo;
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTypeInfo::.ctor()
extern const MethodInfo MonoTypeInfo__ctor_m10075_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTypeInfo__ctor_m10075/* method */
	, &MonoTypeInfo_t1861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTypeInfo_t1861_MethodInfos[] =
{
	&MonoTypeInfo__ctor_m10075_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTypeInfo_t1861_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MonoTypeInfo_t1861_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTypeInfo_t1861_0_0_0;
extern const Il2CppType MonoTypeInfo_t1861_1_0_0;
struct MonoTypeInfo_t1861;
const Il2CppTypeDefinitionMetadata MonoTypeInfo_t1861_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTypeInfo_t1861_VTable/* vtableMethods */
	, MonoTypeInfo_t1861_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2415/* fieldStart */

};
TypeInfo MonoTypeInfo_t1861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTypeInfo"/* name */
	, "System"/* namespaze */
	, MonoTypeInfo_t1861_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTypeInfo_t1861_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTypeInfo_t1861_0_0_0/* byval_arg */
	, &MonoTypeInfo_t1861_1_0_0/* this_arg */
	, &MonoTypeInfo_t1861_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTypeInfo_t1861)/* instance_size */
	, sizeof (MonoTypeInfo_t1861)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoType
#include "mscorlib_System_MonoType.h"
// Metadata Definition System.MonoType
extern TypeInfo MonoType_t_il2cpp_TypeInfo;
// System.MonoType
#include "mscorlib_System_MonoTypeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_get_attributes_m10076_ParameterInfos[] = 
{
	{"type", 0, 134224230, 0, &Type_t_0_0_0},
};
extern const Il2CppType TypeAttributes_t1591_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t1591_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::get_attributes(System.Type)
extern const MethodInfo MonoType_get_attributes_m10076_MethodInfo = 
{
	"get_attributes"/* name */
	, (methodPointerType)&MonoType_get_attributes_m10076/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t1591_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t1591_Object_t/* invoker_method */
	, MonoType_t_MonoType_get_attributes_m10076_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t632_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetDefaultConstructor()
extern const MethodInfo MonoType_GetDefaultConstructor_m10077_MethodInfo = 
{
	"GetDefaultConstructor"/* name */
	, (methodPointerType)&MonoType_GetDefaultConstructor_m10077/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TypeAttributes_t1591 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::GetAttributeFlagsImpl()
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m10078_MethodInfo = 
{
	"GetAttributeFlagsImpl"/* name */
	, (methodPointerType)&MonoType_GetAttributeFlagsImpl_m10078/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t1591_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t1591/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 59/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType CallingConventions_t1565_0_0_0;
extern const Il2CppType CallingConventions_t1565_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructorImpl_m10079_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224231, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 1, 134224232, 0, &Binder_t781_0_0_0},
	{"callConvention", 2, 134224233, 0, &CallingConventions_t1565_0_0_0},
	{"types", 3, 134224234, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 4, 134224235, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetConstructorImpl_m10079_MethodInfo = 
{
	"GetConstructorImpl"/* name */
	, (methodPointerType)&MonoType_GetConstructorImpl_m10079/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructorImpl_m10079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 58/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_internal_m10080_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224236, 0, &BindingFlags_t1564_0_0_0},
	{"reflected_type", 1, 134224237, 0, &Type_t_0_0_0},
};
extern const Il2CppType ConstructorInfoU5BU5D_t773_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetConstructors_internal_m10080_MethodInfo = 
{
	"GetConstructors_internal"/* name */
	, (methodPointerType)&MonoType_GetConstructors_internal_m10080/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t773_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_internal_m10080_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_m10081_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224238, 0, &BindingFlags_t1564_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetConstructors_m10081_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&MonoType_GetConstructors_m10081/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t773_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_m10081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 72/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InternalGetEvent_m10082_ParameterInfos[] = 
{
	{"name", 0, 134224239, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224240, 0, &BindingFlags_t1564_0_0_0},
};
extern const Il2CppType EventInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::InternalGetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_InternalGetEvent_m10082_MethodInfo = 
{
	"InternalGetEvent"/* name */
	, (methodPointerType)&MonoType_InternalGetEvent_m10082/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_InternalGetEvent_m10082_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetEvent_m10083_ParameterInfos[] = 
{
	{"name", 0, 134224241, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224242, 0, &BindingFlags_t1564_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::GetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetEvent_m10083_MethodInfo = 
{
	"GetEvent"/* name */
	, (methodPointerType)&MonoType_GetEvent_m10083/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetEvent_m10083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetField_m10084_ParameterInfos[] = 
{
	{"name", 0, 134224243, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224244, 0, &BindingFlags_t1564_0_0_0},
};
extern const Il2CppType FieldInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.MonoType::GetField(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetField_m10084_MethodInfo = 
{
	"GetField"/* name */
	, (methodPointerType)&MonoType_GetField_m10084/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetField_m10084_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 44/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_internal_m10085_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224245, 0, &BindingFlags_t1564_0_0_0},
	{"reflected_type", 1, 134224246, 0, &Type_t_0_0_0},
};
extern const Il2CppType FieldInfoU5BU5D_t778_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetFields_internal_m10085_MethodInfo = 
{
	"GetFields_internal"/* name */
	, (methodPointerType)&MonoType_GetFields_internal_m10085/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t778_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetFields_internal_m10085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_m10086_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224247, 0, &BindingFlags_t1564_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetFields_m10086_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&MonoType_GetFields_m10086/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t778_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetFields_m10086_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetInterfaces()
extern const MethodInfo MonoType_GetInterfaces_m10087_MethodInfo = 
{
	"GetInterfaces"/* name */
	, (methodPointerType)&MonoType_GetInterfaces_m10087/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 39/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodsByName_m10088_ParameterInfos[] = 
{
	{"name", 0, 134224248, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224249, 0, &BindingFlags_t1564_0_0_0},
	{"ignoreCase", 2, 134224250, 0, &Boolean_t72_0_0_0},
	{"reflected_type", 3, 134224251, 0, &Type_t_0_0_0},
};
extern const Il2CppType MethodInfoU5BU5D_t1575_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetMethodsByName_m10088_MethodInfo = 
{
	"GetMethodsByName"/* name */
	, (methodPointerType)&MonoType_GetMethodsByName_m10088/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t1575_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodsByName_m10088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethods_m10089_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224252, 0, &BindingFlags_t1564_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethods(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetMethods_m10089_MethodInfo = 
{
	"GetMethods"/* name */
	, (methodPointerType)&MonoType_GetMethods_m10089/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t1575_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetMethods_m10089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType CallingConventions_t1565_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodImpl_m10090_ParameterInfos[] = 
{
	{"name", 0, 134224253, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224254, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134224255, 0, &Binder_t781_0_0_0},
	{"callConvention", 3, 134224256, 0, &CallingConventions_t1565_0_0_0},
	{"types", 4, 134224257, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 5, 134224258, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.MonoType::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetMethodImpl_m10090_MethodInfo = 
{
	"GetMethodImpl"/* name */
	, (methodPointerType)&MonoType_GetMethodImpl_m10090/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodImpl_m10090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertiesByName_m10091_ParameterInfos[] = 
{
	{"name", 0, 134224259, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224260, 0, &BindingFlags_t1564_0_0_0},
	{"icase", 2, 134224261, 0, &Boolean_t72_0_0_0},
	{"reflected_type", 3, 134224262, 0, &Type_t_0_0_0},
};
extern const Il2CppType PropertyInfoU5BU5D_t777_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetPropertiesByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetPropertiesByName_m10091_MethodInfo = 
{
	"GetPropertiesByName"/* name */
	, (methodPointerType)&MonoType_GetPropertiesByName_m10091/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t777_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertiesByName_m10091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetProperties_m10092_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224263, 0, &BindingFlags_t1564_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetProperties(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetProperties_m10092_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&MonoType_GetProperties_m10092/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t777_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, MonoType_t_MonoType_GetProperties_m10092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertyImpl_m10093_ParameterInfos[] = 
{
	{"name", 0, 134224264, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224265, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134224266, 0, &Binder_t781_0_0_0},
	{"returnType", 3, 134224267, 0, &Type_t_0_0_0},
	{"types", 4, 134224268, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 5, 134224269, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoType::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetPropertyImpl_m10093_MethodInfo = 
{
	"GetPropertyImpl"/* name */
	, (methodPointerType)&MonoType_GetPropertyImpl_m10093/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertyImpl_m10093_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::HasElementTypeImpl()
extern const MethodInfo MonoType_HasElementTypeImpl_m10094_MethodInfo = 
{
	"HasElementTypeImpl"/* name */
	, (methodPointerType)&MonoType_HasElementTypeImpl_m10094/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 60/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsArrayImpl()
extern const MethodInfo MonoType_IsArrayImpl_m10095_MethodInfo = 
{
	"IsArrayImpl"/* name */
	, (methodPointerType)&MonoType_IsArrayImpl_m10095/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 61/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsByRefImpl()
extern const MethodInfo MonoType_IsByRefImpl_m10096_MethodInfo = 
{
	"IsByRefImpl"/* name */
	, (methodPointerType)&MonoType_IsByRefImpl_m10096/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 62/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPointerImpl()
extern const MethodInfo MonoType_IsPointerImpl_m10097_MethodInfo = 
{
	"IsPointerImpl"/* name */
	, (methodPointerType)&MonoType_IsPointerImpl_m10097/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 63/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPrimitiveImpl()
extern const MethodInfo MonoType_IsPrimitiveImpl_m10098_MethodInfo = 
{
	"IsPrimitiveImpl"/* name */
	, (methodPointerType)&MonoType_IsPrimitiveImpl_m10098/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 64/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsSubclassOf_m10099_ParameterInfos[] = 
{
	{"type", 0, 134224270, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsSubclassOf(System.Type)
extern const MethodInfo MonoType_IsSubclassOf_m10099_MethodInfo = 
{
	"IsSubclassOf"/* name */
	, (methodPointerType)&MonoType_IsSubclassOf_m10099/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MonoType_t_MonoType_IsSubclassOf_m10099_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InvokeMember_m10100_ParameterInfos[] = 
{
	{"name", 0, 134224271, 0, &String_t_0_0_0},
	{"invokeAttr", 1, 134224272, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134224273, 0, &Binder_t781_0_0_0},
	{"target", 3, 134224274, 0, &Object_t_0_0_0},
	{"args", 4, 134224275, 0, &ObjectU5BU5D_t29_0_0_0},
	{"modifiers", 5, 134224276, 0, &ParameterModifierU5BU5D_t782_0_0_0},
	{"culture", 6, 134224277, 0, &CultureInfo_t750_0_0_0},
	{"namedParameters", 7, 134224278, 0, &StringU5BU5D_t45_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.MonoType::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern const MethodInfo MonoType_InvokeMember_m10100_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&MonoType_InvokeMember_m10100/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_InvokeMember_m10100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 73/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetElementType()
extern const MethodInfo MonoType_GetElementType_m10101_MethodInfo = 
{
	"GetElementType"/* name */
	, (methodPointerType)&MonoType_GetElementType_m10101/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_UnderlyingSystemType()
extern const MethodInfo MonoType_get_UnderlyingSystemType_m10102_MethodInfo = 
{
	"get_UnderlyingSystemType"/* name */
	, (methodPointerType)&MonoType_get_UnderlyingSystemType_m10102/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t1164_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.MonoType::get_Assembly()
extern const MethodInfo MonoType_get_Assembly_m10103_MethodInfo = 
{
	"get_Assembly"/* name */
	, (methodPointerType)&MonoType_get_Assembly_m10103/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_AssemblyQualifiedName()
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m10104_MethodInfo = 
{
	"get_AssemblyQualifiedName"/* name */
	, (methodPointerType)&MonoType_get_AssemblyQualifiedName_m10104/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoType_t_MonoType_getFullName_m10105_ParameterInfos[] = 
{
	{"full_name", 0, 134224279, 0, &Boolean_t72_0_0_0},
	{"assembly_qualified", 1, 134224280, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::getFullName(System.Boolean,System.Boolean)
extern const MethodInfo MonoType_getFullName_m10105_MethodInfo = 
{
	"getFullName"/* name */
	, (methodPointerType)&MonoType_getFullName_m10105/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, MonoType_t_MonoType_getFullName_m10105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_BaseType()
extern const MethodInfo MonoType_get_BaseType_m10106_MethodInfo = 
{
	"get_BaseType"/* name */
	, (methodPointerType)&MonoType_get_BaseType_m10106/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_FullName()
extern const MethodInfo MonoType_get_FullName_m10107_MethodInfo = 
{
	"get_FullName"/* name */
	, (methodPointerType)&MonoType_get_FullName_m10107/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsDefined_m10108_ParameterInfos[] = 
{
	{"attributeType", 0, 134224281, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224282, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoType_IsDefined_m10108_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoType_IsDefined_m10108/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoType_t_MonoType_IsDefined_m10108_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m10109_ParameterInfos[] = 
{
	{"inherit", 0, 134224283, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m10109_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m10109/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m10109_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m10110_ParameterInfos[] = 
{
	{"attributeType", 0, 134224284, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224285, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m10110_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m10110/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m10110_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberTypes_t1570_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.MonoType::get_MemberType()
extern const MethodInfo MonoType_get_MemberType_m10111_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&MonoType_get_MemberType_m10111/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Name()
extern const MethodInfo MonoType_get_Name_m10112_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoType_get_Name_m10112/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Namespace()
extern const MethodInfo MonoType_get_Namespace_m10113_MethodInfo = 
{
	"get_Namespace"/* name */
	, (methodPointerType)&MonoType_get_Namespace_m10113/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t1549_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Module System.MonoType::get_Module()
extern const MethodInfo MonoType_get_Module_m10114_MethodInfo = 
{
	"get_Module"/* name */
	, (methodPointerType)&MonoType_get_Module_m10114/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1549_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_DeclaringType()
extern const MethodInfo MonoType_get_DeclaringType_m10115_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoType_get_DeclaringType_m10115/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_ReflectedType()
extern const MethodInfo MonoType_get_ReflectedType_m10116_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoType_get_ReflectedType_m10116/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeTypeHandle_t1371_0_0_0;
extern void* RuntimeInvoker_RuntimeTypeHandle_t1371 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeTypeHandle System.MonoType::get_TypeHandle()
extern const MethodInfo MonoType_get_TypeHandle_m10117_MethodInfo = 
{
	"get_TypeHandle"/* name */
	, (methodPointerType)&MonoType_get_TypeHandle_m10117/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeTypeHandle_t1371_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeTypeHandle_t1371/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetObjectData_m10118_ParameterInfos[] = 
{
	{"info", 0, 134224286, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224287, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoType_GetObjectData_m10118_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoType_GetObjectData_m10118/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoType_t_MonoType_GetObjectData_m10118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 81/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::ToString()
extern const MethodInfo MonoType_ToString_m10119_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoType_ToString_m10119/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetGenericArguments()
extern const MethodInfo MonoType_GetGenericArguments_m10120_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MonoType_GetGenericArguments_m10120/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 74/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_ContainsGenericParameters()
extern const MethodInfo MonoType_get_ContainsGenericParameters_m10121_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MonoType_get_ContainsGenericParameters_m10121/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 75/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_IsGenericParameter()
extern const MethodInfo MonoType_get_IsGenericParameter_m10122_MethodInfo = 
{
	"get_IsGenericParameter"/* name */
	, (methodPointerType)&MonoType_get_IsGenericParameter_m10122/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 80/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetGenericTypeDefinition()
extern const MethodInfo MonoType_GetGenericTypeDefinition_m10123_MethodInfo = 
{
	"GetGenericTypeDefinition"/* name */
	, (methodPointerType)&MonoType_GetGenericTypeDefinition_m10123/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 77/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t47_0_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
static const ParameterInfo MonoType_t_MonoType_CheckMethodSecurity_m10124_ParameterInfos[] = 
{
	{"mb", 0, 134224288, 0, &MethodBase_t47_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.MonoType::CheckMethodSecurity(System.Reflection.MethodBase)
extern const MethodInfo MonoType_CheckMethodSecurity_m10124_MethodInfo = 
{
	"CheckMethodSecurity"/* name */
	, (methodPointerType)&MonoType_CheckMethodSecurity_m10124/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_CheckMethodSecurity_m10124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
static const ParameterInfo MonoType_t_MonoType_ReorderParamArrayArguments_m10125_ParameterInfos[] = 
{
	{"args", 0, 134224289, 0, &ObjectU5BU5D_t29_1_0_0},
	{"method", 1, 134224290, 0, &MethodBase_t47_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::ReorderParamArrayArguments(System.Object[]&,System.Reflection.MethodBase)
extern const MethodInfo MonoType_ReorderParamArrayArguments_m10125_MethodInfo = 
{
	"ReorderParamArrayArguments"/* name */
	, (methodPointerType)&MonoType_ReorderParamArrayArguments_m10125/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t/* invoker_method */
	, MonoType_t_MonoType_ReorderParamArrayArguments_m10125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoType_t_MethodInfos[] =
{
	&MonoType_get_attributes_m10076_MethodInfo,
	&MonoType_GetDefaultConstructor_m10077_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m10078_MethodInfo,
	&MonoType_GetConstructorImpl_m10079_MethodInfo,
	&MonoType_GetConstructors_internal_m10080_MethodInfo,
	&MonoType_GetConstructors_m10081_MethodInfo,
	&MonoType_InternalGetEvent_m10082_MethodInfo,
	&MonoType_GetEvent_m10083_MethodInfo,
	&MonoType_GetField_m10084_MethodInfo,
	&MonoType_GetFields_internal_m10085_MethodInfo,
	&MonoType_GetFields_m10086_MethodInfo,
	&MonoType_GetInterfaces_m10087_MethodInfo,
	&MonoType_GetMethodsByName_m10088_MethodInfo,
	&MonoType_GetMethods_m10089_MethodInfo,
	&MonoType_GetMethodImpl_m10090_MethodInfo,
	&MonoType_GetPropertiesByName_m10091_MethodInfo,
	&MonoType_GetProperties_m10092_MethodInfo,
	&MonoType_GetPropertyImpl_m10093_MethodInfo,
	&MonoType_HasElementTypeImpl_m10094_MethodInfo,
	&MonoType_IsArrayImpl_m10095_MethodInfo,
	&MonoType_IsByRefImpl_m10096_MethodInfo,
	&MonoType_IsPointerImpl_m10097_MethodInfo,
	&MonoType_IsPrimitiveImpl_m10098_MethodInfo,
	&MonoType_IsSubclassOf_m10099_MethodInfo,
	&MonoType_InvokeMember_m10100_MethodInfo,
	&MonoType_GetElementType_m10101_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m10102_MethodInfo,
	&MonoType_get_Assembly_m10103_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m10104_MethodInfo,
	&MonoType_getFullName_m10105_MethodInfo,
	&MonoType_get_BaseType_m10106_MethodInfo,
	&MonoType_get_FullName_m10107_MethodInfo,
	&MonoType_IsDefined_m10108_MethodInfo,
	&MonoType_GetCustomAttributes_m10109_MethodInfo,
	&MonoType_GetCustomAttributes_m10110_MethodInfo,
	&MonoType_get_MemberType_m10111_MethodInfo,
	&MonoType_get_Name_m10112_MethodInfo,
	&MonoType_get_Namespace_m10113_MethodInfo,
	&MonoType_get_Module_m10114_MethodInfo,
	&MonoType_get_DeclaringType_m10115_MethodInfo,
	&MonoType_get_ReflectedType_m10116_MethodInfo,
	&MonoType_get_TypeHandle_m10117_MethodInfo,
	&MonoType_GetObjectData_m10118_MethodInfo,
	&MonoType_ToString_m10119_MethodInfo,
	&MonoType_GetGenericArguments_m10120_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m10121_MethodInfo,
	&MonoType_get_IsGenericParameter_m10122_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m10123_MethodInfo,
	&MonoType_CheckMethodSecurity_m10124_MethodInfo,
	&MonoType_ReorderParamArrayArguments_m10125_MethodInfo,
	NULL
};
extern const MethodInfo MonoType_get_UnderlyingSystemType_m10102_MethodInfo;
static const PropertyInfo MonoType_t____UnderlyingSystemType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "UnderlyingSystemType"/* name */
	, &MonoType_get_UnderlyingSystemType_m10102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Assembly_m10103_MethodInfo;
static const PropertyInfo MonoType_t____Assembly_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Assembly"/* name */
	, &MonoType_get_Assembly_m10103_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m10104_MethodInfo;
static const PropertyInfo MonoType_t____AssemblyQualifiedName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "AssemblyQualifiedName"/* name */
	, &MonoType_get_AssemblyQualifiedName_m10104_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_BaseType_m10106_MethodInfo;
static const PropertyInfo MonoType_t____BaseType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "BaseType"/* name */
	, &MonoType_get_BaseType_m10106_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_FullName_m10107_MethodInfo;
static const PropertyInfo MonoType_t____FullName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "FullName"/* name */
	, &MonoType_get_FullName_m10107_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_MemberType_m10111_MethodInfo;
static const PropertyInfo MonoType_t____MemberType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &MonoType_get_MemberType_m10111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Name_m10112_MethodInfo;
static const PropertyInfo MonoType_t____Name_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoType_get_Name_m10112_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Namespace_m10113_MethodInfo;
static const PropertyInfo MonoType_t____Namespace_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Namespace"/* name */
	, &MonoType_get_Namespace_m10113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Module_m10114_MethodInfo;
static const PropertyInfo MonoType_t____Module_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Module"/* name */
	, &MonoType_get_Module_m10114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_DeclaringType_m10115_MethodInfo;
static const PropertyInfo MonoType_t____DeclaringType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoType_get_DeclaringType_m10115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ReflectedType_m10116_MethodInfo;
static const PropertyInfo MonoType_t____ReflectedType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoType_get_ReflectedType_m10116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_TypeHandle_m10117_MethodInfo;
static const PropertyInfo MonoType_t____TypeHandle_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "TypeHandle"/* name */
	, &MonoType_get_TypeHandle_m10117_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ContainsGenericParameters_m10121_MethodInfo;
static const PropertyInfo MonoType_t____ContainsGenericParameters_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MonoType_get_ContainsGenericParameters_m10121_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_IsGenericParameter_m10122_MethodInfo;
static const PropertyInfo MonoType_t____IsGenericParameter_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "IsGenericParameter"/* name */
	, &MonoType_get_IsGenericParameter_m10122_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoType_t_PropertyInfos[] =
{
	&MonoType_t____UnderlyingSystemType_PropertyInfo,
	&MonoType_t____Assembly_PropertyInfo,
	&MonoType_t____AssemblyQualifiedName_PropertyInfo,
	&MonoType_t____BaseType_PropertyInfo,
	&MonoType_t____FullName_PropertyInfo,
	&MonoType_t____MemberType_PropertyInfo,
	&MonoType_t____Name_PropertyInfo,
	&MonoType_t____Namespace_PropertyInfo,
	&MonoType_t____Module_PropertyInfo,
	&MonoType_t____DeclaringType_PropertyInfo,
	&MonoType_t____ReflectedType_PropertyInfo,
	&MonoType_t____TypeHandle_PropertyInfo,
	&MonoType_t____ContainsGenericParameters_PropertyInfo,
	&MonoType_t____IsGenericParameter_PropertyInfo,
	NULL
};
extern const MethodInfo Type_Equals_m6651_MethodInfo;
extern const MethodInfo Type_GetHashCode_m6665_MethodInfo;
extern const MethodInfo MonoType_ToString_m10119_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m10110_MethodInfo;
extern const MethodInfo MonoType_IsDefined_m10108_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m10109_MethodInfo;
extern const MethodInfo Type_get_Attributes_m6631_MethodInfo;
extern const MethodInfo Type_get_HasElementType_m6633_MethodInfo;
extern const MethodInfo Type_get_IsAbstract_m6634_MethodInfo;
extern const MethodInfo Type_get_IsArray_m6635_MethodInfo;
extern const MethodInfo Type_get_IsByRef_m6636_MethodInfo;
extern const MethodInfo Type_get_IsClass_m6637_MethodInfo;
extern const MethodInfo Type_get_IsContextful_m6638_MethodInfo;
extern const MethodInfo Type_get_IsEnum_m6639_MethodInfo;
extern const MethodInfo Type_get_IsExplicitLayout_m6640_MethodInfo;
extern const MethodInfo Type_get_IsInterface_m6641_MethodInfo;
extern const MethodInfo Type_get_IsMarshalByRef_m6642_MethodInfo;
extern const MethodInfo Type_get_IsPointer_m6643_MethodInfo;
extern const MethodInfo Type_get_IsPrimitive_m6644_MethodInfo;
extern const MethodInfo Type_get_IsSealed_m6645_MethodInfo;
extern const MethodInfo Type_get_IsSerializable_m6646_MethodInfo;
extern const MethodInfo Type_get_IsValueType_m6647_MethodInfo;
extern const MethodInfo Type_Equals_m6652_MethodInfo;
extern const MethodInfo MonoType_IsSubclassOf_m10099_MethodInfo;
extern const MethodInfo MonoType_GetInterfaces_m10087_MethodInfo;
extern const MethodInfo Type_IsAssignableFrom_m6663_MethodInfo;
extern const MethodInfo Type_IsInstanceOfType_m6664_MethodInfo;
extern const MethodInfo MonoType_GetElementType_m10101_MethodInfo;
extern const MethodInfo MonoType_GetEvent_m10083_MethodInfo;
extern const MethodInfo MonoType_GetField_m10084_MethodInfo;
extern const MethodInfo MonoType_GetFields_m10086_MethodInfo;
extern const MethodInfo Type_GetMethod_m6666_MethodInfo;
extern const MethodInfo Type_GetMethod_m6667_MethodInfo;
extern const MethodInfo Type_GetMethod_m6668_MethodInfo;
extern const MethodInfo Type_GetMethod_m6669_MethodInfo;
extern const MethodInfo MonoType_GetMethodImpl_m10090_MethodInfo;
extern const MethodInfo MonoType_GetMethods_m10089_MethodInfo;
extern const MethodInfo MonoType_GetProperties_m10092_MethodInfo;
extern const MethodInfo Type_GetProperty_m6670_MethodInfo;
extern const MethodInfo Type_GetProperty_m6671_MethodInfo;
extern const MethodInfo Type_GetProperty_m6672_MethodInfo;
extern const MethodInfo Type_GetProperty_m6673_MethodInfo;
extern const MethodInfo MonoType_GetPropertyImpl_m10093_MethodInfo;
extern const MethodInfo MonoType_GetConstructorImpl_m10079_MethodInfo;
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m10078_MethodInfo;
extern const MethodInfo MonoType_HasElementTypeImpl_m10094_MethodInfo;
extern const MethodInfo MonoType_IsArrayImpl_m10095_MethodInfo;
extern const MethodInfo MonoType_IsByRefImpl_m10096_MethodInfo;
extern const MethodInfo MonoType_IsPointerImpl_m10097_MethodInfo;
extern const MethodInfo MonoType_IsPrimitiveImpl_m10098_MethodInfo;
extern const MethodInfo Type_IsValueTypeImpl_m6675_MethodInfo;
extern const MethodInfo Type_IsContextfulImpl_m6676_MethodInfo;
extern const MethodInfo Type_IsMarshalByRefImpl_m6677_MethodInfo;
extern const MethodInfo Type_GetConstructor_m6678_MethodInfo;
extern const MethodInfo Type_GetConstructor_m6679_MethodInfo;
extern const MethodInfo Type_GetConstructor_m6680_MethodInfo;
extern const MethodInfo Type_GetConstructors_m6681_MethodInfo;
extern const MethodInfo MonoType_GetConstructors_m10081_MethodInfo;
extern const MethodInfo MonoType_InvokeMember_m10100_MethodInfo;
extern const MethodInfo MonoType_GetGenericArguments_m10120_MethodInfo;
extern const MethodInfo Type_get_IsGenericTypeDefinition_m6686_MethodInfo;
extern const MethodInfo MonoType_GetGenericTypeDefinition_m10123_MethodInfo;
extern const MethodInfo Type_get_IsGenericType_m6689_MethodInfo;
extern const MethodInfo Type_MakeGenericType_m6691_MethodInfo;
extern const MethodInfo MonoType_GetObjectData_m10118_MethodInfo;
static const Il2CppMethodReference MonoType_t_VTable[] =
{
	&Type_Equals_m6651_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Type_GetHashCode_m6665_MethodInfo,
	&MonoType_ToString_m10119_MethodInfo,
	&MonoType_GetCustomAttributes_m10110_MethodInfo,
	&MonoType_IsDefined_m10108_MethodInfo,
	&MonoType_get_DeclaringType_m10115_MethodInfo,
	&MonoType_get_MemberType_m10111_MethodInfo,
	&MonoType_get_Name_m10112_MethodInfo,
	&MonoType_get_ReflectedType_m10116_MethodInfo,
	&MonoType_get_Module_m10114_MethodInfo,
	&MonoType_IsDefined_m10108_MethodInfo,
	&MonoType_GetCustomAttributes_m10109_MethodInfo,
	&MonoType_GetCustomAttributes_m10110_MethodInfo,
	&MonoType_get_Assembly_m10103_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m10104_MethodInfo,
	&Type_get_Attributes_m6631_MethodInfo,
	&MonoType_get_BaseType_m10106_MethodInfo,
	&MonoType_get_FullName_m10107_MethodInfo,
	&Type_get_HasElementType_m6633_MethodInfo,
	&Type_get_IsAbstract_m6634_MethodInfo,
	&Type_get_IsArray_m6635_MethodInfo,
	&Type_get_IsByRef_m6636_MethodInfo,
	&Type_get_IsClass_m6637_MethodInfo,
	&Type_get_IsContextful_m6638_MethodInfo,
	&Type_get_IsEnum_m6639_MethodInfo,
	&Type_get_IsExplicitLayout_m6640_MethodInfo,
	&Type_get_IsInterface_m6641_MethodInfo,
	&Type_get_IsMarshalByRef_m6642_MethodInfo,
	&Type_get_IsPointer_m6643_MethodInfo,
	&Type_get_IsPrimitive_m6644_MethodInfo,
	&Type_get_IsSealed_m6645_MethodInfo,
	&Type_get_IsSerializable_m6646_MethodInfo,
	&Type_get_IsValueType_m6647_MethodInfo,
	&MonoType_get_Namespace_m10113_MethodInfo,
	&MonoType_get_TypeHandle_m10117_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m10102_MethodInfo,
	&Type_Equals_m6652_MethodInfo,
	&MonoType_IsSubclassOf_m10099_MethodInfo,
	&MonoType_GetInterfaces_m10087_MethodInfo,
	&Type_IsAssignableFrom_m6663_MethodInfo,
	&Type_IsInstanceOfType_m6664_MethodInfo,
	&MonoType_GetElementType_m10101_MethodInfo,
	&MonoType_GetEvent_m10083_MethodInfo,
	&MonoType_GetField_m10084_MethodInfo,
	&MonoType_GetFields_m10086_MethodInfo,
	&Type_GetMethod_m6666_MethodInfo,
	&Type_GetMethod_m6667_MethodInfo,
	&Type_GetMethod_m6668_MethodInfo,
	&Type_GetMethod_m6669_MethodInfo,
	&MonoType_GetMethodImpl_m10090_MethodInfo,
	&MonoType_GetMethods_m10089_MethodInfo,
	&MonoType_GetProperties_m10092_MethodInfo,
	&Type_GetProperty_m6670_MethodInfo,
	&Type_GetProperty_m6671_MethodInfo,
	&Type_GetProperty_m6672_MethodInfo,
	&Type_GetProperty_m6673_MethodInfo,
	&MonoType_GetPropertyImpl_m10093_MethodInfo,
	&MonoType_GetConstructorImpl_m10079_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m10078_MethodInfo,
	&MonoType_HasElementTypeImpl_m10094_MethodInfo,
	&MonoType_IsArrayImpl_m10095_MethodInfo,
	&MonoType_IsByRefImpl_m10096_MethodInfo,
	&MonoType_IsPointerImpl_m10097_MethodInfo,
	&MonoType_IsPrimitiveImpl_m10098_MethodInfo,
	&Type_IsValueTypeImpl_m6675_MethodInfo,
	&Type_IsContextfulImpl_m6676_MethodInfo,
	&Type_IsMarshalByRefImpl_m6677_MethodInfo,
	&Type_GetConstructor_m6678_MethodInfo,
	&Type_GetConstructor_m6679_MethodInfo,
	&Type_GetConstructor_m6680_MethodInfo,
	&Type_GetConstructors_m6681_MethodInfo,
	&MonoType_GetConstructors_m10081_MethodInfo,
	&MonoType_InvokeMember_m10100_MethodInfo,
	&MonoType_GetGenericArguments_m10120_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m10121_MethodInfo,
	&Type_get_IsGenericTypeDefinition_m6686_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m10123_MethodInfo,
	&Type_get_IsGenericType_m6689_MethodInfo,
	&Type_MakeGenericType_m6691_MethodInfo,
	&MonoType_get_IsGenericParameter_m10122_MethodInfo,
	&MonoType_GetObjectData_m10118_MethodInfo,
};
static bool MonoType_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoType_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
extern const Il2CppType IReflect_t1963_0_0_0;
extern const Il2CppType _Type_t1961_0_0_0;
extern const Il2CppType _MemberInfo_t1962_0_0_0;
static Il2CppInterfaceOffsetPair MonoType_t_InterfacesOffsets[] = 
{
	{ &IReflect_t1963_0_0_0, 14},
	{ &_Type_t1961_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType MonoType_t_1_0_0;
struct MonoType_t;
const Il2CppTypeDefinitionMetadata MonoType_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoType_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoType_t_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, MonoType_t_VTable/* vtableMethods */
	, MonoType_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2417/* fieldStart */

};
TypeInfo MonoType_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoType"/* name */
	, "System"/* namespaze */
	, MonoType_t_MethodInfos/* methods */
	, MonoType_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoType_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoType_t_0_0_0/* byval_arg */
	, &MonoType_t_1_0_0/* this_arg */
	, &MonoType_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoType_t)/* instance_size */
	, sizeof (MonoType_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 14/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 82/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// Metadata Definition System.MulticastNotSupportedException
extern TypeInfo MulticastNotSupportedException_t1862_il2cpp_TypeInfo;
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor()
extern const MethodInfo MulticastNotSupportedException__ctor_m10126_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m10126/* method */
	, &MulticastNotSupportedException_t1862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t1862_MulticastNotSupportedException__ctor_m10127_ParameterInfos[] = 
{
	{"message", 0, 134224291, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.String)
extern const MethodInfo MulticastNotSupportedException__ctor_m10127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m10127/* method */
	, &MulticastNotSupportedException_t1862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MulticastNotSupportedException_t1862_MulticastNotSupportedException__ctor_m10127_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t1862_MulticastNotSupportedException__ctor_m10128_ParameterInfos[] = 
{
	{"info", 0, 134224292, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224293, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MulticastNotSupportedException__ctor_m10128_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m10128/* method */
	, &MulticastNotSupportedException_t1862_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MulticastNotSupportedException_t1862_MulticastNotSupportedException__ctor_m10128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MulticastNotSupportedException_t1862_MethodInfos[] =
{
	&MulticastNotSupportedException__ctor_m10126_MethodInfo,
	&MulticastNotSupportedException__ctor_m10127_MethodInfo,
	&MulticastNotSupportedException__ctor_m10128_MethodInfo,
	NULL
};
static const Il2CppMethodReference MulticastNotSupportedException_t1862_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MulticastNotSupportedException_t1862_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MulticastNotSupportedException_t1862_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastNotSupportedException_t1862_0_0_0;
extern const Il2CppType MulticastNotSupportedException_t1862_1_0_0;
struct MulticastNotSupportedException_t1862;
const Il2CppTypeDefinitionMetadata MulticastNotSupportedException_t1862_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastNotSupportedException_t1862_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, MulticastNotSupportedException_t1862_VTable/* vtableMethods */
	, MulticastNotSupportedException_t1862_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MulticastNotSupportedException_t1862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastNotSupportedException"/* name */
	, "System"/* namespaze */
	, MulticastNotSupportedException_t1862_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MulticastNotSupportedException_t1862_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 898/* custom_attributes_cache */
	, &MulticastNotSupportedException_t1862_0_0_0/* byval_arg */
	, &MulticastNotSupportedException_t1862_1_0_0/* this_arg */
	, &MulticastNotSupportedException_t1862_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastNotSupportedException_t1862)/* instance_size */
	, sizeof (MulticastNotSupportedException_t1862)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// Metadata Definition System.NonSerializedAttribute
extern TypeInfo NonSerializedAttribute_t1863_il2cpp_TypeInfo;
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NonSerializedAttribute::.ctor()
extern const MethodInfo NonSerializedAttribute__ctor_m10129_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonSerializedAttribute__ctor_m10129/* method */
	, &NonSerializedAttribute_t1863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonSerializedAttribute_t1863_MethodInfos[] =
{
	&NonSerializedAttribute__ctor_m10129_MethodInfo,
	NULL
};
static const Il2CppMethodReference NonSerializedAttribute_t1863_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool NonSerializedAttribute_t1863_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NonSerializedAttribute_t1863_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NonSerializedAttribute_t1863_0_0_0;
extern const Il2CppType NonSerializedAttribute_t1863_1_0_0;
struct NonSerializedAttribute_t1863;
const Il2CppTypeDefinitionMetadata NonSerializedAttribute_t1863_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NonSerializedAttribute_t1863_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, NonSerializedAttribute_t1863_VTable/* vtableMethods */
	, NonSerializedAttribute_t1863_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonSerializedAttribute_t1863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonSerializedAttribute"/* name */
	, "System"/* namespaze */
	, NonSerializedAttribute_t1863_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonSerializedAttribute_t1863_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 899/* custom_attributes_cache */
	, &NonSerializedAttribute_t1863_0_0_0/* byval_arg */
	, &NonSerializedAttribute_t1863_1_0_0/* this_arg */
	, &NonSerializedAttribute_t1863_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonSerializedAttribute_t1863)/* instance_size */
	, sizeof (NonSerializedAttribute_t1863)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// Metadata Definition System.NotImplementedException
extern TypeInfo NotImplementedException_t1165_il2cpp_TypeInfo;
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor()
extern const MethodInfo NotImplementedException__ctor_m10130_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m10130/* method */
	, &NotImplementedException_t1165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotImplementedException_t1165_NotImplementedException__ctor_m4725_ParameterInfos[] = 
{
	{"message", 0, 134224294, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.String)
extern const MethodInfo NotImplementedException__ctor_m4725_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m4725/* method */
	, &NotImplementedException_t1165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NotImplementedException_t1165_NotImplementedException__ctor_m4725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo NotImplementedException_t1165_NotImplementedException__ctor_m10131_ParameterInfos[] = 
{
	{"info", 0, 134224295, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224296, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotImplementedException__ctor_m10131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m10131/* method */
	, &NotImplementedException_t1165_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, NotImplementedException_t1165_NotImplementedException__ctor_m10131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotImplementedException_t1165_MethodInfos[] =
{
	&NotImplementedException__ctor_m10130_MethodInfo,
	&NotImplementedException__ctor_m4725_MethodInfo,
	&NotImplementedException__ctor_m10131_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotImplementedException_t1165_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool NotImplementedException_t1165_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotImplementedException_t1165_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotImplementedException_t1165_0_0_0;
extern const Il2CppType NotImplementedException_t1165_1_0_0;
struct NotImplementedException_t1165;
const Il2CppTypeDefinitionMetadata NotImplementedException_t1165_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotImplementedException_t1165_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, NotImplementedException_t1165_VTable/* vtableMethods */
	, NotImplementedException_t1165_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NotImplementedException_t1165_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotImplementedException"/* name */
	, "System"/* namespaze */
	, NotImplementedException_t1165_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotImplementedException_t1165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 900/* custom_attributes_cache */
	, &NotImplementedException_t1165_0_0_0/* byval_arg */
	, &NotImplementedException_t1165_1_0_0/* this_arg */
	, &NotImplementedException_t1165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotImplementedException_t1165)/* instance_size */
	, sizeof (NotImplementedException_t1165)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// Metadata Definition System.NotSupportedException
extern TypeInfo NotSupportedException_t32_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor()
extern const MethodInfo NotSupportedException__ctor_m85_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m85/* method */
	, &NotSupportedException_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotSupportedException_t32_NotSupportedException__ctor_m4699_ParameterInfos[] = 
{
	{"message", 0, 134224297, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.String)
extern const MethodInfo NotSupportedException__ctor_m4699_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m4699/* method */
	, &NotSupportedException_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NotSupportedException_t32_NotSupportedException__ctor_m4699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo NotSupportedException_t32_NotSupportedException__ctor_m10132_ParameterInfos[] = 
{
	{"info", 0, 134224298, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224299, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotSupportedException__ctor_m10132_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m10132/* method */
	, &NotSupportedException_t32_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, NotSupportedException_t32_NotSupportedException__ctor_m10132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotSupportedException_t32_MethodInfos[] =
{
	&NotSupportedException__ctor_m85_MethodInfo,
	&NotSupportedException__ctor_m4699_MethodInfo,
	&NotSupportedException__ctor_m10132_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotSupportedException_t32_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool NotSupportedException_t32_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotSupportedException_t32_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotSupportedException_t32_0_0_0;
extern const Il2CppType NotSupportedException_t32_1_0_0;
struct NotSupportedException_t32;
const Il2CppTypeDefinitionMetadata NotSupportedException_t32_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotSupportedException_t32_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, NotSupportedException_t32_VTable/* vtableMethods */
	, NotSupportedException_t32_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2418/* fieldStart */

};
TypeInfo NotSupportedException_t32_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotSupportedException"/* name */
	, "System"/* namespaze */
	, NotSupportedException_t32_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotSupportedException_t32_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 901/* custom_attributes_cache */
	, &NotSupportedException_t32_0_0_0/* byval_arg */
	, &NotSupportedException_t32_1_0_0/* this_arg */
	, &NotSupportedException_t32_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotSupportedException_t32)/* instance_size */
	, sizeof (NotSupportedException_t32)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// Metadata Definition System.NullReferenceException
extern TypeInfo NullReferenceException_t727_il2cpp_TypeInfo;
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor()
extern const MethodInfo NullReferenceException__ctor_m10133_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m10133/* method */
	, &NullReferenceException_t727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NullReferenceException_t727_NullReferenceException__ctor_m3374_ParameterInfos[] = 
{
	{"message", 0, 134224300, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.String)
extern const MethodInfo NullReferenceException__ctor_m3374_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m3374/* method */
	, &NullReferenceException_t727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NullReferenceException_t727_NullReferenceException__ctor_m3374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo NullReferenceException_t727_NullReferenceException__ctor_m10134_ParameterInfos[] = 
{
	{"info", 0, 134224301, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224302, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NullReferenceException__ctor_m10134_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m10134/* method */
	, &NullReferenceException_t727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, NullReferenceException_t727_NullReferenceException__ctor_m10134_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NullReferenceException_t727_MethodInfos[] =
{
	&NullReferenceException__ctor_m10133_MethodInfo,
	&NullReferenceException__ctor_m3374_MethodInfo,
	&NullReferenceException__ctor_m10134_MethodInfo,
	NULL
};
static const Il2CppMethodReference NullReferenceException_t727_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool NullReferenceException_t727_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NullReferenceException_t727_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullReferenceException_t727_0_0_0;
extern const Il2CppType NullReferenceException_t727_1_0_0;
struct NullReferenceException_t727;
const Il2CppTypeDefinitionMetadata NullReferenceException_t727_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullReferenceException_t727_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, NullReferenceException_t727_VTable/* vtableMethods */
	, NullReferenceException_t727_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2419/* fieldStart */

};
TypeInfo NullReferenceException_t727_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullReferenceException"/* name */
	, "System"/* namespaze */
	, NullReferenceException_t727_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NullReferenceException_t727_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 902/* custom_attributes_cache */
	, &NullReferenceException_t727_0_0_0/* byval_arg */
	, &NullReferenceException_t727_1_0_0/* this_arg */
	, &NullReferenceException_t727_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullReferenceException_t727)/* instance_size */
	, sizeof (NullReferenceException_t727)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t1864_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::.ctor()
extern const MethodInfo CustomInfo__ctor_m10135_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CustomInfo__ctor_m10135/* method */
	, &CustomInfo_t1864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_1_0_0;
extern const Il2CppType Boolean_t72_1_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_1_0_0;
static const ParameterInfo CustomInfo_t1864_CustomInfo_GetActiveSection_m10136_ParameterInfos[] = 
{
	{"format", 0, 134224453, 0, &String_t_0_0_0},
	{"positive", 1, 134224454, 0, &Boolean_t72_1_0_0},
	{"zero", 2, 134224455, 0, &Boolean_t72_0_0_0},
	{"offset", 3, 134224456, 0, &Int32_t54_1_0_0},
	{"length", 4, 134224457, 0, &Int32_t54_1_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441_SByte_t73_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::GetActiveSection(System.String,System.Boolean&,System.Boolean,System.Int32&,System.Int32&)
extern const MethodInfo CustomInfo_GetActiveSection_m10136_MethodInfo = 
{
	"GetActiveSection"/* name */
	, (methodPointerType)&CustomInfo_GetActiveSection_m10136/* method */
	, &CustomInfo_t1864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441_SByte_t73_Int32U26_t449_Int32U26_t449/* invoker_method */
	, CustomInfo_t1864_CustomInfo_GetActiveSection_m10136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo CustomInfo_t1864_CustomInfo_Parse_m10137_ParameterInfos[] = 
{
	{"format", 0, 134224458, 0, &String_t_0_0_0},
	{"offset", 1, 134224459, 0, &Int32_t54_0_0_0},
	{"length", 2, 134224460, 0, &Int32_t54_0_0_0},
	{"nfi", 3, 134224461, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern const Il2CppType CustomInfo_t1864_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter/CustomInfo System.NumberFormatter/CustomInfo::Parse(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo CustomInfo_Parse_m10137_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&CustomInfo_Parse_m10137/* method */
	, &CustomInfo_t1864_il2cpp_TypeInfo/* declaring_type */
	, &CustomInfo_t1864_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t/* invoker_method */
	, CustomInfo_t1864_CustomInfo_Parse_m10137_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo CustomInfo_t1864_CustomInfo_Format_m10138_ParameterInfos[] = 
{
	{"format", 0, 134224462, 0, &String_t_0_0_0},
	{"offset", 1, 134224463, 0, &Int32_t54_0_0_0},
	{"length", 2, 134224464, 0, &Int32_t54_0_0_0},
	{"nfi", 3, 134224465, 0, &NumberFormatInfo_t1493_0_0_0},
	{"positive", 4, 134224466, 0, &Boolean_t72_0_0_0},
	{"sb_int", 5, 134224467, 0, &StringBuilder_t338_0_0_0},
	{"sb_dec", 6, 134224468, 0, &StringBuilder_t338_0_0_0},
	{"sb_exp", 7, 134224469, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter/CustomInfo::Format(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo,System.Boolean,System.Text.StringBuilder,System.Text.StringBuilder,System.Text.StringBuilder)
extern const MethodInfo CustomInfo_Format_m10138_MethodInfo = 
{
	"Format"/* name */
	, (methodPointerType)&CustomInfo_Format_m10138/* method */
	, &CustomInfo_t1864_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73_Object_t_Object_t_Object_t/* invoker_method */
	, CustomInfo_t1864_CustomInfo_Format_m10138_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CustomInfo_t1864_MethodInfos[] =
{
	&CustomInfo__ctor_m10135_MethodInfo,
	&CustomInfo_GetActiveSection_m10136_MethodInfo,
	&CustomInfo_Parse_m10137_MethodInfo,
	&CustomInfo_Format_m10138_MethodInfo,
	NULL
};
static const Il2CppMethodReference CustomInfo_t1864_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CustomInfo_t1864_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t1864_1_0_0;
extern TypeInfo NumberFormatter_t1865_il2cpp_TypeInfo;
extern const Il2CppType NumberFormatter_t1865_0_0_0;
struct CustomInfo_t1864;
const Il2CppTypeDefinitionMetadata CustomInfo_t1864_DefinitionMetadata = 
{
	&NumberFormatter_t1865_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t1864_VTable/* vtableMethods */
	, CustomInfo_t1864_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2420/* fieldStart */

};
TypeInfo CustomInfo_t1864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, CustomInfo_t1864_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CustomInfo_t1864_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t1864_0_0_0/* byval_arg */
	, &CustomInfo_t1864_1_0_0/* this_arg */
	, &CustomInfo_t1864_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t1864)/* instance_size */
	, sizeof (CustomInfo_t1864)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType Thread_t1634_0_0_0;
extern const Il2CppType Thread_t1634_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter__ctor_m10139_ParameterInfos[] = 
{
	{"current", 0, 134224303, 0, &Thread_t1634_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.ctor(System.Threading.Thread)
extern const MethodInfo NumberFormatter__ctor_m10139_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NumberFormatter__ctor_m10139/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter__ctor_m10139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.cctor()
extern const MethodInfo NumberFormatter__cctor_m10140_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&NumberFormatter__cctor_m10140/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64U2A_t2405_1_0_2;
extern const Il2CppType UInt64U2A_t2405_1_0_0;
extern const Il2CppType Int32U2A_t2406_1_0_2;
extern const Il2CppType Int32U2A_t2406_1_0_0;
extern const Il2CppType CharU2A_t2057_1_0_2;
extern const Il2CppType CharU2A_t2057_1_0_0;
extern const Il2CppType CharU2A_t2057_1_0_2;
extern const Il2CppType Int64U2A_t2407_1_0_2;
extern const Il2CppType Int64U2A_t2407_1_0_0;
extern const Il2CppType Int32U2A_t2406_1_0_2;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_GetFormatterTables_m10141_ParameterInfos[] = 
{
	{"MantissaBitsTable", 0, 134224304, 0, &UInt64U2A_t2405_1_0_2},
	{"TensExponentTable", 1, 134224305, 0, &Int32U2A_t2406_1_0_2},
	{"DigitLowerTable", 2, 134224306, 0, &CharU2A_t2057_1_0_2},
	{"DigitUpperTable", 3, 134224307, 0, &CharU2A_t2057_1_0_2},
	{"TenPowersList", 4, 134224308, 0, &Int64U2A_t2407_1_0_2},
	{"DecHexDigits", 5, 134224309, 0, &Int32U2A_t2406_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_UInt64U2AU26_t2408_Int32U2AU26_t2409_CharU2AU26_t2410_CharU2AU26_t2410_Int64U2AU26_t2411_Int32U2AU26_t2409 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::GetFormatterTables(System.UInt64*&,System.Int32*&,System.Char*&,System.Char*&,System.Int64*&,System.Int32*&)
extern const MethodInfo NumberFormatter_GetFormatterTables_m10141_MethodInfo = 
{
	"GetFormatterTables"/* name */
	, (methodPointerType)&NumberFormatter_GetFormatterTables_m10141/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt64U2AU26_t2408_Int32U2AU26_t2409_CharU2AU26_t2410_CharU2AU26_t2410_Int64U2AU26_t2411_Int32U2AU26_t2409/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_GetFormatterTables_m10141_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_GetTenPowerOf_m10142_ParameterInfos[] = 
{
	{"i", 0, 134224310, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.NumberFormatter::GetTenPowerOf(System.Int32)
extern const MethodInfo NumberFormatter_GetTenPowerOf_m10142_MethodInfo = 
{
	"GetTenPowerOf"/* name */
	, (methodPointerType)&NumberFormatter_GetTenPowerOf_m10142/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_GetTenPowerOf_m10142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType UInt32_t744_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10143_ParameterInfos[] = 
{
	{"value", 0, 134224311, 0, &UInt32_t744_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m10143_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m10143/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10143_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t756_0_0_0;
extern const Il2CppType UInt64_t756_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10144_ParameterInfos[] = 
{
	{"value", 0, 134224312, 0, &UInt64_t756_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m10144_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m10144/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10144_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType UInt64_t756_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10145_ParameterInfos[] = 
{
	{"hi", 0, 134224313, 0, &UInt32_t744_0_0_0},
	{"lo", 1, 134224314, 0, &UInt64_t756_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32,System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m10145_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m10145/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_InitDecHexDigits_m10145_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FastToDecHex_m10146_ParameterInfos[] = 
{
	{"val", 0, 134224315, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t744_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::FastToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_FastToDecHex_m10146_MethodInfo = 
{
	"FastToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_FastToDecHex_m10146/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t744_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t744_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FastToDecHex_m10146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_ToDecHex_m10147_ParameterInfos[] = 
{
	{"val", 0, 134224316, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t744_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::ToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_ToDecHex_m10147_MethodInfo = 
{
	"ToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_ToDecHex_m10147/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t744_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t744_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_ToDecHex_m10147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FastDecHexLen_m10148_ParameterInfos[] = 
{
	{"val", 0, 134224317, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::FastDecHexLen(System.Int32)
extern const MethodInfo NumberFormatter_FastDecHexLen_m10148_MethodInfo = 
{
	"FastDecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_FastDecHexLen_m10148/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FastDecHexLen_m10148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_DecHexLen_m10149_ParameterInfos[] = 
{
	{"val", 0, 134224318, 0, &UInt32_t744_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen(System.UInt32)
extern const MethodInfo NumberFormatter_DecHexLen_m10149_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m10149/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_DecHexLen_m10149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen()
extern const MethodInfo NumberFormatter_DecHexLen_m10150_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m10150/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_ScaleOrder_m10151_ParameterInfos[] = 
{
	{"hi", 0, 134224319, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ScaleOrder(System.Int64)
extern const MethodInfo NumberFormatter_ScaleOrder_m10151_MethodInfo = 
{
	"ScaleOrder"/* name */
	, (methodPointerType)&NumberFormatter_ScaleOrder_m10151/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_ScaleOrder_m10151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::InitialFloatingPrecision()
extern const MethodInfo NumberFormatter_InitialFloatingPrecision_m10152_MethodInfo = 
{
	"InitialFloatingPrecision"/* name */
	, (methodPointerType)&NumberFormatter_InitialFloatingPrecision_m10152/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_ParsePrecision_m10153_ParameterInfos[] = 
{
	{"format", 0, 134224320, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ParsePrecision(System.String)
extern const MethodInfo NumberFormatter_ParsePrecision_m10153_MethodInfo = 
{
	"ParsePrecision"/* name */
	, (methodPointerType)&NumberFormatter_ParsePrecision_m10153/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_ParsePrecision_m10153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10154_ParameterInfos[] = 
{
	{"format", 0, 134224321, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String)
extern const MethodInfo NumberFormatter_Init_m10154_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10154/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t756_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_InitHex_m10155_ParameterInfos[] = 
{
	{"value", 0, 134224322, 0, &UInt64_t756_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitHex(System.UInt64)
extern const MethodInfo NumberFormatter_InitHex_m10155_MethodInfo = 
{
	"InitHex"/* name */
	, (methodPointerType)&NumberFormatter_InitHex_m10155/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_InitHex_m10155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10156_ParameterInfos[] = 
{
	{"format", 0, 134224323, 0, &String_t_0_0_0},
	{"value", 1, 134224324, 0, &Int32_t54_0_0_0},
	{"defPrecision", 2, 134224325, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m10156_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10156/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10157_ParameterInfos[] = 
{
	{"format", 0, 134224326, 0, &String_t_0_0_0},
	{"value", 1, 134224327, 0, &UInt32_t744_0_0_0},
	{"defPrecision", 2, 134224328, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m10157_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10157/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10158_ParameterInfos[] = 
{
	{"format", 0, 134224329, 0, &String_t_0_0_0},
	{"value", 1, 134224330, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int64)
extern const MethodInfo NumberFormatter_Init_m10158_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10158/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t756_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10159_ParameterInfos[] = 
{
	{"format", 0, 134224331, 0, &String_t_0_0_0},
	{"value", 1, 134224332, 0, &UInt64_t756_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt64)
extern const MethodInfo NumberFormatter_Init_m10159_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10159/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64_t753/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10160_ParameterInfos[] = 
{
	{"format", 0, 134224333, 0, &String_t_0_0_0},
	{"value", 1, 134224334, 0, &Double_t752_0_0_0},
	{"defPrecision", 2, 134224335, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Double_t752_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Double,System.Int32)
extern const MethodInfo NumberFormatter_Init_m10160_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10160/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Double_t752_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t755_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Init_m10161_ParameterInfos[] = 
{
	{"format", 0, 134224336, 0, &String_t_0_0_0},
	{"value", 1, 134224337, 0, &Decimal_t755_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Decimal)
extern const MethodInfo NumberFormatter_Init_m10161_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m10161/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Decimal_t755/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Init_m10161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_ResetCharBuf_m10162_ParameterInfos[] = 
{
	{"size", 0, 134224338, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ResetCharBuf(System.Int32)
extern const MethodInfo NumberFormatter_ResetCharBuf_m10162_MethodInfo = 
{
	"ResetCharBuf"/* name */
	, (methodPointerType)&NumberFormatter_ResetCharBuf_m10162/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_ResetCharBuf_m10162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Resize_m10163_ParameterInfos[] = 
{
	{"len", 0, 134224339, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Resize(System.Int32)
extern const MethodInfo NumberFormatter_Resize_m10163_MethodInfo = 
{
	"Resize"/* name */
	, (methodPointerType)&NumberFormatter_Resize_m10163/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Resize_m10163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Append_m10164_ParameterInfos[] = 
{
	{"c", 0, 134224340, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char)
extern const MethodInfo NumberFormatter_Append_m10164_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m10164/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Append_m10164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Append_m10165_ParameterInfos[] = 
{
	{"c", 0, 134224341, 0, &Char_t369_0_0_0},
	{"cnt", 1, 134224342, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char,System.Int32)
extern const MethodInfo NumberFormatter_Append_m10165_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m10165/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Append_m10165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Append_m10166_ParameterInfos[] = 
{
	{"s", 0, 134224343, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.String)
extern const MethodInfo NumberFormatter_Append_m10166_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m10166/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Append_m10166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_GetNumberFormatInstance_m10167_ParameterInfos[] = 
{
	{"fp", 0, 134224344, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.NumberFormatInfo System.NumberFormatter::GetNumberFormatInstance(System.IFormatProvider)
extern const MethodInfo NumberFormatter_GetNumberFormatInstance_m10167_MethodInfo = 
{
	"GetNumberFormatInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetNumberFormatInstance_m10167/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatInfo_t1493_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_GetNumberFormatInstance_m10167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_set_CurrentCulture_m10168_ParameterInfos[] = 
{
	{"value", 0, 134224345, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::set_CurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_set_CurrentCulture_m10168_MethodInfo = 
{
	"set_CurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_set_CurrentCulture_m10168/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_set_CurrentCulture_m10168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_IntegerDigits()
extern const MethodInfo NumberFormatter_get_IntegerDigits_m10169_MethodInfo = 
{
	"get_IntegerDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_IntegerDigits_m10169/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_DecimalDigits()
extern const MethodInfo NumberFormatter_get_DecimalDigits_m10170_MethodInfo = 
{
	"get_DecimalDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_DecimalDigits_m10170/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsFloatingSource()
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m10171_MethodInfo = 
{
	"get_IsFloatingSource"/* name */
	, (methodPointerType)&NumberFormatter_get_IsFloatingSource_m10171/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZero()
extern const MethodInfo NumberFormatter_get_IsZero_m10172_MethodInfo = 
{
	"get_IsZero"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZero_m10172/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZeroInteger()
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m10173_MethodInfo = 
{
	"get_IsZeroInteger"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZeroInteger_m10173/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_RoundPos_m10174_ParameterInfos[] = 
{
	{"pos", 0, 134224346, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RoundPos(System.Int32)
extern const MethodInfo NumberFormatter_RoundPos_m10174_MethodInfo = 
{
	"RoundPos"/* name */
	, (methodPointerType)&NumberFormatter_RoundPos_m10174/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_RoundPos_m10174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_RoundDecimal_m10175_ParameterInfos[] = 
{
	{"decimals", 0, 134224347, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundDecimal(System.Int32)
extern const MethodInfo NumberFormatter_RoundDecimal_m10175_MethodInfo = 
{
	"RoundDecimal"/* name */
	, (methodPointerType)&NumberFormatter_RoundDecimal_m10175/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_RoundDecimal_m10175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_RoundBits_m10176_ParameterInfos[] = 
{
	{"shift", 0, 134224348, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundBits(System.Int32)
extern const MethodInfo NumberFormatter_RoundBits_m10176_MethodInfo = 
{
	"RoundBits"/* name */
	, (methodPointerType)&NumberFormatter_RoundBits_m10176/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_RoundBits_m10176_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RemoveTrailingZeros()
extern const MethodInfo NumberFormatter_RemoveTrailingZeros_m10177_MethodInfo = 
{
	"RemoveTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_RemoveTrailingZeros_m10177/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AddOneToDecHex()
extern const MethodInfo NumberFormatter_AddOneToDecHex_m10178_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m10178/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AddOneToDecHex_m10179_ParameterInfos[] = 
{
	{"val", 0, 134224349, 0, &UInt32_t744_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t744_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::AddOneToDecHex(System.UInt32)
extern const MethodInfo NumberFormatter_AddOneToDecHex_m10179_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m10179/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t744_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t744_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AddOneToDecHex_m10179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros()
extern const MethodInfo NumberFormatter_CountTrailingZeros_m10180_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m10180/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_CountTrailingZeros_m10181_ParameterInfos[] = 
{
	{"val", 0, 134224350, 0, &UInt32_t744_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros(System.UInt32)
extern const MethodInfo NumberFormatter_CountTrailingZeros_m10181_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m10181/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_CountTrailingZeros_m10181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetInstance()
extern const MethodInfo NumberFormatter_GetInstance_m10182_MethodInfo = 
{
	"GetInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetInstance_m10182/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t1865_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Release()
extern const MethodInfo NumberFormatter_Release_m10183_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&NumberFormatter_Release_m10183/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_SetThreadCurrentCulture_m10184_ParameterInfos[] = 
{
	{"culture", 0, 134224351, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::SetThreadCurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_SetThreadCurrentCulture_m10184_MethodInfo = 
{
	"SetThreadCurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_SetThreadCurrentCulture_m10184/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_SetThreadCurrentCulture_m10184_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType SByte_t73_0_0_0;
extern const Il2CppType SByte_t73_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10185_ParameterInfos[] = 
{
	{"format", 0, 134224352, 0, &String_t_0_0_0},
	{"value", 1, 134224353, 0, &SByte_t73_0_0_0},
	{"fp", 2, 134224354, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.SByte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10185_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10185/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10185_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Byte_t367_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10186_ParameterInfos[] = 
{
	{"format", 0, 134224355, 0, &String_t_0_0_0},
	{"value", 1, 134224356, 0, &Byte_t367_0_0_0},
	{"fp", 2, 134224357, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Byte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10186_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10186/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt16_t371_0_0_0;
extern const Il2CppType UInt16_t371_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10187_ParameterInfos[] = 
{
	{"format", 0, 134224358, 0, &String_t_0_0_0},
	{"value", 1, 134224359, 0, &UInt16_t371_0_0_0},
	{"fp", 2, 134224360, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10187_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10187/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t448_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10187_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10188_ParameterInfos[] = 
{
	{"format", 0, 134224361, 0, &String_t_0_0_0},
	{"value", 1, 134224362, 0, &Int16_t448_0_0_0},
	{"fp", 2, 134224363, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10188_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10188/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t448_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10189_ParameterInfos[] = 
{
	{"format", 0, 134224364, 0, &String_t_0_0_0},
	{"value", 1, 134224365, 0, &UInt32_t744_0_0_0},
	{"fp", 2, 134224366, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10189_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10189/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10190_ParameterInfos[] = 
{
	{"format", 0, 134224367, 0, &String_t_0_0_0},
	{"value", 1, 134224368, 0, &Int32_t54_0_0_0},
	{"fp", 2, 134224369, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10190_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10190/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t756_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10191_ParameterInfos[] = 
{
	{"format", 0, 134224370, 0, &String_t_0_0_0},
	{"value", 1, 134224371, 0, &UInt64_t756_0_0_0},
	{"fp", 2, 134224372, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10191_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10191/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10191_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10192_ParameterInfos[] = 
{
	{"format", 0, 134224373, 0, &String_t_0_0_0},
	{"value", 1, 134224374, 0, &Int64_t753_0_0_0},
	{"fp", 2, 134224375, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10192_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10192/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10193_ParameterInfos[] = 
{
	{"format", 0, 134224376, 0, &String_t_0_0_0},
	{"value", 1, 134224377, 0, &Single_t85_0_0_0},
	{"fp", 2, 134224378, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10193_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10193/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t85_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10194_ParameterInfos[] = 
{
	{"format", 0, 134224379, 0, &String_t_0_0_0},
	{"value", 1, 134224380, 0, &Double_t752_0_0_0},
	{"fp", 2, 134224381, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10194_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10194/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Double_t752_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10194_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t755_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10195_ParameterInfos[] = 
{
	{"format", 0, 134224382, 0, &String_t_0_0_0},
	{"value", 1, 134224383, 0, &Decimal_t755_0_0_0},
	{"fp", 2, 134224384, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Decimal_t755_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Decimal,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10195_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10195/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Decimal_t755_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10195_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10196_ParameterInfos[] = 
{
	{"value", 0, 134224385, 0, &UInt32_t744_0_0_0},
	{"fp", 1, 134224386, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10196_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10196/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10197_ParameterInfos[] = 
{
	{"value", 0, 134224387, 0, &Int32_t54_0_0_0},
	{"fp", 1, 134224388, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10197_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10197/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t756_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10198_ParameterInfos[] = 
{
	{"value", 0, 134224389, 0, &UInt64_t756_0_0_0},
	{"fp", 1, 134224390, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10198_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10198/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t753_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10199_ParameterInfos[] = 
{
	{"value", 0, 134224391, 0, &Int64_t753_0_0_0},
	{"fp", 1, 134224392, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10199_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10199/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t753_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10200_ParameterInfos[] = 
{
	{"value", 0, 134224393, 0, &Single_t85_0_0_0},
	{"fp", 1, 134224394, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10200_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10200/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t85_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10201_ParameterInfos[] = 
{
	{"value", 0, 134224395, 0, &Double_t752_0_0_0},
	{"fp", 1, 134224396, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m10201_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10201/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t752_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FastIntegerToString_m10202_ParameterInfos[] = 
{
	{"value", 0, 134224397, 0, &Int32_t54_0_0_0},
	{"fp", 1, 134224398, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FastIntegerToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_FastIntegerToString_m10202_MethodInfo = 
{
	"FastIntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_FastIntegerToString_m10202/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FastIntegerToString_m10202_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t1909_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_IntegerToString_m10203_ParameterInfos[] = 
{
	{"format", 0, 134224399, 0, &String_t_0_0_0},
	{"fp", 1, 134224400, 0, &IFormatProvider_t1909_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::IntegerToString(System.String,System.IFormatProvider)
extern const MethodInfo NumberFormatter_IntegerToString_m10203_MethodInfo = 
{
	"IntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_IntegerToString_m10203/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_IntegerToString_m10203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_NumberToString_m10204_ParameterInfos[] = 
{
	{"format", 0, 134224401, 0, &String_t_0_0_0},
	{"nfi", 1, 134224402, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_NumberToString_m10204_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m10204/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_NumberToString_m10204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatCurrency_m10205_ParameterInfos[] = 
{
	{"precision", 0, 134224403, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224404, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCurrency(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCurrency_m10205_MethodInfo = 
{
	"FormatCurrency"/* name */
	, (methodPointerType)&NumberFormatter_FormatCurrency_m10205/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatCurrency_m10205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatDecimal_m10206_ParameterInfos[] = 
{
	{"precision", 0, 134224405, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224406, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatDecimal(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatDecimal_m10206_MethodInfo = 
{
	"FormatDecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatDecimal_m10206/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatDecimal_m10206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatHexadecimal_m10207_ParameterInfos[] = 
{
	{"precision", 0, 134224407, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatHexadecimal(System.Int32)
extern const MethodInfo NumberFormatter_FormatHexadecimal_m10207_MethodInfo = 
{
	"FormatHexadecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatHexadecimal_m10207/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatHexadecimal_m10207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatFixedPoint_m10208_ParameterInfos[] = 
{
	{"precision", 0, 134224408, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224409, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatFixedPoint(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatFixedPoint_m10208_MethodInfo = 
{
	"FormatFixedPoint"/* name */
	, (methodPointerType)&NumberFormatter_FormatFixedPoint_m10208/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatFixedPoint_m10208_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatRoundtrip_m10209_ParameterInfos[] = 
{
	{"origval", 0, 134224410, 0, &Double_t752_0_0_0},
	{"nfi", 1, 134224411, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Double,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m10209_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m10209/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t752_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatRoundtrip_m10209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatRoundtrip_m10210_ParameterInfos[] = 
{
	{"origval", 0, 134224412, 0, &Single_t85_0_0_0},
	{"nfi", 1, 134224413, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Single,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m10210_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m10210/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t85_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatRoundtrip_m10210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatGeneral_m10211_ParameterInfos[] = 
{
	{"precision", 0, 134224414, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224415, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatGeneral(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatGeneral_m10211_MethodInfo = 
{
	"FormatGeneral"/* name */
	, (methodPointerType)&NumberFormatter_FormatGeneral_m10211/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatGeneral_m10211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatNumber_m10212_ParameterInfos[] = 
{
	{"precision", 0, 134224416, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224417, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatNumber(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatNumber_m10212_MethodInfo = 
{
	"FormatNumber"/* name */
	, (methodPointerType)&NumberFormatter_FormatNumber_m10212/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatNumber_m10212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatPercent_m10213_ParameterInfos[] = 
{
	{"precision", 0, 134224418, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224419, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatPercent(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatPercent_m10213_MethodInfo = 
{
	"FormatPercent"/* name */
	, (methodPointerType)&NumberFormatter_FormatPercent_m10213/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatPercent_m10213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatExponential_m10214_ParameterInfos[] = 
{
	{"precision", 0, 134224420, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224421, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatExponential_m10214_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m10214/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatExponential_m10214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatExponential_m10215_ParameterInfos[] = 
{
	{"precision", 0, 134224422, 0, &Int32_t54_0_0_0},
	{"nfi", 1, 134224423, 0, &NumberFormatInfo_t1493_0_0_0},
	{"expDigits", 2, 134224424, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo,System.Int32)
extern const MethodInfo NumberFormatter_FormatExponential_m10215_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m10215/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatExponential_m10215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FormatCustom_m10216_ParameterInfos[] = 
{
	{"format", 0, 134224425, 0, &String_t_0_0_0},
	{"nfi", 1, 134224426, 0, &NumberFormatInfo_t1493_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCustom(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCustom_m10216_MethodInfo = 
{
	"FormatCustom"/* name */
	, (methodPointerType)&NumberFormatter_FormatCustom_m10216/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FormatCustom_m10216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_ZeroTrimEnd_m10217_ParameterInfos[] = 
{
	{"sb", 0, 134224427, 0, &StringBuilder_t338_0_0_0},
	{"canEmpty", 1, 134224428, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ZeroTrimEnd(System.Text.StringBuilder,System.Boolean)
extern const MethodInfo NumberFormatter_ZeroTrimEnd_m10217_MethodInfo = 
{
	"ZeroTrimEnd"/* name */
	, (methodPointerType)&NumberFormatter_ZeroTrimEnd_m10217/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_ZeroTrimEnd_m10217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_IsZeroOnly_m10218_ParameterInfos[] = 
{
	{"sb", 0, 134224429, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::IsZeroOnly(System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_IsZeroOnly_m10218_MethodInfo = 
{
	"IsZeroOnly"/* name */
	, (methodPointerType)&NumberFormatter_IsZeroOnly_m10218/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_IsZeroOnly_m10218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendNonNegativeNumber_m10219_ParameterInfos[] = 
{
	{"sb", 0, 134224430, 0, &StringBuilder_t338_0_0_0},
	{"v", 1, 134224431, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendNonNegativeNumber(System.Text.StringBuilder,System.Int32)
extern const MethodInfo NumberFormatter_AppendNonNegativeNumber_m10219_MethodInfo = 
{
	"AppendNonNegativeNumber"/* name */
	, (methodPointerType)&NumberFormatter_AppendNonNegativeNumber_m10219/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendNonNegativeNumber_m10219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendIntegerString_m10220_ParameterInfos[] = 
{
	{"minLength", 0, 134224432, 0, &Int32_t54_0_0_0},
	{"sb", 1, 134224433, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendIntegerString_m10220_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m10220/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendIntegerString_m10220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendIntegerString_m10221_ParameterInfos[] = 
{
	{"minLength", 0, 134224434, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32)
extern const MethodInfo NumberFormatter_AppendIntegerString_m10221_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m10221/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendIntegerString_m10221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendDecimalString_m10222_ParameterInfos[] = 
{
	{"precision", 0, 134224435, 0, &Int32_t54_0_0_0},
	{"sb", 1, 134224436, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDecimalString_m10222_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m10222/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendDecimalString_m10222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendDecimalString_m10223_ParameterInfos[] = 
{
	{"precision", 0, 134224437, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32)
extern const MethodInfo NumberFormatter_AppendDecimalString_m10223_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m10223/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendDecimalString_m10223_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendIntegerStringWithGroupSeparator_m10224_ParameterInfos[] = 
{
	{"groups", 0, 134224438, 0, &Int32U5BU5D_t1082_0_0_0},
	{"groupSeparator", 1, 134224439, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerStringWithGroupSeparator(System.Int32[],System.String)
extern const MethodInfo NumberFormatter_AppendIntegerStringWithGroupSeparator_m10224_MethodInfo = 
{
	"AppendIntegerStringWithGroupSeparator"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerStringWithGroupSeparator_m10224/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendIntegerStringWithGroupSeparator_m10224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NumberFormatInfo_t1493_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendExponent_m10225_ParameterInfos[] = 
{
	{"nfi", 0, 134224440, 0, &NumberFormatInfo_t1493_0_0_0},
	{"exponent", 1, 134224441, 0, &Int32_t54_0_0_0},
	{"minDigits", 2, 134224442, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendExponent(System.Globalization.NumberFormatInfo,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendExponent_m10225_MethodInfo = 
{
	"AppendExponent"/* name */
	, (methodPointerType)&NumberFormatter_AppendExponent_m10225/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendExponent_m10225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendOneDigit_m10226_ParameterInfos[] = 
{
	{"start", 0, 134224443, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendOneDigit(System.Int32)
extern const MethodInfo NumberFormatter_AppendOneDigit_m10226_MethodInfo = 
{
	"AppendOneDigit"/* name */
	, (methodPointerType)&NumberFormatter_AppendOneDigit_m10226/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendOneDigit_m10226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_FastAppendDigits_m10227_ParameterInfos[] = 
{
	{"val", 0, 134224444, 0, &Int32_t54_0_0_0},
	{"force", 1, 134224445, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::FastAppendDigits(System.Int32,System.Boolean)
extern const MethodInfo NumberFormatter_FastAppendDigits_m10227_MethodInfo = 
{
	"FastAppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_FastAppendDigits_m10227/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_FastAppendDigits_m10227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendDigits_m10228_ParameterInfos[] = 
{
	{"start", 0, 134224446, 0, &Int32_t54_0_0_0},
	{"end", 1, 134224447, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendDigits_m10228_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m10228/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendDigits_m10228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_AppendDigits_m10229_ParameterInfos[] = 
{
	{"start", 0, 134224448, 0, &Int32_t54_0_0_0},
	{"end", 1, 134224449, 0, &Int32_t54_0_0_0},
	{"sb", 2, 134224450, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDigits_m10229_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m10229/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_AppendDigits_m10229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Multiply10_m10230_ParameterInfos[] = 
{
	{"count", 0, 134224451, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Multiply10(System.Int32)
extern const MethodInfo NumberFormatter_Multiply10_m10230_MethodInfo = 
{
	"Multiply10"/* name */
	, (methodPointerType)&NumberFormatter_Multiply10_m10230/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Multiply10_m10230_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo NumberFormatter_t1865_NumberFormatter_Divide10_m10231_ParameterInfos[] = 
{
	{"count", 0, 134224452, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Divide10(System.Int32)
extern const MethodInfo NumberFormatter_Divide10_m10231_MethodInfo = 
{
	"Divide10"/* name */
	, (methodPointerType)&NumberFormatter_Divide10_m10231/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, NumberFormatter_t1865_NumberFormatter_Divide10_m10231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetClone()
extern const MethodInfo NumberFormatter_GetClone_m10232_MethodInfo = 
{
	"GetClone"/* name */
	, (methodPointerType)&NumberFormatter_GetClone_m10232/* method */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t1865_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NumberFormatter_t1865_MethodInfos[] =
{
	&NumberFormatter__ctor_m10139_MethodInfo,
	&NumberFormatter__cctor_m10140_MethodInfo,
	&NumberFormatter_GetFormatterTables_m10141_MethodInfo,
	&NumberFormatter_GetTenPowerOf_m10142_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m10143_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m10144_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m10145_MethodInfo,
	&NumberFormatter_FastToDecHex_m10146_MethodInfo,
	&NumberFormatter_ToDecHex_m10147_MethodInfo,
	&NumberFormatter_FastDecHexLen_m10148_MethodInfo,
	&NumberFormatter_DecHexLen_m10149_MethodInfo,
	&NumberFormatter_DecHexLen_m10150_MethodInfo,
	&NumberFormatter_ScaleOrder_m10151_MethodInfo,
	&NumberFormatter_InitialFloatingPrecision_m10152_MethodInfo,
	&NumberFormatter_ParsePrecision_m10153_MethodInfo,
	&NumberFormatter_Init_m10154_MethodInfo,
	&NumberFormatter_InitHex_m10155_MethodInfo,
	&NumberFormatter_Init_m10156_MethodInfo,
	&NumberFormatter_Init_m10157_MethodInfo,
	&NumberFormatter_Init_m10158_MethodInfo,
	&NumberFormatter_Init_m10159_MethodInfo,
	&NumberFormatter_Init_m10160_MethodInfo,
	&NumberFormatter_Init_m10161_MethodInfo,
	&NumberFormatter_ResetCharBuf_m10162_MethodInfo,
	&NumberFormatter_Resize_m10163_MethodInfo,
	&NumberFormatter_Append_m10164_MethodInfo,
	&NumberFormatter_Append_m10165_MethodInfo,
	&NumberFormatter_Append_m10166_MethodInfo,
	&NumberFormatter_GetNumberFormatInstance_m10167_MethodInfo,
	&NumberFormatter_set_CurrentCulture_m10168_MethodInfo,
	&NumberFormatter_get_IntegerDigits_m10169_MethodInfo,
	&NumberFormatter_get_DecimalDigits_m10170_MethodInfo,
	&NumberFormatter_get_IsFloatingSource_m10171_MethodInfo,
	&NumberFormatter_get_IsZero_m10172_MethodInfo,
	&NumberFormatter_get_IsZeroInteger_m10173_MethodInfo,
	&NumberFormatter_RoundPos_m10174_MethodInfo,
	&NumberFormatter_RoundDecimal_m10175_MethodInfo,
	&NumberFormatter_RoundBits_m10176_MethodInfo,
	&NumberFormatter_RemoveTrailingZeros_m10177_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m10178_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m10179_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m10180_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m10181_MethodInfo,
	&NumberFormatter_GetInstance_m10182_MethodInfo,
	&NumberFormatter_Release_m10183_MethodInfo,
	&NumberFormatter_SetThreadCurrentCulture_m10184_MethodInfo,
	&NumberFormatter_NumberToString_m10185_MethodInfo,
	&NumberFormatter_NumberToString_m10186_MethodInfo,
	&NumberFormatter_NumberToString_m10187_MethodInfo,
	&NumberFormatter_NumberToString_m10188_MethodInfo,
	&NumberFormatter_NumberToString_m10189_MethodInfo,
	&NumberFormatter_NumberToString_m10190_MethodInfo,
	&NumberFormatter_NumberToString_m10191_MethodInfo,
	&NumberFormatter_NumberToString_m10192_MethodInfo,
	&NumberFormatter_NumberToString_m10193_MethodInfo,
	&NumberFormatter_NumberToString_m10194_MethodInfo,
	&NumberFormatter_NumberToString_m10195_MethodInfo,
	&NumberFormatter_NumberToString_m10196_MethodInfo,
	&NumberFormatter_NumberToString_m10197_MethodInfo,
	&NumberFormatter_NumberToString_m10198_MethodInfo,
	&NumberFormatter_NumberToString_m10199_MethodInfo,
	&NumberFormatter_NumberToString_m10200_MethodInfo,
	&NumberFormatter_NumberToString_m10201_MethodInfo,
	&NumberFormatter_FastIntegerToString_m10202_MethodInfo,
	&NumberFormatter_IntegerToString_m10203_MethodInfo,
	&NumberFormatter_NumberToString_m10204_MethodInfo,
	&NumberFormatter_FormatCurrency_m10205_MethodInfo,
	&NumberFormatter_FormatDecimal_m10206_MethodInfo,
	&NumberFormatter_FormatHexadecimal_m10207_MethodInfo,
	&NumberFormatter_FormatFixedPoint_m10208_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m10209_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m10210_MethodInfo,
	&NumberFormatter_FormatGeneral_m10211_MethodInfo,
	&NumberFormatter_FormatNumber_m10212_MethodInfo,
	&NumberFormatter_FormatPercent_m10213_MethodInfo,
	&NumberFormatter_FormatExponential_m10214_MethodInfo,
	&NumberFormatter_FormatExponential_m10215_MethodInfo,
	&NumberFormatter_FormatCustom_m10216_MethodInfo,
	&NumberFormatter_ZeroTrimEnd_m10217_MethodInfo,
	&NumberFormatter_IsZeroOnly_m10218_MethodInfo,
	&NumberFormatter_AppendNonNegativeNumber_m10219_MethodInfo,
	&NumberFormatter_AppendIntegerString_m10220_MethodInfo,
	&NumberFormatter_AppendIntegerString_m10221_MethodInfo,
	&NumberFormatter_AppendDecimalString_m10222_MethodInfo,
	&NumberFormatter_AppendDecimalString_m10223_MethodInfo,
	&NumberFormatter_AppendIntegerStringWithGroupSeparator_m10224_MethodInfo,
	&NumberFormatter_AppendExponent_m10225_MethodInfo,
	&NumberFormatter_AppendOneDigit_m10226_MethodInfo,
	&NumberFormatter_FastAppendDigits_m10227_MethodInfo,
	&NumberFormatter_AppendDigits_m10228_MethodInfo,
	&NumberFormatter_AppendDigits_m10229_MethodInfo,
	&NumberFormatter_Multiply10_m10230_MethodInfo,
	&NumberFormatter_Divide10_m10231_MethodInfo,
	&NumberFormatter_GetClone_m10232_MethodInfo,
	NULL
};
extern const MethodInfo NumberFormatter_set_CurrentCulture_m10168_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____CurrentCulture_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "CurrentCulture"/* name */
	, NULL/* get */
	, &NumberFormatter_set_CurrentCulture_m10168_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IntegerDigits_m10169_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____IntegerDigits_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "IntegerDigits"/* name */
	, &NumberFormatter_get_IntegerDigits_m10169_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_DecimalDigits_m10170_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____DecimalDigits_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "DecimalDigits"/* name */
	, &NumberFormatter_get_DecimalDigits_m10170_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m10171_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____IsFloatingSource_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "IsFloatingSource"/* name */
	, &NumberFormatter_get_IsFloatingSource_m10171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZero_m10172_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____IsZero_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "IsZero"/* name */
	, &NumberFormatter_get_IsZero_m10172_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m10173_MethodInfo;
static const PropertyInfo NumberFormatter_t1865____IsZeroInteger_PropertyInfo = 
{
	&NumberFormatter_t1865_il2cpp_TypeInfo/* parent */
	, "IsZeroInteger"/* name */
	, &NumberFormatter_get_IsZeroInteger_m10173_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NumberFormatter_t1865_PropertyInfos[] =
{
	&NumberFormatter_t1865____CurrentCulture_PropertyInfo,
	&NumberFormatter_t1865____IntegerDigits_PropertyInfo,
	&NumberFormatter_t1865____DecimalDigits_PropertyInfo,
	&NumberFormatter_t1865____IsFloatingSource_PropertyInfo,
	&NumberFormatter_t1865____IsZero_PropertyInfo,
	&NumberFormatter_t1865____IsZeroInteger_PropertyInfo,
	NULL
};
static const Il2CppType* NumberFormatter_t1865_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t1864_0_0_0,
};
static const Il2CppMethodReference NumberFormatter_t1865_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool NumberFormatter_t1865_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t1865_1_0_0;
struct NumberFormatter_t1865;
const Il2CppTypeDefinitionMetadata NumberFormatter_t1865_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t1865_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t1865_VTable/* vtableMethods */
	, NumberFormatter_t1865_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2434/* fieldStart */

};
TypeInfo NumberFormatter_t1865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NumberFormatter_t1865_MethodInfos/* methods */
	, NumberFormatter_t1865_PropertyInfos/* properties */
	, NULL/* events */
	, &NumberFormatter_t1865_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t1865_0_0_0/* byval_arg */
	, &NumberFormatter_t1865_1_0_0/* this_arg */
	, &NumberFormatter_t1865_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t1865)/* instance_size */
	, sizeof (NumberFormatter_t1865)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t1865_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t1865_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t1344_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1344_ObjectDisposedException__ctor_m5825_ParameterInfos[] = 
{
	{"objectName", 0, 134224470, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern const MethodInfo ObjectDisposedException__ctor_m5825_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m5825/* method */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjectDisposedException_t1344_ObjectDisposedException__ctor_m5825_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1344_ObjectDisposedException__ctor_m10233_ParameterInfos[] = 
{
	{"objectName", 0, 134224471, 0, &String_t_0_0_0},
	{"message", 1, 134224472, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String,System.String)
extern const MethodInfo ObjectDisposedException__ctor_m10233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m10233/* method */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ObjectDisposedException_t1344_ObjectDisposedException__ctor_m10233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ObjectDisposedException_t1344_ObjectDisposedException__ctor_m10234_ParameterInfos[] = 
{
	{"info", 0, 134224473, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224474, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException__ctor_m10234_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m10234/* method */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ObjectDisposedException_t1344_ObjectDisposedException__ctor_m10234_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ObjectDisposedException::get_Message()
extern const MethodInfo ObjectDisposedException_get_Message_m10235_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&ObjectDisposedException_get_Message_m10235/* method */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ObjectDisposedException_t1344_ObjectDisposedException_GetObjectData_m10236_ParameterInfos[] = 
{
	{"info", 0, 134224475, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224476, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException_GetObjectData_m10236_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjectDisposedException_GetObjectData_m10236/* method */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ObjectDisposedException_t1344_ObjectDisposedException_GetObjectData_m10236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectDisposedException_t1344_MethodInfos[] =
{
	&ObjectDisposedException__ctor_m5825_MethodInfo,
	&ObjectDisposedException__ctor_m10233_MethodInfo,
	&ObjectDisposedException__ctor_m10234_MethodInfo,
	&ObjectDisposedException_get_Message_m10235_MethodInfo,
	&ObjectDisposedException_GetObjectData_m10236_MethodInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_get_Message_m10235_MethodInfo;
static const PropertyInfo ObjectDisposedException_t1344____Message_PropertyInfo = 
{
	&ObjectDisposedException_t1344_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &ObjectDisposedException_get_Message_m10235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectDisposedException_t1344_PropertyInfos[] =
{
	&ObjectDisposedException_t1344____Message_PropertyInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_GetObjectData_m10236_MethodInfo;
static const Il2CppMethodReference ObjectDisposedException_t1344_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&ObjectDisposedException_GetObjectData_m10236_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&ObjectDisposedException_get_Message_m10235_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&ObjectDisposedException_GetObjectData_m10236_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool ObjectDisposedException_t1344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ObjectDisposedException_t1344_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t1344_0_0_0;
extern const Il2CppType ObjectDisposedException_t1344_1_0_0;
struct ObjectDisposedException_t1344;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t1344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t1344_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t1159_0_0_0/* parent */
	, ObjectDisposedException_t1344_VTable/* vtableMethods */
	, ObjectDisposedException_t1344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2460/* fieldStart */

};
TypeInfo ObjectDisposedException_t1344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, ObjectDisposedException_t1344_MethodInfos/* methods */
	, ObjectDisposedException_t1344_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectDisposedException_t1344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 904/* custom_attributes_cache */
	, &ObjectDisposedException_t1344_0_0_0/* byval_arg */
	, &ObjectDisposedException_t1344_1_0_0/* this_arg */
	, &ObjectDisposedException_t1344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t1344)/* instance_size */
	, sizeof (ObjectDisposedException_t1344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t1844_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
extern const Il2CppType PlatformID_t1868_0_0_0;
extern const Il2CppType PlatformID_t1868_0_0_0;
extern const Il2CppType Version_t1006_0_0_0;
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo OperatingSystem_t1844_OperatingSystem__ctor_m10237_ParameterInfos[] = 
{
	{"platform", 0, 134224477, 0, &PlatformID_t1868_0_0_0},
	{"version", 1, 134224478, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::.ctor(System.PlatformID,System.Version)
extern const MethodInfo OperatingSystem__ctor_m10237_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OperatingSystem__ctor_m10237/* method */
	, &OperatingSystem_t1844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, OperatingSystem_t1844_OperatingSystem__ctor_m10237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PlatformID_t1868 (const MethodInfo* method, void* obj, void** args);
// System.PlatformID System.OperatingSystem::get_Platform()
extern const MethodInfo OperatingSystem_get_Platform_m10238_MethodInfo = 
{
	"get_Platform"/* name */
	, (methodPointerType)&OperatingSystem_get_Platform_m10238/* method */
	, &OperatingSystem_t1844_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t1868_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t1868/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo OperatingSystem_t1844_OperatingSystem_GetObjectData_m10239_ParameterInfos[] = 
{
	{"info", 0, 134224479, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224480, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OperatingSystem_GetObjectData_m10239_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&OperatingSystem_GetObjectData_m10239/* method */
	, &OperatingSystem_t1844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, OperatingSystem_t1844_OperatingSystem_GetObjectData_m10239_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.OperatingSystem::ToString()
extern const MethodInfo OperatingSystem_ToString_m10240_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&OperatingSystem_ToString_m10240/* method */
	, &OperatingSystem_t1844_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OperatingSystem_t1844_MethodInfos[] =
{
	&OperatingSystem__ctor_m10237_MethodInfo,
	&OperatingSystem_get_Platform_m10238_MethodInfo,
	&OperatingSystem_GetObjectData_m10239_MethodInfo,
	&OperatingSystem_ToString_m10240_MethodInfo,
	NULL
};
extern const MethodInfo OperatingSystem_get_Platform_m10238_MethodInfo;
static const PropertyInfo OperatingSystem_t1844____Platform_PropertyInfo = 
{
	&OperatingSystem_t1844_il2cpp_TypeInfo/* parent */
	, "Platform"/* name */
	, &OperatingSystem_get_Platform_m10238_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* OperatingSystem_t1844_PropertyInfos[] =
{
	&OperatingSystem_t1844____Platform_PropertyInfo,
	NULL
};
extern const MethodInfo OperatingSystem_ToString_m10240_MethodInfo;
extern const MethodInfo OperatingSystem_GetObjectData_m10239_MethodInfo;
static const Il2CppMethodReference OperatingSystem_t1844_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&OperatingSystem_ToString_m10240_MethodInfo,
	&OperatingSystem_GetObjectData_m10239_MethodInfo,
};
static bool OperatingSystem_t1844_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
static const Il2CppType* OperatingSystem_t1844_InterfacesTypeInfos[] = 
{
	&ICloneable_t427_0_0_0,
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t1844_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t1844_0_0_0;
extern const Il2CppType OperatingSystem_t1844_1_0_0;
struct OperatingSystem_t1844;
const Il2CppTypeDefinitionMetadata OperatingSystem_t1844_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t1844_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t1844_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t1844_VTable/* vtableMethods */
	, OperatingSystem_t1844_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2462/* fieldStart */

};
TypeInfo OperatingSystem_t1844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, OperatingSystem_t1844_MethodInfos/* methods */
	, OperatingSystem_t1844_PropertyInfos/* properties */
	, NULL/* events */
	, &OperatingSystem_t1844_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 905/* custom_attributes_cache */
	, &OperatingSystem_t1844_0_0_0/* byval_arg */
	, &OperatingSystem_t1844_1_0_0/* this_arg */
	, &OperatingSystem_t1844_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t1844)/* instance_size */
	, sizeof (OperatingSystem_t1844)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t1866_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor()
extern const MethodInfo OutOfMemoryException__ctor_m10241_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m10241/* method */
	, &OutOfMemoryException_t1866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo OutOfMemoryException_t1866_OutOfMemoryException__ctor_m10242_ParameterInfos[] = 
{
	{"info", 0, 134224481, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224482, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OutOfMemoryException__ctor_m10242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m10242/* method */
	, &OutOfMemoryException_t1866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, OutOfMemoryException_t1866_OutOfMemoryException__ctor_m10242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OutOfMemoryException_t1866_MethodInfos[] =
{
	&OutOfMemoryException__ctor_m10241_MethodInfo,
	&OutOfMemoryException__ctor_m10242_MethodInfo,
	NULL
};
static const Il2CppMethodReference OutOfMemoryException_t1866_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool OutOfMemoryException_t1866_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t1866_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t1866_0_0_0;
extern const Il2CppType OutOfMemoryException_t1866_1_0_0;
struct OutOfMemoryException_t1866;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t1866_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t1866_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, OutOfMemoryException_t1866_VTable/* vtableMethods */
	, OutOfMemoryException_t1866_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2465/* fieldStart */

};
TypeInfo OutOfMemoryException_t1866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, OutOfMemoryException_t1866_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OutOfMemoryException_t1866_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 906/* custom_attributes_cache */
	, &OutOfMemoryException_t1866_0_0_0/* byval_arg */
	, &OutOfMemoryException_t1866_1_0_0/* this_arg */
	, &OutOfMemoryException_t1866_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t1866)/* instance_size */
	, sizeof (OutOfMemoryException_t1866)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t1867_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor()
extern const MethodInfo OverflowException__ctor_m10243_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m10243/* method */
	, &OverflowException_t1867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OverflowException_t1867_OverflowException__ctor_m10244_ParameterInfos[] = 
{
	{"message", 0, 134224483, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.String)
extern const MethodInfo OverflowException__ctor_m10244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m10244/* method */
	, &OverflowException_t1867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, OverflowException_t1867_OverflowException__ctor_m10244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo OverflowException_t1867_OverflowException__ctor_m10245_ParameterInfos[] = 
{
	{"info", 0, 134224484, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224485, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OverflowException__ctor_m10245_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m10245/* method */
	, &OverflowException_t1867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, OverflowException_t1867_OverflowException__ctor_m10245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OverflowException_t1867_MethodInfos[] =
{
	&OverflowException__ctor_m10243_MethodInfo,
	&OverflowException__ctor_m10244_MethodInfo,
	&OverflowException__ctor_m10245_MethodInfo,
	NULL
};
static const Il2CppMethodReference OverflowException_t1867_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool OverflowException_t1867_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OverflowException_t1867_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t1867_0_0_0;
extern const Il2CppType OverflowException_t1867_1_0_0;
extern const Il2CppType ArithmeticException_t1341_0_0_0;
struct OverflowException_t1867;
const Il2CppTypeDefinitionMetadata OverflowException_t1867_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t1867_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1341_0_0_0/* parent */
	, OverflowException_t1867_VTable/* vtableMethods */
	, OverflowException_t1867_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2466/* fieldStart */

};
TypeInfo OverflowException_t1867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, OverflowException_t1867_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OverflowException_t1867_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 907/* custom_attributes_cache */
	, &OverflowException_t1867_0_0_0/* byval_arg */
	, &OverflowException_t1867_1_0_0/* this_arg */
	, &OverflowException_t1867_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t1867)/* instance_size */
	, sizeof (OverflowException_t1867)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t1868_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const MethodInfo* PlatformID_t1868_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PlatformID_t1868_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool PlatformID_t1868_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PlatformID_t1868_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t1868_1_0_0;
const Il2CppTypeDefinitionMetadata PlatformID_t1868_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t1868_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, PlatformID_t1868_VTable/* vtableMethods */
	, PlatformID_t1868_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2467/* fieldStart */

};
TypeInfo PlatformID_t1868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, PlatformID_t1868_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 908/* custom_attributes_cache */
	, &PlatformID_t1868_0_0_0/* byval_arg */
	, &PlatformID_t1868_1_0_0/* this_arg */
	, &PlatformID_t1868_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t1868)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t1868)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t614_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor()
extern const MethodInfo Random__ctor_m10246_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m10246/* method */
	, &Random_t614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Random_t614_Random__ctor_m3437_ParameterInfos[] = 
{
	{"Seed", 0, 134224486, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor(System.Int32)
extern const MethodInfo Random__ctor_m3437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m3437/* method */
	, &Random_t614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Random_t614_Random__ctor_m3437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Random_t614_MethodInfos[] =
{
	&Random__ctor_m10246_MethodInfo,
	&Random__ctor_m3437_MethodInfo,
	NULL
};
static const Il2CppMethodReference Random_t614_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Random_t614_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t614_0_0_0;
extern const Il2CppType Random_t614_1_0_0;
struct Random_t614;
const Il2CppTypeDefinitionMetadata Random_t614_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t614_VTable/* vtableMethods */
	, Random_t614_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2475/* fieldStart */

};
TypeInfo Random_t614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, Random_t614_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Random_t614_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 909/* custom_attributes_cache */
	, &Random_t614_0_0_0/* byval_arg */
	, &Random_t614_1_0_0/* this_arg */
	, &Random_t614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t614)/* instance_size */
	, sizeof (Random_t614)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t1869_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor()
extern const MethodInfo RankException__ctor_m10247_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m10247/* method */
	, &RankException_t1869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RankException_t1869_RankException__ctor_m10248_ParameterInfos[] = 
{
	{"message", 0, 134224487, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.String)
extern const MethodInfo RankException__ctor_m10248_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m10248/* method */
	, &RankException_t1869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RankException_t1869_RankException__ctor_m10248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo RankException_t1869_RankException__ctor_m10249_ParameterInfos[] = 
{
	{"info", 0, 134224488, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224489, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RankException__ctor_m10249_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m10249/* method */
	, &RankException_t1869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, RankException_t1869_RankException__ctor_m10249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RankException_t1869_MethodInfos[] =
{
	&RankException__ctor_m10247_MethodInfo,
	&RankException__ctor_m10248_MethodInfo,
	&RankException__ctor_m10249_MethodInfo,
	NULL
};
static const Il2CppMethodReference RankException_t1869_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool RankException_t1869_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RankException_t1869_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t1869_0_0_0;
extern const Il2CppType RankException_t1869_1_0_0;
struct RankException_t1869;
const Il2CppTypeDefinitionMetadata RankException_t1869_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t1869_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, RankException_t1869_VTable/* vtableMethods */
	, RankException_t1869_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RankException_t1869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, RankException_t1869_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RankException_t1869_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 910/* custom_attributes_cache */
	, &RankException_t1869_0_0_0/* byval_arg */
	, &RankException_t1869_1_0_0/* this_arg */
	, &RankException_t1869_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t1869)/* instance_size */
	, sizeof (RankException_t1869)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t1870_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const MethodInfo* ResolveEventArgs_t1870_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ResolveEventArgs_t1870_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ResolveEventArgs_t1870_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t1870_0_0_0;
extern const Il2CppType ResolveEventArgs_t1870_1_0_0;
extern const Il2CppType EventArgs_t1249_0_0_0;
struct ResolveEventArgs_t1870;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t1870_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1249_0_0_0/* parent */
	, ResolveEventArgs_t1870_VTable/* vtableMethods */
	, ResolveEventArgs_t1870_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventArgs_t1870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, ResolveEventArgs_t1870_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventArgs_t1870_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 911/* custom_attributes_cache */
	, &ResolveEventArgs_t1870_0_0_0/* byval_arg */
	, &ResolveEventArgs_t1870_1_0_0/* this_arg */
	, &ResolveEventArgs_t1870_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t1870)/* instance_size */
	, sizeof (ResolveEventArgs_t1870)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t1871_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1871_RuntimeMethodHandle__ctor_m10250_ParameterInfos[] = 
{
	{"v", 0, 134224490, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.IntPtr)
extern const MethodInfo RuntimeMethodHandle__ctor_m10250_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m10250/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t/* invoker_method */
	, RuntimeMethodHandle_t1871_RuntimeMethodHandle__ctor_m10250_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1871_RuntimeMethodHandle__ctor_m10251_ParameterInfos[] = 
{
	{"info", 0, 134224491, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224492, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle__ctor_m10251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m10251/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, RuntimeMethodHandle_t1871_RuntimeMethodHandle__ctor_m10251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.RuntimeMethodHandle::get_Value()
extern const MethodInfo RuntimeMethodHandle_get_Value_m10252_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&RuntimeMethodHandle_get_Value_m10252/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1871_RuntimeMethodHandle_GetObjectData_m10253_ParameterInfos[] = 
{
	{"info", 0, 134224493, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224494, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m10253_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetObjectData_m10253/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, RuntimeMethodHandle_t1871_RuntimeMethodHandle_GetObjectData_m10253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t1871_RuntimeMethodHandle_Equals_m10254_ParameterInfos[] = 
{
	{"obj", 0, 134224495, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.RuntimeMethodHandle::Equals(System.Object)
extern const MethodInfo RuntimeMethodHandle_Equals_m10254_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&RuntimeMethodHandle_Equals_m10254/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, RuntimeMethodHandle_t1871_RuntimeMethodHandle_Equals_m10254_ParameterInfos/* parameters */
	, 913/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.RuntimeMethodHandle::GetHashCode()
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m10255_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetHashCode_m10255/* method */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RuntimeMethodHandle_t1871_MethodInfos[] =
{
	&RuntimeMethodHandle__ctor_m10250_MethodInfo,
	&RuntimeMethodHandle__ctor_m10251_MethodInfo,
	&RuntimeMethodHandle_get_Value_m10252_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m10253_MethodInfo,
	&RuntimeMethodHandle_Equals_m10254_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m10255_MethodInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_get_Value_m10252_MethodInfo;
static const PropertyInfo RuntimeMethodHandle_t1871____Value_PropertyInfo = 
{
	&RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &RuntimeMethodHandle_get_Value_m10252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RuntimeMethodHandle_t1871_PropertyInfos[] =
{
	&RuntimeMethodHandle_t1871____Value_PropertyInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_Equals_m10254_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m10255_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m10253_MethodInfo;
static const Il2CppMethodReference RuntimeMethodHandle_t1871_VTable[] =
{
	&RuntimeMethodHandle_Equals_m10254_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m10255_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m10253_MethodInfo,
};
static bool RuntimeMethodHandle_t1871_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RuntimeMethodHandle_t1871_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t1871_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t1871_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t1871_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t1871_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t1871_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t1871_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, RuntimeMethodHandle_t1871_VTable/* vtableMethods */
	, RuntimeMethodHandle_t1871_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2478/* fieldStart */

};
TypeInfo RuntimeMethodHandle_t1871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, RuntimeMethodHandle_t1871_MethodInfos/* methods */
	, RuntimeMethodHandle_t1871_PropertyInfos/* properties */
	, NULL/* events */
	, &RuntimeMethodHandle_t1871_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 912/* custom_attributes_cache */
	, &RuntimeMethodHandle_t1871_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t1871_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t1871_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t1871)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t1871)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t1871 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t734_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.ctor()
extern const MethodInfo StringComparer__ctor_m10256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringComparer__ctor_m10256/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.cctor()
extern const MethodInfo StringComparer__cctor_m10257_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringComparer__cctor_m10257/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringComparer_t734_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_InvariantCultureIgnoreCase()
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m4720_MethodInfo = 
{
	"get_InvariantCultureIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_InvariantCultureIgnoreCase_m4720/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t734_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m3388_MethodInfo = 
{
	"get_OrdinalIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_OrdinalIgnoreCase_m3388/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t734_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_Compare_m10258_ParameterInfos[] = 
{
	{"x", 0, 134224496, 0, &Object_t_0_0_0},
	{"y", 1, 134224497, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.Object,System.Object)
extern const MethodInfo StringComparer_Compare_m10258_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&StringComparer_Compare_m10258/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_Compare_m10258_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_Equals_m10259_ParameterInfos[] = 
{
	{"x", 0, 134224498, 0, &Object_t_0_0_0},
	{"y", 1, 134224499, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.Object,System.Object)
extern const MethodInfo StringComparer_Equals_m10259_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&StringComparer_Equals_m10259/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_Equals_m10259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_GetHashCode_m10260_ParameterInfos[] = 
{
	{"obj", 0, 134224500, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.Object)
extern const MethodInfo StringComparer_GetHashCode_m10260_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&StringComparer_GetHashCode_m10260/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_GetHashCode_m10260_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_Compare_m11047_ParameterInfos[] = 
{
	{"x", 0, 134224501, 0, &String_t_0_0_0},
	{"y", 1, 134224502, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.String,System.String)
extern const MethodInfo StringComparer_Compare_m11047_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_Compare_m11047_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_Equals_m11048_ParameterInfos[] = 
{
	{"x", 0, 134224503, 0, &String_t_0_0_0},
	{"y", 1, 134224504, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.String,System.String)
extern const MethodInfo StringComparer_Equals_m11048_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_Equals_m11048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t734_StringComparer_GetHashCode_m11049_ParameterInfos[] = 
{
	{"obj", 0, 134224505, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.String)
extern const MethodInfo StringComparer_GetHashCode_m11049_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &StringComparer_t734_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, StringComparer_t734_StringComparer_GetHashCode_m11049_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringComparer_t734_MethodInfos[] =
{
	&StringComparer__ctor_m10256_MethodInfo,
	&StringComparer__cctor_m10257_MethodInfo,
	&StringComparer_get_InvariantCultureIgnoreCase_m4720_MethodInfo,
	&StringComparer_get_OrdinalIgnoreCase_m3388_MethodInfo,
	&StringComparer_Compare_m10258_MethodInfo,
	&StringComparer_Equals_m10259_MethodInfo,
	&StringComparer_GetHashCode_m10260_MethodInfo,
	&StringComparer_Compare_m11047_MethodInfo,
	&StringComparer_Equals_m11048_MethodInfo,
	&StringComparer_GetHashCode_m11049_MethodInfo,
	NULL
};
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m4720_MethodInfo;
static const PropertyInfo StringComparer_t734____InvariantCultureIgnoreCase_PropertyInfo = 
{
	&StringComparer_t734_il2cpp_TypeInfo/* parent */
	, "InvariantCultureIgnoreCase"/* name */
	, &StringComparer_get_InvariantCultureIgnoreCase_m4720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m3388_MethodInfo;
static const PropertyInfo StringComparer_t734____OrdinalIgnoreCase_PropertyInfo = 
{
	&StringComparer_t734_il2cpp_TypeInfo/* parent */
	, "OrdinalIgnoreCase"/* name */
	, &StringComparer_get_OrdinalIgnoreCase_m3388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* StringComparer_t734_PropertyInfos[] =
{
	&StringComparer_t734____InvariantCultureIgnoreCase_PropertyInfo,
	&StringComparer_t734____OrdinalIgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo StringComparer_Compare_m11047_MethodInfo;
extern const MethodInfo StringComparer_Equals_m11048_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m11049_MethodInfo;
extern const MethodInfo StringComparer_Compare_m10258_MethodInfo;
extern const MethodInfo StringComparer_Equals_m10259_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m10260_MethodInfo;
static const Il2CppMethodReference StringComparer_t734_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&StringComparer_Compare_m11047_MethodInfo,
	&StringComparer_Equals_m11048_MethodInfo,
	&StringComparer_GetHashCode_m11049_MethodInfo,
	&StringComparer_Compare_m10258_MethodInfo,
	&StringComparer_Equals_m10259_MethodInfo,
	&StringComparer_GetHashCode_m10260_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool StringComparer_t734_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_1_t2412_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2413_0_0_0;
extern const Il2CppType IComparer_t980_0_0_0;
extern const Il2CppType IEqualityComparer_t987_0_0_0;
static const Il2CppType* StringComparer_t734_InterfacesTypeInfos[] = 
{
	&IComparer_1_t2412_0_0_0,
	&IEqualityComparer_1_t2413_0_0_0,
	&IComparer_t980_0_0_0,
	&IEqualityComparer_t987_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t734_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2412_0_0_0, 4},
	{ &IEqualityComparer_1_t2413_0_0_0, 5},
	{ &IComparer_t980_0_0_0, 7},
	{ &IEqualityComparer_t987_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t734_1_0_0;
struct StringComparer_t734;
const Il2CppTypeDefinitionMetadata StringComparer_t734_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t734_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t734_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t734_VTable/* vtableMethods */
	, StringComparer_t734_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2479/* fieldStart */

};
TypeInfo StringComparer_t734_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, StringComparer_t734_MethodInfos/* methods */
	, StringComparer_t734_PropertyInfos/* properties */
	, NULL/* events */
	, &StringComparer_t734_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 914/* custom_attributes_cache */
	, &StringComparer_t734_0_0_0/* byval_arg */
	, &StringComparer_t734_1_0_0/* this_arg */
	, &StringComparer_t734_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t734)/* instance_size */
	, sizeof (StringComparer_t734)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t734_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t1872_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CultureAwareComparer_t1872_CultureAwareComparer__ctor_m10261_ParameterInfos[] = 
{
	{"ci", 0, 134224506, 0, &CultureInfo_t750_0_0_0},
	{"ignore_case", 1, 134224507, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CultureAwareComparer::.ctor(System.Globalization.CultureInfo,System.Boolean)
extern const MethodInfo CultureAwareComparer__ctor_m10261_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CultureAwareComparer__ctor_m10261/* method */
	, &CultureAwareComparer_t1872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, CultureAwareComparer_t1872_CultureAwareComparer__ctor_m10261_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t1872_CultureAwareComparer_Compare_m10262_ParameterInfos[] = 
{
	{"x", 0, 134224508, 0, &String_t_0_0_0},
	{"y", 1, 134224509, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::Compare(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Compare_m10262_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&CultureAwareComparer_Compare_m10262/* method */
	, &CultureAwareComparer_t1872_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t1872_CultureAwareComparer_Compare_m10262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t1872_CultureAwareComparer_Equals_m10263_ParameterInfos[] = 
{
	{"x", 0, 134224510, 0, &String_t_0_0_0},
	{"y", 1, 134224511, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CultureAwareComparer::Equals(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Equals_m10263_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&CultureAwareComparer_Equals_m10263/* method */
	, &CultureAwareComparer_t1872_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t1872_CultureAwareComparer_Equals_m10263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t1872_CultureAwareComparer_GetHashCode_m10264_ParameterInfos[] = 
{
	{"s", 0, 134224512, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::GetHashCode(System.String)
extern const MethodInfo CultureAwareComparer_GetHashCode_m10264_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&CultureAwareComparer_GetHashCode_m10264/* method */
	, &CultureAwareComparer_t1872_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, CultureAwareComparer_t1872_CultureAwareComparer_GetHashCode_m10264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CultureAwareComparer_t1872_MethodInfos[] =
{
	&CultureAwareComparer__ctor_m10261_MethodInfo,
	&CultureAwareComparer_Compare_m10262_MethodInfo,
	&CultureAwareComparer_Equals_m10263_MethodInfo,
	&CultureAwareComparer_GetHashCode_m10264_MethodInfo,
	NULL
};
extern const MethodInfo CultureAwareComparer_Compare_m10262_MethodInfo;
extern const MethodInfo CultureAwareComparer_Equals_m10263_MethodInfo;
extern const MethodInfo CultureAwareComparer_GetHashCode_m10264_MethodInfo;
static const Il2CppMethodReference CultureAwareComparer_t1872_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CultureAwareComparer_Compare_m10262_MethodInfo,
	&CultureAwareComparer_Equals_m10263_MethodInfo,
	&CultureAwareComparer_GetHashCode_m10264_MethodInfo,
	&StringComparer_Compare_m10258_MethodInfo,
	&StringComparer_Equals_m10259_MethodInfo,
	&StringComparer_GetHashCode_m10260_MethodInfo,
	&CultureAwareComparer_Compare_m10262_MethodInfo,
	&CultureAwareComparer_Equals_m10263_MethodInfo,
	&CultureAwareComparer_GetHashCode_m10264_MethodInfo,
};
static bool CultureAwareComparer_t1872_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t1872_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2412_0_0_0, 4},
	{ &IEqualityComparer_1_t2413_0_0_0, 5},
	{ &IComparer_t980_0_0_0, 7},
	{ &IEqualityComparer_t987_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t1872_0_0_0;
extern const Il2CppType CultureAwareComparer_t1872_1_0_0;
struct CultureAwareComparer_t1872;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t1872_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t1872_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t734_0_0_0/* parent */
	, CultureAwareComparer_t1872_VTable/* vtableMethods */
	, CultureAwareComparer_t1872_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2483/* fieldStart */

};
TypeInfo CultureAwareComparer_t1872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, CultureAwareComparer_t1872_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CultureAwareComparer_t1872_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t1872_0_0_0/* byval_arg */
	, &CultureAwareComparer_t1872_1_0_0/* this_arg */
	, &CultureAwareComparer_t1872_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t1872)/* instance_size */
	, sizeof (CultureAwareComparer_t1872)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t1873_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo OrdinalComparer_t1873_OrdinalComparer__ctor_m10265_ParameterInfos[] = 
{
	{"ignoreCase", 0, 134224513, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OrdinalComparer::.ctor(System.Boolean)
extern const MethodInfo OrdinalComparer__ctor_m10265_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrdinalComparer__ctor_m10265/* method */
	, &OrdinalComparer_t1873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, OrdinalComparer_t1873_OrdinalComparer__ctor_m10265_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t1873_OrdinalComparer_Compare_m10266_ParameterInfos[] = 
{
	{"x", 0, 134224514, 0, &String_t_0_0_0},
	{"y", 1, 134224515, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::Compare(System.String,System.String)
extern const MethodInfo OrdinalComparer_Compare_m10266_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&OrdinalComparer_Compare_m10266/* method */
	, &OrdinalComparer_t1873_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t1873_OrdinalComparer_Compare_m10266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t1873_OrdinalComparer_Equals_m10267_ParameterInfos[] = 
{
	{"x", 0, 134224516, 0, &String_t_0_0_0},
	{"y", 1, 134224517, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.OrdinalComparer::Equals(System.String,System.String)
extern const MethodInfo OrdinalComparer_Equals_m10267_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&OrdinalComparer_Equals_m10267/* method */
	, &OrdinalComparer_t1873_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t1873_OrdinalComparer_Equals_m10267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t1873_OrdinalComparer_GetHashCode_m10268_ParameterInfos[] = 
{
	{"s", 0, 134224518, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::GetHashCode(System.String)
extern const MethodInfo OrdinalComparer_GetHashCode_m10268_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&OrdinalComparer_GetHashCode_m10268/* method */
	, &OrdinalComparer_t1873_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, OrdinalComparer_t1873_OrdinalComparer_GetHashCode_m10268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrdinalComparer_t1873_MethodInfos[] =
{
	&OrdinalComparer__ctor_m10265_MethodInfo,
	&OrdinalComparer_Compare_m10266_MethodInfo,
	&OrdinalComparer_Equals_m10267_MethodInfo,
	&OrdinalComparer_GetHashCode_m10268_MethodInfo,
	NULL
};
extern const MethodInfo OrdinalComparer_Compare_m10266_MethodInfo;
extern const MethodInfo OrdinalComparer_Equals_m10267_MethodInfo;
extern const MethodInfo OrdinalComparer_GetHashCode_m10268_MethodInfo;
static const Il2CppMethodReference OrdinalComparer_t1873_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&OrdinalComparer_Compare_m10266_MethodInfo,
	&OrdinalComparer_Equals_m10267_MethodInfo,
	&OrdinalComparer_GetHashCode_m10268_MethodInfo,
	&StringComparer_Compare_m10258_MethodInfo,
	&StringComparer_Equals_m10259_MethodInfo,
	&StringComparer_GetHashCode_m10260_MethodInfo,
	&OrdinalComparer_Compare_m10266_MethodInfo,
	&OrdinalComparer_Equals_m10267_MethodInfo,
	&OrdinalComparer_GetHashCode_m10268_MethodInfo,
};
static bool OrdinalComparer_t1873_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t1873_InterfacesOffsets[] = 
{
	{ &IComparer_1_t2412_0_0_0, 4},
	{ &IEqualityComparer_1_t2413_0_0_0, 5},
	{ &IComparer_t980_0_0_0, 7},
	{ &IEqualityComparer_t987_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t1873_0_0_0;
extern const Il2CppType OrdinalComparer_t1873_1_0_0;
struct OrdinalComparer_t1873;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t1873_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t1873_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t734_0_0_0/* parent */
	, OrdinalComparer_t1873_VTable/* vtableMethods */
	, OrdinalComparer_t1873_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2485/* fieldStart */

};
TypeInfo OrdinalComparer_t1873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, OrdinalComparer_t1873_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrdinalComparer_t1873_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t1873_0_0_0/* byval_arg */
	, &OrdinalComparer_t1873_1_0_0/* this_arg */
	, &OrdinalComparer_t1873_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t1873)/* instance_size */
	, sizeof (OrdinalComparer_t1873)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t1874_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const MethodInfo* StringComparison_t1874_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringComparison_t1874_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool StringComparison_t1874_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringComparison_t1874_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t1874_0_0_0;
extern const Il2CppType StringComparison_t1874_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t1874_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t1874_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, StringComparison_t1874_VTable/* vtableMethods */
	, StringComparison_t1874_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2486/* fieldStart */

};
TypeInfo StringComparison_t1874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, StringComparison_t1874_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 915/* custom_attributes_cache */
	, &StringComparison_t1874_0_0_0/* byval_arg */
	, &StringComparison_t1874_1_0_0/* this_arg */
	, &StringComparison_t1874_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t1874)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t1874)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t1875_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const MethodInfo* StringSplitOptions_t1875_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringSplitOptions_t1875_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool StringSplitOptions_t1875_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t1875_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t1875_0_0_0;
extern const Il2CppType StringSplitOptions_t1875_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t1875_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t1875_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, StringSplitOptions_t1875_VTable/* vtableMethods */
	, StringSplitOptions_t1875_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2493/* fieldStart */

};
TypeInfo StringSplitOptions_t1875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, StringSplitOptions_t1875_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 916/* custom_attributes_cache */
	, &StringSplitOptions_t1875_0_0_0/* byval_arg */
	, &StringSplitOptions_t1875_1_0_0/* this_arg */
	, &StringSplitOptions_t1875_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t1875)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t1875)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t1181_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor()
extern const MethodInfo SystemException__ctor_m10269_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m10269/* method */
	, &SystemException_t1181_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SystemException_t1181_SystemException__ctor_m4819_ParameterInfos[] = 
{
	{"message", 0, 134224519, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String)
extern const MethodInfo SystemException__ctor_m4819_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m4819/* method */
	, &SystemException_t1181_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SystemException_t1181_SystemException__ctor_m4819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo SystemException_t1181_SystemException__ctor_m10270_ParameterInfos[] = 
{
	{"info", 0, 134224520, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224521, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SystemException__ctor_m10270_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m10270/* method */
	, &SystemException_t1181_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, SystemException_t1181_SystemException__ctor_m10270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t42_0_0_0;
static const ParameterInfo SystemException_t1181_SystemException__ctor_m10271_ParameterInfos[] = 
{
	{"message", 0, 134224522, 0, &String_t_0_0_0},
	{"innerException", 1, 134224523, 0, &Exception_t42_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern const MethodInfo SystemException__ctor_m10271_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m10271/* method */
	, &SystemException_t1181_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, SystemException_t1181_SystemException__ctor_m10271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SystemException_t1181_MethodInfos[] =
{
	&SystemException__ctor_m10269_MethodInfo,
	&SystemException__ctor_m4819_MethodInfo,
	&SystemException__ctor_m10270_MethodInfo,
	&SystemException__ctor_m10271_MethodInfo,
	NULL
};
static const Il2CppMethodReference SystemException_t1181_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool SystemException_t1181_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SystemException_t1181_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t1181_1_0_0;
struct SystemException_t1181;
const Il2CppTypeDefinitionMetadata SystemException_t1181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t1181_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t42_0_0_0/* parent */
	, SystemException_t1181_VTable/* vtableMethods */
	, SystemException_t1181_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SystemException_t1181_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, SystemException_t1181_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SystemException_t1181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 917/* custom_attributes_cache */
	, &SystemException_t1181_0_0_0/* byval_arg */
	, &SystemException_t1181_1_0_0/* this_arg */
	, &SystemException_t1181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t1181)/* instance_size */
	, sizeof (SystemException_t1181)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t1876_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ThreadStaticAttribute::.ctor()
extern const MethodInfo ThreadStaticAttribute__ctor_m10272_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ThreadStaticAttribute__ctor_m10272/* method */
	, &ThreadStaticAttribute_t1876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadStaticAttribute_t1876_MethodInfos[] =
{
	&ThreadStaticAttribute__ctor_m10272_MethodInfo,
	NULL
};
static const Il2CppMethodReference ThreadStaticAttribute_t1876_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ThreadStaticAttribute_t1876_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t1876_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t1876_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t1876_1_0_0;
struct ThreadStaticAttribute_t1876;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t1876_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t1876_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ThreadStaticAttribute_t1876_VTable/* vtableMethods */
	, ThreadStaticAttribute_t1876_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadStaticAttribute_t1876_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, ThreadStaticAttribute_t1876_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadStaticAttribute_t1876_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 918/* custom_attributes_cache */
	, &ThreadStaticAttribute_t1876_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t1876_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t1876_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t1876)/* instance_size */
	, sizeof (ThreadStaticAttribute_t1876)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t1051_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan__ctor_m10273_ParameterInfos[] = 
{
	{"ticks", 0, 134224524, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int64)
extern const MethodInfo TimeSpan__ctor_m10273_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m10273/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753/* invoker_method */
	, TimeSpan_t1051_TimeSpan__ctor_m10273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan__ctor_m10274_ParameterInfos[] = 
{
	{"hours", 0, 134224525, 0, &Int32_t54_0_0_0},
	{"minutes", 1, 134224526, 0, &Int32_t54_0_0_0},
	{"seconds", 2, 134224527, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m10274_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m10274/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, TimeSpan_t1051_TimeSpan__ctor_m10274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan__ctor_m10275_ParameterInfos[] = 
{
	{"days", 0, 134224528, 0, &Int32_t54_0_0_0},
	{"hours", 1, 134224529, 0, &Int32_t54_0_0_0},
	{"minutes", 2, 134224530, 0, &Int32_t54_0_0_0},
	{"seconds", 3, 134224531, 0, &Int32_t54_0_0_0},
	{"milliseconds", 4, 134224532, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m10275_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m10275/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, TimeSpan_t1051_TimeSpan__ctor_m10275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.cctor()
extern const MethodInfo TimeSpan__cctor_m10276_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeSpan__cctor_m10276/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_CalculateTicks_m10277_ParameterInfos[] = 
{
	{"days", 0, 134224533, 0, &Int32_t54_0_0_0},
	{"hours", 1, 134224534, 0, &Int32_t54_0_0_0},
	{"minutes", 2, 134224535, 0, &Int32_t54_0_0_0},
	{"seconds", 3, 134224536, 0, &Int32_t54_0_0_0},
	{"milliseconds", 4, 134224537, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int64_t753_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::CalculateTicks(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan_CalculateTicks_m10277_MethodInfo = 
{
	"CalculateTicks"/* name */
	, (methodPointerType)&TimeSpan_CalculateTicks_m10277/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, TimeSpan_t1051_TimeSpan_CalculateTicks_m10277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Days()
extern const MethodInfo TimeSpan_get_Days_m10278_MethodInfo = 
{
	"get_Days"/* name */
	, (methodPointerType)&TimeSpan_get_Days_m10278/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Hours()
extern const MethodInfo TimeSpan_get_Hours_m10279_MethodInfo = 
{
	"get_Hours"/* name */
	, (methodPointerType)&TimeSpan_get_Hours_m10279/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Milliseconds()
extern const MethodInfo TimeSpan_get_Milliseconds_m10280_MethodInfo = 
{
	"get_Milliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_Milliseconds_m10280/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Minutes()
extern const MethodInfo TimeSpan_get_Minutes_m10281_MethodInfo = 
{
	"get_Minutes"/* name */
	, (methodPointerType)&TimeSpan_get_Minutes_m10281/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Seconds()
extern const MethodInfo TimeSpan_get_Seconds_m10282_MethodInfo = 
{
	"get_Seconds"/* name */
	, (methodPointerType)&TimeSpan_get_Seconds_m10282/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::get_Ticks()
extern const MethodInfo TimeSpan_get_Ticks_m10283_MethodInfo = 
{
	"get_Ticks"/* name */
	, (methodPointerType)&TimeSpan_get_Ticks_m10283/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalDays()
extern const MethodInfo TimeSpan_get_TotalDays_m10284_MethodInfo = 
{
	"get_TotalDays"/* name */
	, (methodPointerType)&TimeSpan_get_TotalDays_m10284/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalHours()
extern const MethodInfo TimeSpan_get_TotalHours_m10285_MethodInfo = 
{
	"get_TotalHours"/* name */
	, (methodPointerType)&TimeSpan_get_TotalHours_m10285/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m10286_MethodInfo = 
{
	"get_TotalMilliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMilliseconds_m10286/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMinutes()
extern const MethodInfo TimeSpan_get_TotalMinutes_m10287_MethodInfo = 
{
	"get_TotalMinutes"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMinutes_m10287/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalSeconds()
extern const MethodInfo TimeSpan_get_TotalSeconds_m10288_MethodInfo = 
{
	"get_TotalSeconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalSeconds_m10288/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_Add_m10289_ParameterInfos[] = 
{
	{"ts", 0, 134224538, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Add(System.TimeSpan)
extern const MethodInfo TimeSpan_Add_m10289_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&TimeSpan_Add_m10289/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_Add_m10289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_Compare_m10290_ParameterInfos[] = 
{
	{"t1", 0, 134224539, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224540, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::Compare(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_Compare_m10290_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&TimeSpan_Compare_m10290/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_Compare_m10290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_CompareTo_m10291_ParameterInfos[] = 
{
	{"value", 0, 134224541, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.Object)
extern const MethodInfo TimeSpan_CompareTo_m10291_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m10291/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, TimeSpan_t1051_TimeSpan_CompareTo_m10291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_CompareTo_m10292_ParameterInfos[] = 
{
	{"value", 0, 134224542, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern const MethodInfo TimeSpan_CompareTo_m10292_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m10292/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_CompareTo_m10292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_Equals_m10293_ParameterInfos[] = 
{
	{"obj", 0, 134224543, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern const MethodInfo TimeSpan_Equals_m10293_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m10293/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_Equals_m10293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Duration()
extern const MethodInfo TimeSpan_Duration_m10294_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TimeSpan_Duration_m10294/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_Equals_m10295_ParameterInfos[] = 
{
	{"value", 0, 134224544, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.Object)
extern const MethodInfo TimeSpan_Equals_m10295_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m10295/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TimeSpan_t1051_TimeSpan_Equals_m10295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_FromMinutes_m10296_ParameterInfos[] = 
{
	{"value", 0, 134224545, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromMinutes(System.Double)
extern const MethodInfo TimeSpan_FromMinutes_m10296_MethodInfo = 
{
	"FromMinutes"/* name */
	, (methodPointerType)&TimeSpan_FromMinutes_m10296/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_Double_t752/* invoker_method */
	, TimeSpan_t1051_TimeSpan_FromMinutes_m10296_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_From_m10297_ParameterInfos[] = 
{
	{"value", 0, 134224546, 0, &Double_t752_0_0_0},
	{"tickMultiplicator", 1, 134224547, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_Double_t752_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::From(System.Double,System.Int64)
extern const MethodInfo TimeSpan_From_m10297_MethodInfo = 
{
	"From"/* name */
	, (methodPointerType)&TimeSpan_From_m10297/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_Double_t752_Int64_t753/* invoker_method */
	, TimeSpan_t1051_TimeSpan_From_m10297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::GetHashCode()
extern const MethodInfo TimeSpan_GetHashCode_m10298_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TimeSpan_GetHashCode_m10298/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Negate()
extern const MethodInfo TimeSpan_Negate_m10299_MethodInfo = 
{
	"Negate"/* name */
	, (methodPointerType)&TimeSpan_Negate_m10299/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_Subtract_m10300_ParameterInfos[] = 
{
	{"ts", 0, 134224548, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Subtract(System.TimeSpan)
extern const MethodInfo TimeSpan_Subtract_m10300_MethodInfo = 
{
	"Subtract"/* name */
	, (methodPointerType)&TimeSpan_Subtract_m10300/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_Subtract_m10300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TimeSpan::ToString()
extern const MethodInfo TimeSpan_ToString_m10301_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TimeSpan_ToString_m10301/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_Addition_m10302_ParameterInfos[] = 
{
	{"t1", 0, 134224549, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224550, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Addition(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Addition_m10302_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&TimeSpan_op_Addition_m10302/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_Addition_m10302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_Equality_m10303_ParameterInfos[] = 
{
	{"t1", 0, 134224551, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224552, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Equality_m10303_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TimeSpan_op_Equality_m10303/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_Equality_m10303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_GreaterThan_m10304_ParameterInfos[] = 
{
	{"t1", 0, 134224553, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224554, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThan_m10304_MethodInfo = 
{
	"op_GreaterThan"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThan_m10304/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_GreaterThan_m10304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_GreaterThanOrEqual_m10305_ParameterInfos[] = 
{
	{"t1", 0, 134224555, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224556, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThanOrEqual_m10305_MethodInfo = 
{
	"op_GreaterThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThanOrEqual_m10305/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_GreaterThanOrEqual_m10305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_Inequality_m10306_ParameterInfos[] = 
{
	{"t1", 0, 134224557, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224558, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Inequality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Inequality_m10306_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&TimeSpan_op_Inequality_m10306/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_Inequality_m10306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_LessThan_m10307_ParameterInfos[] = 
{
	{"t1", 0, 134224559, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224560, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThan_m10307_MethodInfo = 
{
	"op_LessThan"/* name */
	, (methodPointerType)&TimeSpan_op_LessThan_m10307/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_LessThan_m10307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_LessThanOrEqual_m10308_ParameterInfos[] = 
{
	{"t1", 0, 134224561, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224562, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThanOrEqual_m10308_MethodInfo = 
{
	"op_LessThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_LessThanOrEqual_m10308/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_LessThanOrEqual_m10308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t1051_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeSpan_t1051_TimeSpan_op_Subtraction_m10309_ParameterInfos[] = 
{
	{"t1", 0, 134224563, 0, &TimeSpan_t1051_0_0_0},
	{"t2", 1, 134224564, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Subtraction(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Subtraction_m10309_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&TimeSpan_op_Subtraction_m10309/* method */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051_TimeSpan_t1051/* invoker_method */
	, TimeSpan_t1051_TimeSpan_op_Subtraction_m10309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeSpan_t1051_MethodInfos[] =
{
	&TimeSpan__ctor_m10273_MethodInfo,
	&TimeSpan__ctor_m10274_MethodInfo,
	&TimeSpan__ctor_m10275_MethodInfo,
	&TimeSpan__cctor_m10276_MethodInfo,
	&TimeSpan_CalculateTicks_m10277_MethodInfo,
	&TimeSpan_get_Days_m10278_MethodInfo,
	&TimeSpan_get_Hours_m10279_MethodInfo,
	&TimeSpan_get_Milliseconds_m10280_MethodInfo,
	&TimeSpan_get_Minutes_m10281_MethodInfo,
	&TimeSpan_get_Seconds_m10282_MethodInfo,
	&TimeSpan_get_Ticks_m10283_MethodInfo,
	&TimeSpan_get_TotalDays_m10284_MethodInfo,
	&TimeSpan_get_TotalHours_m10285_MethodInfo,
	&TimeSpan_get_TotalMilliseconds_m10286_MethodInfo,
	&TimeSpan_get_TotalMinutes_m10287_MethodInfo,
	&TimeSpan_get_TotalSeconds_m10288_MethodInfo,
	&TimeSpan_Add_m10289_MethodInfo,
	&TimeSpan_Compare_m10290_MethodInfo,
	&TimeSpan_CompareTo_m10291_MethodInfo,
	&TimeSpan_CompareTo_m10292_MethodInfo,
	&TimeSpan_Equals_m10293_MethodInfo,
	&TimeSpan_Duration_m10294_MethodInfo,
	&TimeSpan_Equals_m10295_MethodInfo,
	&TimeSpan_FromMinutes_m10296_MethodInfo,
	&TimeSpan_From_m10297_MethodInfo,
	&TimeSpan_GetHashCode_m10298_MethodInfo,
	&TimeSpan_Negate_m10299_MethodInfo,
	&TimeSpan_Subtract_m10300_MethodInfo,
	&TimeSpan_ToString_m10301_MethodInfo,
	&TimeSpan_op_Addition_m10302_MethodInfo,
	&TimeSpan_op_Equality_m10303_MethodInfo,
	&TimeSpan_op_GreaterThan_m10304_MethodInfo,
	&TimeSpan_op_GreaterThanOrEqual_m10305_MethodInfo,
	&TimeSpan_op_Inequality_m10306_MethodInfo,
	&TimeSpan_op_LessThan_m10307_MethodInfo,
	&TimeSpan_op_LessThanOrEqual_m10308_MethodInfo,
	&TimeSpan_op_Subtraction_m10309_MethodInfo,
	NULL
};
extern const MethodInfo TimeSpan_get_Days_m10278_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Days_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Days"/* name */
	, &TimeSpan_get_Days_m10278_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Hours_m10279_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Hours_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Hours"/* name */
	, &TimeSpan_get_Hours_m10279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Milliseconds_m10280_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Milliseconds_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Milliseconds"/* name */
	, &TimeSpan_get_Milliseconds_m10280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Minutes_m10281_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Minutes_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Minutes"/* name */
	, &TimeSpan_get_Minutes_m10281_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Seconds_m10282_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Seconds_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Seconds"/* name */
	, &TimeSpan_get_Seconds_m10282_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Ticks_m10283_MethodInfo;
static const PropertyInfo TimeSpan_t1051____Ticks_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "Ticks"/* name */
	, &TimeSpan_get_Ticks_m10283_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalDays_m10284_MethodInfo;
static const PropertyInfo TimeSpan_t1051____TotalDays_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "TotalDays"/* name */
	, &TimeSpan_get_TotalDays_m10284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalHours_m10285_MethodInfo;
static const PropertyInfo TimeSpan_t1051____TotalHours_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "TotalHours"/* name */
	, &TimeSpan_get_TotalHours_m10285_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m10286_MethodInfo;
static const PropertyInfo TimeSpan_t1051____TotalMilliseconds_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "TotalMilliseconds"/* name */
	, &TimeSpan_get_TotalMilliseconds_m10286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMinutes_m10287_MethodInfo;
static const PropertyInfo TimeSpan_t1051____TotalMinutes_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "TotalMinutes"/* name */
	, &TimeSpan_get_TotalMinutes_m10287_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalSeconds_m10288_MethodInfo;
static const PropertyInfo TimeSpan_t1051____TotalSeconds_PropertyInfo = 
{
	&TimeSpan_t1051_il2cpp_TypeInfo/* parent */
	, "TotalSeconds"/* name */
	, &TimeSpan_get_TotalSeconds_m10288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeSpan_t1051_PropertyInfos[] =
{
	&TimeSpan_t1051____Days_PropertyInfo,
	&TimeSpan_t1051____Hours_PropertyInfo,
	&TimeSpan_t1051____Milliseconds_PropertyInfo,
	&TimeSpan_t1051____Minutes_PropertyInfo,
	&TimeSpan_t1051____Seconds_PropertyInfo,
	&TimeSpan_t1051____Ticks_PropertyInfo,
	&TimeSpan_t1051____TotalDays_PropertyInfo,
	&TimeSpan_t1051____TotalHours_PropertyInfo,
	&TimeSpan_t1051____TotalMilliseconds_PropertyInfo,
	&TimeSpan_t1051____TotalMinutes_PropertyInfo,
	&TimeSpan_t1051____TotalSeconds_PropertyInfo,
	NULL
};
extern const MethodInfo TimeSpan_Equals_m10295_MethodInfo;
extern const MethodInfo TimeSpan_GetHashCode_m10298_MethodInfo;
extern const MethodInfo TimeSpan_ToString_m10301_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m10291_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m10292_MethodInfo;
extern const MethodInfo TimeSpan_Equals_m10293_MethodInfo;
static const Il2CppMethodReference TimeSpan_t1051_VTable[] =
{
	&TimeSpan_Equals_m10295_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&TimeSpan_GetHashCode_m10298_MethodInfo,
	&TimeSpan_ToString_m10301_MethodInfo,
	&TimeSpan_CompareTo_m10291_MethodInfo,
	&TimeSpan_CompareTo_m10292_MethodInfo,
	&TimeSpan_Equals_m10293_MethodInfo,
};
static bool TimeSpan_t1051_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t2414_0_0_0;
extern const Il2CppType IEquatable_1_t2415_0_0_0;
static const Il2CppType* TimeSpan_t1051_InterfacesTypeInfos[] = 
{
	&IComparable_t76_0_0_0,
	&IComparable_1_t2414_0_0_0,
	&IEquatable_1_t2415_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t1051_InterfacesOffsets[] = 
{
	{ &IComparable_t76_0_0_0, 4},
	{ &IComparable_1_t2414_0_0_0, 5},
	{ &IEquatable_1_t2415_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t1051_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t1051_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t1051_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t1051_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, TimeSpan_t1051_VTable/* vtableMethods */
	, TimeSpan_t1051_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2496/* fieldStart */

};
TypeInfo TimeSpan_t1051_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, TimeSpan_t1051_MethodInfos/* methods */
	, TimeSpan_t1051_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeSpan_t1051_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 919/* custom_attributes_cache */
	, &TimeSpan_t1051_0_0_0/* byval_arg */
	, &TimeSpan_t1051_1_0_0/* this_arg */
	, &TimeSpan_t1051_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t1051)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t1051)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t1051 )/* native_size */
	, sizeof(TimeSpan_t1051_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 37/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t1877_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.ctor()
extern const MethodInfo TimeZone__ctor_m10310_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeZone__ctor_m10310/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.cctor()
extern const MethodInfo TimeZone__cctor_m10311_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeZone__cctor_m10311/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeZone_t1877_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeZone System.TimeZone::get_CurrentTimeZone()
extern const MethodInfo TimeZone_get_CurrentTimeZone_m10312_MethodInfo = 
{
	"get_CurrentTimeZone"/* name */
	, (methodPointerType)&TimeZone_get_CurrentTimeZone_m10312/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &TimeZone_t1877_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_GetDaylightChanges_m11050_ParameterInfos[] = 
{
	{"year", 0, 134224565, 0, &Int32_t54_0_0_0},
};
extern const Il2CppType DaylightTime_t1498_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo TimeZone_GetDaylightChanges_m11050_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, NULL/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1498_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, TimeZone_t1877_TimeZone_GetDaylightChanges_m11050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_GetUtcOffset_m11051_ParameterInfos[] = 
{
	{"time", 0, 134224566, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo TimeZone_GetUtcOffset_m11051_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, NULL/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_DateTime_t508/* invoker_method */
	, TimeZone_t1877_TimeZone_GetUtcOffset_m11051_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_IsDaylightSavingTime_m10313_ParameterInfos[] = 
{
	{"time", 0, 134224567, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m10313_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m10313/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_DateTime_t508/* invoker_method */
	, TimeZone_t1877_TimeZone_IsDaylightSavingTime_m10313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
extern const Il2CppType DaylightTime_t1498_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_IsDaylightSavingTime_m10314_ParameterInfos[] = 
{
	{"time", 0, 134224568, 0, &DateTime_t508_0_0_0},
	{"daylightTimes", 1, 134224569, 0, &DaylightTime_t1498_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_DateTime_t508_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime,System.Globalization.DaylightTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m10314_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m10314/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_DateTime_t508_Object_t/* invoker_method */
	, TimeZone_t1877_TimeZone_IsDaylightSavingTime_m10314_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_ToLocalTime_m10315_ParameterInfos[] = 
{
	{"time", 0, 134224570, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToLocalTime(System.DateTime)
extern const MethodInfo TimeZone_ToLocalTime_m10315_MethodInfo = 
{
	"ToLocalTime"/* name */
	, (methodPointerType)&TimeZone_ToLocalTime_m10315/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t508_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t508_DateTime_t508/* invoker_method */
	, TimeZone_t1877_TimeZone_ToLocalTime_m10315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_ToUniversalTime_m10316_ParameterInfos[] = 
{
	{"time", 0, 134224571, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToUniversalTime(System.DateTime)
extern const MethodInfo TimeZone_ToUniversalTime_m10316_MethodInfo = 
{
	"ToUniversalTime"/* name */
	, (methodPointerType)&TimeZone_ToUniversalTime_m10316/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t508_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t508_DateTime_t508/* invoker_method */
	, TimeZone_t1877_TimeZone_ToUniversalTime_m10316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_GetLocalTimeDiff_m10317_ParameterInfos[] = 
{
	{"time", 0, 134224572, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m10317_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m10317/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_DateTime_t508/* invoker_method */
	, TimeZone_t1877_TimeZone_GetLocalTimeDiff_m10317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
extern const Il2CppType TimeSpan_t1051_0_0_0;
static const ParameterInfo TimeZone_t1877_TimeZone_GetLocalTimeDiff_m10318_ParameterInfos[] = 
{
	{"time", 0, 134224573, 0, &DateTime_t508_0_0_0},
	{"utc_offset", 1, 134224574, 0, &TimeSpan_t1051_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime,System.TimeSpan)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m10318_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m10318/* method */
	, &TimeZone_t1877_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_DateTime_t508_TimeSpan_t1051/* invoker_method */
	, TimeZone_t1877_TimeZone_GetLocalTimeDiff_m10318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeZone_t1877_MethodInfos[] =
{
	&TimeZone__ctor_m10310_MethodInfo,
	&TimeZone__cctor_m10311_MethodInfo,
	&TimeZone_get_CurrentTimeZone_m10312_MethodInfo,
	&TimeZone_GetDaylightChanges_m11050_MethodInfo,
	&TimeZone_GetUtcOffset_m11051_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m10313_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m10314_MethodInfo,
	&TimeZone_ToLocalTime_m10315_MethodInfo,
	&TimeZone_ToUniversalTime_m10316_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m10317_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m10318_MethodInfo,
	NULL
};
extern const MethodInfo TimeZone_get_CurrentTimeZone_m10312_MethodInfo;
static const PropertyInfo TimeZone_t1877____CurrentTimeZone_PropertyInfo = 
{
	&TimeZone_t1877_il2cpp_TypeInfo/* parent */
	, "CurrentTimeZone"/* name */
	, &TimeZone_get_CurrentTimeZone_m10312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeZone_t1877_PropertyInfos[] =
{
	&TimeZone_t1877____CurrentTimeZone_PropertyInfo,
	NULL
};
extern const MethodInfo TimeZone_IsDaylightSavingTime_m10313_MethodInfo;
extern const MethodInfo TimeZone_ToLocalTime_m10315_MethodInfo;
extern const MethodInfo TimeZone_ToUniversalTime_m10316_MethodInfo;
static const Il2CppMethodReference TimeZone_t1877_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
	&TimeZone_IsDaylightSavingTime_m10313_MethodInfo,
	&TimeZone_ToLocalTime_m10315_MethodInfo,
	&TimeZone_ToUniversalTime_m10316_MethodInfo,
};
static bool TimeZone_t1877_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t1877_1_0_0;
struct TimeZone_t1877;
const Il2CppTypeDefinitionMetadata TimeZone_t1877_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t1877_VTable/* vtableMethods */
	, TimeZone_t1877_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2500/* fieldStart */

};
TypeInfo TimeZone_t1877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, TimeZone_t1877_MethodInfos/* methods */
	, TimeZone_t1877_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeZone_t1877_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 920/* custom_attributes_cache */
	, &TimeZone_t1877_0_0_0/* byval_arg */
	, &TimeZone_t1877_1_0_0/* this_arg */
	, &TimeZone_t1877_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t1877)/* instance_size */
	, sizeof (TimeZone_t1877)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t1877_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t1878_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor()
extern const MethodInfo CurrentSystemTimeZone__ctor_m10319_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m10319/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone__ctor_m10320_ParameterInfos[] = 
{
	{"lnow", 0, 134224575, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor(System.Int64)
extern const MethodInfo CurrentSystemTimeZone__ctor_m10320_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m10320/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone__ctor_m10320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_ParameterInfos[] = 
{
	{"sender", 0, 134224576, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int64U5BU5D_t1912_1_0_2;
extern const Il2CppType Int64U5BU5D_t1912_1_0_0;
extern const Il2CppType StringU5BU5D_t45_1_0_2;
extern const Il2CppType StringU5BU5D_t45_1_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetTimeZoneData_m10322_ParameterInfos[] = 
{
	{"year", 0, 134224577, 0, &Int32_t54_0_0_0},
	{"data", 1, 134224578, 0, &Int64U5BU5D_t1912_1_0_2},
	{"names", 2, 134224579, 0, &StringU5BU5D_t45_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54_Int64U5BU5DU26_t2416_StringU5BU5DU26_t2417 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)
extern const MethodInfo CurrentSystemTimeZone_GetTimeZoneData_m10322_MethodInfo = 
{
	"GetTimeZoneData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetTimeZoneData_m10322/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54_Int64U5BU5DU26_t2416_StringU5BU5DU26_t2417/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetTimeZoneData_m10322_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetDaylightChanges_m10323_ParameterInfos[] = 
{
	{"year", 0, 134224580, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m10323_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightChanges_m10323/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1498_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetDaylightChanges_m10323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetUtcOffset_m10324_ParameterInfos[] = 
{
	{"time", 0, 134224581, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.CurrentSystemTimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m10324_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetUtcOffset_m10324/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t1051_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t1051_DateTime_t508/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetUtcOffset_m10324_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DaylightTime_t1498_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_OnDeserialization_m10325_ParameterInfos[] = 
{
	{"dlt", 0, 134224582, 0, &DaylightTime_t1498_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::OnDeserialization(System.Globalization.DaylightTime)
extern const MethodInfo CurrentSystemTimeZone_OnDeserialization_m10325_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_OnDeserialization_m10325/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_OnDeserialization_m10325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64U5BU5D_t1912_0_0_0;
extern const Il2CppType Int64U5BU5D_t1912_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetDaylightTimeFromData_m10326_ParameterInfos[] = 
{
	{"data", 0, 134224583, 0, &Int64U5BU5D_t1912_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightTimeFromData(System.Int64[])
extern const MethodInfo CurrentSystemTimeZone_GetDaylightTimeFromData_m10326_MethodInfo = 
{
	"GetDaylightTimeFromData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightTimeFromData_m10326/* method */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t1498_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t1878_CurrentSystemTimeZone_GetDaylightTimeFromData_m10326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CurrentSystemTimeZone_t1878_MethodInfos[] =
{
	&CurrentSystemTimeZone__ctor_m10319_MethodInfo,
	&CurrentSystemTimeZone__ctor_m10320_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_MethodInfo,
	&CurrentSystemTimeZone_GetTimeZoneData_m10322_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m10323_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m10324_MethodInfo,
	&CurrentSystemTimeZone_OnDeserialization_m10325_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightTimeFromData_m10326_MethodInfo,
	NULL
};
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m10323_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m10324_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_MethodInfo;
static const Il2CppMethodReference CurrentSystemTimeZone_t1878_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m10323_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m10324_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m10313_MethodInfo,
	&TimeZone_ToLocalTime_m10315_MethodInfo,
	&TimeZone_ToUniversalTime_m10316_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10321_MethodInfo,
};
static bool CurrentSystemTimeZone_t1878_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDeserializationCallback_t1206_0_0_0;
static const Il2CppType* CurrentSystemTimeZone_t1878_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t1206_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t1878_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t1206_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t1878_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t1878_1_0_0;
struct CurrentSystemTimeZone_t1878;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t1878_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t1878_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t1878_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t1877_0_0_0/* parent */
	, CurrentSystemTimeZone_t1878_VTable/* vtableMethods */
	, CurrentSystemTimeZone_t1878_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2501/* fieldStart */

};
TypeInfo CurrentSystemTimeZone_t1878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, CurrentSystemTimeZone_t1878_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CurrentSystemTimeZone_t1878_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t1878_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t1878_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t1878_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t1878)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t1878)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t1878_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// Metadata Definition System.TypeCode
extern TypeInfo TypeCode_t1879_il2cpp_TypeInfo;
// System.TypeCode
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
static const MethodInfo* TypeCode_t1879_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeCode_t1879_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TypeCode_t1879_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeCode_t1879_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeCode_t1879_0_0_0;
extern const Il2CppType TypeCode_t1879_1_0_0;
const Il2CppTypeDefinitionMetadata TypeCode_t1879_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeCode_t1879_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TypeCode_t1879_VTable/* vtableMethods */
	, TypeCode_t1879_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2509/* fieldStart */

};
TypeInfo TypeCode_t1879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeCode"/* name */
	, "System"/* namespaze */
	, TypeCode_t1879_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 921/* custom_attributes_cache */
	, &TypeCode_t1879_0_0_0/* byval_arg */
	, &TypeCode_t1879_1_0_0/* this_arg */
	, &TypeCode_t1879_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeCode_t1879)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeCode_t1879)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// Metadata Definition System.TypeInitializationException
extern TypeInfo TypeInitializationException_t1880_il2cpp_TypeInfo;
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TypeInitializationException_t1880_TypeInitializationException__ctor_m10327_ParameterInfos[] = 
{
	{"info", 0, 134224584, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224585, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException__ctor_m10327_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInitializationException__ctor_m10327/* method */
	, &TypeInitializationException_t1880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TypeInitializationException_t1880_TypeInitializationException__ctor_m10327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TypeInitializationException_t1880_TypeInitializationException_GetObjectData_m10328_ParameterInfos[] = 
{
	{"info", 0, 134224586, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224587, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException_GetObjectData_m10328_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeInitializationException_GetObjectData_m10328/* method */
	, &TypeInitializationException_t1880_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TypeInitializationException_t1880_TypeInitializationException_GetObjectData_m10328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInitializationException_t1880_MethodInfos[] =
{
	&TypeInitializationException__ctor_m10327_MethodInfo,
	&TypeInitializationException_GetObjectData_m10328_MethodInfo,
	NULL
};
extern const MethodInfo TypeInitializationException_GetObjectData_m10328_MethodInfo;
static const Il2CppMethodReference TypeInitializationException_t1880_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&TypeInitializationException_GetObjectData_m10328_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&TypeInitializationException_GetObjectData_m10328_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool TypeInitializationException_t1880_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInitializationException_t1880_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInitializationException_t1880_0_0_0;
extern const Il2CppType TypeInitializationException_t1880_1_0_0;
struct TypeInitializationException_t1880;
const Il2CppTypeDefinitionMetadata TypeInitializationException_t1880_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInitializationException_t1880_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, TypeInitializationException_t1880_VTable/* vtableMethods */
	, TypeInitializationException_t1880_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2528/* fieldStart */

};
TypeInfo TypeInitializationException_t1880_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInitializationException"/* name */
	, "System"/* namespaze */
	, TypeInitializationException_t1880_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInitializationException_t1880_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 922/* custom_attributes_cache */
	, &TypeInitializationException_t1880_0_0_0/* byval_arg */
	, &TypeInitializationException_t1880_1_0_0/* this_arg */
	, &TypeInitializationException_t1880_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInitializationException_t1880)/* instance_size */
	, sizeof (TypeInitializationException_t1880)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// Metadata Definition System.TypeLoadException
extern TypeInfo TypeLoadException_t1836_il2cpp_TypeInfo;
// System.TypeLoadException
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor()
extern const MethodInfo TypeLoadException__ctor_m10329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m10329/* method */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeLoadException_t1836_TypeLoadException__ctor_m10330_ParameterInfos[] = 
{
	{"message", 0, 134224588, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.String)
extern const MethodInfo TypeLoadException__ctor_m10330_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m10330/* method */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TypeLoadException_t1836_TypeLoadException__ctor_m10330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TypeLoadException_t1836_TypeLoadException__ctor_m10331_ParameterInfos[] = 
{
	{"info", 0, 134224589, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224590, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException__ctor_m10331_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m10331/* method */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TypeLoadException_t1836_TypeLoadException__ctor_m10331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TypeLoadException::get_Message()
extern const MethodInfo TypeLoadException_get_Message_m10332_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&TypeLoadException_get_Message_m10332/* method */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TypeLoadException_t1836_TypeLoadException_GetObjectData_m10333_ParameterInfos[] = 
{
	{"info", 0, 134224591, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224592, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException_GetObjectData_m10333_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeLoadException_GetObjectData_m10333/* method */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TypeLoadException_t1836_TypeLoadException_GetObjectData_m10333_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLoadException_t1836_MethodInfos[] =
{
	&TypeLoadException__ctor_m10329_MethodInfo,
	&TypeLoadException__ctor_m10330_MethodInfo,
	&TypeLoadException__ctor_m10331_MethodInfo,
	&TypeLoadException_get_Message_m10332_MethodInfo,
	&TypeLoadException_GetObjectData_m10333_MethodInfo,
	NULL
};
extern const MethodInfo TypeLoadException_get_Message_m10332_MethodInfo;
static const PropertyInfo TypeLoadException_t1836____Message_PropertyInfo = 
{
	&TypeLoadException_t1836_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &TypeLoadException_get_Message_m10332_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeLoadException_t1836_PropertyInfos[] =
{
	&TypeLoadException_t1836____Message_PropertyInfo,
	NULL
};
extern const MethodInfo TypeLoadException_GetObjectData_m10333_MethodInfo;
static const Il2CppMethodReference TypeLoadException_t1836_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&TypeLoadException_GetObjectData_m10333_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&TypeLoadException_get_Message_m10332_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&TypeLoadException_GetObjectData_m10333_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool TypeLoadException_t1836_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLoadException_t1836_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLoadException_t1836_0_0_0;
extern const Il2CppType TypeLoadException_t1836_1_0_0;
struct TypeLoadException_t1836;
const Il2CppTypeDefinitionMetadata TypeLoadException_t1836_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLoadException_t1836_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, TypeLoadException_t1836_VTable/* vtableMethods */
	, TypeLoadException_t1836_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2529/* fieldStart */

};
TypeInfo TypeLoadException_t1836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLoadException"/* name */
	, "System"/* namespaze */
	, TypeLoadException_t1836_MethodInfos/* methods */
	, TypeLoadException_t1836_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeLoadException_t1836_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 923/* custom_attributes_cache */
	, &TypeLoadException_t1836_0_0_0/* byval_arg */
	, &TypeLoadException_t1836_1_0_0/* this_arg */
	, &TypeLoadException_t1836_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLoadException_t1836)/* instance_size */
	, sizeof (TypeLoadException_t1836)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Metadata Definition System.UnauthorizedAccessException
extern TypeInfo UnauthorizedAccessException_t1881_il2cpp_TypeInfo;
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor()
extern const MethodInfo UnauthorizedAccessException__ctor_m10334_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m10334/* method */
	, &UnauthorizedAccessException_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t1881_UnauthorizedAccessException__ctor_m10335_ParameterInfos[] = 
{
	{"message", 0, 134224593, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.String)
extern const MethodInfo UnauthorizedAccessException__ctor_m10335_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m10335/* method */
	, &UnauthorizedAccessException_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnauthorizedAccessException_t1881_UnauthorizedAccessException__ctor_m10335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t1881_UnauthorizedAccessException__ctor_m10336_ParameterInfos[] = 
{
	{"info", 0, 134224594, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224595, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnauthorizedAccessException__ctor_m10336_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m10336/* method */
	, &UnauthorizedAccessException_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UnauthorizedAccessException_t1881_UnauthorizedAccessException__ctor_m10336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnauthorizedAccessException_t1881_MethodInfos[] =
{
	&UnauthorizedAccessException__ctor_m10334_MethodInfo,
	&UnauthorizedAccessException__ctor_m10335_MethodInfo,
	&UnauthorizedAccessException__ctor_m10336_MethodInfo,
	NULL
};
static const Il2CppMethodReference UnauthorizedAccessException_t1881_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool UnauthorizedAccessException_t1881_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnauthorizedAccessException_t1881_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnauthorizedAccessException_t1881_0_0_0;
extern const Il2CppType UnauthorizedAccessException_t1881_1_0_0;
struct UnauthorizedAccessException_t1881;
const Il2CppTypeDefinitionMetadata UnauthorizedAccessException_t1881_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnauthorizedAccessException_t1881_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, UnauthorizedAccessException_t1881_VTable/* vtableMethods */
	, UnauthorizedAccessException_t1881_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnauthorizedAccessException_t1881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnauthorizedAccessException"/* name */
	, "System"/* namespaze */
	, UnauthorizedAccessException_t1881_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnauthorizedAccessException_t1881_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 924/* custom_attributes_cache */
	, &UnauthorizedAccessException_t1881_0_0_0/* byval_arg */
	, &UnauthorizedAccessException_t1881_1_0_0/* this_arg */
	, &UnauthorizedAccessException_t1881_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnauthorizedAccessException_t1881)/* instance_size */
	, sizeof (UnauthorizedAccessException_t1881)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// Metadata Definition System.UnhandledExceptionEventArgs
extern TypeInfo UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo;
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo UnhandledExceptionEventArgs_t1882_UnhandledExceptionEventArgs__ctor_m10337_ParameterInfos[] = 
{
	{"exception", 0, 134224596, 0, &Object_t_0_0_0},
	{"isTerminating", 1, 134224597, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventArgs::.ctor(System.Object,System.Boolean)
extern const MethodInfo UnhandledExceptionEventArgs__ctor_m10337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs__ctor_m10337/* method */
	, &UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, UnhandledExceptionEventArgs_t1882_UnhandledExceptionEventArgs__ctor_m10337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m10338_MethodInfo = 
{
	"get_ExceptionObject"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_ExceptionObject_m10338/* method */
	, &UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 926/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.UnhandledExceptionEventArgs::get_IsTerminating()
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m10339_MethodInfo = 
{
	"get_IsTerminating"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_IsTerminating_m10339/* method */
	, &UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 927/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventArgs_t1882_MethodInfos[] =
{
	&UnhandledExceptionEventArgs__ctor_m10337_MethodInfo,
	&UnhandledExceptionEventArgs_get_ExceptionObject_m10338_MethodInfo,
	&UnhandledExceptionEventArgs_get_IsTerminating_m10339_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m10338_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t1882____ExceptionObject_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* parent */
	, "ExceptionObject"/* name */
	, &UnhandledExceptionEventArgs_get_ExceptionObject_m10338_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m10339_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t1882____IsTerminating_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* parent */
	, "IsTerminating"/* name */
	, &UnhandledExceptionEventArgs_get_IsTerminating_m10339_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UnhandledExceptionEventArgs_t1882_PropertyInfos[] =
{
	&UnhandledExceptionEventArgs_t1882____ExceptionObject_PropertyInfo,
	&UnhandledExceptionEventArgs_t1882____IsTerminating_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UnhandledExceptionEventArgs_t1882_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool UnhandledExceptionEventArgs_t1882_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventArgs_t1882_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t1882_1_0_0;
struct UnhandledExceptionEventArgs_t1882;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventArgs_t1882_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1249_0_0_0/* parent */
	, UnhandledExceptionEventArgs_t1882_VTable/* vtableMethods */
	, UnhandledExceptionEventArgs_t1882_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2532/* fieldStart */

};
TypeInfo UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventArgs"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventArgs_t1882_MethodInfos/* methods */
	, UnhandledExceptionEventArgs_t1882_PropertyInfos/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventArgs_t1882_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 925/* custom_attributes_cache */
	, &UnhandledExceptionEventArgs_t1882_0_0_0/* byval_arg */
	, &UnhandledExceptionEventArgs_t1882_1_0_0/* this_arg */
	, &UnhandledExceptionEventArgs_t1882_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventArgs_t1882)/* instance_size */
	, sizeof (UnhandledExceptionEventArgs_t1882)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// Metadata Definition System.UnitySerializationHolder/UnityType
extern TypeInfo UnityType_t1883_il2cpp_TypeInfo;
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
static const MethodInfo* UnityType_t1883_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityType_t1883_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UnityType_t1883_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityType_t1883_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnityType_t1883_0_0_0;
extern const Il2CppType UnityType_t1883_1_0_0;
extern TypeInfo UnitySerializationHolder_t1884_il2cpp_TypeInfo;
extern const Il2CppType UnitySerializationHolder_t1884_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t367_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityType_t1883_DefinitionMetadata = 
{
	&UnitySerializationHolder_t1884_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityType_t1883_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UnityType_t1883_VTable/* vtableMethods */
	, UnityType_t1883_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2534/* fieldStart */

};
TypeInfo UnityType_t1883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityType"/* name */
	, ""/* namespaze */
	, UnityType_t1883_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityType_t1883_0_0_0/* byval_arg */
	, &UnityType_t1883_1_0_0/* this_arg */
	, &UnityType_t1883_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityType_t1883)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityType_t1883)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// Metadata Definition System.UnitySerializationHolder
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder__ctor_m10340_ParameterInfos[] = 
{
	{"info", 0, 134224598, 0, &SerializationInfo_t725_0_0_0},
	{"ctx", 1, 134224599, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder__ctor_m10340_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnitySerializationHolder__ctor_m10340/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder__ctor_m10340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder_GetTypeData_m10341_ParameterInfos[] = 
{
	{"instance", 0, 134224600, 0, &Type_t_0_0_0},
	{"info", 1, 134224601, 0, &SerializationInfo_t725_0_0_0},
	{"ctx", 2, 134224602, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetTypeData_m10341_MethodInfo = 
{
	"GetTypeData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetTypeData_m10341/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder_GetTypeData_m10341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DBNull_t1827_0_0_0;
extern const Il2CppType DBNull_t1827_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder_GetDBNullData_m10342_ParameterInfos[] = 
{
	{"instance", 0, 134224603, 0, &DBNull_t1827_0_0_0},
	{"info", 1, 134224604, 0, &SerializationInfo_t725_0_0_0},
	{"ctx", 2, 134224605, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetDBNullData_m10342_MethodInfo = 
{
	"GetDBNullData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetDBNullData_m10342/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder_GetDBNullData_m10342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t1549_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder_GetModuleData_m10343_ParameterInfos[] = 
{
	{"instance", 0, 134224606, 0, &Module_t1549_0_0_0},
	{"info", 1, 134224607, 0, &SerializationInfo_t725_0_0_0},
	{"ctx", 2, 134224608, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetModuleData_m10343_MethodInfo = 
{
	"GetModuleData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetModuleData_m10343/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder_GetModuleData_m10343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder_GetObjectData_m10344_ParameterInfos[] = 
{
	{"info", 0, 134224609, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224610, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetObjectData_m10344_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetObjectData_m10344/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder_GetObjectData_m10344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnitySerializationHolder_t1884_UnitySerializationHolder_GetRealObject_m10345_ParameterInfos[] = 
{
	{"context", 0, 134224611, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetRealObject_m10345_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetRealObject_m10345/* method */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t726/* invoker_method */
	, UnitySerializationHolder_t1884_UnitySerializationHolder_GetRealObject_m10345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnitySerializationHolder_t1884_MethodInfos[] =
{
	&UnitySerializationHolder__ctor_m10340_MethodInfo,
	&UnitySerializationHolder_GetTypeData_m10341_MethodInfo,
	&UnitySerializationHolder_GetDBNullData_m10342_MethodInfo,
	&UnitySerializationHolder_GetModuleData_m10343_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m10344_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m10345_MethodInfo,
	NULL
};
static const Il2CppType* UnitySerializationHolder_t1884_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnityType_t1883_0_0_0,
};
extern const MethodInfo UnitySerializationHolder_GetObjectData_m10344_MethodInfo;
extern const MethodInfo UnitySerializationHolder_GetRealObject_m10345_MethodInfo;
static const Il2CppMethodReference UnitySerializationHolder_t1884_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m10344_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m10345_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m10344_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m10345_MethodInfo,
};
static bool UnitySerializationHolder_t1884_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t1941_0_0_0;
static const Il2CppType* UnitySerializationHolder_t1884_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&IObjectReference_t1941_0_0_0,
};
static Il2CppInterfaceOffsetPair UnitySerializationHolder_t1884_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IObjectReference_t1941_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnitySerializationHolder_t1884_1_0_0;
struct UnitySerializationHolder_t1884;
const Il2CppTypeDefinitionMetadata UnitySerializationHolder_t1884_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnitySerializationHolder_t1884_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, UnitySerializationHolder_t1884_InterfacesTypeInfos/* implementedInterfaces */
	, UnitySerializationHolder_t1884_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnitySerializationHolder_t1884_VTable/* vtableMethods */
	, UnitySerializationHolder_t1884_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2539/* fieldStart */

};
TypeInfo UnitySerializationHolder_t1884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnitySerializationHolder"/* name */
	, "System"/* namespaze */
	, UnitySerializationHolder_t1884_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnitySerializationHolder_t1884_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnitySerializationHolder_t1884_0_0_0/* byval_arg */
	, &UnitySerializationHolder_t1884_1_0_0/* this_arg */
	, &UnitySerializationHolder_t1884_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnitySerializationHolder_t1884)/* instance_size */
	, sizeof (UnitySerializationHolder_t1884)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Version
#include "mscorlib_System_Version.h"
// Metadata Definition System.Version
extern TypeInfo Version_t1006_il2cpp_TypeInfo;
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor()
extern const MethodInfo Version__ctor_m10346_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m10346/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1006_Version__ctor_m10347_ParameterInfos[] = 
{
	{"version", 0, 134224612, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.String)
extern const MethodInfo Version__ctor_m10347_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m10347/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Version_t1006_Version__ctor_m10347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Version_t1006_Version__ctor_m4707_ParameterInfos[] = 
{
	{"major", 0, 134224613, 0, &Int32_t54_0_0_0},
	{"minor", 1, 134224614, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m4707_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m4707/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, Version_t1006_Version__ctor_m4707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Version_t1006_Version__ctor_m10348_ParameterInfos[] = 
{
	{"major", 0, 134224615, 0, &Int32_t54_0_0_0},
	{"minor", 1, 134224616, 0, &Int32_t54_0_0_0},
	{"build", 2, 134224617, 0, &Int32_t54_0_0_0},
	{"revision", 3, 134224618, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m10348_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m10348/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Version_t1006_Version__ctor_m10348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Version_t1006_Version_CheckedSet_m10349_ParameterInfos[] = 
{
	{"defined", 0, 134224619, 0, &Int32_t54_0_0_0},
	{"major", 1, 134224620, 0, &Int32_t54_0_0_0},
	{"minor", 2, 134224621, 0, &Int32_t54_0_0_0},
	{"build", 3, 134224622, 0, &Int32_t54_0_0_0},
	{"revision", 4, 134224623, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version_CheckedSet_m10349_MethodInfo = 
{
	"CheckedSet"/* name */
	, (methodPointerType)&Version_CheckedSet_m10349/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Version_t1006_Version_CheckedSet_m10349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Build()
extern const MethodInfo Version_get_Build_m10350_MethodInfo = 
{
	"get_Build"/* name */
	, (methodPointerType)&Version_get_Build_m10350/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Major()
extern const MethodInfo Version_get_Major_m10351_MethodInfo = 
{
	"get_Major"/* name */
	, (methodPointerType)&Version_get_Major_m10351/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Minor()
extern const MethodInfo Version_get_Minor_m10352_MethodInfo = 
{
	"get_Minor"/* name */
	, (methodPointerType)&Version_get_Minor_m10352/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Revision()
extern const MethodInfo Version_get_Revision_m10353_MethodInfo = 
{
	"get_Revision"/* name */
	, (methodPointerType)&Version_get_Revision_m10353/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1006_Version_CompareTo_m10354_ParameterInfos[] = 
{
	{"version", 0, 134224624, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Object)
extern const MethodInfo Version_CompareTo_m10354_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m10354/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Version_t1006_Version_CompareTo_m10354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1006_Version_Equals_m10355_ParameterInfos[] = 
{
	{"obj", 0, 134224625, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Object)
extern const MethodInfo Version_Equals_m10355_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m10355/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Version_t1006_Version_Equals_m10355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo Version_t1006_Version_CompareTo_m10356_ParameterInfos[] = 
{
	{"value", 0, 134224626, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Version)
extern const MethodInfo Version_CompareTo_m10356_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m10356/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Version_t1006_Version_CompareTo_m10356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo Version_t1006_Version_Equals_m10357_ParameterInfos[] = 
{
	{"obj", 0, 134224627, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Version)
extern const MethodInfo Version_Equals_m10357_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m10357/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Version_t1006_Version_Equals_m10357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::GetHashCode()
extern const MethodInfo Version_GetHashCode_m10358_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Version_GetHashCode_m10358/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Version::ToString()
extern const MethodInfo Version_ToString_m10359_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Version_ToString_m10359/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1006_Version_CreateFromString_m10360_ParameterInfos[] = 
{
	{"info", 0, 134224628, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Version::CreateFromString(System.String)
extern const MethodInfo Version_CreateFromString_m10360_MethodInfo = 
{
	"CreateFromString"/* name */
	, (methodPointerType)&Version_CreateFromString_m10360/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Version_t1006_Version_CreateFromString_m10360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo Version_t1006_Version_op_Equality_m10361_ParameterInfos[] = 
{
	{"v1", 0, 134224629, 0, &Version_t1006_0_0_0},
	{"v2", 1, 134224630, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern const MethodInfo Version_op_Equality_m10361_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Version_op_Equality_m10361/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Version_t1006_Version_op_Equality_m10361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo Version_t1006_Version_op_Inequality_m10362_ParameterInfos[] = 
{
	{"v1", 0, 134224631, 0, &Version_t1006_0_0_0},
	{"v2", 1, 134224632, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern const MethodInfo Version_op_Inequality_m10362_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&Version_op_Inequality_m10362/* method */
	, &Version_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Version_t1006_Version_op_Inequality_m10362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Version_t1006_MethodInfos[] =
{
	&Version__ctor_m10346_MethodInfo,
	&Version__ctor_m10347_MethodInfo,
	&Version__ctor_m4707_MethodInfo,
	&Version__ctor_m10348_MethodInfo,
	&Version_CheckedSet_m10349_MethodInfo,
	&Version_get_Build_m10350_MethodInfo,
	&Version_get_Major_m10351_MethodInfo,
	&Version_get_Minor_m10352_MethodInfo,
	&Version_get_Revision_m10353_MethodInfo,
	&Version_CompareTo_m10354_MethodInfo,
	&Version_Equals_m10355_MethodInfo,
	&Version_CompareTo_m10356_MethodInfo,
	&Version_Equals_m10357_MethodInfo,
	&Version_GetHashCode_m10358_MethodInfo,
	&Version_ToString_m10359_MethodInfo,
	&Version_CreateFromString_m10360_MethodInfo,
	&Version_op_Equality_m10361_MethodInfo,
	&Version_op_Inequality_m10362_MethodInfo,
	NULL
};
extern const MethodInfo Version_get_Build_m10350_MethodInfo;
static const PropertyInfo Version_t1006____Build_PropertyInfo = 
{
	&Version_t1006_il2cpp_TypeInfo/* parent */
	, "Build"/* name */
	, &Version_get_Build_m10350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Major_m10351_MethodInfo;
static const PropertyInfo Version_t1006____Major_PropertyInfo = 
{
	&Version_t1006_il2cpp_TypeInfo/* parent */
	, "Major"/* name */
	, &Version_get_Major_m10351_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Minor_m10352_MethodInfo;
static const PropertyInfo Version_t1006____Minor_PropertyInfo = 
{
	&Version_t1006_il2cpp_TypeInfo/* parent */
	, "Minor"/* name */
	, &Version_get_Minor_m10352_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Revision_m10353_MethodInfo;
static const PropertyInfo Version_t1006____Revision_PropertyInfo = 
{
	&Version_t1006_il2cpp_TypeInfo/* parent */
	, "Revision"/* name */
	, &Version_get_Revision_m10353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Version_t1006_PropertyInfos[] =
{
	&Version_t1006____Build_PropertyInfo,
	&Version_t1006____Major_PropertyInfo,
	&Version_t1006____Minor_PropertyInfo,
	&Version_t1006____Revision_PropertyInfo,
	NULL
};
extern const MethodInfo Version_Equals_m10355_MethodInfo;
extern const MethodInfo Version_GetHashCode_m10358_MethodInfo;
extern const MethodInfo Version_ToString_m10359_MethodInfo;
extern const MethodInfo Version_CompareTo_m10354_MethodInfo;
extern const MethodInfo Version_CompareTo_m10356_MethodInfo;
extern const MethodInfo Version_Equals_m10357_MethodInfo;
static const Il2CppMethodReference Version_t1006_VTable[] =
{
	&Version_Equals_m10355_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Version_GetHashCode_m10358_MethodInfo,
	&Version_ToString_m10359_MethodInfo,
	&Version_CompareTo_m10354_MethodInfo,
	&Version_CompareTo_m10356_MethodInfo,
	&Version_Equals_m10357_MethodInfo,
};
static bool Version_t1006_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t2418_0_0_0;
extern const Il2CppType IEquatable_1_t2419_0_0_0;
static const Il2CppType* Version_t1006_InterfacesTypeInfos[] = 
{
	&IComparable_t76_0_0_0,
	&ICloneable_t427_0_0_0,
	&IComparable_1_t2418_0_0_0,
	&IEquatable_1_t2419_0_0_0,
};
static Il2CppInterfaceOffsetPair Version_t1006_InterfacesOffsets[] = 
{
	{ &IComparable_t76_0_0_0, 4},
	{ &ICloneable_t427_0_0_0, 5},
	{ &IComparable_1_t2418_0_0_0, 5},
	{ &IEquatable_1_t2419_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Version_t1006_1_0_0;
struct Version_t1006;
const Il2CppTypeDefinitionMetadata Version_t1006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Version_t1006_InterfacesTypeInfos/* implementedInterfaces */
	, Version_t1006_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Version_t1006_VTable/* vtableMethods */
	, Version_t1006_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2542/* fieldStart */

};
TypeInfo Version_t1006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Version"/* name */
	, "System"/* namespaze */
	, Version_t1006_MethodInfos/* methods */
	, Version_t1006_PropertyInfos/* properties */
	, NULL/* events */
	, &Version_t1006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 928/* custom_attributes_cache */
	, &Version_t1006_0_0_0/* byval_arg */
	, &Version_t1006_1_0_0/* this_arg */
	, &Version_t1006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Version_t1006)/* instance_size */
	, sizeof (Version_t1006)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// Metadata Definition System.WeakReference
extern TypeInfo WeakReference_t1670_il2cpp_TypeInfo;
// System.WeakReference
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor()
extern const MethodInfo WeakReference__ctor_m10363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m10363/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t1670_WeakReference__ctor_m10364_ParameterInfos[] = 
{
	{"target", 0, 134224633, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object)
extern const MethodInfo WeakReference__ctor_m10364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m10364/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, WeakReference_t1670_WeakReference__ctor_m10364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo WeakReference_t1670_WeakReference__ctor_m10365_ParameterInfos[] = 
{
	{"target", 0, 134224634, 0, &Object_t_0_0_0},
	{"trackResurrection", 1, 134224635, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
extern const MethodInfo WeakReference__ctor_m10365_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m10365/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, WeakReference_t1670_WeakReference__ctor_m10365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo WeakReference_t1670_WeakReference__ctor_m10366_ParameterInfos[] = 
{
	{"info", 0, 134224636, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224637, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference__ctor_m10366_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m10366/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, WeakReference_t1670_WeakReference__ctor_m10366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t1670_WeakReference_AllocateHandle_m10367_ParameterInfos[] = 
{
	{"target", 0, 134224638, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::AllocateHandle(System.Object)
extern const MethodInfo WeakReference_AllocateHandle_m10367_MethodInfo = 
{
	"AllocateHandle"/* name */
	, (methodPointerType)&WeakReference_AllocateHandle_m10367/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, WeakReference_t1670_WeakReference_AllocateHandle_m10367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.WeakReference::get_Target()
extern const MethodInfo WeakReference_get_Target_m10368_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&WeakReference_get_Target_m10368/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.WeakReference::get_TrackResurrection()
extern const MethodInfo WeakReference_get_TrackResurrection_m10369_MethodInfo = 
{
	"get_TrackResurrection"/* name */
	, (methodPointerType)&WeakReference_get_TrackResurrection_m10369/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::Finalize()
extern const MethodInfo WeakReference_Finalize_m10370_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&WeakReference_Finalize_m10370/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo WeakReference_t1670_WeakReference_GetObjectData_m10371_ParameterInfos[] = 
{
	{"info", 0, 134224639, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134224640, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference_GetObjectData_m10371_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&WeakReference_GetObjectData_m10371/* method */
	, &WeakReference_t1670_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, WeakReference_t1670_WeakReference_GetObjectData_m10371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WeakReference_t1670_MethodInfos[] =
{
	&WeakReference__ctor_m10363_MethodInfo,
	&WeakReference__ctor_m10364_MethodInfo,
	&WeakReference__ctor_m10365_MethodInfo,
	&WeakReference__ctor_m10366_MethodInfo,
	&WeakReference_AllocateHandle_m10367_MethodInfo,
	&WeakReference_get_Target_m10368_MethodInfo,
	&WeakReference_get_TrackResurrection_m10369_MethodInfo,
	&WeakReference_Finalize_m10370_MethodInfo,
	&WeakReference_GetObjectData_m10371_MethodInfo,
	NULL
};
extern const MethodInfo WeakReference_get_Target_m10368_MethodInfo;
static const PropertyInfo WeakReference_t1670____Target_PropertyInfo = 
{
	&WeakReference_t1670_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &WeakReference_get_Target_m10368_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WeakReference_get_TrackResurrection_m10369_MethodInfo;
static const PropertyInfo WeakReference_t1670____TrackResurrection_PropertyInfo = 
{
	&WeakReference_t1670_il2cpp_TypeInfo/* parent */
	, "TrackResurrection"/* name */
	, &WeakReference_get_TrackResurrection_m10369_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WeakReference_t1670_PropertyInfos[] =
{
	&WeakReference_t1670____Target_PropertyInfo,
	&WeakReference_t1670____TrackResurrection_PropertyInfo,
	NULL
};
extern const MethodInfo WeakReference_Finalize_m10370_MethodInfo;
extern const MethodInfo WeakReference_GetObjectData_m10371_MethodInfo;
static const Il2CppMethodReference WeakReference_t1670_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&WeakReference_Finalize_m10370_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&WeakReference_GetObjectData_m10371_MethodInfo,
	&WeakReference_get_Target_m10368_MethodInfo,
	&WeakReference_get_TrackResurrection_m10369_MethodInfo,
	&WeakReference_GetObjectData_m10371_MethodInfo,
};
static bool WeakReference_t1670_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WeakReference_t1670_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair WeakReference_t1670_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WeakReference_t1670_0_0_0;
extern const Il2CppType WeakReference_t1670_1_0_0;
struct WeakReference_t1670;
const Il2CppTypeDefinitionMetadata WeakReference_t1670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WeakReference_t1670_InterfacesTypeInfos/* implementedInterfaces */
	, WeakReference_t1670_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WeakReference_t1670_VTable/* vtableMethods */
	, WeakReference_t1670_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2547/* fieldStart */

};
TypeInfo WeakReference_t1670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WeakReference"/* name */
	, "System"/* namespaze */
	, WeakReference_t1670_MethodInfos/* methods */
	, WeakReference_t1670_PropertyInfos/* properties */
	, NULL/* events */
	, &WeakReference_t1670_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 929/* custom_attributes_cache */
	, &WeakReference_t1670_0_0_0/* byval_arg */
	, &WeakReference_t1670_1_0_0/* this_arg */
	, &WeakReference_t1670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WeakReference_t1670)/* instance_size */
	, sizeof (WeakReference_t1670)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1885_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t1885_PrimalityTest__ctor_m10372_ParameterInfos[] = 
{
	{"object", 0, 134224641, 0, &Object_t_0_0_0},
	{"method", 1, 134224642, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m10372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m10372/* method */
	, &PrimalityTest_t1885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t1885_PrimalityTest__ctor_m10372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1418_0_0_0;
extern const Il2CppType BigInteger_t1418_0_0_0;
extern const Il2CppType ConfidenceFactor_t1415_0_0_0;
extern const Il2CppType ConfidenceFactor_t1415_0_0_0;
static const ParameterInfo PrimalityTest_t1885_PrimalityTest_Invoke_m10373_ParameterInfos[] = 
{
	{"bi", 0, 134224643, 0, &BigInteger_t1418_0_0_0},
	{"confidence", 1, 134224644, 0, &ConfidenceFactor_t1415_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m10373_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m10373/* method */
	, &PrimalityTest_t1885_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Int32_t54/* invoker_method */
	, PrimalityTest_t1885_PrimalityTest_Invoke_m10373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1418_0_0_0;
extern const Il2CppType ConfidenceFactor_t1415_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t1885_PrimalityTest_BeginInvoke_m10374_ParameterInfos[] = 
{
	{"bi", 0, 134224645, 0, &BigInteger_t1418_0_0_0},
	{"confidence", 1, 134224646, 0, &ConfidenceFactor_t1415_0_0_0},
	{"callback", 2, 134224647, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224648, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m10374_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m10374/* method */
	, &PrimalityTest_t1885_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t1885_PrimalityTest_BeginInvoke_m10374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo PrimalityTest_t1885_PrimalityTest_EndInvoke_m10375_ParameterInfos[] = 
{
	{"result", 0, 134224649, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m10375_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m10375/* method */
	, &PrimalityTest_t1885_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, PrimalityTest_t1885_PrimalityTest_EndInvoke_m10375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t1885_MethodInfos[] =
{
	&PrimalityTest__ctor_m10372_MethodInfo,
	&PrimalityTest_Invoke_m10373_MethodInfo,
	&PrimalityTest_BeginInvoke_m10374_MethodInfo,
	&PrimalityTest_EndInvoke_m10375_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m10373_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m10374_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m10375_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t1885_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&PrimalityTest_Invoke_m10373_MethodInfo,
	&PrimalityTest_BeginInvoke_m10374_MethodInfo,
	&PrimalityTest_EndInvoke_m10375_MethodInfo,
};
static bool PrimalityTest_t1885_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrimalityTest_t1885_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTest_t1885_0_0_0;
extern const Il2CppType PrimalityTest_t1885_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
struct PrimalityTest_t1885;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1885_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1885_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, PrimalityTest_t1885_VTable/* vtableMethods */
	, PrimalityTest_t1885_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t1885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t1885_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t1885_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1885_0_0_0/* byval_arg */
	, &PrimalityTest_t1885_1_0_0/* this_arg */
	, &PrimalityTest_t1885_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1885/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1885)/* instance_size */
	, sizeof (PrimalityTest_t1885)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// Metadata Definition System.Reflection.MemberFilter
extern TypeInfo MemberFilter_t1370_il2cpp_TypeInfo;
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MemberFilter_t1370_MemberFilter__ctor_m10376_ParameterInfos[] = 
{
	{"object", 0, 134224650, 0, &Object_t_0_0_0},
	{"method", 1, 134224651, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MemberFilter__ctor_m10376_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberFilter__ctor_m10376/* method */
	, &MemberFilter_t1370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, MemberFilter_t1370_MemberFilter__ctor_m10376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t1370_MemberFilter_Invoke_m10377_ParameterInfos[] = 
{
	{"m", 0, 134224652, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224653, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern const MethodInfo MemberFilter_Invoke_m10377_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MemberFilter_Invoke_m10377/* method */
	, &MemberFilter_t1370_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, MemberFilter_t1370_MemberFilter_Invoke_m10377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t1370_MemberFilter_BeginInvoke_m10378_ParameterInfos[] = 
{
	{"m", 0, 134224654, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224655, 0, &Object_t_0_0_0},
	{"callback", 2, 134224656, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224657, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo MemberFilter_BeginInvoke_m10378_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MemberFilter_BeginInvoke_m10378/* method */
	, &MemberFilter_t1370_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MemberFilter_t1370_MemberFilter_BeginInvoke_m10378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo MemberFilter_t1370_MemberFilter_EndInvoke_m10379_ParameterInfos[] = 
{
	{"result", 0, 134224658, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo MemberFilter_EndInvoke_m10379_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MemberFilter_EndInvoke_m10379/* method */
	, &MemberFilter_t1370_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MemberFilter_t1370_MemberFilter_EndInvoke_m10379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberFilter_t1370_MethodInfos[] =
{
	&MemberFilter__ctor_m10376_MethodInfo,
	&MemberFilter_Invoke_m10377_MethodInfo,
	&MemberFilter_BeginInvoke_m10378_MethodInfo,
	&MemberFilter_EndInvoke_m10379_MethodInfo,
	NULL
};
extern const MethodInfo MemberFilter_Invoke_m10377_MethodInfo;
extern const MethodInfo MemberFilter_BeginInvoke_m10378_MethodInfo;
extern const MethodInfo MemberFilter_EndInvoke_m10379_MethodInfo;
static const Il2CppMethodReference MemberFilter_t1370_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&MemberFilter_Invoke_m10377_MethodInfo,
	&MemberFilter_BeginInvoke_m10378_MethodInfo,
	&MemberFilter_EndInvoke_m10379_MethodInfo,
};
static bool MemberFilter_t1370_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberFilter_t1370_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberFilter_t1370_0_0_0;
extern const Il2CppType MemberFilter_t1370_1_0_0;
struct MemberFilter_t1370;
const Il2CppTypeDefinitionMetadata MemberFilter_t1370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberFilter_t1370_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, MemberFilter_t1370_VTable/* vtableMethods */
	, MemberFilter_t1370_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberFilter_t1370_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberFilter"/* name */
	, "System.Reflection"/* namespaze */
	, MemberFilter_t1370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberFilter_t1370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 930/* custom_attributes_cache */
	, &MemberFilter_t1370_0_0_0/* byval_arg */
	, &MemberFilter_t1370_1_0_0/* this_arg */
	, &MemberFilter_t1370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MemberFilter_t1370/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberFilter_t1370)/* instance_size */
	, sizeof (MemberFilter_t1370)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// Metadata Definition System.Reflection.TypeFilter
extern TypeInfo TypeFilter_t1574_il2cpp_TypeInfo;
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TypeFilter_t1574_TypeFilter__ctor_m10380_ParameterInfos[] = 
{
	{"object", 0, 134224659, 0, &Object_t_0_0_0},
	{"method", 1, 134224660, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TypeFilter__ctor_m10380_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeFilter__ctor_m10380/* method */
	, &TypeFilter_t1574_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, TypeFilter_t1574_TypeFilter__ctor_m10380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t1574_TypeFilter_Invoke_m10381_ParameterInfos[] = 
{
	{"m", 0, 134224661, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224662, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern const MethodInfo TypeFilter_Invoke_m10381_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TypeFilter_Invoke_m10381/* method */
	, &TypeFilter_t1574_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, TypeFilter_t1574_TypeFilter_Invoke_m10381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t1574_TypeFilter_BeginInvoke_m10382_ParameterInfos[] = 
{
	{"m", 0, 134224663, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224664, 0, &Object_t_0_0_0},
	{"callback", 2, 134224665, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224666, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo TypeFilter_BeginInvoke_m10382_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TypeFilter_BeginInvoke_m10382/* method */
	, &TypeFilter_t1574_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, TypeFilter_t1574_TypeFilter_BeginInvoke_m10382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo TypeFilter_t1574_TypeFilter_EndInvoke_m10383_ParameterInfos[] = 
{
	{"result", 0, 134224667, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo TypeFilter_EndInvoke_m10383_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TypeFilter_EndInvoke_m10383/* method */
	, &TypeFilter_t1574_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TypeFilter_t1574_TypeFilter_EndInvoke_m10383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeFilter_t1574_MethodInfos[] =
{
	&TypeFilter__ctor_m10380_MethodInfo,
	&TypeFilter_Invoke_m10381_MethodInfo,
	&TypeFilter_BeginInvoke_m10382_MethodInfo,
	&TypeFilter_EndInvoke_m10383_MethodInfo,
	NULL
};
extern const MethodInfo TypeFilter_Invoke_m10381_MethodInfo;
extern const MethodInfo TypeFilter_BeginInvoke_m10382_MethodInfo;
extern const MethodInfo TypeFilter_EndInvoke_m10383_MethodInfo;
static const Il2CppMethodReference TypeFilter_t1574_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&TypeFilter_Invoke_m10381_MethodInfo,
	&TypeFilter_BeginInvoke_m10382_MethodInfo,
	&TypeFilter_EndInvoke_m10383_MethodInfo,
};
static bool TypeFilter_t1574_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilter_t1574_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilter_t1574_0_0_0;
extern const Il2CppType TypeFilter_t1574_1_0_0;
struct TypeFilter_t1574;
const Il2CppTypeDefinitionMetadata TypeFilter_t1574_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilter_t1574_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, TypeFilter_t1574_VTable/* vtableMethods */
	, TypeFilter_t1574_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TypeFilter_t1574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilter"/* name */
	, "System.Reflection"/* namespaze */
	, TypeFilter_t1574_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeFilter_t1574_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 931/* custom_attributes_cache */
	, &TypeFilter_t1574_0_0_0/* byval_arg */
	, &TypeFilter_t1574_1_0_0/* this_arg */
	, &TypeFilter_t1574_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TypeFilter_t1574/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilter_t1574)/* instance_size */
	, sizeof (TypeFilter_t1574)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern TypeInfo HeaderHandler_t1887_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo HeaderHandler_t1887_HeaderHandler__ctor_m10384_ParameterInfos[] = 
{
	{"object", 0, 134224668, 0, &Object_t_0_0_0},
	{"method", 1, 134224669, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.HeaderHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo HeaderHandler__ctor_m10384_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HeaderHandler__ctor_m10384/* method */
	, &HeaderHandler_t1887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, HeaderHandler_t1887_HeaderHandler__ctor_m10384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t1886_0_0_0;
extern const Il2CppType HeaderU5BU5D_t1886_0_0_0;
static const ParameterInfo HeaderHandler_t1887_HeaderHandler_Invoke_m10385_ParameterInfos[] = 
{
	{"headers", 0, 134224670, 0, &HeaderU5BU5D_t1886_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::Invoke(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo HeaderHandler_Invoke_m10385_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&HeaderHandler_Invoke_m10385/* method */
	, &HeaderHandler_t1887_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t1887_HeaderHandler_Invoke_m10385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t1886_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HeaderHandler_t1887_HeaderHandler_BeginInvoke_m10386_ParameterInfos[] = 
{
	{"headers", 0, 134224671, 0, &HeaderU5BU5D_t1886_0_0_0},
	{"callback", 1, 134224672, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134224673, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Remoting.Messaging.HeaderHandler::BeginInvoke(System.Runtime.Remoting.Messaging.Header[],System.AsyncCallback,System.Object)
extern const MethodInfo HeaderHandler_BeginInvoke_m10386_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&HeaderHandler_BeginInvoke_m10386/* method */
	, &HeaderHandler_t1887_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t1887_HeaderHandler_BeginInvoke_m10386_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo HeaderHandler_t1887_HeaderHandler_EndInvoke_m10387_ParameterInfos[] = 
{
	{"result", 0, 134224674, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo HeaderHandler_EndInvoke_m10387_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&HeaderHandler_EndInvoke_m10387/* method */
	, &HeaderHandler_t1887_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t1887_HeaderHandler_EndInvoke_m10387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HeaderHandler_t1887_MethodInfos[] =
{
	&HeaderHandler__ctor_m10384_MethodInfo,
	&HeaderHandler_Invoke_m10385_MethodInfo,
	&HeaderHandler_BeginInvoke_m10386_MethodInfo,
	&HeaderHandler_EndInvoke_m10387_MethodInfo,
	NULL
};
extern const MethodInfo HeaderHandler_Invoke_m10385_MethodInfo;
extern const MethodInfo HeaderHandler_BeginInvoke_m10386_MethodInfo;
extern const MethodInfo HeaderHandler_EndInvoke_m10387_MethodInfo;
static const Il2CppMethodReference HeaderHandler_t1887_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&HeaderHandler_Invoke_m10385_MethodInfo,
	&HeaderHandler_BeginInvoke_m10386_MethodInfo,
	&HeaderHandler_EndInvoke_m10387_MethodInfo,
};
static bool HeaderHandler_t1887_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HeaderHandler_t1887_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HeaderHandler_t1887_0_0_0;
extern const Il2CppType HeaderHandler_t1887_1_0_0;
struct HeaderHandler_t1887;
const Il2CppTypeDefinitionMetadata HeaderHandler_t1887_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HeaderHandler_t1887_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, HeaderHandler_t1887_VTable/* vtableMethods */
	, HeaderHandler_t1887_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HeaderHandler_t1887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeaderHandler"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, HeaderHandler_t1887_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HeaderHandler_t1887_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 932/* custom_attributes_cache */
	, &HeaderHandler_t1887_0_0_0/* byval_arg */
	, &HeaderHandler_t1887_1_0_0/* this_arg */
	, &HeaderHandler_t1887_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_HeaderHandler_t1887/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeaderHandler_t1887)/* instance_size */
	, sizeof (HeaderHandler_t1887)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`1
extern TypeInfo Action_1_t2019_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Action_1_t2019_Il2CppGenericContainer;
extern TypeInfo Action_1_t2019_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Action_1_t2019_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Action_1_t2019_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Action_1_t2019_Il2CppGenericParametersArray[1] = 
{
	&Action_1_t2019_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Action_1_t2019_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Action_1_t2019_il2cpp_TypeInfo, 1, 0, Action_1_t2019_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_1_t2019_Action_1__ctor_m11052_ParameterInfos[] = 
{
	{"object", 0, 134224675, 0, &Object_t_0_0_0},
	{"method", 1, 134224676, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Action`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action_1__ctor_m11052_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_1_t2019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2019_Action_1__ctor_m11052_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2019_gp_0_0_0_0;
extern const Il2CppType Action_1_t2019_gp_0_0_0_0;
static const ParameterInfo Action_1_t2019_Action_1_Invoke_m11053_ParameterInfos[] = 
{
	{"obj", 0, 134224677, 0, &Action_1_t2019_gp_0_0_0_0},
};
// System.Void System.Action`1::Invoke(T)
extern const MethodInfo Action_1_Invoke_m11053_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_1_t2019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2019_Action_1_Invoke_m11053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2019_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_1_t2019_Action_1_BeginInvoke_m11054_ParameterInfos[] = 
{
	{"obj", 0, 134224678, 0, &Action_1_t2019_gp_0_0_0_0},
	{"callback", 1, 134224679, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134224680, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Action`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Action_1_BeginInvoke_m11054_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2019_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2019_Action_1_BeginInvoke_m11054_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Action_1_t2019_Action_1_EndInvoke_m11055_ParameterInfos[] = 
{
	{"result", 0, 134224681, 0, &IAsyncResult_t213_0_0_0},
};
// System.Void System.Action`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_1_EndInvoke_m11055_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2019_Action_1_EndInvoke_m11055_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_1_t2019_MethodInfos[] =
{
	&Action_1__ctor_m11052_MethodInfo,
	&Action_1_Invoke_m11053_MethodInfo,
	&Action_1_BeginInvoke_m11054_MethodInfo,
	&Action_1_EndInvoke_m11055_MethodInfo,
	NULL
};
extern const MethodInfo Action_1_Invoke_m11053_MethodInfo;
extern const MethodInfo Action_1_BeginInvoke_m11054_MethodInfo;
extern const MethodInfo Action_1_EndInvoke_m11055_MethodInfo;
static const Il2CppMethodReference Action_1_t2019_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Action_1_Invoke_m11053_MethodInfo,
	&Action_1_BeginInvoke_m11054_MethodInfo,
	&Action_1_EndInvoke_m11055_MethodInfo,
};
static bool Action_1_t2019_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_1_t2019_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Action_1_t2019_0_0_0;
extern const Il2CppType Action_1_t2019_1_0_0;
struct Action_1_t2019;
const Il2CppTypeDefinitionMetadata Action_1_t2019_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_1_t2019_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Action_1_t2019_VTable/* vtableMethods */
	, Action_1_t2019_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_1_t2019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2019_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_1_t2019_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_1_t2019_0_0_0/* byval_arg */
	, &Action_1_t2019_1_0_0/* this_arg */
	, &Action_1_t2019_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_1_t2019_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// Metadata Definition System.AppDomainInitializer
extern TypeInfo AppDomainInitializer_t1818_il2cpp_TypeInfo;
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t1818_AppDomainInitializer__ctor_m10388_ParameterInfos[] = 
{
	{"object", 0, 134224682, 0, &Object_t_0_0_0},
	{"method", 1, 134224683, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AppDomainInitializer__ctor_m10388_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainInitializer__ctor_m10388/* method */
	, &AppDomainInitializer_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, AppDomainInitializer_t1818_AppDomainInitializer__ctor_m10388_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
static const ParameterInfo AppDomainInitializer_t1818_AppDomainInitializer_Invoke_m10389_ParameterInfos[] = 
{
	{"args", 0, 134224684, 0, &StringU5BU5D_t45_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::Invoke(System.String[])
extern const MethodInfo AppDomainInitializer_Invoke_m10389_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AppDomainInitializer_Invoke_m10389/* method */
	, &AppDomainInitializer_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AppDomainInitializer_t1818_AppDomainInitializer_Invoke_m10389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t1818_AppDomainInitializer_BeginInvoke_m10390_ParameterInfos[] = 
{
	{"args", 0, 134224685, 0, &StringU5BU5D_t45_0_0_0},
	{"callback", 1, 134224686, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134224687, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AppDomainInitializer::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern const MethodInfo AppDomainInitializer_BeginInvoke_m10390_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_BeginInvoke_m10390/* method */
	, &AppDomainInitializer_t1818_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AppDomainInitializer_t1818_AppDomainInitializer_BeginInvoke_m10390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo AppDomainInitializer_t1818_AppDomainInitializer_EndInvoke_m10391_ParameterInfos[] = 
{
	{"result", 0, 134224688, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::EndInvoke(System.IAsyncResult)
extern const MethodInfo AppDomainInitializer_EndInvoke_m10391_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_EndInvoke_m10391/* method */
	, &AppDomainInitializer_t1818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AppDomainInitializer_t1818_AppDomainInitializer_EndInvoke_m10391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainInitializer_t1818_MethodInfos[] =
{
	&AppDomainInitializer__ctor_m10388_MethodInfo,
	&AppDomainInitializer_Invoke_m10389_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m10390_MethodInfo,
	&AppDomainInitializer_EndInvoke_m10391_MethodInfo,
	NULL
};
extern const MethodInfo AppDomainInitializer_Invoke_m10389_MethodInfo;
extern const MethodInfo AppDomainInitializer_BeginInvoke_m10390_MethodInfo;
extern const MethodInfo AppDomainInitializer_EndInvoke_m10391_MethodInfo;
static const Il2CppMethodReference AppDomainInitializer_t1818_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&AppDomainInitializer_Invoke_m10389_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m10390_MethodInfo,
	&AppDomainInitializer_EndInvoke_m10391_MethodInfo,
};
static bool AppDomainInitializer_t1818_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AppDomainInitializer_t1818_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainInitializer_t1818_0_0_0;
extern const Il2CppType AppDomainInitializer_t1818_1_0_0;
struct AppDomainInitializer_t1818;
const Il2CppTypeDefinitionMetadata AppDomainInitializer_t1818_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppDomainInitializer_t1818_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, AppDomainInitializer_t1818_VTable/* vtableMethods */
	, AppDomainInitializer_t1818_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AppDomainInitializer_t1818_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainInitializer"/* name */
	, "System"/* namespaze */
	, AppDomainInitializer_t1818_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainInitializer_t1818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 933/* custom_attributes_cache */
	, &AppDomainInitializer_t1818_0_0_0/* byval_arg */
	, &AppDomainInitializer_t1818_1_0_0/* this_arg */
	, &AppDomainInitializer_t1818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AppDomainInitializer_t1818/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainInitializer_t1818)/* instance_size */
	, sizeof (AppDomainInitializer_t1818)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// Metadata Definition System.AssemblyLoadEventHandler
extern TypeInfo AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo;
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler__ctor_m10392_ParameterInfos[] = 
{
	{"object", 0, 134224689, 0, &Object_t_0_0_0},
	{"method", 1, 134224690, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AssemblyLoadEventHandler__ctor_m10392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler__ctor_m10392/* method */
	, &AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler__ctor_m10392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t1822_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t1822_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_Invoke_m10393_ParameterInfos[] = 
{
	{"sender", 0, 134224691, 0, &Object_t_0_0_0},
	{"args", 1, 134224692, 0, &AssemblyLoadEventArgs_t1822_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::Invoke(System.Object,System.AssemblyLoadEventArgs)
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m10393_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_Invoke_m10393/* method */
	, &AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_Invoke_m10393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t1822_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_BeginInvoke_m10394_ParameterInfos[] = 
{
	{"sender", 0, 134224693, 0, &Object_t_0_0_0},
	{"args", 1, 134224694, 0, &AssemblyLoadEventArgs_t1822_0_0_0},
	{"callback", 2, 134224695, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224696, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AssemblyLoadEventHandler::BeginInvoke(System.Object,System.AssemblyLoadEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m10394_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_BeginInvoke_m10394/* method */
	, &AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_BeginInvoke_m10394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_EndInvoke_m10395_ParameterInfos[] = 
{
	{"result", 0, 134224697, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m10395_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_EndInvoke_m10395/* method */
	, &AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t1814_AssemblyLoadEventHandler_EndInvoke_m10395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyLoadEventHandler_t1814_MethodInfos[] =
{
	&AssemblyLoadEventHandler__ctor_m10392_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m10393_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m10394_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m10395_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m10393_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m10394_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m10395_MethodInfo;
static const Il2CppMethodReference AssemblyLoadEventHandler_t1814_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m10393_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m10394_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m10395_MethodInfo,
};
static bool AssemblyLoadEventHandler_t1814_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyLoadEventHandler_t1814_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventHandler_t1814_0_0_0;
extern const Il2CppType AssemblyLoadEventHandler_t1814_1_0_0;
struct AssemblyLoadEventHandler_t1814;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventHandler_t1814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyLoadEventHandler_t1814_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, AssemblyLoadEventHandler_t1814_VTable/* vtableMethods */
	, AssemblyLoadEventHandler_t1814_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventHandler"/* name */
	, "System"/* namespaze */
	, AssemblyLoadEventHandler_t1814_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyLoadEventHandler_t1814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 934/* custom_attributes_cache */
	, &AssemblyLoadEventHandler_t1814_0_0_0/* byval_arg */
	, &AssemblyLoadEventHandler_t1814_1_0_0/* this_arg */
	, &AssemblyLoadEventHandler_t1814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1814/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventHandler_t1814)/* instance_size */
	, sizeof (AssemblyLoadEventHandler_t1814)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Comparison`1
extern TypeInfo Comparison_1_t2020_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Comparison_1_t2020_Il2CppGenericContainer;
extern TypeInfo Comparison_1_t2020_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Comparison_1_t2020_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Comparison_1_t2020_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Comparison_1_t2020_Il2CppGenericParametersArray[1] = 
{
	&Comparison_1_t2020_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Comparison_1_t2020_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Comparison_1_t2020_il2cpp_TypeInfo, 1, 0, Comparison_1_t2020_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Comparison_1_t2020_Comparison_1__ctor_m11056_ParameterInfos[] = 
{
	{"object", 0, 134224698, 0, &Object_t_0_0_0},
	{"method", 1, 134224699, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Comparison`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Comparison_1__ctor_m11056_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Comparison_1_t2020_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2020_Comparison_1__ctor_m11056_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2020_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2020_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2020_gp_0_0_0_0;
static const ParameterInfo Comparison_1_t2020_Comparison_1_Invoke_m11057_ParameterInfos[] = 
{
	{"x", 0, 134224700, 0, &Comparison_1_t2020_gp_0_0_0_0},
	{"y", 1, 134224701, 0, &Comparison_1_t2020_gp_0_0_0_0},
};
// System.Int32 System.Comparison`1::Invoke(T,T)
extern const MethodInfo Comparison_1_Invoke_m11057_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2020_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2020_Comparison_1_Invoke_m11057_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2020_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2020_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Comparison_1_t2020_Comparison_1_BeginInvoke_m11058_ParameterInfos[] = 
{
	{"x", 0, 134224702, 0, &Comparison_1_t2020_gp_0_0_0_0},
	{"y", 1, 134224703, 0, &Comparison_1_t2020_gp_0_0_0_0},
	{"callback", 2, 134224704, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224705, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Comparison`1::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern const MethodInfo Comparison_1_BeginInvoke_m11058_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2020_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2020_Comparison_1_BeginInvoke_m11058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Comparison_1_t2020_Comparison_1_EndInvoke_m11059_ParameterInfos[] = 
{
	{"result", 0, 134224706, 0, &IAsyncResult_t213_0_0_0},
};
// System.Int32 System.Comparison`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Comparison_1_EndInvoke_m11059_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2020_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2020_Comparison_1_EndInvoke_m11059_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Comparison_1_t2020_MethodInfos[] =
{
	&Comparison_1__ctor_m11056_MethodInfo,
	&Comparison_1_Invoke_m11057_MethodInfo,
	&Comparison_1_BeginInvoke_m11058_MethodInfo,
	&Comparison_1_EndInvoke_m11059_MethodInfo,
	NULL
};
extern const MethodInfo Comparison_1_Invoke_m11057_MethodInfo;
extern const MethodInfo Comparison_1_BeginInvoke_m11058_MethodInfo;
extern const MethodInfo Comparison_1_EndInvoke_m11059_MethodInfo;
static const Il2CppMethodReference Comparison_1_t2020_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Comparison_1_Invoke_m11057_MethodInfo,
	&Comparison_1_BeginInvoke_m11058_MethodInfo,
	&Comparison_1_EndInvoke_m11059_MethodInfo,
};
static bool Comparison_1_t2020_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Comparison_1_t2020_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparison_1_t2020_0_0_0;
extern const Il2CppType Comparison_1_t2020_1_0_0;
struct Comparison_1_t2020;
const Il2CppTypeDefinitionMetadata Comparison_1_t2020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Comparison_1_t2020_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Comparison_1_t2020_VTable/* vtableMethods */
	, Comparison_1_t2020_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Comparison_1_t2020_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2020_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Comparison_1_t2020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparison_1_t2020_0_0_0/* byval_arg */
	, &Comparison_1_t2020_1_0_0/* this_arg */
	, &Comparison_1_t2020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Comparison_1_t2020_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Converter`2
extern TypeInfo Converter_2_t2021_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Converter_2_t2021_Il2CppGenericContainer;
extern TypeInfo Converter_2_t2021_gp_TInput_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2021_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2021_Il2CppGenericContainer, NULL, "TInput", 0, 0 };
extern TypeInfo Converter_2_t2021_gp_TOutput_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2021_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2021_Il2CppGenericContainer, NULL, "TOutput", 1, 0 };
static const Il2CppGenericParameter* Converter_2_t2021_Il2CppGenericParametersArray[2] = 
{
	&Converter_2_t2021_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull,
	&Converter_2_t2021_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Converter_2_t2021_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Converter_2_t2021_il2cpp_TypeInfo, 2, 0, Converter_2_t2021_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Converter_2_t2021_Converter_2__ctor_m11060_ParameterInfos[] = 
{
	{"object", 0, 134224707, 0, &Object_t_0_0_0},
	{"method", 1, 134224708, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Converter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Converter_2__ctor_m11060_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Converter_2_t2021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2021_Converter_2__ctor_m11060_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2021_gp_0_0_0_0;
extern const Il2CppType Converter_2_t2021_gp_0_0_0_0;
static const ParameterInfo Converter_2_t2021_Converter_2_Invoke_m11061_ParameterInfos[] = 
{
	{"input", 0, 134224709, 0, &Converter_2_t2021_gp_0_0_0_0},
};
extern const Il2CppType Converter_2_t2021_gp_1_0_0_0;
// TOutput System.Converter`2::Invoke(TInput)
extern const MethodInfo Converter_2_Invoke_m11061_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Converter_2_t2021_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2021_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2021_Converter_2_Invoke_m11061_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2021_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Converter_2_t2021_Converter_2_BeginInvoke_m11062_ParameterInfos[] = 
{
	{"input", 0, 134224710, 0, &Converter_2_t2021_gp_0_0_0_0},
	{"callback", 1, 134224711, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134224712, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Converter`2::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern const MethodInfo Converter_2_BeginInvoke_m11062_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2021_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2021_Converter_2_BeginInvoke_m11062_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Converter_2_t2021_Converter_2_EndInvoke_m11063_ParameterInfos[] = 
{
	{"result", 0, 134224713, 0, &IAsyncResult_t213_0_0_0},
};
// TOutput System.Converter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Converter_2_EndInvoke_m11063_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2021_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2021_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2021_Converter_2_EndInvoke_m11063_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Converter_2_t2021_MethodInfos[] =
{
	&Converter_2__ctor_m11060_MethodInfo,
	&Converter_2_Invoke_m11061_MethodInfo,
	&Converter_2_BeginInvoke_m11062_MethodInfo,
	&Converter_2_EndInvoke_m11063_MethodInfo,
	NULL
};
extern const MethodInfo Converter_2_Invoke_m11061_MethodInfo;
extern const MethodInfo Converter_2_BeginInvoke_m11062_MethodInfo;
extern const MethodInfo Converter_2_EndInvoke_m11063_MethodInfo;
static const Il2CppMethodReference Converter_2_t2021_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Converter_2_Invoke_m11061_MethodInfo,
	&Converter_2_BeginInvoke_m11062_MethodInfo,
	&Converter_2_EndInvoke_m11063_MethodInfo,
};
static bool Converter_2_t2021_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Converter_2_t2021_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Converter_2_t2021_0_0_0;
extern const Il2CppType Converter_2_t2021_1_0_0;
struct Converter_2_t2021;
const Il2CppTypeDefinitionMetadata Converter_2_t2021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Converter_2_t2021_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Converter_2_t2021_VTable/* vtableMethods */
	, Converter_2_t2021_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Converter_2_t2021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Converter`2"/* name */
	, "System"/* namespaze */
	, Converter_2_t2021_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Converter_2_t2021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Converter_2_t2021_0_0_0/* byval_arg */
	, &Converter_2_t2021_1_0_0/* this_arg */
	, &Converter_2_t2021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Converter_2_t2021_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// Metadata Definition System.EventHandler
extern TypeInfo EventHandler_t1816_il2cpp_TypeInfo;
// System.EventHandler
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EventHandler_t1816_EventHandler__ctor_m10396_ParameterInfos[] = 
{
	{"object", 0, 134224714, 0, &Object_t_0_0_0},
	{"method", 1, 134224715, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EventHandler__ctor_m10396_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventHandler__ctor_m10396/* method */
	, &EventHandler_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, EventHandler_t1816_EventHandler__ctor_m10396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1249_0_0_0;
static const ParameterInfo EventHandler_t1816_EventHandler_Invoke_m10397_ParameterInfos[] = 
{
	{"sender", 0, 134224716, 0, &Object_t_0_0_0},
	{"e", 1, 134224717, 0, &EventArgs_t1249_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern const MethodInfo EventHandler_Invoke_m10397_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EventHandler_Invoke_m10397/* method */
	, &EventHandler_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, EventHandler_t1816_EventHandler_Invoke_m10397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1249_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EventHandler_t1816_EventHandler_BeginInvoke_m10398_ParameterInfos[] = 
{
	{"sender", 0, 134224718, 0, &Object_t_0_0_0},
	{"e", 1, 134224719, 0, &EventArgs_t1249_0_0_0},
	{"callback", 2, 134224720, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224721, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.EventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo EventHandler_BeginInvoke_m10398_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EventHandler_BeginInvoke_m10398/* method */
	, &EventHandler_t1816_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, EventHandler_t1816_EventHandler_BeginInvoke_m10398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo EventHandler_t1816_EventHandler_EndInvoke_m10399_ParameterInfos[] = 
{
	{"result", 0, 134224722, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo EventHandler_EndInvoke_m10399_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EventHandler_EndInvoke_m10399/* method */
	, &EventHandler_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, EventHandler_t1816_EventHandler_EndInvoke_m10399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventHandler_t1816_MethodInfos[] =
{
	&EventHandler__ctor_m10396_MethodInfo,
	&EventHandler_Invoke_m10397_MethodInfo,
	&EventHandler_BeginInvoke_m10398_MethodInfo,
	&EventHandler_EndInvoke_m10399_MethodInfo,
	NULL
};
extern const MethodInfo EventHandler_Invoke_m10397_MethodInfo;
extern const MethodInfo EventHandler_BeginInvoke_m10398_MethodInfo;
extern const MethodInfo EventHandler_EndInvoke_m10399_MethodInfo;
static const Il2CppMethodReference EventHandler_t1816_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&EventHandler_Invoke_m10397_MethodInfo,
	&EventHandler_BeginInvoke_m10398_MethodInfo,
	&EventHandler_EndInvoke_m10399_MethodInfo,
};
static bool EventHandler_t1816_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventHandler_t1816_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventHandler_t1816_0_0_0;
extern const Il2CppType EventHandler_t1816_1_0_0;
struct EventHandler_t1816;
const Il2CppTypeDefinitionMetadata EventHandler_t1816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandler_t1816_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, EventHandler_t1816_VTable/* vtableMethods */
	, EventHandler_t1816_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EventHandler_t1816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandler"/* name */
	, "System"/* namespaze */
	, EventHandler_t1816_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EventHandler_t1816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 935/* custom_attributes_cache */
	, &EventHandler_t1816_0_0_0/* byval_arg */
	, &EventHandler_t1816_1_0_0/* this_arg */
	, &EventHandler_t1816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EventHandler_t1816/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandler_t1816)/* instance_size */
	, sizeof (EventHandler_t1816)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Predicate`1
extern TypeInfo Predicate_1_t2022_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Predicate_1_t2022_Il2CppGenericContainer;
extern TypeInfo Predicate_1_t2022_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Predicate_1_t2022_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Predicate_1_t2022_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Predicate_1_t2022_Il2CppGenericParametersArray[1] = 
{
	&Predicate_1_t2022_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Predicate_1_t2022_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Predicate_1_t2022_il2cpp_TypeInfo, 1, 0, Predicate_1_t2022_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Predicate_1_t2022_Predicate_1__ctor_m11064_ParameterInfos[] = 
{
	{"object", 0, 134224723, 0, &Object_t_0_0_0},
	{"method", 1, 134224724, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Predicate`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Predicate_1__ctor_m11064_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Predicate_1_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2022_Predicate_1__ctor_m11064_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2022_gp_0_0_0_0;
extern const Il2CppType Predicate_1_t2022_gp_0_0_0_0;
static const ParameterInfo Predicate_1_t2022_Predicate_1_Invoke_m11065_ParameterInfos[] = 
{
	{"obj", 0, 134224725, 0, &Predicate_1_t2022_gp_0_0_0_0},
};
// System.Boolean System.Predicate`1::Invoke(T)
extern const MethodInfo Predicate_1_Invoke_m11065_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2022_Predicate_1_Invoke_m11065_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2022_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Predicate_1_t2022_Predicate_1_BeginInvoke_m11066_ParameterInfos[] = 
{
	{"obj", 0, 134224726, 0, &Predicate_1_t2022_gp_0_0_0_0},
	{"callback", 1, 134224727, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134224728, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Predicate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Predicate_1_BeginInvoke_m11066_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2022_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2022_Predicate_1_BeginInvoke_m11066_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Predicate_1_t2022_Predicate_1_EndInvoke_m11067_ParameterInfos[] = 
{
	{"result", 0, 134224729, 0, &IAsyncResult_t213_0_0_0},
};
// System.Boolean System.Predicate`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Predicate_1_EndInvoke_m11067_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2022_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2022_Predicate_1_EndInvoke_m11067_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Predicate_1_t2022_MethodInfos[] =
{
	&Predicate_1__ctor_m11064_MethodInfo,
	&Predicate_1_Invoke_m11065_MethodInfo,
	&Predicate_1_BeginInvoke_m11066_MethodInfo,
	&Predicate_1_EndInvoke_m11067_MethodInfo,
	NULL
};
extern const MethodInfo Predicate_1_Invoke_m11065_MethodInfo;
extern const MethodInfo Predicate_1_BeginInvoke_m11066_MethodInfo;
extern const MethodInfo Predicate_1_EndInvoke_m11067_MethodInfo;
static const Il2CppMethodReference Predicate_1_t2022_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Predicate_1_Invoke_m11065_MethodInfo,
	&Predicate_1_BeginInvoke_m11066_MethodInfo,
	&Predicate_1_EndInvoke_m11067_MethodInfo,
};
static bool Predicate_1_t2022_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Predicate_1_t2022_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Predicate_1_t2022_0_0_0;
extern const Il2CppType Predicate_1_t2022_1_0_0;
struct Predicate_1_t2022;
const Il2CppTypeDefinitionMetadata Predicate_1_t2022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Predicate_1_t2022_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Predicate_1_t2022_VTable/* vtableMethods */
	, Predicate_1_t2022_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Predicate_1_t2022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t2022_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Predicate_1_t2022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Predicate_1_t2022_0_0_0/* byval_arg */
	, &Predicate_1_t2022_1_0_0/* this_arg */
	, &Predicate_1_t2022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Predicate_1_t2022_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// Metadata Definition System.ResolveEventHandler
extern TypeInfo ResolveEventHandler_t1815_il2cpp_TypeInfo;
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t1815_ResolveEventHandler__ctor_m10400_ParameterInfos[] = 
{
	{"object", 0, 134224730, 0, &Object_t_0_0_0},
	{"method", 1, 134224731, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ResolveEventHandler__ctor_m10400_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ResolveEventHandler__ctor_m10400/* method */
	, &ResolveEventHandler_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, ResolveEventHandler_t1815_ResolveEventHandler__ctor_m10400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t1870_0_0_0;
static const ParameterInfo ResolveEventHandler_t1815_ResolveEventHandler_Invoke_m10401_ParameterInfos[] = 
{
	{"sender", 0, 134224732, 0, &Object_t_0_0_0},
	{"args", 1, 134224733, 0, &ResolveEventArgs_t1870_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern const MethodInfo ResolveEventHandler_Invoke_m10401_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ResolveEventHandler_Invoke_m10401/* method */
	, &ResolveEventHandler_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t1815_ResolveEventHandler_Invoke_m10401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t1870_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t1815_ResolveEventHandler_BeginInvoke_m10402_ParameterInfos[] = 
{
	{"sender", 0, 134224734, 0, &Object_t_0_0_0},
	{"args", 1, 134224735, 0, &ResolveEventArgs_t1870_0_0_0},
	{"callback", 2, 134224736, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224737, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo ResolveEventHandler_BeginInvoke_m10402_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_BeginInvoke_m10402/* method */
	, &ResolveEventHandler_t1815_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t1815_ResolveEventHandler_BeginInvoke_m10402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo ResolveEventHandler_t1815_ResolveEventHandler_EndInvoke_m10403_ParameterInfos[] = 
{
	{"result", 0, 134224738, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo ResolveEventHandler_EndInvoke_m10403_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_EndInvoke_m10403/* method */
	, &ResolveEventHandler_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t1815_ResolveEventHandler_EndInvoke_m10403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ResolveEventHandler_t1815_MethodInfos[] =
{
	&ResolveEventHandler__ctor_m10400_MethodInfo,
	&ResolveEventHandler_Invoke_m10401_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m10402_MethodInfo,
	&ResolveEventHandler_EndInvoke_m10403_MethodInfo,
	NULL
};
extern const MethodInfo ResolveEventHandler_Invoke_m10401_MethodInfo;
extern const MethodInfo ResolveEventHandler_BeginInvoke_m10402_MethodInfo;
extern const MethodInfo ResolveEventHandler_EndInvoke_m10403_MethodInfo;
static const Il2CppMethodReference ResolveEventHandler_t1815_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&ResolveEventHandler_Invoke_m10401_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m10402_MethodInfo,
	&ResolveEventHandler_EndInvoke_m10403_MethodInfo,
};
static bool ResolveEventHandler_t1815_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ResolveEventHandler_t1815_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHandler_t1815_0_0_0;
extern const Il2CppType ResolveEventHandler_t1815_1_0_0;
struct ResolveEventHandler_t1815;
const Il2CppTypeDefinitionMetadata ResolveEventHandler_t1815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResolveEventHandler_t1815_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, ResolveEventHandler_t1815_VTable/* vtableMethods */
	, ResolveEventHandler_t1815_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventHandler_t1815_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHandler"/* name */
	, "System"/* namespaze */
	, ResolveEventHandler_t1815_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventHandler_t1815_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 936/* custom_attributes_cache */
	, &ResolveEventHandler_t1815_0_0_0/* byval_arg */
	, &ResolveEventHandler_t1815_1_0_0/* this_arg */
	, &ResolveEventHandler_t1815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ResolveEventHandler_t1815/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHandler_t1815)/* instance_size */
	, sizeof (ResolveEventHandler_t1815)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// Metadata Definition System.UnhandledExceptionEventHandler
extern TypeInfo UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo;
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler__ctor_m10404_ParameterInfos[] = 
{
	{"object", 0, 134224739, 0, &Object_t_0_0_0},
	{"method", 1, 134224740, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnhandledExceptionEventHandler__ctor_m10404_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler__ctor_m10404/* method */
	, &UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler__ctor_m10404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t1882_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_Invoke_m10405_ParameterInfos[] = 
{
	{"sender", 0, 134224741, 0, &Object_t_0_0_0},
	{"e", 1, 134224742, 0, &UnhandledExceptionEventArgs_t1882_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::Invoke(System.Object,System.UnhandledExceptionEventArgs)
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m10405_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_Invoke_m10405/* method */
	, &UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_Invoke_m10405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t1882_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_BeginInvoke_m10406_ParameterInfos[] = 
{
	{"sender", 0, 134224743, 0, &Object_t_0_0_0},
	{"e", 1, 134224744, 0, &UnhandledExceptionEventArgs_t1882_0_0_0},
	{"callback", 2, 134224745, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134224746, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.UnhandledExceptionEventHandler::BeginInvoke(System.Object,System.UnhandledExceptionEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m10406_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_BeginInvoke_m10406/* method */
	, &UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_BeginInvoke_m10406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_EndInvoke_m10407_ParameterInfos[] = 
{
	{"result", 0, 134224747, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m10407_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_EndInvoke_m10407/* method */
	, &UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t1817_UnhandledExceptionEventHandler_EndInvoke_m10407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventHandler_t1817_MethodInfos[] =
{
	&UnhandledExceptionEventHandler__ctor_m10404_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m10405_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m10406_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m10407_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m10405_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m10406_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m10407_MethodInfo;
static const Il2CppMethodReference UnhandledExceptionEventHandler_t1817_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m10405_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m10406_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m10407_MethodInfo,
};
static bool UnhandledExceptionEventHandler_t1817_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnhandledExceptionEventHandler_t1817_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventHandler_t1817_0_0_0;
extern const Il2CppType UnhandledExceptionEventHandler_t1817_1_0_0;
struct UnhandledExceptionEventHandler_t1817;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventHandler_t1817_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnhandledExceptionEventHandler_t1817_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnhandledExceptionEventHandler_t1817_VTable/* vtableMethods */
	, UnhandledExceptionEventHandler_t1817_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventHandler"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventHandler_t1817_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventHandler_t1817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 937/* custom_attributes_cache */
	, &UnhandledExceptionEventHandler_t1817_0_0_0/* byval_arg */
	, &UnhandledExceptionEventHandler_t1817_1_0_0/* this_arg */
	, &UnhandledExceptionEventHandler_t1817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1817/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventHandler_t1817)/* instance_size */
	, sizeof (UnhandledExceptionEventHandler_t1817)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
extern TypeInfo U24ArrayTypeU2456_t1888_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2456_t1888_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU2456_t1888_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2456_t1888_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2456_t1888_0_0_0;
extern const Il2CppType U24ArrayTypeU2456_t1888_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1908_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1908_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2456_t1888_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2456_t1888_VTable/* vtableMethods */
	, U24ArrayTypeU2456_t1888_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2456_t1888_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$56"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2456_t1888_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2456_t1888_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2456_t1888_0_0_0/* byval_arg */
	, &U24ArrayTypeU2456_t1888_1_0_0/* this_arg */
	, &U24ArrayTypeU2456_t1888_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2456_t1888_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t1888_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t1888_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2456_t1888)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2456_t1888)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2456_t1888_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
extern TypeInfo U24ArrayTypeU2424_t1889_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2424_t1889_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2424_t1889_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2424_t1889_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2424_t1889_0_0_0;
extern const Il2CppType U24ArrayTypeU2424_t1889_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2424_t1889_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2424_t1889_VTable/* vtableMethods */
	, U24ArrayTypeU2424_t1889_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2424_t1889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$24"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2424_t1889_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2424_t1889_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2424_t1889_0_0_0/* byval_arg */
	, &U24ArrayTypeU2424_t1889_1_0_0/* this_arg */
	, &U24ArrayTypeU2424_t1889_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2424_t1889_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t1889_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t1889_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2424_t1889)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2424_t1889)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2424_t1889_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1890_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t1890_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t1890_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2416_t1890_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1890_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1890_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1890_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2416_t1890_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t1890_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t1890_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t1890_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t1890_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1890_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1890_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1890_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1890_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1890_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1890_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1890)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1890)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1890_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t1891_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24120_t1891_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24120_t1891_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24120_t1891_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t1891_0_0_0;
extern const Il2CppType U24ArrayTypeU24120_t1891_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t1891_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24120_t1891_VTable/* vtableMethods */
	, U24ArrayTypeU24120_t1891_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24120_t1891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24120_t1891_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24120_t1891_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t1891_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t1891_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t1891_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t1891_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t1891_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t1891_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t1891)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t1891)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t1891_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1892_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t1892_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU243132_t1892_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU243132_t1892_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1892_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t1892_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1892_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU243132_t1892_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t1892_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t1892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t1892_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t1892_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1892_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1892_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1892_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1892_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1892_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1892_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1892)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1892)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1892_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1893_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t1893_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t1893_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2420_t1893_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1893_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1893_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1893_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2420_t1893_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t1893_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t1893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t1893_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t1893_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1893_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1893_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1893_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1893_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1893_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1893_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1893)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1893)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1893_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1894_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t1894_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t1894_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2432_t1894_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1894_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1894_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1894_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2432_t1894_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t1894_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t1894_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t1894_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t1894_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1894_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1894_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1894_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1894_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1894_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1894_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1894)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1894)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1894_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1895_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t1895_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t1895_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2448_t1895_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1895_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1895_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1895_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2448_t1895_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t1895_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t1895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t1895_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t1895_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1895_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1895_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1895_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1895_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1895_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1895_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1895)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1895)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1895_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1896_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t1896_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t1896_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2464_t1896_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1896_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1896_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1896_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2464_t1896_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t1896_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t1896_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t1896_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t1896_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1896_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1896_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1896_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1896_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1896_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1896_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1896)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1896)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1896_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1897_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1897_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1897_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2412_t1897_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1897_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1897_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1897_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2412_t1897_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1897_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1897_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1897_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1897_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1897_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1897_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1897_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1897_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1897_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1897)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1897)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1897_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t1898_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t1898_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t1898_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24136_t1898_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t1898_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t1898_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t1898_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24136_t1898_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t1898_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t1898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t1898_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t1898_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t1898_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t1898_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t1898_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t1898_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1898_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1898_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t1898)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t1898)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t1898_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
extern TypeInfo U24ArrayTypeU2472_t1899_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2472_t1899_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2472_t1899_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2472_t1899_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2472_t1899_0_0_0;
extern const Il2CppType U24ArrayTypeU2472_t1899_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2472_t1899_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2472_t1899_VTable/* vtableMethods */
	, U24ArrayTypeU2472_t1899_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2472_t1899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$72"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2472_t1899_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2472_t1899_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2472_t1899_0_0_0/* byval_arg */
	, &U24ArrayTypeU2472_t1899_1_0_0/* this_arg */
	, &U24ArrayTypeU2472_t1899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2472_t1899_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t1899_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t1899_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2472_t1899)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2472_t1899)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2472_t1899_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t1900_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24124_t1900_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24124_t1900_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24124_t1900_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t1900_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t1900_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t1900_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24124_t1900_VTable/* vtableMethods */
	, U24ArrayTypeU24124_t1900_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24124_t1900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24124_t1900_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24124_t1900_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t1900_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t1900_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t1900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t1900_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t1900_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t1900_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t1900)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t1900)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t1900_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
extern TypeInfo U24ArrayTypeU2496_t1901_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2496_t1901_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2496_t1901_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2496_t1901_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2496_t1901_0_0_0;
extern const Il2CppType U24ArrayTypeU2496_t1901_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2496_t1901_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2496_t1901_VTable/* vtableMethods */
	, U24ArrayTypeU2496_t1901_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2496_t1901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$96"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2496_t1901_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2496_t1901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2496_t1901_0_0_0/* byval_arg */
	, &U24ArrayTypeU2496_t1901_1_0_0/* this_arg */
	, &U24ArrayTypeU2496_t1901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2496_t1901_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t1901_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t1901_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2496_t1901)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2496_t1901)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2496_t1901_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
extern TypeInfo U24ArrayTypeU242048_t1902_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU242048_t1902_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU242048_t1902_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU242048_t1902_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU242048_t1902_0_0_0;
extern const Il2CppType U24ArrayTypeU242048_t1902_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU242048_t1902_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU242048_t1902_VTable/* vtableMethods */
	, U24ArrayTypeU242048_t1902_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU242048_t1902_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$2048"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU242048_t1902_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU242048_t1902_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU242048_t1902_0_0_0/* byval_arg */
	, &U24ArrayTypeU242048_t1902_1_0_0/* this_arg */
	, &U24ArrayTypeU242048_t1902_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU242048_t1902_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t1902_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t1902_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU242048_t1902)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU242048_t1902)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU242048_t1902_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1903_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t1903_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t1903_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24256_t1903_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1903_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1903_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1903_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24256_t1903_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t1903_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t1903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t1903_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t1903_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1903_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1903_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1903_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1903_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1903_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1903_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1903)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1903)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1903_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t1904_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU241024_t1904_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU241024_t1904_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU241024_t1904_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t1904_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t1904_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t1904_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU241024_t1904_VTable/* vtableMethods */
	, U24ArrayTypeU241024_t1904_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU241024_t1904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU241024_t1904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU241024_t1904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t1904_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t1904_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t1904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t1904_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t1904_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t1904_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t1904)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t1904)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t1904_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
extern TypeInfo U24ArrayTypeU24640_t1905_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24640_t1905_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24640_t1905_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24640_t1905_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24640_t1905_0_0_0;
extern const Il2CppType U24ArrayTypeU24640_t1905_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24640_t1905_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24640_t1905_VTable/* vtableMethods */
	, U24ArrayTypeU24640_t1905_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24640_t1905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$640"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24640_t1905_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24640_t1905_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24640_t1905_0_0_0/* byval_arg */
	, &U24ArrayTypeU24640_t1905_1_0_0/* this_arg */
	, &U24ArrayTypeU24640_t1905_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24640_t1905_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t1905_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t1905_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24640_t1905)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24640_t1905)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24640_t1905_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t1906_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t1906_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t1906_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24128_t1906_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t1906_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t1906_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t1906_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24128_t1906_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t1906_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t1906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t1906_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t1906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t1906_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t1906_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t1906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t1906_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1906_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1906_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t1906)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t1906)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t1906_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
extern TypeInfo U24ArrayTypeU2452_t1907_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2452_t1907_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2452_t1907_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2452_t1907_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2452_t1907_0_0_0;
extern const Il2CppType U24ArrayTypeU2452_t1907_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2452_t1907_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2452_t1907_VTable/* vtableMethods */
	, U24ArrayTypeU2452_t1907_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2452_t1907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$52"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2452_t1907_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2452_t1907_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2452_t1907_0_0_0/* byval_arg */
	, &U24ArrayTypeU2452_t1907_1_0_0/* this_arg */
	, &U24ArrayTypeU2452_t1907_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2452_t1907_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t1907_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t1907_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2452_t1907)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2452_t1907)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2452_t1907_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1908_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1908_il2cpp_TypeInfo__nestedTypes[20] =
{
	&U24ArrayTypeU2456_t1888_0_0_0,
	&U24ArrayTypeU2424_t1889_0_0_0,
	&U24ArrayTypeU2416_t1890_0_0_0,
	&U24ArrayTypeU24120_t1891_0_0_0,
	&U24ArrayTypeU243132_t1892_0_0_0,
	&U24ArrayTypeU2420_t1893_0_0_0,
	&U24ArrayTypeU2432_t1894_0_0_0,
	&U24ArrayTypeU2448_t1895_0_0_0,
	&U24ArrayTypeU2464_t1896_0_0_0,
	&U24ArrayTypeU2412_t1897_0_0_0,
	&U24ArrayTypeU24136_t1898_0_0_0,
	&U24ArrayTypeU2472_t1899_0_0_0,
	&U24ArrayTypeU24124_t1900_0_0_0,
	&U24ArrayTypeU2496_t1901_0_0_0,
	&U24ArrayTypeU242048_t1902_0_0_0,
	&U24ArrayTypeU24256_t1903_0_0_0,
	&U24ArrayTypeU241024_t1904_0_0_0,
	&U24ArrayTypeU24640_t1905_0_0_0,
	&U24ArrayTypeU24128_t1906_0_0_0,
	&U24ArrayTypeU2452_t1907_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1908_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1908_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1908_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1908;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1908_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1908_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1908_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2549/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1908_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1908_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 938/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1908_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1908_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1908)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1908)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1908_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 20/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
