﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
struct Enumerator_t2784;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2779;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16237_gshared (Enumerator_t2784 * __this, Dictionary_2_t2779 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16237(__this, ___host, method) (( void (*) (Enumerator_t2784 *, Dictionary_2_t2779 *, const MethodInfo*))Enumerator__ctor_m16237_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16238_gshared (Enumerator_t2784 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16238(__this, method) (( Object_t * (*) (Enumerator_t2784 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m16239_gshared (Enumerator_t2784 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16239(__this, method) (( void (*) (Enumerator_t2784 *, const MethodInfo*))Enumerator_Dispose_m16239_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16240_gshared (Enumerator_t2784 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16240(__this, method) (( bool (*) (Enumerator_t2784 *, const MethodInfo*))Enumerator_MoveNext_m16240_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16241_gshared (Enumerator_t2784 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16241(__this, method) (( Object_t * (*) (Enumerator_t2784 *, const MethodInfo*))Enumerator_get_Current_m16241_gshared)(__this, method)
