﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2959;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1083;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18328_gshared (Enumerator_t2959 * __this, Dictionary_2_t1083 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m18328(__this, ___host, method) (( void (*) (Enumerator_t2959 *, Dictionary_2_t1083 *, const MethodInfo*))Enumerator__ctor_m18328_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18329_gshared (Enumerator_t2959 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18329(__this, method) (( Object_t * (*) (Enumerator_t2959 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m18330_gshared (Enumerator_t2959 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18330(__this, method) (( void (*) (Enumerator_t2959 *, const MethodInfo*))Enumerator_Dispose_m18330_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18331_gshared (Enumerator_t2959 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18331(__this, method) (( bool (*) (Enumerator_t2959 *, const MethodInfo*))Enumerator_MoveNext_m18331_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m18332_gshared (Enumerator_t2959 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18332(__this, method) (( int32_t (*) (Enumerator_t2959 *, const MethodInfo*))Enumerator_get_Current_m18332_gshared)(__this, method)
