﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1001;
// System.Net.WebRequest
struct WebRequest_t999;
// System.Uri
struct Uri_t617;

// System.Void System.Net.FtpRequestCreator::.ctor()
extern "C" void FtpRequestCreator__ctor_m3880 (FtpRequestCreator_t1001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t999 * FtpRequestCreator_Create_m3881 (FtpRequestCreator_t1001 * __this, Uri_t617 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
