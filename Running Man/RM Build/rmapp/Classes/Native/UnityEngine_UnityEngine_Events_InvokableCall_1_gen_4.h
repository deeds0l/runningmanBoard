﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t2912;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct  InvokableCall_1_t2911  : public BaseInvokableCall_t682
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Int32>::Delegate
	UnityAction_1_t2912 * ___Delegate_0;
};
