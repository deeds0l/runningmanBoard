﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>
struct KeyValuePair_2_t2643;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t325;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m14224(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2643 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m13149_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m14225(__this, method) (( Object_t * (*) (KeyValuePair_2_t2643 *, const MethodInfo*))KeyValuePair_2_get_Key_m13150_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14226(__this, ___value, method) (( void (*) (KeyValuePair_2_t2643 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13151_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m14227(__this, method) (( int32_t (*) (KeyValuePair_2_t2643 *, const MethodInfo*))KeyValuePair_2_get_Value_m13152_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14228(__this, ___value, method) (( void (*) (KeyValuePair_2_t2643 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m13153_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m14229(__this, method) (( String_t* (*) (KeyValuePair_2_t2643 *, const MethodInfo*))KeyValuePair_2_ToString_m13154_gshared)(__this, method)
