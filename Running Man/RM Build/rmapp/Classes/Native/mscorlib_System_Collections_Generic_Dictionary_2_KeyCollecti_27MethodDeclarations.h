﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
struct Enumerator_t2936;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t2932;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18114_gshared (Enumerator_t2936 * __this, Dictionary_2_t2932 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m18114(__this, ___host, method) (( void (*) (Enumerator_t2936 *, Dictionary_2_t2932 *, const MethodInfo*))Enumerator__ctor_m18114_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18115_gshared (Enumerator_t2936 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18115(__this, method) (( Object_t * (*) (Enumerator_t2936 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18115_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m18116_gshared (Enumerator_t2936 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18116(__this, method) (( void (*) (Enumerator_t2936 *, const MethodInfo*))Enumerator_Dispose_m18116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18117_gshared (Enumerator_t2936 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18117(__this, method) (( bool (*) (Enumerator_t2936 *, const MethodInfo*))Enumerator_MoveNext_m18117_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m18118_gshared (Enumerator_t2936 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18118(__this, method) (( Object_t * (*) (Enumerator_t2936 *, const MethodInfo*))Enumerator_get_Current_m18118_gshared)(__this, method)
