﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int64>
struct InternalEnumerator_1_t2782;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16218_gshared (InternalEnumerator_1_t2782 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16218(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2782 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16218_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16219_gshared (InternalEnumerator_1_t2782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16219(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2782 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16219_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16220_gshared (InternalEnumerator_1_t2782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16220(__this, method) (( void (*) (InternalEnumerator_1_t2782 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16220_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16221_gshared (InternalEnumerator_1_t2782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16221(__this, method) (( bool (*) (InternalEnumerator_1_t2782 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16221_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m16222_gshared (InternalEnumerator_1_t2782 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16222(__this, method) (( int64_t (*) (InternalEnumerator_1_t2782 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16222_gshared)(__this, method)
