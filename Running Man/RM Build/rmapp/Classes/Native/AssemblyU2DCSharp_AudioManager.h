﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1;
// MonoManager`1<AudioManager>
#include "AssemblyU2DCSharp_MonoManager_1_gen.h"
// AudioManager
struct  AudioManager_t2  : public MonoManager_1_t3
{
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> AudioManager::m_listClips
	List_1_t1 * ___m_listClips_3;
};
