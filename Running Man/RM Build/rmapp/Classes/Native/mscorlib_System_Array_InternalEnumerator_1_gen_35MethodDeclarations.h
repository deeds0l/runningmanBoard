﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IntPtr>
struct InternalEnumerator_1_t2750;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15716_gshared (InternalEnumerator_1_t2750 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15716(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2750 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15716_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15717_gshared (InternalEnumerator_1_t2750 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15717(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2750 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15717_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15718_gshared (InternalEnumerator_1_t2750 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15718(__this, method) (( void (*) (InternalEnumerator_1_t2750 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15718_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15719_gshared (InternalEnumerator_1_t2750 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15719(__this, method) (( bool (*) (InternalEnumerator_1_t2750 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15719_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m15720_gshared (InternalEnumerator_1_t2750 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15720(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t2750 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15720_gshared)(__this, method)
