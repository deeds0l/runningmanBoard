﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t30;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Transform>
struct  Comparison_1_t2523  : public MulticastDelegate_t216
{
};
