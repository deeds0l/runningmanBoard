﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2535;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3065;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t2539;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t2543;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2532;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Object>
struct IDictionary_2_t3070;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3071;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t3072;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m12701_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m12701(__this, method) (( void (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2__ctor_m12701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m12703_gshared (Dictionary_2_t2535 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m12703(__this, ___comparer, method) (( void (*) (Dictionary_2_t2535 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m12703_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m12705_gshared (Dictionary_2_t2535 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m12705(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2535 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m12705_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m12707_gshared (Dictionary_2_t2535 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m12707(__this, ___capacity, method) (( void (*) (Dictionary_2_t2535 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m12707_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m12709_gshared (Dictionary_2_t2535 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m12709(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2535 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m12709_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m12711_gshared (Dictionary_2_t2535 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m12711(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2535 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m12711_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m12713_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m12713(__this, method) (( Object_t* (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m12713_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m12715_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m12715(__this, method) (( Object_t* (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m12715_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m12717_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m12717(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m12717_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m12719_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m12719(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2535 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m12719_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m12721_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m12721(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2535 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m12721_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m12723_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m12723(__this, ___key, method) (( bool (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m12723_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m12725_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m12725(__this, ___key, method) (( void (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m12725_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12727_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12727(__this, method) (( bool (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12727_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12729_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12729(__this, method) (( Object_t * (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12731_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12731(__this, method) (( bool (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12731_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12733_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2_t2536  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12733(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2535 *, KeyValuePair_2_t2536 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12733_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12735_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2_t2536  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12735(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2535 *, KeyValuePair_2_t2536 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12735_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12737_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2U5BU5D_t3071* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12737(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2535 *, KeyValuePair_2U5BU5D_t3071*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12737_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12739_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2_t2536  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12739(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2535 *, KeyValuePair_2_t2536 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12739_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m12741_gshared (Dictionary_2_t2535 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m12741(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2535 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m12741_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12743_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12743(__this, method) (( Object_t * (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12743_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12745_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12745(__this, method) (( Object_t* (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12745_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12747_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12747(__this, method) (( Object_t * (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12747_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m12749_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m12749(__this, method) (( int32_t (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_get_Count_m12749_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m12751_gshared (Dictionary_2_t2535 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m12751(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2535 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m12751_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m12753_gshared (Dictionary_2_t2535 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m12753(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2535 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m12753_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m12755_gshared (Dictionary_2_t2535 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m12755(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2535 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m12755_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m12757_gshared (Dictionary_2_t2535 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m12757(__this, ___size, method) (( void (*) (Dictionary_2_t2535 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m12757_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m12759_gshared (Dictionary_2_t2535 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m12759(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2535 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m12759_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2536  Dictionary_2_make_pair_m12761_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m12761(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2536  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m12761_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m12763_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m12763(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m12763_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m12765_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m12765(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m12765_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m12767_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2U5BU5D_t3071* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m12767(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2535 *, KeyValuePair_2U5BU5D_t3071*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m12767_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m12769_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m12769(__this, method) (( void (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_Resize_m12769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m12771_gshared (Dictionary_2_t2535 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m12771(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2535 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m12771_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m12773_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m12773(__this, method) (( void (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_Clear_m12773_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m12775_gshared (Dictionary_2_t2535 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m12775(__this, ___key, method) (( bool (*) (Dictionary_2_t2535 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m12775_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m12777_gshared (Dictionary_2_t2535 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m12777(__this, ___value, method) (( bool (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m12777_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m12779_gshared (Dictionary_2_t2535 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m12779(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2535 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m12779_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m12781_gshared (Dictionary_2_t2535 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m12781(__this, ___sender, method) (( void (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m12781_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m12783_gshared (Dictionary_2_t2535 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m12783(__this, ___key, method) (( bool (*) (Dictionary_2_t2535 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m12783_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m12785_gshared (Dictionary_2_t2535 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m12785(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2535 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m12785_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t2539 * Dictionary_2_get_Keys_m12787_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m12787(__this, method) (( KeyCollection_t2539 * (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_get_Keys_m12787_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t2543 * Dictionary_2_get_Values_m12788_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m12788(__this, method) (( ValueCollection_t2543 * (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_get_Values_m12788_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m12790_gshared (Dictionary_2_t2535 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m12790(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m12790_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m12792_gshared (Dictionary_2_t2535 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m12792(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2535 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m12792_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m12794_gshared (Dictionary_2_t2535 * __this, KeyValuePair_2_t2536  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m12794(__this, ___pair, method) (( bool (*) (Dictionary_2_t2535 *, KeyValuePair_2_t2536 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m12794_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t2541  Dictionary_2_GetEnumerator_m12795_gshared (Dictionary_2_t2535 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m12795(__this, method) (( Enumerator_t2541  (*) (Dictionary_2_t2535 *, const MethodInfo*))Dictionary_2_GetEnumerator_m12795_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m12797_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m12797(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m12797_gshared)(__this /* static, unused */, ___key, ___value, method)
