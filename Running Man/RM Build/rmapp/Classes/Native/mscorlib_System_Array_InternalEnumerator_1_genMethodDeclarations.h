﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t2428;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11345_gshared (InternalEnumerator_1_t2428 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11345(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2428 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared (InternalEnumerator_1_t2428 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2428 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11347_gshared (InternalEnumerator_1_t2428 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11347(__this, method) (( void (*) (InternalEnumerator_1_t2428 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11348_gshared (InternalEnumerator_1_t2428 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11348(__this, method) (( bool (*) (InternalEnumerator_1_t2428 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m11349_gshared (InternalEnumerator_1_t2428 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11349(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2428 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
