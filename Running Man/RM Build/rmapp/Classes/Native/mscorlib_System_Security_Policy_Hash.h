﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1164;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Policy.Hash
struct  Hash_t1763  : public Object_t
{
	// System.Reflection.Assembly System.Security.Policy.Hash::assembly
	Assembly_t1164 * ___assembly_0;
	// System.Byte[] System.Security.Policy.Hash::data
	ByteU5BU5D_t546* ___data_1;
};
