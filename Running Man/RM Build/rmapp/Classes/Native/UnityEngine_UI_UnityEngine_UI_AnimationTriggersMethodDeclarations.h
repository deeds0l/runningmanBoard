﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164;
// System.String
struct String_t;

// System.Void UnityEngine.UI.AnimationTriggers::.ctor()
extern "C" void AnimationTriggers__ctor_m554 (AnimationTriggers_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_normalTrigger()
extern "C" String_t* AnimationTriggers_get_normalTrigger_m555 (AnimationTriggers_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_normalTrigger(System.String)
extern "C" void AnimationTriggers_set_normalTrigger_m556 (AnimationTriggers_t164 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_highlightedTrigger()
extern "C" String_t* AnimationTriggers_get_highlightedTrigger_m557 (AnimationTriggers_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_highlightedTrigger(System.String)
extern "C" void AnimationTriggers_set_highlightedTrigger_m558 (AnimationTriggers_t164 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_pressedTrigger()
extern "C" String_t* AnimationTriggers_get_pressedTrigger_m559 (AnimationTriggers_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_pressedTrigger(System.String)
extern "C" void AnimationTriggers_set_pressedTrigger_m560 (AnimationTriggers_t164 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_disabledTrigger()
extern "C" String_t* AnimationTriggers_get_disabledTrigger_m561 (AnimationTriggers_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_disabledTrigger(System.String)
extern "C" void AnimationTriggers_set_disabledTrigger_m562 (AnimationTriggers_t164 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
