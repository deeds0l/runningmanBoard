﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2865;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t3245;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t2866;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ValueCollection_t2870;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2565;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IDictionary_2_t3246;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t3247;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct IEnumerator_1_t3248;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Dictionary_2__ctor_m17276_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17276(__this, method) (( void (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2__ctor_m17276_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17278_gshared (Dictionary_2_t2865 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17278(__this, ___comparer, method) (( void (*) (Dictionary_2_t2865 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17278_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m17280_gshared (Dictionary_2_t2865 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m17280(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2865 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17280_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17282_gshared (Dictionary_2_t2865 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17282(__this, ___capacity, method) (( void (*) (Dictionary_2_t2865 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17282_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17284_gshared (Dictionary_2_t2865 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17284(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2865 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17284_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17286_gshared (Dictionary_2_t2865 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17286(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2865 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m17286_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17288_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17288(__this, method) (( Object_t* (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17288_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17290_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17290(__this, method) (( Object_t* (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17290_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17292_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17292(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17292_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17294_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17294(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17294_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17296_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17296(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17296_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17298_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17298(__this, ___key, method) (( bool (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17298_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17300_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17300(__this, ___key, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17300_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17302_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17302(__this, method) (( bool (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17302_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17304_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17304(__this, method) (( Object_t * (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17304_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17306_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17306(__this, method) (( bool (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17306_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17308_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2_t2839  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17308(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2865 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17308_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17310_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2_t2839  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17310(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2865 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17310_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17312_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2U5BU5D_t3247* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17312(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2865 *, KeyValuePair_2U5BU5D_t3247*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17312_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17314_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2_t2839  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17314(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2865 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17314_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17316_gshared (Dictionary_2_t2865 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17316(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2865 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17316_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17318_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17318(__this, method) (( Object_t * (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17318_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17320_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17320(__this, method) (( Object_t* (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17320_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17322_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17322(__this, method) (( Object_t * (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17322_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17324_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17324(__this, method) (( int32_t (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_get_Count_m17324_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(TKey)
extern "C" KeyValuePair_2_t2593  Dictionary_2_get_Item_m17326_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17326(__this, ___key, method) (( KeyValuePair_2_t2593  (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m17326_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17328_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17328(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_set_Item_m17328_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17330_gshared (Dictionary_2_t2865 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17330(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2865 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17330_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17332_gshared (Dictionary_2_t2865 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17332(__this, ___size, method) (( void (*) (Dictionary_2_t2865 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17332_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17334_gshared (Dictionary_2_t2865 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17334(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2865 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17334_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2839  Dictionary_2_make_pair_m17336_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17336(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2839  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_make_pair_m17336_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m17338_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17338(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_pick_key_m17338_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_value(TKey,TValue)
extern "C" KeyValuePair_2_t2593  Dictionary_2_pick_value_m17340_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17340(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2593  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_pick_value_m17340_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17342_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2U5BU5D_t3247* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17342(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2865 *, KeyValuePair_2U5BU5D_t3247*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17342_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Resize()
extern "C" void Dictionary_2_Resize_m17344_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17344(__this, method) (( void (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_Resize_m17344_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17346_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17346(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_Add_m17346_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Dictionary_2_Clear_m17348_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17348(__this, method) (( void (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_Clear_m17348_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17350_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17350(__this, ___key, method) (( bool (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m17350_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17352_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17352(__this, ___value, method) (( bool (*) (Dictionary_2_t2865 *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_ContainsValue_m17352_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17354_gshared (Dictionary_2_t2865 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17354(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2865 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m17354_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17356_gshared (Dictionary_2_t2865 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17356(__this, ___sender, method) (( void (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17356_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17358_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17358(__this, ___key, method) (( bool (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m17358_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17360_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, KeyValuePair_2_t2593 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17360(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2865 *, Object_t *, KeyValuePair_2_t2593 *, const MethodInfo*))Dictionary_2_TryGetValue_m17360_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Keys()
extern "C" KeyCollection_t2866 * Dictionary_2_get_Keys_m17362_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17362(__this, method) (( KeyCollection_t2866 * (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_get_Keys_m17362_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Values()
extern "C" ValueCollection_t2870 * Dictionary_2_get_Values_m17364_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17364(__this, method) (( ValueCollection_t2870 * (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_get_Values_m17364_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m17366_gshared (Dictionary_2_t2865 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17366(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17366_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTValue(System.Object)
extern "C" KeyValuePair_2_t2593  Dictionary_2_ToTValue_m17368_gshared (Dictionary_2_t2865 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17368(__this, ___value, method) (( KeyValuePair_2_t2593  (*) (Dictionary_2_t2865 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17368_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17370_gshared (Dictionary_2_t2865 * __this, KeyValuePair_2_t2839  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17370(__this, ___pair, method) (( bool (*) (Dictionary_2_t2865 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17370_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t2868  Dictionary_2_GetEnumerator_m17372_gshared (Dictionary_2_t2865 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17372(__this, method) (( Enumerator_t2868  (*) (Dictionary_2_t2865 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17372_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m17374_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t2593  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17374(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t2593 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17374_gshared)(__this /* static, unused */, ___key, ___value, method)
