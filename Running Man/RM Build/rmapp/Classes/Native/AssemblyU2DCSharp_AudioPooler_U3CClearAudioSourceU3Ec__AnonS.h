﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/<ClearAudioSource>c__AnonStorey8
struct  U3CClearAudioSourceU3Ec__AnonStorey8_t15  : public Object_t
{
	// UnityEngine.GameObject AudioPooler/<ClearAudioSource>c__AnonStorey8::p_audioContainer
	GameObject_t5 * ___p_audioContainer_0;
};
