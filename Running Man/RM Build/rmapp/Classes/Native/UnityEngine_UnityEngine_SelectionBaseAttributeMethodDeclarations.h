﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t419;

// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m2049 (SelectionBaseAttribute_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
