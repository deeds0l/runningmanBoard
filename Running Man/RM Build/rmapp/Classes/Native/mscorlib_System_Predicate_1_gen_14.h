﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct ButtonState_t146;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct  Predicate_1_t2556  : public MulticastDelegate_t216
{
};
