﻿#pragma once
#include <stdint.h>
// AudioPooler
struct AudioPooler_t10;
// UnityEngine.AudioSource
struct AudioSource_t9;
// UnityEngine.AudioClip
struct AudioClip_t20;
// AudioPooler/objectSource
struct objectSource_t8;
// System.Collections.Generic.List`1<AudioPooler/objectSource>
struct List_1_t21;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// AudioPooler
struct  AudioPooler_t10  : public MonoBehaviour_t22
{
	// UnityEngine.AudioSource AudioPooler::m_currentAudioSource
	AudioSource_t9 * ___m_currentAudioSource_3;
	// UnityEngine.AudioClip AudioPooler::m_currentAudioClip
	AudioClip_t20 * ___m_currentAudioClip_4;
	// AudioPooler/objectSource AudioPooler::newObjectSource
	objectSource_t8 * ___newObjectSource_5;
	// System.Int32 AudioPooler::m_audioPreloadCount
	int32_t ___m_audioPreloadCount_6;
	// System.Boolean AudioPooler::m_bstoppedMusic
	bool ___m_bstoppedMusic_7;
	// System.Boolean AudioPooler::m_bstoppedSound
	bool ___m_bstoppedSound_8;
	// System.Collections.Generic.List`1<AudioPooler/objectSource> AudioPooler::m_objectSourceList
	List_1_t21 * ___m_objectSourceList_9;
};
struct AudioPooler_t10_StaticFields{
	// AudioPooler AudioPooler::instance
	AudioPooler_t10 * ___instance_2;
};
