﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t223;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Text>
struct  Comparison_1_t2590  : public MulticastDelegate_t216
{
};
