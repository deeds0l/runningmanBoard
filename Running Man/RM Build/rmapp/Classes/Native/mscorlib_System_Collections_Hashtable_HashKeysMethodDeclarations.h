﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Hashtable/HashKeys
struct HashKeys_t1473;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t975;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;

// System.Void System.Collections.Hashtable/HashKeys::.ctor(System.Collections.Hashtable)
extern "C" void HashKeys__ctor_m7366 (HashKeys_t1473 * __this, Hashtable_t975 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Hashtable/HashKeys::get_Count()
extern "C" int32_t HashKeys_get_Count_m7367 (HashKeys_t1473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Hashtable/HashKeys::get_IsSynchronized()
extern "C" bool HashKeys_get_IsSynchronized_m7368 (HashKeys_t1473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Hashtable/HashKeys::get_SyncRoot()
extern "C" Object_t * HashKeys_get_SyncRoot_m7369 (HashKeys_t1473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable/HashKeys::CopyTo(System.Array,System.Int32)
extern "C" void HashKeys_CopyTo_m7370 (HashKeys_t1473 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Hashtable/HashKeys::GetEnumerator()
extern "C" Object_t * HashKeys_GetEnumerator_m7371 (HashKeys_t1473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
