﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Type>
struct Enumerator_t2892;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t780;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m17634(__this, ___l, method) (( void (*) (Enumerator_t2892 *, List_1_t780 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17635(__this, method) (( Object_t * (*) (Enumerator_t2892 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m17636(__this, method) (( void (*) (Enumerator_t2892 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::VerifyState()
#define Enumerator_VerifyState_m17637(__this, method) (( void (*) (Enumerator_t2892 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m17638(__this, method) (( bool (*) (Enumerator_t2892 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m17639(__this, method) (( Type_t * (*) (Enumerator_t2892 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
