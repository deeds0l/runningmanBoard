﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t474;
// UnityEngine.Object
struct Object_t33;
struct Object_t33_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t697;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m2166 (AssetBundleRequest_t474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t33 * AssetBundleRequest_get_asset_m2167 (AssetBundleRequest_t474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t697* AssetBundleRequest_get_allAssets_m2168 (AssetBundleRequest_t474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
