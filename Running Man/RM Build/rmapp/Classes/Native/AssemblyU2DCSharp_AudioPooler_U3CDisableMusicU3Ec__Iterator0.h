﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Object
struct Object_t;
// AudioPooler
struct AudioPooler_t10;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/<DisableMusic>c__Iterator0
struct  U3CDisableMusicU3Ec__Iterator0_t11  : public Object_t
{
	// UnityEngine.AudioSource AudioPooler/<DisableMusic>c__Iterator0::p_audioSource
	AudioSource_t9 * ___p_audioSource_0;
	// System.Boolean AudioPooler/<DisableMusic>c__Iterator0::p_bisLooping
	bool ___p_bisLooping_1;
	// System.Int32 AudioPooler/<DisableMusic>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// System.Object AudioPooler/<DisableMusic>c__Iterator0::$current
	Object_t * ___U24current_3;
	// UnityEngine.AudioSource AudioPooler/<DisableMusic>c__Iterator0::<$>p_audioSource
	AudioSource_t9 * ___U3CU24U3Ep_audioSource_4;
	// System.Boolean AudioPooler/<DisableMusic>c__Iterator0::<$>p_bisLooping
	bool ___U3CU24U3Ep_bisLooping_5;
	// AudioPooler AudioPooler/<DisableMusic>c__Iterator0::<>f__this
	AudioPooler_t10 * ___U3CU3Ef__this_6;
};
