﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Byte>
struct InvokableCall_1_t2676;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Byte>
struct UnityAction_1_t2675;
// System.Object[]
struct ObjectU5BU5D_t29;

// System.Void UnityEngine.Events.InvokableCall`1<System.Byte>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m14611_gshared (InvokableCall_1_t2676 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m14611(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2676 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m14611_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Byte>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m14612_gshared (InvokableCall_1_t2676 * __this, UnityAction_1_t2675 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m14612(__this, ___callback, method) (( void (*) (InvokableCall_1_t2676 *, UnityAction_1_t2675 *, const MethodInfo*))InvokableCall_1__ctor_m14612_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Byte>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m14613_gshared (InvokableCall_1_t2676 * __this, ObjectU5BU5D_t29* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m14613(__this, ___args, method) (( void (*) (InvokableCall_1_t2676 *, ObjectU5BU5D_t29*, const MethodInfo*))InvokableCall_1_Invoke_m14613_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Byte>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m14614_gshared (InvokableCall_1_t2676 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m14614(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2676 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m14614_gshared)(__this, ___targetObj, ___method, method)
