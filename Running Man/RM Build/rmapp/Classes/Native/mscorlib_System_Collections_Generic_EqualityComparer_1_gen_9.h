﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct EqualityComparer_1_t3019;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.DateTime>
struct  EqualityComparer_1_t3019  : public Object_t
{
};
struct EqualityComparer_1_t3019_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.DateTime>::_default
	EqualityComparer_1_t3019 * ____default_0;
};
