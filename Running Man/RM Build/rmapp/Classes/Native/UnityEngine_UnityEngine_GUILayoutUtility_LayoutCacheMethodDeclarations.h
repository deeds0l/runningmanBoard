﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t511;

// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern "C" void LayoutCache__ctor_m2311 (LayoutCache_t511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
