﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA384
struct  HMACSHA384_t1729  : public HMAC_t1346
{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy
	bool ___legacy_11;
};
struct HMACSHA384_t1729_StaticFields{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy_mode
	bool ___legacy_mode_10;
};
