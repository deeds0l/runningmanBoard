﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t2768;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t590;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16013_gshared (Enumerator_t2768 * __this, List_1_t590 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16013(__this, ___l, method) (( void (*) (Enumerator_t2768 *, List_1_t590 *, const MethodInfo*))Enumerator__ctor_m16013_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16014_gshared (Enumerator_t2768 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16014(__this, method) (( Object_t * (*) (Enumerator_t2768 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16014_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m16015_gshared (Enumerator_t2768 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16015(__this, method) (( void (*) (Enumerator_t2768 *, const MethodInfo*))Enumerator_Dispose_m16015_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m16016_gshared (Enumerator_t2768 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16016(__this, method) (( void (*) (Enumerator_t2768 *, const MethodInfo*))Enumerator_VerifyState_m16016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16017_gshared (Enumerator_t2768 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16017(__this, method) (( bool (*) (Enumerator_t2768 *, const MethodInfo*))Enumerator_MoveNext_m16017_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t375  Enumerator_get_Current_m16018_gshared (Enumerator_t2768 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16018(__this, method) (( UILineInfo_t375  (*) (Enumerator_t2768 *, const MethodInfo*))Enumerator_get_Current_m16018_gshared)(__this, method)
