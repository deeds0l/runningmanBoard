﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<System.Security.Policy.StrongName>
struct Predicate_1_t3014;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t1764;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<System.Security.Policy.StrongName>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m18676(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3014 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m11435_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Security.Policy.StrongName>::Invoke(T)
#define Predicate_1_Invoke_m18677(__this, ___obj, method) (( bool (*) (Predicate_1_t3014 *, StrongName_t1764 *, const MethodInfo*))Predicate_1_Invoke_m11436_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Security.Policy.StrongName>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m18678(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3014 *, StrongName_t1764 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m11437_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Security.Policy.StrongName>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m18679(__this, ___result, method) (( bool (*) (Predicate_1_t3014 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m11438_gshared)(__this, ___result, method)
