﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RIPEMD160
struct RIPEMD160_t1736;

// System.Void System.Security.Cryptography.RIPEMD160::.ctor()
extern "C" void RIPEMD160__ctor_m9032 (RIPEMD160_t1736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
