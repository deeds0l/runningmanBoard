﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t398;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
extern "C" void AssemblyConfigurationAttribute__ctor_m1980 (AssemblyConfigurationAttribute_t398 * __this, String_t* ___configuration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
