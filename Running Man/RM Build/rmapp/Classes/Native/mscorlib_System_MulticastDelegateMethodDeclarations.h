﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t216;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t1911;
// System.Delegate
struct Delegate_t361;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m2105 (MulticastDelegate_t216 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m2103 (MulticastDelegate_t216 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m2104 (MulticastDelegate_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t1911* MulticastDelegate_GetInvocationList_m2107 (MulticastDelegate_t216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t361 * MulticastDelegate_CombineImpl_m2108 (MulticastDelegate_t216 * __this, Delegate_t361 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m6482 (MulticastDelegate_t216 * __this, MulticastDelegate_t216 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t216 * MulticastDelegate_KPM_m6483 (Object_t * __this /* static, unused */, MulticastDelegate_t216 * ___needle, MulticastDelegate_t216 * ___haystack, MulticastDelegate_t216 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t361 * MulticastDelegate_RemoveImpl_m2109 (MulticastDelegate_t216 * __this, Delegate_t361 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
