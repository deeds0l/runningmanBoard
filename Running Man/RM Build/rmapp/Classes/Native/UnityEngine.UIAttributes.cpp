﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t398_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t400_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo_var;
void g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		AssemblyConfigurationAttribute_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(362);
		AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		AssemblyProductAttribute_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(365);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(367);
		RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(369);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 11;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("d4f464c7-9b15-460d-b4bc-2cacd1c1df73"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t397 * tmp;
		tmp = (AssemblyDescriptionAttribute_t397 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m1979(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t398 * tmp;
		tmp = (AssemblyConfigurationAttribute_t398 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t398_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m1980(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t399 * tmp;
		tmp = (AssemblyCompanyAttribute_t399 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m1981(tmp, il2cpp_codegen_string_new_wrapper("Microsoft"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t400 * tmp;
		tmp = (AssemblyProductAttribute_t400 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t400_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m1982(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t401 * tmp;
		tmp = (AssemblyCopyrightAttribute_t401 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m1983(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Microsoft 2013"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t403 * tmp;
		tmp = (AssemblyTitleAttribute_t403 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m1985(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t56 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t56 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m152(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153(tmp, true, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t404 * tmp;
		tmp = (AssemblyFileVersionAttribute_t404 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m1986(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t405 * tmp;
		tmp = (AssemblyTrademarkAttribute_t405 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m1987(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void EventHandle_t99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Event System"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_m_FirstSelected(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Selected"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_m_sendNavigationEvents(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_m_DragThreshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_get_current_m269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_set_current_m270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_t104____lastSelectedGameObject_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("lastSelectedGameObject is no longer supported"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void EventTrigger_t110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Event Trigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void EventTrigger_t110_CustomAttributesCacheGenerator_m_Delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("delegates"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void EventTrigger_t110_CustomAttributesCacheGenerator_delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2009(tmp, il2cpp_codegen_string_new_wrapper("Please use triggers instead (UnityUpgradable)"), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ExecuteEvents_t132_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ExecuteEvents_t132_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m395(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_position_m407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_position_m408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_delta_m409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_delta_m410(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_button_m427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_button_m428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_t143____worldPosition_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldPosition or pointerPressRaycast.worldPosition"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_t143____worldNormal_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldNormal or pointerPressRaycast.worldNormal"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
extern const Il2CppType* EventSystem_t104_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void BaseInputModule_t101_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventSystem_t104_0_0_0_var = il2cpp_codegen_type_from_index(151);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(EventSystem_t104_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Standalone Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_HorizontalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_VerticalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_SubmitButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_CancelButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_InputActionsPerSecond(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_RepeatDelay(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void StandaloneInputModule_t152_CustomAttributesCacheGenerator_StandaloneInputModule_t152____inputMode_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2009(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void InputMode_t151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2009(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void TouchInputModule_t153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Touch Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void TouchInputModule_t153_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void BaseRaycaster_t136_CustomAttributesCacheGenerator_BaseRaycaster_t136____priority_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2009(tmp, il2cpp_codegen_string_new_wrapper("Please use sortOrderPriority and renderOrderPriority"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t156_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Physics2DRaycaster_t154_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t156_0_0_0_var = il2cpp_codegen_type_from_index(203);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(Camera_t156_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics 2D Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t156_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t156_0_0_0_var = il2cpp_codegen_type_from_index(203);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(Camera_t156_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t155_CustomAttributesCacheGenerator_m_EventMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t155_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t155_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m537(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void TweenRunner_1_t413_CustomAttributesCacheGenerator_TweenRunner_1_Start_m2029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2033(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m2036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m2037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void AnimationTriggers_t164_CustomAttributesCacheGenerator_m_NormalTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("normalTrigger"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AnimationTriggers_t164_CustomAttributesCacheGenerator_m_HighlightedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("highlightedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedTrigger"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AnimationTriggers_t164_CustomAttributesCacheGenerator_m_PressedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("pressedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AnimationTriggers_t164_CustomAttributesCacheGenerator_m_DisabledTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("disabledTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Button_t167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Button"), 30, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Button_t167_CustomAttributesCacheGenerator_m_OnClick(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("onClick"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void Button_t167_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m568(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m569(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m593(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_NormalColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("normalColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_HighlightedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("highlightedColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_PressedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("pressedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_DisabledColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("disabledColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_ColorMultiplier(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 1.0f, 5.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void ColorBlock_t175_CustomAttributesCacheGenerator_m_FadeDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("fadeDuration"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("font"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_FontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("fontSize"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_FontStyle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("fontStyle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_BestFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_MinSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_MaxSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_Alignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("alignment"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_RichText(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("richText"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_HorizontalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_VerticalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void FontData_t177_CustomAttributesCacheGenerator_m_LineSpacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern const Il2CppType* CanvasRenderer_t184_0_0_0_var;
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t417_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasRenderer_t184_0_0_0_var = il2cpp_codegen_type_from_index(253);
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		DisallowMultipleComponent_t417_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(376);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_t184_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t417 * tmp;
		tmp = (DisallowMultipleComponent_t417 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t417_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2044(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_m_Material(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Mat"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Graphic_t188_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern const Il2CppType* Canvas_t48_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t48_0_0_0_var = il2cpp_codegen_type_from_index(30);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper("Event/Graphic Raycaster"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(Canvas_t48_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("ignoreReversedGraphics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_BlockingObjects(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("blockingObjects"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_BlockingMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void GraphicRaycaster_t51_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Image"), 10, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Frame"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_PreserveAspect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_FillCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_FillMethod(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_FillAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_FillClockwise(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Image_t203_CustomAttributesCacheGenerator_m_FillOrigin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Input Field"), 31, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_TextComponent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("text"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_Placeholder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_ContentType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_InputType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("inputType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_AsteriskChar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("asteriskChar"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_KeyboardType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("keyboardType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_LineType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_HideMobileInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("hideMobileInput"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_CharacterValidation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("validation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_CharacterLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("characterLimit"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_EndEdit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("onSubmit"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_OnSubmit"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_OnValueChange(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("onValueChange"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_OnValidateInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("onValidateInput"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("selectionColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("mValue"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_m_CaretBlinkRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 4.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_InputField_CaretBlink_m823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void InputField_t217_CustomAttributesCacheGenerator_InputField_t217_InputField_SetToCustomIfContentTypeIsNot_m891_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Navigation_t232_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnUp(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("selectOnUp"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnDown(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("selectOnDown"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnLeft(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("selectOnLeft"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnRight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("selectOnRight"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void Mode_t231_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void RawImage_t234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Raw Image"), 12, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void RawImage_t234_CustomAttributesCacheGenerator_m_Texture(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Tex"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void RawImage_t234_CustomAttributesCacheGenerator_m_UVRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Scrollbar"), 32, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_Size(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_NumberOfSteps(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 11.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern TypeInfo* SpaceAttribute_t418_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t418 * tmp;
		tmp = (SpaceAttribute_t418 *)il2cpp_codegen_object_new (SpaceAttribute_t418_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2048(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void Scrollbar_t239_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m966(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m936(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t419_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		SelectionBaseAttribute_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Scroll Rect"), 33, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t419 * tmp;
		tmp = (SelectionBaseAttribute_t419 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t419_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m2049(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_Horizontal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_Vertical(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_MovementType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_Elasticity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_Inertia(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_DecelerationRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_ScrollSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_HorizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_VerticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ScrollRect_t246_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DisallowMultipleComponent_t417_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t419_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponent_t417_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(376);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		SelectionBaseAttribute_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DisallowMultipleComponent_t417 * tmp;
		tmp = (DisallowMultipleComponent_t417 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t417_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2044(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t419 * tmp;
		tmp = (SelectionBaseAttribute_t419 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t419_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m2049(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Selectable"), 70, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_Navigation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("navigation"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_Transition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("transition"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("colors"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_SpriteState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("spriteState"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_AnimationTriggers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("animationTriggers"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_Interactable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("Can the Selectable be interacted with?"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_m_TargetGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_HighlightGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("highlightGraphic"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m1051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m1052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m1053(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m1054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m1055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m1056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Selectable_t169_CustomAttributesCacheGenerator_Selectable_IsPressed_m1082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2009(tmp, il2cpp_codegen_string_new_wrapper("Is Pressed no longer requires eventData"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Slider"), 34, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_FillRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* SpaceAttribute_t418_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		SpaceAttribute_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SpaceAttribute_t418 * tmp;
		tmp = (SpaceAttribute_t418 *)il2cpp_codegen_object_new (SpaceAttribute_t418_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2048(tmp, 6.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_WholeNumbers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SpaceAttribute_t418_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Slider_t257_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t418_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t418 * tmp;
		tmp = (SpaceAttribute_t418 *)il2cpp_codegen_object_new (SpaceAttribute_t418_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2048(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void SpriteState_t252_CustomAttributesCacheGenerator_m_HighlightedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("highlightedSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void SpriteState_t252_CustomAttributesCacheGenerator_m_PressedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("pressedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void SpriteState_t252_CustomAttributesCacheGenerator_m_DisabledSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("disabledSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Text_t223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Text"), 11, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Text_t223_CustomAttributesCacheGenerator_m_FontData(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TextAreaAttribute_t421_il2cpp_TypeInfo_var;
void Text_t223_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TextAreaAttribute_t421_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(380);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TextAreaAttribute_t421 * tmp;
		tmp = (TextAreaAttribute_t421 *)il2cpp_codegen_object_new (TextAreaAttribute_t421_il2cpp_TypeInfo_var);
		TextAreaAttribute__ctor_m2053(tmp, 3, 10, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Toggle_t265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle"), 35, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Toggle_t265_CustomAttributesCacheGenerator_m_Group(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void Toggle_t265_CustomAttributesCacheGenerator_m_IsOn(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_IsActive"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("Is the toggle currently on or off?"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle Group"), 36, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator_m_AllowSwitchOff(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ToggleGroup_t264_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m1226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void AspectRatioFitter_t270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Aspect Ratio Fitter"), 142, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AspectRatioFitter_t270_CustomAttributesCacheGenerator_m_AspectMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AspectRatioFitter_t270_CustomAttributesCacheGenerator_m_AspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Canvas_t48_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t48_0_0_0_var = il2cpp_codegen_type_from_index(30);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Canvas Scaler"), 101, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(Canvas_t48_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_UiScaleMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("Determines how UI elements in the Canvas are scaled."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_ScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("Scales all UI elements in the Canvas by this factor."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_ReferenceResolution(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_ScreenMatchMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t415_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_MatchWidthOrHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t415_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t415 * tmp;
		tmp = (RangeAttribute_t415 *)il2cpp_codegen_object_new (RangeAttribute_t415_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2042(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("Determines if the scaling is using the width or height as reference, or a mix in between."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_PhysicalUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("The physical unit to specify positions and sizes in."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_FallbackScreenDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("The DPI to assume if the screen DPI is not known."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_DefaultSpriteDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t420_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void CanvasScaler_t50_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t420_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t420 * tmp;
		tmp = (TooltipAttribute_t420 *)il2cpp_codegen_object_new (TooltipAttribute_t420_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2050(tmp, il2cpp_codegen_string_new_wrapper("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
void ContentSizeFitter_t275_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Content Size Fitter"), 141, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ContentSizeFitter_t275_CustomAttributesCacheGenerator_m_HorizontalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ContentSizeFitter_t275_CustomAttributesCacheGenerator_m_VerticalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Grid Layout Group"), 152, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void HorizontalLayoutGroup_t281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Horizontal Layout Group"), 150, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_ChildForceExpandWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_ChildForceExpandHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Layout Element"), 140, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_IgnoreLayout(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_MinWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_PreferredWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_PreferredHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_FlexibleWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutElement_t283_CustomAttributesCacheGenerator_m_FlexibleHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t183_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t417_il2cpp_TypeInfo_var;
void LayoutGroup_t280_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t183_0_0_0_var = il2cpp_codegen_type_from_index(251);
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		DisallowMultipleComponent_t417_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(376);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(RectTransform_t183_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t417 * tmp;
		tmp = (DisallowMultipleComponent_t417 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t417_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2044(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutGroup_t280_CustomAttributesCacheGenerator_m_Padding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void LayoutGroup_t280_CustomAttributesCacheGenerator_m_ChildAlignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Alignment"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m1381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m1382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m1383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m1384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m1397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m1401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void VerticalLayoutGroup_t291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("Layout/Vertical Layout Group"), 151, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Mask_t292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Mask"), 13, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void Mask_t292_CustomAttributesCacheGenerator_m_ShowMaskGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_ShowGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void IndexedSet_1_t422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasListPool_t295_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CanvasListPool_t295_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ComponentListPool_t298_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ComponentListPool_t298_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ObjectPool_1_t424_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ObjectPool_1_t424_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m2090(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ObjectPool_1_t424_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m2091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
void BaseVertexEffect_t299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Outline_t300_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Outline"), 15, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void PositionAsUV1_t302_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Position As UV1"), 16, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void Shadow_t301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2038(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Shadow"), 14, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Shadow_t301_CustomAttributesCacheGenerator_m_EffectColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Shadow_t301_CustomAttributesCacheGenerator_m_EffectDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void Shadow_t301_CustomAttributesCacheGenerator_m_UseGraphicAlpha(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_UI_Assembly_AttributeGenerators[350] = 
{
	NULL,
	g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator,
	EventHandle_t99_CustomAttributesCacheGenerator,
	EventSystem_t104_CustomAttributesCacheGenerator,
	EventSystem_t104_CustomAttributesCacheGenerator_m_FirstSelected,
	EventSystem_t104_CustomAttributesCacheGenerator_m_sendNavigationEvents,
	EventSystem_t104_CustomAttributesCacheGenerator_m_DragThreshold,
	EventSystem_t104_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField,
	EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_get_current_m269,
	EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_set_current_m270,
	EventSystem_t104_CustomAttributesCacheGenerator_EventSystem_t104____lastSelectedGameObject_PropertyInfo,
	EventTrigger_t110_CustomAttributesCacheGenerator,
	EventTrigger_t110_CustomAttributesCacheGenerator_m_Delegates,
	EventTrigger_t110_CustomAttributesCacheGenerator_delegates,
	ExecuteEvents_t132_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13,
	ExecuteEvents_t132_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m354,
	AxisEventData_t139_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField,
	AxisEventData_t139_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField,
	AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m379,
	AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m380,
	AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m381,
	AxisEventData_t139_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m382,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m391,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m392,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m393,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m394,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m395,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m396,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m397,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m398,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m399,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m400,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m401,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m402,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m403,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m404,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m405,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m406,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_position_m407,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_position_m408,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_delta_m409,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_delta_m410,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m411,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m412,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m413,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m414,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m415,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m416,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m417,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m418,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m419,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m420,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m421,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m422,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m423,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m424,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m425,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m426,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_get_button_m427,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_set_button_m428,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_t143____worldPosition_PropertyInfo,
	PointerEventData_t143_CustomAttributesCacheGenerator_PointerEventData_t143____worldNormal_PropertyInfo,
	BaseInputModule_t101_CustomAttributesCacheGenerator,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_HorizontalAxis,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_VerticalAxis,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_SubmitButton,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_CancelButton,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_InputActionsPerSecond,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_RepeatDelay,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice,
	StandaloneInputModule_t152_CustomAttributesCacheGenerator_StandaloneInputModule_t152____inputMode_PropertyInfo,
	InputMode_t151_CustomAttributesCacheGenerator,
	TouchInputModule_t153_CustomAttributesCacheGenerator,
	TouchInputModule_t153_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone,
	BaseRaycaster_t136_CustomAttributesCacheGenerator_BaseRaycaster_t136____priority_PropertyInfo,
	Physics2DRaycaster_t154_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t155_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t155_CustomAttributesCacheGenerator_m_EventMask,
	PhysicsRaycaster_t155_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	PhysicsRaycaster_t155_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m537,
	TweenRunner_1_t413_CustomAttributesCacheGenerator_TweenRunner_1_Start_m2029,
	U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2033,
	U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2034,
	U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m2036,
	U3CStartU3Ec__Iterator0_t414_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m2037,
	AnimationTriggers_t164_CustomAttributesCacheGenerator_m_NormalTrigger,
	AnimationTriggers_t164_CustomAttributesCacheGenerator_m_HighlightedTrigger,
	AnimationTriggers_t164_CustomAttributesCacheGenerator_m_PressedTrigger,
	AnimationTriggers_t164_CustomAttributesCacheGenerator_m_DisabledTrigger,
	Button_t167_CustomAttributesCacheGenerator,
	Button_t167_CustomAttributesCacheGenerator_m_OnClick,
	Button_t167_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m576,
	U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator,
	U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m565,
	U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m566,
	U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m568,
	U3COnFinishSubmitU3Ec__Iterator1_t168_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m569,
	CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m593,
	CanvasUpdateRegistry_t171_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m594,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_NormalColor,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_HighlightedColor,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_PressedColor,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_DisabledColor,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_ColorMultiplier,
	ColorBlock_t175_CustomAttributesCacheGenerator_m_FadeDuration,
	FontData_t177_CustomAttributesCacheGenerator_m_Font,
	FontData_t177_CustomAttributesCacheGenerator_m_FontSize,
	FontData_t177_CustomAttributesCacheGenerator_m_FontStyle,
	FontData_t177_CustomAttributesCacheGenerator_m_BestFit,
	FontData_t177_CustomAttributesCacheGenerator_m_MinSize,
	FontData_t177_CustomAttributesCacheGenerator_m_MaxSize,
	FontData_t177_CustomAttributesCacheGenerator_m_Alignment,
	FontData_t177_CustomAttributesCacheGenerator_m_RichText,
	FontData_t177_CustomAttributesCacheGenerator_m_HorizontalOverflow,
	FontData_t177_CustomAttributesCacheGenerator_m_VerticalOverflow,
	FontData_t177_CustomAttributesCacheGenerator_m_LineSpacing,
	Graphic_t188_CustomAttributesCacheGenerator,
	Graphic_t188_CustomAttributesCacheGenerator_m_Material,
	Graphic_t188_CustomAttributesCacheGenerator_m_Color,
	Graphic_t188_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE,
	Graphic_t188_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF,
	Graphic_t188_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m683,
	Graphic_t188_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m684,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_BlockingObjects,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator_m_BlockingMask,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	GraphicRaycaster_t51_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m697,
	Image_t203_CustomAttributesCacheGenerator,
	Image_t203_CustomAttributesCacheGenerator_m_Sprite,
	Image_t203_CustomAttributesCacheGenerator_m_Type,
	Image_t203_CustomAttributesCacheGenerator_m_PreserveAspect,
	Image_t203_CustomAttributesCacheGenerator_m_FillCenter,
	Image_t203_CustomAttributesCacheGenerator_m_FillMethod,
	Image_t203_CustomAttributesCacheGenerator_m_FillAmount,
	Image_t203_CustomAttributesCacheGenerator_m_FillClockwise,
	Image_t203_CustomAttributesCacheGenerator_m_FillOrigin,
	InputField_t217_CustomAttributesCacheGenerator,
	InputField_t217_CustomAttributesCacheGenerator_m_TextComponent,
	InputField_t217_CustomAttributesCacheGenerator_m_Placeholder,
	InputField_t217_CustomAttributesCacheGenerator_m_ContentType,
	InputField_t217_CustomAttributesCacheGenerator_m_InputType,
	InputField_t217_CustomAttributesCacheGenerator_m_AsteriskChar,
	InputField_t217_CustomAttributesCacheGenerator_m_KeyboardType,
	InputField_t217_CustomAttributesCacheGenerator_m_LineType,
	InputField_t217_CustomAttributesCacheGenerator_m_HideMobileInput,
	InputField_t217_CustomAttributesCacheGenerator_m_CharacterValidation,
	InputField_t217_CustomAttributesCacheGenerator_m_CharacterLimit,
	InputField_t217_CustomAttributesCacheGenerator_m_EndEdit,
	InputField_t217_CustomAttributesCacheGenerator_m_OnValueChange,
	InputField_t217_CustomAttributesCacheGenerator_m_OnValidateInput,
	InputField_t217_CustomAttributesCacheGenerator_m_SelectionColor,
	InputField_t217_CustomAttributesCacheGenerator_m_Text,
	InputField_t217_CustomAttributesCacheGenerator_m_CaretBlinkRate,
	InputField_t217_CustomAttributesCacheGenerator_InputField_CaretBlink_m823,
	InputField_t217_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m840,
	InputField_t217_CustomAttributesCacheGenerator_InputField_t217_InputField_SetToCustomIfContentTypeIsNot_m891_Arg0_ParameterInfo,
	U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator,
	U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m760,
	U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m761,
	U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m763,
	U3CCaretBlinkU3Ec__Iterator2_t218_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m764,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m766,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m767,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m769,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t219_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m770,
	Navigation_t232_CustomAttributesCacheGenerator_m_Mode,
	Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnUp,
	Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnDown,
	Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnLeft,
	Navigation_t232_CustomAttributesCacheGenerator_m_SelectOnRight,
	Mode_t231_CustomAttributesCacheGenerator,
	RawImage_t234_CustomAttributesCacheGenerator,
	RawImage_t234_CustomAttributesCacheGenerator_m_Texture,
	RawImage_t234_CustomAttributesCacheGenerator_m_UVRect,
	Scrollbar_t239_CustomAttributesCacheGenerator,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_HandleRect,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_Direction,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_Value,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_Size,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_NumberOfSteps,
	Scrollbar_t239_CustomAttributesCacheGenerator_m_OnValueChanged,
	Scrollbar_t239_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m966,
	U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator,
	U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m932,
	U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m933,
	U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m935,
	U3CClickRepeatU3Ec__Iterator4_t240_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m936,
	ScrollRect_t246_CustomAttributesCacheGenerator,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_Content,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_Horizontal,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_Vertical,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_MovementType,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_Elasticity,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_Inertia,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_DecelerationRate,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_ScrollSensitivity,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_HorizontalScrollbar,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_VerticalScrollbar,
	ScrollRect_t246_CustomAttributesCacheGenerator_m_OnValueChanged,
	Selectable_t169_CustomAttributesCacheGenerator,
	Selectable_t169_CustomAttributesCacheGenerator_m_Navigation,
	Selectable_t169_CustomAttributesCacheGenerator_m_Transition,
	Selectable_t169_CustomAttributesCacheGenerator_m_Colors,
	Selectable_t169_CustomAttributesCacheGenerator_m_SpriteState,
	Selectable_t169_CustomAttributesCacheGenerator_m_AnimationTriggers,
	Selectable_t169_CustomAttributesCacheGenerator_m_Interactable,
	Selectable_t169_CustomAttributesCacheGenerator_m_TargetGraphic,
	Selectable_t169_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField,
	Selectable_t169_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField,
	Selectable_t169_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m1051,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m1052,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m1053,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m1054,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m1055,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m1056,
	Selectable_t169_CustomAttributesCacheGenerator_Selectable_IsPressed_m1082,
	Slider_t257_CustomAttributesCacheGenerator,
	Slider_t257_CustomAttributesCacheGenerator_m_FillRect,
	Slider_t257_CustomAttributesCacheGenerator_m_HandleRect,
	Slider_t257_CustomAttributesCacheGenerator_m_Direction,
	Slider_t257_CustomAttributesCacheGenerator_m_MinValue,
	Slider_t257_CustomAttributesCacheGenerator_m_MaxValue,
	Slider_t257_CustomAttributesCacheGenerator_m_WholeNumbers,
	Slider_t257_CustomAttributesCacheGenerator_m_Value,
	Slider_t257_CustomAttributesCacheGenerator_m_OnValueChanged,
	SpriteState_t252_CustomAttributesCacheGenerator_m_HighlightedSprite,
	SpriteState_t252_CustomAttributesCacheGenerator_m_PressedSprite,
	SpriteState_t252_CustomAttributesCacheGenerator_m_DisabledSprite,
	Text_t223_CustomAttributesCacheGenerator,
	Text_t223_CustomAttributesCacheGenerator_m_FontData,
	Text_t223_CustomAttributesCacheGenerator_m_Text,
	Toggle_t265_CustomAttributesCacheGenerator,
	Toggle_t265_CustomAttributesCacheGenerator_m_Group,
	Toggle_t265_CustomAttributesCacheGenerator_m_IsOn,
	ToggleGroup_t264_CustomAttributesCacheGenerator,
	ToggleGroup_t264_CustomAttributesCacheGenerator_m_AllowSwitchOff,
	ToggleGroup_t264_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ToggleGroup_t264_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	ToggleGroup_t264_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225,
	ToggleGroup_t264_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m1226,
	AspectRatioFitter_t270_CustomAttributesCacheGenerator,
	AspectRatioFitter_t270_CustomAttributesCacheGenerator_m_AspectMode,
	AspectRatioFitter_t270_CustomAttributesCacheGenerator_m_AspectRatio,
	CanvasScaler_t50_CustomAttributesCacheGenerator,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_UiScaleMode,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_ScaleFactor,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_ReferenceResolution,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_ScreenMatchMode,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_MatchWidthOrHeight,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_PhysicalUnit,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_FallbackScreenDPI,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_DefaultSpriteDPI,
	CanvasScaler_t50_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit,
	ContentSizeFitter_t275_CustomAttributesCacheGenerator,
	ContentSizeFitter_t275_CustomAttributesCacheGenerator_m_HorizontalFit,
	ContentSizeFitter_t275_CustomAttributesCacheGenerator_m_VerticalFit,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_StartCorner,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_StartAxis,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_CellSize,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_Spacing,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_Constraint,
	GridLayoutGroup_t279_CustomAttributesCacheGenerator_m_ConstraintCount,
	HorizontalLayoutGroup_t281_CustomAttributesCacheGenerator,
	HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_Spacing,
	HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_ChildForceExpandWidth,
	HorizontalOrVerticalLayoutGroup_t282_CustomAttributesCacheGenerator_m_ChildForceExpandHeight,
	LayoutElement_t283_CustomAttributesCacheGenerator,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_IgnoreLayout,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_MinWidth,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_MinHeight,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_PreferredWidth,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_PreferredHeight,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_FlexibleWidth,
	LayoutElement_t283_CustomAttributesCacheGenerator_m_FlexibleHeight,
	LayoutGroup_t280_CustomAttributesCacheGenerator,
	LayoutGroup_t280_CustomAttributesCacheGenerator_m_Padding,
	LayoutGroup_t280_CustomAttributesCacheGenerator_m_ChildAlignment,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m1381,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m1382,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m1383,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m1384,
	LayoutRebuilder_t288_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutUtility_t290_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m1397,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m1401,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403,
	LayoutUtility_t290_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404,
	VerticalLayoutGroup_t291_CustomAttributesCacheGenerator,
	Mask_t292_CustomAttributesCacheGenerator,
	Mask_t292_CustomAttributesCacheGenerator_m_ShowMaskGraphic,
	IndexedSet_1_t422_CustomAttributesCacheGenerator,
	CanvasListPool_t295_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	CanvasListPool_t295_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426,
	ComponentListPool_t298_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ComponentListPool_t298_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430,
	ObjectPool_1_t424_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField,
	ObjectPool_1_t424_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m2090,
	ObjectPool_1_t424_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m2091,
	BaseVertexEffect_t299_CustomAttributesCacheGenerator,
	Outline_t300_CustomAttributesCacheGenerator,
	PositionAsUV1_t302_CustomAttributesCacheGenerator,
	Shadow_t301_CustomAttributesCacheGenerator,
	Shadow_t301_CustomAttributesCacheGenerator_m_EffectColor,
	Shadow_t301_CustomAttributesCacheGenerator_m_EffectDistance,
	Shadow_t301_CustomAttributesCacheGenerator_m_UseGraphicAlpha,
};
