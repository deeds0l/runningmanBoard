﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t186;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t22;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m1615_gshared (TweenRunner_1_t186 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m1615(__this, method) (( void (*) (TweenRunner_1_t186 *, const MethodInfo*))TweenRunner_1__ctor_m1615_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m13815_gshared (Object_t * __this /* static, unused */, ColorTween_t162  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m13815(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t162 , const MethodInfo*))TweenRunner_1_Start_m13815_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m1616_gshared (TweenRunner_1_t186 * __this, MonoBehaviour_t22 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m1616(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t186 *, MonoBehaviour_t22 *, const MethodInfo*))TweenRunner_1_Init_m1616_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m1645_gshared (TweenRunner_1_t186 * __this, ColorTween_t162  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m1645(__this, ___info, method) (( void (*) (TweenRunner_1_t186 *, ColorTween_t162 , const MethodInfo*))TweenRunner_1_StartTween_m1645_gshared)(__this, ___info, method)
