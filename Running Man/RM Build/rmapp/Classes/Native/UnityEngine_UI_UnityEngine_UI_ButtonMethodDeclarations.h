﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t167;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t165;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t143;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t102;
// System.Collections.IEnumerator
struct IEnumerator_t28;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m570 (Button_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t165 * Button_get_onClick_m571 (Button_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m572 (Button_t167 * __this, ButtonClickedEvent_t165 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m573 (Button_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m574 (Button_t167 * __this, PointerEventData_t143 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m575 (Button_t167 * __this, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m576 (Button_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
