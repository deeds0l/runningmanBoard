﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2774;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Comparer_1__ctor_m16097_gshared (Comparer_1_t2774 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m16097(__this, method) (( void (*) (Comparer_1_t2774 *, const MethodInfo*))Comparer_1__ctor_m16097_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void Comparer_1__cctor_m16098_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m16098(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m16098_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m16099_gshared (Comparer_1_t2774 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m16099(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2774 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m16099_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C" Comparer_1_t2774 * Comparer_1_get_Default_m16100_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m16100(__this /* static, unused */, method) (( Comparer_1_t2774 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m16100_gshared)(__this /* static, unused */, method)
