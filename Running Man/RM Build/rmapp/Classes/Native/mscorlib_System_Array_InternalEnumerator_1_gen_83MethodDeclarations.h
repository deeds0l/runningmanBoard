﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
struct InternalEnumerator_1_t3001;
// System.Object
struct Object_t;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1544;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18545(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3001 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18546(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3001 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m18547(__this, method) (( void (*) (InternalEnumerator_1_t3001 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18548(__this, method) (( bool (*) (InternalEnumerator_1_t3001 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m18549(__this, method) (( FieldBuilder_t1544 * (*) (InternalEnumerator_1_t3001 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
