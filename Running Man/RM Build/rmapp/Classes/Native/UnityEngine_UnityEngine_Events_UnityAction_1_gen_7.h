﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t102;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityAction_1_t2518  : public MulticastDelegate_t216
{
};
