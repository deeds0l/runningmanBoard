﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2774;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct  Comparer_1_t2774  : public Object_t
{
};
struct Comparer_1_t2774_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::_default
	Comparer_1_t2774 * ____default_0;
};
