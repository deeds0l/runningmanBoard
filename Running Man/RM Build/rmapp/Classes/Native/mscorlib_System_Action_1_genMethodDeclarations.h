﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Font>
struct Action_1_t355;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t176;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
#define Action_1__ctor_m1607(__this, ___object, ___method, method) (( void (*) (Action_1_t355 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m13652_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Font>::Invoke(T)
#define Action_1_Invoke_m13653(__this, ___obj, method) (( void (*) (Action_1_t355 *, Font_t176 *, const MethodInfo*))Action_1_Invoke_m13654_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Font>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m13655(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t355 *, Font_t176 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m13656_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m13657(__this, ___result, method) (( void (*) (Action_1_t355 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m13658_gshared)(__this, ___result, method)
