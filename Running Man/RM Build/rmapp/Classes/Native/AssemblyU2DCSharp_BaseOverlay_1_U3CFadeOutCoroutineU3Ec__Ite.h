﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// BaseOverlay`1<System.Object>
struct BaseOverlay_1_t2465;
// System.Object
#include "mscorlib_System_Object.h"
// BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>
struct  U3CFadeOutCoroutineU3Ec__Iterator4_t2468  : public Object_t
{
	// System.Single BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::duration
	float ___duration_1;
	// System.Int32 BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::$PC
	int32_t ___U24PC_2;
	// System.Object BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::$current
	Object_t * ___U24current_3;
	// System.Single BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::<$>duration
	float ___U3CU24U3Eduration_4;
	// BaseOverlay`1<T> BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::<>f__this
	BaseOverlay_1_t2465 * ___U3CU3Ef__this_5;
};
