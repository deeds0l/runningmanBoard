﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2960;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1083;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18333_gshared (Enumerator_t2960 * __this, Dictionary_2_t1083 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m18333(__this, ___dictionary, method) (( void (*) (Enumerator_t2960 *, Dictionary_2_t1083 *, const MethodInfo*))Enumerator__ctor_m18333_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18334_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18334(__this, method) (( Object_t * (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18334_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1147  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18335_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18335(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18336_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18336(__this, method) (( Object_t * (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18336_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18337_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18337(__this, method) (( Object_t * (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18337_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18338_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18338(__this, method) (( bool (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_MoveNext_m18338_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2956  Enumerator_get_Current_m18339_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18339(__this, method) (( KeyValuePair_2_t2956  (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_get_Current_m18339_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m18340_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m18340(__this, method) (( int32_t (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_get_CurrentKey_m18340_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m18341_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m18341(__this, method) (( int32_t (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_get_CurrentValue_m18341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m18342_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18342(__this, method) (( void (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_VerifyState_m18342_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m18343_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m18343(__this, method) (( void (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_VerifyCurrent_m18343_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m18344_gshared (Enumerator_t2960 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18344(__this, method) (( void (*) (Enumerator_t2960 *, const MethodInfo*))Enumerator_Dispose_m18344_gshared)(__this, method)
