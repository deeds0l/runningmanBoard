﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t180;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.StencilMaterial/MatEntry
struct  MatEntry_t258  : public Object_t
{
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::baseMat
	Material_t180 * ___baseMat_0;
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::customMat
	Material_t180 * ___customMat_1;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::count
	int32_t ___count_2;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::stencilID
	int32_t ___stencilID_3;
};
