﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ColorBlock
struct ColorBlock_t175;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
extern "C" Color_t163  ColorBlock_get_normalColor_m595 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
extern "C" void ColorBlock_set_normalColor_m596 (ColorBlock_t175 * __this, Color_t163  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
extern "C" Color_t163  ColorBlock_get_highlightedColor_m597 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_highlightedColor_m598 (ColorBlock_t175 * __this, Color_t163  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
extern "C" Color_t163  ColorBlock_get_pressedColor_m599 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
extern "C" void ColorBlock_set_pressedColor_m600 (ColorBlock_t175 * __this, Color_t163  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
extern "C" Color_t163  ColorBlock_get_disabledColor_m601 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
extern "C" void ColorBlock_set_disabledColor_m602 (ColorBlock_t175 * __this, Color_t163  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
extern "C" float ColorBlock_get_colorMultiplier_m603 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
extern "C" void ColorBlock_set_colorMultiplier_m604 (ColorBlock_t175 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
extern "C" float ColorBlock_get_fadeDuration_m605 (ColorBlock_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
extern "C" void ColorBlock_set_fadeDuration_m606 (ColorBlock_t175 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
extern "C" ColorBlock_t175  ColorBlock_get_defaultColorBlock_m607 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
