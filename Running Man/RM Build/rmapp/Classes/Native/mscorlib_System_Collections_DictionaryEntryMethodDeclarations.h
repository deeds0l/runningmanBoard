﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.DictionaryEntry
struct DictionaryEntry_t1147;
// System.Object
struct Object_t;

// System.Void System.Collections.DictionaryEntry::.ctor(System.Object,System.Object)
extern "C" void DictionaryEntry__ctor_m4683 (DictionaryEntry_t1147 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Key()
extern "C" Object_t * DictionaryEntry_get_Key_m7353 (DictionaryEntry_t1147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Value()
extern "C" Object_t * DictionaryEntry_get_Value_m7354 (DictionaryEntry_t1147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
