﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct U24ArrayTypeU24640_t1905;
struct U24ArrayTypeU24640_t1905_marshaled;

void U24ArrayTypeU24640_t1905_marshal(const U24ArrayTypeU24640_t1905& unmarshaled, U24ArrayTypeU24640_t1905_marshaled& marshaled);
void U24ArrayTypeU24640_t1905_marshal_back(const U24ArrayTypeU24640_t1905_marshaled& marshaled, U24ArrayTypeU24640_t1905& unmarshaled);
void U24ArrayTypeU24640_t1905_marshal_cleanup(U24ArrayTypeU24640_t1905_marshaled& marshaled);
