﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t1622;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1618;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m8504 (ContextLevelActivator_t1622 * __this, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
