﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t227;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3104;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t386;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t3107;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t2609;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t224;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t2610;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t2612;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m1744_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1__ctor_m1744(__this, method) (( void (*) (List_1_t227 *, const MethodInfo*))List_1__ctor_m1744_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3423_gshared (List_1_t227 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3423(__this, ___capacity, method) (( void (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1__ctor_m3423_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m13718_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13718(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13718_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13719_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13719(__this, method) (( Object_t* (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13719_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13720_gshared (List_1_t227 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13720(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t227 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13720_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13721(__this, method) (( Object_t * (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13722_gshared (List_1_t227 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13722(__this, ___item, method) (( int32_t (*) (List_1_t227 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13722_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13723_gshared (List_1_t227 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13723(__this, ___item, method) (( bool (*) (List_1_t227 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13723_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13724_gshared (List_1_t227 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13724(__this, ___item, method) (( int32_t (*) (List_1_t227 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13724_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13725_gshared (List_1_t227 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13725(__this, ___index, ___item, method) (( void (*) (List_1_t227 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13725_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13726_gshared (List_1_t227 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13726(__this, ___item, method) (( void (*) (List_1_t227 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13726_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13727_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13727(__this, method) (( bool (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13728_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13728(__this, method) (( bool (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13728_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13729_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13729(__this, method) (( Object_t * (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13730_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13730(__this, method) (( bool (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13730_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13731_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13731(__this, method) (( bool (*) (List_1_t227 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13731_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13732_gshared (List_1_t227 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13732(__this, ___index, method) (( Object_t * (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13732_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13733_gshared (List_1_t227 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13733(__this, ___index, ___value, method) (( void (*) (List_1_t227 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13733_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m13734_gshared (List_1_t227 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define List_1_Add_m13734(__this, ___item, method) (( void (*) (List_1_t227 *, UIVertex_t225 , const MethodInfo*))List_1_Add_m13734_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13735_gshared (List_1_t227 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13735(__this, ___newCount, method) (( void (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13735_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13736_gshared (List_1_t227 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13736(__this, ___collection, method) (( void (*) (List_1_t227 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13736_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13737_gshared (List_1_t227 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13737(__this, ___enumerable, method) (( void (*) (List_1_t227 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13737_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13738_gshared (List_1_t227 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13738(__this, ___collection, method) (( void (*) (List_1_t227 *, Object_t*, const MethodInfo*))List_1_AddRange_m13738_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2609 * List_1_AsReadOnly_m13739_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13739(__this, method) (( ReadOnlyCollection_1_t2609 * (*) (List_1_t227 *, const MethodInfo*))List_1_AsReadOnly_m13739_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m13740_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_Clear_m13740(__this, method) (( void (*) (List_1_t227 *, const MethodInfo*))List_1_Clear_m13740_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m13741_gshared (List_1_t227 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define List_1_Contains_m13741(__this, ___item, method) (( bool (*) (List_1_t227 *, UIVertex_t225 , const MethodInfo*))List_1_Contains_m13741_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13742_gshared (List_1_t227 * __this, UIVertexU5BU5D_t224* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13742(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t227 *, UIVertexU5BU5D_t224*, int32_t, const MethodInfo*))List_1_CopyTo_m13742_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t225  List_1_Find_m13743_gshared (List_1_t227 * __this, Predicate_1_t2610 * ___match, const MethodInfo* method);
#define List_1_Find_m13743(__this, ___match, method) (( UIVertex_t225  (*) (List_1_t227 *, Predicate_1_t2610 *, const MethodInfo*))List_1_Find_m13743_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13744_gshared (Object_t * __this /* static, unused */, Predicate_1_t2610 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13744(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2610 *, const MethodInfo*))List_1_CheckMatch_m13744_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13745_gshared (List_1_t227 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2610 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13745(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t227 *, int32_t, int32_t, Predicate_1_t2610 *, const MethodInfo*))List_1_GetIndex_m13745_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t2611  List_1_GetEnumerator_m13746_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13746(__this, method) (( Enumerator_t2611  (*) (List_1_t227 *, const MethodInfo*))List_1_GetEnumerator_m13746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13747_gshared (List_1_t227 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define List_1_IndexOf_m13747(__this, ___item, method) (( int32_t (*) (List_1_t227 *, UIVertex_t225 , const MethodInfo*))List_1_IndexOf_m13747_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13748_gshared (List_1_t227 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13748(__this, ___start, ___delta, method) (( void (*) (List_1_t227 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13748_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13749_gshared (List_1_t227 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13749(__this, ___index, method) (( void (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13749_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13750_gshared (List_1_t227 * __this, int32_t ___index, UIVertex_t225  ___item, const MethodInfo* method);
#define List_1_Insert_m13750(__this, ___index, ___item, method) (( void (*) (List_1_t227 *, int32_t, UIVertex_t225 , const MethodInfo*))List_1_Insert_m13750_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13751_gshared (List_1_t227 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13751(__this, ___collection, method) (( void (*) (List_1_t227 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13751_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m13752_gshared (List_1_t227 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define List_1_Remove_m13752(__this, ___item, method) (( bool (*) (List_1_t227 *, UIVertex_t225 , const MethodInfo*))List_1_Remove_m13752_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13753_gshared (List_1_t227 * __this, Predicate_1_t2610 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13753(__this, ___match, method) (( int32_t (*) (List_1_t227 *, Predicate_1_t2610 *, const MethodInfo*))List_1_RemoveAll_m13753_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13754_gshared (List_1_t227 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13754(__this, ___index, method) (( void (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13754_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m13755_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_Reverse_m13755(__this, method) (( void (*) (List_1_t227 *, const MethodInfo*))List_1_Reverse_m13755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m13756_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_Sort_m13756(__this, method) (( void (*) (List_1_t227 *, const MethodInfo*))List_1_Sort_m13756_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m13757_gshared (List_1_t227 * __this, Comparison_1_t2612 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m13757(__this, ___comparison, method) (( void (*) (List_1_t227 *, Comparison_1_t2612 *, const MethodInfo*))List_1_Sort_m13757_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t224* List_1_ToArray_m1801_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_ToArray_m1801(__this, method) (( UIVertexU5BU5D_t224* (*) (List_1_t227 *, const MethodInfo*))List_1_ToArray_m1801_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m13758_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13758(__this, method) (( void (*) (List_1_t227 *, const MethodInfo*))List_1_TrimExcess_m13758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1649_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1649(__this, method) (( int32_t (*) (List_1_t227 *, const MethodInfo*))List_1_get_Capacity_m1649_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1650_gshared (List_1_t227 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1650(__this, ___value, method) (( void (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1650_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m13759_gshared (List_1_t227 * __this, const MethodInfo* method);
#define List_1_get_Count_m13759(__this, method) (( int32_t (*) (List_1_t227 *, const MethodInfo*))List_1_get_Count_m13759_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t225  List_1_get_Item_m13760_gshared (List_1_t227 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13760(__this, ___index, method) (( UIVertex_t225  (*) (List_1_t227 *, int32_t, const MethodInfo*))List_1_get_Item_m13760_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13761_gshared (List_1_t227 * __this, int32_t ___index, UIVertex_t225  ___value, const MethodInfo* method);
#define List_1_set_Item_m13761(__this, ___index, ___value, method) (( void (*) (List_1_t227 *, int32_t, UIVertex_t225 , const MethodInfo*))List_1_set_Item_m13761_gshared)(__this, ___index, ___value, method)
