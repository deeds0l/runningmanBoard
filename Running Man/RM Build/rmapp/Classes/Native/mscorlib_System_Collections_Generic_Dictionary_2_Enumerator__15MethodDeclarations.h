﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Enumerator_t2836;
// System.Object
struct Object_t;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t613;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Dictionary_2_t615;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"
#define Enumerator__ctor_m16778(__this, ___dictionary, method) (( void (*) (Enumerator_t2836 *, Dictionary_2_t615 *, const MethodInfo*))Enumerator__ctor_m16676_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16779(__this, method) (( Object_t * (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16677_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16780(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16678_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16781(__this, method) (( Object_t * (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16679_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16782(__this, method) (( Object_t * (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16680_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::MoveNext()
#define Enumerator_MoveNext_m16783(__this, method) (( bool (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_MoveNext_m16681_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Current()
#define Enumerator_get_Current_m16784(__this, method) (( KeyValuePair_2_t2833  (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_get_Current_m16682_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16785(__this, method) (( uint64_t (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_get_CurrentKey_m16683_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16786(__this, method) (( NetworkAccessToken_t613 * (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_get_CurrentValue_m16684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyState()
#define Enumerator_VerifyState_m16787(__this, method) (( void (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_VerifyState_m16685_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16788(__this, method) (( void (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_VerifyCurrent_m16686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::Dispose()
#define Enumerator_Dispose_m16789(__this, method) (( void (*) (Enumerator_t2836 *, const MethodInfo*))Enumerator_Dispose_m16687_gshared)(__this, method)
