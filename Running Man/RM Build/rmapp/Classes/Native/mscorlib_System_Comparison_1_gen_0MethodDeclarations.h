﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t157;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Comparison`1<UnityEngine.RaycastHit>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m1581_gshared (Comparison_1_t157 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m1581(__this, ___object, ___method, method) (( void (*) (Comparison_1_t157 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1581_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m13046_gshared (Comparison_1_t157 * __this, RaycastHit_t323  ___x, RaycastHit_t323  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m13046(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t157 *, RaycastHit_t323 , RaycastHit_t323 , const MethodInfo*))Comparison_1_Invoke_m13046_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.RaycastHit>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m13047_gshared (Comparison_1_t157 * __this, RaycastHit_t323  ___x, RaycastHit_t323  ___y, AsyncCallback_t214 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m13047(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t157 *, RaycastHit_t323 , RaycastHit_t323 , AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m13047_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.RaycastHit>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m13048_gshared (Comparison_1_t157 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m13048(__this, ___result, method) (( int32_t (*) (Comparison_1_t157 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m13048_gshared)(__this, ___result, method)
