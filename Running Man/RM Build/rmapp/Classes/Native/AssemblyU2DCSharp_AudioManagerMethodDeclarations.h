﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioManager
struct AudioManager_t2;
// System.String
struct String_t;

// System.Void AudioManager::.ctor()
extern "C" void AudioManager__ctor_m0 (AudioManager_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::Awake()
extern "C" void AudioManager_Awake_m1 (AudioManager_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayMusic(System.String,System.Boolean)
extern "C" void AudioManager_PlayMusic_m2 (AudioManager_t2 * __this, String_t* ___p_soundName, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlaySound(System.String,System.Boolean)
extern "C" void AudioManager_PlaySound_m3 (AudioManager_t2 * __this, String_t* ___p_soundName, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::StopAudio()
extern "C" void AudioManager_StopAudio_m4 (AudioManager_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::StopMusic()
extern "C" void AudioManager_StopMusic_m5 (AudioManager_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::StopSoundEffects()
extern "C" void AudioManager_StopSoundEffects_m6 (AudioManager_t2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
