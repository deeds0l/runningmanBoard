﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler
struct AudioPooler_t10;
// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPE.h"

// System.Void AudioPooler::.ctor()
extern "C" void AudioPooler__ctor_m34 (AudioPooler_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AudioPooler AudioPooler::get_Instance()
extern "C" AudioPooler_t10 * AudioPooler_get_Instance_m35 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::Awake()
extern "C" void AudioPooler_Awake_m36 (AudioPooler_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::CreatePool(UnityEngine.GameObject)
extern "C" void AudioPooler_CreatePool_m37 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource AudioPooler::GetAudioSource(UnityEngine.GameObject,AudioPooler/SOUNDTYPE)
extern "C" AudioSource_t9 * AudioPooler_GetAudioSource_m38 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, int32_t ___p_soundType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::ReturnAudioSource(UnityEngine.AudioSource,AudioPooler/SOUNDTYPE)
extern "C" void AudioPooler_ReturnAudioSource_m39 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_currentAudioSource, int32_t ___p_soundType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::ClearAudioSource(UnityEngine.GameObject)
extern "C" void AudioPooler_ClearAudioSource_m40 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::ClearMusic(UnityEngine.GameObject)
extern "C" void AudioPooler_ClearMusic_m41 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::ClearSoundEffects(UnityEngine.GameObject)
extern "C" void AudioPooler_ClearSoundEffects_m42 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::SearchMusic(System.String,UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_SearchMusic_m43 (AudioPooler_t10 * __this, String_t* ___key, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::SearchSound(System.String,UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_SearchSound_m44 (AudioPooler_t10 * __this, String_t* ___key, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::LookForMusicSource(UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_LookForMusicSource_m45 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler::LookForSoundSource(UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_LookForSoundSource_m46 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AudioPooler::DisableMusic(UnityEngine.AudioSource,System.Boolean)
extern "C" Object_t * AudioPooler_DisableMusic_m47 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AudioPooler::DisableSound(UnityEngine.AudioSource,System.Boolean)
extern "C" Object_t * AudioPooler_DisableSound_m48 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
