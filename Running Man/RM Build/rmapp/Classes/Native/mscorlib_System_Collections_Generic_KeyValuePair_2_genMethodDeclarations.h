﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct KeyValuePair_2_t343;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t143;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"
#define KeyValuePair_2__ctor_m12906(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t343 *, int32_t, PointerEventData_t143 *, const MethodInfo*))KeyValuePair_2__ctor_m12803_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
#define KeyValuePair_2_get_Key_m1531(__this, method) (( int32_t (*) (KeyValuePair_2_t343 *, const MethodInfo*))KeyValuePair_2_get_Key_m12804_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m12907(__this, ___value, method) (( void (*) (KeyValuePair_2_t343 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m12805_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
#define KeyValuePair_2_get_Value_m1530(__this, method) (( PointerEventData_t143 * (*) (KeyValuePair_2_t343 *, const MethodInfo*))KeyValuePair_2_get_Value_m12806_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m12908(__this, ___value, method) (( void (*) (KeyValuePair_2_t343 *, PointerEventData_t143 *, const MethodInfo*))KeyValuePair_2_set_Value_m12807_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
#define KeyValuePair_2_ToString_m1557(__this, method) (( String_t* (*) (KeyValuePair_2_t343 *, const MethodInfo*))KeyValuePair_2_ToString_m12808_gshared)(__this, method)
