﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t293;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t294;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t334;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m1970(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t293 *, UnityAction_1_t294 *, UnityAction_1_t294 *, const MethodInfo*))ObjectPool_1__ctor_m11987_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m14821(__this, method) (( int32_t (*) (ObjectPool_1_t293 *, const MethodInfo*))ObjectPool_1_get_countAll_m11989_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m14822(__this, ___value, method) (( void (*) (ObjectPool_1_t293 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11991_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m14823(__this, method) (( int32_t (*) (ObjectPool_1_t293 *, const MethodInfo*))ObjectPool_1_get_countActive_m11993_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m14824(__this, method) (( int32_t (*) (ObjectPool_1_t293 *, const MethodInfo*))ObjectPool_1_get_countInactive_m11995_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m1971(__this, method) (( List_1_t334 * (*) (ObjectPool_1_t293 *, const MethodInfo*))ObjectPool_1_Get_m11997_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m1972(__this, ___element, method) (( void (*) (ObjectPool_1_t293 *, List_1_t334 *, const MethodInfo*))ObjectPool_1_Release_m11999_gshared)(__this, ___element, method)
