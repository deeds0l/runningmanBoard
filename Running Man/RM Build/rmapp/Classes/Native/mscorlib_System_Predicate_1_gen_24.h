﻿#pragma once
#include <stdint.h>
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t515;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.GUILayoutEntry>
struct  Predicate_1_t2722  : public MulticastDelegate_t216
{
};
