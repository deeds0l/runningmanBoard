﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t736;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t513;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t525;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9MethodDeclarations.h"
#define Enumerator__ctor_m15320(__this, ___host, method) (( void (*) (Enumerator_t736 *, Dictionary_2_t525 *, const MethodInfo*))Enumerator__ctor_m13535_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15321(__this, method) (( Object_t * (*) (Enumerator_t736 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m15322(__this, method) (( void (*) (Enumerator_t736 *, const MethodInfo*))Enumerator_Dispose_m13537_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m15323(__this, method) (( bool (*) (Enumerator_t736 *, const MethodInfo*))Enumerator_MoveNext_m13538_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m15324(__this, method) (( GUIStyle_t513 * (*) (Enumerator_t736 *, const MethodInfo*))Enumerator_get_Current_m13539_gshared)(__this, method)
