﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1148;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// System.Security.Cryptography.DSASignatureDeformatter
struct  DSASignatureDeformatter_t1349  : public AsymmetricSignatureDeformatter_t1299
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureDeformatter::dsa
	DSA_t1148 * ___dsa_0;
};
