﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>
struct U3CFadeOutCoroutineU3Ec__Iterator4_t2468;
// System.Object
struct Object_t;

// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::.ctor()
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m11776_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m11776(__this, method) (( void (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m11776_gshared)(__this, method)
// System.Object BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11777_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11777(__this, method) (( Object_t * (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11777_gshared)(__this, method)
// System.Object BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m11778_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m11778(__this, method) (( Object_t * (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m11778_gshared)(__this, method)
// System.Boolean BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::MoveNext()
extern "C" bool U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m11779_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m11779(__this, method) (( bool (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m11779_gshared)(__this, method)
// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::Dispose()
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m11780_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m11780(__this, method) (( void (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m11780_gshared)(__this, method)
// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4<System.Object>::Reset()
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m11781_gshared (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 * __this, const MethodInfo* method);
#define U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m11781(__this, method) (( void (*) (U3CFadeOutCoroutineU3Ec__Iterator4_t2468 *, const MethodInfo*))U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m11781_gshared)(__this, method)
