﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct Dictionary_2_t2842;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t3218;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t3219;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t631;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyCollection_t3222;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ValueCollection_t3223;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t2845;
// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t625;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t3220;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t3221;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16MethodDeclarations.h"
#define Dictionary_2__ctor_m16969(__this, method) (( void (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2__ctor_m13332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16970(__this, ___comparer, method) (( void (*) (Dictionary_2_t2842 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13334_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m16971(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2842 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13336_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m16972(__this, ___capacity, method) (( void (*) (Dictionary_2_t2842 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13338_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m16973(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2842 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13340_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m16974(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2842 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m13342_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16975(__this, method) (( Object_t* (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13344_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16976(__this, method) (( Object_t* (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13346_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16977(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13348_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16978(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2842 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13350_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m16979(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2842 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13352_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m16980(__this, ___key, method) (( bool (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13354_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m16981(__this, ___key, method) (( void (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13356_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16982(__this, method) (( bool (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13358_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16983(__this, method) (( Object_t * (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13360_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16984(__this, method) (( bool (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16985(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2842 *, KeyValuePair_2_t2847 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13364_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16986(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2842 *, KeyValuePair_2_t2847 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13366_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16987(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2842 *, KeyValuePair_2U5BU5D_t3220*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13368_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16988(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2842 *, KeyValuePair_2_t2847 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13370_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16989(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2842 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13372_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16990(__this, method) (( Object_t * (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13374_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16991(__this, method) (( Object_t* (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13376_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16992(__this, method) (( Object_t * (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13378_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define Dictionary_2_get_Count_m16993(__this, method) (( int32_t (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_get_Count_m13380_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m16994(__this, ___key, method) (( ConstructorDelegate_t631 * (*) (Dictionary_2_t2842 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m13382_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m16995(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2842 *, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_set_Item_m13384_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m16996(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2842 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13386_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m16997(__this, ___size, method) (( void (*) (Dictionary_2_t2842 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13388_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m16998(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2842 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13390_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m16999(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2847  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_make_pair_m13392_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m17000(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_pick_key_m13394_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m17001(__this /* static, unused */, ___key, ___value, method) (( ConstructorDelegate_t631 * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_pick_value_m13396_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m17002(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2842 *, KeyValuePair_2U5BU5D_t3220*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13398_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Resize()
#define Dictionary_2_Resize_m17003(__this, method) (( void (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_Resize_m13400_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m17004(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2842 *, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_Add_m13402_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define Dictionary_2_Clear_m17005(__this, method) (( void (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_Clear_m13404_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m17006(__this, ___key, method) (( bool (*) (Dictionary_2_t2842 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m13406_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m17007(__this, ___value, method) (( bool (*) (Dictionary_2_t2842 *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_ContainsValue_m13408_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m17008(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2842 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m13410_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m17009(__this, ___sender, method) (( void (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13412_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m17010(__this, ___key, method) (( bool (*) (Dictionary_2_t2842 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m13414_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m17011(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2842 *, Type_t *, ConstructorDelegate_t631 **, const MethodInfo*))Dictionary_2_TryGetValue_m13416_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m17012(__this, method) (( KeyCollection_t3222 * (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_get_Keys_m13418_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define Dictionary_2_get_Values_m17013(__this, method) (( ValueCollection_t3223 * (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_get_Values_m13420_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m17014(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13422_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m17015(__this, ___value, method) (( ConstructorDelegate_t631 * (*) (Dictionary_2_t2842 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13424_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m17016(__this, ___pair, method) (( bool (*) (Dictionary_2_t2842 *, KeyValuePair_2_t2847 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13426_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m17017(__this, method) (( Enumerator_t3224  (*) (Dictionary_2_t2842 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m17018(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13430_gshared)(__this /* static, unused */, ___key, ___value, method)
