﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1006;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t929  : public Attribute_t539
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1006 * ___ver_0;
};
