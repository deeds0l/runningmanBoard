﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.AudioSource>
struct  Predicate_1_t2459  : public MulticastDelegate_t216
{
};
