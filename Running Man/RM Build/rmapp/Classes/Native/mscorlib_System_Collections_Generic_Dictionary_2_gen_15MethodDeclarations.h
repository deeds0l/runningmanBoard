﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2563;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3065;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t2569;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t2573;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2565;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
struct IDictionary_2_t3080;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3081;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t3082;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m13094_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m13094(__this, method) (( void (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2__ctor_m13094_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m13095_gshared (Dictionary_2_t2563 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m13095(__this, ___comparer, method) (( void (*) (Dictionary_2_t2563 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13095_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m13096_gshared (Dictionary_2_t2563 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m13096(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2563 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13096_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m13097_gshared (Dictionary_2_t2563 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m13097(__this, ___capacity, method) (( void (*) (Dictionary_2_t2563 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m13098_gshared (Dictionary_2_t2563 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m13098(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2563 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13098_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m13099_gshared (Dictionary_2_t2563 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m13099(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2563 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m13099_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100(__this, method) (( Object_t* (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101(__this, method) (( Object_t* (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m13102_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m13102(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13102_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13103_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m13103(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13103_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13104_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m13104(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13104_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m13105_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m13105(__this, ___key, method) (( bool (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13105_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13106_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m13106(__this, ___key, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13106_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107(__this, method) (( bool (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108(__this, method) (( Object_t * (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109(__this, method) (( bool (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2_t2567  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2563 *, KeyValuePair_2_t2567 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2_t2567  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2563 *, KeyValuePair_2_t2567 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2U5BU5D_t3081* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2563 *, KeyValuePair_2U5BU5D_t3081*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2_t2567  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2563 *, KeyValuePair_2_t2567 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13114_gshared (Dictionary_2_t2563 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m13114(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2563 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13114_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115(__this, method) (( Object_t * (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116(__this, method) (( Object_t* (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117(__this, method) (( Object_t * (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m13118_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m13118(__this, method) (( int32_t (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_get_Count_m13118_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m13119_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m13119(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m13119_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m13120_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m13120(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m13120_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m13121_gshared (Dictionary_2_t2563 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m13121(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2563 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13121_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m13122_gshared (Dictionary_2_t2563 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m13122(__this, ___size, method) (( void (*) (Dictionary_2_t2563 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13122_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m13123_gshared (Dictionary_2_t2563 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m13123(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2563 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13123_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2567  Dictionary_2_make_pair_m13124_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m13124(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2567  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m13124_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m13125_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m13125(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m13125_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m13126_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m13126(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m13126_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m13127_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2U5BU5D_t3081* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m13127(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2563 *, KeyValuePair_2U5BU5D_t3081*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13127_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m13128_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m13128(__this, method) (( void (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_Resize_m13128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m13129_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m13129(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m13129_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m13130_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m13130(__this, method) (( void (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_Clear_m13130_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m13131_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m13131(__this, ___key, method) (( bool (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m13131_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m13132_gshared (Dictionary_2_t2563 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m13132(__this, ___value, method) (( bool (*) (Dictionary_2_t2563 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m13132_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m13133_gshared (Dictionary_2_t2563 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m13133(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2563 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m13133_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m13134_gshared (Dictionary_2_t2563 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m13134(__this, ___sender, method) (( void (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13134_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m13135_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m13135(__this, ___key, method) (( bool (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m13135_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m13136_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m13136(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2563 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m13136_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t2569 * Dictionary_2_get_Keys_m13137_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m13137(__this, method) (( KeyCollection_t2569 * (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_get_Keys_m13137_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t2573 * Dictionary_2_get_Values_m13138_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m13138(__this, method) (( ValueCollection_t2573 * (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_get_Values_m13138_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m13139_gshared (Dictionary_2_t2563 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m13139(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13139_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m13140_gshared (Dictionary_2_t2563 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m13140(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2563 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13140_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m13141_gshared (Dictionary_2_t2563 * __this, KeyValuePair_2_t2567  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m13141(__this, ___pair, method) (( bool (*) (Dictionary_2_t2563 *, KeyValuePair_2_t2567 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13141_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2571  Dictionary_2_GetEnumerator_m13142_gshared (Dictionary_2_t2563 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m13142(__this, method) (( Enumerator_t2571  (*) (Dictionary_2_t2563 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13142_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m13143_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m13143(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13143_gshared)(__this /* static, unused */, ___key, ___value, method)
