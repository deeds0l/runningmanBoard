﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
struct KeyValuePair_2_t767;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t630;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m3483(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t767 *, Type_t *, SetDelegate_t630 *, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m16909(__this, method) (( Type_t * (*) (KeyValuePair_2_t767 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16910(__this, ___value, method) (( void (*) (KeyValuePair_2_t767 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m16911(__this, method) (( SetDelegate_t630 * (*) (KeyValuePair_2_t767 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16912(__this, ___value, method) (( void (*) (KeyValuePair_2_t767 *, SetDelegate_t630 *, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::ToString()
#define KeyValuePair_2_ToString_m16913(__this, method) (( String_t* (*) (KeyValuePair_2_t767 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
