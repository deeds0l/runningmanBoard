﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
struct Queue_1_t7;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioSource>
struct IEnumerator_1_t3037;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t2456;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::.ctor()
// System.Collections.Generic.Queue`1<System.Object>
#include "System_System_Collections_Generic_Queue_1_gen_0MethodDeclarations.h"
#define Queue_1__ctor_m102(__this, method) (( void (*) (Queue_1_t7 *, const MethodInfo*))Queue_1__ctor_m11704_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m11705(__this, ___array, ___idx, method) (( void (*) (Queue_1_t7 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m11706_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m11707(__this, method) (( bool (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m11709(__this, method) (( Object_t * (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m11710_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11711(__this, method) (( Object_t* (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m11713(__this, method) (( Object_t * (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m11715(__this, ___array, ___idx, method) (( void (*) (Queue_1_t7 *, AudioSourceU5BU5D_t2456*, int32_t, const MethodInfo*))Queue_1_CopyTo_m11716_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::Dequeue()
#define Queue_1_Dequeue_m106(__this, method) (( AudioSource_t9 * (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_Dequeue_m11717_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::Peek()
#define Queue_1_Peek_m11718(__this, method) (( AudioSource_t9 * (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_Peek_m11719_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::Enqueue(T)
#define Queue_1_Enqueue_m103(__this, ___item, method) (( void (*) (Queue_1_t7 *, AudioSource_t9 *, const MethodInfo*))Queue_1_Enqueue_m11720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m11721(__this, ___new_size, method) (( void (*) (Queue_1_t7 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m11722_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::get_Count()
#define Queue_1_get_Count_m11723(__this, method) (( int32_t (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_get_Count_m11724_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::GetEnumerator()
#define Queue_1_GetEnumerator_m11725(__this, method) (( Enumerator_t2463  (*) (Queue_1_t7 *, const MethodInfo*))Queue_1_GetEnumerator_m11726_gshared)(__this, method)
