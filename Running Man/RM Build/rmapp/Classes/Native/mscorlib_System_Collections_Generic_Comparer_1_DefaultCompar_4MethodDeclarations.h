﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t2968;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m18388_gshared (DefaultComparer_t2968 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18388(__this, method) (( void (*) (DefaultComparer_t2968 *, const MethodInfo*))DefaultComparer__ctor_m18388_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18389_gshared (DefaultComparer_t2968 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18389(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2968 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m18389_gshared)(__this, ___x, ___y, method)
