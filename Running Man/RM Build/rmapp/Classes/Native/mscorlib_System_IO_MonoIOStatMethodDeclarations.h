﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t1520;
struct MonoIOStat_t1520_marshaled;

void MonoIOStat_t1520_marshal(const MonoIOStat_t1520& unmarshaled, MonoIOStat_t1520_marshaled& marshaled);
void MonoIOStat_t1520_marshal_back(const MonoIOStat_t1520_marshaled& marshaled, MonoIOStat_t1520& unmarshaled);
void MonoIOStat_t1520_marshal_cleanup(MonoIOStat_t1520_marshaled& marshaled);
