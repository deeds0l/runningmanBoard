﻿#pragma once
#include <stdint.h>
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t418  : public PropertyAttribute_t672
{
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;
};
