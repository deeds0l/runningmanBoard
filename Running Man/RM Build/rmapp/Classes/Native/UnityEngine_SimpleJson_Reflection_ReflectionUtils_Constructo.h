﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t631  : public MulticastDelegate_t216
{
};
