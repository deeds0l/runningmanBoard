﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct KeyValuePair_2_t2851;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t720;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17096(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2851 *, Type_t *, Object_t*, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m17097(__this, method) (( Type_t * (*) (KeyValuePair_2_t2851 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17098(__this, ___value, method) (( void (*) (KeyValuePair_2_t2851 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m17099(__this, method) (( Object_t* (*) (KeyValuePair_2_t2851 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17100(__this, ___value, method) (( void (*) (KeyValuePair_2_t2851 *, Object_t*, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m17101(__this, method) (( String_t* (*) (KeyValuePair_2_t2851 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
