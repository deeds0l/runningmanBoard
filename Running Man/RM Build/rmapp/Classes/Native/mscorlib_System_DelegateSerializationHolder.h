﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t361;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t1833  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t361 * ____delegate_0;
};
