﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t3026;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m18733_gshared (DefaultComparer_t3026 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18733(__this, method) (( void (*) (DefaultComparer_t3026 *, const MethodInfo*))DefaultComparer__ctor_m18733_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18734_gshared (DefaultComparer_t3026 * __this, Guid_t769  ___x, Guid_t769  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18734(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3026 *, Guid_t769 , Guid_t769 , const MethodInfo*))DefaultComparer_Compare_m18734_gshared)(__this, ___x, ___y, method)
