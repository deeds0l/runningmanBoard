﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct U24ArrayTypeU2416_t1890;
struct U24ArrayTypeU2416_t1890_marshaled;

void U24ArrayTypeU2416_t1890_marshal(const U24ArrayTypeU2416_t1890& unmarshaled, U24ArrayTypeU2416_t1890_marshaled& marshaled);
void U24ArrayTypeU2416_t1890_marshal_back(const U24ArrayTypeU2416_t1890_marshaled& marshaled, U24ArrayTypeU2416_t1890& unmarshaled);
void U24ArrayTypeU2416_t1890_marshal_cleanup(U24ArrayTypeU2416_t1890_marshaled& marshaled);
