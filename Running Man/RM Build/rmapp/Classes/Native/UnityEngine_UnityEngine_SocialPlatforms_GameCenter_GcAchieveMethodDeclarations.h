﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t649;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t660;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern "C" AchievementDescription_t660 * GcAchievementDescriptionData_ToAchievementDescription_m3206 (GcAchievementDescriptionData_t649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
