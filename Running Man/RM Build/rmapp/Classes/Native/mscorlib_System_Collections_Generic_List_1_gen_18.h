﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t2678;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct  List_1_t266  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_items
	ToggleU5BU5D_t2678* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_version
	int32_t ____version_3;
};
struct List_1_t266_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::EmptyArray
	ToggleU5BU5D_t2678* ___EmptyArray_4;
};
