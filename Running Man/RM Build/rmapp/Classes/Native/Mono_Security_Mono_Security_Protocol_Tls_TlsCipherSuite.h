﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Object
struct Object_t;
// Mono.Security.Protocol.Tls.CipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuite.h"
// Mono.Security.Protocol.Tls.TlsCipherSuite
struct  TlsCipherSuite_t1312  : public CipherSuite_t1273
{
	// System.Byte[] Mono.Security.Protocol.Tls.TlsCipherSuite::header
	ByteU5BU5D_t546* ___header_21;
	// System.Object Mono.Security.Protocol.Tls.TlsCipherSuite::headerLock
	Object_t * ___headerLock_22;
};
