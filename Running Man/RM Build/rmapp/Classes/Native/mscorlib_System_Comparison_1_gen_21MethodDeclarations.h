﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UI.Toggle>
struct Comparison_1_t2682;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t265;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.UI.Toggle>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m14705(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2682 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11465_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Toggle>::Invoke(T,T)
#define Comparison_1_Invoke_m14706(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2682 *, Toggle_t265 *, Toggle_t265 *, const MethodInfo*))Comparison_1_Invoke_m11466_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UI.Toggle>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m14707(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2682 *, Toggle_t265 *, Toggle_t265 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11467_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Toggle>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m14708(__this, ___result, method) (( int32_t (*) (Comparison_1_t2682 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11468_gshared)(__this, ___result, method)
