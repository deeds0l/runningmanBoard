﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
struct Enumerator_t2746;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t544;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m15696(__this, ___l, method) (( void (*) (Enumerator_t2746 *, List_1_t544 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15697(__this, method) (( Object_t * (*) (Enumerator_t2746 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::Dispose()
#define Enumerator_Dispose_m15698(__this, method) (( void (*) (Enumerator_t2746 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m15699(__this, method) (( void (*) (Enumerator_t2746 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m15700(__this, method) (( bool (*) (Enumerator_t2746 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte[]>::get_Current()
#define Enumerator_get_Current_m15701(__this, method) (( ByteU5BU5D_t546* (*) (Enumerator_t2746 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
