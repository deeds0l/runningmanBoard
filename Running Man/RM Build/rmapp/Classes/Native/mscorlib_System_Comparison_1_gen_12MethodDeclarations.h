﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Transform>
struct Comparison_1_t2523;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t30;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m12605(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2523 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11465_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Transform>::Invoke(T,T)
#define Comparison_1_Invoke_m12606(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2523 *, Transform_t30 *, Transform_t30 *, const MethodInfo*))Comparison_1_Invoke_m11466_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Transform>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m12607(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2523 *, Transform_t30 *, Transform_t30 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11467_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m12608(__this, ___result, method) (( int32_t (*) (Comparison_1_t2523 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11468_gshared)(__this, ___result, method)
