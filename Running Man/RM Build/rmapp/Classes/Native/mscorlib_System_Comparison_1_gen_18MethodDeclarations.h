﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.UI.Selectable>
struct Comparison_1_t2661;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t169;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.UI.Selectable>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m14409(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2661 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11465_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Selectable>::Invoke(T,T)
#define Comparison_1_Invoke_m14410(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2661 *, Selectable_t169 *, Selectable_t169 *, const MethodInfo*))Comparison_1_Invoke_m11466_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UI.Selectable>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m14411(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2661 *, Selectable_t169 *, Selectable_t169 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11467_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Selectable>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m14412(__this, ___result, method) (( int32_t (*) (Comparison_1_t2661 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11468_gshared)(__this, ___result, method)
