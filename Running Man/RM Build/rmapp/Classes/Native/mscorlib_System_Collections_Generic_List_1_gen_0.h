﻿#pragma once
#include <stdint.h>
// AudioPooler/objectSource[]
struct objectSourceU5BU5D_t2448;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<AudioPooler/objectSource>
struct  List_1_t21  : public Object_t
{
	// T[] System.Collections.Generic.List`1<AudioPooler/objectSource>::_items
	objectSourceU5BU5D_t2448* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::_version
	int32_t ____version_3;
};
struct List_1_t21_StaticFields{
	// T[] System.Collections.Generic.List`1<AudioPooler/objectSource>::EmptyArray
	objectSourceU5BU5D_t2448* ___EmptyArray_4;
};
