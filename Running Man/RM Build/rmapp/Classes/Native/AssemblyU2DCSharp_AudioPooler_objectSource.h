﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t6;
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
struct Queue_1_t7;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/objectSource
struct  objectSource_t8  : public Object_t
{
	// UnityEngine.GameObject AudioPooler/objectSource::m_audioContainer
	GameObject_t5 * ___m_audioContainer_0;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> AudioPooler/objectSource::m_spawnedMusicSource
	List_1_t6 * ___m_spawnedMusicSource_1;
	// System.Collections.Generic.Queue`1<UnityEngine.AudioSource> AudioPooler/objectSource::m_musicSourceQueue
	Queue_1_t7 * ___m_musicSourceQueue_2;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> AudioPooler/objectSource::m_spawnedSoundSource
	List_1_t6 * ___m_spawnedSoundSource_3;
	// System.Collections.Generic.Queue`1<UnityEngine.AudioSource> AudioPooler/objectSource::m_soundSourceQueue
	Queue_1_t7 * ___m_soundSourceQueue_4;
};
