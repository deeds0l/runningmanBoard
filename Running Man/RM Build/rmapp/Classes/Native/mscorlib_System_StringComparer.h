﻿#pragma once
#include <stdint.h>
// System.StringComparer
struct StringComparer_t734;
// System.Object
#include "mscorlib_System_Object.h"
// System.StringComparer
struct  StringComparer_t734  : public Object_t
{
};
struct StringComparer_t734_StaticFields{
	// System.StringComparer System.StringComparer::invariantCultureIgnoreCase
	StringComparer_t734 * ___invariantCultureIgnoreCase_0;
	// System.StringComparer System.StringComparer::invariantCulture
	StringComparer_t734 * ___invariantCulture_1;
	// System.StringComparer System.StringComparer::ordinalIgnoreCase
	StringComparer_t734 * ___ordinalIgnoreCase_2;
	// System.StringComparer System.StringComparer::ordinal
	StringComparer_t734 * ___ordinal_3;
};
