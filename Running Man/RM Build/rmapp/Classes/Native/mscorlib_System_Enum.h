﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t222;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t77  : public ValueType_t439
{
};
struct Enum_t77_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t222* ___split_char_0;
};
