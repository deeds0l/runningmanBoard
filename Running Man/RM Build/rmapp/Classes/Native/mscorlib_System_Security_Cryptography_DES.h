﻿#pragma once
#include <stdint.h>
// System.Byte[,]
struct ByteU5BU2CU5D_t1722;
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// System.Security.Cryptography.DES
struct  DES_t1353  : public SymmetricAlgorithm_t1248
{
};
struct DES_t1353_StaticFields{
	// System.Byte[,] System.Security.Cryptography.DES::weakKeys
	ByteU5BU2CU5D_t1722* ___weakKeys_10;
	// System.Byte[,] System.Security.Cryptography.DES::semiWeakKeys
	ByteU5BU2CU5D_t1722* ___semiWeakKeys_11;
};
