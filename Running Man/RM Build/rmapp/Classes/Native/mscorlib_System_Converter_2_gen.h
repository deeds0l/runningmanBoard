﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Converter`2<System.Object,System.Object>
struct  Converter_2_t2978  : public MulticastDelegate_t216
{
};
