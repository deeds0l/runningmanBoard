﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t243_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern const MethodInfo ScrollRectEvent__ctor_m977_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRectEvent__ctor_m977/* method */
	, &ScrollRectEvent_t243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRectEvent_t243_MethodInfos[] =
{
	&ScrollRectEvent__ctor_m977_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo UnityEventBase_ToString_m2098_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo;
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2129_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2130_GenericMethod;
static const Il2CppMethodReference ScrollRectEvent_t243_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2129_GenericMethod,
	&UnityEvent_1_GetDelegate_m2130_GenericMethod,
};
static bool ScrollRectEvent_t243_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
extern const Il2CppType ISerializationCallbackReceiver_t425_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t243_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRectEvent_t243_0_0_0;
extern const Il2CppType ScrollRectEvent_t243_1_0_0;
extern const Il2CppType UnityEvent_1_t244_0_0_0;
extern TypeInfo ScrollRect_t246_il2cpp_TypeInfo;
extern const Il2CppType ScrollRect_t246_0_0_0;
struct ScrollRectEvent_t243;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t243_DefinitionMetadata = 
{
	&ScrollRect_t246_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t243_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t244_0_0_0/* parent */
	, ScrollRectEvent_t243_VTable/* vtableMethods */
	, ScrollRectEvent_t243_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ScrollRectEvent_t243_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, ScrollRectEvent_t243_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScrollRectEvent_t243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t243_0_0_0/* byval_arg */
	, &ScrollRectEvent_t243_1_0_0/* this_arg */
	, &ScrollRectEvent_t243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t243)/* instance_size */
	, sizeof (ScrollRectEvent_t243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::.ctor()
extern const MethodInfo ScrollRect__ctor_m978_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRect__ctor_m978/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
extern const MethodInfo ScrollRect_get_content_m979_MethodInfo = 
{
	"get_content"/* name */
	, (methodPointerType)&ScrollRect_get_content_m979/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_content_m980_ParameterInfos[] = 
{
	{"value", 0, 134218190, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_content(UnityEngine.RectTransform)
extern const MethodInfo ScrollRect_set_content_m980_MethodInfo = 
{
	"set_content"/* name */
	, (methodPointerType)&ScrollRect_set_content_m980/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_content_m980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_horizontal()
extern const MethodInfo ScrollRect_get_horizontal_m981_MethodInfo = 
{
	"get_horizontal"/* name */
	, (methodPointerType)&ScrollRect_get_horizontal_m981/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_horizontal_m982_ParameterInfos[] = 
{
	{"value", 0, 134218191, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontal(System.Boolean)
extern const MethodInfo ScrollRect_set_horizontal_m982_MethodInfo = 
{
	"set_horizontal"/* name */
	, (methodPointerType)&ScrollRect_set_horizontal_m982/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_horizontal_m982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_vertical()
extern const MethodInfo ScrollRect_get_vertical_m983_MethodInfo = 
{
	"get_vertical"/* name */
	, (methodPointerType)&ScrollRect_get_vertical_m983/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_vertical_m984_ParameterInfos[] = 
{
	{"value", 0, 134218192, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_vertical(System.Boolean)
extern const MethodInfo ScrollRect_set_vertical_m984_MethodInfo = 
{
	"set_vertical"/* name */
	, (methodPointerType)&ScrollRect_set_vertical_m984/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_vertical_m984_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t242_0_0_0;
extern void* RuntimeInvoker_MovementType_t242 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::get_movementType()
extern const MethodInfo ScrollRect_get_movementType_m985_MethodInfo = 
{
	"get_movementType"/* name */
	, (methodPointerType)&ScrollRect_get_movementType_m985/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &MovementType_t242_0_0_0/* return_type */
	, RuntimeInvoker_MovementType_t242/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t242_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_movementType_m986_ParameterInfos[] = 
{
	{"value", 0, 134218193, 0, &MovementType_t242_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
extern const MethodInfo ScrollRect_set_movementType_m986_MethodInfo = 
{
	"set_movementType"/* name */
	, (methodPointerType)&ScrollRect_set_movementType_m986/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_movementType_m986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_elasticity()
extern const MethodInfo ScrollRect_get_elasticity_m987_MethodInfo = 
{
	"get_elasticity"/* name */
	, (methodPointerType)&ScrollRect_get_elasticity_m987/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_elasticity_m988_ParameterInfos[] = 
{
	{"value", 0, 134218194, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_elasticity(System.Single)
extern const MethodInfo ScrollRect_set_elasticity_m988_MethodInfo = 
{
	"set_elasticity"/* name */
	, (methodPointerType)&ScrollRect_set_elasticity_m988/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_elasticity_m988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_inertia()
extern const MethodInfo ScrollRect_get_inertia_m989_MethodInfo = 
{
	"get_inertia"/* name */
	, (methodPointerType)&ScrollRect_get_inertia_m989/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_inertia_m990_ParameterInfos[] = 
{
	{"value", 0, 134218195, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_inertia(System.Boolean)
extern const MethodInfo ScrollRect_set_inertia_m990_MethodInfo = 
{
	"set_inertia"/* name */
	, (methodPointerType)&ScrollRect_set_inertia_m990/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_inertia_m990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_decelerationRate()
extern const MethodInfo ScrollRect_get_decelerationRate_m991_MethodInfo = 
{
	"get_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_get_decelerationRate_m991/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_decelerationRate_m992_ParameterInfos[] = 
{
	{"value", 0, 134218196, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_decelerationRate(System.Single)
extern const MethodInfo ScrollRect_set_decelerationRate_m992_MethodInfo = 
{
	"set_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_set_decelerationRate_m992/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_decelerationRate_m992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_scrollSensitivity()
extern const MethodInfo ScrollRect_get_scrollSensitivity_m993_MethodInfo = 
{
	"get_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_get_scrollSensitivity_m993/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_scrollSensitivity_m994_ParameterInfos[] = 
{
	{"value", 0, 134218197, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_scrollSensitivity(System.Single)
extern const MethodInfo ScrollRect_set_scrollSensitivity_m994_MethodInfo = 
{
	"set_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_set_scrollSensitivity_m994/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_scrollSensitivity_m994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t239_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_horizontalScrollbar()
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m995_MethodInfo = 
{
	"get_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalScrollbar_m995/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t239_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t239_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_horizontalScrollbar_m996_ParameterInfos[] = 
{
	{"value", 0, 134218198, 0, &Scrollbar_t239_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m996_MethodInfo = 
{
	"set_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalScrollbar_m996/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_horizontalScrollbar_m996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_verticalScrollbar()
extern const MethodInfo ScrollRect_get_verticalScrollbar_m997_MethodInfo = 
{
	"get_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_verticalScrollbar_m997/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t239_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t239_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_verticalScrollbar_m998_ParameterInfos[] = 
{
	{"value", 0, 134218199, 0, &Scrollbar_t239_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_verticalScrollbar_m998_MethodInfo = 
{
	"set_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_verticalScrollbar_m998/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_verticalScrollbar_m998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::get_onValueChanged()
extern const MethodInfo ScrollRect_get_onValueChanged_m999_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_get_onValueChanged_m999/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &ScrollRectEvent_t243_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScrollRectEvent_t243_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_onValueChanged_m1000_ParameterInfos[] = 
{
	{"value", 0, 134218200, 0, &ScrollRectEvent_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_onValueChanged(UnityEngine.UI.ScrollRect/ScrollRectEvent)
extern const MethodInfo ScrollRect_set_onValueChanged_m1000_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_set_onValueChanged_m1000/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_onValueChanged_m1000_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_viewRect()
extern const MethodInfo ScrollRect_get_viewRect_m1001_MethodInfo = 
{
	"get_viewRect"/* name */
	, (methodPointerType)&ScrollRect_get_viewRect_m1001/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_velocity()
extern const MethodInfo ScrollRect_get_velocity_m1002_MethodInfo = 
{
	"get_velocity"/* name */
	, (methodPointerType)&ScrollRect_get_velocity_m1002/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_velocity_m1003_ParameterInfos[] = 
{
	{"value", 0, 134218201, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_velocity_m1003_MethodInfo = 
{
	"set_velocity"/* name */
	, (methodPointerType)&ScrollRect_set_velocity_m1003/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_velocity_m1003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t170_0_0_0;
extern const Il2CppType CanvasUpdate_t170_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_Rebuild_m1004_ParameterInfos[] = 
{
	{"executing", 0, 134218202, 0, &CanvasUpdate_t170_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo ScrollRect_Rebuild_m1004_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&ScrollRect_Rebuild_m1004/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ScrollRect_t246_ScrollRect_Rebuild_m1004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEnable()
extern const MethodInfo ScrollRect_OnEnable_m1005_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScrollRect_OnEnable_m1005/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDisable()
extern const MethodInfo ScrollRect_OnDisable_m1006_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScrollRect_OnDisable_m1006/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::IsActive()
extern const MethodInfo ScrollRect_IsActive_m1007_MethodInfo = 
{
	"IsActive"/* name */
	, (methodPointerType)&ScrollRect_IsActive_m1007/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::EnsureLayoutHasRebuilt()
extern const MethodInfo ScrollRect_EnsureLayoutHasRebuilt_m1008_MethodInfo = 
{
	"EnsureLayoutHasRebuilt"/* name */
	, (methodPointerType)&ScrollRect_EnsureLayoutHasRebuilt_m1008/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::StopMovement()
extern const MethodInfo ScrollRect_StopMovement_m1009_MethodInfo = 
{
	"StopMovement"/* name */
	, (methodPointerType)&ScrollRect_StopMovement_m1009/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_OnScroll_m1010_ParameterInfos[] = 
{
	{"data", 0, 134218203, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnScroll_m1010_MethodInfo = 
{
	"OnScroll"/* name */
	, (methodPointerType)&ScrollRect_OnScroll_m1010/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_OnScroll_m1010_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_OnInitializePotentialDrag_m1011_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1011_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&ScrollRect_OnInitializePotentialDrag_m1011/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_OnInitializePotentialDrag_m1011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_OnBeginDrag_m1012_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnBeginDrag_m1012_MethodInfo = 
{
	"OnBeginDrag"/* name */
	, (methodPointerType)&ScrollRect_OnBeginDrag_m1012/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_OnBeginDrag_m1012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_OnEndDrag_m1013_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnEndDrag_m1013_MethodInfo = 
{
	"OnEndDrag"/* name */
	, (methodPointerType)&ScrollRect_OnEndDrag_m1013/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_OnEndDrag_m1013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_OnDrag_m1014_ParameterInfos[] = 
{
	{"eventData", 0, 134218207, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnDrag_m1014_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&ScrollRect_OnDrag_m1014/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ScrollRect_t246_ScrollRect_OnDrag_m1014_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_SetContentAnchoredPosition_m1015_ParameterInfos[] = 
{
	{"position", 0, 134218208, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetContentAnchoredPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1015_MethodInfo = 
{
	"SetContentAnchoredPosition"/* name */
	, (methodPointerType)&ScrollRect_SetContentAnchoredPosition_m1015/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, ScrollRect_t246_ScrollRect_SetContentAnchoredPosition_m1015_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::LateUpdate()
extern const MethodInfo ScrollRect_LateUpdate_m1016_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ScrollRect_LateUpdate_m1016/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdatePrevData()
extern const MethodInfo ScrollRect_UpdatePrevData_m1017_MethodInfo = 
{
	"UpdatePrevData"/* name */
	, (methodPointerType)&ScrollRect_UpdatePrevData_m1017/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_UpdateScrollbars_m1018_ParameterInfos[] = 
{
	{"offset", 0, 134218209, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateScrollbars(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_UpdateScrollbars_m1018_MethodInfo = 
{
	"UpdateScrollbars"/* name */
	, (methodPointerType)&ScrollRect_UpdateScrollbars_m1018/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, ScrollRect_t246_ScrollRect_UpdateScrollbars_m1018_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_normalizedPosition()
extern const MethodInfo ScrollRect_get_normalizedPosition_m1019_MethodInfo = 
{
	"get_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_normalizedPosition_m1019/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_normalizedPosition_m1020_ParameterInfos[] = 
{
	{"value", 0, 134218210, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_normalizedPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_normalizedPosition_m1020_MethodInfo = 
{
	"set_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_normalizedPosition_m1020/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_normalizedPosition_m1020_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_horizontalNormalizedPosition()
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1021_MethodInfo = 
{
	"get_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalNormalizedPosition_m1021/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_horizontalNormalizedPosition_m1022_ParameterInfos[] = 
{
	{"value", 0, 134218211, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1022_MethodInfo = 
{
	"set_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalNormalizedPosition_m1022/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_horizontalNormalizedPosition_m1022_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_verticalNormalizedPosition()
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1023_MethodInfo = 
{
	"get_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_verticalNormalizedPosition_m1023/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_set_verticalNormalizedPosition_m1024_ParameterInfos[] = 
{
	{"value", 0, 134218212, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1024_MethodInfo = 
{
	"set_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_verticalNormalizedPosition_m1024/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_set_verticalNormalizedPosition_m1024_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_SetHorizontalNormalizedPosition_m1025_ParameterInfos[] = 
{
	{"value", 0, 134218213, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetHorizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetHorizontalNormalizedPosition_m1025_MethodInfo = 
{
	"SetHorizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetHorizontalNormalizedPosition_m1025/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_SetHorizontalNormalizedPosition_m1025_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_SetVerticalNormalizedPosition_m1026_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetVerticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetVerticalNormalizedPosition_m1026_MethodInfo = 
{
	"SetVerticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetVerticalNormalizedPosition_m1026/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_SetVerticalNormalizedPosition_m1026_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_SetNormalizedPosition_m1027_ParameterInfos[] = 
{
	{"value", 0, 134218215, 0, &Single_t85_0_0_0},
	{"axis", 1, 134218216, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetNormalizedPosition(System.Single,System.Int32)
extern const MethodInfo ScrollRect_SetNormalizedPosition_m1027_MethodInfo = 
{
	"SetNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetNormalizedPosition_m1027/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85_Int32_t54/* invoker_method */
	, ScrollRect_t246_ScrollRect_SetNormalizedPosition_m1027_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_RubberDelta_m1028_ParameterInfos[] = 
{
	{"overStretching", 0, 134218217, 0, &Single_t85_0_0_0},
	{"viewSize", 1, 134218218, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::RubberDelta(System.Single,System.Single)
extern const MethodInfo ScrollRect_RubberDelta_m1028_MethodInfo = 
{
	"RubberDelta"/* name */
	, (methodPointerType)&ScrollRect_RubberDelta_m1028/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Single_t85_Single_t85/* invoker_method */
	, ScrollRect_t246_ScrollRect_RubberDelta_m1028_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateBounds()
extern const MethodInfo ScrollRect_UpdateBounds_m1029_MethodInfo = 
{
	"UpdateBounds"/* name */
	, (methodPointerType)&ScrollRect_UpdateBounds_m1029/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Bounds_t247_0_0_0;
extern void* RuntimeInvoker_Bounds_t247 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Bounds UnityEngine.UI.ScrollRect::GetBounds()
extern const MethodInfo ScrollRect_GetBounds_m1030_MethodInfo = 
{
	"GetBounds"/* name */
	, (methodPointerType)&ScrollRect_GetBounds_m1030/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Bounds_t247_0_0_0/* return_type */
	, RuntimeInvoker_Bounds_t247/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo ScrollRect_t246_ScrollRect_CalculateOffset_m1031_ParameterInfos[] = 
{
	{"delta", 0, 134218219, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::CalculateOffset(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_CalculateOffset_m1031_MethodInfo = 
{
	"CalculateOffset"/* name */
	, (methodPointerType)&ScrollRect_CalculateOffset_m1031/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53_Vector2_t53/* invoker_method */
	, ScrollRect_t246_ScrollRect_CalculateOffset_m1031_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033/* method */
	, &ScrollRect_t246_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRect_t246_MethodInfos[] =
{
	&ScrollRect__ctor_m978_MethodInfo,
	&ScrollRect_get_content_m979_MethodInfo,
	&ScrollRect_set_content_m980_MethodInfo,
	&ScrollRect_get_horizontal_m981_MethodInfo,
	&ScrollRect_set_horizontal_m982_MethodInfo,
	&ScrollRect_get_vertical_m983_MethodInfo,
	&ScrollRect_set_vertical_m984_MethodInfo,
	&ScrollRect_get_movementType_m985_MethodInfo,
	&ScrollRect_set_movementType_m986_MethodInfo,
	&ScrollRect_get_elasticity_m987_MethodInfo,
	&ScrollRect_set_elasticity_m988_MethodInfo,
	&ScrollRect_get_inertia_m989_MethodInfo,
	&ScrollRect_set_inertia_m990_MethodInfo,
	&ScrollRect_get_decelerationRate_m991_MethodInfo,
	&ScrollRect_set_decelerationRate_m992_MethodInfo,
	&ScrollRect_get_scrollSensitivity_m993_MethodInfo,
	&ScrollRect_set_scrollSensitivity_m994_MethodInfo,
	&ScrollRect_get_horizontalScrollbar_m995_MethodInfo,
	&ScrollRect_set_horizontalScrollbar_m996_MethodInfo,
	&ScrollRect_get_verticalScrollbar_m997_MethodInfo,
	&ScrollRect_set_verticalScrollbar_m998_MethodInfo,
	&ScrollRect_get_onValueChanged_m999_MethodInfo,
	&ScrollRect_set_onValueChanged_m1000_MethodInfo,
	&ScrollRect_get_viewRect_m1001_MethodInfo,
	&ScrollRect_get_velocity_m1002_MethodInfo,
	&ScrollRect_set_velocity_m1003_MethodInfo,
	&ScrollRect_Rebuild_m1004_MethodInfo,
	&ScrollRect_OnEnable_m1005_MethodInfo,
	&ScrollRect_OnDisable_m1006_MethodInfo,
	&ScrollRect_IsActive_m1007_MethodInfo,
	&ScrollRect_EnsureLayoutHasRebuilt_m1008_MethodInfo,
	&ScrollRect_StopMovement_m1009_MethodInfo,
	&ScrollRect_OnScroll_m1010_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1011_MethodInfo,
	&ScrollRect_OnBeginDrag_m1012_MethodInfo,
	&ScrollRect_OnEndDrag_m1013_MethodInfo,
	&ScrollRect_OnDrag_m1014_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1015_MethodInfo,
	&ScrollRect_LateUpdate_m1016_MethodInfo,
	&ScrollRect_UpdatePrevData_m1017_MethodInfo,
	&ScrollRect_UpdateScrollbars_m1018_MethodInfo,
	&ScrollRect_get_normalizedPosition_m1019_MethodInfo,
	&ScrollRect_set_normalizedPosition_m1020_MethodInfo,
	&ScrollRect_get_horizontalNormalizedPosition_m1021_MethodInfo,
	&ScrollRect_set_horizontalNormalizedPosition_m1022_MethodInfo,
	&ScrollRect_get_verticalNormalizedPosition_m1023_MethodInfo,
	&ScrollRect_set_verticalNormalizedPosition_m1024_MethodInfo,
	&ScrollRect_SetHorizontalNormalizedPosition_m1025_MethodInfo,
	&ScrollRect_SetVerticalNormalizedPosition_m1026_MethodInfo,
	&ScrollRect_SetNormalizedPosition_m1027_MethodInfo,
	&ScrollRect_RubberDelta_m1028_MethodInfo,
	&ScrollRect_UpdateBounds_m1029_MethodInfo,
	&ScrollRect_GetBounds_m1030_MethodInfo,
	&ScrollRect_CalculateOffset_m1031_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033_MethodInfo,
	NULL
};
extern const MethodInfo ScrollRect_get_content_m979_MethodInfo;
extern const MethodInfo ScrollRect_set_content_m980_MethodInfo;
static const PropertyInfo ScrollRect_t246____content_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "content"/* name */
	, &ScrollRect_get_content_m979_MethodInfo/* get */
	, &ScrollRect_set_content_m980_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontal_m981_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontal_m982_MethodInfo;
static const PropertyInfo ScrollRect_t246____horizontal_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "horizontal"/* name */
	, &ScrollRect_get_horizontal_m981_MethodInfo/* get */
	, &ScrollRect_set_horizontal_m982_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_vertical_m983_MethodInfo;
extern const MethodInfo ScrollRect_set_vertical_m984_MethodInfo;
static const PropertyInfo ScrollRect_t246____vertical_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "vertical"/* name */
	, &ScrollRect_get_vertical_m983_MethodInfo/* get */
	, &ScrollRect_set_vertical_m984_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_movementType_m985_MethodInfo;
extern const MethodInfo ScrollRect_set_movementType_m986_MethodInfo;
static const PropertyInfo ScrollRect_t246____movementType_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "movementType"/* name */
	, &ScrollRect_get_movementType_m985_MethodInfo/* get */
	, &ScrollRect_set_movementType_m986_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_elasticity_m987_MethodInfo;
extern const MethodInfo ScrollRect_set_elasticity_m988_MethodInfo;
static const PropertyInfo ScrollRect_t246____elasticity_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "elasticity"/* name */
	, &ScrollRect_get_elasticity_m987_MethodInfo/* get */
	, &ScrollRect_set_elasticity_m988_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_inertia_m989_MethodInfo;
extern const MethodInfo ScrollRect_set_inertia_m990_MethodInfo;
static const PropertyInfo ScrollRect_t246____inertia_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "inertia"/* name */
	, &ScrollRect_get_inertia_m989_MethodInfo/* get */
	, &ScrollRect_set_inertia_m990_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_decelerationRate_m991_MethodInfo;
extern const MethodInfo ScrollRect_set_decelerationRate_m992_MethodInfo;
static const PropertyInfo ScrollRect_t246____decelerationRate_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "decelerationRate"/* name */
	, &ScrollRect_get_decelerationRate_m991_MethodInfo/* get */
	, &ScrollRect_set_decelerationRate_m992_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_scrollSensitivity_m993_MethodInfo;
extern const MethodInfo ScrollRect_set_scrollSensitivity_m994_MethodInfo;
static const PropertyInfo ScrollRect_t246____scrollSensitivity_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "scrollSensitivity"/* name */
	, &ScrollRect_get_scrollSensitivity_m993_MethodInfo/* get */
	, &ScrollRect_set_scrollSensitivity_m994_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m995_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m996_MethodInfo;
static const PropertyInfo ScrollRect_t246____horizontalScrollbar_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "horizontalScrollbar"/* name */
	, &ScrollRect_get_horizontalScrollbar_m995_MethodInfo/* get */
	, &ScrollRect_set_horizontalScrollbar_m996_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalScrollbar_m997_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalScrollbar_m998_MethodInfo;
static const PropertyInfo ScrollRect_t246____verticalScrollbar_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "verticalScrollbar"/* name */
	, &ScrollRect_get_verticalScrollbar_m997_MethodInfo/* get */
	, &ScrollRect_set_verticalScrollbar_m998_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_onValueChanged_m999_MethodInfo;
extern const MethodInfo ScrollRect_set_onValueChanged_m1000_MethodInfo;
static const PropertyInfo ScrollRect_t246____onValueChanged_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &ScrollRect_get_onValueChanged_m999_MethodInfo/* get */
	, &ScrollRect_set_onValueChanged_m1000_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_viewRect_m1001_MethodInfo;
static const PropertyInfo ScrollRect_t246____viewRect_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "viewRect"/* name */
	, &ScrollRect_get_viewRect_m1001_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_velocity_m1002_MethodInfo;
extern const MethodInfo ScrollRect_set_velocity_m1003_MethodInfo;
static const PropertyInfo ScrollRect_t246____velocity_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "velocity"/* name */
	, &ScrollRect_get_velocity_m1002_MethodInfo/* get */
	, &ScrollRect_set_velocity_m1003_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_normalizedPosition_m1019_MethodInfo;
extern const MethodInfo ScrollRect_set_normalizedPosition_m1020_MethodInfo;
static const PropertyInfo ScrollRect_t246____normalizedPosition_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "normalizedPosition"/* name */
	, &ScrollRect_get_normalizedPosition_m1019_MethodInfo/* get */
	, &ScrollRect_set_normalizedPosition_m1020_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1021_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1022_MethodInfo;
static const PropertyInfo ScrollRect_t246____horizontalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "horizontalNormalizedPosition"/* name */
	, &ScrollRect_get_horizontalNormalizedPosition_m1021_MethodInfo/* get */
	, &ScrollRect_set_horizontalNormalizedPosition_m1022_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1023_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1024_MethodInfo;
static const PropertyInfo ScrollRect_t246____verticalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t246_il2cpp_TypeInfo/* parent */
	, "verticalNormalizedPosition"/* name */
	, &ScrollRect_get_verticalNormalizedPosition_m1023_MethodInfo/* get */
	, &ScrollRect_set_verticalNormalizedPosition_m1024_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ScrollRect_t246_PropertyInfos[] =
{
	&ScrollRect_t246____content_PropertyInfo,
	&ScrollRect_t246____horizontal_PropertyInfo,
	&ScrollRect_t246____vertical_PropertyInfo,
	&ScrollRect_t246____movementType_PropertyInfo,
	&ScrollRect_t246____elasticity_PropertyInfo,
	&ScrollRect_t246____inertia_PropertyInfo,
	&ScrollRect_t246____decelerationRate_PropertyInfo,
	&ScrollRect_t246____scrollSensitivity_PropertyInfo,
	&ScrollRect_t246____horizontalScrollbar_PropertyInfo,
	&ScrollRect_t246____verticalScrollbar_PropertyInfo,
	&ScrollRect_t246____onValueChanged_PropertyInfo,
	&ScrollRect_t246____viewRect_PropertyInfo,
	&ScrollRect_t246____velocity_PropertyInfo,
	&ScrollRect_t246____normalizedPosition_PropertyInfo,
	&ScrollRect_t246____horizontalNormalizedPosition_PropertyInfo,
	&ScrollRect_t246____verticalNormalizedPosition_PropertyInfo,
	NULL
};
static const Il2CppType* ScrollRect_t246_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t242_0_0_0,
	&ScrollRectEvent_t243_0_0_0,
};
extern const MethodInfo Object_Equals_m217_MethodInfo;
extern const MethodInfo Object_GetHashCode_m219_MethodInfo;
extern const MethodInfo Object_ToString_m220_MethodInfo;
extern const MethodInfo UIBehaviour_Awake_m365_MethodInfo;
extern const MethodInfo ScrollRect_OnEnable_m1005_MethodInfo;
extern const MethodInfo UIBehaviour_Start_m367_MethodInfo;
extern const MethodInfo ScrollRect_OnDisable_m1006_MethodInfo;
extern const MethodInfo UIBehaviour_OnDestroy_m369_MethodInfo;
extern const MethodInfo ScrollRect_IsActive_m1007_MethodInfo;
extern const MethodInfo UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo;
extern const MethodInfo UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo;
extern const MethodInfo UIBehaviour_OnTransformParentChanged_m373_MethodInfo;
extern const MethodInfo UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo;
extern const MethodInfo ScrollRect_OnBeginDrag_m1012_MethodInfo;
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1011_MethodInfo;
extern const MethodInfo ScrollRect_OnDrag_m1014_MethodInfo;
extern const MethodInfo ScrollRect_OnEndDrag_m1013_MethodInfo;
extern const MethodInfo ScrollRect_OnScroll_m1010_MethodInfo;
extern const MethodInfo ScrollRect_Rebuild_m1004_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032_MethodInfo;
extern const MethodInfo ScrollRect_StopMovement_m1009_MethodInfo;
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1015_MethodInfo;
extern const MethodInfo ScrollRect_LateUpdate_m1016_MethodInfo;
static const Il2CppMethodReference ScrollRect_t246_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&ScrollRect_OnEnable_m1005_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&ScrollRect_OnDisable_m1006_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&ScrollRect_IsActive_m1007_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&ScrollRect_OnBeginDrag_m1012_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1011_MethodInfo,
	&ScrollRect_OnDrag_m1014_MethodInfo,
	&ScrollRect_OnEndDrag_m1013_MethodInfo,
	&ScrollRect_OnScroll_m1010_MethodInfo,
	&ScrollRect_Rebuild_m1004_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032_MethodInfo,
	&ScrollRect_Rebuild_m1004_MethodInfo,
	&ScrollRect_StopMovement_m1009_MethodInfo,
	&ScrollRect_OnScroll_m1010_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1011_MethodInfo,
	&ScrollRect_OnBeginDrag_m1012_MethodInfo,
	&ScrollRect_OnEndDrag_m1013_MethodInfo,
	&ScrollRect_OnDrag_m1014_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1015_MethodInfo,
	&ScrollRect_LateUpdate_m1016_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1032_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1033_MethodInfo,
};
static bool ScrollRect_t246_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEventSystemHandler_t407_0_0_0;
extern const Il2CppType IBeginDragHandler_t309_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t308_0_0_0;
extern const Il2CppType IDragHandler_t310_0_0_0;
extern const Il2CppType IEndDragHandler_t311_0_0_0;
extern const Il2CppType IScrollHandler_t313_0_0_0;
extern const Il2CppType ICanvasElement_t325_0_0_0;
static const Il2CppType* ScrollRect_t246_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t407_0_0_0,
	&IBeginDragHandler_t309_0_0_0,
	&IInitializePotentialDragHandler_t308_0_0_0,
	&IDragHandler_t310_0_0_0,
	&IEndDragHandler_t311_0_0_0,
	&IScrollHandler_t313_0_0_0,
	&ICanvasElement_t325_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t246_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t407_0_0_0, 16},
	{ &IBeginDragHandler_t309_0_0_0, 16},
	{ &IInitializePotentialDragHandler_t308_0_0_0, 17},
	{ &IDragHandler_t310_0_0_0, 18},
	{ &IEndDragHandler_t311_0_0_0, 19},
	{ &IScrollHandler_t313_0_0_0, 20},
	{ &ICanvasElement_t325_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRect_t246_1_0_0;
extern const Il2CppType UIBehaviour_t105_0_0_0;
struct ScrollRect_t246;
const Il2CppTypeDefinitionMetadata ScrollRect_t246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t246_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t246_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t246_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, ScrollRect_t246_VTable/* vtableMethods */
	, ScrollRect_t246_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 418/* fieldStart */

};
TypeInfo ScrollRect_t246_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ScrollRect_t246_MethodInfos/* methods */
	, ScrollRect_t246_PropertyInfos/* properties */
	, NULL/* events */
	, &ScrollRect_t246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 213/* custom_attributes_cache */
	, &ScrollRect_t246_0_0_0/* byval_arg */
	, &ScrollRect_t246_1_0_0/* this_arg */
	, &ScrollRect_t246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t246)/* instance_size */
	, sizeof (ScrollRect_t246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 35/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t248_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static const MethodInfo* Transition_t248_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference Transition_t248_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Transition_t248_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair Transition_t248_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Transition_t248_0_0_0;
extern const Il2CppType Transition_t248_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
extern TypeInfo Selectable_t169_il2cpp_TypeInfo;
extern const Il2CppType Selectable_t169_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Transition_t248_DefinitionMetadata = 
{
	&Selectable_t169_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t248_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Transition_t248_VTable/* vtableMethods */
	, Transition_t248_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 441/* fieldStart */

};
TypeInfo Transition_t248_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, Transition_t248_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t248_0_0_0/* byval_arg */
	, &Transition_t248_1_0_0/* this_arg */
	, &Transition_t248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t248)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t248)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t249_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static const MethodInfo* SelectionState_t249_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SelectionState_t249_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool SelectionState_t249_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionState_t249_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SelectionState_t249_0_0_0;
extern const Il2CppType SelectionState_t249_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t249_DefinitionMetadata = 
{
	&Selectable_t169_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t249_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, SelectionState_t249_VTable/* vtableMethods */
	, SelectionState_t249_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 446/* fieldStart */

};
TypeInfo SelectionState_t249_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, SelectionState_t249_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t249_0_0_0/* byval_arg */
	, &SelectionState_t249_1_0_0/* this_arg */
	, &SelectionState_t249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t249)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t249)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.ctor()
extern const MethodInfo Selectable__ctor_m1034_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Selectable__ctor_m1034/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.cctor()
extern const MethodInfo Selectable__cctor_m1035_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Selectable__cctor_m1035/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t250_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::get_allSelectables()
extern const MethodInfo Selectable_get_allSelectables_m1036_MethodInfo = 
{
	"get_allSelectables"/* name */
	, (methodPointerType)&Selectable_get_allSelectables_m1036/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t250_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t232_0_0_0;
extern void* RuntimeInvoker_Navigation_t232 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::get_navigation()
extern const MethodInfo Selectable_get_navigation_m1037_MethodInfo = 
{
	"get_navigation"/* name */
	, (methodPointerType)&Selectable_get_navigation_m1037/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Navigation_t232_0_0_0/* return_type */
	, RuntimeInvoker_Navigation_t232/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t232_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_navigation_m1038_ParameterInfos[] = 
{
	{"value", 0, 134218220, 0, &Navigation_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Navigation_t232 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_navigation(UnityEngine.UI.Navigation)
extern const MethodInfo Selectable_set_navigation_m1038_MethodInfo = 
{
	"set_navigation"/* name */
	, (methodPointerType)&Selectable_set_navigation_m1038/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Navigation_t232/* invoker_method */
	, Selectable_t169_Selectable_set_navigation_m1038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Transition_t248 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::get_transition()
extern const MethodInfo Selectable_get_transition_m1039_MethodInfo = 
{
	"get_transition"/* name */
	, (methodPointerType)&Selectable_get_transition_m1039/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Transition_t248_0_0_0/* return_type */
	, RuntimeInvoker_Transition_t248/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transition_t248_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_transition_m1040_ParameterInfos[] = 
{
	{"value", 0, 134218221, 0, &Transition_t248_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_transition(UnityEngine.UI.Selectable/Transition)
extern const MethodInfo Selectable_set_transition_m1040_MethodInfo = 
{
	"set_transition"/* name */
	, (methodPointerType)&Selectable_set_transition_m1040/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Selectable_t169_Selectable_set_transition_m1040_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t175_0_0_0;
extern void* RuntimeInvoker_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern const MethodInfo Selectable_get_colors_m1041_MethodInfo = 
{
	"get_colors"/* name */
	, (methodPointerType)&Selectable_get_colors_m1041/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &ColorBlock_t175_0_0_0/* return_type */
	, RuntimeInvoker_ColorBlock_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t175_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_colors_m1042_ParameterInfos[] = 
{
	{"value", 0, 134218222, 0, &ColorBlock_t175_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern const MethodInfo Selectable_set_colors_m1042_MethodInfo = 
{
	"set_colors"/* name */
	, (methodPointerType)&Selectable_set_colors_m1042/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_ColorBlock_t175/* invoker_method */
	, Selectable_t169_Selectable_set_colors_m1042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t252_0_0_0;
extern void* RuntimeInvoker_SpriteState_t252 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::get_spriteState()
extern const MethodInfo Selectable_get_spriteState_m1043_MethodInfo = 
{
	"get_spriteState"/* name */
	, (methodPointerType)&Selectable_get_spriteState_m1043/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &SpriteState_t252_0_0_0/* return_type */
	, RuntimeInvoker_SpriteState_t252/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t252_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_spriteState_m1044_ParameterInfos[] = 
{
	{"value", 0, 134218223, 0, &SpriteState_t252_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SpriteState_t252 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_spriteState(UnityEngine.UI.SpriteState)
extern const MethodInfo Selectable_set_spriteState_m1044_MethodInfo = 
{
	"set_spriteState"/* name */
	, (methodPointerType)&Selectable_set_spriteState_m1044/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SpriteState_t252/* invoker_method */
	, Selectable_t169_Selectable_set_spriteState_m1044_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::get_animationTriggers()
extern const MethodInfo Selectable_get_animationTriggers_m1045_MethodInfo = 
{
	"get_animationTriggers"/* name */
	, (methodPointerType)&Selectable_get_animationTriggers_m1045/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &AnimationTriggers_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t164_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_animationTriggers_m1046_ParameterInfos[] = 
{
	{"value", 0, 134218224, 0, &AnimationTriggers_t164_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_animationTriggers(UnityEngine.UI.AnimationTriggers)
extern const MethodInfo Selectable_set_animationTriggers_m1046_MethodInfo = 
{
	"set_animationTriggers"/* name */
	, (methodPointerType)&Selectable_set_animationTriggers_m1046/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_set_animationTriggers_m1046_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t188_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::get_targetGraphic()
extern const MethodInfo Selectable_get_targetGraphic_m1047_MethodInfo = 
{
	"get_targetGraphic"/* name */
	, (methodPointerType)&Selectable_get_targetGraphic_m1047/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t188_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t188_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_targetGraphic_m1048_ParameterInfos[] = 
{
	{"value", 0, 134218225, 0, &Graphic_t188_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
extern const MethodInfo Selectable_set_targetGraphic_m1048_MethodInfo = 
{
	"set_targetGraphic"/* name */
	, (methodPointerType)&Selectable_set_targetGraphic_m1048/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_set_targetGraphic_m1048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
extern const MethodInfo Selectable_get_interactable_m1049_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&Selectable_get_interactable_m1049/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_interactable_m1050_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern const MethodInfo Selectable_set_interactable_m1050_MethodInfo = 
{
	"set_interactable"/* name */
	, (methodPointerType)&Selectable_set_interactable_m1050/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_set_interactable_m1050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerInside()
extern const MethodInfo Selectable_get_isPointerInside_m1051_MethodInfo = 
{
	"get_isPointerInside"/* name */
	, (methodPointerType)&Selectable_get_isPointerInside_m1051/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 236/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_isPointerInside_m1052_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerInside(System.Boolean)
extern const MethodInfo Selectable_set_isPointerInside_m1052_MethodInfo = 
{
	"set_isPointerInside"/* name */
	, (methodPointerType)&Selectable_set_isPointerInside_m1052/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_set_isPointerInside_m1052_ParameterInfos/* parameters */
	, 237/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerDown()
extern const MethodInfo Selectable_get_isPointerDown_m1053_MethodInfo = 
{
	"get_isPointerDown"/* name */
	, (methodPointerType)&Selectable_get_isPointerDown_m1053/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 238/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_isPointerDown_m1054_ParameterInfos[] = 
{
	{"value", 0, 134218228, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerDown(System.Boolean)
extern const MethodInfo Selectable_set_isPointerDown_m1054_MethodInfo = 
{
	"set_isPointerDown"/* name */
	, (methodPointerType)&Selectable_set_isPointerDown_m1054/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_set_isPointerDown_m1054_ParameterInfos/* parameters */
	, 239/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_hasSelection()
extern const MethodInfo Selectable_get_hasSelection_m1055_MethodInfo = 
{
	"get_hasSelection"/* name */
	, (methodPointerType)&Selectable_get_hasSelection_m1055/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 240/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_hasSelection_m1056_ParameterInfos[] = 
{
	{"value", 0, 134218229, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_hasSelection(System.Boolean)
extern const MethodInfo Selectable_set_hasSelection_m1056_MethodInfo = 
{
	"set_hasSelection"/* name */
	, (methodPointerType)&Selectable_set_hasSelection_m1056/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_set_hasSelection_m1056_ParameterInfos/* parameters */
	, 241/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t203_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
extern const MethodInfo Selectable_get_image_m1057_MethodInfo = 
{
	"get_image"/* name */
	, (methodPointerType)&Selectable_get_image_m1057/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Image_t203_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t203_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_set_image_m1058_ParameterInfos[] = 
{
	{"value", 0, 134218230, 0, &Image_t203_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_image(UnityEngine.UI.Image)
extern const MethodInfo Selectable_set_image_m1058_MethodInfo = 
{
	"set_image"/* name */
	, (methodPointerType)&Selectable_set_image_m1058/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_set_image_m1058_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator UnityEngine.UI.Selectable::get_animator()
extern const MethodInfo Selectable_get_animator_m1059_MethodInfo = 
{
	"get_animator"/* name */
	, (methodPointerType)&Selectable_get_animator_m1059/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t329_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Awake()
extern const MethodInfo Selectable_Awake_m1060_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Selectable_Awake_m1060/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnCanvasGroupChanged()
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1061_MethodInfo = 
{
	"OnCanvasGroupChanged"/* name */
	, (methodPointerType)&Selectable_OnCanvasGroupChanged_m1061/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsInteractable()
extern const MethodInfo Selectable_IsInteractable_m1062_MethodInfo = 
{
	"IsInteractable"/* name */
	, (methodPointerType)&Selectable_IsInteractable_m1062/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDidApplyAnimationProperties()
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&Selectable_OnDidApplyAnimationProperties_m1063/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnEnable()
extern const MethodInfo Selectable_OnEnable_m1064_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Selectable_OnEnable_m1064/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSetProperty()
extern const MethodInfo Selectable_OnSetProperty_m1065_MethodInfo = 
{
	"OnSetProperty"/* name */
	, (methodPointerType)&Selectable_OnSetProperty_m1065/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDisable()
extern const MethodInfo Selectable_OnDisable_m1066_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Selectable_OnDisable_m1066/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SelectionState_t249 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::get_currentSelectionState()
extern const MethodInfo Selectable_get_currentSelectionState_m1067_MethodInfo = 
{
	"get_currentSelectionState"/* name */
	, (methodPointerType)&Selectable_get_currentSelectionState_m1067/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &SelectionState_t249_0_0_0/* return_type */
	, RuntimeInvoker_SelectionState_t249/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InstantClearState()
extern const MethodInfo Selectable_InstantClearState_m1068_MethodInfo = 
{
	"InstantClearState"/* name */
	, (methodPointerType)&Selectable_InstantClearState_m1068/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SelectionState_t249_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_DoStateTransition_m1069_ParameterInfos[] = 
{
	{"state", 0, 134218231, 0, &SelectionState_t249_0_0_0},
	{"instant", 1, 134218232, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoStateTransition(UnityEngine.UI.Selectable/SelectionState,System.Boolean)
extern const MethodInfo Selectable_DoStateTransition_m1069_MethodInfo = 
{
	"DoStateTransition"/* name */
	, (methodPointerType)&Selectable_DoStateTransition_m1069/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_DoStateTransition_m1069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t138_0_0_0;
extern const Il2CppType Vector3_t138_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_FindSelectable_m1070_ParameterInfos[] = 
{
	{"dir", 0, 134218233, 0, &Vector3_t138_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectable(UnityEngine.Vector3)
extern const MethodInfo Selectable_FindSelectable_m1070_MethodInfo = 
{
	"FindSelectable"/* name */
	, (methodPointerType)&Selectable_FindSelectable_m1070/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t138/* invoker_method */
	, Selectable_t169_Selectable_FindSelectable_m1070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_GetPointOnRectEdge_m1071_ParameterInfos[] = 
{
	{"rect", 0, 134218234, 0, &RectTransform_t183_0_0_0},
	{"dir", 1, 134218235, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t138_Object_t_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityEngine.UI.Selectable::GetPointOnRectEdge(UnityEngine.RectTransform,UnityEngine.Vector2)
extern const MethodInfo Selectable_GetPointOnRectEdge_m1071_MethodInfo = 
{
	"GetPointOnRectEdge"/* name */
	, (methodPointerType)&Selectable_GetPointOnRectEdge_m1071/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t138_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t138_Object_t_Vector2_t53/* invoker_method */
	, Selectable_t169_Selectable_GetPointOnRectEdge_m1071_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t139_0_0_0;
extern const Il2CppType AxisEventData_t139_0_0_0;
extern const Il2CppType Selectable_t169_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_Navigate_m1072_ParameterInfos[] = 
{
	{"eventData", 0, 134218236, 0, &AxisEventData_t139_0_0_0},
	{"sel", 1, 134218237, 0, &Selectable_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Navigate(UnityEngine.EventSystems.AxisEventData,UnityEngine.UI.Selectable)
extern const MethodInfo Selectable_Navigate_m1072_MethodInfo = 
{
	"Navigate"/* name */
	, (methodPointerType)&Selectable_Navigate_m1072/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Selectable_t169_Selectable_Navigate_m1072_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnLeft()
extern const MethodInfo Selectable_FindSelectableOnLeft_m1073_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnLeft_m1073/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnRight()
extern const MethodInfo Selectable_FindSelectableOnRight_m1074_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnRight_m1074/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnUp()
extern const MethodInfo Selectable_FindSelectableOnUp_m1075_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnUp_m1075/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnDown()
extern const MethodInfo Selectable_FindSelectableOnDown_m1076_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnDown_m1076/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t139_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnMove_m1077_ParameterInfos[] = 
{
	{"eventData", 0, 134218238, 0, &AxisEventData_t139_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Selectable_OnMove_m1077_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Selectable_OnMove_m1077/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnMove_m1077_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t163_0_0_0;
extern const Il2CppType Color_t163_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_StartColorTween_m1078_ParameterInfos[] = 
{
	{"targetColor", 0, 134218239, 0, &Color_t163_0_0_0},
	{"instant", 1, 134218240, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Color_t163_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::StartColorTween(UnityEngine.Color,System.Boolean)
extern const MethodInfo Selectable_StartColorTween_m1078_MethodInfo = 
{
	"StartColorTween"/* name */
	, (methodPointerType)&Selectable_StartColorTween_m1078/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Color_t163_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_StartColorTween_m1078_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t201_0_0_0;
extern const Il2CppType Sprite_t201_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_DoSpriteSwap_m1079_ParameterInfos[] = 
{
	{"newSprite", 0, 134218241, 0, &Sprite_t201_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoSpriteSwap(UnityEngine.Sprite)
extern const MethodInfo Selectable_DoSpriteSwap_m1079_MethodInfo = 
{
	"DoSpriteSwap"/* name */
	, (methodPointerType)&Selectable_DoSpriteSwap_m1079/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_DoSpriteSwap_m1079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_TriggerAnimation_m1080_ParameterInfos[] = 
{
	{"triggername", 0, 134218242, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::TriggerAnimation(System.String)
extern const MethodInfo Selectable_TriggerAnimation_m1080_MethodInfo = 
{
	"TriggerAnimation"/* name */
	, (methodPointerType)&Selectable_TriggerAnimation_m1080/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_TriggerAnimation_m1080_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_IsHighlighted_m1081_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsHighlighted(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsHighlighted_m1081_MethodInfo = 
{
	"IsHighlighted"/* name */
	, (methodPointerType)&Selectable_IsHighlighted_m1081/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Selectable_t169_Selectable_IsHighlighted_m1081_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_IsPressed_m1082_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsPressed_m1082_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1082/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Selectable_t169_Selectable_IsPressed_m1082_ParameterInfos/* parameters */
	, 242/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed()
extern const MethodInfo Selectable_IsPressed_m1083_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1083/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_UpdateSelectionState_m1084_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::UpdateSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_UpdateSelectionState_m1084_MethodInfo = 
{
	"UpdateSelectionState"/* name */
	, (methodPointerType)&Selectable_UpdateSelectionState_m1084/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_UpdateSelectionState_m1084_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_EvaluateAndTransitionToSelectionState_m1085_ParameterInfos[] = 
{
	{"eventData", 0, 134218246, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::EvaluateAndTransitionToSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_EvaluateAndTransitionToSelectionState_m1085_MethodInfo = 
{
	"EvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_EvaluateAndTransitionToSelectionState_m1085/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_EvaluateAndTransitionToSelectionState_m1085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_InternalEvaluateAndTransitionToSelectionState_m1086_ParameterInfos[] = 
{
	{"instant", 0, 134218247, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InternalEvaluateAndTransitionToSelectionState(System.Boolean)
extern const MethodInfo Selectable_InternalEvaluateAndTransitionToSelectionState_m1086_MethodInfo = 
{
	"InternalEvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_InternalEvaluateAndTransitionToSelectionState_m1086/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Selectable_t169_Selectable_InternalEvaluateAndTransitionToSelectionState_m1086_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnPointerDown_m1087_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerDown_m1087_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Selectable_OnPointerDown_m1087/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnPointerDown_m1087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnPointerUp_m1088_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerUp_m1088_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Selectable_OnPointerUp_m1088/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnPointerUp_m1088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnPointerEnter_m1089_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerEnter_m1089_MethodInfo = 
{
	"OnPointerEnter"/* name */
	, (methodPointerType)&Selectable_OnPointerEnter_m1089/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnPointerEnter_m1089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnPointerExit_m1090_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerExit_m1090_MethodInfo = 
{
	"OnPointerExit"/* name */
	, (methodPointerType)&Selectable_OnPointerExit_m1090/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnPointerExit_m1090_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnSelect_m1091_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnSelect_m1091_MethodInfo = 
{
	"OnSelect"/* name */
	, (methodPointerType)&Selectable_OnSelect_m1091/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnSelect_m1091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Selectable_t169_Selectable_OnDeselect_m1092_ParameterInfos[] = 
{
	{"eventData", 0, 134218253, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnDeselect_m1092_MethodInfo = 
{
	"OnDeselect"/* name */
	, (methodPointerType)&Selectable_OnDeselect_m1092/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Selectable_t169_Selectable_OnDeselect_m1092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Select()
extern const MethodInfo Selectable_Select_m1093_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&Selectable_Select_m1093/* method */
	, &Selectable_t169_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Selectable_t169_MethodInfos[] =
{
	&Selectable__ctor_m1034_MethodInfo,
	&Selectable__cctor_m1035_MethodInfo,
	&Selectable_get_allSelectables_m1036_MethodInfo,
	&Selectable_get_navigation_m1037_MethodInfo,
	&Selectable_set_navigation_m1038_MethodInfo,
	&Selectable_get_transition_m1039_MethodInfo,
	&Selectable_set_transition_m1040_MethodInfo,
	&Selectable_get_colors_m1041_MethodInfo,
	&Selectable_set_colors_m1042_MethodInfo,
	&Selectable_get_spriteState_m1043_MethodInfo,
	&Selectable_set_spriteState_m1044_MethodInfo,
	&Selectable_get_animationTriggers_m1045_MethodInfo,
	&Selectable_set_animationTriggers_m1046_MethodInfo,
	&Selectable_get_targetGraphic_m1047_MethodInfo,
	&Selectable_set_targetGraphic_m1048_MethodInfo,
	&Selectable_get_interactable_m1049_MethodInfo,
	&Selectable_set_interactable_m1050_MethodInfo,
	&Selectable_get_isPointerInside_m1051_MethodInfo,
	&Selectable_set_isPointerInside_m1052_MethodInfo,
	&Selectable_get_isPointerDown_m1053_MethodInfo,
	&Selectable_set_isPointerDown_m1054_MethodInfo,
	&Selectable_get_hasSelection_m1055_MethodInfo,
	&Selectable_set_hasSelection_m1056_MethodInfo,
	&Selectable_get_image_m1057_MethodInfo,
	&Selectable_set_image_m1058_MethodInfo,
	&Selectable_get_animator_m1059_MethodInfo,
	&Selectable_Awake_m1060_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1061_MethodInfo,
	&Selectable_IsInteractable_m1062_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo,
	&Selectable_OnEnable_m1064_MethodInfo,
	&Selectable_OnSetProperty_m1065_MethodInfo,
	&Selectable_OnDisable_m1066_MethodInfo,
	&Selectable_get_currentSelectionState_m1067_MethodInfo,
	&Selectable_InstantClearState_m1068_MethodInfo,
	&Selectable_DoStateTransition_m1069_MethodInfo,
	&Selectable_FindSelectable_m1070_MethodInfo,
	&Selectable_GetPointOnRectEdge_m1071_MethodInfo,
	&Selectable_Navigate_m1072_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1073_MethodInfo,
	&Selectable_FindSelectableOnRight_m1074_MethodInfo,
	&Selectable_FindSelectableOnUp_m1075_MethodInfo,
	&Selectable_FindSelectableOnDown_m1076_MethodInfo,
	&Selectable_OnMove_m1077_MethodInfo,
	&Selectable_StartColorTween_m1078_MethodInfo,
	&Selectable_DoSpriteSwap_m1079_MethodInfo,
	&Selectable_TriggerAnimation_m1080_MethodInfo,
	&Selectable_IsHighlighted_m1081_MethodInfo,
	&Selectable_IsPressed_m1082_MethodInfo,
	&Selectable_IsPressed_m1083_MethodInfo,
	&Selectable_UpdateSelectionState_m1084_MethodInfo,
	&Selectable_EvaluateAndTransitionToSelectionState_m1085_MethodInfo,
	&Selectable_InternalEvaluateAndTransitionToSelectionState_m1086_MethodInfo,
	&Selectable_OnPointerDown_m1087_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_Select_m1093_MethodInfo,
	NULL
};
extern const MethodInfo Selectable_get_allSelectables_m1036_MethodInfo;
static const PropertyInfo Selectable_t169____allSelectables_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "allSelectables"/* name */
	, &Selectable_get_allSelectables_m1036_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_navigation_m1037_MethodInfo;
extern const MethodInfo Selectable_set_navigation_m1038_MethodInfo;
static const PropertyInfo Selectable_t169____navigation_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "navigation"/* name */
	, &Selectable_get_navigation_m1037_MethodInfo/* get */
	, &Selectable_set_navigation_m1038_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_transition_m1039_MethodInfo;
extern const MethodInfo Selectable_set_transition_m1040_MethodInfo;
static const PropertyInfo Selectable_t169____transition_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "transition"/* name */
	, &Selectable_get_transition_m1039_MethodInfo/* get */
	, &Selectable_set_transition_m1040_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_colors_m1041_MethodInfo;
extern const MethodInfo Selectable_set_colors_m1042_MethodInfo;
static const PropertyInfo Selectable_t169____colors_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "colors"/* name */
	, &Selectable_get_colors_m1041_MethodInfo/* get */
	, &Selectable_set_colors_m1042_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_spriteState_m1043_MethodInfo;
extern const MethodInfo Selectable_set_spriteState_m1044_MethodInfo;
static const PropertyInfo Selectable_t169____spriteState_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "spriteState"/* name */
	, &Selectable_get_spriteState_m1043_MethodInfo/* get */
	, &Selectable_set_spriteState_m1044_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animationTriggers_m1045_MethodInfo;
extern const MethodInfo Selectable_set_animationTriggers_m1046_MethodInfo;
static const PropertyInfo Selectable_t169____animationTriggers_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "animationTriggers"/* name */
	, &Selectable_get_animationTriggers_m1045_MethodInfo/* get */
	, &Selectable_set_animationTriggers_m1046_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_targetGraphic_m1047_MethodInfo;
extern const MethodInfo Selectable_set_targetGraphic_m1048_MethodInfo;
static const PropertyInfo Selectable_t169____targetGraphic_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "targetGraphic"/* name */
	, &Selectable_get_targetGraphic_m1047_MethodInfo/* get */
	, &Selectable_set_targetGraphic_m1048_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_interactable_m1049_MethodInfo;
extern const MethodInfo Selectable_set_interactable_m1050_MethodInfo;
static const PropertyInfo Selectable_t169____interactable_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &Selectable_get_interactable_m1049_MethodInfo/* get */
	, &Selectable_set_interactable_m1050_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerInside_m1051_MethodInfo;
extern const MethodInfo Selectable_set_isPointerInside_m1052_MethodInfo;
static const PropertyInfo Selectable_t169____isPointerInside_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "isPointerInside"/* name */
	, &Selectable_get_isPointerInside_m1051_MethodInfo/* get */
	, &Selectable_set_isPointerInside_m1052_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerDown_m1053_MethodInfo;
extern const MethodInfo Selectable_set_isPointerDown_m1054_MethodInfo;
static const PropertyInfo Selectable_t169____isPointerDown_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "isPointerDown"/* name */
	, &Selectable_get_isPointerDown_m1053_MethodInfo/* get */
	, &Selectable_set_isPointerDown_m1054_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_hasSelection_m1055_MethodInfo;
extern const MethodInfo Selectable_set_hasSelection_m1056_MethodInfo;
static const PropertyInfo Selectable_t169____hasSelection_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "hasSelection"/* name */
	, &Selectable_get_hasSelection_m1055_MethodInfo/* get */
	, &Selectable_set_hasSelection_m1056_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_image_m1057_MethodInfo;
extern const MethodInfo Selectable_set_image_m1058_MethodInfo;
static const PropertyInfo Selectable_t169____image_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "image"/* name */
	, &Selectable_get_image_m1057_MethodInfo/* get */
	, &Selectable_set_image_m1058_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animator_m1059_MethodInfo;
static const PropertyInfo Selectable_t169____animator_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "animator"/* name */
	, &Selectable_get_animator_m1059_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_currentSelectionState_m1067_MethodInfo;
static const PropertyInfo Selectable_t169____currentSelectionState_PropertyInfo = 
{
	&Selectable_t169_il2cpp_TypeInfo/* parent */
	, "currentSelectionState"/* name */
	, &Selectable_get_currentSelectionState_m1067_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Selectable_t169_PropertyInfos[] =
{
	&Selectable_t169____allSelectables_PropertyInfo,
	&Selectable_t169____navigation_PropertyInfo,
	&Selectable_t169____transition_PropertyInfo,
	&Selectable_t169____colors_PropertyInfo,
	&Selectable_t169____spriteState_PropertyInfo,
	&Selectable_t169____animationTriggers_PropertyInfo,
	&Selectable_t169____targetGraphic_PropertyInfo,
	&Selectable_t169____interactable_PropertyInfo,
	&Selectable_t169____isPointerInside_PropertyInfo,
	&Selectable_t169____isPointerDown_PropertyInfo,
	&Selectable_t169____hasSelection_PropertyInfo,
	&Selectable_t169____image_PropertyInfo,
	&Selectable_t169____animator_PropertyInfo,
	&Selectable_t169____currentSelectionState_PropertyInfo,
	NULL
};
static const Il2CppType* Selectable_t169_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t248_0_0_0,
	&SelectionState_t249_0_0_0,
};
extern const MethodInfo Selectable_Awake_m1060_MethodInfo;
extern const MethodInfo Selectable_OnEnable_m1064_MethodInfo;
extern const MethodInfo Selectable_OnDisable_m1066_MethodInfo;
extern const MethodInfo UIBehaviour_IsActive_m370_MethodInfo;
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo;
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1061_MethodInfo;
extern const MethodInfo Selectable_OnPointerEnter_m1089_MethodInfo;
extern const MethodInfo Selectable_OnPointerExit_m1090_MethodInfo;
extern const MethodInfo Selectable_OnPointerDown_m1087_MethodInfo;
extern const MethodInfo Selectable_OnPointerUp_m1088_MethodInfo;
extern const MethodInfo Selectable_OnSelect_m1091_MethodInfo;
extern const MethodInfo Selectable_OnDeselect_m1092_MethodInfo;
extern const MethodInfo Selectable_OnMove_m1077_MethodInfo;
extern const MethodInfo Selectable_IsInteractable_m1062_MethodInfo;
extern const MethodInfo Selectable_InstantClearState_m1068_MethodInfo;
extern const MethodInfo Selectable_DoStateTransition_m1069_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnLeft_m1073_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnRight_m1074_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnUp_m1075_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnDown_m1076_MethodInfo;
extern const MethodInfo Selectable_Select_m1093_MethodInfo;
static const Il2CppMethodReference Selectable_t169_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&Selectable_Awake_m1060_MethodInfo,
	&Selectable_OnEnable_m1064_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&Selectable_OnDisable_m1066_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1061_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnPointerDown_m1087_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_OnMove_m1077_MethodInfo,
	&Selectable_IsInteractable_m1062_MethodInfo,
	&Selectable_InstantClearState_m1068_MethodInfo,
	&Selectable_DoStateTransition_m1069_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1073_MethodInfo,
	&Selectable_FindSelectableOnRight_m1074_MethodInfo,
	&Selectable_FindSelectableOnUp_m1075_MethodInfo,
	&Selectable_FindSelectableOnDown_m1076_MethodInfo,
	&Selectable_OnMove_m1077_MethodInfo,
	&Selectable_OnPointerDown_m1087_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_Select_m1093_MethodInfo,
};
static bool Selectable_t169_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerEnterHandler_t303_0_0_0;
extern const Il2CppType IPointerExitHandler_t304_0_0_0;
extern const Il2CppType IPointerDownHandler_t305_0_0_0;
extern const Il2CppType IPointerUpHandler_t306_0_0_0;
extern const Il2CppType ISelectHandler_t315_0_0_0;
extern const Il2CppType IDeselectHandler_t316_0_0_0;
extern const Il2CppType IMoveHandler_t317_0_0_0;
static const Il2CppType* Selectable_t169_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t407_0_0_0,
	&IPointerEnterHandler_t303_0_0_0,
	&IPointerExitHandler_t304_0_0_0,
	&IPointerDownHandler_t305_0_0_0,
	&IPointerUpHandler_t306_0_0_0,
	&ISelectHandler_t315_0_0_0,
	&IDeselectHandler_t316_0_0_0,
	&IMoveHandler_t317_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t169_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t407_0_0_0, 16},
	{ &IPointerEnterHandler_t303_0_0_0, 16},
	{ &IPointerExitHandler_t304_0_0_0, 17},
	{ &IPointerDownHandler_t305_0_0_0, 18},
	{ &IPointerUpHandler_t306_0_0_0, 19},
	{ &ISelectHandler_t315_0_0_0, 20},
	{ &IDeselectHandler_t316_0_0_0, 21},
	{ &IMoveHandler_t317_0_0_0, 22},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Selectable_t169_1_0_0;
struct Selectable_t169;
const Il2CppTypeDefinitionMetadata Selectable_t169_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t169_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t169_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t169_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, Selectable_t169_VTable/* vtableMethods */
	, Selectable_t169_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 451/* fieldStart */

};
TypeInfo Selectable_t169_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Selectable_t169_MethodInfos/* methods */
	, Selectable_t169_PropertyInfos/* properties */
	, NULL/* events */
	, &Selectable_t169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 225/* custom_attributes_cache */
	, &Selectable_t169_0_0_0/* byval_arg */
	, &Selectable_t169_1_0_0/* this_arg */
	, &Selectable_t169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t169)/* instance_size */
	, sizeof (Selectable_t169)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t169_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t253_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern const Il2CppType Color_t163_1_0_0;
extern const Il2CppType Color_t163_1_0_0;
extern const Il2CppType Color_t163_0_0_0;
static const ParameterInfo SetPropertyUtility_t253_SetPropertyUtility_SetColor_m1094_ParameterInfos[] = 
{
	{"currentValue", 0, 134218254, 0, &Color_t163_1_0_0},
	{"newValue", 1, 134218255, 0, &Color_t163_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_ColorU26_t450_Color_t163 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern const MethodInfo SetPropertyUtility_SetColor_m1094_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&SetPropertyUtility_SetColor_m1094/* method */
	, &SetPropertyUtility_t253_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_ColorU26_t450_Color_t163/* invoker_method */
	, SetPropertyUtility_t253_SetPropertyUtility_SetColor_m1094_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SetPropertyUtility_SetStruct_m2051_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2051_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2051_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2051_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t253_SetPropertyUtility_SetStruct_m2051_ParameterInfos[] = 
{
	{"currentValue", 0, 134218256, 0, &SetPropertyUtility_SetStruct_m2051_gp_0_1_0_0},
	{"newValue", 1, 134218257, 0, &SetPropertyUtility_SetStruct_m2051_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2051_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetStruct_m2051_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t439_0_0_0;
static const Il2CppType* SetPropertyUtility_SetStruct_m2051_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t439_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter SetPropertyUtility_SetStruct_m2051_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetStruct_m2051_Il2CppGenericContainer, SetPropertyUtility_SetStruct_m2051_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 24 };
static const Il2CppGenericParameter* SetPropertyUtility_SetStruct_m2051_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetStruct_m2051_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetStruct_m2051_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2051_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetStruct_m2051_MethodInfo, 1, 1, SetPropertyUtility_SetStruct_m2051_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m2051_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetStruct_m2051_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct(T&,T)
extern const MethodInfo SetPropertyUtility_SetStruct_m2051_MethodInfo = 
{
	"SetStruct"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t253_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t253_SetPropertyUtility_SetStruct_m2051_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, SetPropertyUtility_SetStruct_m2051_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetStruct_m2051_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType SetPropertyUtility_SetClass_m2052_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2052_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2052_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2052_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t253_SetPropertyUtility_SetClass_m2052_ParameterInfos[] = 
{
	{"currentValue", 0, 134218258, 0, &SetPropertyUtility_SetClass_m2052_gp_0_1_0_0},
	{"newValue", 1, 134218259, 0, &SetPropertyUtility_SetClass_m2052_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2052_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetClass_m2052_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter SetPropertyUtility_SetClass_m2052_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetClass_m2052_Il2CppGenericContainer, NULL, "T", 0, 4 };
static const Il2CppGenericParameter* SetPropertyUtility_SetClass_m2052_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetClass_m2052_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetClass_m2052_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2052_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetClass_m2052_MethodInfo, 1, 1, SetPropertyUtility_SetClass_m2052_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m2052_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetClass_m2052_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetClass(T&,T)
extern const MethodInfo SetPropertyUtility_SetClass_m2052_MethodInfo = 
{
	"SetClass"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t253_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t253_SetPropertyUtility_SetClass_m2052_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, SetPropertyUtility_SetClass_m2052_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetClass_m2052_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* SetPropertyUtility_t253_MethodInfos[] =
{
	&SetPropertyUtility_SetColor_m1094_MethodInfo,
	&SetPropertyUtility_SetStruct_m2051_MethodInfo,
	&SetPropertyUtility_SetClass_m2052_MethodInfo,
	NULL
};
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference SetPropertyUtility_t253_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SetPropertyUtility_t253_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SetPropertyUtility_t253_0_0_0;
extern const Il2CppType SetPropertyUtility_t253_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t253;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t253_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t253_VTable/* vtableMethods */
	, SetPropertyUtility_t253_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetPropertyUtility_t253_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SetPropertyUtility_t253_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetPropertyUtility_t253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t253_0_0_0/* byval_arg */
	, &SetPropertyUtility_t253_1_0_0/* this_arg */
	, &SetPropertyUtility_t253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t253)/* instance_size */
	, sizeof (SetPropertyUtility_t253)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t254_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static const MethodInfo* Direction_t254_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Direction_t254_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Direction_t254_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t254_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t254_0_0_0;
extern const Il2CppType Direction_t254_1_0_0;
extern TypeInfo Slider_t257_il2cpp_TypeInfo;
extern const Il2CppType Slider_t257_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t254_DefinitionMetadata = 
{
	&Slider_t257_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t254_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Direction_t254_VTable/* vtableMethods */
	, Direction_t254_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 465/* fieldStart */

};
TypeInfo Direction_t254_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t254_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t254_0_0_0/* byval_arg */
	, &Direction_t254_1_0_0/* this_arg */
	, &Direction_t254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t254)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t254)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t255_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern const MethodInfo SliderEvent__ctor_m1095_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderEvent__ctor_m1095/* method */
	, &SliderEvent_t255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderEvent_t255_MethodInfos[] =
{
	&SliderEvent__ctor_m1095_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2127_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2128_GenericMethod;
static const Il2CppMethodReference SliderEvent_t255_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2127_GenericMethod,
	&UnityEvent_1_GetDelegate_m2128_GenericMethod,
};
static bool SliderEvent_t255_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair SliderEvent_t255_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SliderEvent_t255_0_0_0;
extern const Il2CppType SliderEvent_t255_1_0_0;
extern const Il2CppType UnityEvent_1_t237_0_0_0;
struct SliderEvent_t255;
const Il2CppTypeDefinitionMetadata SliderEvent_t255_DefinitionMetadata = 
{
	&Slider_t257_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t255_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t237_0_0_0/* parent */
	, SliderEvent_t255_VTable/* vtableMethods */
	, SliderEvent_t255_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SliderEvent_t255_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, SliderEvent_t255_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderEvent_t255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t255_0_0_0/* byval_arg */
	, &SliderEvent_t255_1_0_0/* this_arg */
	, &SliderEvent_t255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t255)/* instance_size */
	, sizeof (SliderEvent_t255)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t256_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t256_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t256_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Axis_t256_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t256_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t256_0_0_0;
extern const Il2CppType Axis_t256_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t256_DefinitionMetadata = 
{
	&Slider_t257_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t256_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Axis_t256_VTable/* vtableMethods */
	, Axis_t256_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 470/* fieldStart */

};
TypeInfo Axis_t256_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t256_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t256_0_0_0/* byval_arg */
	, &Axis_t256_1_0_0/* this_arg */
	, &Axis_t256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t256)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t256)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::.ctor()
extern const MethodInfo Slider__ctor_m1096_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Slider__ctor_m1096/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_fillRect()
extern const MethodInfo Slider_get_fillRect_m1097_MethodInfo = 
{
	"get_fillRect"/* name */
	, (methodPointerType)&Slider_get_fillRect_m1097/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_fillRect_m1098_ParameterInfos[] = 
{
	{"value", 0, 134218260, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_fillRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_fillRect_m1098_MethodInfo = 
{
	"set_fillRect"/* name */
	, (methodPointerType)&Slider_set_fillRect_m1098/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_set_fillRect_m1098_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_handleRect()
extern const MethodInfo Slider_get_handleRect_m1099_MethodInfo = 
{
	"get_handleRect"/* name */
	, (methodPointerType)&Slider_get_handleRect_m1099/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_handleRect_m1100_ParameterInfos[] = 
{
	{"value", 0, 134218261, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_handleRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_handleRect_m1100_MethodInfo = 
{
	"set_handleRect"/* name */
	, (methodPointerType)&Slider_set_handleRect_m1100/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_set_handleRect_m1100_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t254 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::get_direction()
extern const MethodInfo Slider_get_direction_m1101_MethodInfo = 
{
	"get_direction"/* name */
	, (methodPointerType)&Slider_get_direction_m1101/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t254_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t254/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t254_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_direction_m1102_ParameterInfos[] = 
{
	{"value", 0, 134218262, 0, &Direction_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_direction(UnityEngine.UI.Slider/Direction)
extern const MethodInfo Slider_set_direction_m1102_MethodInfo = 
{
	"set_direction"/* name */
	, (methodPointerType)&Slider_set_direction_m1102/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Slider_t257_Slider_set_direction_m1102_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_minValue()
extern const MethodInfo Slider_get_minValue_m1103_MethodInfo = 
{
	"get_minValue"/* name */
	, (methodPointerType)&Slider_get_minValue_m1103/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_minValue_m1104_ParameterInfos[] = 
{
	{"value", 0, 134218263, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
extern const MethodInfo Slider_set_minValue_m1104_MethodInfo = 
{
	"set_minValue"/* name */
	, (methodPointerType)&Slider_set_minValue_m1104/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Slider_t257_Slider_set_minValue_m1104_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_maxValue()
extern const MethodInfo Slider_get_maxValue_m1105_MethodInfo = 
{
	"get_maxValue"/* name */
	, (methodPointerType)&Slider_get_maxValue_m1105/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_maxValue_m1106_ParameterInfos[] = 
{
	{"value", 0, 134218264, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern const MethodInfo Slider_set_maxValue_m1106_MethodInfo = 
{
	"set_maxValue"/* name */
	, (methodPointerType)&Slider_set_maxValue_m1106/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Slider_t257_Slider_set_maxValue_m1106_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_wholeNumbers()
extern const MethodInfo Slider_get_wholeNumbers_m1107_MethodInfo = 
{
	"get_wholeNumbers"/* name */
	, (methodPointerType)&Slider_get_wholeNumbers_m1107/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_wholeNumbers_m1108_ParameterInfos[] = 
{
	{"value", 0, 134218265, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_wholeNumbers(System.Boolean)
extern const MethodInfo Slider_set_wholeNumbers_m1108_MethodInfo = 
{
	"set_wholeNumbers"/* name */
	, (methodPointerType)&Slider_set_wholeNumbers_m1108/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Slider_t257_Slider_set_wholeNumbers_m1108_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_value()
extern const MethodInfo Slider_get_value_m1109_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Slider_get_value_m1109/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_value_m1110_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_value(System.Single)
extern const MethodInfo Slider_set_value_m1110_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Slider_set_value_m1110/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Slider_t257_Slider_set_value_m1110_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
extern const MethodInfo Slider_get_normalizedValue_m1111_MethodInfo = 
{
	"get_normalizedValue"/* name */
	, (methodPointerType)&Slider_get_normalizedValue_m1111/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_normalizedValue_m1112_ParameterInfos[] = 
{
	{"value", 0, 134218267, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
extern const MethodInfo Slider_set_normalizedValue_m1112_MethodInfo = 
{
	"set_normalizedValue"/* name */
	, (methodPointerType)&Slider_set_normalizedValue_m1112/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Slider_t257_Slider_set_normalizedValue_m1112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
extern const MethodInfo Slider_get_onValueChanged_m1113_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&Slider_get_onValueChanged_m1113/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &SliderEvent_t255_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SliderEvent_t255_0_0_0;
static const ParameterInfo Slider_t257_Slider_set_onValueChanged_m1114_ParameterInfos[] = 
{
	{"value", 0, 134218268, 0, &SliderEvent_t255_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_onValueChanged(UnityEngine.UI.Slider/SliderEvent)
extern const MethodInfo Slider_set_onValueChanged_m1114_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&Slider_set_onValueChanged_m1114/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_set_onValueChanged_m1114_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_stepSize()
extern const MethodInfo Slider_get_stepSize_m1115_MethodInfo = 
{
	"get_stepSize"/* name */
	, (methodPointerType)&Slider_get_stepSize_m1115/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t170_0_0_0;
static const ParameterInfo Slider_t257_Slider_Rebuild_m1116_ParameterInfos[] = 
{
	{"executing", 0, 134218269, 0, &CanvasUpdate_t170_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Slider_Rebuild_m1116_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Slider_Rebuild_m1116/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Slider_t257_Slider_Rebuild_m1116_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnEnable()
extern const MethodInfo Slider_OnEnable_m1117_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Slider_OnEnable_m1117/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDisable()
extern const MethodInfo Slider_OnDisable_m1118_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Slider_OnDisable_m1118/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateCachedReferences()
extern const MethodInfo Slider_UpdateCachedReferences_m1119_MethodInfo = 
{
	"UpdateCachedReferences"/* name */
	, (methodPointerType)&Slider_UpdateCachedReferences_m1119/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Slider_t257_Slider_Set_m1120_ParameterInfos[] = 
{
	{"input", 0, 134218270, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single)
extern const MethodInfo Slider_Set_m1120_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1120/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Slider_t257_Slider_Set_m1120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Slider_t257_Slider_Set_m1121_ParameterInfos[] = 
{
	{"input", 0, 134218271, 0, &Single_t85_0_0_0},
	{"sendCallback", 1, 134218272, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single,System.Boolean)
extern const MethodInfo Slider_Set_m1121_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1121/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85_SByte_t73/* invoker_method */
	, Slider_t257_Slider_Set_m1121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnRectTransformDimensionsChange()
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1122_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&Slider_OnRectTransformDimensionsChange_m1122/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t256 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Axis UnityEngine.UI.Slider::get_axis()
extern const MethodInfo Slider_get_axis_m1123_MethodInfo = 
{
	"get_axis"/* name */
	, (methodPointerType)&Slider_get_axis_m1123/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t256_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t256/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_reverseValue()
extern const MethodInfo Slider_get_reverseValue_m1124_MethodInfo = 
{
	"get_reverseValue"/* name */
	, (methodPointerType)&Slider_get_reverseValue_m1124/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateVisuals()
extern const MethodInfo Slider_UpdateVisuals_m1125_MethodInfo = 
{
	"UpdateVisuals"/* name */
	, (methodPointerType)&Slider_UpdateVisuals_m1125/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
extern const Il2CppType Camera_t156_0_0_0;
extern const Il2CppType Camera_t156_0_0_0;
static const ParameterInfo Slider_t257_Slider_UpdateDrag_m1126_ParameterInfos[] = 
{
	{"eventData", 0, 134218273, 0, &PointerEventData_t143_0_0_0},
	{"cam", 1, 134218274, 0, &Camera_t156_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern const MethodInfo Slider_UpdateDrag_m1126_MethodInfo = 
{
	"UpdateDrag"/* name */
	, (methodPointerType)&Slider_UpdateDrag_m1126/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Slider_t257_Slider_UpdateDrag_m1126_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Slider_t257_Slider_MayDrag_m1127_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_MayDrag_m1127_MethodInfo = 
{
	"MayDrag"/* name */
	, (methodPointerType)&Slider_MayDrag_m1127/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Slider_t257_Slider_MayDrag_m1127_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Slider_t257_Slider_OnPointerDown_m1128_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnPointerDown_m1128_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Slider_OnPointerDown_m1128/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_OnPointerDown_m1128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Slider_t257_Slider_OnDrag_m1129_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnDrag_m1129_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Slider_OnDrag_m1129/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_OnDrag_m1129_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t139_0_0_0;
static const ParameterInfo Slider_t257_Slider_OnMove_m1130_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, 0, &AxisEventData_t139_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Slider_OnMove_m1130_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Slider_OnMove_m1130/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_OnMove_m1130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnLeft()
extern const MethodInfo Slider_FindSelectableOnLeft_m1131_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Slider_FindSelectableOnLeft_m1131/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnRight()
extern const MethodInfo Slider_FindSelectableOnRight_m1132_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Slider_FindSelectableOnRight_m1132/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnUp()
extern const MethodInfo Slider_FindSelectableOnUp_m1133_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Slider_FindSelectableOnUp_m1133/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnDown()
extern const MethodInfo Slider_FindSelectableOnDown_m1134_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Slider_FindSelectableOnDown_m1134/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t169_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Slider_t257_Slider_OnInitializePotentialDrag_m1135_ParameterInfos[] = 
{
	{"eventData", 0, 134218279, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnInitializePotentialDrag_m1135_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&Slider_OnInitializePotentialDrag_m1135/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Slider_t257_Slider_OnInitializePotentialDrag_m1135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t254_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Slider_t257_Slider_SetDirection_m1136_ParameterInfos[] = 
{
	{"direction", 0, 134218280, 0, &Direction_t254_0_0_0},
	{"includeRectLayouts", 1, 134218281, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::SetDirection(UnityEngine.UI.Slider/Direction,System.Boolean)
extern const MethodInfo Slider_SetDirection_m1136_MethodInfo = 
{
	"SetDirection"/* name */
	, (methodPointerType)&Slider_SetDirection_m1136/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, Slider_t257_Slider_SetDirection_m1136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138/* method */
	, &Slider_t257_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Slider_t257_MethodInfos[] =
{
	&Slider__ctor_m1096_MethodInfo,
	&Slider_get_fillRect_m1097_MethodInfo,
	&Slider_set_fillRect_m1098_MethodInfo,
	&Slider_get_handleRect_m1099_MethodInfo,
	&Slider_set_handleRect_m1100_MethodInfo,
	&Slider_get_direction_m1101_MethodInfo,
	&Slider_set_direction_m1102_MethodInfo,
	&Slider_get_minValue_m1103_MethodInfo,
	&Slider_set_minValue_m1104_MethodInfo,
	&Slider_get_maxValue_m1105_MethodInfo,
	&Slider_set_maxValue_m1106_MethodInfo,
	&Slider_get_wholeNumbers_m1107_MethodInfo,
	&Slider_set_wholeNumbers_m1108_MethodInfo,
	&Slider_get_value_m1109_MethodInfo,
	&Slider_set_value_m1110_MethodInfo,
	&Slider_get_normalizedValue_m1111_MethodInfo,
	&Slider_set_normalizedValue_m1112_MethodInfo,
	&Slider_get_onValueChanged_m1113_MethodInfo,
	&Slider_set_onValueChanged_m1114_MethodInfo,
	&Slider_get_stepSize_m1115_MethodInfo,
	&Slider_Rebuild_m1116_MethodInfo,
	&Slider_OnEnable_m1117_MethodInfo,
	&Slider_OnDisable_m1118_MethodInfo,
	&Slider_UpdateCachedReferences_m1119_MethodInfo,
	&Slider_Set_m1120_MethodInfo,
	&Slider_Set_m1121_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1122_MethodInfo,
	&Slider_get_axis_m1123_MethodInfo,
	&Slider_get_reverseValue_m1124_MethodInfo,
	&Slider_UpdateVisuals_m1125_MethodInfo,
	&Slider_UpdateDrag_m1126_MethodInfo,
	&Slider_MayDrag_m1127_MethodInfo,
	&Slider_OnPointerDown_m1128_MethodInfo,
	&Slider_OnDrag_m1129_MethodInfo,
	&Slider_OnMove_m1130_MethodInfo,
	&Slider_FindSelectableOnLeft_m1131_MethodInfo,
	&Slider_FindSelectableOnRight_m1132_MethodInfo,
	&Slider_FindSelectableOnUp_m1133_MethodInfo,
	&Slider_FindSelectableOnDown_m1134_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1135_MethodInfo,
	&Slider_SetDirection_m1136_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138_MethodInfo,
	NULL
};
extern const MethodInfo Slider_get_fillRect_m1097_MethodInfo;
extern const MethodInfo Slider_set_fillRect_m1098_MethodInfo;
static const PropertyInfo Slider_t257____fillRect_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "fillRect"/* name */
	, &Slider_get_fillRect_m1097_MethodInfo/* get */
	, &Slider_set_fillRect_m1098_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_handleRect_m1099_MethodInfo;
extern const MethodInfo Slider_set_handleRect_m1100_MethodInfo;
static const PropertyInfo Slider_t257____handleRect_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "handleRect"/* name */
	, &Slider_get_handleRect_m1099_MethodInfo/* get */
	, &Slider_set_handleRect_m1100_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_direction_m1101_MethodInfo;
extern const MethodInfo Slider_set_direction_m1102_MethodInfo;
static const PropertyInfo Slider_t257____direction_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "direction"/* name */
	, &Slider_get_direction_m1101_MethodInfo/* get */
	, &Slider_set_direction_m1102_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_minValue_m1103_MethodInfo;
extern const MethodInfo Slider_set_minValue_m1104_MethodInfo;
static const PropertyInfo Slider_t257____minValue_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "minValue"/* name */
	, &Slider_get_minValue_m1103_MethodInfo/* get */
	, &Slider_set_minValue_m1104_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_maxValue_m1105_MethodInfo;
extern const MethodInfo Slider_set_maxValue_m1106_MethodInfo;
static const PropertyInfo Slider_t257____maxValue_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "maxValue"/* name */
	, &Slider_get_maxValue_m1105_MethodInfo/* get */
	, &Slider_set_maxValue_m1106_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_wholeNumbers_m1107_MethodInfo;
extern const MethodInfo Slider_set_wholeNumbers_m1108_MethodInfo;
static const PropertyInfo Slider_t257____wholeNumbers_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "wholeNumbers"/* name */
	, &Slider_get_wholeNumbers_m1107_MethodInfo/* get */
	, &Slider_set_wholeNumbers_m1108_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_value_m1109_MethodInfo;
extern const MethodInfo Slider_set_value_m1110_MethodInfo;
static const PropertyInfo Slider_t257____value_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Slider_get_value_m1109_MethodInfo/* get */
	, &Slider_set_value_m1110_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_normalizedValue_m1111_MethodInfo;
extern const MethodInfo Slider_set_normalizedValue_m1112_MethodInfo;
static const PropertyInfo Slider_t257____normalizedValue_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "normalizedValue"/* name */
	, &Slider_get_normalizedValue_m1111_MethodInfo/* get */
	, &Slider_set_normalizedValue_m1112_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_onValueChanged_m1113_MethodInfo;
extern const MethodInfo Slider_set_onValueChanged_m1114_MethodInfo;
static const PropertyInfo Slider_t257____onValueChanged_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &Slider_get_onValueChanged_m1113_MethodInfo/* get */
	, &Slider_set_onValueChanged_m1114_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_stepSize_m1115_MethodInfo;
static const PropertyInfo Slider_t257____stepSize_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "stepSize"/* name */
	, &Slider_get_stepSize_m1115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_axis_m1123_MethodInfo;
static const PropertyInfo Slider_t257____axis_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "axis"/* name */
	, &Slider_get_axis_m1123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_reverseValue_m1124_MethodInfo;
static const PropertyInfo Slider_t257____reverseValue_PropertyInfo = 
{
	&Slider_t257_il2cpp_TypeInfo/* parent */
	, "reverseValue"/* name */
	, &Slider_get_reverseValue_m1124_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Slider_t257_PropertyInfos[] =
{
	&Slider_t257____fillRect_PropertyInfo,
	&Slider_t257____handleRect_PropertyInfo,
	&Slider_t257____direction_PropertyInfo,
	&Slider_t257____minValue_PropertyInfo,
	&Slider_t257____maxValue_PropertyInfo,
	&Slider_t257____wholeNumbers_PropertyInfo,
	&Slider_t257____value_PropertyInfo,
	&Slider_t257____normalizedValue_PropertyInfo,
	&Slider_t257____onValueChanged_PropertyInfo,
	&Slider_t257____stepSize_PropertyInfo,
	&Slider_t257____axis_PropertyInfo,
	&Slider_t257____reverseValue_PropertyInfo,
	NULL
};
static const Il2CppType* Slider_t257_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t254_0_0_0,
	&SliderEvent_t255_0_0_0,
	&Axis_t256_0_0_0,
};
extern const MethodInfo Slider_OnEnable_m1117_MethodInfo;
extern const MethodInfo Slider_OnDisable_m1118_MethodInfo;
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1122_MethodInfo;
extern const MethodInfo Slider_OnPointerDown_m1128_MethodInfo;
extern const MethodInfo Slider_OnMove_m1130_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnLeft_m1131_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnRight_m1132_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnUp_m1133_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnDown_m1134_MethodInfo;
extern const MethodInfo Slider_OnInitializePotentialDrag_m1135_MethodInfo;
extern const MethodInfo Slider_OnDrag_m1129_MethodInfo;
extern const MethodInfo Slider_Rebuild_m1116_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137_MethodInfo;
static const Il2CppMethodReference Slider_t257_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&Selectable_Awake_m1060_MethodInfo,
	&Slider_OnEnable_m1117_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&Slider_OnDisable_m1118_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1122_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1061_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Slider_OnPointerDown_m1128_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Slider_OnMove_m1130_MethodInfo,
	&Selectable_IsInteractable_m1062_MethodInfo,
	&Selectable_InstantClearState_m1068_MethodInfo,
	&Selectable_DoStateTransition_m1069_MethodInfo,
	&Slider_FindSelectableOnLeft_m1131_MethodInfo,
	&Slider_FindSelectableOnRight_m1132_MethodInfo,
	&Slider_FindSelectableOnUp_m1133_MethodInfo,
	&Slider_FindSelectableOnDown_m1134_MethodInfo,
	&Slider_OnMove_m1130_MethodInfo,
	&Slider_OnPointerDown_m1128_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_Select_m1093_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1135_MethodInfo,
	&Slider_OnDrag_m1129_MethodInfo,
	&Slider_Rebuild_m1116_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137_MethodInfo,
	&Slider_Rebuild_m1116_MethodInfo,
	&Slider_OnDrag_m1129_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1135_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1137_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1138_MethodInfo,
};
static bool Slider_t257_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Slider_t257_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t407_0_0_0,
	&IInitializePotentialDragHandler_t308_0_0_0,
	&IDragHandler_t310_0_0_0,
	&ICanvasElement_t325_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t257_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t407_0_0_0, 16},
	{ &IPointerEnterHandler_t303_0_0_0, 16},
	{ &IPointerExitHandler_t304_0_0_0, 17},
	{ &IPointerDownHandler_t305_0_0_0, 18},
	{ &IPointerUpHandler_t306_0_0_0, 19},
	{ &ISelectHandler_t315_0_0_0, 20},
	{ &IDeselectHandler_t316_0_0_0, 21},
	{ &IMoveHandler_t317_0_0_0, 22},
	{ &IInitializePotentialDragHandler_t308_0_0_0, 38},
	{ &IDragHandler_t310_0_0_0, 39},
	{ &ICanvasElement_t325_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Slider_t257_1_0_0;
struct Slider_t257;
const Il2CppTypeDefinitionMetadata Slider_t257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t257_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t257_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t257_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t169_0_0_0/* parent */
	, Slider_t257_VTable/* vtableMethods */
	, Slider_t257_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 473/* fieldStart */

};
TypeInfo Slider_t257_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Slider_t257_MethodInfos/* methods */
	, Slider_t257_PropertyInfos/* properties */
	, NULL/* events */
	, &Slider_t257_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 243/* custom_attributes_cache */
	, &Slider_t257_0_0_0/* byval_arg */
	, &Slider_t257_1_0_0/* this_arg */
	, &Slider_t257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t257)/* instance_size */
	, sizeof (Slider_t257)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t252_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern const MethodInfo SpriteState_get_highlightedSprite_m1139_MethodInfo = 
{
	"get_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_get_highlightedSprite_m1139/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t201_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t201_0_0_0;
static const ParameterInfo SpriteState_t252_SpriteState_set_highlightedSprite_m1140_ParameterInfos[] = 
{
	{"value", 0, 134218282, 0, &Sprite_t201_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_highlightedSprite_m1140_MethodInfo = 
{
	"set_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_set_highlightedSprite_m1140/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SpriteState_t252_SpriteState_set_highlightedSprite_m1140_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern const MethodInfo SpriteState_get_pressedSprite_m1141_MethodInfo = 
{
	"get_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_get_pressedSprite_m1141/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t201_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t201_0_0_0;
static const ParameterInfo SpriteState_t252_SpriteState_set_pressedSprite_m1142_ParameterInfos[] = 
{
	{"value", 0, 134218283, 0, &Sprite_t201_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_pressedSprite_m1142_MethodInfo = 
{
	"set_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_set_pressedSprite_m1142/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SpriteState_t252_SpriteState_set_pressedSprite_m1142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern const MethodInfo SpriteState_get_disabledSprite_m1143_MethodInfo = 
{
	"get_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_get_disabledSprite_m1143/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t201_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t201_0_0_0;
static const ParameterInfo SpriteState_t252_SpriteState_set_disabledSprite_m1144_ParameterInfos[] = 
{
	{"value", 0, 134218284, 0, &Sprite_t201_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_disabledSprite_m1144_MethodInfo = 
{
	"set_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_set_disabledSprite_m1144/* method */
	, &SpriteState_t252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SpriteState_t252_SpriteState_set_disabledSprite_m1144_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpriteState_t252_MethodInfos[] =
{
	&SpriteState_get_highlightedSprite_m1139_MethodInfo,
	&SpriteState_set_highlightedSprite_m1140_MethodInfo,
	&SpriteState_get_pressedSprite_m1141_MethodInfo,
	&SpriteState_set_pressedSprite_m1142_MethodInfo,
	&SpriteState_get_disabledSprite_m1143_MethodInfo,
	&SpriteState_set_disabledSprite_m1144_MethodInfo,
	NULL
};
extern const MethodInfo SpriteState_get_highlightedSprite_m1139_MethodInfo;
extern const MethodInfo SpriteState_set_highlightedSprite_m1140_MethodInfo;
static const PropertyInfo SpriteState_t252____highlightedSprite_PropertyInfo = 
{
	&SpriteState_t252_il2cpp_TypeInfo/* parent */
	, "highlightedSprite"/* name */
	, &SpriteState_get_highlightedSprite_m1139_MethodInfo/* get */
	, &SpriteState_set_highlightedSprite_m1140_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_pressedSprite_m1141_MethodInfo;
extern const MethodInfo SpriteState_set_pressedSprite_m1142_MethodInfo;
static const PropertyInfo SpriteState_t252____pressedSprite_PropertyInfo = 
{
	&SpriteState_t252_il2cpp_TypeInfo/* parent */
	, "pressedSprite"/* name */
	, &SpriteState_get_pressedSprite_m1141_MethodInfo/* get */
	, &SpriteState_set_pressedSprite_m1142_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_disabledSprite_m1143_MethodInfo;
extern const MethodInfo SpriteState_set_disabledSprite_m1144_MethodInfo;
static const PropertyInfo SpriteState_t252____disabledSprite_PropertyInfo = 
{
	&SpriteState_t252_il2cpp_TypeInfo/* parent */
	, "disabledSprite"/* name */
	, &SpriteState_get_disabledSprite_m1143_MethodInfo/* get */
	, &SpriteState_set_disabledSprite_m1144_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SpriteState_t252_PropertyInfos[] =
{
	&SpriteState_t252____highlightedSprite_PropertyInfo,
	&SpriteState_t252____pressedSprite_PropertyInfo,
	&SpriteState_t252____disabledSprite_PropertyInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
static const Il2CppMethodReference SpriteState_t252_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool SpriteState_t252_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SpriteState_t252_1_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t252_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, SpriteState_t252_VTable/* vtableMethods */
	, SpriteState_t252_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 488/* fieldStart */

};
TypeInfo SpriteState_t252_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SpriteState_t252_MethodInfos/* methods */
	, SpriteState_t252_PropertyInfos/* properties */
	, NULL/* events */
	, &SpriteState_t252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t252_0_0_0/* byval_arg */
	, &SpriteState_t252_1_0_0/* this_arg */
	, &SpriteState_t252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t252)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t252)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t258_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern const MethodInfo MatEntry__ctor_m1145_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatEntry__ctor_m1145/* method */
	, &MatEntry_t258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatEntry_t258_MethodInfos[] =
{
	&MatEntry__ctor_m1145_MethodInfo,
	NULL
};
static const Il2CppMethodReference MatEntry_t258_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MatEntry_t258_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MatEntry_t258_0_0_0;
extern const Il2CppType MatEntry_t258_1_0_0;
extern TypeInfo StencilMaterial_t260_il2cpp_TypeInfo;
extern const Il2CppType StencilMaterial_t260_0_0_0;
struct MatEntry_t258;
const Il2CppTypeDefinitionMetadata MatEntry_t258_DefinitionMetadata = 
{
	&StencilMaterial_t260_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t258_VTable/* vtableMethods */
	, MatEntry_t258_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 491/* fieldStart */

};
TypeInfo MatEntry_t258_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, MatEntry_t258_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatEntry_t258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t258_0_0_0/* byval_arg */
	, &MatEntry_t258_1_0_0/* this_arg */
	, &MatEntry_t258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t258)/* instance_size */
	, sizeof (MatEntry_t258)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
extern const MethodInfo StencilMaterial__cctor_m1146_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StencilMaterial__cctor_m1146/* method */
	, &StencilMaterial_t260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t180_0_0_0;
extern const Il2CppType Material_t180_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StencilMaterial_t260_StencilMaterial_Add_m1147_ParameterInfos[] = 
{
	{"baseMat", 0, 134218285, 0, &Material_t180_0_0_0},
	{"stencilID", 1, 134218286, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
extern const MethodInfo StencilMaterial_Add_m1147_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&StencilMaterial_Add_m1147/* method */
	, &StencilMaterial_t260_il2cpp_TypeInfo/* declaring_type */
	, &Material_t180_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, StencilMaterial_t260_StencilMaterial_Add_m1147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t180_0_0_0;
static const ParameterInfo StencilMaterial_t260_StencilMaterial_Remove_m1148_ParameterInfos[] = 
{
	{"customMat", 0, 134218287, 0, &Material_t180_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern const MethodInfo StencilMaterial_Remove_m1148_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&StencilMaterial_Remove_m1148/* method */
	, &StencilMaterial_t260_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, StencilMaterial_t260_StencilMaterial_Remove_m1148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StencilMaterial_t260_MethodInfos[] =
{
	&StencilMaterial__cctor_m1146_MethodInfo,
	&StencilMaterial_Add_m1147_MethodInfo,
	&StencilMaterial_Remove_m1148_MethodInfo,
	NULL
};
static const Il2CppType* StencilMaterial_t260_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t258_0_0_0,
};
static const Il2CppMethodReference StencilMaterial_t260_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool StencilMaterial_t260_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StencilMaterial_t260_1_0_0;
struct StencilMaterial_t260;
const Il2CppTypeDefinitionMetadata StencilMaterial_t260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t260_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t260_VTable/* vtableMethods */
	, StencilMaterial_t260_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 495/* fieldStart */

};
TypeInfo StencilMaterial_t260_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, StencilMaterial_t260_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StencilMaterial_t260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t260_0_0_0/* byval_arg */
	, &StencilMaterial_t260_1_0_0/* this_arg */
	, &StencilMaterial_t260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t260)/* instance_size */
	, sizeof (StencilMaterial_t260)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t260_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t223_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.ctor()
extern const MethodInfo Text__ctor_m1149_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Text__ctor_m1149/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.cctor()
extern const MethodInfo Text__cctor_m1150_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Text__cctor_m1150/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerator_t226_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
extern const MethodInfo Text_get_cachedTextGenerator_m1151_MethodInfo = 
{
	"get_cachedTextGenerator"/* name */
	, (methodPointerType)&Text_get_cachedTextGenerator_m1151/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1152_MethodInfo = 
{
	"get_cachedTextGeneratorForLayout"/* name */
	, (methodPointerType)&Text_get_cachedTextGeneratorForLayout_m1152/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Text::get_defaultMaterial()
extern const MethodInfo Text_get_defaultMaterial_m1153_MethodInfo = 
{
	"get_defaultMaterial"/* name */
	, (methodPointerType)&Text_get_defaultMaterial_m1153/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Material_t180_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture_t233_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
extern const MethodInfo Text_get_mainTexture_m1154_MethodInfo = 
{
	"get_mainTexture"/* name */
	, (methodPointerType)&Text_get_mainTexture_m1154/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Texture_t233_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::FontTextureChanged()
extern const MethodInfo Text_FontTextureChanged_m1155_MethodInfo = 
{
	"FontTextureChanged"/* name */
	, (methodPointerType)&Text_FontTextureChanged_m1155/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t176_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern const MethodInfo Text_get_font_m1156_MethodInfo = 
{
	"get_font"/* name */
	, (methodPointerType)&Text_get_font_m1156/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Font_t176_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t176_0_0_0;
static const ParameterInfo Text_t223_Text_set_font_m1157_ParameterInfos[] = 
{
	{"value", 0, 134218288, 0, &Font_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern const MethodInfo Text_set_font_m1157_MethodInfo = 
{
	"set_font"/* name */
	, (methodPointerType)&Text_set_font_m1157/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Text_t223_Text_set_font_m1157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.Text::get_text()
extern const MethodInfo Text_get_text_m1158_MethodInfo = 
{
	"get_text"/* name */
	, (methodPointerType)&Text_get_text_m1158/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Text_t223_Text_set_text_m1159_ParameterInfos[] = 
{
	{"value", 0, 134218289, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_text(System.String)
extern const MethodInfo Text_set_text_m1159_MethodInfo = 
{
	"set_text"/* name */
	, (methodPointerType)&Text_set_text_m1159/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Text_t223_Text_set_text_m1159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 48/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern const MethodInfo Text_get_supportRichText_m1160_MethodInfo = 
{
	"get_supportRichText"/* name */
	, (methodPointerType)&Text_get_supportRichText_m1160/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Text_t223_Text_set_supportRichText_m1161_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
extern const MethodInfo Text_set_supportRichText_m1161_MethodInfo = 
{
	"set_supportRichText"/* name */
	, (methodPointerType)&Text_set_supportRichText_m1161/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Text_t223_Text_set_supportRichText_m1161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern const MethodInfo Text_get_resizeTextForBestFit_m1162_MethodInfo = 
{
	"get_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_get_resizeTextForBestFit_m1162/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Text_t223_Text_set_resizeTextForBestFit_m1163_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern const MethodInfo Text_set_resizeTextForBestFit_m1163_MethodInfo = 
{
	"set_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_set_resizeTextForBestFit_m1163/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Text_t223_Text_set_resizeTextForBestFit_m1163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern const MethodInfo Text_get_resizeTextMinSize_m1164_MethodInfo = 
{
	"get_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMinSize_m1164/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Text_t223_Text_set_resizeTextMinSize_m1165_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMinSize_m1165_MethodInfo = 
{
	"set_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMinSize_m1165/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_resizeTextMinSize_m1165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern const MethodInfo Text_get_resizeTextMaxSize_m1166_MethodInfo = 
{
	"get_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMaxSize_m1166/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Text_t223_Text_set_resizeTextMaxSize_m1167_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMaxSize_m1167_MethodInfo = 
{
	"set_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMaxSize_m1167/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_resizeTextMaxSize_m1167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t388_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t388 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern const MethodInfo Text_get_alignment_m1168_MethodInfo = 
{
	"get_alignment"/* name */
	, (methodPointerType)&Text_get_alignment_m1168/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t388_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t388/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t388_0_0_0;
static const ParameterInfo Text_t223_Text_set_alignment_m1169_ParameterInfos[] = 
{
	{"value", 0, 134218294, 0, &TextAnchor_t388_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern const MethodInfo Text_set_alignment_m1169_MethodInfo = 
{
	"set_alignment"/* name */
	, (methodPointerType)&Text_set_alignment_m1169/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_alignment_m1169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern const MethodInfo Text_get_fontSize_m1170_MethodInfo = 
{
	"get_fontSize"/* name */
	, (methodPointerType)&Text_get_fontSize_m1170/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Text_t223_Text_set_fontSize_m1171_ParameterInfos[] = 
{
	{"value", 0, 134218295, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern const MethodInfo Text_set_fontSize_m1171_MethodInfo = 
{
	"set_fontSize"/* name */
	, (methodPointerType)&Text_set_fontSize_m1171/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_fontSize_m1171_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t446_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t446 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern const MethodInfo Text_get_horizontalOverflow_m1172_MethodInfo = 
{
	"get_horizontalOverflow"/* name */
	, (methodPointerType)&Text_get_horizontalOverflow_m1172/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t446_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t446/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t446_0_0_0;
static const ParameterInfo Text_t223_Text_set_horizontalOverflow_m1173_ParameterInfos[] = 
{
	{"value", 0, 134218296, 0, &HorizontalWrapMode_t446_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
extern const MethodInfo Text_set_horizontalOverflow_m1173_MethodInfo = 
{
	"set_horizontalOverflow"/* name */
	, (methodPointerType)&Text_set_horizontalOverflow_m1173/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_horizontalOverflow_m1173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t447_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t447 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern const MethodInfo Text_get_verticalOverflow_m1174_MethodInfo = 
{
	"get_verticalOverflow"/* name */
	, (methodPointerType)&Text_get_verticalOverflow_m1174/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t447_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t447/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t447_0_0_0;
static const ParameterInfo Text_t223_Text_set_verticalOverflow_m1175_ParameterInfos[] = 
{
	{"value", 0, 134218297, 0, &VerticalWrapMode_t447_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
extern const MethodInfo Text_set_verticalOverflow_m1175_MethodInfo = 
{
	"set_verticalOverflow"/* name */
	, (methodPointerType)&Text_set_verticalOverflow_m1175/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_verticalOverflow_m1175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern const MethodInfo Text_get_lineSpacing_m1176_MethodInfo = 
{
	"get_lineSpacing"/* name */
	, (methodPointerType)&Text_get_lineSpacing_m1176/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Text_t223_Text_set_lineSpacing_m1177_ParameterInfos[] = 
{
	{"value", 0, 134218298, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
extern const MethodInfo Text_set_lineSpacing_m1177_MethodInfo = 
{
	"set_lineSpacing"/* name */
	, (methodPointerType)&Text_set_lineSpacing_m1177/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, Text_t223_Text_set_lineSpacing_m1177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t445_0_0_0;
extern void* RuntimeInvoker_FontStyle_t445 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern const MethodInfo Text_get_fontStyle_m1178_MethodInfo = 
{
	"get_fontStyle"/* name */
	, (methodPointerType)&Text_get_fontStyle_m1178/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &FontStyle_t445_0_0_0/* return_type */
	, RuntimeInvoker_FontStyle_t445/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t445_0_0_0;
static const ParameterInfo Text_t223_Text_set_fontStyle_m1179_ParameterInfos[] = 
{
	{"value", 0, 134218299, 0, &FontStyle_t445_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
extern const MethodInfo Text_set_fontStyle_m1179_MethodInfo = 
{
	"set_fontStyle"/* name */
	, (methodPointerType)&Text_set_fontStyle_m1179/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Text_t223_Text_set_fontStyle_m1179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
extern const MethodInfo Text_get_pixelsPerUnit_m1180_MethodInfo = 
{
	"get_pixelsPerUnit"/* name */
	, (methodPointerType)&Text_get_pixelsPerUnit_m1180/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnEnable()
extern const MethodInfo Text_OnEnable_m1181_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Text_OnEnable_m1181/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnDisable()
extern const MethodInfo Text_OnDisable_m1182_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Text_OnDisable_m1182/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern const MethodInfo Text_UpdateGeometry_m1183_MethodInfo = 
{
	"UpdateGeometry"/* name */
	, (methodPointerType)&Text_UpdateGeometry_m1183/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo Text_t223_Text_GetGenerationSettings_m1184_ParameterInfos[] = 
{
	{"extents", 0, 134218300, 0, &Vector2_t53_0_0_0},
};
extern const Il2CppType TextGenerationSettings_t330_0_0_0;
extern void* RuntimeInvoker_TextGenerationSettings_t330_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
extern const MethodInfo Text_GetGenerationSettings_m1184_MethodInfo = 
{
	"GetGenerationSettings"/* name */
	, (methodPointerType)&Text_GetGenerationSettings_m1184/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerationSettings_t330_0_0_0/* return_type */
	, RuntimeInvoker_TextGenerationSettings_t330_Vector2_t53/* invoker_method */
	, Text_t223_Text_GetGenerationSettings_m1184_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t388_0_0_0;
static const ParameterInfo Text_t223_Text_GetTextAnchorPivot_m1185_ParameterInfos[] = 
{
	{"anchor", 0, 134218301, 0, &TextAnchor_t388_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t53_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
extern const MethodInfo Text_GetTextAnchorPivot_m1185_MethodInfo = 
{
	"GetTextAnchorPivot"/* name */
	, (methodPointerType)&Text_GetTextAnchorPivot_m1185/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53_Int32_t54/* invoker_method */
	, Text_t223_Text_GetTextAnchorPivot_m1185_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo Text_t223_Text_OnFillVBO_m1186_ParameterInfos[] = 
{
	{"vbo", 0, 134218302, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Text_OnFillVBO_m1186_MethodInfo = 
{
	"OnFillVBO"/* name */
	, (methodPointerType)&Text_OnFillVBO_m1186/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Text_t223_Text_OnFillVBO_m1186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1187_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputHorizontal_m1187/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 49/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern const MethodInfo Text_CalculateLayoutInputVertical_m1188_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputVertical_m1188/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minWidth()
extern const MethodInfo Text_get_minWidth_m1189_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&Text_get_minWidth_m1189/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern const MethodInfo Text_get_preferredWidth_m1190_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&Text_get_preferredWidth_m1190/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern const MethodInfo Text_get_flexibleWidth_m1191_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&Text_get_flexibleWidth_m1191/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 53/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minHeight()
extern const MethodInfo Text_get_minHeight_m1192_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&Text_get_minHeight_m1192/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 54/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern const MethodInfo Text_get_preferredHeight_m1193_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&Text_get_preferredHeight_m1193/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 55/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern const MethodInfo Text_get_flexibleHeight_m1194_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&Text_get_flexibleHeight_m1194/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 56/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern const MethodInfo Text_get_layoutPriority_m1195_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&Text_get_layoutPriority_m1195/* method */
	, &Text_t223_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Text_t223_MethodInfos[] =
{
	&Text__ctor_m1149_MethodInfo,
	&Text__cctor_m1150_MethodInfo,
	&Text_get_cachedTextGenerator_m1151_MethodInfo,
	&Text_get_cachedTextGeneratorForLayout_m1152_MethodInfo,
	&Text_get_defaultMaterial_m1153_MethodInfo,
	&Text_get_mainTexture_m1154_MethodInfo,
	&Text_FontTextureChanged_m1155_MethodInfo,
	&Text_get_font_m1156_MethodInfo,
	&Text_set_font_m1157_MethodInfo,
	&Text_get_text_m1158_MethodInfo,
	&Text_set_text_m1159_MethodInfo,
	&Text_get_supportRichText_m1160_MethodInfo,
	&Text_set_supportRichText_m1161_MethodInfo,
	&Text_get_resizeTextForBestFit_m1162_MethodInfo,
	&Text_set_resizeTextForBestFit_m1163_MethodInfo,
	&Text_get_resizeTextMinSize_m1164_MethodInfo,
	&Text_set_resizeTextMinSize_m1165_MethodInfo,
	&Text_get_resizeTextMaxSize_m1166_MethodInfo,
	&Text_set_resizeTextMaxSize_m1167_MethodInfo,
	&Text_get_alignment_m1168_MethodInfo,
	&Text_set_alignment_m1169_MethodInfo,
	&Text_get_fontSize_m1170_MethodInfo,
	&Text_set_fontSize_m1171_MethodInfo,
	&Text_get_horizontalOverflow_m1172_MethodInfo,
	&Text_set_horizontalOverflow_m1173_MethodInfo,
	&Text_get_verticalOverflow_m1174_MethodInfo,
	&Text_set_verticalOverflow_m1175_MethodInfo,
	&Text_get_lineSpacing_m1176_MethodInfo,
	&Text_set_lineSpacing_m1177_MethodInfo,
	&Text_get_fontStyle_m1178_MethodInfo,
	&Text_set_fontStyle_m1179_MethodInfo,
	&Text_get_pixelsPerUnit_m1180_MethodInfo,
	&Text_OnEnable_m1181_MethodInfo,
	&Text_OnDisable_m1182_MethodInfo,
	&Text_UpdateGeometry_m1183_MethodInfo,
	&Text_GetGenerationSettings_m1184_MethodInfo,
	&Text_GetTextAnchorPivot_m1185_MethodInfo,
	&Text_OnFillVBO_m1186_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1187_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1188_MethodInfo,
	&Text_get_minWidth_m1189_MethodInfo,
	&Text_get_preferredWidth_m1190_MethodInfo,
	&Text_get_flexibleWidth_m1191_MethodInfo,
	&Text_get_minHeight_m1192_MethodInfo,
	&Text_get_preferredHeight_m1193_MethodInfo,
	&Text_get_flexibleHeight_m1194_MethodInfo,
	&Text_get_layoutPriority_m1195_MethodInfo,
	NULL
};
extern const MethodInfo Text_get_cachedTextGenerator_m1151_MethodInfo;
static const PropertyInfo Text_t223____cachedTextGenerator_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "cachedTextGenerator"/* name */
	, &Text_get_cachedTextGenerator_m1151_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1152_MethodInfo;
static const PropertyInfo Text_t223____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "cachedTextGeneratorForLayout"/* name */
	, &Text_get_cachedTextGeneratorForLayout_m1152_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_defaultMaterial_m1153_MethodInfo;
static const PropertyInfo Text_t223____defaultMaterial_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "defaultMaterial"/* name */
	, &Text_get_defaultMaterial_m1153_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_mainTexture_m1154_MethodInfo;
static const PropertyInfo Text_t223____mainTexture_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "mainTexture"/* name */
	, &Text_get_mainTexture_m1154_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_font_m1156_MethodInfo;
extern const MethodInfo Text_set_font_m1157_MethodInfo;
static const PropertyInfo Text_t223____font_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "font"/* name */
	, &Text_get_font_m1156_MethodInfo/* get */
	, &Text_set_font_m1157_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_text_m1158_MethodInfo;
extern const MethodInfo Text_set_text_m1159_MethodInfo;
static const PropertyInfo Text_t223____text_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "text"/* name */
	, &Text_get_text_m1158_MethodInfo/* get */
	, &Text_set_text_m1159_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_supportRichText_m1160_MethodInfo;
extern const MethodInfo Text_set_supportRichText_m1161_MethodInfo;
static const PropertyInfo Text_t223____supportRichText_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "supportRichText"/* name */
	, &Text_get_supportRichText_m1160_MethodInfo/* get */
	, &Text_set_supportRichText_m1161_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextForBestFit_m1162_MethodInfo;
extern const MethodInfo Text_set_resizeTextForBestFit_m1163_MethodInfo;
static const PropertyInfo Text_t223____resizeTextForBestFit_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "resizeTextForBestFit"/* name */
	, &Text_get_resizeTextForBestFit_m1162_MethodInfo/* get */
	, &Text_set_resizeTextForBestFit_m1163_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMinSize_m1164_MethodInfo;
extern const MethodInfo Text_set_resizeTextMinSize_m1165_MethodInfo;
static const PropertyInfo Text_t223____resizeTextMinSize_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "resizeTextMinSize"/* name */
	, &Text_get_resizeTextMinSize_m1164_MethodInfo/* get */
	, &Text_set_resizeTextMinSize_m1165_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMaxSize_m1166_MethodInfo;
extern const MethodInfo Text_set_resizeTextMaxSize_m1167_MethodInfo;
static const PropertyInfo Text_t223____resizeTextMaxSize_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "resizeTextMaxSize"/* name */
	, &Text_get_resizeTextMaxSize_m1166_MethodInfo/* get */
	, &Text_set_resizeTextMaxSize_m1167_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_alignment_m1168_MethodInfo;
extern const MethodInfo Text_set_alignment_m1169_MethodInfo;
static const PropertyInfo Text_t223____alignment_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "alignment"/* name */
	, &Text_get_alignment_m1168_MethodInfo/* get */
	, &Text_set_alignment_m1169_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontSize_m1170_MethodInfo;
extern const MethodInfo Text_set_fontSize_m1171_MethodInfo;
static const PropertyInfo Text_t223____fontSize_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "fontSize"/* name */
	, &Text_get_fontSize_m1170_MethodInfo/* get */
	, &Text_set_fontSize_m1171_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_horizontalOverflow_m1172_MethodInfo;
extern const MethodInfo Text_set_horizontalOverflow_m1173_MethodInfo;
static const PropertyInfo Text_t223____horizontalOverflow_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "horizontalOverflow"/* name */
	, &Text_get_horizontalOverflow_m1172_MethodInfo/* get */
	, &Text_set_horizontalOverflow_m1173_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_verticalOverflow_m1174_MethodInfo;
extern const MethodInfo Text_set_verticalOverflow_m1175_MethodInfo;
static const PropertyInfo Text_t223____verticalOverflow_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "verticalOverflow"/* name */
	, &Text_get_verticalOverflow_m1174_MethodInfo/* get */
	, &Text_set_verticalOverflow_m1175_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_lineSpacing_m1176_MethodInfo;
extern const MethodInfo Text_set_lineSpacing_m1177_MethodInfo;
static const PropertyInfo Text_t223____lineSpacing_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "lineSpacing"/* name */
	, &Text_get_lineSpacing_m1176_MethodInfo/* get */
	, &Text_set_lineSpacing_m1177_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontStyle_m1178_MethodInfo;
extern const MethodInfo Text_set_fontStyle_m1179_MethodInfo;
static const PropertyInfo Text_t223____fontStyle_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "fontStyle"/* name */
	, &Text_get_fontStyle_m1178_MethodInfo/* get */
	, &Text_set_fontStyle_m1179_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_pixelsPerUnit_m1180_MethodInfo;
static const PropertyInfo Text_t223____pixelsPerUnit_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "pixelsPerUnit"/* name */
	, &Text_get_pixelsPerUnit_m1180_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minWidth_m1189_MethodInfo;
static const PropertyInfo Text_t223____minWidth_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &Text_get_minWidth_m1189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredWidth_m1190_MethodInfo;
static const PropertyInfo Text_t223____preferredWidth_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &Text_get_preferredWidth_m1190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleWidth_m1191_MethodInfo;
static const PropertyInfo Text_t223____flexibleWidth_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &Text_get_flexibleWidth_m1191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minHeight_m1192_MethodInfo;
static const PropertyInfo Text_t223____minHeight_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &Text_get_minHeight_m1192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredHeight_m1193_MethodInfo;
static const PropertyInfo Text_t223____preferredHeight_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &Text_get_preferredHeight_m1193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleHeight_m1194_MethodInfo;
static const PropertyInfo Text_t223____flexibleHeight_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &Text_get_flexibleHeight_m1194_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_layoutPriority_m1195_MethodInfo;
static const PropertyInfo Text_t223____layoutPriority_PropertyInfo = 
{
	&Text_t223_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &Text_get_layoutPriority_m1195_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Text_t223_PropertyInfos[] =
{
	&Text_t223____cachedTextGenerator_PropertyInfo,
	&Text_t223____cachedTextGeneratorForLayout_PropertyInfo,
	&Text_t223____defaultMaterial_PropertyInfo,
	&Text_t223____mainTexture_PropertyInfo,
	&Text_t223____font_PropertyInfo,
	&Text_t223____text_PropertyInfo,
	&Text_t223____supportRichText_PropertyInfo,
	&Text_t223____resizeTextForBestFit_PropertyInfo,
	&Text_t223____resizeTextMinSize_PropertyInfo,
	&Text_t223____resizeTextMaxSize_PropertyInfo,
	&Text_t223____alignment_PropertyInfo,
	&Text_t223____fontSize_PropertyInfo,
	&Text_t223____horizontalOverflow_PropertyInfo,
	&Text_t223____verticalOverflow_PropertyInfo,
	&Text_t223____lineSpacing_PropertyInfo,
	&Text_t223____fontStyle_PropertyInfo,
	&Text_t223____pixelsPerUnit_PropertyInfo,
	&Text_t223____minWidth_PropertyInfo,
	&Text_t223____preferredWidth_PropertyInfo,
	&Text_t223____flexibleWidth_PropertyInfo,
	&Text_t223____minHeight_PropertyInfo,
	&Text_t223____preferredHeight_PropertyInfo,
	&Text_t223____flexibleHeight_PropertyInfo,
	&Text_t223____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo Text_OnEnable_m1181_MethodInfo;
extern const MethodInfo Text_OnDisable_m1182_MethodInfo;
extern const MethodInfo Graphic_OnRectTransformDimensionsChange_m647_MethodInfo;
extern const MethodInfo Graphic_OnBeforeTransformParentChanged_m648_MethodInfo;
extern const MethodInfo MaskableGraphic_OnTransformParentChanged_m904_MethodInfo;
extern const MethodInfo Graphic_OnDidApplyAnimationProperties_m668_MethodInfo;
extern const MethodInfo Graphic_OnCanvasHierarchyChanged_m663_MethodInfo;
extern const MethodInfo Graphic_Rebuild_m664_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_get_transform_m686_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m685_MethodInfo;
extern const MethodInfo Graphic_SetAllDirty_m643_MethodInfo;
extern const MethodInfo Graphic_SetLayoutDirty_m644_MethodInfo;
extern const MethodInfo Graphic_SetVerticesDirty_m645_MethodInfo;
extern const MethodInfo MaskableGraphic_SetMaterialDirty_m907_MethodInfo;
extern const MethodInfo MaskableGraphic_get_material_m899_MethodInfo;
extern const MethodInfo MaskableGraphic_set_material_m900_MethodInfo;
extern const MethodInfo Graphic_get_materialForRendering_m658_MethodInfo;
extern const MethodInfo Text_UpdateGeometry_m1183_MethodInfo;
extern const MethodInfo Graphic_UpdateMaterial_m666_MethodInfo;
extern const MethodInfo Text_OnFillVBO_m1186_MethodInfo;
extern const MethodInfo Graphic_SetNativeSize_m669_MethodInfo;
extern const MethodInfo Graphic_Raycast_m670_MethodInfo;
extern const MethodInfo MaskableGraphic_ParentMaskStateChanged_m905_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1187_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputVertical_m1188_MethodInfo;
static const Il2CppMethodReference Text_t223_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&Text_OnEnable_m1181_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&Text_OnDisable_m1182_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&Graphic_OnRectTransformDimensionsChange_m647_MethodInfo,
	&Graphic_OnBeforeTransformParentChanged_m648_MethodInfo,
	&MaskableGraphic_OnTransformParentChanged_m904_MethodInfo,
	&Graphic_OnDidApplyAnimationProperties_m668_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&Graphic_OnCanvasHierarchyChanged_m663_MethodInfo,
	&Graphic_Rebuild_m664_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m686_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m685_MethodInfo,
	&Graphic_SetAllDirty_m643_MethodInfo,
	&Graphic_SetLayoutDirty_m644_MethodInfo,
	&Graphic_SetVerticesDirty_m645_MethodInfo,
	&MaskableGraphic_SetMaterialDirty_m907_MethodInfo,
	&Text_get_defaultMaterial_m1153_MethodInfo,
	&MaskableGraphic_get_material_m899_MethodInfo,
	&MaskableGraphic_set_material_m900_MethodInfo,
	&Graphic_get_materialForRendering_m658_MethodInfo,
	&Text_get_mainTexture_m1154_MethodInfo,
	&Graphic_Rebuild_m664_MethodInfo,
	&Text_UpdateGeometry_m1183_MethodInfo,
	&Graphic_UpdateMaterial_m666_MethodInfo,
	&Text_OnFillVBO_m1186_MethodInfo,
	&Graphic_SetNativeSize_m669_MethodInfo,
	&Graphic_Raycast_m670_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m685_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m686_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m905_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m905_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1187_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1188_MethodInfo,
	&Text_get_minWidth_m1189_MethodInfo,
	&Text_get_preferredWidth_m1190_MethodInfo,
	&Text_get_flexibleWidth_m1191_MethodInfo,
	&Text_get_minHeight_m1192_MethodInfo,
	&Text_get_preferredHeight_m1193_MethodInfo,
	&Text_get_flexibleHeight_m1194_MethodInfo,
	&Text_get_layoutPriority_m1195_MethodInfo,
	&Text_get_text_m1158_MethodInfo,
	&Text_set_text_m1159_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1187_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1188_MethodInfo,
	&Text_get_minWidth_m1189_MethodInfo,
	&Text_get_preferredWidth_m1190_MethodInfo,
	&Text_get_flexibleWidth_m1191_MethodInfo,
	&Text_get_minHeight_m1192_MethodInfo,
	&Text_get_preferredHeight_m1193_MethodInfo,
	&Text_get_flexibleHeight_m1194_MethodInfo,
	&Text_get_layoutPriority_m1195_MethodInfo,
};
static bool Text_t223_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const Il2CppType* Text_t223_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t333_0_0_0,
};
extern const Il2CppType IMaskable_t395_0_0_0;
static Il2CppInterfaceOffsetPair Text_t223_InterfacesOffsets[] = 
{
	{ &IMaskable_t395_0_0_0, 36},
	{ &ICanvasElement_t325_0_0_0, 16},
	{ &ILayoutElement_t333_0_0_0, 38},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Text_t223_0_0_0;
extern const Il2CppType Text_t223_1_0_0;
extern const Il2CppType MaskableGraphic_t204_0_0_0;
struct Text_t223;
const Il2CppTypeDefinitionMetadata Text_t223_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t223_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t223_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t204_0_0_0/* parent */
	, Text_t223_VTable/* vtableMethods */
	, Text_t223_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 496/* fieldStart */

};
TypeInfo Text_t223_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Text_t223_MethodInfos/* methods */
	, Text_t223_PropertyInfos/* properties */
	, NULL/* events */
	, &Text_t223_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 255/* custom_attributes_cache */
	, &Text_t223_0_0_0/* byval_arg */
	, &Text_t223_1_0_0/* this_arg */
	, &Text_t223_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t223)/* instance_size */
	, sizeof (Text_t223)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t223_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 58/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t261_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static const MethodInfo* ToggleTransition_t261_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ToggleTransition_t261_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ToggleTransition_t261_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t261_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleTransition_t261_0_0_0;
extern const Il2CppType ToggleTransition_t261_1_0_0;
extern TypeInfo Toggle_t265_il2cpp_TypeInfo;
extern const Il2CppType Toggle_t265_0_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t261_DefinitionMetadata = 
{
	&Toggle_t265_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t261_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ToggleTransition_t261_VTable/* vtableMethods */
	, ToggleTransition_t261_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 502/* fieldStart */

};
TypeInfo ToggleTransition_t261_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, ToggleTransition_t261_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t261_0_0_0/* byval_arg */
	, &ToggleTransition_t261_1_0_0/* this_arg */
	, &ToggleTransition_t261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t261)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t261)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t262_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern const MethodInfo ToggleEvent__ctor_m1196_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleEvent__ctor_m1196/* method */
	, &ToggleEvent_t262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleEvent_t262_MethodInfos[] =
{
	&ToggleEvent__ctor_m1196_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2131_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2132_GenericMethod;
static const Il2CppMethodReference ToggleEvent_t262_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2131_GenericMethod,
	&UnityEvent_1_GetDelegate_m2132_GenericMethod,
};
static bool ToggleEvent_t262_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t262_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleEvent_t262_0_0_0;
extern const Il2CppType ToggleEvent_t262_1_0_0;
extern const Il2CppType UnityEvent_1_t263_0_0_0;
struct ToggleEvent_t262;
const Il2CppTypeDefinitionMetadata ToggleEvent_t262_DefinitionMetadata = 
{
	&Toggle_t265_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t262_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t263_0_0_0/* parent */
	, ToggleEvent_t262_VTable/* vtableMethods */
	, ToggleEvent_t262_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ToggleEvent_t262_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, ToggleEvent_t262_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ToggleEvent_t262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t262_0_0_0/* byval_arg */
	, &ToggleEvent_t262_1_0_0/* this_arg */
	, &ToggleEvent_t262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t262)/* instance_size */
	, sizeof (ToggleEvent_t262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::.ctor()
extern const MethodInfo Toggle__ctor_m1197_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Toggle__ctor_m1197/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t264_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern const MethodInfo Toggle_get_group_m1198_MethodInfo = 
{
	"get_group"/* name */
	, (methodPointerType)&Toggle_get_group_m1198/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &ToggleGroup_t264_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t264_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_set_group_m1199_ParameterInfos[] = 
{
	{"value", 0, 134218303, 0, &ToggleGroup_t264_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
extern const MethodInfo Toggle_set_group_m1199_MethodInfo = 
{
	"set_group"/* name */
	, (methodPointerType)&Toggle_set_group_m1199/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Toggle_t265_Toggle_set_group_m1199_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t170_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_Rebuild_m1200_ParameterInfos[] = 
{
	{"executing", 0, 134218304, 0, &CanvasUpdate_t170_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Toggle_Rebuild_m1200_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Toggle_Rebuild_m1200/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Toggle_t265_Toggle_Rebuild_m1200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern const MethodInfo Toggle_OnEnable_m1201_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Toggle_OnEnable_m1201/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern const MethodInfo Toggle_OnDisable_m1202_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Toggle_OnDisable_m1202/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t264_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_SetToggleGroup_m1203_ParameterInfos[] = 
{
	{"newGroup", 0, 134218305, 0, &ToggleGroup_t264_0_0_0},
	{"setMemberValue", 1, 134218306, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
extern const MethodInfo Toggle_SetToggleGroup_m1203_MethodInfo = 
{
	"SetToggleGroup"/* name */
	, (methodPointerType)&Toggle_SetToggleGroup_m1203/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Toggle_t265_Toggle_SetToggleGroup_m1203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern const MethodInfo Toggle_get_isOn_m1204_MethodInfo = 
{
	"get_isOn"/* name */
	, (methodPointerType)&Toggle_get_isOn_m1204/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_set_isOn_m1205_ParameterInfos[] = 
{
	{"value", 0, 134218307, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern const MethodInfo Toggle_set_isOn_m1205_MethodInfo = 
{
	"set_isOn"/* name */
	, (methodPointerType)&Toggle_set_isOn_m1205/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Toggle_t265_Toggle_set_isOn_m1205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_Set_m1206_ParameterInfos[] = 
{
	{"value", 0, 134218308, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern const MethodInfo Toggle_Set_m1206_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1206/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Toggle_t265_Toggle_Set_m1206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_Set_m1207_ParameterInfos[] = 
{
	{"value", 0, 134218309, 0, &Boolean_t72_0_0_0},
	{"sendCallback", 1, 134218310, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
extern const MethodInfo Toggle_Set_m1207_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1207/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_SByte_t73/* invoker_method */
	, Toggle_t265_Toggle_Set_m1207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_PlayEffect_m1208_ParameterInfos[] = 
{
	{"instant", 0, 134218311, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
extern const MethodInfo Toggle_PlayEffect_m1208_MethodInfo = 
{
	"PlayEffect"/* name */
	, (methodPointerType)&Toggle_PlayEffect_m1208/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Toggle_t265_Toggle_PlayEffect_m1208_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Start()
extern const MethodInfo Toggle_Start_m1209_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Toggle_Start_m1209/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern const MethodInfo Toggle_InternalToggle_m1210_MethodInfo = 
{
	"InternalToggle"/* name */
	, (methodPointerType)&Toggle_InternalToggle_m1210/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t143_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_OnPointerClick_m1211_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, 0, &PointerEventData_t143_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Toggle_OnPointerClick_m1211_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&Toggle_OnPointerClick_m1211/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Toggle_t265_Toggle_OnPointerClick_m1211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t102_0_0_0;
static const ParameterInfo Toggle_t265_Toggle_OnSubmit_m1212_ParameterInfos[] = 
{
	{"eventData", 0, 134218313, 0, &BaseEventData_t102_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Toggle_OnSubmit_m1212_MethodInfo = 
{
	"OnSubmit"/* name */
	, (methodPointerType)&Toggle_OnSubmit_m1212/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Toggle_t265_Toggle_OnSubmit_m1212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214/* method */
	, &Toggle_t265_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Toggle_t265_MethodInfos[] =
{
	&Toggle__ctor_m1197_MethodInfo,
	&Toggle_get_group_m1198_MethodInfo,
	&Toggle_set_group_m1199_MethodInfo,
	&Toggle_Rebuild_m1200_MethodInfo,
	&Toggle_OnEnable_m1201_MethodInfo,
	&Toggle_OnDisable_m1202_MethodInfo,
	&Toggle_SetToggleGroup_m1203_MethodInfo,
	&Toggle_get_isOn_m1204_MethodInfo,
	&Toggle_set_isOn_m1205_MethodInfo,
	&Toggle_Set_m1206_MethodInfo,
	&Toggle_Set_m1207_MethodInfo,
	&Toggle_PlayEffect_m1208_MethodInfo,
	&Toggle_Start_m1209_MethodInfo,
	&Toggle_InternalToggle_m1210_MethodInfo,
	&Toggle_OnPointerClick_m1211_MethodInfo,
	&Toggle_OnSubmit_m1212_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214_MethodInfo,
	NULL
};
extern const MethodInfo Toggle_get_group_m1198_MethodInfo;
extern const MethodInfo Toggle_set_group_m1199_MethodInfo;
static const PropertyInfo Toggle_t265____group_PropertyInfo = 
{
	&Toggle_t265_il2cpp_TypeInfo/* parent */
	, "group"/* name */
	, &Toggle_get_group_m1198_MethodInfo/* get */
	, &Toggle_set_group_m1199_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Toggle_get_isOn_m1204_MethodInfo;
extern const MethodInfo Toggle_set_isOn_m1205_MethodInfo;
static const PropertyInfo Toggle_t265____isOn_PropertyInfo = 
{
	&Toggle_t265_il2cpp_TypeInfo/* parent */
	, "isOn"/* name */
	, &Toggle_get_isOn_m1204_MethodInfo/* get */
	, &Toggle_set_isOn_m1205_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Toggle_t265_PropertyInfos[] =
{
	&Toggle_t265____group_PropertyInfo,
	&Toggle_t265____isOn_PropertyInfo,
	NULL
};
static const Il2CppType* Toggle_t265_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t261_0_0_0,
	&ToggleEvent_t262_0_0_0,
};
extern const MethodInfo Toggle_OnEnable_m1201_MethodInfo;
extern const MethodInfo Toggle_Start_m1209_MethodInfo;
extern const MethodInfo Toggle_OnDisable_m1202_MethodInfo;
extern const MethodInfo Toggle_OnPointerClick_m1211_MethodInfo;
extern const MethodInfo Toggle_OnSubmit_m1212_MethodInfo;
extern const MethodInfo Toggle_Rebuild_m1200_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213_MethodInfo;
static const Il2CppMethodReference Toggle_t265_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&Selectable_Awake_m1060_MethodInfo,
	&Toggle_OnEnable_m1201_MethodInfo,
	&Toggle_Start_m1209_MethodInfo,
	&Toggle_OnDisable_m1202_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1063_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1061_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnPointerDown_m1087_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_OnMove_m1077_MethodInfo,
	&Selectable_IsInteractable_m1062_MethodInfo,
	&Selectable_InstantClearState_m1068_MethodInfo,
	&Selectable_DoStateTransition_m1069_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1073_MethodInfo,
	&Selectable_FindSelectableOnRight_m1074_MethodInfo,
	&Selectable_FindSelectableOnUp_m1075_MethodInfo,
	&Selectable_FindSelectableOnDown_m1076_MethodInfo,
	&Selectable_OnMove_m1077_MethodInfo,
	&Selectable_OnPointerDown_m1087_MethodInfo,
	&Selectable_OnPointerUp_m1088_MethodInfo,
	&Selectable_OnPointerEnter_m1089_MethodInfo,
	&Selectable_OnPointerExit_m1090_MethodInfo,
	&Selectable_OnSelect_m1091_MethodInfo,
	&Selectable_OnDeselect_m1092_MethodInfo,
	&Selectable_Select_m1093_MethodInfo,
	&Toggle_OnPointerClick_m1211_MethodInfo,
	&Toggle_OnSubmit_m1212_MethodInfo,
	&Toggle_Rebuild_m1200_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213_MethodInfo,
	&Toggle_Rebuild_m1200_MethodInfo,
	&Toggle_OnPointerClick_m1211_MethodInfo,
	&Toggle_OnSubmit_m1212_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1213_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1214_MethodInfo,
};
static bool Toggle_t265_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerClickHandler_t307_0_0_0;
extern const Il2CppType ISubmitHandler_t318_0_0_0;
static const Il2CppType* Toggle_t265_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t407_0_0_0,
	&IPointerClickHandler_t307_0_0_0,
	&ISubmitHandler_t318_0_0_0,
	&ICanvasElement_t325_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t265_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t407_0_0_0, 16},
	{ &IPointerEnterHandler_t303_0_0_0, 16},
	{ &IPointerExitHandler_t304_0_0_0, 17},
	{ &IPointerDownHandler_t305_0_0_0, 18},
	{ &IPointerUpHandler_t306_0_0_0, 19},
	{ &ISelectHandler_t315_0_0_0, 20},
	{ &IDeselectHandler_t316_0_0_0, 21},
	{ &IMoveHandler_t317_0_0_0, 22},
	{ &IPointerClickHandler_t307_0_0_0, 38},
	{ &ISubmitHandler_t318_0_0_0, 39},
	{ &ICanvasElement_t325_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Toggle_t265_1_0_0;
struct Toggle_t265;
const Il2CppTypeDefinitionMetadata Toggle_t265_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t265_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t265_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t265_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t169_0_0_0/* parent */
	, Toggle_t265_VTable/* vtableMethods */
	, Toggle_t265_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 505/* fieldStart */

};
TypeInfo Toggle_t265_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Toggle_t265_MethodInfos/* methods */
	, Toggle_t265_PropertyInfos/* properties */
	, NULL/* events */
	, &Toggle_t265_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 258/* custom_attributes_cache */
	, &Toggle_t265_0_0_0/* byval_arg */
	, &Toggle_t265_1_0_0/* this_arg */
	, &Toggle_t265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t265)/* instance_size */
	, sizeof (Toggle_t265)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t264_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
extern const MethodInfo ToggleGroup__ctor_m1215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleGroup__ctor_m1215/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1216_MethodInfo = 
{
	"get_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_get_allowSwitchOff_m1216/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_set_allowSwitchOff_m1217_ParameterInfos[] = 
{
	{"value", 0, 134218314, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1217_MethodInfo = 
{
	"set_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_set_allowSwitchOff_m1217/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_set_allowSwitchOff_m1217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_ValidateToggleIsInGroup_m1218_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_ValidateToggleIsInGroup_m1218_MethodInfo = 
{
	"ValidateToggleIsInGroup"/* name */
	, (methodPointerType)&ToggleGroup_ValidateToggleIsInGroup_m1218/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_ValidateToggleIsInGroup_m1218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_NotifyToggleOn_m1219_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_NotifyToggleOn_m1219_MethodInfo = 
{
	"NotifyToggleOn"/* name */
	, (methodPointerType)&ToggleGroup_NotifyToggleOn_m1219/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_NotifyToggleOn_m1219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_UnregisterToggle_m1220_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_UnregisterToggle_m1220_MethodInfo = 
{
	"UnregisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_UnregisterToggle_m1220/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_UnregisterToggle_m1220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_RegisterToggle_m1221_ParameterInfos[] = 
{
	{"toggle", 0, 134218318, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_RegisterToggle_m1221_MethodInfo = 
{
	"RegisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_RegisterToggle_m1221/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_RegisterToggle_m1221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
extern const MethodInfo ToggleGroup_AnyTogglesOn_m1222_MethodInfo = 
{
	"AnyTogglesOn"/* name */
	, (methodPointerType)&ToggleGroup_AnyTogglesOn_m1222/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerable_1_t331_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
extern const MethodInfo ToggleGroup_ActiveToggles_m1223_MethodInfo = 
{
	"ActiveToggles"/* name */
	, (methodPointerType)&ToggleGroup_ActiveToggles_m1223/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t331_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern const MethodInfo ToggleGroup_SetAllTogglesOff_m1224_MethodInfo = 
{
	"SetAllTogglesOff"/* name */
	, (methodPointerType)&ToggleGroup_SetAllTogglesOff_m1224/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225_ParameterInfos[] = 
{
	{"x", 0, 134218319, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225_MethodInfo = 
{
	"<AnyTogglesOn>m__7"/* name */
	, (methodPointerType)&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225_ParameterInfos/* parameters */
	, 265/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t265_0_0_0;
static const ParameterInfo ToggleGroup_t264_ToggleGroup_U3CActiveTogglesU3Em__8_m1226_ParameterInfos[] = 
{
	{"x", 0, 134218320, 0, &Toggle_t265_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CActiveTogglesU3Em__8_m1226_MethodInfo = 
{
	"<ActiveToggles>m__8"/* name */
	, (methodPointerType)&ToggleGroup_U3CActiveTogglesU3Em__8_m1226/* method */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, ToggleGroup_t264_ToggleGroup_U3CActiveTogglesU3Em__8_m1226_ParameterInfos/* parameters */
	, 266/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleGroup_t264_MethodInfos[] =
{
	&ToggleGroup__ctor_m1215_MethodInfo,
	&ToggleGroup_get_allowSwitchOff_m1216_MethodInfo,
	&ToggleGroup_set_allowSwitchOff_m1217_MethodInfo,
	&ToggleGroup_ValidateToggleIsInGroup_m1218_MethodInfo,
	&ToggleGroup_NotifyToggleOn_m1219_MethodInfo,
	&ToggleGroup_UnregisterToggle_m1220_MethodInfo,
	&ToggleGroup_RegisterToggle_m1221_MethodInfo,
	&ToggleGroup_AnyTogglesOn_m1222_MethodInfo,
	&ToggleGroup_ActiveToggles_m1223_MethodInfo,
	&ToggleGroup_SetAllTogglesOff_m1224_MethodInfo,
	&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1225_MethodInfo,
	&ToggleGroup_U3CActiveTogglesU3Em__8_m1226_MethodInfo,
	NULL
};
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1216_MethodInfo;
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1217_MethodInfo;
static const PropertyInfo ToggleGroup_t264____allowSwitchOff_PropertyInfo = 
{
	&ToggleGroup_t264_il2cpp_TypeInfo/* parent */
	, "allowSwitchOff"/* name */
	, &ToggleGroup_get_allowSwitchOff_m1216_MethodInfo/* get */
	, &ToggleGroup_set_allowSwitchOff_m1217_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ToggleGroup_t264_PropertyInfos[] =
{
	&ToggleGroup_t264____allowSwitchOff_PropertyInfo,
	NULL
};
extern const MethodInfo UIBehaviour_OnEnable_m366_MethodInfo;
extern const MethodInfo UIBehaviour_OnDisable_m368_MethodInfo;
static const Il2CppMethodReference ToggleGroup_t264_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&UIBehaviour_OnEnable_m366_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&UIBehaviour_OnDisable_m368_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
};
static bool ToggleGroup_t264_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleGroup_t264_1_0_0;
struct ToggleGroup_t264;
const Il2CppTypeDefinitionMetadata ToggleGroup_t264_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, ToggleGroup_t264_VTable/* vtableMethods */
	, ToggleGroup_t264_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 510/* fieldStart */

};
TypeInfo ToggleGroup_t264_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ToggleGroup_t264_MethodInfos/* methods */
	, ToggleGroup_t264_PropertyInfos/* properties */
	, NULL/* events */
	, &ToggleGroup_t264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 261/* custom_attributes_cache */
	, &ToggleGroup_t264_0_0_0/* byval_arg */
	, &ToggleGroup_t264_1_0_0/* this_arg */
	, &ToggleGroup_t264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t264)/* instance_size */
	, sizeof (ToggleGroup_t264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t264_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t269_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static const MethodInfo* AspectMode_t269_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AspectMode_t269_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool AspectMode_t269_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AspectMode_t269_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectMode_t269_0_0_0;
extern const Il2CppType AspectMode_t269_1_0_0;
extern TypeInfo AspectRatioFitter_t270_il2cpp_TypeInfo;
extern const Il2CppType AspectRatioFitter_t270_0_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t269_DefinitionMetadata = 
{
	&AspectRatioFitter_t270_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t269_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, AspectMode_t269_VTable/* vtableMethods */
	, AspectMode_t269_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 514/* fieldStart */

};
TypeInfo AspectMode_t269_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, AspectMode_t269_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t269_0_0_0/* byval_arg */
	, &AspectMode_t269_1_0_0/* this_arg */
	, &AspectMode_t269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t269)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t269)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern const MethodInfo AspectRatioFitter__ctor_m1227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AspectRatioFitter__ctor_m1227/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_AspectMode_t269 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1228_MethodInfo = 
{
	"get_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectMode_m1228/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &AspectMode_t269_0_0_0/* return_type */
	, RuntimeInvoker_AspectMode_t269/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AspectMode_t269_0_0_0;
static const ParameterInfo AspectRatioFitter_t270_AspectRatioFitter_set_aspectMode_m1229_ParameterInfos[] = 
{
	{"value", 0, 134218321, 0, &AspectMode_t269_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1229_MethodInfo = 
{
	"set_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectMode_m1229/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, AspectRatioFitter_t270_AspectRatioFitter_set_aspectMode_m1229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1230_MethodInfo = 
{
	"get_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectRatio_m1230/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo AspectRatioFitter_t270_AspectRatioFitter_set_aspectRatio_m1231_ParameterInfos[] = 
{
	{"value", 0, 134218322, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1231_MethodInfo = 
{
	"set_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectRatio_m1231/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, AspectRatioFitter_t270_AspectRatioFitter_set_aspectRatio_m1231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1232_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&AspectRatioFitter_get_rectTransform_m1232/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern const MethodInfo AspectRatioFitter_OnEnable_m1233_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnEnable_m1233/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern const MethodInfo AspectRatioFitter_OnDisable_m1234_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnDisable_m1234/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1235_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&AspectRatioFitter_OnRectTransformDimensionsChange_m1235/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern const MethodInfo AspectRatioFitter_UpdateRect_m1236_MethodInfo = 
{
	"UpdateRect"/* name */
	, (methodPointerType)&AspectRatioFitter_UpdateRect_m1236/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AspectRatioFitter_t270_AspectRatioFitter_GetSizeDeltaToProduceSize_m1237_ParameterInfos[] = 
{
	{"size", 0, 134218323, 0, &Single_t85_0_0_0},
	{"axis", 1, 134218324, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern const MethodInfo AspectRatioFitter_GetSizeDeltaToProduceSize_m1237_MethodInfo = 
{
	"GetSizeDeltaToProduceSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetSizeDeltaToProduceSize_m1237/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Single_t85_Int32_t54/* invoker_method */
	, AspectRatioFitter_t270_AspectRatioFitter_GetSizeDeltaToProduceSize_m1237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern const MethodInfo AspectRatioFitter_GetParentSize_m1238_MethodInfo = 
{
	"GetParentSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetParentSize_m1238/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1239_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutHorizontal_m1239/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1240_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutVertical_m1240/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern const MethodInfo AspectRatioFitter_SetDirty_m1241_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&AspectRatioFitter_SetDirty_m1241/* method */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AspectRatioFitter_t270_MethodInfos[] =
{
	&AspectRatioFitter__ctor_m1227_MethodInfo,
	&AspectRatioFitter_get_aspectMode_m1228_MethodInfo,
	&AspectRatioFitter_set_aspectMode_m1229_MethodInfo,
	&AspectRatioFitter_get_aspectRatio_m1230_MethodInfo,
	&AspectRatioFitter_set_aspectRatio_m1231_MethodInfo,
	&AspectRatioFitter_get_rectTransform_m1232_MethodInfo,
	&AspectRatioFitter_OnEnable_m1233_MethodInfo,
	&AspectRatioFitter_OnDisable_m1234_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1235_MethodInfo,
	&AspectRatioFitter_UpdateRect_m1236_MethodInfo,
	&AspectRatioFitter_GetSizeDeltaToProduceSize_m1237_MethodInfo,
	&AspectRatioFitter_GetParentSize_m1238_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1239_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1240_MethodInfo,
	&AspectRatioFitter_SetDirty_m1241_MethodInfo,
	NULL
};
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1228_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1229_MethodInfo;
static const PropertyInfo AspectRatioFitter_t270____aspectMode_PropertyInfo = 
{
	&AspectRatioFitter_t270_il2cpp_TypeInfo/* parent */
	, "aspectMode"/* name */
	, &AspectRatioFitter_get_aspectMode_m1228_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectMode_m1229_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1230_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1231_MethodInfo;
static const PropertyInfo AspectRatioFitter_t270____aspectRatio_PropertyInfo = 
{
	&AspectRatioFitter_t270_il2cpp_TypeInfo/* parent */
	, "aspectRatio"/* name */
	, &AspectRatioFitter_get_aspectRatio_m1230_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectRatio_m1231_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1232_MethodInfo;
static const PropertyInfo AspectRatioFitter_t270____rectTransform_PropertyInfo = 
{
	&AspectRatioFitter_t270_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &AspectRatioFitter_get_rectTransform_m1232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AspectRatioFitter_t270_PropertyInfos[] =
{
	&AspectRatioFitter_t270____aspectMode_PropertyInfo,
	&AspectRatioFitter_t270____aspectRatio_PropertyInfo,
	&AspectRatioFitter_t270____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* AspectRatioFitter_t270_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t269_0_0_0,
};
extern const MethodInfo AspectRatioFitter_OnEnable_m1233_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnDisable_m1234_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1235_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1239_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1240_MethodInfo;
static const Il2CppMethodReference AspectRatioFitter_t270_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&AspectRatioFitter_OnEnable_m1233_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&AspectRatioFitter_OnDisable_m1234_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1235_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1239_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1240_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1239_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1240_MethodInfo,
};
static bool AspectRatioFitter_t270_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutController_t392_0_0_0;
extern const Il2CppType ILayoutSelfController_t393_0_0_0;
static const Il2CppType* AspectRatioFitter_t270_InterfacesTypeInfos[] = 
{
	&ILayoutController_t392_0_0_0,
	&ILayoutSelfController_t393_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t270_InterfacesOffsets[] = 
{
	{ &ILayoutController_t392_0_0_0, 16},
	{ &ILayoutSelfController_t393_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectRatioFitter_t270_1_0_0;
struct AspectRatioFitter_t270;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t270_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t270_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t270_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, AspectRatioFitter_t270_VTable/* vtableMethods */
	, AspectRatioFitter_t270_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 520/* fieldStart */

};
TypeInfo AspectRatioFitter_t270_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, AspectRatioFitter_t270_MethodInfos/* methods */
	, AspectRatioFitter_t270_PropertyInfos/* properties */
	, NULL/* events */
	, &AspectRatioFitter_t270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 267/* custom_attributes_cache */
	, &AspectRatioFitter_t270_0_0_0/* byval_arg */
	, &AspectRatioFitter_t270_1_0_0/* this_arg */
	, &AspectRatioFitter_t270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t270)/* instance_size */
	, sizeof (AspectRatioFitter_t270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t271_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static const MethodInfo* ScaleMode_t271_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScaleMode_t271_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ScaleMode_t271_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScaleMode_t271_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScaleMode_t271_0_0_0;
extern const Il2CppType ScaleMode_t271_1_0_0;
extern TypeInfo CanvasScaler_t50_il2cpp_TypeInfo;
extern const Il2CppType CanvasScaler_t50_0_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t271_DefinitionMetadata = 
{
	&CanvasScaler_t50_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t271_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ScaleMode_t271_VTable/* vtableMethods */
	, ScaleMode_t271_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 524/* fieldStart */

};
TypeInfo ScaleMode_t271_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, ScaleMode_t271_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t271_0_0_0/* byval_arg */
	, &ScaleMode_t271_1_0_0/* this_arg */
	, &ScaleMode_t271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t271)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t271)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t272_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static const MethodInfo* ScreenMatchMode_t272_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenMatchMode_t272_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ScreenMatchMode_t272_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t272_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScreenMatchMode_t272_0_0_0;
extern const Il2CppType ScreenMatchMode_t272_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t272_DefinitionMetadata = 
{
	&CanvasScaler_t50_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t272_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ScreenMatchMode_t272_VTable/* vtableMethods */
	, ScreenMatchMode_t272_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 528/* fieldStart */

};
TypeInfo ScreenMatchMode_t272_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, ScreenMatchMode_t272_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t272_0_0_0/* byval_arg */
	, &ScreenMatchMode_t272_1_0_0/* this_arg */
	, &ScreenMatchMode_t272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t272)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t272)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t273_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static const MethodInfo* Unit_t273_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Unit_t273_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Unit_t273_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Unit_t273_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Unit_t273_0_0_0;
extern const Il2CppType Unit_t273_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t273_DefinitionMetadata = 
{
	&CanvasScaler_t50_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t273_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Unit_t273_VTable/* vtableMethods */
	, Unit_t273_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 532/* fieldStart */

};
TypeInfo Unit_t273_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, Unit_t273_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t273_0_0_0/* byval_arg */
	, &Unit_t273_1_0_0/* this_arg */
	, &Unit_t273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t273)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t273)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
extern const MethodInfo CanvasScaler__ctor_m1242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CanvasScaler__ctor_m1242/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScaleMode_t271 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1243_MethodInfo = 
{
	"get_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_get_uiScaleMode_m1243/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &ScaleMode_t271_0_0_0/* return_type */
	, RuntimeInvoker_ScaleMode_t271/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScaleMode_t271_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_uiScaleMode_m130_ParameterInfos[] = 
{
	{"value", 0, 134218325, 0, &ScaleMode_t271_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
extern const MethodInfo CanvasScaler_set_uiScaleMode_m130_MethodInfo = 
{
	"set_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_set_uiScaleMode_m130/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_uiScaleMode_m130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1244_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_referencePixelsPerUnit_m1244/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_referencePixelsPerUnit_m135_ParameterInfos[] = 
{
	{"value", 0, 134218326, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m135_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_referencePixelsPerUnit_m135/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_referencePixelsPerUnit_m135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern const MethodInfo CanvasScaler_get_scaleFactor_m1245_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_get_scaleFactor_m1245/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_scaleFactor_m1246_ParameterInfos[] = 
{
	{"value", 0, 134218327, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern const MethodInfo CanvasScaler_set_scaleFactor_m1246_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_set_scaleFactor_m1246/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_scaleFactor_m1246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern const MethodInfo CanvasScaler_get_referenceResolution_m1247_MethodInfo = 
{
	"get_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_get_referenceResolution_m1247/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_referenceResolution_m132_ParameterInfos[] = 
{
	{"value", 0, 134218328, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
extern const MethodInfo CanvasScaler_set_referenceResolution_m132_MethodInfo = 
{
	"set_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_set_referenceResolution_m132/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_referenceResolution_m132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScreenMatchMode_t272 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1248_MethodInfo = 
{
	"get_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_get_screenMatchMode_m1248/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &ScreenMatchMode_t272_0_0_0/* return_type */
	, RuntimeInvoker_ScreenMatchMode_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenMatchMode_t272_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_screenMatchMode_m133_ParameterInfos[] = 
{
	{"value", 0, 134218329, 0, &ScreenMatchMode_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
extern const MethodInfo CanvasScaler_set_screenMatchMode_m133_MethodInfo = 
{
	"set_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_set_screenMatchMode_m133/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_screenMatchMode_m133_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1249_MethodInfo = 
{
	"get_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_get_matchWidthOrHeight_m1249/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_matchWidthOrHeight_m134_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m134_MethodInfo = 
{
	"set_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_set_matchWidthOrHeight_m134/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_matchWidthOrHeight_m134_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Unit_t273 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern const MethodInfo CanvasScaler_get_physicalUnit_m1250_MethodInfo = 
{
	"get_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_physicalUnit_m1250/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Unit_t273_0_0_0/* return_type */
	, RuntimeInvoker_Unit_t273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Unit_t273_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_physicalUnit_m1251_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &Unit_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
extern const MethodInfo CanvasScaler_set_physicalUnit_m1251_MethodInfo = 
{
	"set_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_physicalUnit_m1251/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_physicalUnit_m1251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1252_MethodInfo = 
{
	"get_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_fallbackScreenDPI_m1252/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_fallbackScreenDPI_m1253_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1253_MethodInfo = 
{
	"set_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_fallbackScreenDPI_m1253/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_fallbackScreenDPI_m1253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1254_MethodInfo = 
{
	"get_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_defaultSpriteDPI_m1254/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_defaultSpriteDPI_m1255_ParameterInfos[] = 
{
	{"value", 0, 134218333, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1255_MethodInfo = 
{
	"set_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_defaultSpriteDPI_m1255/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_defaultSpriteDPI_m1255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1256_MethodInfo = 
{
	"get_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_dynamicPixelsPerUnit_m1256/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_set_dynamicPixelsPerUnit_m1257_ParameterInfos[] = 
{
	{"value", 0, 134218334, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1257_MethodInfo = 
{
	"set_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_dynamicPixelsPerUnit_m1257/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_set_dynamicPixelsPerUnit_m1257_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
extern const MethodInfo CanvasScaler_OnEnable_m1258_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CanvasScaler_OnEnable_m1258/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
extern const MethodInfo CanvasScaler_OnDisable_m1259_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CanvasScaler_OnDisable_m1259/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern const MethodInfo CanvasScaler_Update_m1260_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CanvasScaler_Update_m1260/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Handle()
extern const MethodInfo CanvasScaler_Handle_m1261_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&CanvasScaler_Handle_m1261/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1262_MethodInfo = 
{
	"HandleWorldCanvas"/* name */
	, (methodPointerType)&CanvasScaler_HandleWorldCanvas_m1262/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1263_MethodInfo = 
{
	"HandleConstantPixelSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPixelSize_m1263/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1264_MethodInfo = 
{
	"HandleScaleWithScreenSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleScaleWithScreenSize_m1264/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1265_MethodInfo = 
{
	"HandleConstantPhysicalSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPhysicalSize_m1265/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_SetScaleFactor_m1266_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218335, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern const MethodInfo CanvasScaler_SetScaleFactor_m1266_MethodInfo = 
{
	"SetScaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_SetScaleFactor_m1266/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_SetScaleFactor_m1266_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo CanvasScaler_t50_CanvasScaler_SetReferencePixelsPerUnit_m1267_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218336, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_SetReferencePixelsPerUnit_m1267_MethodInfo = 
{
	"SetReferencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_SetReferencePixelsPerUnit_m1267/* method */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, CanvasScaler_t50_CanvasScaler_SetReferencePixelsPerUnit_m1267_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasScaler_t50_MethodInfos[] =
{
	&CanvasScaler__ctor_m1242_MethodInfo,
	&CanvasScaler_get_uiScaleMode_m1243_MethodInfo,
	&CanvasScaler_set_uiScaleMode_m130_MethodInfo,
	&CanvasScaler_get_referencePixelsPerUnit_m1244_MethodInfo,
	&CanvasScaler_set_referencePixelsPerUnit_m135_MethodInfo,
	&CanvasScaler_get_scaleFactor_m1245_MethodInfo,
	&CanvasScaler_set_scaleFactor_m1246_MethodInfo,
	&CanvasScaler_get_referenceResolution_m1247_MethodInfo,
	&CanvasScaler_set_referenceResolution_m132_MethodInfo,
	&CanvasScaler_get_screenMatchMode_m1248_MethodInfo,
	&CanvasScaler_set_screenMatchMode_m133_MethodInfo,
	&CanvasScaler_get_matchWidthOrHeight_m1249_MethodInfo,
	&CanvasScaler_set_matchWidthOrHeight_m134_MethodInfo,
	&CanvasScaler_get_physicalUnit_m1250_MethodInfo,
	&CanvasScaler_set_physicalUnit_m1251_MethodInfo,
	&CanvasScaler_get_fallbackScreenDPI_m1252_MethodInfo,
	&CanvasScaler_set_fallbackScreenDPI_m1253_MethodInfo,
	&CanvasScaler_get_defaultSpriteDPI_m1254_MethodInfo,
	&CanvasScaler_set_defaultSpriteDPI_m1255_MethodInfo,
	&CanvasScaler_get_dynamicPixelsPerUnit_m1256_MethodInfo,
	&CanvasScaler_set_dynamicPixelsPerUnit_m1257_MethodInfo,
	&CanvasScaler_OnEnable_m1258_MethodInfo,
	&CanvasScaler_OnDisable_m1259_MethodInfo,
	&CanvasScaler_Update_m1260_MethodInfo,
	&CanvasScaler_Handle_m1261_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1262_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1263_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1264_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1265_MethodInfo,
	&CanvasScaler_SetScaleFactor_m1266_MethodInfo,
	&CanvasScaler_SetReferencePixelsPerUnit_m1267_MethodInfo,
	NULL
};
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1243_MethodInfo;
extern const MethodInfo CanvasScaler_set_uiScaleMode_m130_MethodInfo;
static const PropertyInfo CanvasScaler_t50____uiScaleMode_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "uiScaleMode"/* name */
	, &CanvasScaler_get_uiScaleMode_m1243_MethodInfo/* get */
	, &CanvasScaler_set_uiScaleMode_m130_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1244_MethodInfo;
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m135_MethodInfo;
static const PropertyInfo CanvasScaler_t50____referencePixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &CanvasScaler_get_referencePixelsPerUnit_m1244_MethodInfo/* get */
	, &CanvasScaler_set_referencePixelsPerUnit_m135_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_scaleFactor_m1245_MethodInfo;
extern const MethodInfo CanvasScaler_set_scaleFactor_m1246_MethodInfo;
static const PropertyInfo CanvasScaler_t50____scaleFactor_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &CanvasScaler_get_scaleFactor_m1245_MethodInfo/* get */
	, &CanvasScaler_set_scaleFactor_m1246_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referenceResolution_m1247_MethodInfo;
extern const MethodInfo CanvasScaler_set_referenceResolution_m132_MethodInfo;
static const PropertyInfo CanvasScaler_t50____referenceResolution_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "referenceResolution"/* name */
	, &CanvasScaler_get_referenceResolution_m1247_MethodInfo/* get */
	, &CanvasScaler_set_referenceResolution_m132_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1248_MethodInfo;
extern const MethodInfo CanvasScaler_set_screenMatchMode_m133_MethodInfo;
static const PropertyInfo CanvasScaler_t50____screenMatchMode_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "screenMatchMode"/* name */
	, &CanvasScaler_get_screenMatchMode_m1248_MethodInfo/* get */
	, &CanvasScaler_set_screenMatchMode_m133_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1249_MethodInfo;
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m134_MethodInfo;
static const PropertyInfo CanvasScaler_t50____matchWidthOrHeight_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "matchWidthOrHeight"/* name */
	, &CanvasScaler_get_matchWidthOrHeight_m1249_MethodInfo/* get */
	, &CanvasScaler_set_matchWidthOrHeight_m134_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_physicalUnit_m1250_MethodInfo;
extern const MethodInfo CanvasScaler_set_physicalUnit_m1251_MethodInfo;
static const PropertyInfo CanvasScaler_t50____physicalUnit_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "physicalUnit"/* name */
	, &CanvasScaler_get_physicalUnit_m1250_MethodInfo/* get */
	, &CanvasScaler_set_physicalUnit_m1251_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1252_MethodInfo;
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1253_MethodInfo;
static const PropertyInfo CanvasScaler_t50____fallbackScreenDPI_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "fallbackScreenDPI"/* name */
	, &CanvasScaler_get_fallbackScreenDPI_m1252_MethodInfo/* get */
	, &CanvasScaler_set_fallbackScreenDPI_m1253_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1254_MethodInfo;
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1255_MethodInfo;
static const PropertyInfo CanvasScaler_t50____defaultSpriteDPI_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "defaultSpriteDPI"/* name */
	, &CanvasScaler_get_defaultSpriteDPI_m1254_MethodInfo/* get */
	, &CanvasScaler_set_defaultSpriteDPI_m1255_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1256_MethodInfo;
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1257_MethodInfo;
static const PropertyInfo CanvasScaler_t50____dynamicPixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t50_il2cpp_TypeInfo/* parent */
	, "dynamicPixelsPerUnit"/* name */
	, &CanvasScaler_get_dynamicPixelsPerUnit_m1256_MethodInfo/* get */
	, &CanvasScaler_set_dynamicPixelsPerUnit_m1257_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasScaler_t50_PropertyInfos[] =
{
	&CanvasScaler_t50____uiScaleMode_PropertyInfo,
	&CanvasScaler_t50____referencePixelsPerUnit_PropertyInfo,
	&CanvasScaler_t50____scaleFactor_PropertyInfo,
	&CanvasScaler_t50____referenceResolution_PropertyInfo,
	&CanvasScaler_t50____screenMatchMode_PropertyInfo,
	&CanvasScaler_t50____matchWidthOrHeight_PropertyInfo,
	&CanvasScaler_t50____physicalUnit_PropertyInfo,
	&CanvasScaler_t50____fallbackScreenDPI_PropertyInfo,
	&CanvasScaler_t50____defaultSpriteDPI_PropertyInfo,
	&CanvasScaler_t50____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
static const Il2CppType* CanvasScaler_t50_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t271_0_0_0,
	&ScreenMatchMode_t272_0_0_0,
	&Unit_t273_0_0_0,
};
extern const MethodInfo CanvasScaler_OnEnable_m1258_MethodInfo;
extern const MethodInfo CanvasScaler_OnDisable_m1259_MethodInfo;
extern const MethodInfo CanvasScaler_Update_m1260_MethodInfo;
extern const MethodInfo CanvasScaler_Handle_m1261_MethodInfo;
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1262_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1263_MethodInfo;
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1264_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1265_MethodInfo;
static const Il2CppMethodReference CanvasScaler_t50_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&CanvasScaler_OnEnable_m1258_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&CanvasScaler_OnDisable_m1259_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&CanvasScaler_Update_m1260_MethodInfo,
	&CanvasScaler_Handle_m1261_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1262_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1263_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1264_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1265_MethodInfo,
};
static bool CanvasScaler_t50_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasScaler_t50_1_0_0;
struct CanvasScaler_t50;
const Il2CppTypeDefinitionMetadata CanvasScaler_t50_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t50_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, CanvasScaler_t50_VTable/* vtableMethods */
	, CanvasScaler_t50_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 538/* fieldStart */

};
TypeInfo CanvasScaler_t50_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasScaler_t50_MethodInfos/* methods */
	, CanvasScaler_t50_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasScaler_t50_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 270/* custom_attributes_cache */
	, &CanvasScaler_t50_0_0_0/* byval_arg */
	, &CanvasScaler_t50_1_0_0/* this_arg */
	, &CanvasScaler_t50_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t50)/* instance_size */
	, sizeof (CanvasScaler_t50)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t274_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static const MethodInfo* FitMode_t274_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FitMode_t274_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool FitMode_t274_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FitMode_t274_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FitMode_t274_0_0_0;
extern const Il2CppType FitMode_t274_1_0_0;
extern TypeInfo ContentSizeFitter_t275_il2cpp_TypeInfo;
extern const Il2CppType ContentSizeFitter_t275_0_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t274_DefinitionMetadata = 
{
	&ContentSizeFitter_t275_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t274_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, FitMode_t274_VTable/* vtableMethods */
	, FitMode_t274_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo FitMode_t274_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, FitMode_t274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t274_0_0_0/* byval_arg */
	, &FitMode_t274_1_0_0/* this_arg */
	, &FitMode_t274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t274)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t274)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern const MethodInfo ContentSizeFitter__ctor_m1268_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContentSizeFitter__ctor_m1268/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t274 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1269_MethodInfo = 
{
	"get_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_horizontalFit_m1269/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t274_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t274/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t274_0_0_0;
static const ParameterInfo ContentSizeFitter_t275_ContentSizeFitter_set_horizontalFit_m1270_ParameterInfos[] = 
{
	{"value", 0, 134218337, 0, &FitMode_t274_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1270_MethodInfo = 
{
	"set_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_horizontalFit_m1270/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ContentSizeFitter_t275_ContentSizeFitter_set_horizontalFit_m1270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t274 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1271_MethodInfo = 
{
	"get_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_verticalFit_m1271/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t274_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t274/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t274_0_0_0;
static const ParameterInfo ContentSizeFitter_t275_ContentSizeFitter_set_verticalFit_m1272_ParameterInfos[] = 
{
	{"value", 0, 134218338, 0, &FitMode_t274_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1272_MethodInfo = 
{
	"set_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_verticalFit_m1272/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ContentSizeFitter_t275_ContentSizeFitter_set_verticalFit_m1272_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1273_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&ContentSizeFitter_get_rectTransform_m1273/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern const MethodInfo ContentSizeFitter_OnEnable_m1274_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnEnable_m1274/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern const MethodInfo ContentSizeFitter_OnDisable_m1275_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnDisable_m1275/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1276_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&ContentSizeFitter_OnRectTransformDimensionsChange_m1276/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ContentSizeFitter_t275_ContentSizeFitter_HandleSelfFittingAlongAxis_m1277_ParameterInfos[] = 
{
	{"axis", 0, 134218339, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern const MethodInfo ContentSizeFitter_HandleSelfFittingAlongAxis_m1277_MethodInfo = 
{
	"HandleSelfFittingAlongAxis"/* name */
	, (methodPointerType)&ContentSizeFitter_HandleSelfFittingAlongAxis_m1277/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ContentSizeFitter_t275_ContentSizeFitter_HandleSelfFittingAlongAxis_m1277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1278_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutHorizontal_m1278/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1279_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutVertical_m1279/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern const MethodInfo ContentSizeFitter_SetDirty_m1280_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&ContentSizeFitter_SetDirty_m1280/* method */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContentSizeFitter_t275_MethodInfos[] =
{
	&ContentSizeFitter__ctor_m1268_MethodInfo,
	&ContentSizeFitter_get_horizontalFit_m1269_MethodInfo,
	&ContentSizeFitter_set_horizontalFit_m1270_MethodInfo,
	&ContentSizeFitter_get_verticalFit_m1271_MethodInfo,
	&ContentSizeFitter_set_verticalFit_m1272_MethodInfo,
	&ContentSizeFitter_get_rectTransform_m1273_MethodInfo,
	&ContentSizeFitter_OnEnable_m1274_MethodInfo,
	&ContentSizeFitter_OnDisable_m1275_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1276_MethodInfo,
	&ContentSizeFitter_HandleSelfFittingAlongAxis_m1277_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1278_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1279_MethodInfo,
	&ContentSizeFitter_SetDirty_m1280_MethodInfo,
	NULL
};
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1269_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1270_MethodInfo;
static const PropertyInfo ContentSizeFitter_t275____horizontalFit_PropertyInfo = 
{
	&ContentSizeFitter_t275_il2cpp_TypeInfo/* parent */
	, "horizontalFit"/* name */
	, &ContentSizeFitter_get_horizontalFit_m1269_MethodInfo/* get */
	, &ContentSizeFitter_set_horizontalFit_m1270_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1271_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1272_MethodInfo;
static const PropertyInfo ContentSizeFitter_t275____verticalFit_PropertyInfo = 
{
	&ContentSizeFitter_t275_il2cpp_TypeInfo/* parent */
	, "verticalFit"/* name */
	, &ContentSizeFitter_get_verticalFit_m1271_MethodInfo/* get */
	, &ContentSizeFitter_set_verticalFit_m1272_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1273_MethodInfo;
static const PropertyInfo ContentSizeFitter_t275____rectTransform_PropertyInfo = 
{
	&ContentSizeFitter_t275_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &ContentSizeFitter_get_rectTransform_m1273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContentSizeFitter_t275_PropertyInfos[] =
{
	&ContentSizeFitter_t275____horizontalFit_PropertyInfo,
	&ContentSizeFitter_t275____verticalFit_PropertyInfo,
	&ContentSizeFitter_t275____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* ContentSizeFitter_t275_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t274_0_0_0,
};
extern const MethodInfo ContentSizeFitter_OnEnable_m1274_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnDisable_m1275_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1276_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1278_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1279_MethodInfo;
static const Il2CppMethodReference ContentSizeFitter_t275_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&ContentSizeFitter_OnEnable_m1274_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&ContentSizeFitter_OnDisable_m1275_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1276_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1278_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1279_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1278_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1279_MethodInfo,
};
static bool ContentSizeFitter_t275_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContentSizeFitter_t275_InterfacesTypeInfos[] = 
{
	&ILayoutController_t392_0_0_0,
	&ILayoutSelfController_t393_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t275_InterfacesOffsets[] = 
{
	{ &ILayoutController_t392_0_0_0, 16},
	{ &ILayoutSelfController_t393_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentSizeFitter_t275_1_0_0;
struct ContentSizeFitter_t275;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t275_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t275_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t275_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, ContentSizeFitter_t275_VTable/* vtableMethods */
	, ContentSizeFitter_t275_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 556/* fieldStart */

};
TypeInfo ContentSizeFitter_t275_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ContentSizeFitter_t275_MethodInfos/* methods */
	, ContentSizeFitter_t275_PropertyInfos/* properties */
	, NULL/* events */
	, &ContentSizeFitter_t275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 281/* custom_attributes_cache */
	, &ContentSizeFitter_t275_0_0_0/* byval_arg */
	, &ContentSizeFitter_t275_1_0_0/* this_arg */
	, &ContentSizeFitter_t275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t275)/* instance_size */
	, sizeof (ContentSizeFitter_t275)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t276_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static const MethodInfo* Corner_t276_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Corner_t276_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Corner_t276_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Corner_t276_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Corner_t276_0_0_0;
extern const Il2CppType Corner_t276_1_0_0;
extern TypeInfo GridLayoutGroup_t279_il2cpp_TypeInfo;
extern const Il2CppType GridLayoutGroup_t279_0_0_0;
const Il2CppTypeDefinitionMetadata Corner_t276_DefinitionMetadata = 
{
	&GridLayoutGroup_t279_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t276_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Corner_t276_VTable/* vtableMethods */
	, Corner_t276_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 560/* fieldStart */

};
TypeInfo Corner_t276_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, Corner_t276_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t276_0_0_0/* byval_arg */
	, &Corner_t276_1_0_0/* this_arg */
	, &Corner_t276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t276)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t276)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t277_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t277_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t277_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Axis_t277_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t277_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t277_0_0_0;
extern const Il2CppType Axis_t277_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t277_DefinitionMetadata = 
{
	&GridLayoutGroup_t279_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t277_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Axis_t277_VTable/* vtableMethods */
	, Axis_t277_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 565/* fieldStart */

};
TypeInfo Axis_t277_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t277_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t277_0_0_0/* byval_arg */
	, &Axis_t277_1_0_0/* this_arg */
	, &Axis_t277_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t277)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t277)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t278_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static const MethodInfo* Constraint_t278_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Constraint_t278_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Constraint_t278_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Constraint_t278_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Constraint_t278_0_0_0;
extern const Il2CppType Constraint_t278_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t278_DefinitionMetadata = 
{
	&GridLayoutGroup_t279_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t278_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Constraint_t278_VTable/* vtableMethods */
	, Constraint_t278_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 568/* fieldStart */

};
TypeInfo Constraint_t278_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, Constraint_t278_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t278_0_0_0/* byval_arg */
	, &Constraint_t278_1_0_0/* this_arg */
	, &Constraint_t278_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t278)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t278)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern const MethodInfo GridLayoutGroup__ctor_m1281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GridLayoutGroup__ctor_m1281/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Corner_t276 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern const MethodInfo GridLayoutGroup_get_startCorner_m1282_MethodInfo = 
{
	"get_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startCorner_m1282/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Corner_t276_0_0_0/* return_type */
	, RuntimeInvoker_Corner_t276/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Corner_t276_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_startCorner_m1283_ParameterInfos[] = 
{
	{"value", 0, 134218340, 0, &Corner_t276_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo GridLayoutGroup_set_startCorner_m1283_MethodInfo = 
{
	"set_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startCorner_m1283/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_startCorner_m1283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t277 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern const MethodInfo GridLayoutGroup_get_startAxis_m1284_MethodInfo = 
{
	"get_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startAxis_m1284/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t277_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t277/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Axis_t277_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_startAxis_m1285_ParameterInfos[] = 
{
	{"value", 0, 134218341, 0, &Axis_t277_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo GridLayoutGroup_set_startAxis_m1285_MethodInfo = 
{
	"set_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startAxis_m1285/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_startAxis_m1285_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern const MethodInfo GridLayoutGroup_get_cellSize_m1286_MethodInfo = 
{
	"get_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_get_cellSize_m1286/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_cellSize_m1287_ParameterInfos[] = 
{
	{"value", 0, 134218342, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_cellSize_m1287_MethodInfo = 
{
	"set_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_set_cellSize_m1287/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_cellSize_m1287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern const MethodInfo GridLayoutGroup_get_spacing_m1288_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_get_spacing_m1288/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_spacing_m1289_ParameterInfos[] = 
{
	{"value", 0, 134218343, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_spacing_m1289_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_set_spacing_m1289/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_spacing_m1289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Constraint_t278 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern const MethodInfo GridLayoutGroup_get_constraint_m1290_MethodInfo = 
{
	"get_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraint_m1290/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Constraint_t278_0_0_0/* return_type */
	, RuntimeInvoker_Constraint_t278/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Constraint_t278_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_constraint_m1291_ParameterInfos[] = 
{
	{"value", 0, 134218344, 0, &Constraint_t278_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo GridLayoutGroup_set_constraint_m1291_MethodInfo = 
{
	"set_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraint_m1291/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_constraint_m1291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1292_MethodInfo = 
{
	"get_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraintCount_m1292/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_set_constraintCount_m1293_ParameterInfos[] = 
{
	{"value", 0, 134218345, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1293_MethodInfo = 
{
	"set_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraintCount_m1293/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_set_constraintCount_m1293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1294_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputHorizontal_m1294/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1295_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputVertical_m1295/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1296_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutHorizontal_m1296/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1297_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutVertical_m1297/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo GridLayoutGroup_t279_GridLayoutGroup_SetCellsAlongAxis_m1298_ParameterInfos[] = 
{
	{"axis", 0, 134218346, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern const MethodInfo GridLayoutGroup_SetCellsAlongAxis_m1298_MethodInfo = 
{
	"SetCellsAlongAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_SetCellsAlongAxis_m1298/* method */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GridLayoutGroup_t279_GridLayoutGroup_SetCellsAlongAxis_m1298_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GridLayoutGroup_t279_MethodInfos[] =
{
	&GridLayoutGroup__ctor_m1281_MethodInfo,
	&GridLayoutGroup_get_startCorner_m1282_MethodInfo,
	&GridLayoutGroup_set_startCorner_m1283_MethodInfo,
	&GridLayoutGroup_get_startAxis_m1284_MethodInfo,
	&GridLayoutGroup_set_startAxis_m1285_MethodInfo,
	&GridLayoutGroup_get_cellSize_m1286_MethodInfo,
	&GridLayoutGroup_set_cellSize_m1287_MethodInfo,
	&GridLayoutGroup_get_spacing_m1288_MethodInfo,
	&GridLayoutGroup_set_spacing_m1289_MethodInfo,
	&GridLayoutGroup_get_constraint_m1290_MethodInfo,
	&GridLayoutGroup_set_constraint_m1291_MethodInfo,
	&GridLayoutGroup_get_constraintCount_m1292_MethodInfo,
	&GridLayoutGroup_set_constraintCount_m1293_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1294_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1295_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1296_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1297_MethodInfo,
	&GridLayoutGroup_SetCellsAlongAxis_m1298_MethodInfo,
	NULL
};
extern const MethodInfo GridLayoutGroup_get_startCorner_m1282_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startCorner_m1283_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____startCorner_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "startCorner"/* name */
	, &GridLayoutGroup_get_startCorner_m1282_MethodInfo/* get */
	, &GridLayoutGroup_set_startCorner_m1283_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_startAxis_m1284_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startAxis_m1285_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____startAxis_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "startAxis"/* name */
	, &GridLayoutGroup_get_startAxis_m1284_MethodInfo/* get */
	, &GridLayoutGroup_set_startAxis_m1285_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_cellSize_m1286_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_cellSize_m1287_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____cellSize_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "cellSize"/* name */
	, &GridLayoutGroup_get_cellSize_m1286_MethodInfo/* get */
	, &GridLayoutGroup_set_cellSize_m1287_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_spacing_m1288_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_spacing_m1289_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____spacing_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &GridLayoutGroup_get_spacing_m1288_MethodInfo/* get */
	, &GridLayoutGroup_set_spacing_m1289_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraint_m1290_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraint_m1291_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____constraint_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "constraint"/* name */
	, &GridLayoutGroup_get_constraint_m1290_MethodInfo/* get */
	, &GridLayoutGroup_set_constraint_m1291_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1292_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1293_MethodInfo;
static const PropertyInfo GridLayoutGroup_t279____constraintCount_PropertyInfo = 
{
	&GridLayoutGroup_t279_il2cpp_TypeInfo/* parent */
	, "constraintCount"/* name */
	, &GridLayoutGroup_get_constraintCount_m1292_MethodInfo/* get */
	, &GridLayoutGroup_set_constraintCount_m1293_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GridLayoutGroup_t279_PropertyInfos[] =
{
	&GridLayoutGroup_t279____startCorner_PropertyInfo,
	&GridLayoutGroup_t279____startAxis_PropertyInfo,
	&GridLayoutGroup_t279____cellSize_PropertyInfo,
	&GridLayoutGroup_t279____spacing_PropertyInfo,
	&GridLayoutGroup_t279____constraint_PropertyInfo,
	&GridLayoutGroup_t279____constraintCount_PropertyInfo,
	NULL
};
static const Il2CppType* GridLayoutGroup_t279_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t276_0_0_0,
	&Axis_t277_0_0_0,
	&Constraint_t278_0_0_0,
};
extern const MethodInfo LayoutGroup_OnEnable_m1352_MethodInfo;
extern const MethodInfo LayoutGroup_OnDisable_m1353_MethodInfo;
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo;
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1294_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1295_MethodInfo;
extern const MethodInfo LayoutGroup_get_minWidth_m1345_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredWidth_m1346_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1347_MethodInfo;
extern const MethodInfo LayoutGroup_get_minHeight_m1348_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredHeight_m1349_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1350_MethodInfo;
extern const MethodInfo LayoutGroup_get_layoutPriority_m1351_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1296_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1297_MethodInfo;
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo;
static const Il2CppMethodReference GridLayoutGroup_t279_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1294_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1295_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1296_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1297_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1294_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1295_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1296_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1297_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
};
static bool GridLayoutGroup_t279_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutGroup_t390_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t279_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutController_t392_0_0_0, 25},
	{ &ILayoutGroup_t390_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GridLayoutGroup_t279_1_0_0;
extern const Il2CppType LayoutGroup_t280_0_0_0;
struct GridLayoutGroup_t279;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t279_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t279_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t280_0_0_0/* parent */
	, GridLayoutGroup_t279_VTable/* vtableMethods */
	, GridLayoutGroup_t279_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 572/* fieldStart */

};
TypeInfo GridLayoutGroup_t279_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, GridLayoutGroup_t279_MethodInfos/* methods */
	, GridLayoutGroup_t279_PropertyInfos/* properties */
	, NULL/* events */
	, &GridLayoutGroup_t279_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 284/* custom_attributes_cache */
	, &GridLayoutGroup_t279_0_0_0/* byval_arg */
	, &GridLayoutGroup_t279_1_0_0/* this_arg */
	, &GridLayoutGroup_t279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t279)/* instance_size */
	, sizeof (GridLayoutGroup_t279)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t281_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern const MethodInfo HorizontalLayoutGroup__ctor_m1299_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalLayoutGroup__ctor_m1299/* method */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300/* method */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301/* method */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1302_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutHorizontal_m1302/* method */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1303_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutVertical_m1303/* method */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalLayoutGroup_t281_MethodInfos[] =
{
	&HorizontalLayoutGroup__ctor_m1299_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1302_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1303_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1302_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1303_MethodInfo;
static const Il2CppMethodReference HorizontalLayoutGroup_t281_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1302_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1303_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1300_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1301_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1302_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1303_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
};
static bool HorizontalLayoutGroup_t281_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t281_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutController_t392_0_0_0, 25},
	{ &ILayoutGroup_t390_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalLayoutGroup_t281_0_0_0;
extern const Il2CppType HorizontalLayoutGroup_t281_1_0_0;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t282_0_0_0;
struct HorizontalLayoutGroup_t281;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t281_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t281_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t282_0_0_0/* parent */
	, HorizontalLayoutGroup_t281_VTable/* vtableMethods */
	, HorizontalLayoutGroup_t281_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HorizontalLayoutGroup_t281_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalLayoutGroup_t281_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HorizontalLayoutGroup_t281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 291/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t281_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t281_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t281)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t281)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern const MethodInfo HorizontalOrVerticalLayoutGroup__ctor_m1304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup__ctor_m1304/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1305_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_spacing_m1305/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_spacing_m1306_ParameterInfos[] = 
{
	{"value", 0, 134218347, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1306_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_spacing_m1306/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_spacing_m1306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1307_MethodInfo = 
{
	"get_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1307/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_ParameterInfos[] = 
{
	{"value", 0, 134218348, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_MethodInfo = 
{
	"set_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1309_MethodInfo = 
{
	"get_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1309/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_ParameterInfos[] = 
{
	{"value", 0, 134218349, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_MethodInfo = 
{
	"set_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1311_ParameterInfos[] = 
{
	{"axis", 0, 134218350, 0, &Int32_t54_0_0_0},
	{"isVertical", 1, 134218351, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1311_MethodInfo = 
{
	"CalcAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1311/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1312_ParameterInfos[] = 
{
	{"axis", 0, 134218352, 0, &Int32_t54_0_0_0},
	{"isVertical", 1, 134218353, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1312_MethodInfo = 
{
	"SetChildrenAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1312/* method */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t282_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1312_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalOrVerticalLayoutGroup_t282_MethodInfos[] =
{
	&HorizontalOrVerticalLayoutGroup__ctor_m1304_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_spacing_m1305_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_spacing_m1306_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1307_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1309_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1311_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1312_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1305_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1306_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t282____spacing_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_spacing_m1305_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_spacing_m1306_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1307_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t282____childForceExpandWidth_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* parent */
	, "childForceExpandWidth"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1307_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1308_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1309_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t282____childForceExpandHeight_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* parent */
	, "childForceExpandHeight"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1309_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1310_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HorizontalOrVerticalLayoutGroup_t282_PropertyInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t282____spacing_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t282____childForceExpandWidth_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t282____childForceExpandHeight_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo;
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2066_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2067_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2068_MethodInfo;
static const Il2CppMethodReference HorizontalOrVerticalLayoutGroup_t282_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2066_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2067_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2068_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
};
static bool HorizontalOrVerticalLayoutGroup_t282_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t282_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutController_t392_0_0_0, 25},
	{ &ILayoutGroup_t390_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t282_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t282;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t282_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t282_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t280_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t282_VTable/* vtableMethods */
	, HorizontalOrVerticalLayoutGroup_t282_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 578/* fieldStart */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalOrVerticalLayoutGroup_t282_MethodInfos/* methods */
	, HorizontalOrVerticalLayoutGroup_t282_PropertyInfos/* properties */
	, NULL/* events */
	, &HorizontalOrVerticalLayoutGroup_t282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t282_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t282_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t282)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t282)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t333_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo ILayoutElement_CalculateLayoutInputHorizontal_m2054_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo ILayoutElement_CalculateLayoutInputVertical_m2055_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minWidth()
extern const MethodInfo ILayoutElement_get_minWidth_m2056_MethodInfo = 
{
	"get_minWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth()
extern const MethodInfo ILayoutElement_get_preferredWidth_m2057_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth()
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2058_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minHeight()
extern const MethodInfo ILayoutElement_get_minHeight_m2059_MethodInfo = 
{
	"get_minHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight()
extern const MethodInfo ILayoutElement_get_preferredHeight_m2060_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight()
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2061_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority()
extern const MethodInfo ILayoutElement_get_layoutPriority_m2062_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, NULL/* method */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutElement_t333_MethodInfos[] =
{
	&ILayoutElement_CalculateLayoutInputHorizontal_m2054_MethodInfo,
	&ILayoutElement_CalculateLayoutInputVertical_m2055_MethodInfo,
	&ILayoutElement_get_minWidth_m2056_MethodInfo,
	&ILayoutElement_get_preferredWidth_m2057_MethodInfo,
	&ILayoutElement_get_flexibleWidth_m2058_MethodInfo,
	&ILayoutElement_get_minHeight_m2059_MethodInfo,
	&ILayoutElement_get_preferredHeight_m2060_MethodInfo,
	&ILayoutElement_get_flexibleHeight_m2061_MethodInfo,
	&ILayoutElement_get_layoutPriority_m2062_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutElement_get_minWidth_m2056_MethodInfo;
static const PropertyInfo ILayoutElement_t333____minWidth_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &ILayoutElement_get_minWidth_m2056_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredWidth_m2057_MethodInfo;
static const PropertyInfo ILayoutElement_t333____preferredWidth_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &ILayoutElement_get_preferredWidth_m2057_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2058_MethodInfo;
static const PropertyInfo ILayoutElement_t333____flexibleWidth_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &ILayoutElement_get_flexibleWidth_m2058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_minHeight_m2059_MethodInfo;
static const PropertyInfo ILayoutElement_t333____minHeight_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &ILayoutElement_get_minHeight_m2059_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredHeight_m2060_MethodInfo;
static const PropertyInfo ILayoutElement_t333____preferredHeight_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &ILayoutElement_get_preferredHeight_m2060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2061_MethodInfo;
static const PropertyInfo ILayoutElement_t333____flexibleHeight_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &ILayoutElement_get_flexibleHeight_m2061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_layoutPriority_m2062_MethodInfo;
static const PropertyInfo ILayoutElement_t333____layoutPriority_PropertyInfo = 
{
	&ILayoutElement_t333_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &ILayoutElement_get_layoutPriority_m2062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutElement_t333_PropertyInfos[] =
{
	&ILayoutElement_t333____minWidth_PropertyInfo,
	&ILayoutElement_t333____preferredWidth_PropertyInfo,
	&ILayoutElement_t333____flexibleWidth_PropertyInfo,
	&ILayoutElement_t333____minHeight_PropertyInfo,
	&ILayoutElement_t333____preferredHeight_PropertyInfo,
	&ILayoutElement_t333____flexibleHeight_PropertyInfo,
	&ILayoutElement_t333____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutElement_t333_1_0_0;
struct ILayoutElement_t333;
const Il2CppTypeDefinitionMetadata ILayoutElement_t333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutElement_t333_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutElement_t333_MethodInfos/* methods */
	, ILayoutElement_t333_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutElement_t333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t333_0_0_0/* byval_arg */
	, &ILayoutElement_t333_1_0_0/* this_arg */
	, &ILayoutElement_t333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t392_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal()
extern const MethodInfo ILayoutController_SetLayoutHorizontal_m2063_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &ILayoutController_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical()
extern const MethodInfo ILayoutController_SetLayoutVertical_m2064_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &ILayoutController_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutController_t392_MethodInfos[] =
{
	&ILayoutController_SetLayoutHorizontal_m2063_MethodInfo,
	&ILayoutController_SetLayoutVertical_m2064_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutController_t392_1_0_0;
struct ILayoutController_t392;
const Il2CppTypeDefinitionMetadata ILayoutController_t392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutController_t392_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutController_t392_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutController_t392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t392_0_0_0/* byval_arg */
	, &ILayoutController_t392_1_0_0/* this_arg */
	, &ILayoutController_t392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t390_il2cpp_TypeInfo;
static const MethodInfo* ILayoutGroup_t390_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutGroup_t390_InterfacesTypeInfos[] = 
{
	&ILayoutController_t392_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutGroup_t390_1_0_0;
struct ILayoutGroup_t390;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t390_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutGroup_t390_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutGroup_t390_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutGroup_t390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t390_0_0_0/* byval_arg */
	, &ILayoutGroup_t390_1_0_0/* this_arg */
	, &ILayoutGroup_t390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t393_il2cpp_TypeInfo;
static const MethodInfo* ILayoutSelfController_t393_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutSelfController_t393_InterfacesTypeInfos[] = 
{
	&ILayoutController_t392_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutSelfController_t393_1_0_0;
struct ILayoutSelfController_t393;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t393_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t393_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutSelfController_t393_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutSelfController_t393_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutSelfController_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t393_0_0_0/* byval_arg */
	, &ILayoutSelfController_t393_1_0_0/* this_arg */
	, &ILayoutSelfController_t393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t389_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout()
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2065_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, NULL/* method */
	, &ILayoutIgnorer_t389_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutIgnorer_t389_MethodInfos[] =
{
	&ILayoutIgnorer_get_ignoreLayout_m2065_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2065_MethodInfo;
static const PropertyInfo ILayoutIgnorer_t389____ignoreLayout_PropertyInfo = 
{
	&ILayoutIgnorer_t389_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &ILayoutIgnorer_get_ignoreLayout_m2065_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutIgnorer_t389_PropertyInfos[] =
{
	&ILayoutIgnorer_t389____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutIgnorer_t389_0_0_0;
extern const Il2CppType ILayoutIgnorer_t389_1_0_0;
struct ILayoutIgnorer_t389;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t389_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutIgnorer_t389_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutIgnorer_t389_MethodInfos/* methods */
	, ILayoutIgnorer_t389_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutIgnorer_t389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t389_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t389_1_0_0/* this_arg */
	, &ILayoutIgnorer_t389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t283_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern const MethodInfo LayoutElement__ctor_m1313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutElement__ctor_m1313/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern const MethodInfo LayoutElement_get_ignoreLayout_m1314_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_get_ignoreLayout_m1314/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_ignoreLayout_m1315_ParameterInfos[] = 
{
	{"value", 0, 134218354, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo LayoutElement_set_ignoreLayout_m1315_MethodInfo = 
{
	"set_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_set_ignoreLayout_m1315/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_ignoreLayout_m1315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1316_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputHorizontal_m1316/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1317_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputVertical_m1317/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern const MethodInfo LayoutElement_get_minWidth_m1318_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutElement_get_minWidth_m1318/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_minWidth_m1319_ParameterInfos[] = 
{
	{"value", 0, 134218355, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo LayoutElement_set_minWidth_m1319_MethodInfo = 
{
	"set_minWidth"/* name */
	, (methodPointerType)&LayoutElement_set_minWidth_m1319/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_minWidth_m1319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern const MethodInfo LayoutElement_get_minHeight_m1320_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutElement_get_minHeight_m1320/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_minHeight_m1321_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo LayoutElement_set_minHeight_m1321_MethodInfo = 
{
	"set_minHeight"/* name */
	, (methodPointerType)&LayoutElement_set_minHeight_m1321/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_minHeight_m1321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern const MethodInfo LayoutElement_get_preferredWidth_m1322_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_get_preferredWidth_m1322/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_preferredWidth_m1323_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo LayoutElement_set_preferredWidth_m1323_MethodInfo = 
{
	"set_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_set_preferredWidth_m1323/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_preferredWidth_m1323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern const MethodInfo LayoutElement_get_preferredHeight_m1324_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_get_preferredHeight_m1324/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_preferredHeight_m1325_ParameterInfos[] = 
{
	{"value", 0, 134218358, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo LayoutElement_set_preferredHeight_m1325_MethodInfo = 
{
	"set_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_set_preferredHeight_m1325/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_preferredHeight_m1325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern const MethodInfo LayoutElement_get_flexibleWidth_m1326_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleWidth_m1326/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_flexibleWidth_m1327_ParameterInfos[] = 
{
	{"value", 0, 134218359, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo LayoutElement_set_flexibleWidth_m1327_MethodInfo = 
{
	"set_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleWidth_m1327/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_flexibleWidth_m1327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 39/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern const MethodInfo LayoutElement_get_flexibleHeight_m1328_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleHeight_m1328/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 40/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutElement_t283_LayoutElement_set_flexibleHeight_m1329_ParameterInfos[] = 
{
	{"value", 0, 134218360, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo LayoutElement_set_flexibleHeight_m1329_MethodInfo = 
{
	"set_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleHeight_m1329/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, LayoutElement_t283_LayoutElement_set_flexibleHeight_m1329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 41/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern const MethodInfo LayoutElement_get_layoutPriority_m1330_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutElement_get_layoutPriority_m1330/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern const MethodInfo LayoutElement_OnEnable_m1331_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutElement_OnEnable_m1331/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1332_MethodInfo = 
{
	"OnTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnTransformParentChanged_m1332/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern const MethodInfo LayoutElement_OnDisable_m1333_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutElement_OnDisable_m1333/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1334_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutElement_OnDidApplyAnimationProperties_m1334/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1335_MethodInfo = 
{
	"OnBeforeTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnBeforeTransformParentChanged_m1335/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern const MethodInfo LayoutElement_SetDirty_m1336_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutElement_SetDirty_m1336/* method */
	, &LayoutElement_t283_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutElement_t283_MethodInfos[] =
{
	&LayoutElement__ctor_m1313_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1314_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1315_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1316_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1317_MethodInfo,
	&LayoutElement_get_minWidth_m1318_MethodInfo,
	&LayoutElement_set_minWidth_m1319_MethodInfo,
	&LayoutElement_get_minHeight_m1320_MethodInfo,
	&LayoutElement_set_minHeight_m1321_MethodInfo,
	&LayoutElement_get_preferredWidth_m1322_MethodInfo,
	&LayoutElement_set_preferredWidth_m1323_MethodInfo,
	&LayoutElement_get_preferredHeight_m1324_MethodInfo,
	&LayoutElement_set_preferredHeight_m1325_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1326_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1327_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1328_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1329_MethodInfo,
	&LayoutElement_get_layoutPriority_m1330_MethodInfo,
	&LayoutElement_OnEnable_m1331_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1332_MethodInfo,
	&LayoutElement_OnDisable_m1333_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1334_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1335_MethodInfo,
	&LayoutElement_SetDirty_m1336_MethodInfo,
	NULL
};
extern const MethodInfo LayoutElement_get_ignoreLayout_m1314_MethodInfo;
extern const MethodInfo LayoutElement_set_ignoreLayout_m1315_MethodInfo;
static const PropertyInfo LayoutElement_t283____ignoreLayout_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &LayoutElement_get_ignoreLayout_m1314_MethodInfo/* get */
	, &LayoutElement_set_ignoreLayout_m1315_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minWidth_m1318_MethodInfo;
extern const MethodInfo LayoutElement_set_minWidth_m1319_MethodInfo;
static const PropertyInfo LayoutElement_t283____minWidth_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutElement_get_minWidth_m1318_MethodInfo/* get */
	, &LayoutElement_set_minWidth_m1319_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minHeight_m1320_MethodInfo;
extern const MethodInfo LayoutElement_set_minHeight_m1321_MethodInfo;
static const PropertyInfo LayoutElement_t283____minHeight_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutElement_get_minHeight_m1320_MethodInfo/* get */
	, &LayoutElement_set_minHeight_m1321_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredWidth_m1322_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredWidth_m1323_MethodInfo;
static const PropertyInfo LayoutElement_t283____preferredWidth_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutElement_get_preferredWidth_m1322_MethodInfo/* get */
	, &LayoutElement_set_preferredWidth_m1323_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredHeight_m1324_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredHeight_m1325_MethodInfo;
static const PropertyInfo LayoutElement_t283____preferredHeight_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutElement_get_preferredHeight_m1324_MethodInfo/* get */
	, &LayoutElement_set_preferredHeight_m1325_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleWidth_m1326_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleWidth_m1327_MethodInfo;
static const PropertyInfo LayoutElement_t283____flexibleWidth_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutElement_get_flexibleWidth_m1326_MethodInfo/* get */
	, &LayoutElement_set_flexibleWidth_m1327_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleHeight_m1328_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleHeight_m1329_MethodInfo;
static const PropertyInfo LayoutElement_t283____flexibleHeight_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutElement_get_flexibleHeight_m1328_MethodInfo/* get */
	, &LayoutElement_set_flexibleHeight_m1329_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_layoutPriority_m1330_MethodInfo;
static const PropertyInfo LayoutElement_t283____layoutPriority_PropertyInfo = 
{
	&LayoutElement_t283_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutElement_get_layoutPriority_m1330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutElement_t283_PropertyInfos[] =
{
	&LayoutElement_t283____ignoreLayout_PropertyInfo,
	&LayoutElement_t283____minWidth_PropertyInfo,
	&LayoutElement_t283____minHeight_PropertyInfo,
	&LayoutElement_t283____preferredWidth_PropertyInfo,
	&LayoutElement_t283____preferredHeight_PropertyInfo,
	&LayoutElement_t283____flexibleWidth_PropertyInfo,
	&LayoutElement_t283____flexibleHeight_PropertyInfo,
	&LayoutElement_t283____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutElement_OnEnable_m1331_MethodInfo;
extern const MethodInfo LayoutElement_OnDisable_m1333_MethodInfo;
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1335_MethodInfo;
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1332_MethodInfo;
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1334_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1316_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1317_MethodInfo;
static const Il2CppMethodReference LayoutElement_t283_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutElement_OnEnable_m1331_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutElement_OnDisable_m1333_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1335_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1332_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1334_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1316_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1317_MethodInfo,
	&LayoutElement_get_minWidth_m1318_MethodInfo,
	&LayoutElement_get_preferredWidth_m1322_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1326_MethodInfo,
	&LayoutElement_get_minHeight_m1320_MethodInfo,
	&LayoutElement_get_preferredHeight_m1324_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1328_MethodInfo,
	&LayoutElement_get_layoutPriority_m1330_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1314_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1314_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1315_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1316_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1317_MethodInfo,
	&LayoutElement_get_minWidth_m1318_MethodInfo,
	&LayoutElement_set_minWidth_m1319_MethodInfo,
	&LayoutElement_get_minHeight_m1320_MethodInfo,
	&LayoutElement_set_minHeight_m1321_MethodInfo,
	&LayoutElement_get_preferredWidth_m1322_MethodInfo,
	&LayoutElement_set_preferredWidth_m1323_MethodInfo,
	&LayoutElement_get_preferredHeight_m1324_MethodInfo,
	&LayoutElement_set_preferredHeight_m1325_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1326_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1327_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1328_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1329_MethodInfo,
	&LayoutElement_get_layoutPriority_m1330_MethodInfo,
};
static bool LayoutElement_t283_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutElement_t283_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t333_0_0_0,
	&ILayoutIgnorer_t389_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t283_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutIgnorer_t389_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutElement_t283_0_0_0;
extern const Il2CppType LayoutElement_t283_1_0_0;
struct LayoutElement_t283;
const Il2CppTypeDefinitionMetadata LayoutElement_t283_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t283_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t283_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, LayoutElement_t283_VTable/* vtableMethods */
	, LayoutElement_t283_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 581/* fieldStart */

};
TypeInfo LayoutElement_t283_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutElement_t283_MethodInfos/* methods */
	, LayoutElement_t283_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutElement_t283_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 295/* custom_attributes_cache */
	, &LayoutElement_t283_0_0_0/* byval_arg */
	, &LayoutElement_t283_1_0_0/* this_arg */
	, &LayoutElement_t283_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t283)/* instance_size */
	, sizeof (LayoutElement_t283)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 43/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t280_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern const MethodInfo LayoutGroup__ctor_m1337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutGroup__ctor_m1337/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t284_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern const MethodInfo LayoutGroup_get_padding_m1338_MethodInfo = 
{
	"get_padding"/* name */
	, (methodPointerType)&LayoutGroup_get_padding_m1338/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t284_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_set_padding_m1339_ParameterInfos[] = 
{
	{"value", 0, 134218361, 0, &RectOffset_t284_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo LayoutGroup_set_padding_m1339_MethodInfo = 
{
	"set_padding"/* name */
	, (methodPointerType)&LayoutGroup_set_padding_m1339/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_set_padding_m1339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TextAnchor_t388 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern const MethodInfo LayoutGroup_get_childAlignment_m1340_MethodInfo = 
{
	"get_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_get_childAlignment_m1340/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t388_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t388/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t388_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_set_childAlignment_m1341_ParameterInfos[] = 
{
	{"value", 0, 134218362, 0, &TextAnchor_t388_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo LayoutGroup_set_childAlignment_m1341_MethodInfo = 
{
	"set_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_set_childAlignment_m1341/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_set_childAlignment_m1341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo LayoutGroup_get_rectTransform_m1342_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&LayoutGroup_get_rectTransform_m1342/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t285_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern const MethodInfo LayoutGroup_get_rectChildren_m1343_MethodInfo = 
{
	"get_rectChildren"/* name */
	, (methodPointerType)&LayoutGroup_get_rectChildren_m1343/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutGroup_CalculateLayoutInputHorizontal_m1344/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2066_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern const MethodInfo LayoutGroup_get_minWidth_m1345_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_minWidth_m1345/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern const MethodInfo LayoutGroup_get_preferredWidth_m1346_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredWidth_m1346/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1347_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleWidth_m1347/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern const MethodInfo LayoutGroup_get_minHeight_m1348_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_minHeight_m1348/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern const MethodInfo LayoutGroup_get_preferredHeight_m1349_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredHeight_m1349/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1350_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleHeight_m1350/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern const MethodInfo LayoutGroup_get_layoutPriority_m1351_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutGroup_get_layoutPriority_m1351/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2067_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2068_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern const MethodInfo LayoutGroup_OnEnable_m1352_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutGroup_OnEnable_m1352/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern const MethodInfo LayoutGroup_OnDisable_m1353_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutGroup_OnDisable_m1353/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutGroup_OnDidApplyAnimationProperties_m1354/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_GetTotalMinSize_m1355_ParameterInfos[] = 
{
	{"axis", 0, 134218363, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalMinSize_m1355_MethodInfo = 
{
	"GetTotalMinSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalMinSize_m1355/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Int32_t54/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_GetTotalMinSize_m1355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_GetTotalPreferredSize_m1356_ParameterInfos[] = 
{
	{"axis", 0, 134218364, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalPreferredSize_m1356_MethodInfo = 
{
	"GetTotalPreferredSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalPreferredSize_m1356/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Int32_t54/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_GetTotalPreferredSize_m1356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_GetTotalFlexibleSize_m1357_ParameterInfos[] = 
{
	{"axis", 0, 134218365, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalFlexibleSize_m1357_MethodInfo = 
{
	"GetTotalFlexibleSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalFlexibleSize_m1357/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Int32_t54/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_GetTotalFlexibleSize_m1357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_GetStartOffset_m1358_ParameterInfos[] = 
{
	{"axis", 0, 134218366, 0, &Int32_t54_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218367, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern const MethodInfo LayoutGroup_GetStartOffset_m1358_MethodInfo = 
{
	"GetStartOffset"/* name */
	, (methodPointerType)&LayoutGroup_GetStartOffset_m1358/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Int32_t54_Single_t85/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_GetStartOffset_m1358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_SetLayoutInputForAxis_m1359_ParameterInfos[] = 
{
	{"totalMin", 0, 134218368, 0, &Single_t85_0_0_0},
	{"totalPreferred", 1, 134218369, 0, &Single_t85_0_0_0},
	{"totalFlexible", 2, 134218370, 0, &Single_t85_0_0_0},
	{"axis", 3, 134218371, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern const MethodInfo LayoutGroup_SetLayoutInputForAxis_m1359_MethodInfo = 
{
	"SetLayoutInputForAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetLayoutInputForAxis_m1359/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85_Int32_t54/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_SetLayoutInputForAxis_m1359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_SetChildAlongAxis_m1360_ParameterInfos[] = 
{
	{"rect", 0, 134218372, 0, &RectTransform_t183_0_0_0},
	{"axis", 1, 134218373, 0, &Int32_t54_0_0_0},
	{"pos", 2, 134218374, 0, &Single_t85_0_0_0},
	{"size", 3, 134218375, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern const MethodInfo LayoutGroup_SetChildAlongAxis_m1360_MethodInfo = 
{
	"SetChildAlongAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetChildAlongAxis_m1360/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Single_t85_Single_t85/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_SetChildAlongAxis_m1360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1361_MethodInfo = 
{
	"get_isRootLayoutGroup"/* name */
	, (methodPointerType)&LayoutGroup_get_isRootLayoutGroup_m1361/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&LayoutGroup_OnRectTransformDimensionsChange_m1362/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo = 
{
	"OnTransformChildrenChanged"/* name */
	, (methodPointerType)&LayoutGroup_OnTransformChildrenChanged_m1363/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutGroup_SetProperty_m2069_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2069_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2069_gp_0_0_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2069_gp_0_0_0_0;
static const ParameterInfo LayoutGroup_t280_LayoutGroup_SetProperty_m2069_ParameterInfos[] = 
{
	{"currentValue", 0, 134218376, 0, &LayoutGroup_SetProperty_m2069_gp_0_1_0_0},
	{"newValue", 1, 134218377, 0, &LayoutGroup_SetProperty_m2069_gp_0_0_0_0},
};
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2069_Il2CppGenericContainer;
extern TypeInfo LayoutGroup_SetProperty_m2069_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter LayoutGroup_SetProperty_m2069_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &LayoutGroup_SetProperty_m2069_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* LayoutGroup_SetProperty_m2069_Il2CppGenericParametersArray[1] = 
{
	&LayoutGroup_SetProperty_m2069_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo LayoutGroup_SetProperty_m2069_MethodInfo;
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2069_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&LayoutGroup_SetProperty_m2069_MethodInfo, 1, 1, LayoutGroup_SetProperty_m2069_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition LayoutGroup_SetProperty_m2069_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LayoutGroup_SetProperty_m2069_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.UI.LayoutGroup::SetProperty(T&,T)
extern const MethodInfo LayoutGroup_SetProperty_m2069_MethodInfo = 
{
	"SetProperty"/* name */
	, NULL/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, LayoutGroup_t280_LayoutGroup_SetProperty_m2069_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1173/* token */
	, LayoutGroup_SetProperty_m2069_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LayoutGroup_SetProperty_m2069_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern const MethodInfo LayoutGroup_SetDirty_m1364_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutGroup_SetDirty_m1364/* method */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutGroup_t280_MethodInfos[] =
{
	&LayoutGroup__ctor_m1337_MethodInfo,
	&LayoutGroup_get_padding_m1338_MethodInfo,
	&LayoutGroup_set_padding_m1339_MethodInfo,
	&LayoutGroup_get_childAlignment_m1340_MethodInfo,
	&LayoutGroup_set_childAlignment_m1341_MethodInfo,
	&LayoutGroup_get_rectTransform_m1342_MethodInfo,
	&LayoutGroup_get_rectChildren_m1343_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2066_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2067_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2068_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&LayoutGroup_GetTotalMinSize_m1355_MethodInfo,
	&LayoutGroup_GetTotalPreferredSize_m1356_MethodInfo,
	&LayoutGroup_GetTotalFlexibleSize_m1357_MethodInfo,
	&LayoutGroup_GetStartOffset_m1358_MethodInfo,
	&LayoutGroup_SetLayoutInputForAxis_m1359_MethodInfo,
	&LayoutGroup_SetChildAlongAxis_m1360_MethodInfo,
	&LayoutGroup_get_isRootLayoutGroup_m1361_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
	&LayoutGroup_SetProperty_m2069_MethodInfo,
	&LayoutGroup_SetDirty_m1364_MethodInfo,
	NULL
};
extern const MethodInfo LayoutGroup_get_padding_m1338_MethodInfo;
extern const MethodInfo LayoutGroup_set_padding_m1339_MethodInfo;
static const PropertyInfo LayoutGroup_t280____padding_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "padding"/* name */
	, &LayoutGroup_get_padding_m1338_MethodInfo/* get */
	, &LayoutGroup_set_padding_m1339_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_childAlignment_m1340_MethodInfo;
extern const MethodInfo LayoutGroup_set_childAlignment_m1341_MethodInfo;
static const PropertyInfo LayoutGroup_t280____childAlignment_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "childAlignment"/* name */
	, &LayoutGroup_get_childAlignment_m1340_MethodInfo/* get */
	, &LayoutGroup_set_childAlignment_m1341_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectTransform_m1342_MethodInfo;
static const PropertyInfo LayoutGroup_t280____rectTransform_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &LayoutGroup_get_rectTransform_m1342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectChildren_m1343_MethodInfo;
static const PropertyInfo LayoutGroup_t280____rectChildren_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "rectChildren"/* name */
	, &LayoutGroup_get_rectChildren_m1343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____minWidth_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutGroup_get_minWidth_m1345_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____preferredWidth_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutGroup_get_preferredWidth_m1346_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____flexibleWidth_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutGroup_get_flexibleWidth_m1347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____minHeight_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutGroup_get_minHeight_m1348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____preferredHeight_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutGroup_get_preferredHeight_m1349_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____flexibleHeight_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutGroup_get_flexibleHeight_m1350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t280____layoutPriority_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutGroup_get_layoutPriority_m1351_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1361_MethodInfo;
static const PropertyInfo LayoutGroup_t280____isRootLayoutGroup_PropertyInfo = 
{
	&LayoutGroup_t280_il2cpp_TypeInfo/* parent */
	, "isRootLayoutGroup"/* name */
	, &LayoutGroup_get_isRootLayoutGroup_m1361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutGroup_t280_PropertyInfos[] =
{
	&LayoutGroup_t280____padding_PropertyInfo,
	&LayoutGroup_t280____childAlignment_PropertyInfo,
	&LayoutGroup_t280____rectTransform_PropertyInfo,
	&LayoutGroup_t280____rectChildren_PropertyInfo,
	&LayoutGroup_t280____minWidth_PropertyInfo,
	&LayoutGroup_t280____preferredWidth_PropertyInfo,
	&LayoutGroup_t280____flexibleWidth_PropertyInfo,
	&LayoutGroup_t280____minHeight_PropertyInfo,
	&LayoutGroup_t280____preferredHeight_PropertyInfo,
	&LayoutGroup_t280____flexibleHeight_PropertyInfo,
	&LayoutGroup_t280____layoutPriority_PropertyInfo,
	&LayoutGroup_t280____isRootLayoutGroup_PropertyInfo,
	NULL
};
static const Il2CppMethodReference LayoutGroup_t280_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2066_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2067_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2068_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1344_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
};
static bool LayoutGroup_t280_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutGroup_t280_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t333_0_0_0,
	&ILayoutController_t392_0_0_0,
	&ILayoutGroup_t390_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t280_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutController_t392_0_0_0, 25},
	{ &ILayoutGroup_t390_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutGroup_t280_1_0_0;
struct LayoutGroup_t280;
const Il2CppTypeDefinitionMetadata LayoutGroup_t280_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t280_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t280_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, LayoutGroup_t280_VTable/* vtableMethods */
	, LayoutGroup_t280_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 588/* fieldStart */

};
TypeInfo LayoutGroup_t280_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutGroup_t280_MethodInfos/* methods */
	, LayoutGroup_t280_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutGroup_t280_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 303/* custom_attributes_cache */
	, &LayoutGroup_t280_0_0_0/* byval_arg */
	, &LayoutGroup_t280_1_0_0/* this_arg */
	, &LayoutGroup_t280_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t280)/* instance_size */
	, sizeof (LayoutGroup_t280)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t288_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder__ctor_m1365_ParameterInfos[] = 
{
	{"controller", 0, 134218378, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder__ctor_m1365_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutRebuilder__ctor_m1365/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder__ctor_m1365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern const MethodInfo LayoutRebuilder__cctor_m1366_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LayoutRebuilder__cctor_m1366/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t170_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_ParameterInfos[] = 
{
	{"executing", 0, 134218379, 0, &CanvasUpdate_t170_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_ReapplyDrivenProperties_m1368_ParameterInfos[] = 
{
	{"driven", 0, 134218380, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ReapplyDrivenProperties_m1368_MethodInfo = 
{
	"ReapplyDrivenProperties"/* name */
	, (methodPointerType)&LayoutRebuilder_ReapplyDrivenProperties_m1368/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_ReapplyDrivenProperties_m1368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern const MethodInfo LayoutRebuilder_get_transform_m1369_MethodInfo = 
{
	"get_transform"/* name */
	, (methodPointerType)&LayoutRebuilder_get_transform_m1369/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1370_MethodInfo = 
{
	"IsDestroyed"/* name */
	, (methodPointerType)&LayoutRebuilder_IsDestroyed_m1370/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t332_0_0_0;
extern const Il2CppType List_1_t332_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_StripDisabledBehavioursFromList_m1371_ParameterInfos[] = 
{
	{"components", 0, 134218381, 0, &List_1_t332_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_StripDisabledBehavioursFromList_m1371_MethodInfo = 
{
	"StripDisabledBehavioursFromList"/* name */
	, (methodPointerType)&LayoutRebuilder_StripDisabledBehavioursFromList_m1371/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_StripDisabledBehavioursFromList_m1371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType UnityAction_1_t286_0_0_0;
extern const Il2CppType UnityAction_1_t286_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_PerformLayoutControl_m1372_ParameterInfos[] = 
{
	{"rect", 0, 134218382, 0, &RectTransform_t183_0_0_0},
	{"action", 1, 134218383, 0, &UnityAction_1_t286_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutControl_m1372_MethodInfo = 
{
	"PerformLayoutControl"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutControl_m1372/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_PerformLayoutControl_m1372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType UnityAction_1_t286_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_PerformLayoutCalculation_m1373_ParameterInfos[] = 
{
	{"rect", 0, 134218384, 0, &RectTransform_t183_0_0_0},
	{"action", 1, 134218385, 0, &UnityAction_1_t286_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutCalculation_m1373_MethodInfo = 
{
	"PerformLayoutCalculation"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutCalculation_m1373/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_PerformLayoutCalculation_m1373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_MarkLayoutForRebuild_m1374_ParameterInfos[] = 
{
	{"rect", 0, 134218386, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutForRebuild_m1374_MethodInfo = 
{
	"MarkLayoutForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutForRebuild_m1374/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_MarkLayoutForRebuild_m1374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_ValidLayoutGroup_m1375_ParameterInfos[] = 
{
	{"parent", 0, 134218387, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidLayoutGroup_m1375_MethodInfo = 
{
	"ValidLayoutGroup"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidLayoutGroup_m1375/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_ValidLayoutGroup_m1375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_ValidController_m1376_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218388, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidController_m1376_MethodInfo = 
{
	"ValidController"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidController_m1376/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_ValidController_m1376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_MarkLayoutRootForRebuild_m1377_ParameterInfos[] = 
{
	{"controller", 0, 134218389, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutRootForRebuild_m1377_MethodInfo = 
{
	"MarkLayoutRootForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutRootForRebuild_m1377/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_MarkLayoutRootForRebuild_m1377_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutRebuilder_t288_0_0_0;
extern const Il2CppType LayoutRebuilder_t288_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_Equals_m1378_ParameterInfos[] = 
{
	{"other", 0, 134218390, 0, &LayoutRebuilder_t288_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_LayoutRebuilder_t288 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
extern const MethodInfo LayoutRebuilder_Equals_m1378_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&LayoutRebuilder_Equals_m1378/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_LayoutRebuilder_t288/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_Equals_m1378_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern const MethodInfo LayoutRebuilder_GetHashCode_m1379_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&LayoutRebuilder_GetHashCode_m1379/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern const MethodInfo LayoutRebuilder_ToString_m1380_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&LayoutRebuilder_ToString_m1380/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t49_0_0_0;
extern const Il2CppType Component_t49_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__9_m1381_ParameterInfos[] = 
{
	{"e", 0, 134218391, 0, &Component_t49_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__9_m1381_MethodInfo = 
{
	"<Rebuild>m__9"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__9_m1381/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__9_m1381_ParameterInfos/* parameters */
	, 311/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t49_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__A_m1382_ParameterInfos[] = 
{
	{"e", 0, 134218392, 0, &Component_t49_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__A_m1382_MethodInfo = 
{
	"<Rebuild>m__A"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__A_m1382/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__A_m1382_ParameterInfos/* parameters */
	, 312/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t49_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__B_m1383_ParameterInfos[] = 
{
	{"e", 0, 134218393, 0, &Component_t49_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__B_m1383_MethodInfo = 
{
	"<Rebuild>m__B"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__B_m1383/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__B_m1383_ParameterInfos/* parameters */
	, 313/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t49_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__C_m1384_ParameterInfos[] = 
{
	{"e", 0, 134218394, 0, &Component_t49_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__C_m1384_MethodInfo = 
{
	"<Rebuild>m__C"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__C_m1384/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_U3CRebuildU3Em__C_m1384_ParameterInfos/* parameters */
	, 314/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t49_0_0_0;
static const ParameterInfo LayoutRebuilder_t288_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385_ParameterInfos[] = 
{
	{"e", 0, 134218395, 0, &Component_t49_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385_MethodInfo = 
{
	"<StripDisabledBehavioursFromList>m__D"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385/* method */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, LayoutRebuilder_t288_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385_ParameterInfos/* parameters */
	, 315/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutRebuilder_t288_MethodInfos[] =
{
	&LayoutRebuilder__ctor_m1365_MethodInfo,
	&LayoutRebuilder__cctor_m1366_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_MethodInfo,
	&LayoutRebuilder_ReapplyDrivenProperties_m1368_MethodInfo,
	&LayoutRebuilder_get_transform_m1369_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1370_MethodInfo,
	&LayoutRebuilder_StripDisabledBehavioursFromList_m1371_MethodInfo,
	&LayoutRebuilder_PerformLayoutControl_m1372_MethodInfo,
	&LayoutRebuilder_PerformLayoutCalculation_m1373_MethodInfo,
	&LayoutRebuilder_MarkLayoutForRebuild_m1374_MethodInfo,
	&LayoutRebuilder_ValidLayoutGroup_m1375_MethodInfo,
	&LayoutRebuilder_ValidController_m1376_MethodInfo,
	&LayoutRebuilder_MarkLayoutRootForRebuild_m1377_MethodInfo,
	&LayoutRebuilder_Equals_m1378_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1379_MethodInfo,
	&LayoutRebuilder_ToString_m1380_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__9_m1381_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__A_m1382_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__B_m1383_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__C_m1384_MethodInfo,
	&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1385_MethodInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_get_transform_m1369_MethodInfo;
static const PropertyInfo LayoutRebuilder_t288____transform_PropertyInfo = 
{
	&LayoutRebuilder_t288_il2cpp_TypeInfo/* parent */
	, "transform"/* name */
	, &LayoutRebuilder_get_transform_m1369_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutRebuilder_t288_PropertyInfos[] =
{
	&LayoutRebuilder_t288____transform_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_GetHashCode_m1379_MethodInfo;
extern const MethodInfo LayoutRebuilder_ToString_m1380_MethodInfo;
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_MethodInfo;
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1370_MethodInfo;
extern const MethodInfo LayoutRebuilder_Equals_m1378_MethodInfo;
static const Il2CppMethodReference LayoutRebuilder_t288_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1379_MethodInfo,
	&LayoutRebuilder_ToString_m1380_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1367_MethodInfo,
	&LayoutRebuilder_get_transform_m1369_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1370_MethodInfo,
	&LayoutRebuilder_Equals_m1378_MethodInfo,
};
static bool LayoutRebuilder_t288_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEquatable_1_t454_0_0_0;
static const Il2CppType* LayoutRebuilder_t288_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t325_0_0_0,
	&IEquatable_1_t454_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t288_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t325_0_0_0, 4},
	{ &IEquatable_1_t454_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutRebuilder_t288_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t288_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t288_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, LayoutRebuilder_t288_VTable/* vtableMethods */
	, LayoutRebuilder_t288_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 596/* fieldStart */

};
TypeInfo LayoutRebuilder_t288_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutRebuilder_t288_MethodInfos/* methods */
	, LayoutRebuilder_t288_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutRebuilder_t288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t288_0_0_0/* byval_arg */
	, &LayoutRebuilder_t288_1_0_0/* this_arg */
	, &LayoutRebuilder_t288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t288)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t288)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t288_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t290_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetMinSize_m1386_ParameterInfos[] = 
{
	{"rect", 0, 134218396, 0, &RectTransform_t183_0_0_0},
	{"axis", 1, 134218397, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetMinSize_m1386_MethodInfo = 
{
	"GetMinSize"/* name */
	, (methodPointerType)&LayoutUtility_GetMinSize_m1386/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t_Int32_t54/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetMinSize_m1386_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetPreferredSize_m1387_ParameterInfos[] = 
{
	{"rect", 0, 134218398, 0, &RectTransform_t183_0_0_0},
	{"axis", 1, 134218399, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetPreferredSize_m1387_MethodInfo = 
{
	"GetPreferredSize"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredSize_m1387/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t_Int32_t54/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetPreferredSize_m1387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetFlexibleSize_m1388_ParameterInfos[] = 
{
	{"rect", 0, 134218400, 0, &RectTransform_t183_0_0_0},
	{"axis", 1, 134218401, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetFlexibleSize_m1388_MethodInfo = 
{
	"GetFlexibleSize"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleSize_m1388/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t_Int32_t54/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetFlexibleSize_m1388_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetMinWidth_m1389_ParameterInfos[] = 
{
	{"rect", 0, 134218402, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinWidth_m1389_MethodInfo = 
{
	"GetMinWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetMinWidth_m1389/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetMinWidth_m1389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetPreferredWidth_m1390_ParameterInfos[] = 
{
	{"rect", 0, 134218403, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredWidth_m1390_MethodInfo = 
{
	"GetPreferredWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredWidth_m1390/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetPreferredWidth_m1390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetFlexibleWidth_m1391_ParameterInfos[] = 
{
	{"rect", 0, 134218404, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleWidth_m1391_MethodInfo = 
{
	"GetFlexibleWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleWidth_m1391/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetFlexibleWidth_m1391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetMinHeight_m1392_ParameterInfos[] = 
{
	{"rect", 0, 134218405, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinHeight_m1392_MethodInfo = 
{
	"GetMinHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetMinHeight_m1392/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetMinHeight_m1392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetPreferredHeight_m1393_ParameterInfos[] = 
{
	{"rect", 0, 134218406, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredHeight_m1393_MethodInfo = 
{
	"GetPreferredHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredHeight_m1393/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetPreferredHeight_m1393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetFlexibleHeight_m1394_ParameterInfos[] = 
{
	{"rect", 0, 134218407, 0, &RectTransform_t183_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleHeight_m1394_MethodInfo = 
{
	"GetFlexibleHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleHeight_m1394/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetFlexibleHeight_m1394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Func_2_t289_0_0_0;
extern const Il2CppType Func_2_t289_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetLayoutProperty_m1395_ParameterInfos[] = 
{
	{"rect", 0, 134218408, 0, &RectTransform_t183_0_0_0},
	{"property", 1, 134218409, 0, &Func_2_t289_0_0_0},
	{"defaultValue", 2, 134218410, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1395_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1395/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetLayoutProperty_m1395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t183_0_0_0;
extern const Il2CppType Func_2_t289_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType ILayoutElement_t333_1_0_2;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_GetLayoutProperty_m1396_ParameterInfos[] = 
{
	{"rect", 0, 134218411, 0, &RectTransform_t183_0_0_0},
	{"property", 1, 134218412, 0, &Func_2_t289_0_0_0},
	{"defaultValue", 2, 134218413, 0, &Single_t85_0_0_0},
	{"source", 3, 134218414, 0, &ILayoutElement_t333_1_0_2},
};
extern void* RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85_ILayoutElementU26_t455 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1396_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1396/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85_ILayoutElementU26_t455/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_GetLayoutProperty_m1396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetMinWidthU3Em__E_m1397_ParameterInfos[] = 
{
	{"e", 0, 134218415, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinWidthU3Em__E_m1397_MethodInfo = 
{
	"<GetMinWidth>m__E"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinWidthU3Em__E_m1397/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetMinWidthU3Em__E_m1397_ParameterInfos/* parameters */
	, 324/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398_ParameterInfos[] = 
{
	{"e", 0, 134218416, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398_MethodInfo = 
{
	"<GetPreferredWidth>m__F"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398_ParameterInfos/* parameters */
	, 325/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399_ParameterInfos[] = 
{
	{"e", 0, 134218417, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399_MethodInfo = 
{
	"<GetPreferredWidth>m__10"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399_ParameterInfos/* parameters */
	, 326/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400_ParameterInfos[] = 
{
	{"e", 0, 134218418, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400_MethodInfo = 
{
	"<GetFlexibleWidth>m__11"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400_ParameterInfos/* parameters */
	, 327/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetMinHeightU3Em__12_m1401_ParameterInfos[] = 
{
	{"e", 0, 134218419, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinHeightU3Em__12_m1401_MethodInfo = 
{
	"<GetMinHeight>m__12"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinHeightU3Em__12_m1401/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetMinHeightU3Em__12_m1401_ParameterInfos/* parameters */
	, 328/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402_ParameterInfos[] = 
{
	{"e", 0, 134218420, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402_MethodInfo = 
{
	"<GetPreferredHeight>m__13"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402_ParameterInfos/* parameters */
	, 329/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403_ParameterInfos[] = 
{
	{"e", 0, 134218421, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403_MethodInfo = 
{
	"<GetPreferredHeight>m__14"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403_ParameterInfos/* parameters */
	, 330/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t333_0_0_0;
static const ParameterInfo LayoutUtility_t290_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404_ParameterInfos[] = 
{
	{"e", 0, 134218422, 0, &ILayoutElement_t333_0_0_0},
};
extern void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404_MethodInfo = 
{
	"<GetFlexibleHeight>m__15"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404/* method */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85_Object_t/* invoker_method */
	, LayoutUtility_t290_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404_ParameterInfos/* parameters */
	, 331/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutUtility_t290_MethodInfos[] =
{
	&LayoutUtility_GetMinSize_m1386_MethodInfo,
	&LayoutUtility_GetPreferredSize_m1387_MethodInfo,
	&LayoutUtility_GetFlexibleSize_m1388_MethodInfo,
	&LayoutUtility_GetMinWidth_m1389_MethodInfo,
	&LayoutUtility_GetPreferredWidth_m1390_MethodInfo,
	&LayoutUtility_GetFlexibleWidth_m1391_MethodInfo,
	&LayoutUtility_GetMinHeight_m1392_MethodInfo,
	&LayoutUtility_GetPreferredHeight_m1393_MethodInfo,
	&LayoutUtility_GetFlexibleHeight_m1394_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1395_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1396_MethodInfo,
	&LayoutUtility_U3CGetMinWidthU3Em__E_m1397_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1398_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1399_MethodInfo,
	&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1400_MethodInfo,
	&LayoutUtility_U3CGetMinHeightU3Em__12_m1401_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1402_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1403_MethodInfo,
	&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1404_MethodInfo,
	NULL
};
static const Il2CppMethodReference LayoutUtility_t290_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool LayoutUtility_t290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutUtility_t290_0_0_0;
extern const Il2CppType LayoutUtility_t290_1_0_0;
struct LayoutUtility_t290;
const Il2CppTypeDefinitionMetadata LayoutUtility_t290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t290_VTable/* vtableMethods */
	, LayoutUtility_t290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 603/* fieldStart */

};
TypeInfo LayoutUtility_t290_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutUtility_t290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LayoutUtility_t290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t290_0_0_0/* byval_arg */
	, &LayoutUtility_t290_1_0_0/* this_arg */
	, &LayoutUtility_t290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t290)/* instance_size */
	, sizeof (LayoutUtility_t290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t290_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t291_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern const MethodInfo VerticalLayoutGroup__ctor_m1405_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VerticalLayoutGroup__ctor_m1405/* method */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406/* method */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1407_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputVertical_m1407/* method */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1408_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutHorizontal_m1408/* method */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1409_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutVertical_m1409/* method */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VerticalLayoutGroup_t291_MethodInfos[] =
{
	&VerticalLayoutGroup__ctor_m1405_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1407_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1408_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1409_MethodInfo,
	NULL
};
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1407_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1408_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1409_MethodInfo;
static const Il2CppMethodReference VerticalLayoutGroup_t291_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&LayoutGroup_OnEnable_m1352_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&LayoutGroup_OnDisable_m1353_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1362_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1354_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1407_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1408_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1409_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1406_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1407_MethodInfo,
	&LayoutGroup_get_minWidth_m1345_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1346_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1347_MethodInfo,
	&LayoutGroup_get_minHeight_m1348_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1349_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1350_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1351_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1408_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1409_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1363_MethodInfo,
};
static bool VerticalLayoutGroup_t291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t291_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t333_0_0_0, 16},
	{ &ILayoutController_t392_0_0_0, 25},
	{ &ILayoutGroup_t390_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType VerticalLayoutGroup_t291_0_0_0;
extern const Il2CppType VerticalLayoutGroup_t291_1_0_0;
struct VerticalLayoutGroup_t291;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t291_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t282_0_0_0/* parent */
	, VerticalLayoutGroup_t291_VTable/* vtableMethods */
	, VerticalLayoutGroup_t291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VerticalLayoutGroup_t291_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, VerticalLayoutGroup_t291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VerticalLayoutGroup_t291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 332/* custom_attributes_cache */
	, &VerticalLayoutGroup_t291_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t291_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t291)/* instance_size */
	, sizeof (VerticalLayoutGroup_t291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t356_il2cpp_TypeInfo;
extern const Il2CppType Material_t180_0_0_0;
static const ParameterInfo IMaterialModifier_t356_IMaterialModifier_GetModifiedMaterial_m2070_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218423, 0, &Material_t180_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.IMaterialModifier::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo IMaterialModifier_GetModifiedMaterial_m2070_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, NULL/* method */
	, &IMaterialModifier_t356_il2cpp_TypeInfo/* declaring_type */
	, &Material_t180_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IMaterialModifier_t356_IMaterialModifier_GetModifiedMaterial_m2070_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMaterialModifier_t356_MethodInfos[] =
{
	&IMaterialModifier_GetModifiedMaterial_m2070_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaterialModifier_t356_0_0_0;
extern const Il2CppType IMaterialModifier_t356_1_0_0;
struct IMaterialModifier_t356;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMaterialModifier_t356_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IMaterialModifier_t356_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMaterialModifier_t356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t356_0_0_0/* byval_arg */
	, &IMaterialModifier_t356_1_0_0/* this_arg */
	, &IMaterialModifier_t356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t292_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::.ctor()
extern const MethodInfo Mask__ctor_m1410_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mask__ctor_m1410/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern const MethodInfo Mask_get_graphic_m1411_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&Mask_get_graphic_m1411/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t188_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
extern const MethodInfo Mask_get_showMaskGraphic_m1412_MethodInfo = 
{
	"get_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_get_showMaskGraphic_m1412/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Mask_t292_Mask_set_showMaskGraphic_m1413_ParameterInfos[] = 
{
	{"value", 0, 134218424, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
extern const MethodInfo Mask_set_showMaskGraphic_m1413_MethodInfo = 
{
	"set_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_set_showMaskGraphic_m1413/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Mask_t292_Mask_set_showMaskGraphic_m1413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern const MethodInfo Mask_get_rectTransform_m1414_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&Mask_get_rectTransform_m1414/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t183_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
extern const MethodInfo Mask_MaskEnabled_m1415_MethodInfo = 
{
	"MaskEnabled"/* name */
	, (methodPointerType)&Mask_MaskEnabled_m1415/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1416_MethodInfo = 
{
	"OnSiblingGraphicEnabledDisabled"/* name */
	, (methodPointerType)&Mask_OnSiblingGraphicEnabledDisabled_m1416/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
extern const MethodInfo Mask_NotifyMaskStateChanged_m1417_MethodInfo = 
{
	"NotifyMaskStateChanged"/* name */
	, (methodPointerType)&Mask_NotifyMaskStateChanged_m1417/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
extern const MethodInfo Mask_ClearCachedMaterial_m1418_MethodInfo = 
{
	"ClearCachedMaterial"/* name */
	, (methodPointerType)&Mask_ClearCachedMaterial_m1418/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnEnable()
extern const MethodInfo Mask_OnEnable_m1419_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Mask_OnEnable_m1419/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnDisable()
extern const MethodInfo Mask_OnDisable_m1420_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Mask_OnDisable_m1420/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
extern const Il2CppType Camera_t156_0_0_0;
static const ParameterInfo Mask_t292_Mask_IsRaycastLocationValid_m1421_ParameterInfos[] = 
{
	{"sp", 0, 134218425, 0, &Vector2_t53_0_0_0},
	{"eventCamera", 1, 134218426, 0, &Camera_t156_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo Mask_IsRaycastLocationValid_m1421_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&Mask_IsRaycastLocationValid_m1421/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Vector2_t53_Object_t/* invoker_method */
	, Mask_t292_Mask_IsRaycastLocationValid_m1421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t180_0_0_0;
static const ParameterInfo Mask_t292_Mask_GetModifiedMaterial_m1422_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218427, 0, &Material_t180_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo Mask_GetModifiedMaterial_m1422_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, (methodPointerType)&Mask_GetModifiedMaterial_m1422/* method */
	, &Mask_t292_il2cpp_TypeInfo/* declaring_type */
	, &Material_t180_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mask_t292_Mask_GetModifiedMaterial_m1422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mask_t292_MethodInfos[] =
{
	&Mask__ctor_m1410_MethodInfo,
	&Mask_get_graphic_m1411_MethodInfo,
	&Mask_get_showMaskGraphic_m1412_MethodInfo,
	&Mask_set_showMaskGraphic_m1413_MethodInfo,
	&Mask_get_rectTransform_m1414_MethodInfo,
	&Mask_MaskEnabled_m1415_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1416_MethodInfo,
	&Mask_NotifyMaskStateChanged_m1417_MethodInfo,
	&Mask_ClearCachedMaterial_m1418_MethodInfo,
	&Mask_OnEnable_m1419_MethodInfo,
	&Mask_OnDisable_m1420_MethodInfo,
	&Mask_IsRaycastLocationValid_m1421_MethodInfo,
	&Mask_GetModifiedMaterial_m1422_MethodInfo,
	NULL
};
extern const MethodInfo Mask_get_graphic_m1411_MethodInfo;
static const PropertyInfo Mask_t292____graphic_PropertyInfo = 
{
	&Mask_t292_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &Mask_get_graphic_m1411_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_showMaskGraphic_m1412_MethodInfo;
extern const MethodInfo Mask_set_showMaskGraphic_m1413_MethodInfo;
static const PropertyInfo Mask_t292____showMaskGraphic_PropertyInfo = 
{
	&Mask_t292_il2cpp_TypeInfo/* parent */
	, "showMaskGraphic"/* name */
	, &Mask_get_showMaskGraphic_m1412_MethodInfo/* get */
	, &Mask_set_showMaskGraphic_m1413_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_rectTransform_m1414_MethodInfo;
static const PropertyInfo Mask_t292____rectTransform_PropertyInfo = 
{
	&Mask_t292_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &Mask_get_rectTransform_m1414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mask_t292_PropertyInfos[] =
{
	&Mask_t292____graphic_PropertyInfo,
	&Mask_t292____showMaskGraphic_PropertyInfo,
	&Mask_t292____rectTransform_PropertyInfo,
	NULL
};
extern const MethodInfo Mask_OnEnable_m1419_MethodInfo;
extern const MethodInfo Mask_OnDisable_m1420_MethodInfo;
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1416_MethodInfo;
extern const MethodInfo Mask_MaskEnabled_m1415_MethodInfo;
extern const MethodInfo Mask_IsRaycastLocationValid_m1421_MethodInfo;
extern const MethodInfo Mask_GetModifiedMaterial_m1422_MethodInfo;
static const Il2CppMethodReference Mask_t292_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&Mask_OnEnable_m1419_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&Mask_OnDisable_m1420_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1416_MethodInfo,
	&Mask_MaskEnabled_m1415_MethodInfo,
	&Mask_IsRaycastLocationValid_m1421_MethodInfo,
	&Mask_GetModifiedMaterial_m1422_MethodInfo,
	&Mask_MaskEnabled_m1415_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1416_MethodInfo,
	&Mask_IsRaycastLocationValid_m1421_MethodInfo,
	&Mask_GetModifiedMaterial_m1422_MethodInfo,
};
static bool Mask_t292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IGraphicEnabledDisabled_t357_0_0_0;
extern const Il2CppType IMask_t380_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t359_0_0_0;
static const Il2CppType* Mask_t292_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t357_0_0_0,
	&IMask_t380_0_0_0,
	&ICanvasRaycastFilter_t359_0_0_0,
	&IMaterialModifier_t356_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t292_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t357_0_0_0, 16},
	{ &IMask_t380_0_0_0, 17},
	{ &ICanvasRaycastFilter_t359_0_0_0, 18},
	{ &IMaterialModifier_t356_0_0_0, 19},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mask_t292_0_0_0;
extern const Il2CppType Mask_t292_1_0_0;
struct Mask_t292;
const Il2CppTypeDefinitionMetadata Mask_t292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t292_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t292_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, Mask_t292_VTable/* vtableMethods */
	, Mask_t292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 611/* fieldStart */

};
TypeInfo Mask_t292_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Mask_t292_MethodInfos/* methods */
	, Mask_t292_PropertyInfos/* properties */
	, NULL/* events */
	, &Mask_t292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 333/* custom_attributes_cache */
	, &Mask_t292_0_0_0/* byval_arg */
	, &Mask_t292_1_0_0/* this_arg */
	, &Mask_t292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t292)/* instance_size */
	, sizeof (Mask_t292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t422_il2cpp_TypeInfo;
extern const Il2CppGenericContainer IndexedSet_1_t422_Il2CppGenericContainer;
extern TypeInfo IndexedSet_1_t422_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter IndexedSet_1_t422_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &IndexedSet_1_t422_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* IndexedSet_1_t422_Il2CppGenericParametersArray[1] = 
{
	&IndexedSet_1_t422_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer IndexedSet_1_t422_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&IndexedSet_1_t422_il2cpp_TypeInfo, 1, 0, IndexedSet_1_t422_Il2CppGenericParametersArray };
// System.Void UnityEngine.UI.Collections.IndexedSet`1::.ctor()
extern const MethodInfo IndexedSet_1__ctor_m2072_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t28_0_0_0;
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2073_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_Add_m2074_ParameterInfos[] = 
{
	{"item", 0, 134218428, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Add(T)
extern const MethodInfo IndexedSet_1_Add_m2074_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_Add_m2074_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_Remove_m2075_ParameterInfos[] = 
{
	{"item", 0, 134218429, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Remove(T)
extern const MethodInfo IndexedSet_1_Remove_m2075_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_Remove_m2075_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t457_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1::GetEnumerator()
extern const MethodInfo IndexedSet_1_GetEnumerator_m2076_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t457_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Clear()
extern const MethodInfo IndexedSet_1_Clear_m2077_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_Contains_m2078_ParameterInfos[] = 
{
	{"item", 0, 134218430, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Contains(T)
extern const MethodInfo IndexedSet_1_Contains_m2078_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_Contains_m2078_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t458_0_0_0;
extern const Il2CppType TU5BU5D_t458_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_CopyTo_m2079_ParameterInfos[] = 
{
	{"array", 0, 134218431, 0, &TU5BU5D_t458_0_0_0},
	{"arrayIndex", 1, 134218432, 0, &Int32_t54_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo IndexedSet_1_CopyTo_m2079_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_CopyTo_m2079_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::get_Count()
extern const MethodInfo IndexedSet_1_get_Count_m2080_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::get_IsReadOnly()
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2081_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_IndexOf_m2082_ParameterInfos[] = 
{
	{"item", 0, 134218433, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::IndexOf(T)
extern const MethodInfo IndexedSet_1_IndexOf_m2082_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_IndexOf_m2082_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_Insert_m2083_ParameterInfos[] = 
{
	{"index", 0, 134218434, 0, &Int32_t54_0_0_0},
	{"item", 1, 134218435, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Insert(System.Int32,T)
extern const MethodInfo IndexedSet_1_Insert_m2083_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_Insert_m2083_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_RemoveAt_m2084_ParameterInfos[] = 
{
	{"index", 0, 134218436, 0, &Int32_t54_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAt(System.Int32)
extern const MethodInfo IndexedSet_1_RemoveAt_m2084_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_RemoveAt_m2084_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_get_Item_m2085_ParameterInfos[] = 
{
	{"index", 0, 134218437, 0, &Int32_t54_0_0_0},
};
// T UnityEngine.UI.Collections.IndexedSet`1::get_Item(System.Int32)
extern const MethodInfo IndexedSet_1_get_Item_m2085_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &IndexedSet_1_t422_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_get_Item_m2085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IndexedSet_1_t422_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_set_Item_m2086_ParameterInfos[] = 
{
	{"index", 0, 134218438, 0, &Int32_t54_0_0_0},
	{"value", 1, 134218439, 0, &IndexedSet_1_t422_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::set_Item(System.Int32,T)
extern const MethodInfo IndexedSet_1_set_Item_m2086_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_set_Item_m2086_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t459_0_0_0;
extern const Il2CppType Predicate_1_t459_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_RemoveAll_m2087_ParameterInfos[] = 
{
	{"match", 0, 134218440, 0, &Predicate_1_t459_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAll(System.Predicate`1<T>)
extern const MethodInfo IndexedSet_1_RemoveAll_m2087_MethodInfo = 
{
	"RemoveAll"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_RemoveAll_m2087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t460_0_0_0;
extern const Il2CppType Comparison_1_t460_0_0_0;
static const ParameterInfo IndexedSet_1_t422_IndexedSet_1_Sort_m2088_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218441, 0, &Comparison_1_t460_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Sort(System.Comparison`1<T>)
extern const MethodInfo IndexedSet_1_Sort_m2088_MethodInfo = 
{
	"Sort"/* name */
	, NULL/* method */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t422_IndexedSet_1_Sort_m2088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexedSet_1_t422_MethodInfos[] =
{
	&IndexedSet_1__ctor_m2072_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2073_MethodInfo,
	&IndexedSet_1_Add_m2074_MethodInfo,
	&IndexedSet_1_Remove_m2075_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2076_MethodInfo,
	&IndexedSet_1_Clear_m2077_MethodInfo,
	&IndexedSet_1_Contains_m2078_MethodInfo,
	&IndexedSet_1_CopyTo_m2079_MethodInfo,
	&IndexedSet_1_get_Count_m2080_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2081_MethodInfo,
	&IndexedSet_1_IndexOf_m2082_MethodInfo,
	&IndexedSet_1_Insert_m2083_MethodInfo,
	&IndexedSet_1_RemoveAt_m2084_MethodInfo,
	&IndexedSet_1_get_Item_m2085_MethodInfo,
	&IndexedSet_1_set_Item_m2086_MethodInfo,
	&IndexedSet_1_RemoveAll_m2087_MethodInfo,
	&IndexedSet_1_Sort_m2088_MethodInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_get_Count_m2080_MethodInfo;
static const PropertyInfo IndexedSet_1_t422____Count_PropertyInfo = 
{
	&IndexedSet_1_t422_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IndexedSet_1_get_Count_m2080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2081_MethodInfo;
static const PropertyInfo IndexedSet_1_t422____IsReadOnly_PropertyInfo = 
{
	&IndexedSet_1_t422_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &IndexedSet_1_get_IsReadOnly_m2081_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_Item_m2085_MethodInfo;
extern const MethodInfo IndexedSet_1_set_Item_m2086_MethodInfo;
static const PropertyInfo IndexedSet_1_t422____Item_PropertyInfo = 
{
	&IndexedSet_1_t422_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IndexedSet_1_get_Item_m2085_MethodInfo/* get */
	, &IndexedSet_1_set_Item_m2086_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IndexedSet_1_t422_PropertyInfos[] =
{
	&IndexedSet_1_t422____Count_PropertyInfo,
	&IndexedSet_1_t422____IsReadOnly_PropertyInfo,
	&IndexedSet_1_t422____Item_PropertyInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_IndexOf_m2082_MethodInfo;
extern const MethodInfo IndexedSet_1_Insert_m2083_MethodInfo;
extern const MethodInfo IndexedSet_1_RemoveAt_m2084_MethodInfo;
extern const MethodInfo IndexedSet_1_Add_m2074_MethodInfo;
extern const MethodInfo IndexedSet_1_Clear_m2077_MethodInfo;
extern const MethodInfo IndexedSet_1_Contains_m2078_MethodInfo;
extern const MethodInfo IndexedSet_1_CopyTo_m2079_MethodInfo;
extern const MethodInfo IndexedSet_1_Remove_m2075_MethodInfo;
extern const MethodInfo IndexedSet_1_GetEnumerator_m2076_MethodInfo;
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2073_MethodInfo;
static const Il2CppMethodReference IndexedSet_1_t422_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&IndexedSet_1_IndexOf_m2082_MethodInfo,
	&IndexedSet_1_Insert_m2083_MethodInfo,
	&IndexedSet_1_RemoveAt_m2084_MethodInfo,
	&IndexedSet_1_get_Item_m2085_MethodInfo,
	&IndexedSet_1_set_Item_m2086_MethodInfo,
	&IndexedSet_1_get_Count_m2080_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2081_MethodInfo,
	&IndexedSet_1_Add_m2074_MethodInfo,
	&IndexedSet_1_Clear_m2077_MethodInfo,
	&IndexedSet_1_Contains_m2078_MethodInfo,
	&IndexedSet_1_CopyTo_m2079_MethodInfo,
	&IndexedSet_1_Remove_m2075_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2076_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2073_MethodInfo,
};
static bool IndexedSet_1_t422_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IList_1_t461_0_0_0;
extern const Il2CppType ICollection_1_t462_0_0_0;
extern const Il2CppType IEnumerable_1_t463_0_0_0;
extern const Il2CppType IEnumerable_t464_0_0_0;
static const Il2CppType* IndexedSet_1_t422_InterfacesTypeInfos[] = 
{
	&IList_1_t461_0_0_0,
	&ICollection_1_t462_0_0_0,
	&IEnumerable_1_t463_0_0_0,
	&IEnumerable_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t422_InterfacesOffsets[] = 
{
	{ &IList_1_t461_0_0_0, 4},
	{ &ICollection_1_t462_0_0_0, 9},
	{ &IEnumerable_1_t463_0_0_0, 16},
	{ &IEnumerable_t464_0_0_0, 17},
};
extern const Il2CppType List_1_t465_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m2133_GenericMethod;
extern const Il2CppType Dictionary_2_t466_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m2134_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_GetEnumerator_m2135_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m2136_GenericMethod;
extern const Il2CppGenericMethod List_1_Add_m2137_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Count_m2138_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Add_m2139_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m2140_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_RemoveAt_m2141_GenericMethod;
extern const Il2CppGenericMethod List_1_Clear_m2142_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Clear_m2143_GenericMethod;
extern const Il2CppGenericMethod List_1_CopyTo_m2144_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Item_m2145_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Remove_m2146_GenericMethod;
extern const Il2CppGenericMethod List_1_RemoveAt_m2147_GenericMethod;
extern const Il2CppGenericMethod List_1_set_Item_m2148_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m2149_GenericMethod;
extern const Il2CppGenericMethod Predicate_1_Invoke_m2150_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_Remove_m2151_GenericMethod;
extern const Il2CppGenericMethod List_1_Sort_m2152_GenericMethod;
static Il2CppRGCTXDefinition IndexedSet_1_t422_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t465_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m2133_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t466_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m2134_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_GetEnumerator_m2135_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m2136_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m2137_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Count_m2138_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m2139_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m2140_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_RemoveAt_m2141_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Clear_m2142_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Clear_m2143_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_CopyTo_m2144_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Item_m2145_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Remove_m2146_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_RemoveAt_m2147_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_set_Item_m2148_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m2149_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Predicate_1_Invoke_m2150_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_Remove_m2151_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Sort_m2152_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IndexedSet_1_t422_0_0_0;
extern const Il2CppType IndexedSet_1_t422_1_0_0;
struct IndexedSet_1_t422;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t422_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t422_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t422_VTable/* vtableMethods */
	, IndexedSet_1_t422_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, IndexedSet_1_t422_RGCTXData/* rgctxDefinition */
	, 615/* fieldStart */

};
TypeInfo IndexedSet_1_t422_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, IndexedSet_1_t422_MethodInfos/* methods */
	, IndexedSet_1_t422_PropertyInfos/* properties */
	, NULL/* events */
	, &IndexedSet_1_t422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 335/* custom_attributes_cache */
	, &IndexedSet_1_t422_0_0_0/* byval_arg */
	, &IndexedSet_1_t422_1_0_0/* this_arg */
	, &IndexedSet_1_t422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &IndexedSet_1_t422_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t295_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::.cctor()
extern const MethodInfo CanvasListPool__cctor_m1423_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CanvasListPool__cctor_m1423/* method */
	, &CanvasListPool_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t334_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
extern const MethodInfo CanvasListPool_Get_m1424_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&CanvasListPool_Get_m1424/* method */
	, &CanvasListPool_t295_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t334_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t334_0_0_0;
static const ParameterInfo CanvasListPool_t295_CanvasListPool_Release_m1425_ParameterInfos[] = 
{
	{"toRelease", 0, 134218442, 0, &List_1_t334_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_Release_m1425_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&CanvasListPool_Release_m1425/* method */
	, &CanvasListPool_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CanvasListPool_t295_CanvasListPool_Release_m1425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t334_0_0_0;
static const ParameterInfo CanvasListPool_t295_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426_ParameterInfos[] = 
{
	{"l", 0, 134218443, 0, &List_1_t334_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426_MethodInfo = 
{
	"<s_CanvasListPool>m__16"/* name */
	, (methodPointerType)&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426/* method */
	, &CanvasListPool_t295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CanvasListPool_t295_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426_ParameterInfos/* parameters */
	, 337/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasListPool_t295_MethodInfos[] =
{
	&CanvasListPool__cctor_m1423_MethodInfo,
	&CanvasListPool_Get_m1424_MethodInfo,
	&CanvasListPool_Release_m1425_MethodInfo,
	&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1426_MethodInfo,
	NULL
};
static const Il2CppMethodReference CanvasListPool_t295_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CanvasListPool_t295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasListPool_t295_0_0_0;
extern const Il2CppType CanvasListPool_t295_1_0_0;
struct CanvasListPool_t295;
const Il2CppTypeDefinitionMetadata CanvasListPool_t295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t295_VTable/* vtableMethods */
	, CanvasListPool_t295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */

};
TypeInfo CanvasListPool_t295_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasListPool_t295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CanvasListPool_t295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t295_0_0_0/* byval_arg */
	, &CanvasListPool_t295_1_0_0/* this_arg */
	, &CanvasListPool_t295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t295)/* instance_size */
	, sizeof (CanvasListPool_t295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t295_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t298_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::.cctor()
extern const MethodInfo ComponentListPool__cctor_m1427_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ComponentListPool__cctor_m1427/* method */
	, &ComponentListPool_t298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
extern const MethodInfo ComponentListPool_Get_m1428_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ComponentListPool_Get_m1428/* method */
	, &ComponentListPool_t298_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t332_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t332_0_0_0;
static const ParameterInfo ComponentListPool_t298_ComponentListPool_Release_m1429_ParameterInfos[] = 
{
	{"toRelease", 0, 134218444, 0, &List_1_t332_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_Release_m1429_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ComponentListPool_Release_m1429/* method */
	, &ComponentListPool_t298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ComponentListPool_t298_ComponentListPool_Release_m1429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t332_0_0_0;
static const ParameterInfo ComponentListPool_t298_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430_ParameterInfos[] = 
{
	{"l", 0, 134218445, 0, &List_1_t332_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430_MethodInfo = 
{
	"<s_ComponentListPool>m__17"/* name */
	, (methodPointerType)&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430/* method */
	, &ComponentListPool_t298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ComponentListPool_t298_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430_ParameterInfos/* parameters */
	, 339/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentListPool_t298_MethodInfos[] =
{
	&ComponentListPool__cctor_m1427_MethodInfo,
	&ComponentListPool_Get_m1428_MethodInfo,
	&ComponentListPool_Release_m1429_MethodInfo,
	&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1430_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentListPool_t298_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ComponentListPool_t298_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ComponentListPool_t298_0_0_0;
extern const Il2CppType ComponentListPool_t298_1_0_0;
struct ComponentListPool_t298;
const Il2CppTypeDefinitionMetadata ComponentListPool_t298_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t298_VTable/* vtableMethods */
	, ComponentListPool_t298_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 619/* fieldStart */

};
TypeInfo ComponentListPool_t298_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ComponentListPool_t298_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentListPool_t298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t298_0_0_0/* byval_arg */
	, &ComponentListPool_t298_1_0_0/* this_arg */
	, &ComponentListPool_t298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t298)/* instance_size */
	, sizeof (ComponentListPool_t298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t298_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t424_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ObjectPool_1_t424_Il2CppGenericContainer;
extern TypeInfo ObjectPool_1_t424_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ObjectPool_1_t424_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ObjectPool_1_t424_Il2CppGenericContainer, NULL, "T", 0, 16 };
static const Il2CppGenericParameter* ObjectPool_1_t424_Il2CppGenericParametersArray[1] = 
{
	&ObjectPool_1_t424_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ObjectPool_1_t424_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ObjectPool_1_t424_il2cpp_TypeInfo, 1, 0, ObjectPool_1_t424_Il2CppGenericParametersArray };
extern const Il2CppType UnityAction_1_t468_0_0_0;
extern const Il2CppType UnityAction_1_t468_0_0_0;
extern const Il2CppType UnityAction_1_t468_0_0_0;
static const ParameterInfo ObjectPool_1_t424_ObjectPool_1__ctor_m2089_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218446, 0, &UnityAction_1_t468_0_0_0},
	{"actionOnRelease", 1, 134218447, 0, &UnityAction_1_t468_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern const MethodInfo ObjectPool_1__ctor_m2089_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t424_ObjectPool_1__ctor_m2089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countAll()
extern const MethodInfo ObjectPool_1_get_countAll_m2090_MethodInfo = 
{
	"get_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 341/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ObjectPool_1_t424_ObjectPool_1_set_countAll_m2091_ParameterInfos[] = 
{
	{"value", 0, 134218448, 0, &Int32_t54_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::set_countAll(System.Int32)
extern const MethodInfo ObjectPool_1_set_countAll_m2091_MethodInfo = 
{
	"set_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t424_ObjectPool_1_set_countAll_m2091_ParameterInfos/* parameters */
	, 342/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countActive()
extern const MethodInfo ObjectPool_1_get_countActive_m2092_MethodInfo = 
{
	"get_countActive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countInactive()
extern const MethodInfo ObjectPool_1_get_countInactive_m2093_MethodInfo = 
{
	"get_countInactive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t424_gp_0_0_0_0;
// T UnityEngine.UI.ObjectPool`1::Get()
extern const MethodInfo ObjectPool_1_Get_m2094_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &ObjectPool_1_t424_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t424_gp_0_0_0_0;
static const ParameterInfo ObjectPool_1_t424_ObjectPool_1_Release_m2095_ParameterInfos[] = 
{
	{"element", 0, 134218449, 0, &ObjectPool_1_t424_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::Release(T)
extern const MethodInfo ObjectPool_1_Release_m2095_MethodInfo = 
{
	"Release"/* name */
	, NULL/* method */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t424_ObjectPool_1_Release_m2095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectPool_1_t424_MethodInfos[] =
{
	&ObjectPool_1__ctor_m2089_MethodInfo,
	&ObjectPool_1_get_countAll_m2090_MethodInfo,
	&ObjectPool_1_set_countAll_m2091_MethodInfo,
	&ObjectPool_1_get_countActive_m2092_MethodInfo,
	&ObjectPool_1_get_countInactive_m2093_MethodInfo,
	&ObjectPool_1_Get_m2094_MethodInfo,
	&ObjectPool_1_Release_m2095_MethodInfo,
	NULL
};
extern const MethodInfo ObjectPool_1_get_countAll_m2090_MethodInfo;
extern const MethodInfo ObjectPool_1_set_countAll_m2091_MethodInfo;
static const PropertyInfo ObjectPool_1_t424____countAll_PropertyInfo = 
{
	&ObjectPool_1_t424_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m2090_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m2091_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countActive_m2092_MethodInfo;
static const PropertyInfo ObjectPool_1_t424____countActive_PropertyInfo = 
{
	&ObjectPool_1_t424_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m2092_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countInactive_m2093_MethodInfo;
static const PropertyInfo ObjectPool_1_t424____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t424_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m2093_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectPool_1_t424_PropertyInfos[] =
{
	&ObjectPool_1_t424____countAll_PropertyInfo,
	&ObjectPool_1_t424____countActive_PropertyInfo,
	&ObjectPool_1_t424____countInactive_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ObjectPool_1_t424_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ObjectPool_1_t424_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType Stack_1_t469_0_0_0;
extern const Il2CppGenericMethod Stack_1__ctor_m2153_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countAll_m2154_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countInactive_m2155_GenericMethod;
extern const Il2CppGenericMethod Stack_1_get_Count_m2156_GenericMethod;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t467_m2157_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_set_countAll_m2158_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Pop_m2159_GenericMethod;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m2160_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Peek_m2161_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Push_m2162_GenericMethod;
static Il2CppRGCTXDefinition ObjectPool_1_t424_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Stack_1_t469_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1__ctor_m2153_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countAll_m2154_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countInactive_m2155_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_get_Count_m2156_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ObjectPool_1_t424_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t467_m2157_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_set_countAll_m2158_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Pop_m2159_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m2160_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Peek_m2161_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Push_m2162_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ObjectPool_1_t424_0_0_0;
extern const Il2CppType ObjectPool_1_t424_1_0_0;
struct ObjectPool_1_t424;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t424_VTable/* vtableMethods */
	, ObjectPool_1_t424_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ObjectPool_1_t424_RGCTXData/* rgctxDefinition */
	, 621/* fieldStart */

};
TypeInfo ObjectPool_1_t424_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t424_MethodInfos/* methods */
	, ObjectPool_1_t424_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectPool_1_t424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t424_0_0_0/* byval_arg */
	, &ObjectPool_1_t424_1_0_0/* this_arg */
	, &ObjectPool_1_t424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ObjectPool_1_t424_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t299_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern const MethodInfo BaseVertexEffect__ctor_m1431_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseVertexEffect__ctor_m1431/* method */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern const MethodInfo BaseVertexEffect_get_graphic_m1432_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&BaseVertexEffect_get_graphic_m1432/* method */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t188_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern const MethodInfo BaseVertexEffect_OnEnable_m1433_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnEnable_m1433/* method */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern const MethodInfo BaseVertexEffect_OnDisable_m1434_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnDisable_m1434/* method */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo BaseVertexEffect_t299_BaseVertexEffect_ModifyVertices_m2096_ParameterInfos[] = 
{
	{"verts", 0, 134218450, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2096_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, BaseVertexEffect_t299_BaseVertexEffect_ModifyVertices_m2096_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseVertexEffect_t299_MethodInfos[] =
{
	&BaseVertexEffect__ctor_m1431_MethodInfo,
	&BaseVertexEffect_get_graphic_m1432_MethodInfo,
	&BaseVertexEffect_OnEnable_m1433_MethodInfo,
	&BaseVertexEffect_OnDisable_m1434_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2096_MethodInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_get_graphic_m1432_MethodInfo;
static const PropertyInfo BaseVertexEffect_t299____graphic_PropertyInfo = 
{
	&BaseVertexEffect_t299_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &BaseVertexEffect_get_graphic_m1432_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BaseVertexEffect_t299_PropertyInfos[] =
{
	&BaseVertexEffect_t299____graphic_PropertyInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_OnEnable_m1433_MethodInfo;
extern const MethodInfo BaseVertexEffect_OnDisable_m1434_MethodInfo;
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2096_MethodInfo;
static const Il2CppMethodReference BaseVertexEffect_t299_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&BaseVertexEffect_OnEnable_m1433_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&BaseVertexEffect_OnDisable_m1434_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2096_MethodInfo,
	NULL,
};
static bool BaseVertexEffect_t299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVertexModifier_t358_0_0_0;
static const Il2CppType* BaseVertexEffect_t299_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t358_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t299_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t358_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseVertexEffect_t299_0_0_0;
extern const Il2CppType BaseVertexEffect_t299_1_0_0;
struct BaseVertexEffect_t299;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t299_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t299_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t105_0_0_0/* parent */
	, BaseVertexEffect_t299_VTable/* vtableMethods */
	, BaseVertexEffect_t299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 625/* fieldStart */

};
TypeInfo BaseVertexEffect_t299_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, BaseVertexEffect_t299_MethodInfos/* methods */
	, BaseVertexEffect_t299_PropertyInfos/* properties */
	, NULL/* events */
	, &BaseVertexEffect_t299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 343/* custom_attributes_cache */
	, &BaseVertexEffect_t299_0_0_0/* byval_arg */
	, &BaseVertexEffect_t299_1_0_0/* this_arg */
	, &BaseVertexEffect_t299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t299)/* instance_size */
	, sizeof (BaseVertexEffect_t299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t358_il2cpp_TypeInfo;
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo IVertexModifier_t358_IVertexModifier_ModifyVertices_m2097_ParameterInfos[] = 
{
	{"verts", 0, 134218451, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.IVertexModifier::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo IVertexModifier_ModifyVertices_m2097_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &IVertexModifier_t358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IVertexModifier_t358_IVertexModifier_ModifyVertices_m2097_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IVertexModifier_t358_MethodInfos[] =
{
	&IVertexModifier_ModifyVertices_m2097_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IVertexModifier_t358_1_0_0;
struct IVertexModifier_t358;
const Il2CppTypeDefinitionMetadata IVertexModifier_t358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IVertexModifier_t358_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IVertexModifier_t358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IVertexModifier_t358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t358_0_0_0/* byval_arg */
	, &IVertexModifier_t358_1_0_0/* this_arg */
	, &IVertexModifier_t358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t300_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::.ctor()
extern const MethodInfo Outline__ctor_m1435_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Outline__ctor_m1435/* method */
	, &Outline_t300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo Outline_t300_Outline_ModifyVertices_m1436_ParameterInfos[] = 
{
	{"verts", 0, 134218452, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Outline_ModifyVertices_m1436_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Outline_ModifyVertices_m1436/* method */
	, &Outline_t300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Outline_t300_Outline_ModifyVertices_m1436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Outline_t300_MethodInfos[] =
{
	&Outline__ctor_m1435_MethodInfo,
	&Outline_ModifyVertices_m1436_MethodInfo,
	NULL
};
extern const MethodInfo Outline_ModifyVertices_m1436_MethodInfo;
static const Il2CppMethodReference Outline_t300_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&BaseVertexEffect_OnEnable_m1433_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&BaseVertexEffect_OnDisable_m1434_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Outline_ModifyVertices_m1436_MethodInfo,
	&Outline_ModifyVertices_m1436_MethodInfo,
};
static bool Outline_t300_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Outline_t300_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t358_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Outline_t300_0_0_0;
extern const Il2CppType Outline_t300_1_0_0;
extern const Il2CppType Shadow_t301_0_0_0;
struct Outline_t300;
const Il2CppTypeDefinitionMetadata Outline_t300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t300_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t301_0_0_0/* parent */
	, Outline_t300_VTable/* vtableMethods */
	, Outline_t300_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Outline_t300_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Outline_t300_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Outline_t300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 344/* custom_attributes_cache */
	, &Outline_t300_0_0_0/* byval_arg */
	, &Outline_t300_1_0_0/* this_arg */
	, &Outline_t300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t300)/* instance_size */
	, sizeof (Outline_t300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t302_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern const MethodInfo PositionAsUV1__ctor_m1437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAsUV1__ctor_m1437/* method */
	, &PositionAsUV1_t302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo PositionAsUV1_t302_PositionAsUV1_ModifyVertices_m1438_ParameterInfos[] = 
{
	{"verts", 0, 134218453, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo PositionAsUV1_ModifyVertices_m1438_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&PositionAsUV1_ModifyVertices_m1438/* method */
	, &PositionAsUV1_t302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PositionAsUV1_t302_PositionAsUV1_ModifyVertices_m1438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAsUV1_t302_MethodInfos[] =
{
	&PositionAsUV1__ctor_m1437_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1438_MethodInfo,
	NULL
};
extern const MethodInfo PositionAsUV1_ModifyVertices_m1438_MethodInfo;
static const Il2CppMethodReference PositionAsUV1_t302_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&BaseVertexEffect_OnEnable_m1433_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&BaseVertexEffect_OnDisable_m1434_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1438_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1438_MethodInfo,
};
static bool PositionAsUV1_t302_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t302_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t358_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PositionAsUV1_t302_0_0_0;
extern const Il2CppType PositionAsUV1_t302_1_0_0;
struct PositionAsUV1_t302;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t302_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t299_0_0_0/* parent */
	, PositionAsUV1_t302_VTable/* vtableMethods */
	, PositionAsUV1_t302_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PositionAsUV1_t302_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, PositionAsUV1_t302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAsUV1_t302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 345/* custom_attributes_cache */
	, &PositionAsUV1_t302_0_0_0/* byval_arg */
	, &PositionAsUV1_t302_1_0_0/* this_arg */
	, &PositionAsUV1_t302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t302)/* instance_size */
	, sizeof (PositionAsUV1_t302)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t301_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::.ctor()
extern const MethodInfo Shadow__ctor_m1439_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Shadow__ctor_m1439/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Color_t163 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern const MethodInfo Shadow_get_effectColor_m1440_MethodInfo = 
{
	"get_effectColor"/* name */
	, (methodPointerType)&Shadow_get_effectColor_m1440/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Color_t163_0_0_0/* return_type */
	, RuntimeInvoker_Color_t163/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t163_0_0_0;
static const ParameterInfo Shadow_t301_Shadow_set_effectColor_m1441_ParameterInfos[] = 
{
	{"value", 0, 134218454, 0, &Color_t163_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Color_t163 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern const MethodInfo Shadow_set_effectColor_m1441_MethodInfo = 
{
	"set_effectColor"/* name */
	, (methodPointerType)&Shadow_set_effectColor_m1441/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Color_t163/* invoker_method */
	, Shadow_t301_Shadow_set_effectColor_m1441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern const MethodInfo Shadow_get_effectDistance_m1442_MethodInfo = 
{
	"get_effectDistance"/* name */
	, (methodPointerType)&Shadow_get_effectDistance_m1442/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t53_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t53/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo Shadow_t301_Shadow_set_effectDistance_m1443_ParameterInfos[] = 
{
	{"value", 0, 134218455, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern const MethodInfo Shadow_set_effectDistance_m1443_MethodInfo = 
{
	"set_effectDistance"/* name */
	, (methodPointerType)&Shadow_set_effectDistance_m1443/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Vector2_t53/* invoker_method */
	, Shadow_t301_Shadow_set_effectDistance_m1443_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern const MethodInfo Shadow_get_useGraphicAlpha_m1444_MethodInfo = 
{
	"get_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_get_useGraphicAlpha_m1444/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Shadow_t301_Shadow_set_useGraphicAlpha_m1445_ParameterInfos[] = 
{
	{"value", 0, 134218456, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern const MethodInfo Shadow_set_useGraphicAlpha_m1445_MethodInfo = 
{
	"set_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_set_useGraphicAlpha_m1445/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Shadow_t301_Shadow_set_useGraphicAlpha_m1445_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
extern const Il2CppType Color32_t335_0_0_0;
extern const Il2CppType Color32_t335_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Shadow_t301_Shadow_ApplyShadow_m1446_ParameterInfos[] = 
{
	{"verts", 0, 134218457, 0, &List_1_t227_0_0_0},
	{"color", 1, 134218458, 0, &Color32_t335_0_0_0},
	{"start", 2, 134218459, 0, &Int32_t54_0_0_0},
	{"end", 3, 134218460, 0, &Int32_t54_0_0_0},
	{"x", 4, 134218461, 0, &Single_t85_0_0_0},
	{"y", 5, 134218462, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Color32_t335_Int32_t54_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo Shadow_ApplyShadow_m1446_MethodInfo = 
{
	"ApplyShadow"/* name */
	, (methodPointerType)&Shadow_ApplyShadow_m1446/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Color32_t335_Int32_t54_Int32_t54_Single_t85_Single_t85/* invoker_method */
	, Shadow_t301_Shadow_ApplyShadow_m1446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t227_0_0_0;
static const ParameterInfo Shadow_t301_Shadow_ModifyVertices_m1447_ParameterInfos[] = 
{
	{"verts", 0, 134218463, 0, &List_1_t227_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Shadow_ModifyVertices_m1447_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Shadow_ModifyVertices_m1447/* method */
	, &Shadow_t301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Shadow_t301_Shadow_ModifyVertices_m1447_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Shadow_t301_MethodInfos[] =
{
	&Shadow__ctor_m1439_MethodInfo,
	&Shadow_get_effectColor_m1440_MethodInfo,
	&Shadow_set_effectColor_m1441_MethodInfo,
	&Shadow_get_effectDistance_m1442_MethodInfo,
	&Shadow_set_effectDistance_m1443_MethodInfo,
	&Shadow_get_useGraphicAlpha_m1444_MethodInfo,
	&Shadow_set_useGraphicAlpha_m1445_MethodInfo,
	&Shadow_ApplyShadow_m1446_MethodInfo,
	&Shadow_ModifyVertices_m1447_MethodInfo,
	NULL
};
extern const MethodInfo Shadow_get_effectColor_m1440_MethodInfo;
extern const MethodInfo Shadow_set_effectColor_m1441_MethodInfo;
static const PropertyInfo Shadow_t301____effectColor_PropertyInfo = 
{
	&Shadow_t301_il2cpp_TypeInfo/* parent */
	, "effectColor"/* name */
	, &Shadow_get_effectColor_m1440_MethodInfo/* get */
	, &Shadow_set_effectColor_m1441_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_effectDistance_m1442_MethodInfo;
extern const MethodInfo Shadow_set_effectDistance_m1443_MethodInfo;
static const PropertyInfo Shadow_t301____effectDistance_PropertyInfo = 
{
	&Shadow_t301_il2cpp_TypeInfo/* parent */
	, "effectDistance"/* name */
	, &Shadow_get_effectDistance_m1442_MethodInfo/* get */
	, &Shadow_set_effectDistance_m1443_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_useGraphicAlpha_m1444_MethodInfo;
extern const MethodInfo Shadow_set_useGraphicAlpha_m1445_MethodInfo;
static const PropertyInfo Shadow_t301____useGraphicAlpha_PropertyInfo = 
{
	&Shadow_t301_il2cpp_TypeInfo/* parent */
	, "useGraphicAlpha"/* name */
	, &Shadow_get_useGraphicAlpha_m1444_MethodInfo/* get */
	, &Shadow_set_useGraphicAlpha_m1445_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Shadow_t301_PropertyInfos[] =
{
	&Shadow_t301____effectColor_PropertyInfo,
	&Shadow_t301____effectDistance_PropertyInfo,
	&Shadow_t301____useGraphicAlpha_PropertyInfo,
	NULL
};
extern const MethodInfo Shadow_ModifyVertices_m1447_MethodInfo;
static const Il2CppMethodReference Shadow_t301_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&UIBehaviour_Awake_m365_MethodInfo,
	&BaseVertexEffect_OnEnable_m1433_MethodInfo,
	&UIBehaviour_Start_m367_MethodInfo,
	&BaseVertexEffect_OnDisable_m1434_MethodInfo,
	&UIBehaviour_OnDestroy_m369_MethodInfo,
	&UIBehaviour_IsActive_m370_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m371_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m372_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m373_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m374_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m375_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m376_MethodInfo,
	&Shadow_ModifyVertices_m1447_MethodInfo,
	&Shadow_ModifyVertices_m1447_MethodInfo,
};
static bool Shadow_t301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Shadow_t301_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t358_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Shadow_t301_1_0_0;
struct Shadow_t301;
const Il2CppTypeDefinitionMetadata Shadow_t301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t301_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t299_0_0_0/* parent */
	, Shadow_t301_VTable/* vtableMethods */
	, Shadow_t301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 626/* fieldStart */

};
TypeInfo Shadow_t301_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Shadow_t301_MethodInfos/* methods */
	, Shadow_t301_PropertyInfos/* properties */
	, NULL/* events */
	, &Shadow_t301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 346/* custom_attributes_cache */
	, &Shadow_t301_0_0_0/* byval_arg */
	, &Shadow_t301_1_0_0/* this_arg */
	, &Shadow_t301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t301)/* instance_size */
	, sizeof (Shadow_t301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
