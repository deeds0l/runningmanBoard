﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct U24ArrayTypeU242048_t1902;
struct U24ArrayTypeU242048_t1902_marshaled;

void U24ArrayTypeU242048_t1902_marshal(const U24ArrayTypeU242048_t1902& unmarshaled, U24ArrayTypeU242048_t1902_marshaled& marshaled);
void U24ArrayTypeU242048_t1902_marshal_back(const U24ArrayTypeU242048_t1902_marshaled& marshaled, U24ArrayTypeU242048_t1902& unmarshaled);
void U24ArrayTypeU242048_t1902_marshal_cleanup(U24ArrayTypeU242048_t1902_marshaled& marshaled);
