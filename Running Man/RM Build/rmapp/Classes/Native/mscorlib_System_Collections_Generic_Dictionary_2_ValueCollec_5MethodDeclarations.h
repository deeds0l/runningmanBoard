﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t2544;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2535;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m12863_gshared (Enumerator_t2544 * __this, Dictionary_2_t2535 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m12863(__this, ___host, method) (( void (*) (Enumerator_t2544 *, Dictionary_2_t2535 *, const MethodInfo*))Enumerator__ctor_m12863_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12864_gshared (Enumerator_t2544 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12864(__this, method) (( Object_t * (*) (Enumerator_t2544 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12864_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m12865_gshared (Enumerator_t2544 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12865(__this, method) (( void (*) (Enumerator_t2544 *, const MethodInfo*))Enumerator_Dispose_m12865_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12866_gshared (Enumerator_t2544 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12866(__this, method) (( bool (*) (Enumerator_t2544 *, const MethodInfo*))Enumerator_MoveNext_m12866_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m12867_gshared (Enumerator_t2544 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12867(__this, method) (( Object_t * (*) (Enumerator_t2544 *, const MethodInfo*))Enumerator_get_Current_m12867_gshared)(__this, method)
