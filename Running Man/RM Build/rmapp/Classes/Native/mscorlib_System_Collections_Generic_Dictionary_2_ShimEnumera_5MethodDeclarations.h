﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>
struct ShimEnumerator_t2943;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t2932;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m18166_gshared (ShimEnumerator_t2943 * __this, Dictionary_2_t2932 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m18166(__this, ___host, method) (( void (*) (ShimEnumerator_t2943 *, Dictionary_2_t2932 *, const MethodInfo*))ShimEnumerator__ctor_m18166_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m18167_gshared (ShimEnumerator_t2943 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m18167(__this, method) (( bool (*) (ShimEnumerator_t2943 *, const MethodInfo*))ShimEnumerator_MoveNext_m18167_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Entry()
extern "C" DictionaryEntry_t1147  ShimEnumerator_get_Entry_m18168_gshared (ShimEnumerator_t2943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m18168(__this, method) (( DictionaryEntry_t1147  (*) (ShimEnumerator_t2943 *, const MethodInfo*))ShimEnumerator_get_Entry_m18168_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m18169_gshared (ShimEnumerator_t2943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m18169(__this, method) (( Object_t * (*) (ShimEnumerator_t2943 *, const MethodInfo*))ShimEnumerator_get_Key_m18169_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m18170_gshared (ShimEnumerator_t2943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m18170(__this, method) (( Object_t * (*) (ShimEnumerator_t2943 *, const MethodInfo*))ShimEnumerator_get_Value_m18170_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m18171_gshared (ShimEnumerator_t2943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m18171(__this, method) (( Object_t * (*) (ShimEnumerator_t2943 *, const MethodInfo*))ShimEnumerator_get_Current_m18171_gshared)(__this, method)
