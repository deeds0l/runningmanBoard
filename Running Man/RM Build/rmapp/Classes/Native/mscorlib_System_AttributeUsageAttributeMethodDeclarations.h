﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t803;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C" void AttributeUsageAttribute__ctor_m3590 (AttributeUsageAttribute_t803 * __this, int32_t ___validOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
extern "C" bool AttributeUsageAttribute_get_AllowMultiple_m5976 (AttributeUsageAttribute_t803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m3592 (AttributeUsageAttribute_t803 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
extern "C" bool AttributeUsageAttribute_get_Inherited_m5977 (AttributeUsageAttribute_t803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C" void AttributeUsageAttribute_set_Inherited_m3591 (AttributeUsageAttribute_t803 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
