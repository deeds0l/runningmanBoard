﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<ClearAudioSource>c__AnonStorey8
struct U3CClearAudioSourceU3Ec__AnonStorey8_t15;
// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/<ClearAudioSource>c__AnonStorey8::.ctor()
extern "C" void U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24 (U3CClearAudioSourceU3Ec__AnonStorey8_t15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<ClearAudioSource>c__AnonStorey8::<>m__2(AudioPooler/objectSource)
extern "C" bool U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25 (U3CClearAudioSourceU3Ec__AnonStorey8_t15 * __this, objectSource_t8 * ___audio, const MethodInfo* method) IL2CPP_METHOD_ATTR;
