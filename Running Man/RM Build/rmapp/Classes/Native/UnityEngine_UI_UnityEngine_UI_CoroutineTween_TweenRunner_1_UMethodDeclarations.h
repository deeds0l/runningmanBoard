﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t2619;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m13816_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m13816(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m13816_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13817_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13817(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13817_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13818_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13818(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13818_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m13819_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m13819(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m13819_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m13820_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m13820(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m13820_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m13821_gshared (U3CStartU3Ec__Iterator0_t2619 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m13821(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2619 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m13821_gshared)(__this, method)
