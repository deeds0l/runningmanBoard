﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t169;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Selectable>
struct  Comparison_1_t2661  : public MulticastDelegate_t216
{
};
