﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t233;
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.RawImage
struct  RawImage_t234  : public MaskableGraphic_t204
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t233 * ___m_Texture_23;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t220  ___m_UVRect_24;
};
