﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t2817;
// System.Collections.Generic.ICollection`1<System.UInt64>
struct ICollection_1_t3207;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt64,System.Object>
struct KeyCollection_t2821;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt64,System.Object>
struct ValueCollection_t2825;
// System.Collections.Generic.IEqualityComparer`1<System.UInt64>
struct IEqualityComparer_1_t2815;
// System.Collections.Generic.IDictionary`2<System.UInt64,System.Object>
struct IDictionary_2_t3208;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
struct KeyValuePair_2U5BU5D_t3209;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>
struct IEnumerator_1_t3210;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m16542_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m16542(__this, method) (( void (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2__ctor_m16542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16544_gshared (Dictionary_2_t2817 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16544(__this, ___comparer, method) (( void (*) (Dictionary_2_t2817 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16544_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m16546_gshared (Dictionary_2_t2817 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m16546(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2817 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16546_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16548_gshared (Dictionary_2_t2817 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16548(__this, ___capacity, method) (( void (*) (Dictionary_2_t2817 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16548_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16550_gshared (Dictionary_2_t2817 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16550(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2817 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16550_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16552_gshared (Dictionary_2_t2817 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16552(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2817 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m16552_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16554_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16554(__this, method) (( Object_t* (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16554_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16556_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16556(__this, method) (( Object_t* (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16556_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16558_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16558(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16558_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16560_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16560(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2817 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16560_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16562_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16562(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2817 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16562_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16564_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16564(__this, ___key, method) (( bool (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16564_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16566_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16566(__this, ___key, method) (( void (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16566_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16568_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16568(__this, method) (( bool (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16568_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16570_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16570(__this, method) (( Object_t * (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16570_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572(__this, method) (( bool (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16574_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2_t2818  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16574(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2817 *, KeyValuePair_2_t2818 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16574_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16576_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2_t2818  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16576(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2817 *, KeyValuePair_2_t2818 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16576_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16578_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2U5BU5D_t3209* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16578(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2817 *, KeyValuePair_2U5BU5D_t3209*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16578_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16580_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2_t2818  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16580(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2817 *, KeyValuePair_2_t2818 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16580_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16582_gshared (Dictionary_2_t2817 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16582(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2817 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16582_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16584_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16584(__this, method) (( Object_t * (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16584_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16586_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16586(__this, method) (( Object_t* (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16586_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16588_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16588(__this, method) (( Object_t * (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16588_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16590_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16590(__this, method) (( int32_t (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_get_Count_m16590_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m16592_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16592(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2817 *, uint64_t, const MethodInfo*))Dictionary_2_get_Item_m16592_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16594_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16594(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2817 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m16594_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16596_gshared (Dictionary_2_t2817 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16596(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2817 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16596_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16598_gshared (Dictionary_2_t2817 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16598(__this, ___size, method) (( void (*) (Dictionary_2_t2817 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16598_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16600_gshared (Dictionary_2_t2817 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16600(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2817 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16600_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2818  Dictionary_2_make_pair_m16602_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16602(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2818  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m16602_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::pick_key(TKey,TValue)
extern "C" uint64_t Dictionary_2_pick_key_m16604_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16604(__this /* static, unused */, ___key, ___value, method) (( uint64_t (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m16604_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m16606_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16606(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m16606_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16608_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2U5BU5D_t3209* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16608(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2817 *, KeyValuePair_2U5BU5D_t3209*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16608_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m16610_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16610(__this, method) (( void (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_Resize_m16610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16612_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16612(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2817 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m16612_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m16614_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16614(__this, method) (( void (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_Clear_m16614_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16616_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16616(__this, ___key, method) (( bool (*) (Dictionary_2_t2817 *, uint64_t, const MethodInfo*))Dictionary_2_ContainsKey_m16616_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16618_gshared (Dictionary_2_t2817 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16618(__this, ___value, method) (( bool (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m16618_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16620_gshared (Dictionary_2_t2817 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16620(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2817 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m16620_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16622_gshared (Dictionary_2_t2817 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16622(__this, ___sender, method) (( void (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16622_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16624_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16624(__this, ___key, method) (( bool (*) (Dictionary_2_t2817 *, uint64_t, const MethodInfo*))Dictionary_2_Remove_m16624_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16626_gshared (Dictionary_2_t2817 * __this, uint64_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16626(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2817 *, uint64_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m16626_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Keys()
extern "C" KeyCollection_t2821 * Dictionary_2_get_Keys_m16628_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16628(__this, method) (( KeyCollection_t2821 * (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_get_Keys_m16628_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::get_Values()
extern "C" ValueCollection_t2825 * Dictionary_2_get_Values_m16630_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16630(__this, method) (( ValueCollection_t2825 * (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_get_Values_m16630_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ToTKey(System.Object)
extern "C" uint64_t Dictionary_2_ToTKey_m16632_gshared (Dictionary_2_t2817 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16632(__this, ___key, method) (( uint64_t (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16632_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m16634_gshared (Dictionary_2_t2817 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16634(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2817 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16634_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16636_gshared (Dictionary_2_t2817 * __this, KeyValuePair_2_t2818  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16636(__this, ___pair, method) (( bool (*) (Dictionary_2_t2817 *, KeyValuePair_2_t2818 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16636_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::GetEnumerator()
extern "C" Enumerator_t2823  Dictionary_2_GetEnumerator_m16638_gshared (Dictionary_2_t2817 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16638(__this, method) (( Enumerator_t2823  (*) (Dictionary_2_t2817 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16638_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m16640_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16640(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16640_gshared)(__this /* static, unused */, ___key, ___value, method)
