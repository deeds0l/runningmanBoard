﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>
struct InternalEnumerator_1_t2887;
// System.Object
struct Object_t;
// UnityEngine.RequireComponent
struct RequireComponent_t61;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17535(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2887 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17536(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2887 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m17537(__this, method) (( void (*) (InternalEnumerator_1_t2887 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17538(__this, method) (( bool (*) (InternalEnumerator_1_t2887 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RequireComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m17539(__this, method) (( RequireComponent_t61 * (*) (InternalEnumerator_1_t2887 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
