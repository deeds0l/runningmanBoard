﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t1027;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t1025;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1024;
// System.Security.Cryptography.Oid
struct Oid_t1026;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1035;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Security.Cryptography.DSA
struct DSA_t1148;
// System.Security.Cryptography.RSA
struct RSA_t1149;

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m3995 (PublicKey_t1027 * __this, X509Certificate_t1035 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t1025 * PublicKey_get_EncodedKeyValue_m3996 (PublicKey_t1027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t1025 * PublicKey_get_EncodedParameters_m3997 (PublicKey_t1027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t1024 * PublicKey_get_Key_m3998 (PublicKey_t1027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t1026 * PublicKey_get_Oid_m3999 (PublicKey_t1027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t546* PublicKey_GetUnsignedBigInteger_m4000 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t1148 * PublicKey_DecodeDSA_m4001 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___rawPublicKey, ByteU5BU5D_t546* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t1149 * PublicKey_DecodeRSA_m4002 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
