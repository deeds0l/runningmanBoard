﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<ClearMusic>c__AnonStorey9
struct U3CClearMusicU3Ec__AnonStorey9_t16;
// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/<ClearMusic>c__AnonStorey9::.ctor()
extern "C" void U3CClearMusicU3Ec__AnonStorey9__ctor_m26 (U3CClearMusicU3Ec__AnonStorey9_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<ClearMusic>c__AnonStorey9::<>m__3(AudioPooler/objectSource)
extern "C" bool U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27 (U3CClearMusicU3Ec__AnonStorey9_t16 * __this, objectSource_t8 * ___audio, const MethodInfo* method) IL2CPP_METHOD_ATTR;
