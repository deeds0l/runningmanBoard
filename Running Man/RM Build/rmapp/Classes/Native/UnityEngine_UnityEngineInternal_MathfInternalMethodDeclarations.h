﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.MathfInternal
struct MathfInternal_t532;

// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern "C" void MathfInternal__cctor_m2600 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
