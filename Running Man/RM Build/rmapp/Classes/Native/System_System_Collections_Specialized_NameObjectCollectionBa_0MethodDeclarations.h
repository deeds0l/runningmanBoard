﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
struct _KeysEnumerator_t983;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t982;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C" void _KeysEnumerator__ctor_m3826 (_KeysEnumerator_t983 * __this, NameObjectCollectionBase_t982 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::get_Current()
extern "C" Object_t * _KeysEnumerator_get_Current_m3827 (_KeysEnumerator_t983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::MoveNext()
extern "C" bool _KeysEnumerator_MoveNext_m3828 (_KeysEnumerator_t983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator::Reset()
extern "C" void _KeysEnumerator_Reset_m3829 (_KeysEnumerator_t983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
