﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/objectSource::.ctor()
extern "C" void objectSource__ctor_m7 (objectSource_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
