﻿#pragma once
#include <stdint.h>
// UnityEngine.Component[]
struct ComponentU5BU5D_t2488;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
struct  List_1_t332  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Component>::_items
	ComponentU5BU5D_t2488* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::_version
	int32_t ____version_3;
};
struct List_1_t332_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Component>::EmptyArray
	ComponentU5BU5D_t2488* ___EmptyArray_4;
};
