﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
struct Enumerator_t2611;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t227;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m13697_gshared (Enumerator_t2611 * __this, List_1_t227 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m13697(__this, ___l, method) (( void (*) (Enumerator_t2611 *, List_1_t227 *, const MethodInfo*))Enumerator__ctor_m13697_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13698_gshared (Enumerator_t2611 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13698(__this, method) (( Object_t * (*) (Enumerator_t2611 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13698_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m13699_gshared (Enumerator_t2611 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13699(__this, method) (( void (*) (Enumerator_t2611 *, const MethodInfo*))Enumerator_Dispose_m13699_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m13700_gshared (Enumerator_t2611 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13700(__this, method) (( void (*) (Enumerator_t2611 *, const MethodInfo*))Enumerator_VerifyState_m13700_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13701_gshared (Enumerator_t2611 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13701(__this, method) (( bool (*) (Enumerator_t2611 *, const MethodInfo*))Enumerator_MoveNext_m13701_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t225  Enumerator_get_Current_m13702_gshared (Enumerator_t2611 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13702(__this, method) (( UIVertex_t225  (*) (Enumerator_t2611 *, const MethodInfo*))Enumerator_get_Current_m13702_gshared)(__this, method)
