﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1;
// System.Object
struct Object_t;
// UnityEngine.AudioClip
struct AudioClip_t20;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>
struct IEnumerator_1_t3033;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>
struct ICollection_1_t3034;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>
struct IEnumerable_1_t39;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>
struct ReadOnlyCollection_1_t2444;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2427;
// System.Predicate`1<UnityEngine.AudioClip>
struct Predicate_1_t2445;
// System.Comparison`1<UnityEngine.AudioClip>
struct Comparison_1_t2447;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m11274(__this, method) (( void (*) (List_1_t1 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.ctor(System.Int32)
#define List_1__ctor_m11275(__this, ___capacity, method) (( void (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.cctor()
#define List_1__cctor_m11277(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11279(__this, method) (( Object_t* (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m11280(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11281(__this, method) (( Object_t * (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m11282(__this, ___item, method) (( int32_t (*) (List_1_t1 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m11283(__this, ___item, method) (( bool (*) (List_1_t1 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m11284(__this, ___item, method) (( int32_t (*) (List_1_t1 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m11285(__this, ___index, ___item, method) (( void (*) (List_1_t1 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m11286(__this, ___item, method) (( void (*) (List_1_t1 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11287(__this, method) (( bool (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m11288(__this, method) (( bool (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m11289(__this, method) (( Object_t * (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m11290(__this, method) (( bool (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m11291(__this, method) (( bool (*) (List_1_t1 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m11292(__this, ___index, method) (( Object_t * (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m11293(__this, ___index, ___value, method) (( void (*) (List_1_t1 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Add(T)
#define List_1_Add_m11294(__this, ___item, method) (( void (*) (List_1_t1 *, AudioClip_t20 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m11295(__this, ___newCount, method) (( void (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m11297(__this, ___collection, method) (( void (*) (List_1_t1 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m11299(__this, ___enumerable, method) (( void (*) (List_1_t1 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m11301(__this, ___collection, method) (( void (*) (List_1_t1 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::AsReadOnly()
#define List_1_AsReadOnly_m11303(__this, method) (( ReadOnlyCollection_1_t2444 * (*) (List_1_t1 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Clear()
#define List_1_Clear_m11305(__this, method) (( void (*) (List_1_t1 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::Contains(T)
#define List_1_Contains_m11306(__this, ___item, method) (( bool (*) (List_1_t1 *, AudioClip_t20 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m11307(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1 *, AudioClipU5BU5D_t2427*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioClip>::Find(System.Predicate`1<T>)
#define List_1_Find_m11308(__this, ___match, method) (( AudioClip_t20 * (*) (List_1_t1 *, Predicate_1_t2445 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m11310(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2445 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m11312(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1 *, int32_t, int32_t, Predicate_1_t2445 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::GetEnumerator()
#define List_1_GetEnumerator_m11314(__this, method) (( Enumerator_t2446  (*) (List_1_t1 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::IndexOf(T)
#define List_1_IndexOf_m11316(__this, ___item, method) (( int32_t (*) (List_1_t1 *, AudioClip_t20 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m11317(__this, ___start, ___delta, method) (( void (*) (List_1_t1 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m11319(__this, ___index, method) (( void (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Insert(System.Int32,T)
#define List_1_Insert_m11321(__this, ___index, ___item, method) (( void (*) (List_1_t1 *, int32_t, AudioClip_t20 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m11322(__this, ___collection, method) (( void (*) (List_1_t1 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::Remove(T)
#define List_1_Remove_m11324(__this, ___item, method) (( bool (*) (List_1_t1 *, AudioClip_t20 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m11325(__this, ___match, method) (( int32_t (*) (List_1_t1 *, Predicate_1_t2445 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m11327(__this, ___index, method) (( void (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Reverse()
#define List_1_Reverse_m11328(__this, method) (( void (*) (List_1_t1 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Sort()
#define List_1_Sort_m11330(__this, method) (( void (*) (List_1_t1 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m11332(__this, ___comparison, method) (( void (*) (List_1_t1 *, Comparison_1_t2447 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.AudioClip>::ToArray()
#define List_1_ToArray_m11334(__this, method) (( AudioClipU5BU5D_t2427* (*) (List_1_t1 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::TrimExcess()
#define List_1_TrimExcess_m11336(__this, method) (( void (*) (List_1_t1 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Capacity()
#define List_1_get_Capacity_m11338(__this, method) (( int32_t (*) (List_1_t1 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m11340(__this, ___value, method) (( void (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Count()
#define List_1_get_Count_m11342(__this, method) (( int32_t (*) (List_1_t1 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Item(System.Int32)
#define List_1_get_Item_m11343(__this, ___index, method) (( AudioClip_t20 * (*) (List_1_t1 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::set_Item(System.Int32,T)
#define List_1_set_Item_m11344(__this, ___index, ___value, method) (( void (*) (List_1_t1 *, int32_t, AudioClip_t20 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
