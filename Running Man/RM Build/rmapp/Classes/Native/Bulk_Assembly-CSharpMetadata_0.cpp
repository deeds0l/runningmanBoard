﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t0_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t0_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioManager
#include "AssemblyU2DCSharp_AudioManager.h"
// Metadata Definition AudioManager
extern TypeInfo AudioManager_t2_il2cpp_TypeInfo;
// AudioManager
#include "AssemblyU2DCSharp_AudioManagerMethodDeclarations.h"
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::.ctor()
extern const MethodInfo AudioManager__ctor_m0_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AudioManager__ctor_m0/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::Awake()
extern const MethodInfo AudioManager_Awake_m1_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&AudioManager_Awake_m1/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioManager_t2_AudioManager_PlayMusic_m2_ParameterInfos[] = 
{
	{"p_soundName", 0, 134217729, 0, &String_t_0_0_0},
	{"p_bisLooping", 1, 134217730, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::PlayMusic(System.String,System.Boolean)
extern const MethodInfo AudioManager_PlayMusic_m2_MethodInfo = 
{
	"PlayMusic"/* name */
	, (methodPointerType)&AudioManager_PlayMusic_m2/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, AudioManager_t2_AudioManager_PlayMusic_m2_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioManager_t2_AudioManager_PlaySound_m3_ParameterInfos[] = 
{
	{"p_soundName", 0, 134217731, 0, &String_t_0_0_0},
	{"p_bisLooping", 1, 134217732, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::PlaySound(System.String,System.Boolean)
extern const MethodInfo AudioManager_PlaySound_m3_MethodInfo = 
{
	"PlaySound"/* name */
	, (methodPointerType)&AudioManager_PlaySound_m3/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, AudioManager_t2_AudioManager_PlaySound_m3_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::StopAudio()
extern const MethodInfo AudioManager_StopAudio_m4_MethodInfo = 
{
	"StopAudio"/* name */
	, (methodPointerType)&AudioManager_StopAudio_m4/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::StopMusic()
extern const MethodInfo AudioManager_StopMusic_m5_MethodInfo = 
{
	"StopMusic"/* name */
	, (methodPointerType)&AudioManager_StopMusic_m5/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioManager::StopSoundEffects()
extern const MethodInfo AudioManager_StopSoundEffects_m6_MethodInfo = 
{
	"StopSoundEffects"/* name */
	, (methodPointerType)&AudioManager_StopSoundEffects_m6/* method */
	, &AudioManager_t2_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AudioManager_t2_MethodInfos[] =
{
	&AudioManager__ctor_m0_MethodInfo,
	&AudioManager_Awake_m1_MethodInfo,
	&AudioManager_PlayMusic_m2_MethodInfo,
	&AudioManager_PlaySound_m3_MethodInfo,
	&AudioManager_StopAudio_m4_MethodInfo,
	&AudioManager_StopMusic_m5_MethodInfo,
	&AudioManager_StopSoundEffects_m6_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m217_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Object_GetHashCode_m219_MethodInfo;
extern const MethodInfo Object_ToString_m220_MethodInfo;
extern const MethodInfo AudioManager_Awake_m1_MethodInfo;
extern const Il2CppGenericMethod MonoManager_1_OnDestroy_m221_GenericMethod;
static const Il2CppMethodReference AudioManager_t2_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&AudioManager_Awake_m1_MethodInfo,
	&MonoManager_1_OnDestroy_m221_GenericMethod,
};
static bool AudioManager_t2_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AudioManager_t2_0_0_0;
extern const Il2CppType AudioManager_t2_1_0_0;
extern const Il2CppType MonoManager_1_t3_0_0_0;
struct AudioManager_t2;
const Il2CppTypeDefinitionMetadata AudioManager_t2_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoManager_1_t3_0_0_0/* parent */
	, AudioManager_t2_VTable/* vtableMethods */
	, AudioManager_t2_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo AudioManager_t2_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioManager"/* name */
	, ""/* namespaze */
	, AudioManager_t2_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AudioManager_t2_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioManager_t2_0_0_0/* byval_arg */
	, &AudioManager_t2_1_0_0/* this_arg */
	, &AudioManager_t2_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioManager_t2)/* instance_size */
	, sizeof (AudioManager_t2)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPE.h"
// Metadata Definition AudioPooler/SOUNDTYPE
extern TypeInfo SOUNDTYPE_t4_il2cpp_TypeInfo;
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPEMethodDeclarations.h"
static const MethodInfo* SOUNDTYPE_t4_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference SOUNDTYPE_t4_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool SOUNDTYPE_t4_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair SOUNDTYPE_t4_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SOUNDTYPE_t4_0_0_0;
extern const Il2CppType SOUNDTYPE_t4_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
extern TypeInfo AudioPooler_t10_il2cpp_TypeInfo;
extern const Il2CppType AudioPooler_t10_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata SOUNDTYPE_t4_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SOUNDTYPE_t4_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, SOUNDTYPE_t4_VTable/* vtableMethods */
	, SOUNDTYPE_t4_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1/* fieldStart */

};
TypeInfo SOUNDTYPE_t4_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SOUNDTYPE"/* name */
	, ""/* namespaze */
	, SOUNDTYPE_t4_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SOUNDTYPE_t4_0_0_0/* byval_arg */
	, &SOUNDTYPE_t4_1_0_0/* this_arg */
	, &SOUNDTYPE_t4_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SOUNDTYPE_t4)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SOUNDTYPE_t4)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// AudioPooler/objectSource
#include "AssemblyU2DCSharp_AudioPooler_objectSource.h"
// Metadata Definition AudioPooler/objectSource
extern TypeInfo objectSource_t8_il2cpp_TypeInfo;
// AudioPooler/objectSource
#include "AssemblyU2DCSharp_AudioPooler_objectSourceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/objectSource::.ctor()
extern const MethodInfo objectSource__ctor_m7_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&objectSource__ctor_m7/* method */
	, &objectSource_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* objectSource_t8_MethodInfos[] =
{
	&objectSource__ctor_m7_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference objectSource_t8_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool objectSource_t8_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType objectSource_t8_0_0_0;
extern const Il2CppType objectSource_t8_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct objectSource_t8;
const Il2CppTypeDefinitionMetadata objectSource_t8_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, objectSource_t8_VTable/* vtableMethods */
	, objectSource_t8_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 4/* fieldStart */

};
TypeInfo objectSource_t8_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "objectSource"/* name */
	, ""/* namespaze */
	, objectSource_t8_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &objectSource_t8_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &objectSource_t8_0_0_0/* byval_arg */
	, &objectSource_t8_1_0_0/* this_arg */
	, &objectSource_t8_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (objectSource_t8)/* instance_size */
	, sizeof (objectSource_t8)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<DisableMusic>c__Iterator0
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableMusicU3Ec__Iterator0.h"
// Metadata Definition AudioPooler/<DisableMusic>c__Iterator0
extern TypeInfo U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo;
// AudioPooler/<DisableMusic>c__Iterator0
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableMusicU3Ec__Iterator0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableMusic>c__Iterator0::.ctor()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0__ctor_m8_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0__ctor_m8/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 6/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 7/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<DisableMusic>c__Iterator0::MoveNext()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_MoveNext_m11_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0_MoveNext_m11/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Dispose()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_Dispose_m12_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0_Dispose_m12/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 8/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Reset()
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_Reset_m13_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDisableMusicU3Ec__Iterator0_Reset_m13/* method */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 9/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDisableMusicU3Ec__Iterator0_t11_MethodInfos[] =
{
	&U3CDisableMusicU3Ec__Iterator0__ctor_m8_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_MoveNext_m11_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_Dispose_m12_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_Reset_m13_MethodInfo,
	NULL
};
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9_MethodInfo;
static const PropertyInfo U3CDisableMusicU3Ec__Iterator0_t11____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10_MethodInfo;
static const PropertyInfo U3CDisableMusicU3Ec__Iterator0_t11____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDisableMusicU3Ec__Iterator0_t11_PropertyInfos[] =
{
	&U3CDisableMusicU3Ec__Iterator0_t11____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDisableMusicU3Ec__Iterator0_t11____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_Dispose_m12_MethodInfo;
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_MoveNext_m11_MethodInfo;
extern const MethodInfo U3CDisableMusicU3Ec__Iterator0_Reset_m13_MethodInfo;
static const Il2CppMethodReference U3CDisableMusicU3Ec__Iterator0_t11_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_Dispose_m12_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_MoveNext_m11_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9_MethodInfo,
	&U3CDisableMusicU3Ec__Iterator0_Reset_m13_MethodInfo,
};
static bool U3CDisableMusicU3Ec__Iterator0_t11_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t43_0_0_0;
extern const Il2CppType IEnumerator_t28_0_0_0;
extern const Il2CppType IEnumerator_1_t78_0_0_0;
static const Il2CppType* U3CDisableMusicU3Ec__Iterator0_t11_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDisableMusicU3Ec__Iterator0_t11_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CDisableMusicU3Ec__Iterator0_t11_0_0_0;
extern const Il2CppType U3CDisableMusicU3Ec__Iterator0_t11_1_0_0;
struct U3CDisableMusicU3Ec__Iterator0_t11;
const Il2CppTypeDefinitionMetadata U3CDisableMusicU3Ec__Iterator0_t11_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDisableMusicU3Ec__Iterator0_t11_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDisableMusicU3Ec__Iterator0_t11_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDisableMusicU3Ec__Iterator0_t11_VTable/* vtableMethods */
	, U3CDisableMusicU3Ec__Iterator0_t11_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 9/* fieldStart */

};
TypeInfo U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<DisableMusic>c__Iterator0"/* name */
	, ""/* namespaze */
	, U3CDisableMusicU3Ec__Iterator0_t11_MethodInfos/* methods */
	, U3CDisableMusicU3Ec__Iterator0_t11_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 5/* custom_attributes_cache */
	, &U3CDisableMusicU3Ec__Iterator0_t11_0_0_0/* byval_arg */
	, &U3CDisableMusicU3Ec__Iterator0_t11_1_0_0/* this_arg */
	, &U3CDisableMusicU3Ec__Iterator0_t11_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDisableMusicU3Ec__Iterator0_t11)/* instance_size */
	, sizeof (U3CDisableMusicU3Ec__Iterator0_t11)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// AudioPooler/<DisableSound>c__Iterator1
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableSoundU3Ec__Iterator1.h"
// Metadata Definition AudioPooler/<DisableSound>c__Iterator1
extern TypeInfo U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo;
// AudioPooler/<DisableSound>c__Iterator1
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableSoundU3Ec__Iterator1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableSound>c__Iterator1::.ctor()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1__ctor_m14_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1__ctor_m14/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 11/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 12/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<DisableSound>c__Iterator1::MoveNext()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_MoveNext_m17_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1_MoveNext_m17/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableSound>c__Iterator1::Dispose()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_Dispose_m18_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1_Dispose_m18/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 13/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<DisableSound>c__Iterator1::Reset()
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_Reset_m19_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CDisableSoundU3Ec__Iterator1_Reset_m19/* method */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 14/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CDisableSoundU3Ec__Iterator1_t12_MethodInfos[] =
{
	&U3CDisableSoundU3Ec__Iterator1__ctor_m14_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_MoveNext_m17_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_Dispose_m18_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_Reset_m19_MethodInfo,
	NULL
};
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15_MethodInfo;
static const PropertyInfo U3CDisableSoundU3Ec__Iterator1_t12____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16_MethodInfo;
static const PropertyInfo U3CDisableSoundU3Ec__Iterator1_t12____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CDisableSoundU3Ec__Iterator1_t12_PropertyInfos[] =
{
	&U3CDisableSoundU3Ec__Iterator1_t12____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CDisableSoundU3Ec__Iterator1_t12____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_Dispose_m18_MethodInfo;
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_MoveNext_m17_MethodInfo;
extern const MethodInfo U3CDisableSoundU3Ec__Iterator1_Reset_m19_MethodInfo;
static const Il2CppMethodReference U3CDisableSoundU3Ec__Iterator1_t12_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_Dispose_m18_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_MoveNext_m17_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15_MethodInfo,
	&U3CDisableSoundU3Ec__Iterator1_Reset_m19_MethodInfo,
};
static bool U3CDisableSoundU3Ec__Iterator1_t12_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CDisableSoundU3Ec__Iterator1_t12_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CDisableSoundU3Ec__Iterator1_t12_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CDisableSoundU3Ec__Iterator1_t12_0_0_0;
extern const Il2CppType U3CDisableSoundU3Ec__Iterator1_t12_1_0_0;
struct U3CDisableSoundU3Ec__Iterator1_t12;
const Il2CppTypeDefinitionMetadata U3CDisableSoundU3Ec__Iterator1_t12_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CDisableSoundU3Ec__Iterator1_t12_InterfacesTypeInfos/* implementedInterfaces */
	, U3CDisableSoundU3Ec__Iterator1_t12_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CDisableSoundU3Ec__Iterator1_t12_VTable/* vtableMethods */
	, U3CDisableSoundU3Ec__Iterator1_t12_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 16/* fieldStart */

};
TypeInfo U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<DisableSound>c__Iterator1"/* name */
	, ""/* namespaze */
	, U3CDisableSoundU3Ec__Iterator1_t12_MethodInfos/* methods */
	, U3CDisableSoundU3Ec__Iterator1_t12_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 10/* custom_attributes_cache */
	, &U3CDisableSoundU3Ec__Iterator1_t12_0_0_0/* byval_arg */
	, &U3CDisableSoundU3Ec__Iterator1_t12_1_0_0/* this_arg */
	, &U3CDisableSoundU3Ec__Iterator1_t12_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CDisableSoundU3Ec__Iterator1_t12)/* instance_size */
	, sizeof (U3CDisableSoundU3Ec__Iterator1_t12)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// AudioPooler/<GetAudioSource>c__AnonStorey6
#include "AssemblyU2DCSharp_AudioPooler_U3CGetAudioSourceU3Ec__AnonSto.h"
// Metadata Definition AudioPooler/<GetAudioSource>c__AnonStorey6
extern TypeInfo U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo;
// AudioPooler/<GetAudioSource>c__AnonStorey6
#include "AssemblyU2DCSharp_AudioPooler_U3CGetAudioSourceU3Ec__AnonStoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<GetAudioSource>c__AnonStorey6::.ctor()
extern const MethodInfo U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20/* method */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType objectSource_t8_0_0_0;
static const ParameterInfo U3CGetAudioSourceU3Ec__AnonStorey6_t13_U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_ParameterInfos[] = 
{
	{"audio", 0, 134217755, 0, &objectSource_t8_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<GetAudioSource>c__AnonStorey6::<>m__0(AudioPooler/objectSource)
extern const MethodInfo U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_MethodInfo = 
{
	"<>m__0"/* name */
	, (methodPointerType)&U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21/* method */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CGetAudioSourceU3Ec__AnonStorey6_t13_U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetAudioSourceU3Ec__AnonStorey6_t13_MethodInfos[] =
{
	&U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20_MethodInfo,
	&U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetAudioSourceU3Ec__AnonStorey6_t13_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CGetAudioSourceU3Ec__AnonStorey6_t13_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CGetAudioSourceU3Ec__AnonStorey6_t13_0_0_0;
extern const Il2CppType U3CGetAudioSourceU3Ec__AnonStorey6_t13_1_0_0;
struct U3CGetAudioSourceU3Ec__AnonStorey6_t13;
const Il2CppTypeDefinitionMetadata U3CGetAudioSourceU3Ec__AnonStorey6_t13_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetAudioSourceU3Ec__AnonStorey6_t13_VTable/* vtableMethods */
	, U3CGetAudioSourceU3Ec__AnonStorey6_t13_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 23/* fieldStart */

};
TypeInfo U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetAudioSource>c__AnonStorey6"/* name */
	, ""/* namespaze */
	, U3CGetAudioSourceU3Ec__AnonStorey6_t13_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 15/* custom_attributes_cache */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_0_0_0/* byval_arg */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_1_0_0/* this_arg */
	, &U3CGetAudioSourceU3Ec__AnonStorey6_t13_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetAudioSourceU3Ec__AnonStorey6_t13)/* instance_size */
	, sizeof (U3CGetAudioSourceU3Ec__AnonStorey6_t13)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
#include "AssemblyU2DCSharp_AudioPooler_U3CReturnAudioSourceU3Ec__Anon.h"
// Metadata Definition AudioPooler/<ReturnAudioSource>c__AnonStorey7
extern TypeInfo U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo;
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
#include "AssemblyU2DCSharp_AudioPooler_U3CReturnAudioSourceU3Ec__AnonMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<ReturnAudioSource>c__AnonStorey7::.ctor()
extern const MethodInfo U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22/* method */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType objectSource_t8_0_0_0;
static const ParameterInfo U3CReturnAudioSourceU3Ec__AnonStorey7_t14_U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_ParameterInfos[] = 
{
	{"audio", 0, 134217756, 0, &objectSource_t8_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<ReturnAudioSource>c__AnonStorey7::<>m__1(AudioPooler/objectSource)
extern const MethodInfo U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_MethodInfo = 
{
	"<>m__1"/* name */
	, (methodPointerType)&U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23/* method */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CReturnAudioSourceU3Ec__AnonStorey7_t14_U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CReturnAudioSourceU3Ec__AnonStorey7_t14_MethodInfos[] =
{
	&U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22_MethodInfo,
	&U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CReturnAudioSourceU3Ec__AnonStorey7_t14_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CReturnAudioSourceU3Ec__AnonStorey7_t14_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CReturnAudioSourceU3Ec__AnonStorey7_t14_0_0_0;
extern const Il2CppType U3CReturnAudioSourceU3Ec__AnonStorey7_t14_1_0_0;
struct U3CReturnAudioSourceU3Ec__AnonStorey7_t14;
const Il2CppTypeDefinitionMetadata U3CReturnAudioSourceU3Ec__AnonStorey7_t14_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CReturnAudioSourceU3Ec__AnonStorey7_t14_VTable/* vtableMethods */
	, U3CReturnAudioSourceU3Ec__AnonStorey7_t14_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 24/* fieldStart */

};
TypeInfo U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ReturnAudioSource>c__AnonStorey7"/* name */
	, ""/* namespaze */
	, U3CReturnAudioSourceU3Ec__AnonStorey7_t14_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 16/* custom_attributes_cache */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_0_0_0/* byval_arg */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_1_0_0/* this_arg */
	, &U3CReturnAudioSourceU3Ec__AnonStorey7_t14_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CReturnAudioSourceU3Ec__AnonStorey7_t14)/* instance_size */
	, sizeof (U3CReturnAudioSourceU3Ec__AnonStorey7_t14)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<ClearAudioSource>c__AnonStorey8
#include "AssemblyU2DCSharp_AudioPooler_U3CClearAudioSourceU3Ec__AnonS.h"
// Metadata Definition AudioPooler/<ClearAudioSource>c__AnonStorey8
extern TypeInfo U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo;
// AudioPooler/<ClearAudioSource>c__AnonStorey8
#include "AssemblyU2DCSharp_AudioPooler_U3CClearAudioSourceU3Ec__AnonSMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<ClearAudioSource>c__AnonStorey8::.ctor()
extern const MethodInfo U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24/* method */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType objectSource_t8_0_0_0;
static const ParameterInfo U3CClearAudioSourceU3Ec__AnonStorey8_t15_U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_ParameterInfos[] = 
{
	{"audio", 0, 134217757, 0, &objectSource_t8_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<ClearAudioSource>c__AnonStorey8::<>m__2(AudioPooler/objectSource)
extern const MethodInfo U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_MethodInfo = 
{
	"<>m__2"/* name */
	, (methodPointerType)&U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25/* method */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CClearAudioSourceU3Ec__AnonStorey8_t15_U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CClearAudioSourceU3Ec__AnonStorey8_t15_MethodInfos[] =
{
	&U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24_MethodInfo,
	&U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CClearAudioSourceU3Ec__AnonStorey8_t15_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CClearAudioSourceU3Ec__AnonStorey8_t15_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CClearAudioSourceU3Ec__AnonStorey8_t15_0_0_0;
extern const Il2CppType U3CClearAudioSourceU3Ec__AnonStorey8_t15_1_0_0;
struct U3CClearAudioSourceU3Ec__AnonStorey8_t15;
const Il2CppTypeDefinitionMetadata U3CClearAudioSourceU3Ec__AnonStorey8_t15_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CClearAudioSourceU3Ec__AnonStorey8_t15_VTable/* vtableMethods */
	, U3CClearAudioSourceU3Ec__AnonStorey8_t15_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 25/* fieldStart */

};
TypeInfo U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ClearAudioSource>c__AnonStorey8"/* name */
	, ""/* namespaze */
	, U3CClearAudioSourceU3Ec__AnonStorey8_t15_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 17/* custom_attributes_cache */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_0_0_0/* byval_arg */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_1_0_0/* this_arg */
	, &U3CClearAudioSourceU3Ec__AnonStorey8_t15_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CClearAudioSourceU3Ec__AnonStorey8_t15)/* instance_size */
	, sizeof (U3CClearAudioSourceU3Ec__AnonStorey8_t15)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<ClearMusic>c__AnonStorey9
#include "AssemblyU2DCSharp_AudioPooler_U3CClearMusicU3Ec__AnonStorey9.h"
// Metadata Definition AudioPooler/<ClearMusic>c__AnonStorey9
extern TypeInfo U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo;
// AudioPooler/<ClearMusic>c__AnonStorey9
#include "AssemblyU2DCSharp_AudioPooler_U3CClearMusicU3Ec__AnonStorey9MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<ClearMusic>c__AnonStorey9::.ctor()
extern const MethodInfo U3CClearMusicU3Ec__AnonStorey9__ctor_m26_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CClearMusicU3Ec__AnonStorey9__ctor_m26/* method */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType objectSource_t8_0_0_0;
static const ParameterInfo U3CClearMusicU3Ec__AnonStorey9_t16_U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_ParameterInfos[] = 
{
	{"audio", 0, 134217758, 0, &objectSource_t8_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<ClearMusic>c__AnonStorey9::<>m__3(AudioPooler/objectSource)
extern const MethodInfo U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_MethodInfo = 
{
	"<>m__3"/* name */
	, (methodPointerType)&U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27/* method */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CClearMusicU3Ec__AnonStorey9_t16_U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CClearMusicU3Ec__AnonStorey9_t16_MethodInfos[] =
{
	&U3CClearMusicU3Ec__AnonStorey9__ctor_m26_MethodInfo,
	&U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CClearMusicU3Ec__AnonStorey9_t16_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CClearMusicU3Ec__AnonStorey9_t16_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CClearMusicU3Ec__AnonStorey9_t16_0_0_0;
extern const Il2CppType U3CClearMusicU3Ec__AnonStorey9_t16_1_0_0;
struct U3CClearMusicU3Ec__AnonStorey9_t16;
const Il2CppTypeDefinitionMetadata U3CClearMusicU3Ec__AnonStorey9_t16_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CClearMusicU3Ec__AnonStorey9_t16_VTable/* vtableMethods */
	, U3CClearMusicU3Ec__AnonStorey9_t16_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 26/* fieldStart */

};
TypeInfo U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ClearMusic>c__AnonStorey9"/* name */
	, ""/* namespaze */
	, U3CClearMusicU3Ec__AnonStorey9_t16_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 18/* custom_attributes_cache */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_0_0_0/* byval_arg */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_1_0_0/* this_arg */
	, &U3CClearMusicU3Ec__AnonStorey9_t16_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CClearMusicU3Ec__AnonStorey9_t16)/* instance_size */
	, sizeof (U3CClearMusicU3Ec__AnonStorey9_t16)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
#include "AssemblyU2DCSharp_AudioPooler_U3CClearSoundEffectsU3Ec__Anon.h"
// Metadata Definition AudioPooler/<ClearSoundEffects>c__AnonStoreyA
extern TypeInfo U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo;
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
#include "AssemblyU2DCSharp_AudioPooler_U3CClearSoundEffectsU3Ec__AnonMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<ClearSoundEffects>c__AnonStoreyA::.ctor()
extern const MethodInfo U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28/* method */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType objectSource_t8_0_0_0;
static const ParameterInfo U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_ParameterInfos[] = 
{
	{"audio", 0, 134217759, 0, &objectSource_t8_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<ClearSoundEffects>c__AnonStoreyA::<>m__4(AudioPooler/objectSource)
extern const MethodInfo U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_MethodInfo = 
{
	"<>m__4"/* name */
	, (methodPointerType)&U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29/* method */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_MethodInfos[] =
{
	&U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28_MethodInfo,
	&U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_0_0_0;
extern const Il2CppType U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_1_0_0;
struct U3CClearSoundEffectsU3Ec__AnonStoreyA_t17;
const Il2CppTypeDefinitionMetadata U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_VTable/* vtableMethods */
	, U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 27/* fieldStart */

};
TypeInfo U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ClearSoundEffects>c__AnonStoreyA"/* name */
	, ""/* namespaze */
	, U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 19/* custom_attributes_cache */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_0_0_0/* byval_arg */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_1_0_0/* this_arg */
	, &U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17)/* instance_size */
	, sizeof (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<SearchMusic>c__AnonStoreyB
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchMusicU3Ec__AnonStorey.h"
// Metadata Definition AudioPooler/<SearchMusic>c__AnonStoreyB
extern TypeInfo U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo;
// AudioPooler/<SearchMusic>c__AnonStoreyB
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchMusicU3Ec__AnonStoreyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<SearchMusic>c__AnonStoreyB::.ctor()
extern const MethodInfo U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30/* method */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioClip_t20_0_0_0;
extern const Il2CppType AudioClip_t20_0_0_0;
static const ParameterInfo U3CSearchMusicU3Ec__AnonStoreyB_t18_U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_ParameterInfos[] = 
{
	{"clip", 0, 134217760, 0, &AudioClip_t20_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<SearchMusic>c__AnonStoreyB::<>m__5(UnityEngine.AudioClip)
extern const MethodInfo U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_MethodInfo = 
{
	"<>m__5"/* name */
	, (methodPointerType)&U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31/* method */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CSearchMusicU3Ec__AnonStoreyB_t18_U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CSearchMusicU3Ec__AnonStoreyB_t18_MethodInfos[] =
{
	&U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30_MethodInfo,
	&U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CSearchMusicU3Ec__AnonStoreyB_t18_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CSearchMusicU3Ec__AnonStoreyB_t18_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CSearchMusicU3Ec__AnonStoreyB_t18_0_0_0;
extern const Il2CppType U3CSearchMusicU3Ec__AnonStoreyB_t18_1_0_0;
struct U3CSearchMusicU3Ec__AnonStoreyB_t18;
const Il2CppTypeDefinitionMetadata U3CSearchMusicU3Ec__AnonStoreyB_t18_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CSearchMusicU3Ec__AnonStoreyB_t18_VTable/* vtableMethods */
	, U3CSearchMusicU3Ec__AnonStoreyB_t18_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 28/* fieldStart */

};
TypeInfo U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<SearchMusic>c__AnonStoreyB"/* name */
	, ""/* namespaze */
	, U3CSearchMusicU3Ec__AnonStoreyB_t18_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 20/* custom_attributes_cache */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_0_0_0/* byval_arg */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_1_0_0/* this_arg */
	, &U3CSearchMusicU3Ec__AnonStoreyB_t18_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CSearchMusicU3Ec__AnonStoreyB_t18)/* instance_size */
	, sizeof (U3CSearchMusicU3Ec__AnonStoreyB_t18)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler/<SearchSound>c__AnonStoreyC
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchSoundU3Ec__AnonStorey.h"
// Metadata Definition AudioPooler/<SearchSound>c__AnonStoreyC
extern TypeInfo U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo;
// AudioPooler/<SearchSound>c__AnonStoreyC
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchSoundU3Ec__AnonStoreyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler/<SearchSound>c__AnonStoreyC::.ctor()
extern const MethodInfo U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32/* method */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioClip_t20_0_0_0;
static const ParameterInfo U3CSearchSoundU3Ec__AnonStoreyC_t19_U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_ParameterInfos[] = 
{
	{"clip", 0, 134217761, 0, &AudioClip_t20_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean AudioPooler/<SearchSound>c__AnonStoreyC::<>m__6(UnityEngine.AudioClip)
extern const MethodInfo U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_MethodInfo = 
{
	"<>m__6"/* name */
	, (methodPointerType)&U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33/* method */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, U3CSearchSoundU3Ec__AnonStoreyC_t19_U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CSearchSoundU3Ec__AnonStoreyC_t19_MethodInfos[] =
{
	&U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32_MethodInfo,
	&U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CSearchSoundU3Ec__AnonStoreyC_t19_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CSearchSoundU3Ec__AnonStoreyC_t19_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CSearchSoundU3Ec__AnonStoreyC_t19_0_0_0;
extern const Il2CppType U3CSearchSoundU3Ec__AnonStoreyC_t19_1_0_0;
struct U3CSearchSoundU3Ec__AnonStoreyC_t19;
const Il2CppTypeDefinitionMetadata U3CSearchSoundU3Ec__AnonStoreyC_t19_DefinitionMetadata = 
{
	&AudioPooler_t10_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CSearchSoundU3Ec__AnonStoreyC_t19_VTable/* vtableMethods */
	, U3CSearchSoundU3Ec__AnonStoreyC_t19_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 29/* fieldStart */

};
TypeInfo U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<SearchSound>c__AnonStoreyC"/* name */
	, ""/* namespaze */
	, U3CSearchSoundU3Ec__AnonStoreyC_t19_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 21/* custom_attributes_cache */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_0_0_0/* byval_arg */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_1_0_0/* this_arg */
	, &U3CSearchSoundU3Ec__AnonStoreyC_t19_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CSearchSoundU3Ec__AnonStoreyC_t19)/* instance_size */
	, sizeof (U3CSearchSoundU3Ec__AnonStoreyC_t19)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AudioPooler
#include "AssemblyU2DCSharp_AudioPooler.h"
// Metadata Definition AudioPooler
// AudioPooler
#include "AssemblyU2DCSharp_AudioPoolerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::.ctor()
extern const MethodInfo AudioPooler__ctor_m34_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AudioPooler__ctor_m34/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// AudioPooler AudioPooler::get_Instance()
extern const MethodInfo AudioPooler_get_Instance_m35_MethodInfo = 
{
	"get_Instance"/* name */
	, (methodPointerType)&AudioPooler_get_Instance_m35/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &AudioPooler_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::Awake()
extern const MethodInfo AudioPooler_Awake_m36_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&AudioPooler_Awake_m36/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t5_0_0_0;
extern const Il2CppType GameObject_t5_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_CreatePool_m37_ParameterInfos[] = 
{
	{"p_audioContainer", 0, 134217733, 0, &GameObject_t5_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::CreatePool(UnityEngine.GameObject)
extern const MethodInfo AudioPooler_CreatePool_m37_MethodInfo = 
{
	"CreatePool"/* name */
	, (methodPointerType)&AudioPooler_CreatePool_m37/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AudioPooler_t10_AudioPooler_CreatePool_m37_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t5_0_0_0;
extern const Il2CppType SOUNDTYPE_t4_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_GetAudioSource_m38_ParameterInfos[] = 
{
	{"p_audioContainer", 0, 134217734, 0, &GameObject_t5_0_0_0},
	{"p_soundType", 1, 134217735, 0, &SOUNDTYPE_t4_0_0_0},
};
extern const Il2CppType AudioSource_t9_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.AudioSource AudioPooler::GetAudioSource(UnityEngine.GameObject,AudioPooler/SOUNDTYPE)
extern const MethodInfo AudioPooler_GetAudioSource_m38_MethodInfo = 
{
	"GetAudioSource"/* name */
	, (methodPointerType)&AudioPooler_GetAudioSource_m38/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &AudioSource_t9_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, AudioPooler_t10_AudioPooler_GetAudioSource_m38_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType SOUNDTYPE_t4_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_ReturnAudioSource_m39_ParameterInfos[] = 
{
	{"p_currentAudioSource", 0, 134217736, 0, &AudioSource_t9_0_0_0},
	{"p_soundType", 1, 134217737, 0, &SOUNDTYPE_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::ReturnAudioSource(UnityEngine.AudioSource,AudioPooler/SOUNDTYPE)
extern const MethodInfo AudioPooler_ReturnAudioSource_m39_MethodInfo = 
{
	"ReturnAudioSource"/* name */
	, (methodPointerType)&AudioPooler_ReturnAudioSource_m39/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, AudioPooler_t10_AudioPooler_ReturnAudioSource_m39_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t5_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_ClearAudioSource_m40_ParameterInfos[] = 
{
	{"p_audioContainer", 0, 134217738, 0, &GameObject_t5_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::ClearAudioSource(UnityEngine.GameObject)
extern const MethodInfo AudioPooler_ClearAudioSource_m40_MethodInfo = 
{
	"ClearAudioSource"/* name */
	, (methodPointerType)&AudioPooler_ClearAudioSource_m40/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AudioPooler_t10_AudioPooler_ClearAudioSource_m40_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t5_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_ClearMusic_m41_ParameterInfos[] = 
{
	{"p_audioContainer", 0, 134217739, 0, &GameObject_t5_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::ClearMusic(UnityEngine.GameObject)
extern const MethodInfo AudioPooler_ClearMusic_m41_MethodInfo = 
{
	"ClearMusic"/* name */
	, (methodPointerType)&AudioPooler_ClearMusic_m41/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AudioPooler_t10_AudioPooler_ClearMusic_m41_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t5_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_ClearSoundEffects_m42_ParameterInfos[] = 
{
	{"p_audioContainer", 0, 134217740, 0, &GameObject_t5_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::ClearSoundEffects(UnityEngine.GameObject)
extern const MethodInfo AudioPooler_ClearSoundEffects_m42_MethodInfo = 
{
	"ClearSoundEffects"/* name */
	, (methodPointerType)&AudioPooler_ClearSoundEffects_m42/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AudioPooler_t10_AudioPooler_ClearSoundEffects_m42_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_SearchMusic_m43_ParameterInfos[] = 
{
	{"key", 0, 134217741, 0, &String_t_0_0_0},
	{"p_audioSource", 1, 134217742, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 2, 134217743, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::SearchMusic(System.String,UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_SearchMusic_m43_MethodInfo = 
{
	"SearchMusic"/* name */
	, (methodPointerType)&AudioPooler_SearchMusic_m43/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_SearchMusic_m43_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_SearchSound_m44_ParameterInfos[] = 
{
	{"key", 0, 134217744, 0, &String_t_0_0_0},
	{"p_audioSource", 1, 134217745, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 2, 134217746, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::SearchSound(System.String,UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_SearchSound_m44_MethodInfo = 
{
	"SearchSound"/* name */
	, (methodPointerType)&AudioPooler_SearchSound_m44/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_SearchSound_m44_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_LookForMusicSource_m45_ParameterInfos[] = 
{
	{"p_audioSource", 0, 134217747, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 1, 134217748, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::LookForMusicSource(UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_LookForMusicSource_m45_MethodInfo = 
{
	"LookForMusicSource"/* name */
	, (methodPointerType)&AudioPooler_LookForMusicSource_m45/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_LookForMusicSource_m45_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_LookForSoundSource_m46_ParameterInfos[] = 
{
	{"p_audioSource", 0, 134217749, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 1, 134217750, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void AudioPooler::LookForSoundSource(UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_LookForSoundSource_m46_MethodInfo = 
{
	"LookForSoundSource"/* name */
	, (methodPointerType)&AudioPooler_LookForSoundSource_m46/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_LookForSoundSource_m46_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_DisableMusic_m47_ParameterInfos[] = 
{
	{"p_audioSource", 0, 134217751, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 1, 134217752, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator AudioPooler::DisableMusic(UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_DisableMusic_m47_MethodInfo = 
{
	"DisableMusic"/* name */
	, (methodPointerType)&AudioPooler_DisableMusic_m47/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_DisableMusic_m47_ParameterInfos/* parameters */
	, 3/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AudioSource_t9_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AudioPooler_t10_AudioPooler_DisableSound_m48_ParameterInfos[] = 
{
	{"p_audioSource", 0, 134217753, 0, &AudioSource_t9_0_0_0},
	{"p_bisLooping", 1, 134217754, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator AudioPooler::DisableSound(UnityEngine.AudioSource,System.Boolean)
extern const MethodInfo AudioPooler_DisableSound_m48_MethodInfo = 
{
	"DisableSound"/* name */
	, (methodPointerType)&AudioPooler_DisableSound_m48/* method */
	, &AudioPooler_t10_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, AudioPooler_t10_AudioPooler_DisableSound_m48_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AudioPooler_t10_MethodInfos[] =
{
	&AudioPooler__ctor_m34_MethodInfo,
	&AudioPooler_get_Instance_m35_MethodInfo,
	&AudioPooler_Awake_m36_MethodInfo,
	&AudioPooler_CreatePool_m37_MethodInfo,
	&AudioPooler_GetAudioSource_m38_MethodInfo,
	&AudioPooler_ReturnAudioSource_m39_MethodInfo,
	&AudioPooler_ClearAudioSource_m40_MethodInfo,
	&AudioPooler_ClearMusic_m41_MethodInfo,
	&AudioPooler_ClearSoundEffects_m42_MethodInfo,
	&AudioPooler_SearchMusic_m43_MethodInfo,
	&AudioPooler_SearchSound_m44_MethodInfo,
	&AudioPooler_LookForMusicSource_m45_MethodInfo,
	&AudioPooler_LookForSoundSource_m46_MethodInfo,
	&AudioPooler_DisableMusic_m47_MethodInfo,
	&AudioPooler_DisableSound_m48_MethodInfo,
	NULL
};
extern const MethodInfo AudioPooler_get_Instance_m35_MethodInfo;
static const PropertyInfo AudioPooler_t10____Instance_PropertyInfo = 
{
	&AudioPooler_t10_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &AudioPooler_get_Instance_m35_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AudioPooler_t10_PropertyInfos[] =
{
	&AudioPooler_t10____Instance_PropertyInfo,
	NULL
};
static const Il2CppType* AudioPooler_t10_il2cpp_TypeInfo__nestedTypes[11] =
{
	&SOUNDTYPE_t4_0_0_0,
	&objectSource_t8_0_0_0,
	&U3CDisableMusicU3Ec__Iterator0_t11_0_0_0,
	&U3CDisableSoundU3Ec__Iterator1_t12_0_0_0,
	&U3CGetAudioSourceU3Ec__AnonStorey6_t13_0_0_0,
	&U3CReturnAudioSourceU3Ec__AnonStorey7_t14_0_0_0,
	&U3CClearAudioSourceU3Ec__AnonStorey8_t15_0_0_0,
	&U3CClearMusicU3Ec__AnonStorey9_t16_0_0_0,
	&U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_0_0_0,
	&U3CSearchMusicU3Ec__AnonStoreyB_t18_0_0_0,
	&U3CSearchSoundU3Ec__AnonStoreyC_t19_0_0_0,
};
static const Il2CppMethodReference AudioPooler_t10_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
};
static bool AudioPooler_t10_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AudioPooler_t10_1_0_0;
extern const Il2CppType MonoBehaviour_t22_0_0_0;
struct AudioPooler_t10;
const Il2CppTypeDefinitionMetadata AudioPooler_t10_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AudioPooler_t10_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, AudioPooler_t10_VTable/* vtableMethods */
	, AudioPooler_t10_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 30/* fieldStart */

};
TypeInfo AudioPooler_t10_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioPooler"/* name */
	, ""/* namespaze */
	, AudioPooler_t10_MethodInfos/* methods */
	, AudioPooler_t10_PropertyInfos/* properties */
	, NULL/* events */
	, &AudioPooler_t10_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioPooler_t10_0_0_0/* byval_arg */
	, &AudioPooler_t10_1_0_0/* this_arg */
	, &AudioPooler_t10_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioPooler_t10)/* instance_size */
	, sizeof (AudioPooler_t10)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AudioPooler_t10_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 11/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition BaseCanvas`1
extern TypeInfo BaseCanvas_1_t60_il2cpp_TypeInfo;
extern const Il2CppGenericContainer BaseCanvas_1_t60_Il2CppGenericContainer;
extern TypeInfo BaseCanvas_1_t60_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* BaseCanvas_1_t60_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter BaseCanvas_1_t60_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseCanvas_1_t60_Il2CppGenericContainer, BaseCanvas_1_t60_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* BaseCanvas_1_t60_Il2CppGenericParametersArray[1] = 
{
	&BaseCanvas_1_t60_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer BaseCanvas_1_t60_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseCanvas_1_t60_il2cpp_TypeInfo, 1, 0, BaseCanvas_1_t60_Il2CppGenericParametersArray };
// System.Void BaseCanvas`1::.ctor()
extern const MethodInfo BaseCanvas_1__ctor_m158_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseCanvas`1::.cctor()
extern const MethodInfo BaseCanvas_1__cctor_m159_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseCanvas_1_t60_gp_0_0_0_0;
// T BaseCanvas`1::get_Instance()
extern const MethodInfo BaseCanvas_1_get_Instance_m160_MethodInfo = 
{
	"get_Instance"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &BaseCanvas_1_t60_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t30_0_0_0;
extern const Il2CppType Transform_t30_0_0_0;
static const ParameterInfo BaseCanvas_1_t60_BaseCanvas_1_Create_m161_ParameterInfos[] = 
{
	{"p_parent", 0, 134217762, 0, &Transform_t30_0_0_0},
};
// System.Void BaseCanvas`1::Create(UnityEngine.Transform)
extern const MethodInfo BaseCanvas_1_Create_m161_MethodInfo = 
{
	"Create"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseCanvas_1_t60_BaseCanvas_1_Create_m161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseCanvas`1::Awake()
extern const MethodInfo BaseCanvas_1_Awake_m162_MethodInfo = 
{
	"Awake"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseCanvas`1::OnDestroy()
extern const MethodInfo BaseCanvas_1_OnDestroy_m163_MethodInfo = 
{
	"OnDestroy"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseCanvas`1::Open()
extern const MethodInfo BaseCanvas_1_Open_m164_MethodInfo = 
{
	"Open"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseCanvas`1::Close()
extern const MethodInfo BaseCanvas_1_Close_m165_MethodInfo = 
{
	"Close"/* name */
	, NULL/* method */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseCanvas_1_t60_MethodInfos[] =
{
	&BaseCanvas_1__ctor_m158_MethodInfo,
	&BaseCanvas_1__cctor_m159_MethodInfo,
	&BaseCanvas_1_get_Instance_m160_MethodInfo,
	&BaseCanvas_1_Create_m161_MethodInfo,
	&BaseCanvas_1_Awake_m162_MethodInfo,
	&BaseCanvas_1_OnDestroy_m163_MethodInfo,
	&BaseCanvas_1_Open_m164_MethodInfo,
	&BaseCanvas_1_Close_m165_MethodInfo,
	NULL
};
extern const MethodInfo BaseCanvas_1_get_Instance_m160_MethodInfo;
static const PropertyInfo BaseCanvas_1_t60____Instance_PropertyInfo = 
{
	&BaseCanvas_1_t60_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &BaseCanvas_1_get_Instance_m160_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BaseCanvas_1_t60_PropertyInfos[] =
{
	&BaseCanvas_1_t60____Instance_PropertyInfo,
	NULL
};
extern const MethodInfo BaseCanvas_1_Awake_m162_MethodInfo;
extern const MethodInfo BaseCanvas_1_OnDestroy_m163_MethodInfo;
extern const MethodInfo BaseCanvas_1_Open_m164_MethodInfo;
extern const MethodInfo BaseCanvas_1_Close_m165_MethodInfo;
static const Il2CppMethodReference BaseCanvas_1_t60_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&BaseCanvas_1_Awake_m162_MethodInfo,
	&BaseCanvas_1_OnDestroy_m163_MethodInfo,
	&BaseCanvas_1_Open_m164_MethodInfo,
	&BaseCanvas_1_Close_m165_MethodInfo,
};
static bool BaseCanvas_1_t60_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType BaseCanvas_1_t80_0_0_0;
extern const Il2CppGenericMethod Tools_InstantiateCanvas_TisT_t79_m247_GenericMethod;
static Il2CppRGCTXDefinition BaseCanvas_1_t60_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseCanvas_1_t80_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseCanvas_1_t60_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&BaseCanvas_1_t60_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tools_InstantiateCanvas_TisT_t79_m247_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BaseCanvas_1_t60_0_0_0;
extern const Il2CppType BaseCanvas_1_t60_1_0_0;
struct BaseCanvas_1_t60;
const Il2CppTypeDefinitionMetadata BaseCanvas_1_t60_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, BaseCanvas_1_t60_VTable/* vtableMethods */
	, BaseCanvas_1_t60_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, BaseCanvas_1_t60_RGCTXData/* rgctxDefinition */
	, 38/* fieldStart */

};
TypeInfo BaseCanvas_1_t60_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseCanvas`1"/* name */
	, ""/* namespaze */
	, BaseCanvas_1_t60_MethodInfos/* methods */
	, BaseCanvas_1_t60_PropertyInfos/* properties */
	, NULL/* events */
	, &BaseCanvas_1_t60_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 22/* custom_attributes_cache */
	, &BaseCanvas_1_t60_0_0_0/* byval_arg */
	, &BaseCanvas_1_t60_1_0_0/* this_arg */
	, &BaseCanvas_1_t60_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &BaseCanvas_1_t60_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition ICanvasInteractable
extern TypeInfo ICanvasInteractable_t62_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void ICanvasInteractable::Open()
extern const MethodInfo ICanvasInteractable_Open_m166_MethodInfo = 
{
	"Open"/* name */
	, NULL/* method */
	, &ICanvasInteractable_t62_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void ICanvasInteractable::Close()
extern const MethodInfo ICanvasInteractable_Close_m167_MethodInfo = 
{
	"Close"/* name */
	, NULL/* method */
	, &ICanvasInteractable_t62_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ICanvasInteractable_t62_ICanvasInteractable_SetSortingOrder_m168_ParameterInfos[] = 
{
	{"p_order", 0, 134217763, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void ICanvasInteractable::SetSortingOrder(System.Int32)
extern const MethodInfo ICanvasInteractable_SetSortingOrder_m168_MethodInfo = 
{
	"SetSortingOrder"/* name */
	, NULL/* method */
	, &ICanvasInteractable_t62_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ICanvasInteractable_t62_ICanvasInteractable_SetSortingOrder_m168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICanvasInteractable_t62_MethodInfos[] =
{
	&ICanvasInteractable_Open_m166_MethodInfo,
	&ICanvasInteractable_Close_m167_MethodInfo,
	&ICanvasInteractable_SetSortingOrder_m168_MethodInfo,
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ICanvasInteractable_t62_0_0_0;
extern const Il2CppType ICanvasInteractable_t62_1_0_0;
struct ICanvasInteractable_t62;
const Il2CppTypeDefinitionMetadata ICanvasInteractable_t62_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICanvasInteractable_t62_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICanvasInteractable"/* name */
	, ""/* namespaze */
	, ICanvasInteractable_t62_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICanvasInteractable_t62_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICanvasInteractable_t62_0_0_0/* byval_arg */
	, &ICanvasInteractable_t62_1_0_0/* this_arg */
	, &ICanvasInteractable_t62_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2
extern TypeInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericContainer;
extern TypeInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericContainer, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericParametersArray[1] = 
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo, 1, 0, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericParametersArray };
// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::.ctor()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m182_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 27/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 28/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::MoveNext()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m185_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::Dispose()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 29/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2::Reset()
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 30/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_MethodInfos[] =
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m182_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m185_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187_MethodInfo,
	NULL
};
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183_MethodInfo;
static const PropertyInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184_MethodInfo;
static const PropertyInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_PropertyInfos[] =
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186_MethodInfo;
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m185_MethodInfo;
extern const MethodInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187_MethodInfo;
static const Il2CppMethodReference U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m185_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183_MethodInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187_MethodInfo,
};
static bool U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern const Il2CppGenericMethod BaseOverlay_1_Close_m248_GenericMethod;
static Il2CppRGCTXDefinition U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_Close_m248_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_0_0_0;
extern const Il2CppType U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_1_0_0;
extern TypeInfo BaseOverlay_1_t63_il2cpp_TypeInfo;
extern const Il2CppType BaseOverlay_1_t63_0_0_0;
struct U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64;
const Il2CppTypeDefinitionMetadata U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_DefinitionMetadata = 
{
	&BaseOverlay_1_t63_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_VTable/* vtableMethods */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_RGCTXData/* rgctxDefinition */
	, 41/* fieldStart */

};
TypeInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CloseWithDelayCoroutine>c__Iterator2"/* name */
	, ""/* namespaze */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_MethodInfos/* methods */
	, U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 26/* custom_attributes_cache */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_0_0_0/* byval_arg */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_1_0_0/* this_arg */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition BaseOverlay`1/<FadeInCoroutine>c__Iterator3
extern TypeInfo U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericContainer;
extern TypeInfo U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericContainer, U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericParametersArray[1] = 
{
	&U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo, 1, 0, U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericParametersArray };
// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3::.ctor()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3__ctor_m188_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<FadeInCoroutine>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 32/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<FadeInCoroutine>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 33/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean BaseOverlay`1/<FadeInCoroutine>c__Iterator3::MoveNext()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m191_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3::Dispose()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 34/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3::Reset()
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 35/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CFadeInCoroutineU3Ec__Iterator3_t65_MethodInfos[] =
{
	&U3CFadeInCoroutineU3Ec__Iterator3__ctor_m188_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m191_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193_MethodInfo,
	NULL
};
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189_MethodInfo;
static const PropertyInfo U3CFadeInCoroutineU3Ec__Iterator3_t65____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190_MethodInfo;
static const PropertyInfo U3CFadeInCoroutineU3Ec__Iterator3_t65____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CFadeInCoroutineU3Ec__Iterator3_t65_PropertyInfos[] =
{
	&U3CFadeInCoroutineU3Ec__Iterator3_t65____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_t65____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192_MethodInfo;
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m191_MethodInfo;
extern const MethodInfo U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193_MethodInfo;
static const Il2CppMethodReference U3CFadeInCoroutineU3Ec__Iterator3_t65_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m191_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189_MethodInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193_MethodInfo,
};
static bool U3CFadeInCoroutineU3Ec__Iterator3_t65_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CFadeInCoroutineU3Ec__Iterator3_t65_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFadeInCoroutineU3Ec__Iterator3_t65_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CFadeInCoroutineU3Ec__Iterator3_t65_0_0_0;
extern const Il2CppType U3CFadeInCoroutineU3Ec__Iterator3_t65_1_0_0;
struct U3CFadeInCoroutineU3Ec__Iterator3_t65;
const Il2CppTypeDefinitionMetadata U3CFadeInCoroutineU3Ec__Iterator3_t65_DefinitionMetadata = 
{
	&BaseOverlay_1_t63_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_VTable/* vtableMethods */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 46/* fieldStart */

};
TypeInfo U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FadeInCoroutine>c__Iterator3"/* name */
	, ""/* namespaze */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_MethodInfos/* methods */
	, U3CFadeInCoroutineU3Ec__Iterator3_t65_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 31/* custom_attributes_cache */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_0_0_0/* byval_arg */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_1_0_0/* this_arg */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CFadeInCoroutineU3Ec__Iterator3_t65_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition BaseOverlay`1/<FadeOutCoroutine>c__Iterator4
extern TypeInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericContainer;
extern TypeInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericContainer, U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericParametersArray[1] = 
{
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo, 1, 0, U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericParametersArray };
// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::.ctor()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m194_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 37/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 38/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::MoveNext()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m197_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::Dispose()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 39/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1/<FadeOutCoroutine>c__Iterator4::Reset()
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 40/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CFadeOutCoroutineU3Ec__Iterator4_t66_MethodInfos[] =
{
	&U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m194_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m197_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199_MethodInfo,
	NULL
};
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195_MethodInfo;
static const PropertyInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196_MethodInfo;
static const PropertyInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CFadeOutCoroutineU3Ec__Iterator4_t66_PropertyInfos[] =
{
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198_MethodInfo;
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m197_MethodInfo;
extern const MethodInfo U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199_MethodInfo;
static const Il2CppMethodReference U3CFadeOutCoroutineU3Ec__Iterator4_t66_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m197_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195_MethodInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199_MethodInfo,
};
static bool U3CFadeOutCoroutineU3Ec__Iterator4_t66_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CFadeOutCoroutineU3Ec__Iterator4_t66_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFadeOutCoroutineU3Ec__Iterator4_t66_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern const Il2CppGenericMethod BaseOverlay_1_OnClose_m249_GenericMethod;
static Il2CppRGCTXDefinition U3CFadeOutCoroutineU3Ec__Iterator4_t66_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_OnClose_m249_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CFadeOutCoroutineU3Ec__Iterator4_t66_0_0_0;
extern const Il2CppType U3CFadeOutCoroutineU3Ec__Iterator4_t66_1_0_0;
struct U3CFadeOutCoroutineU3Ec__Iterator4_t66;
const Il2CppTypeDefinitionMetadata U3CFadeOutCoroutineU3Ec__Iterator4_t66_DefinitionMetadata = 
{
	&BaseOverlay_1_t63_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_VTable/* vtableMethods */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_RGCTXData/* rgctxDefinition */
	, 52/* fieldStart */

};
TypeInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FadeOutCoroutine>c__Iterator4"/* name */
	, ""/* namespaze */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_MethodInfos/* methods */
	, U3CFadeOutCoroutineU3Ec__Iterator4_t66_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 36/* custom_attributes_cache */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_0_0_0/* byval_arg */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_1_0_0/* this_arg */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CFadeOutCoroutineU3Ec__Iterator4_t66_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition BaseOverlay`1
extern const Il2CppGenericContainer BaseOverlay_1_t63_Il2CppGenericContainer;
extern TypeInfo BaseOverlay_1_t63_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* BaseOverlay_1_t63_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter BaseOverlay_1_t63_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseOverlay_1_t63_Il2CppGenericContainer, BaseOverlay_1_t63_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* BaseOverlay_1_t63_Il2CppGenericParametersArray[1] = 
{
	&BaseOverlay_1_t63_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer BaseOverlay_1_t63_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseOverlay_1_t63_il2cpp_TypeInfo, 1, 0, BaseOverlay_1_t63_Il2CppGenericParametersArray };
// System.Void BaseOverlay`1::.ctor()
extern const MethodInfo BaseOverlay_1__ctor_m169_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::Awake()
extern const MethodInfo BaseOverlay_1_Awake_m170_MethodInfo = 
{
	"Awake"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::Open()
extern const MethodInfo BaseOverlay_1_Open_m171_MethodInfo = 
{
	"Open"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::Close()
extern const MethodInfo BaseOverlay_1_Close_m172_MethodInfo = 
{
	"Close"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::OnClose()
extern const MethodInfo BaseOverlay_1_OnClose_m173_MethodInfo = 
{
	"OnClose"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::CancelClose()
extern const MethodInfo BaseOverlay_1_CancelClose_m174_MethodInfo = 
{
	"CancelClose"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo BaseOverlay_1_t63_BaseOverlay_1_CloseWithDelay_m175_ParameterInfos[] = 
{
	{"p_delay", 0, 134217764, 0, &Single_t85_0_0_0},
};
// System.Void BaseOverlay`1::CloseWithDelay(System.Single)
extern const MethodInfo BaseOverlay_1_CloseWithDelay_m175_MethodInfo = 
{
	"CloseWithDelay"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseOverlay_1_t63_BaseOverlay_1_CloseWithDelay_m175_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo BaseOverlay_1_t63_BaseOverlay_1_CloseWithDelayCoroutine_m176_ParameterInfos[] = 
{
	{"p_delay", 0, 134217765, 0, &Single_t85_0_0_0},
};
// System.Collections.IEnumerator BaseOverlay`1::CloseWithDelayCoroutine(System.Single)
extern const MethodInfo BaseOverlay_1_CloseWithDelayCoroutine_m176_MethodInfo = 
{
	"CloseWithDelayCoroutine"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseOverlay_1_t63_BaseOverlay_1_CloseWithDelayCoroutine_m176_ParameterInfos/* parameters */
	, 23/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo BaseOverlay_1_t63_BaseOverlay_1_SetSortingOrder_m177_ParameterInfos[] = 
{
	{"p_order", 0, 134217766, 0, &Int32_t54_0_0_0},
};
// System.Void BaseOverlay`1::SetSortingOrder(System.Int32)
extern const MethodInfo BaseOverlay_1_SetSortingOrder_m177_MethodInfo = 
{
	"SetSortingOrder"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseOverlay_1_t63_BaseOverlay_1_SetSortingOrder_m177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::FadeIn()
extern const MethodInfo BaseOverlay_1_FadeIn_m178_MethodInfo = 
{
	"FadeIn"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void BaseOverlay`1::FadeOut()
extern const MethodInfo BaseOverlay_1_FadeOut_m179_MethodInfo = 
{
	"FadeOut"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo BaseOverlay_1_t63_BaseOverlay_1_FadeInCoroutine_m180_ParameterInfos[] = 
{
	{"duration", 0, 134217767, 0, &Single_t85_0_0_0},
};
// System.Collections.IEnumerator BaseOverlay`1::FadeInCoroutine(System.Single)
extern const MethodInfo BaseOverlay_1_FadeInCoroutine_m180_MethodInfo = 
{
	"FadeInCoroutine"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseOverlay_1_t63_BaseOverlay_1_FadeInCoroutine_m180_ParameterInfos/* parameters */
	, 24/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo BaseOverlay_1_t63_BaseOverlay_1_FadeOutCoroutine_m181_ParameterInfos[] = 
{
	{"duration", 0, 134217768, 0, &Single_t85_0_0_0},
};
// System.Collections.IEnumerator BaseOverlay`1::FadeOutCoroutine(System.Single)
extern const MethodInfo BaseOverlay_1_FadeOutCoroutine_m181_MethodInfo = 
{
	"FadeOutCoroutine"/* name */
	, NULL/* method */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseOverlay_1_t63_BaseOverlay_1_FadeOutCoroutine_m181_ParameterInfos/* parameters */
	, 25/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseOverlay_1_t63_MethodInfos[] =
{
	&BaseOverlay_1__ctor_m169_MethodInfo,
	&BaseOverlay_1_Awake_m170_MethodInfo,
	&BaseOverlay_1_Open_m171_MethodInfo,
	&BaseOverlay_1_Close_m172_MethodInfo,
	&BaseOverlay_1_OnClose_m173_MethodInfo,
	&BaseOverlay_1_CancelClose_m174_MethodInfo,
	&BaseOverlay_1_CloseWithDelay_m175_MethodInfo,
	&BaseOverlay_1_CloseWithDelayCoroutine_m176_MethodInfo,
	&BaseOverlay_1_SetSortingOrder_m177_MethodInfo,
	&BaseOverlay_1_FadeIn_m178_MethodInfo,
	&BaseOverlay_1_FadeOut_m179_MethodInfo,
	&BaseOverlay_1_FadeInCoroutine_m180_MethodInfo,
	&BaseOverlay_1_FadeOutCoroutine_m181_MethodInfo,
	NULL
};
static const Il2CppType* BaseOverlay_1_t63_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_0_0_0,
	&U3CFadeInCoroutineU3Ec__Iterator3_t65_0_0_0,
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66_0_0_0,
};
extern const MethodInfo BaseOverlay_1_Awake_m170_MethodInfo;
extern const Il2CppGenericMethod BaseCanvas_1_OnDestroy_m250_GenericMethod;
extern const MethodInfo BaseOverlay_1_Open_m171_MethodInfo;
extern const MethodInfo BaseOverlay_1_Close_m172_MethodInfo;
extern const MethodInfo BaseOverlay_1_SetSortingOrder_m177_MethodInfo;
extern const MethodInfo BaseOverlay_1_FadeIn_m178_MethodInfo;
extern const MethodInfo BaseOverlay_1_FadeOut_m179_MethodInfo;
static const Il2CppMethodReference BaseOverlay_1_t63_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&BaseOverlay_1_Awake_m170_MethodInfo,
	&BaseCanvas_1_OnDestroy_m250_GenericMethod,
	&BaseOverlay_1_Open_m171_MethodInfo,
	&BaseOverlay_1_Close_m172_MethodInfo,
	&BaseOverlay_1_Open_m171_MethodInfo,
	&BaseOverlay_1_Close_m172_MethodInfo,
	&BaseOverlay_1_SetSortingOrder_m177_MethodInfo,
	&BaseOverlay_1_SetSortingOrder_m177_MethodInfo,
	&BaseOverlay_1_FadeIn_m178_MethodInfo,
	&BaseOverlay_1_FadeOut_m179_MethodInfo,
};
static bool BaseOverlay_1_t63_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* BaseOverlay_1_t63_InterfacesTypeInfos[] = 
{
	&ICanvasInteractable_t62_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseOverlay_1_t63_InterfacesOffsets[] = 
{
	{ &ICanvasInteractable_t62_0_0_0, 8},
};
extern const Il2CppGenericMethod BaseCanvas_1__ctor_m251_GenericMethod;
extern const Il2CppGenericMethod BaseCanvas_1_Awake_m252_GenericMethod;
extern const Il2CppGenericMethod BaseCanvas_1_Open_m253_GenericMethod;
extern const Il2CppGenericMethod BaseOverlay_1_FadeIn_m254_GenericMethod;
extern const Il2CppGenericMethod BaseOverlay_1_FadeOut_m255_GenericMethod;
extern const Il2CppGenericMethod BaseOverlay_1_CloseWithDelayCoroutine_m256_GenericMethod;
extern const Il2CppType U3CCloseWithDelayCoroutineU3Ec__Iterator2_t86_0_0_0;
extern const Il2CppGenericMethod U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m257_GenericMethod;
extern const Il2CppGenericMethod BaseOverlay_1_FadeInCoroutine_m258_GenericMethod;
extern const Il2CppGenericMethod BaseOverlay_1_FadeOutCoroutine_m259_GenericMethod;
extern const Il2CppType U3CFadeInCoroutineU3Ec__Iterator3_t87_0_0_0;
extern const Il2CppGenericMethod U3CFadeInCoroutineU3Ec__Iterator3__ctor_m260_GenericMethod;
extern const Il2CppType U3CFadeOutCoroutineU3Ec__Iterator4_t88_0_0_0;
extern const Il2CppGenericMethod U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m261_GenericMethod;
static Il2CppRGCTXDefinition BaseOverlay_1_t63_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseCanvas_1__ctor_m251_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseCanvas_1_Awake_m252_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseCanvas_1_Open_m253_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_FadeIn_m254_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_FadeOut_m255_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_CloseWithDelayCoroutine_m256_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t86_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m257_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_FadeInCoroutine_m258_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseOverlay_1_FadeOutCoroutine_m259_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CFadeInCoroutineU3Ec__Iterator3_t87_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CFadeInCoroutineU3Ec__Iterator3__ctor_m260_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CFadeOutCoroutineU3Ec__Iterator4_t88_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m261_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BaseOverlay_1_t63_1_0_0;
extern const Il2CppType BaseCanvas_1_t89_0_0_0;
struct BaseOverlay_1_t63;
const Il2CppTypeDefinitionMetadata BaseOverlay_1_t63_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BaseOverlay_1_t63_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, BaseOverlay_1_t63_InterfacesTypeInfos/* implementedInterfaces */
	, BaseOverlay_1_t63_InterfacesOffsets/* interfaceOffsets */
	, &BaseCanvas_1_t89_0_0_0/* parent */
	, BaseOverlay_1_t63_VTable/* vtableMethods */
	, BaseOverlay_1_t63_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, BaseOverlay_1_t63_RGCTXData/* rgctxDefinition */
	, 58/* fieldStart */

};
TypeInfo BaseOverlay_1_t63_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseOverlay`1"/* name */
	, ""/* namespaze */
	, BaseOverlay_1_t63_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseOverlay_1_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseOverlay_1_t63_0_0_0/* byval_arg */
	, &BaseOverlay_1_t63_1_0_0/* this_arg */
	, &BaseOverlay_1_t63_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &BaseOverlay_1_t63_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// BoardController
#include "AssemblyU2DCSharp_BoardController.h"
// Metadata Definition BoardController
extern TypeInfo BoardController_t23_il2cpp_TypeInfo;
// BoardController
#include "AssemblyU2DCSharp_BoardControllerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void BoardController::.ctor()
extern const MethodInfo BoardController__ctor_m49_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BoardController__ctor_m49/* method */
	, &BoardController_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void BoardController::Awake()
extern const MethodInfo BoardController_Awake_m50_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&BoardController_Awake_m50/* method */
	, &BoardController_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void BoardController::Stop()
extern const MethodInfo BoardController_Stop_m51_MethodInfo = 
{
	"Stop"/* name */
	, (methodPointerType)&BoardController_Stop_m51/* method */
	, &BoardController_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo BoardController_t23_BoardController_PlaySound_m52_ParameterInfos[] = 
{
	{"sound", 0, 134217769, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void BoardController::PlaySound(System.Int32)
extern const MethodInfo BoardController_PlaySound_m52_MethodInfo = 
{
	"PlaySound"/* name */
	, (methodPointerType)&BoardController_PlaySound_m52/* method */
	, &BoardController_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, BoardController_t23_BoardController_PlaySound_m52_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BoardController_t23_MethodInfos[] =
{
	&BoardController__ctor_m49_MethodInfo,
	&BoardController_Awake_m50_MethodInfo,
	&BoardController_Stop_m51_MethodInfo,
	&BoardController_PlaySound_m52_MethodInfo,
	NULL
};
static const Il2CppMethodReference BoardController_t23_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
};
static bool BoardController_t23_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BoardController_t23_0_0_0;
extern const Il2CppType BoardController_t23_1_0_0;
struct BoardController_t23;
const Il2CppTypeDefinitionMetadata BoardController_t23_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, BoardController_t23_VTable/* vtableMethods */
	, BoardController_t23_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BoardController_t23_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BoardController"/* name */
	, ""/* namespaze */
	, BoardController_t23_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BoardController_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BoardController_t23_0_0_0/* byval_arg */
	, &BoardController_t23_1_0_0/* this_arg */
	, &BoardController_t23_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BoardController_t23)/* instance_size */
	, sizeof (BoardController_t23)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DebugUtil
#include "AssemblyU2DCSharp_DebugUtil.h"
// Metadata Definition DebugUtil
extern TypeInfo DebugUtil_t24_il2cpp_TypeInfo;
// DebugUtil
#include "AssemblyU2DCSharp_DebugUtilMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::.ctor()
extern const MethodInfo DebugUtil__ctor_m53_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DebugUtil__ctor_m53/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::.cctor()
extern const MethodInfo DebugUtil__cctor_m54_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DebugUtil__cctor_m54/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_log_m55_ParameterInfos[] = 
{
	{"format", 0, 134217770, 0, &Object_t_0_0_0},
	{"paramList", 1, 134217771, 42, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::log(System.Object,System.Object[])
extern const MethodInfo DebugUtil_log_m55_MethodInfo = 
{
	"log"/* name */
	, (methodPointerType)&DebugUtil_log_m55/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, DebugUtil_t24_DebugUtil_log_m55_ParameterInfos/* parameters */
	, 41/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_warn_m56_ParameterInfos[] = 
{
	{"format", 0, 134217772, 0, &Object_t_0_0_0},
	{"paramList", 1, 134217773, 44, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::warn(System.Object,System.Object[])
extern const MethodInfo DebugUtil_warn_m56_MethodInfo = 
{
	"warn"/* name */
	, (methodPointerType)&DebugUtil_warn_m56/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, DebugUtil_t24_DebugUtil_warn_m56_ParameterInfos/* parameters */
	, 43/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_error_m57_ParameterInfos[] = 
{
	{"format", 0, 134217774, 0, &Object_t_0_0_0},
	{"paramList", 1, 134217775, 46, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::error(System.Object,System.Object[])
extern const MethodInfo DebugUtil_error_m57_MethodInfo = 
{
	"error"/* name */
	, (methodPointerType)&DebugUtil_error_m57/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, DebugUtil_t24_DebugUtil_error_m57_ParameterInfos/* parameters */
	, 45/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_assert_m58_ParameterInfos[] = 
{
	{"condition", 0, 134217776, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::assert(System.Boolean)
extern const MethodInfo DebugUtil_assert_m58_MethodInfo = 
{
	"assert"/* name */
	, (methodPointerType)&DebugUtil_assert_m58/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, DebugUtil_t24_DebugUtil_assert_m58_ParameterInfos/* parameters */
	, 47/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_assert_m59_ParameterInfos[] = 
{
	{"condition", 0, 134217777, 0, &Boolean_t72_0_0_0},
	{"assertString", 1, 134217778, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::assert(System.Boolean,System.String)
extern const MethodInfo DebugUtil_assert_m59_MethodInfo = 
{
	"assert"/* name */
	, (methodPointerType)&DebugUtil_assert_m59/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_Object_t/* invoker_method */
	, DebugUtil_t24_DebugUtil_assert_m59_ParameterInfos/* parameters */
	, 48/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_assert_m60_ParameterInfos[] = 
{
	{"condition", 0, 134217779, 0, &Boolean_t72_0_0_0},
	{"assertString", 1, 134217780, 0, &String_t_0_0_0},
	{"pauseOnFail", 2, 134217781, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::assert(System.Boolean,System.String,System.Boolean)
extern const MethodInfo DebugUtil_assert_m60_MethodInfo = 
{
	"assert"/* name */
	, (methodPointerType)&DebugUtil_assert_m60/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_Object_t_SByte_t73/* invoker_method */
	, DebugUtil_t24_DebugUtil_assert_m60_ParameterInfos/* parameters */
	, 49/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_Log_m61_ParameterInfos[] = 
{
	{"p_s", 0, 134217782, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::Log(System.String)
extern const MethodInfo DebugUtil_Log_m61_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&DebugUtil_Log_m61/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, DebugUtil_t24_DebugUtil_Log_m61_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType LogType_t90_0_0_0;
extern const Il2CppType LogType_t90_0_0_0;
static const ParameterInfo DebugUtil_t24_DebugUtil_LogMessage_m62_ParameterInfos[] = 
{
	{"p_s", 0, 134217783, 0, &String_t_0_0_0},
	{"p_logType", 1, 134217784, 0, &LogType_t90_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void DebugUtil::LogMessage(System.String,UnityEngine.LogType)
extern const MethodInfo DebugUtil_LogMessage_m62_MethodInfo = 
{
	"LogMessage"/* name */
	, (methodPointerType)&DebugUtil_LogMessage_m62/* method */
	, &DebugUtil_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, DebugUtil_t24_DebugUtil_LogMessage_m62_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DebugUtil_t24_MethodInfos[] =
{
	&DebugUtil__ctor_m53_MethodInfo,
	&DebugUtil__cctor_m54_MethodInfo,
	&DebugUtil_log_m55_MethodInfo,
	&DebugUtil_warn_m56_MethodInfo,
	&DebugUtil_error_m57_MethodInfo,
	&DebugUtil_assert_m58_MethodInfo,
	&DebugUtil_assert_m59_MethodInfo,
	&DebugUtil_assert_m60_MethodInfo,
	&DebugUtil_Log_m61_MethodInfo,
	&DebugUtil_LogMessage_m62_MethodInfo,
	NULL
};
static const Il2CppMethodReference DebugUtil_t24_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool DebugUtil_t24_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DebugUtil_t24_0_0_0;
extern const Il2CppType DebugUtil_t24_1_0_0;
struct DebugUtil_t24;
const Il2CppTypeDefinitionMetadata DebugUtil_t24_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DebugUtil_t24_VTable/* vtableMethods */
	, DebugUtil_t24_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 59/* fieldStart */

};
TypeInfo DebugUtil_t24_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebugUtil"/* name */
	, ""/* namespaze */
	, DebugUtil_t24_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DebugUtil_t24_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DebugUtil_t24_0_0_0/* byval_arg */
	, &DebugUtil_t24_1_0_0/* this_arg */
	, &DebugUtil_t24_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebugUtil_t24)/* instance_size */
	, sizeof (DebugUtil_t24)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DebugUtil_t24_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetup.h"
// Metadata Definition DefiniteCasinoCanvasSetup
extern TypeInfo DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo;
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void DefiniteCasinoCanvasSetup::.ctor()
extern const MethodInfo DefiniteCasinoCanvasSetup__ctor_m63_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefiniteCasinoCanvasSetup__ctor_m63/* method */
	, &DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void DefiniteCasinoCanvasSetup::Awake()
extern const MethodInfo DefiniteCasinoCanvasSetup_Awake_m64_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DefiniteCasinoCanvasSetup_Awake_m64/* method */
	, &DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void DefiniteCasinoCanvasSetup::ResetToDefiniteCasinoSetup()
extern const MethodInfo DefiniteCasinoCanvasSetup_ResetToDefiniteCasinoSetup_m65_MethodInfo = 
{
	"ResetToDefiniteCasinoSetup"/* name */
	, (methodPointerType)&DefiniteCasinoCanvasSetup_ResetToDefiniteCasinoSetup_m65/* method */
	, &DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefiniteCasinoCanvasSetup_t25_MethodInfos[] =
{
	&DefiniteCasinoCanvasSetup__ctor_m63_MethodInfo,
	&DefiniteCasinoCanvasSetup_Awake_m64_MethodInfo,
	&DefiniteCasinoCanvasSetup_ResetToDefiniteCasinoSetup_m65_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefiniteCasinoCanvasSetup_t25_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
};
static bool DefiniteCasinoCanvasSetup_t25_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DefiniteCasinoCanvasSetup_t25_0_0_0;
extern const Il2CppType DefiniteCasinoCanvasSetup_t25_1_0_0;
struct DefiniteCasinoCanvasSetup_t25;
const Il2CppTypeDefinitionMetadata DefiniteCasinoCanvasSetup_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, DefiniteCasinoCanvasSetup_t25_VTable/* vtableMethods */
	, DefiniteCasinoCanvasSetup_t25_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefiniteCasinoCanvasSetup"/* name */
	, ""/* namespaze */
	, DefiniteCasinoCanvasSetup_t25_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 50/* custom_attributes_cache */
	, &DefiniteCasinoCanvasSetup_t25_0_0_0/* byval_arg */
	, &DefiniteCasinoCanvasSetup_t25_1_0_0/* this_arg */
	, &DefiniteCasinoCanvasSetup_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefiniteCasinoCanvasSetup_t25)/* instance_size */
	, sizeof (DefiniteCasinoCanvasSetup_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition MonoManager`1
extern TypeInfo MonoManager_1_t69_il2cpp_TypeInfo;
extern const Il2CppGenericContainer MonoManager_1_t69_Il2CppGenericContainer;
extern TypeInfo MonoManager_1_t69_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* MonoManager_1_t69_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter MonoManager_1_t69_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &MonoManager_1_t69_Il2CppGenericContainer, MonoManager_1_t69_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* MonoManager_1_t69_Il2CppGenericParametersArray[1] = 
{
	&MonoManager_1_t69_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer MonoManager_1_t69_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MonoManager_1_t69_il2cpp_TypeInfo, 1, 0, MonoManager_1_t69_Il2CppGenericParametersArray };
// System.Void MonoManager`1::.ctor()
extern const MethodInfo MonoManager_1__ctor_m202_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void MonoManager`1::.cctor()
extern const MethodInfo MonoManager_1__cctor_m203_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoManager_1_t69_gp_0_0_0_0;
// T MonoManager`1::get_Instance()
extern const MethodInfo MonoManager_1_get_Instance_m204_MethodInfo = 
{
	"get_Instance"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &MonoManager_1_t69_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t30_0_0_4112;
static const ParameterInfo MonoManager_1_t69_MonoManager_1_Create_m205_ParameterInfos[] = 
{
	{"p_parent", 0, 134217785, 0, &Transform_t30_0_0_4112},
};
// System.Void MonoManager`1::Create(UnityEngine.Transform)
extern const MethodInfo MonoManager_1_Create_m205_MethodInfo = 
{
	"Create"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MonoManager_1_t69_MonoManager_1_Create_m205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void MonoManager`1::Awake()
extern const MethodInfo MonoManager_1_Awake_m206_MethodInfo = 
{
	"Awake"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void MonoManager`1::OnDestroy()
extern const MethodInfo MonoManager_1_OnDestroy_m207_MethodInfo = 
{
	"OnDestroy"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void MonoManager`1::Destroy()
extern const MethodInfo MonoManager_1_Destroy_m208_MethodInfo = 
{
	"Destroy"/* name */
	, NULL/* method */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoManager_1_t69_MethodInfos[] =
{
	&MonoManager_1__ctor_m202_MethodInfo,
	&MonoManager_1__cctor_m203_MethodInfo,
	&MonoManager_1_get_Instance_m204_MethodInfo,
	&MonoManager_1_Create_m205_MethodInfo,
	&MonoManager_1_Awake_m206_MethodInfo,
	&MonoManager_1_OnDestroy_m207_MethodInfo,
	&MonoManager_1_Destroy_m208_MethodInfo,
	NULL
};
extern const MethodInfo MonoManager_1_get_Instance_m204_MethodInfo;
static const PropertyInfo MonoManager_1_t69____Instance_PropertyInfo = 
{
	&MonoManager_1_t69_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &MonoManager_1_get_Instance_m204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoManager_1_t69_PropertyInfos[] =
{
	&MonoManager_1_t69____Instance_PropertyInfo,
	NULL
};
extern const MethodInfo MonoManager_1_Awake_m206_MethodInfo;
extern const MethodInfo MonoManager_1_OnDestroy_m207_MethodInfo;
static const Il2CppMethodReference MonoManager_1_t69_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&MonoManager_1_Awake_m206_MethodInfo,
	&MonoManager_1_OnDestroy_m207_MethodInfo,
};
static bool MonoManager_1_t69_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType MonoManager_1_t92_0_0_0;
extern const Il2CppGenericMethod Tools_InstantiateManager_TisT_t91_m262_GenericMethod;
static Il2CppRGCTXDefinition MonoManager_1_t69_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoManager_1_t92_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoManager_1_t69_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&MonoManager_1_t69_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tools_InstantiateManager_TisT_t91_m262_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MonoManager_1_t69_0_0_0;
extern const Il2CppType MonoManager_1_t69_1_0_0;
struct MonoManager_1_t69;
const Il2CppTypeDefinitionMetadata MonoManager_1_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, MonoManager_1_t69_VTable/* vtableMethods */
	, MonoManager_1_t69_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, MonoManager_1_t69_RGCTXData/* rgctxDefinition */
	, 61/* fieldStart */

};
TypeInfo MonoManager_1_t69_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoManager`1"/* name */
	, ""/* namespaze */
	, MonoManager_1_t69_MethodInfos/* methods */
	, MonoManager_1_t69_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoManager_1_t69_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoManager_1_t69_0_0_0/* byval_arg */
	, &MonoManager_1_t69_1_0_0/* this_arg */
	, &MonoManager_1_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &MonoManager_1_t69_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition NonMonoManager`1
extern TypeInfo NonMonoManager_1_t70_il2cpp_TypeInfo;
extern const Il2CppGenericContainer NonMonoManager_1_t70_Il2CppGenericContainer;
extern TypeInfo NonMonoManager_1_t70_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter NonMonoManager_1_t70_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &NonMonoManager_1_t70_Il2CppGenericContainer, NULL, "T", 0, 20 };
static const Il2CppGenericParameter* NonMonoManager_1_t70_Il2CppGenericParametersArray[1] = 
{
	&NonMonoManager_1_t70_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer NonMonoManager_1_t70_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&NonMonoManager_1_t70_il2cpp_TypeInfo, 1, 0, NonMonoManager_1_t70_Il2CppGenericParametersArray };
// System.Void NonMonoManager`1::.ctor()
extern const MethodInfo NonMonoManager_1__ctor_m209_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void NonMonoManager`1::.cctor()
extern const MethodInfo NonMonoManager_1__cctor_m210_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NonMonoManager_1_t70_gp_0_0_0_0;
// T NonMonoManager`1::get_Instance()
extern const MethodInfo NonMonoManager_1_get_Instance_m211_MethodInfo = 
{
	"get_Instance"/* name */
	, NULL/* method */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* declaring_type */
	, &NonMonoManager_1_t70_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void NonMonoManager`1::Create()
extern const MethodInfo NonMonoManager_1_Create_m212_MethodInfo = 
{
	"Create"/* name */
	, NULL/* method */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void NonMonoManager`1::Destroy()
extern const MethodInfo NonMonoManager_1_Destroy_m213_MethodInfo = 
{
	"Destroy"/* name */
	, NULL/* method */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonMonoManager_1_t70_MethodInfos[] =
{
	&NonMonoManager_1__ctor_m209_MethodInfo,
	&NonMonoManager_1__cctor_m210_MethodInfo,
	&NonMonoManager_1_get_Instance_m211_MethodInfo,
	&NonMonoManager_1_Create_m212_MethodInfo,
	&NonMonoManager_1_Destroy_m213_MethodInfo,
	NULL
};
extern const MethodInfo NonMonoManager_1_get_Instance_m211_MethodInfo;
static const PropertyInfo NonMonoManager_1_t70____Instance_PropertyInfo = 
{
	&NonMonoManager_1_t70_il2cpp_TypeInfo/* parent */
	, "Instance"/* name */
	, &NonMonoManager_1_get_Instance_m211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NonMonoManager_1_t70_PropertyInfos[] =
{
	&NonMonoManager_1_t70____Instance_PropertyInfo,
	NULL
};
extern const MethodInfo NonMonoManager_1_Destroy_m213_MethodInfo;
static const Il2CppMethodReference NonMonoManager_1_t70_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&NonMonoManager_1_Destroy_m213_MethodInfo,
};
static bool NonMonoManager_1_t70_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType NonMonoManager_1_t94_0_0_0;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t93_m263_GenericMethod;
static Il2CppRGCTXDefinition NonMonoManager_1_t70_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&NonMonoManager_1_t94_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&NonMonoManager_1_t70_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t93_m263_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&NonMonoManager_1_t70_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType NonMonoManager_1_t70_0_0_0;
extern const Il2CppType NonMonoManager_1_t70_1_0_0;
struct NonMonoManager_1_t70;
const Il2CppTypeDefinitionMetadata NonMonoManager_1_t70_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NonMonoManager_1_t70_VTable/* vtableMethods */
	, NonMonoManager_1_t70_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NonMonoManager_1_t70_RGCTXData/* rgctxDefinition */
	, 62/* fieldStart */

};
TypeInfo NonMonoManager_1_t70_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonMonoManager`1"/* name */
	, ""/* namespaze */
	, NonMonoManager_1_t70_MethodInfos/* methods */
	, NonMonoManager_1_t70_PropertyInfos/* properties */
	, NULL/* events */
	, &NonMonoManager_1_t70_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonMonoManager_1_t70_0_0_0/* byval_arg */
	, &NonMonoManager_1_t70_1_0_0/* this_arg */
	, &NonMonoManager_1_t70_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &NonMonoManager_1_t70_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Tools/<WaitForRealTime>c__Iterator5
#include "AssemblyU2DCSharp_Tools_U3CWaitForRealTimeU3Ec__Iterator5.h"
// Metadata Definition Tools/<WaitForRealTime>c__Iterator5
extern TypeInfo U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo;
// Tools/<WaitForRealTime>c__Iterator5
#include "AssemblyU2DCSharp_Tools_U3CWaitForRealTimeU3Ec__Iterator5MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Tools/<WaitForRealTime>c__Iterator5::.ctor()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 53/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 54/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Tools/<WaitForRealTime>c__Iterator5::MoveNext()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Tools/<WaitForRealTime>c__Iterator5::Dispose()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 55/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Tools/<WaitForRealTime>c__Iterator5::Reset()
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71/* method */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 56/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForRealTimeU3Ec__Iterator5_t26_MethodInfos[] =
{
	&U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67_MethodInfo;
static const PropertyInfo U3CWaitForRealTimeU3Ec__Iterator5_t26____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<object>.Current"/* name */
	, &U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68_MethodInfo;
static const PropertyInfo U3CWaitForRealTimeU3Ec__Iterator5_t26____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForRealTimeU3Ec__Iterator5_t26_PropertyInfos[] =
{
	&U3CWaitForRealTimeU3Ec__Iterator5_t26____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_t26____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70_MethodInfo;
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69_MethodInfo;
extern const MethodInfo U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71_MethodInfo;
static const Il2CppMethodReference U3CWaitForRealTimeU3Ec__Iterator5_t26_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67_MethodInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71_MethodInfo,
};
static bool U3CWaitForRealTimeU3Ec__Iterator5_t26_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForRealTimeU3Ec__Iterator5_t26_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
	&IEnumerator_t28_0_0_0,
	&IEnumerator_1_t78_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForRealTimeU3Ec__Iterator5_t26_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
	{ &IEnumerator_t28_0_0_0, 5},
	{ &IEnumerator_1_t78_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitForRealTimeU3Ec__Iterator5_t26_0_0_0;
extern const Il2CppType U3CWaitForRealTimeU3Ec__Iterator5_t26_1_0_0;
extern TypeInfo Tools_t27_il2cpp_TypeInfo;
extern const Il2CppType Tools_t27_0_0_0;
struct U3CWaitForRealTimeU3Ec__Iterator5_t26;
const Il2CppTypeDefinitionMetadata U3CWaitForRealTimeU3Ec__Iterator5_t26_DefinitionMetadata = 
{
	&Tools_t27_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_VTable/* vtableMethods */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 63/* fieldStart */

};
TypeInfo U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForRealTime>c__Iterator5"/* name */
	, ""/* namespaze */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_MethodInfos/* methods */
	, U3CWaitForRealTimeU3Ec__Iterator5_t26_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 52/* custom_attributes_cache */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_0_0_0/* byval_arg */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_1_0_0/* this_arg */
	, &U3CWaitForRealTimeU3Ec__Iterator5_t26_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForRealTimeU3Ec__Iterator5_t26)/* instance_size */
	, sizeof (U3CWaitForRealTimeU3Ec__Iterator5_t26)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Tools
#include "AssemblyU2DCSharp_Tools.h"
// Metadata Definition Tools
// Tools
#include "AssemblyU2DCSharp_ToolsMethodDeclarations.h"
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo Tools_t27_Tools_WaitForRealTime_m72_ParameterInfos[] = 
{
	{"p_delay", 0, 134217786, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator Tools::WaitForRealTime(System.Single)
extern const MethodInfo Tools_WaitForRealTime_m72_MethodInfo = 
{
	"WaitForRealTime"/* name */
	, (methodPointerType)&Tools_WaitForRealTime_m72/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t85/* invoker_method */
	, Tools_t27_Tools_WaitForRealTime_m72_ParameterInfos/* parameters */
	, 51/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Transform_t30_0_0_4112;
static const ParameterInfo Tools_t27_Tools_InstantiateCanvas_m214_ParameterInfos[] = 
{
	{"p_name", 0, 134217787, 0, &String_t_0_0_0},
	{"p_parent", 1, 134217788, 0, &Transform_t30_0_0_4112},
};
extern const Il2CppType Tools_InstantiateCanvas_m214_gp_0_0_0_0;
extern const Il2CppGenericContainer Tools_InstantiateCanvas_m214_Il2CppGenericContainer;
extern TypeInfo Tools_InstantiateCanvas_m214_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* Tools_InstantiateCanvas_m214_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter Tools_InstantiateCanvas_m214_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Tools_InstantiateCanvas_m214_Il2CppGenericContainer, Tools_InstantiateCanvas_m214_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* Tools_InstantiateCanvas_m214_Il2CppGenericParametersArray[1] = 
{
	&Tools_InstantiateCanvas_m214_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tools_InstantiateCanvas_m214_MethodInfo;
extern const Il2CppGenericContainer Tools_InstantiateCanvas_m214_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tools_InstantiateCanvas_m214_MethodInfo, 1, 1, Tools_InstantiateCanvas_m214_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Tools_InstantiateResource_TisT_t95_m264_GenericMethod;
static Il2CppRGCTXDefinition Tools_InstantiateCanvas_m214_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Tools_InstantiateResource_TisT_t95_m264_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T Tools::InstantiateCanvas(System.String,UnityEngine.Transform)
extern const MethodInfo Tools_InstantiateCanvas_m214_MethodInfo = 
{
	"InstantiateCanvas"/* name */
	, NULL/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &Tools_InstantiateCanvas_m214_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tools_t27_Tools_InstantiateCanvas_m214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, Tools_InstantiateCanvas_m214_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tools_InstantiateCanvas_m214_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Transform_t30_0_0_4112;
static const ParameterInfo Tools_t27_Tools_InstantiateResource_m73_ParameterInfos[] = 
{
	{"p_path", 0, 134217789, 0, &String_t_0_0_0},
	{"p_parent", 1, 134217790, 0, &Transform_t30_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Tools::InstantiateResource(System.String,UnityEngine.Transform)
extern const MethodInfo Tools_InstantiateResource_m73_MethodInfo = 
{
	"InstantiateResource"/* name */
	, (methodPointerType)&Tools_InstantiateResource_m73/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Tools_t27_Tools_InstantiateResource_m73_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Transform_t30_0_0_4112;
static const ParameterInfo Tools_t27_Tools_InstantiateManager_m215_ParameterInfos[] = 
{
	{"p_name", 0, 134217791, 0, &String_t_0_0_0},
	{"p_parent", 1, 134217792, 0, &Transform_t30_0_0_4112},
};
extern const Il2CppType Tools_InstantiateManager_m215_gp_0_0_0_0;
extern const Il2CppGenericContainer Tools_InstantiateManager_m215_Il2CppGenericContainer;
extern TypeInfo Tools_InstantiateManager_m215_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* Tools_InstantiateManager_m215_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter Tools_InstantiateManager_m215_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Tools_InstantiateManager_m215_Il2CppGenericContainer, Tools_InstantiateManager_m215_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* Tools_InstantiateManager_m215_Il2CppGenericParametersArray[1] = 
{
	&Tools_InstantiateManager_m215_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tools_InstantiateManager_m215_MethodInfo;
extern const Il2CppGenericContainer Tools_InstantiateManager_m215_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tools_InstantiateManager_m215_MethodInfo, 1, 1, Tools_InstantiateManager_m215_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Tools_InstantiateResource_TisT_t96_m265_GenericMethod;
static Il2CppRGCTXDefinition Tools_InstantiateManager_m215_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Tools_InstantiateResource_TisT_t96_m265_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T Tools::InstantiateManager(System.String,UnityEngine.Transform)
extern const MethodInfo Tools_InstantiateManager_m215_MethodInfo = 
{
	"InstantiateManager"/* name */
	, NULL/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &Tools_InstantiateManager_m215_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tools_t27_Tools_InstantiateManager_m215_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, Tools_InstantiateManager_m215_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tools_InstantiateManager_m215_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Transform_t30_0_0_4112;
static const ParameterInfo Tools_t27_Tools_InstantiateResource_m216_ParameterInfos[] = 
{
	{"p_path", 0, 134217793, 0, &String_t_0_0_0},
	{"p_parent", 1, 134217794, 0, &Transform_t30_0_0_4112},
};
extern const Il2CppType Tools_InstantiateResource_m216_gp_0_0_0_0;
extern const Il2CppGenericContainer Tools_InstantiateResource_m216_Il2CppGenericContainer;
extern TypeInfo Tools_InstantiateResource_m216_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* Tools_InstantiateResource_m216_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&MonoBehaviour_t22_0_0_0 /* UnityEngine.MonoBehaviour */, 
 NULL };
extern const Il2CppGenericParameter Tools_InstantiateResource_m216_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Tools_InstantiateResource_m216_Il2CppGenericContainer, Tools_InstantiateResource_m216_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* Tools_InstantiateResource_m216_Il2CppGenericParametersArray[1] = 
{
	&Tools_InstantiateResource_m216_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tools_InstantiateResource_m216_MethodInfo;
extern const Il2CppGenericContainer Tools_InstantiateResource_m216_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tools_InstantiateResource_m216_MethodInfo, 1, 1, Tools_InstantiateResource_m216_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod GameObject_GetComponent_TisT_t97_m266_GenericMethod;
static Il2CppRGCTXDefinition Tools_InstantiateResource_m216_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &GameObject_GetComponent_TisT_t97_m266_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Tools_InstantiateResource_m216_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T Tools::InstantiateResource(System.String,UnityEngine.Transform)
extern const MethodInfo Tools_InstantiateResource_m216_MethodInfo = 
{
	"InstantiateResource"/* name */
	, NULL/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &Tools_InstantiateResource_m216_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tools_t27_Tools_InstantiateResource_m216_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, Tools_InstantiateResource_m216_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tools_InstantiateResource_m216_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Tools_t27_Tools_RemoveSubstring_m74_ParameterInfos[] = 
{
	{"p_mainStr", 0, 134217795, 0, &String_t_0_0_0},
	{"p_subStr", 1, 134217796, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Tools::RemoveSubstring(System.String,System.String)
extern const MethodInfo Tools_RemoveSubstring_m74_MethodInfo = 
{
	"RemoveSubstring"/* name */
	, (methodPointerType)&Tools_RemoveSubstring_m74/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Tools_t27_Tools_RemoveSubstring_m74_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Tools::IsMultipleTouch()
extern const MethodInfo Tools_IsMultipleTouch_m75_MethodInfo = 
{
	"IsMultipleTouch"/* name */
	, (methodPointerType)&Tools_IsMultipleTouch_m75/* method */
	, &Tools_t27_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Tools_t27_MethodInfos[] =
{
	&Tools_WaitForRealTime_m72_MethodInfo,
	&Tools_InstantiateCanvas_m214_MethodInfo,
	&Tools_InstantiateResource_m73_MethodInfo,
	&Tools_InstantiateManager_m215_MethodInfo,
	&Tools_InstantiateResource_m216_MethodInfo,
	&Tools_RemoveSubstring_m74_MethodInfo,
	&Tools_IsMultipleTouch_m75_MethodInfo,
	NULL
};
static const Il2CppType* Tools_t27_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitForRealTimeU3Ec__Iterator5_t26_0_0_0,
};
static const Il2CppMethodReference Tools_t27_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Tools_t27_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Tools_t27_1_0_0;
struct Tools_t27;
const Il2CppTypeDefinitionMetadata Tools_t27_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Tools_t27_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Tools_t27_VTable/* vtableMethods */
	, Tools_t27_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Tools_t27_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tools"/* name */
	, ""/* namespaze */
	, Tools_t27_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tools_t27_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Tools_t27_0_0_0/* byval_arg */
	, &Tools_t27_1_0_0/* this_arg */
	, &Tools_t27_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tools_t27)/* instance_size */
	, sizeof (Tools_t27)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
