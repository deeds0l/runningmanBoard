﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct Stack_1_t2691;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct IEnumerator_1_t3141;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t334;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m14825(__this, method) (( void (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1__ctor_m12000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m14826(__this, method) (( bool (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m12001_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m14827(__this, method) (( Object_t * (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m12002_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m14828(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2691 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m12003_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14829(__this, method) (( Object_t* (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12004_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m14830(__this, method) (( Object_t * (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m12005_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Peek()
#define Stack_1_Peek_m14831(__this, method) (( List_1_t334 * (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_Peek_m12006_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Pop()
#define Stack_1_Pop_m14832(__this, method) (( List_1_t334 * (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_Pop_m12007_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Push(T)
#define Stack_1_Push_m14833(__this, ___t, method) (( void (*) (Stack_1_t2691 *, List_1_t334 *, const MethodInfo*))Stack_1_Push_m12008_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
#define Stack_1_get_Count_m14834(__this, method) (( int32_t (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_get_Count_m12009_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
#define Stack_1_GetEnumerator_m14835(__this, method) (( Enumerator_t3142  (*) (Stack_1_t2691 *, const MethodInfo*))Stack_1_GetEnumerator_m12010_gshared)(__this, method)
