﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1148;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t1725  : public AsymmetricSignatureFormatter_t1301
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t1148 * ___dsa_0;
};
