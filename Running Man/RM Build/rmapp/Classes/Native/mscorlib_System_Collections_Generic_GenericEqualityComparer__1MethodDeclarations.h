﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t1949;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m10422_gshared (GenericEqualityComparer_1_t1949 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m10422(__this, method) (( void (*) (GenericEqualityComparer_1_t1949 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m10422_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m18735_gshared (GenericEqualityComparer_1_t1949 * __this, Guid_t769  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m18735(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1949 *, Guid_t769 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m18735_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m18736_gshared (GenericEqualityComparer_1_t1949 * __this, Guid_t769  ___x, Guid_t769  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m18736(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1949 *, Guid_t769 , Guid_t769 , const MethodInfo*))GenericEqualityComparer_1_Equals_m18736_gshared)(__this, ___x, ___y, method)
