﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.ICancelHandler
struct ICancelHandler_t319;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t102;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct  EventFunction_1_t128  : public MulticastDelegate_t216
{
};
