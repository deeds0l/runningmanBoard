﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t2843;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t2841;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2863;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3097;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m16932_gshared (ThreadSafeDictionary_2_t2843 * __this, ThreadSafeDictionaryValueFactory_2_t2841 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m16932(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, ThreadSafeDictionaryValueFactory_2_t2841 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m16932_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m16936_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m16936(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m16936_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m16938_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m16938(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m16938_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m16940_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m16940(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m16940_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m16942_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m16942(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m16942_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool ThreadSafeDictionary_2_Remove_m16944_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m16944(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m16944_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m16946_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m16946(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m16946_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m16948_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m16948(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m16948_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m16950_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m16950(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m16950_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m16952_gshared (ThreadSafeDictionary_2_t2843 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m16952(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m16952_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m16954_gshared (ThreadSafeDictionary_2_t2843 * __this, KeyValuePair_2_t2593  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m16954(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, KeyValuePair_2_t2593 , const MethodInfo*))ThreadSafeDictionary_2_Add_m16954_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m16956_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m16956(__this, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m16956_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m16958_gshared (ThreadSafeDictionary_2_t2843 * __this, KeyValuePair_2_t2593  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m16958(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2843 *, KeyValuePair_2_t2593 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m16958_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m16960_gshared (ThreadSafeDictionary_2_t2843 * __this, KeyValuePair_2U5BU5D_t2863* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m16960(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t2843 *, KeyValuePair_2U5BU5D_t2863*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m16960_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m16962_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m16962(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m16962_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m16964_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m16964(__this, method) (( bool (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m16964_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m16966_gshared (ThreadSafeDictionary_2_t2843 * __this, KeyValuePair_2_t2593  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m16966(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2843 *, KeyValuePair_2_t2593 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m16966_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m16968_gshared (ThreadSafeDictionary_2_t2843 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m16968(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2843 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m16968_gshared)(__this, method)
