﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>
struct InternalEnumerator_1_t2712;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t519;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15025(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2712 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15026(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::Dispose()
#define InternalEnumerator_1_Dispose_m15027(__this, method) (( void (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15028(__this, method) (( bool (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::get_Current()
#define InternalEnumerator_1_get_Current_m15029(__this, method) (( GUILayoutOption_t519 * (*) (InternalEnumerator_1_t2712 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
