﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct U24ArrayTypeU2448_t1333;
struct U24ArrayTypeU2448_t1333_marshaled;

void U24ArrayTypeU2448_t1333_marshal(const U24ArrayTypeU2448_t1333& unmarshaled, U24ArrayTypeU2448_t1333_marshaled& marshaled);
void U24ArrayTypeU2448_t1333_marshal_back(const U24ArrayTypeU2448_t1333_marshaled& marshaled, U24ArrayTypeU2448_t1333& unmarshaled);
void U24ArrayTypeU2448_t1333_marshal_cleanup(U24ArrayTypeU2448_t1333_marshaled& marshaled);
