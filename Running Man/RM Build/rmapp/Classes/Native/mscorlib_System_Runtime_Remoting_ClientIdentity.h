﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t1670;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct  ClientIdentity_t1671  : public Identity_t1663
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t1670 * ____proxyReference_5;
};
