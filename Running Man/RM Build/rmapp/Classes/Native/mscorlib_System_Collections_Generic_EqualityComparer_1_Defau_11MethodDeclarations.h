﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t3028;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m18742_gshared (DefaultComparer_t3028 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18742(__this, method) (( void (*) (DefaultComparer_t3028 *, const MethodInfo*))DefaultComparer__ctor_m18742_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m18743_gshared (DefaultComparer_t3028 * __this, Guid_t769  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m18743(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3028 *, Guid_t769 , const MethodInfo*))DefaultComparer_GetHashCode_m18743_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m18744_gshared (DefaultComparer_t3028 * __this, Guid_t769  ___x, Guid_t769  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m18744(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3028 *, Guid_t769 , Guid_t769 , const MethodInfo*))DefaultComparer_Equals_m18744_gshared)(__this, ___x, ___y, method)
