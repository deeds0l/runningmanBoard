﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
struct U3CClearSoundEffectsU3Ec__AnonStoreyA_t17;
// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/<ClearSoundEffects>c__AnonStoreyA::.ctor()
extern "C" void U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28 (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<ClearSoundEffects>c__AnonStoreyA::<>m__4(AudioPooler/objectSource)
extern "C" bool U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29 (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * __this, objectSource_t8 * ___audio, const MethodInfo* method) IL2CPP_METHOD_ATTR;
