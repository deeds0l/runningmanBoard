﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$1024
struct U24ArrayTypeU241024_t1904;
struct U24ArrayTypeU241024_t1904_marshaled;

void U24ArrayTypeU241024_t1904_marshal(const U24ArrayTypeU241024_t1904& unmarshaled, U24ArrayTypeU241024_t1904_marshaled& marshaled);
void U24ArrayTypeU241024_t1904_marshal_back(const U24ArrayTypeU241024_t1904_marshaled& marshaled, U24ArrayTypeU241024_t1904& unmarshaled);
void U24ArrayTypeU241024_t1904_marshal_cleanup(U24ArrayTypeU241024_t1904_marshaled& marshaled);
