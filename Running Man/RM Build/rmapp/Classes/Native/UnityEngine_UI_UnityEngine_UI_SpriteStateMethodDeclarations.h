﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.SpriteState
struct SpriteState_t252;
// UnityEngine.Sprite
struct Sprite_t201;

// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern "C" Sprite_t201 * SpriteState_get_highlightedSprite_m1139 (SpriteState_t252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_highlightedSprite_m1140 (SpriteState_t252 * __this, Sprite_t201 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern "C" Sprite_t201 * SpriteState_get_pressedSprite_m1141 (SpriteState_t252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_pressedSprite_m1142 (SpriteState_t252 * __this, Sprite_t201 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern "C" Sprite_t201 * SpriteState_get_disabledSprite_m1143 (SpriteState_t252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern "C" void SpriteState_set_disabledSprite_m1144 (SpriteState_t252 * __this, Sprite_t201 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
