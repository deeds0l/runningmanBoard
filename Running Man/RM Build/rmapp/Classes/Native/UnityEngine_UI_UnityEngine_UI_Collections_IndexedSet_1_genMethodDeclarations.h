﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t172;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t325;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3079;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t2578;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t174;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t173;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m1593(__this, method) (( void (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1__ctor_m13063_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13064(__this, method) (( Object_t * (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m13066(__this, ___item, method) (( void (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m13067_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m13068(__this, ___item, method) (( bool (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m13069_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m13070(__this, method) (( Object_t* (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m13071_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m13072(__this, method) (( void (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_Clear_m13073_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m13074(__this, ___item, method) (( bool (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m13075_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m13076(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t172 *, ICanvasElementU5BU5D_t2578*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m13077_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m13078(__this, method) (( int32_t (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_get_Count_m13079_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m13080(__this, method) (( bool (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m13081_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m13082(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m13083_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m13084(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t172 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m13085_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m13086(__this, ___index, method) (( void (*) (IndexedSet_1_t172 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13087_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m13088(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t172 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13089_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m13090(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t172 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m13091_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m1598(__this, ___match, method) (( void (*) (IndexedSet_1_t172 *, Predicate_1_t174 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13092_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m1599(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t172 *, Comparison_1_t173 *, const MethodInfo*))IndexedSet_1_Sort_m13093_gshared)(__this, ___sortLayoutFunction, method)
