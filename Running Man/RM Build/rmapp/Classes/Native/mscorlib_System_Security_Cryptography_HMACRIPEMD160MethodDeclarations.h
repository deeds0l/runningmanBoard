﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t1727;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m8977 (HMACRIPEMD160_t1727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern "C" void HMACRIPEMD160__ctor_m8978 (HMACRIPEMD160_t1727 * __this, ByteU5BU5D_t546* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
