﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.ExecuteEvents
struct ExecuteEvents_t132;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t112;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t113;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t114;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t115;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t116;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t117;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t118;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t119;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t120;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t121;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t122;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t123;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t124;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t125;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t126;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t127;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t128;
// UnityEngine.EventSystems.IPointerEnterHandler
struct IPointerEnterHandler_t303;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t102;
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t304;
// UnityEngine.EventSystems.IPointerDownHandler
struct IPointerDownHandler_t305;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t306;
// UnityEngine.EventSystems.IPointerClickHandler
struct IPointerClickHandler_t307;
// UnityEngine.EventSystems.IInitializePotentialDragHandler
struct IInitializePotentialDragHandler_t308;
// UnityEngine.EventSystems.IBeginDragHandler
struct IBeginDragHandler_t309;
// UnityEngine.EventSystems.IDragHandler
struct IDragHandler_t310;
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t311;
// UnityEngine.EventSystems.IDropHandler
struct IDropHandler_t312;
// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t313;
// UnityEngine.EventSystems.IUpdateSelectedHandler
struct IUpdateSelectedHandler_t314;
// UnityEngine.EventSystems.ISelectHandler
struct ISelectHandler_t315;
// UnityEngine.EventSystems.IDeselectHandler
struct IDeselectHandler_t316;
// UnityEngine.EventSystems.IMoveHandler
struct IMoveHandler_t317;
// UnityEngine.EventSystems.ISubmitHandler
struct ISubmitHandler_t318;
// UnityEngine.EventSystems.ICancelHandler
struct ICancelHandler_t319;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Collections.Generic.IList`1<UnityEngine.Transform>
struct IList_1_t320;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t321;

// System.Void UnityEngine.EventSystems.ExecuteEvents::.cctor()
extern "C" void ExecuteEvents__cctor_m318 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerEnterHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m319 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerExitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m320 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerDownHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m321 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerUpHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m322 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerClickHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m323 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IInitializePotentialDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m324 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IBeginDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m325 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m326 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IEndDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m327 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDropHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m328 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IScrollHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m329 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IUpdateSelectedHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m330 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISelectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m331 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDeselectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m332 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IMoveHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m333 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISubmitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m334 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ICancelHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m335 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t102 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerEnterHandler()
extern "C" EventFunction_1_t112 * ExecuteEvents_get_pointerEnterHandler_m336 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerExitHandler()
extern "C" EventFunction_1_t113 * ExecuteEvents_get_pointerExitHandler_m337 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerDownHandler()
extern "C" EventFunction_1_t114 * ExecuteEvents_get_pointerDownHandler_m338 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerUpHandler()
extern "C" EventFunction_1_t115 * ExecuteEvents_get_pointerUpHandler_m339 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerClickHandler()
extern "C" EventFunction_1_t116 * ExecuteEvents_get_pointerClickHandler_m340 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_initializePotentialDrag()
extern "C" EventFunction_1_t117 * ExecuteEvents_get_initializePotentialDrag_m341 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_beginDragHandler()
extern "C" EventFunction_1_t118 * ExecuteEvents_get_beginDragHandler_m342 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_dragHandler()
extern "C" EventFunction_1_t119 * ExecuteEvents_get_dragHandler_m343 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_endDragHandler()
extern "C" EventFunction_1_t120 * ExecuteEvents_get_endDragHandler_m344 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::get_dropHandler()
extern "C" EventFunction_1_t121 * ExecuteEvents_get_dropHandler_m345 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::get_scrollHandler()
extern "C" EventFunction_1_t122 * ExecuteEvents_get_scrollHandler_m346 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::get_updateSelectedHandler()
extern "C" EventFunction_1_t123 * ExecuteEvents_get_updateSelectedHandler_m347 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::get_selectHandler()
extern "C" EventFunction_1_t124 * ExecuteEvents_get_selectHandler_m348 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::get_deselectHandler()
extern "C" EventFunction_1_t125 * ExecuteEvents_get_deselectHandler_m349 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::get_moveHandler()
extern "C" EventFunction_1_t126 * ExecuteEvents_get_moveHandler_m350 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::get_submitHandler()
extern "C" EventFunction_1_t127 * ExecuteEvents_get_submitHandler_m351 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::get_cancelHandler()
extern "C" EventFunction_1_t128 * ExecuteEvents_get_cancelHandler_m352 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventChain(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.Transform>)
extern "C" void ExecuteEvents_GetEventChain_m353 (Object_t * __this /* static, unused */, GameObject_t5 * ___root, Object_t* ___eventChain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::<s_HandlerListPool>m__0(System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>)
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m354 (Object_t * __this /* static, unused */, List_1_t321 * ___l, const MethodInfo* method) IL2CPP_METHOD_ATTR;
