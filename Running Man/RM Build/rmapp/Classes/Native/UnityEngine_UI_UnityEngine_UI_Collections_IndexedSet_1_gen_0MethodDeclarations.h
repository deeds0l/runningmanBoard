﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t366;
// UnityEngine.UI.Graphic
struct Graphic_t188;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3111;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t2626;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t2628;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t191;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m1673(__this, method) (( void (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1__ctor_m13063_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14108(__this, method) (( Object_t * (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m14109(__this, ___item, method) (( void (*) (IndexedSet_1_t366 *, Graphic_t188 *, const MethodInfo*))IndexedSet_1_Add_m13067_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m14110(__this, ___item, method) (( bool (*) (IndexedSet_1_t366 *, Graphic_t188 *, const MethodInfo*))IndexedSet_1_Remove_m13069_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m14111(__this, method) (( Object_t* (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m13071_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m14112(__this, method) (( void (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1_Clear_m13073_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m14113(__this, ___item, method) (( bool (*) (IndexedSet_1_t366 *, Graphic_t188 *, const MethodInfo*))IndexedSet_1_Contains_m13075_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m14114(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t366 *, GraphicU5BU5D_t2626*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m13077_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m14115(__this, method) (( int32_t (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1_get_Count_m13079_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m14116(__this, method) (( bool (*) (IndexedSet_1_t366 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m13081_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m14117(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t366 *, Graphic_t188 *, const MethodInfo*))IndexedSet_1_IndexOf_m13083_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m14118(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t366 *, int32_t, Graphic_t188 *, const MethodInfo*))IndexedSet_1_Insert_m13085_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m14119(__this, ___index, method) (( void (*) (IndexedSet_1_t366 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13087_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m14120(__this, ___index, method) (( Graphic_t188 * (*) (IndexedSet_1_t366 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13089_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m14121(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t366 *, int32_t, Graphic_t188 *, const MethodInfo*))IndexedSet_1_set_Item_m13091_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m14122(__this, ___match, method) (( void (*) (IndexedSet_1_t366 *, Predicate_1_t2628 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13092_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m14123(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t366 *, Comparison_1_t191 *, const MethodInfo*))IndexedSet_1_Sort_m13093_gshared)(__this, ___sortLayoutFunction, method)
