﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<AudioPooler/objectSource>
struct List_1_t21;
// AudioPooler/objectSource
struct objectSource_t8;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>
struct  Enumerator_t2452 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::l
	List_1_t21 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::current
	objectSource_t8 * ___current_3;
};
