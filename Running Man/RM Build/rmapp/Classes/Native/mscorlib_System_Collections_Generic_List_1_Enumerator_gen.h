﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t6;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
struct  Enumerator_t41 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::l
	List_1_t6 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::current
	AudioSource_t9 * ___current_3;
};
