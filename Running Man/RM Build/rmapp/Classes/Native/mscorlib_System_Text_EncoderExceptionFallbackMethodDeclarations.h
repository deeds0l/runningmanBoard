﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderExceptionFallback
struct EncoderExceptionFallback_t1779;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1782;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderExceptionFallback::.ctor()
extern "C" void EncoderExceptionFallback__ctor_m9321 (EncoderExceptionFallback_t1779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderExceptionFallback::CreateFallbackBuffer()
extern "C" EncoderFallbackBuffer_t1782 * EncoderExceptionFallback_CreateFallbackBuffer_m9322 (EncoderExceptionFallback_t1779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderExceptionFallback::Equals(System.Object)
extern "C" bool EncoderExceptionFallback_Equals_m9323 (EncoderExceptionFallback_t1779 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderExceptionFallback::GetHashCode()
extern "C" int32_t EncoderExceptionFallback_GetHashCode_m9324 (EncoderExceptionFallback_t1779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
