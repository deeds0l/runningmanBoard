﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.CryptoConvert
struct CryptoConvert_t1238;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.String Mono.Security.Cryptography.CryptoConvert::ToHex(System.Byte[])
extern "C" String_t* CryptoConvert_ToHex_m4815 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
