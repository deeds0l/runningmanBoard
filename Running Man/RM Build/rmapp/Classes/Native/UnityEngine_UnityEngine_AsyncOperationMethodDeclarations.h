﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t472;
struct AsyncOperation_t472_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m2687 (AsyncOperation_t472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m2688 (AsyncOperation_t472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m2689 (AsyncOperation_t472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t472_marshal(const AsyncOperation_t472& unmarshaled, AsyncOperation_t472_marshaled& marshaled);
void AsyncOperation_t472_marshal_back(const AsyncOperation_t472_marshaled& marshaled, AsyncOperation_t472& unmarshaled);
void AsyncOperation_t472_marshal_cleanup(AsyncOperation_t472_marshaled& marshaled);
