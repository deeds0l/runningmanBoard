﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>/$ArrayType$256
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU24256_t1330 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t1330__padding[256];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
#pragma pack(push, tp, 1)
struct U24ArrayTypeU24256_t1330_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t1330__padding[256];
	};
};
#pragma pack(pop, tp)
