﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t525;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t715;
// System.Collections.Generic.ICollection`1<UnityEngine.GUIStyle>
struct ICollection_1_t3158;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t513;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GUIStyle>
struct KeyCollection_t2727;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
struct ValueCollection_t735;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2413;
// System.Collections.Generic.IDictionary`2<System.String,UnityEngine.GUIStyle>
struct IDictionary_2_t3159;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t3160;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>>
struct IEnumerator_1_t3161;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__10.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_16MethodDeclarations.h"
#define Dictionary_2__ctor_m15223(__this, method) (( void (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2__ctor_m13332_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3389(__this, ___comparer, method) (( void (*) (Dictionary_2_t525 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13334_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m15224(__this, ___dictionary, method) (( void (*) (Dictionary_2_t525 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13336_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor(System.Int32)
#define Dictionary_2__ctor_m15225(__this, ___capacity, method) (( void (*) (Dictionary_2_t525 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m13338_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m15226(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t525 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m13340_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m15227(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t525 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m13342_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15228(__this, method) (( Object_t* (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13344_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15229(__this, method) (( Object_t* (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13346_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15230(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m13348_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15231(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t525 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m13350_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m15232(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t525 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m13352_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m15233(__this, ___key, method) (( bool (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m13354_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m15234(__this, ___key, method) (( void (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m13356_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15235(__this, method) (( bool (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13358_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15236(__this, method) (( Object_t * (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13360_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15237(__this, method) (( bool (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15238(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t525 *, KeyValuePair_2_t2726 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13364_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15239(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t525 *, KeyValuePair_2_t2726 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13366_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15240(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t525 *, KeyValuePair_2U5BU5D_t3160*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13368_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15241(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t525 *, KeyValuePair_2_t2726 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13370_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15242(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t525 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m13372_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15243(__this, method) (( Object_t * (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13374_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15244(__this, method) (( Object_t* (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13376_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15245(__this, method) (( Object_t * (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13378_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::get_Count()
#define Dictionary_2_get_Count_m15246(__this, method) (( int32_t (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_get_Count_m13380_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::get_Item(TKey)
#define Dictionary_2_get_Item_m15247(__this, ___key, method) (( GUIStyle_t513 * (*) (Dictionary_2_t525 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m13382_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m15248(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t525 *, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_set_Item_m13384_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m15249(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t525 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m13386_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m15250(__this, ___size, method) (( void (*) (Dictionary_2_t525 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m13388_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m15251(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t525 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m13390_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m15252(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2726  (*) (Object_t * /* static, unused */, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_make_pair_m13392_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m15253(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_pick_key_m13394_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m15254(__this /* static, unused */, ___key, ___value, method) (( GUIStyle_t513 * (*) (Object_t * /* static, unused */, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_pick_value_m13396_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m15255(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t525 *, KeyValuePair_2U5BU5D_t3160*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m13398_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::Resize()
#define Dictionary_2_Resize_m15256(__this, method) (( void (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_Resize_m13400_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::Add(TKey,TValue)
#define Dictionary_2_Add_m15257(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t525 *, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_Add_m13402_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::Clear()
#define Dictionary_2_Clear_m15258(__this, method) (( void (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_Clear_m13404_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m15259(__this, ___key, method) (( bool (*) (Dictionary_2_t525 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m13406_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m15260(__this, ___value, method) (( bool (*) (Dictionary_2_t525 *, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_ContainsValue_m13408_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m15261(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t525 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m13410_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m15262(__this, ___sender, method) (( void (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m13412_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::Remove(TKey)
#define Dictionary_2_Remove_m15263(__this, ___key, method) (( bool (*) (Dictionary_2_t525 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m13414_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m15264(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t525 *, String_t*, GUIStyle_t513 **, const MethodInfo*))Dictionary_2_TryGetValue_m13416_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::get_Keys()
#define Dictionary_2_get_Keys_m15265(__this, method) (( KeyCollection_t2727 * (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_get_Keys_m13418_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::get_Values()
#define Dictionary_2_get_Values_m3390(__this, method) (( ValueCollection_t735 * (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_get_Values_m13420_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m15266(__this, ___key, method) (( String_t* (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m13422_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m15267(__this, ___value, method) (( GUIStyle_t513 * (*) (Dictionary_2_t525 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m13424_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m15268(__this, ___pair, method) (( bool (*) (Dictionary_2_t525 *, KeyValuePair_2_t2726 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m13426_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m15269(__this, method) (( Enumerator_t2728  (*) (Dictionary_2_t525 *, const MethodInfo*))Dictionary_2_GetEnumerator_m13428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m15270(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, String_t*, GUIStyle_t513 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m13430_gshared)(__this /* static, unused */, ___key, ___value, method)
