﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t484;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t663;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
#define Action_1__ctor_m14888(__this, ___object, ___method, method) (( void (*) (Action_1_t484 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m13652_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::Invoke(T)
#define Action_1_Invoke_m14889(__this, ___obj, method) (( void (*) (Action_1_t484 *, IScoreU5BU5D_t663*, const MethodInfo*))Action_1_Invoke_m13654_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m14890(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t484 *, IScoreU5BU5D_t663*, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m13656_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IScore[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m14891(__this, ___result, method) (( void (*) (Action_1_t484 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m13658_gshared)(__this, ___result, method)
