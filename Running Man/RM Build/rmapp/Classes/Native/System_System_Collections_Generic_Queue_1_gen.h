﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t2456;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
struct  Queue_1_t7  : public Object_t
{
	// T[] System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::_array
	AudioSourceU5BU5D_t2456* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::_version
	int32_t ____version_4;
};
