﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t1876;

// System.Void System.ThreadStaticAttribute::.ctor()
extern "C" void ThreadStaticAttribute__ctor_m10272 (ThreadStaticAttribute_t1876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
