﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t3024;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m18721_gshared (DefaultComparer_t3024 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18721(__this, method) (( void (*) (DefaultComparer_t3024 *, const MethodInfo*))DefaultComparer__ctor_m18721_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m18722_gshared (DefaultComparer_t3024 * __this, DateTimeOffset_t768  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m18722(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3024 *, DateTimeOffset_t768 , const MethodInfo*))DefaultComparer_GetHashCode_m18722_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m18723_gshared (DefaultComparer_t3024 * __this, DateTimeOffset_t768  ___x, DateTimeOffset_t768  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m18723(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3024 *, DateTimeOffset_t768 , DateTimeOffset_t768 , const MethodInfo*))DefaultComparer_Equals_m18723_gshared)(__this, ___x, ___y, method)
