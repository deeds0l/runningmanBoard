﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct InternalEnumerator_1_t2594;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13475_gshared (InternalEnumerator_1_t2594 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13475(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2594 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13475_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13476_gshared (InternalEnumerator_1_t2594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13476(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2594 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13476_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13477_gshared (InternalEnumerator_1_t2594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13477(__this, method) (( void (*) (InternalEnumerator_1_t2594 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13477_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13478_gshared (InternalEnumerator_1_t2594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13478(__this, method) (( bool (*) (InternalEnumerator_1_t2594 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13478_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2593  InternalEnumerator_1_get_Current_m13479_gshared (InternalEnumerator_1_t2594 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13479(__this, method) (( KeyValuePair_2_t2593  (*) (InternalEnumerator_1_t2594 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13479_gshared)(__this, method)
