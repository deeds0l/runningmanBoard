﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MonoManager`1<System.Object>
struct MonoManager_1_t2425;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t30;

// System.Void MonoManager`1<System.Object>::.ctor()
extern "C" void MonoManager_1__ctor_m11264_gshared (MonoManager_1_t2425 * __this, const MethodInfo* method);
#define MonoManager_1__ctor_m11264(__this, method) (( void (*) (MonoManager_1_t2425 *, const MethodInfo*))MonoManager_1__ctor_m11264_gshared)(__this, method)
// System.Void MonoManager`1<System.Object>::.cctor()
extern "C" void MonoManager_1__cctor_m11266_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define MonoManager_1__cctor_m11266(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))MonoManager_1__cctor_m11266_gshared)(__this /* static, unused */, method)
// T MonoManager`1<System.Object>::get_Instance()
extern "C" Object_t * MonoManager_1_get_Instance_m11267_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define MonoManager_1_get_Instance_m11267(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))MonoManager_1_get_Instance_m11267_gshared)(__this /* static, unused */, method)
// System.Void MonoManager`1<System.Object>::Create(UnityEngine.Transform)
extern "C" void MonoManager_1_Create_m11268_gshared (Object_t * __this /* static, unused */, Transform_t30 * ___p_parent, const MethodInfo* method);
#define MonoManager_1_Create_m11268(__this /* static, unused */, ___p_parent, method) (( void (*) (Object_t * /* static, unused */, Transform_t30 *, const MethodInfo*))MonoManager_1_Create_m11268_gshared)(__this /* static, unused */, ___p_parent, method)
// System.Void MonoManager`1<System.Object>::Awake()
extern "C" void MonoManager_1_Awake_m11270_gshared (MonoManager_1_t2425 * __this, const MethodInfo* method);
#define MonoManager_1_Awake_m11270(__this, method) (( void (*) (MonoManager_1_t2425 *, const MethodInfo*))MonoManager_1_Awake_m11270_gshared)(__this, method)
// System.Void MonoManager`1<System.Object>::OnDestroy()
extern "C" void MonoManager_1_OnDestroy_m11271_gshared (MonoManager_1_t2425 * __this, const MethodInfo* method);
#define MonoManager_1_OnDestroy_m11271(__this, method) (( void (*) (MonoManager_1_t2425 *, const MethodInfo*))MonoManager_1_OnDestroy_m11271_gshared)(__this, method)
// System.Void MonoManager`1<System.Object>::Destroy()
extern "C" void MonoManager_1_Destroy_m11273_gshared (MonoManager_1_t2425 * __this, const MethodInfo* method);
#define MonoManager_1_Destroy_m11273(__this, method) (( void (*) (MonoManager_1_t2425 *, const MethodInfo*))MonoManager_1_Destroy_m11273_gshared)(__this, method)
