﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t2548;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2535;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m12885_gshared (ShimEnumerator_t2548 * __this, Dictionary_2_t2535 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m12885(__this, ___host, method) (( void (*) (ShimEnumerator_t2548 *, Dictionary_2_t2535 *, const MethodInfo*))ShimEnumerator__ctor_m12885_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m12886_gshared (ShimEnumerator_t2548 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m12886(__this, method) (( bool (*) (ShimEnumerator_t2548 *, const MethodInfo*))ShimEnumerator_MoveNext_m12886_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1147  ShimEnumerator_get_Entry_m12887_gshared (ShimEnumerator_t2548 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m12887(__this, method) (( DictionaryEntry_t1147  (*) (ShimEnumerator_t2548 *, const MethodInfo*))ShimEnumerator_get_Entry_m12887_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m12888_gshared (ShimEnumerator_t2548 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m12888(__this, method) (( Object_t * (*) (ShimEnumerator_t2548 *, const MethodInfo*))ShimEnumerator_get_Key_m12888_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m12889_gshared (ShimEnumerator_t2548 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m12889(__this, method) (( Object_t * (*) (ShimEnumerator_t2548 *, const MethodInfo*))ShimEnumerator_get_Value_m12889_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m12890_gshared (ShimEnumerator_t2548 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m12890(__this, method) (( Object_t * (*) (ShimEnumerator_t2548 *, const MethodInfo*))ShimEnumerator_get_Current_m12890_gshared)(__this, method)
