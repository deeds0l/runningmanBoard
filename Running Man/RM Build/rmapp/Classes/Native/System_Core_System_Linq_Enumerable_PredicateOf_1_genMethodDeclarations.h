﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/PredicateOf`1<System.Object>
struct PredicateOf_1_t2454;
// System.Object
struct Object_t;

// System.Void System.Linq.Enumerable/PredicateOf`1<System.Object>::.cctor()
extern "C" void PredicateOf_1__cctor_m11605_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define PredicateOf_1__cctor_m11605(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))PredicateOf_1__cctor_m11605_gshared)(__this /* static, unused */, method)
// System.Boolean System.Linq.Enumerable/PredicateOf`1<System.Object>::<Always>m__76(T)
extern "C" bool PredicateOf_1_U3CAlwaysU3Em__76_m11606_gshared (Object_t * __this /* static, unused */, Object_t * ___t, const MethodInfo* method);
#define PredicateOf_1_U3CAlwaysU3Em__76_m11606(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))PredicateOf_1_U3CAlwaysU3Em__76_m11606_gshared)(__this /* static, unused */, ___t, method)
