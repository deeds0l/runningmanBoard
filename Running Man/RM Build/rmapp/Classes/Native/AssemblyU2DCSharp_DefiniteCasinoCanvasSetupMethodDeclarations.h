﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DefiniteCasinoCanvasSetup
struct DefiniteCasinoCanvasSetup_t25;

// System.Void DefiniteCasinoCanvasSetup::.ctor()
extern "C" void DefiniteCasinoCanvasSetup__ctor_m63 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefiniteCasinoCanvasSetup::Awake()
extern "C" void DefiniteCasinoCanvasSetup_Awake_m64 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefiniteCasinoCanvasSetup::ResetToDefiniteCasinoSetup()
extern "C" void DefiniteCasinoCanvasSetup_ResetToDefiniteCasinoSetup_m65 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
