﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Object>
struct InternalEnumerator_1_t2695;
// System.Object
struct Object_t;
// UnityEngine.Object
struct Object_t33;
struct Object_t33_marshaled;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14857(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2695 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Object>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14858(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2695 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::Dispose()
#define InternalEnumerator_1_Dispose_m14859(__this, method) (( void (*) (InternalEnumerator_1_t2695 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Object>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14860(__this, method) (( bool (*) (InternalEnumerator_1_t2695 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Object>::get_Current()
#define InternalEnumerator_1_get_Current_m14861(__this, method) (( Object_t33 * (*) (InternalEnumerator_1_t2695 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
