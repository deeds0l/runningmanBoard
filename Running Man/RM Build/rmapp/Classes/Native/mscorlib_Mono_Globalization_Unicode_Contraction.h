﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t222;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.Contraction
struct  Contraction_t1396  : public Object_t
{
	// System.Char[] Mono.Globalization.Unicode.Contraction::Source
	CharU5BU5D_t222* ___Source_0;
	// System.String Mono.Globalization.Unicode.Contraction::Replacement
	String_t* ___Replacement_1;
	// System.Byte[] Mono.Globalization.Unicode.Contraction::SortKey
	ByteU5BU5D_t546* ___SortKey_2;
};
