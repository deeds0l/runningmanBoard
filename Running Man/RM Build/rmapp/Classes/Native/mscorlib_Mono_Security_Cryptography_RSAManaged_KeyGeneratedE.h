﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1249;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t1432  : public MulticastDelegate_t216
{
};
