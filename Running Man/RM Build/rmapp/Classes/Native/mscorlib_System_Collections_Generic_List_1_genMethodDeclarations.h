﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t6;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioSource>
struct IEnumerator_1_t3037;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.AudioSource>
struct ICollection_1_t3038;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AudioSource>
struct IEnumerable_1_t3039;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioSource>
struct ReadOnlyCollection_1_t2458;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t2456;
// System.Predicate`1<UnityEngine.AudioSource>
struct Predicate_1_t2459;
// System.Comparison`1<UnityEngine.AudioSource>
struct Comparison_1_t2460;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m101(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.ctor(System.Int32)
#define List_1__ctor_m11615(__this, ___capacity, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.cctor()
#define List_1__cctor_m11616(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11617(__this, method) (( Object_t* (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m11618(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t6 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11619(__this, method) (( Object_t * (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m11620(__this, ___item, method) (( int32_t (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m11621(__this, ___item, method) (( bool (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m11622(__this, ___item, method) (( int32_t (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m11623(__this, ___index, ___item, method) (( void (*) (List_1_t6 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m11624(__this, ___item, method) (( void (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11625(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m11626(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m11627(__this, method) (( Object_t * (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m11628(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m11629(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m11630(__this, ___index, method) (( Object_t * (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m11631(__this, ___index, ___value, method) (( void (*) (List_1_t6 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Add(T)
#define List_1_Add_m11632(__this, ___item, method) (( void (*) (List_1_t6 *, AudioSource_t9 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m11633(__this, ___newCount, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m11634(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m11635(__this, ___enumerable, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m11636(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::AsReadOnly()
#define List_1_AsReadOnly_m11637(__this, method) (( ReadOnlyCollection_1_t2458 * (*) (List_1_t6 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear()
#define List_1_Clear_m11638(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Contains(T)
#define List_1_Contains_m11639(__this, ___item, method) (( bool (*) (List_1_t6 *, AudioSource_t9 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m11640(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t6 *, AudioSourceU5BU5D_t2456*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioSource>::Find(System.Predicate`1<T>)
#define List_1_Find_m11641(__this, ___match, method) (( AudioSource_t9 * (*) (List_1_t6 *, Predicate_1_t2459 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m11642(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2459 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m11643(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t6 *, int32_t, int32_t, Predicate_1_t2459 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::GetEnumerator()
#define List_1_GetEnumerator_m109(__this, method) (( Enumerator_t41  (*) (List_1_t6 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::IndexOf(T)
#define List_1_IndexOf_m11644(__this, ___item, method) (( int32_t (*) (List_1_t6 *, AudioSource_t9 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m11645(__this, ___start, ___delta, method) (( void (*) (List_1_t6 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m11646(__this, ___index, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Insert(System.Int32,T)
#define List_1_Insert_m11647(__this, ___index, ___item, method) (( void (*) (List_1_t6 *, int32_t, AudioSource_t9 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m11648(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Remove(T)
#define List_1_Remove_m11649(__this, ___item, method) (( bool (*) (List_1_t6 *, AudioSource_t9 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m11650(__this, ___match, method) (( int32_t (*) (List_1_t6 *, Predicate_1_t2459 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m11651(__this, ___index, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Reverse()
#define List_1_Reverse_m11652(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Sort()
#define List_1_Sort_m11653(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m11654(__this, ___comparison, method) (( void (*) (List_1_t6 *, Comparison_1_t2460 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.AudioSource>::ToArray()
#define List_1_ToArray_m11655(__this, method) (( AudioSourceU5BU5D_t2456* (*) (List_1_t6 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::TrimExcess()
#define List_1_TrimExcess_m11656(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Capacity()
#define List_1_get_Capacity_m11657(__this, method) (( int32_t (*) (List_1_t6 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m11658(__this, ___value, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Count()
#define List_1_get_Count_m11659(__this, method) (( int32_t (*) (List_1_t6 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Item(System.Int32)
#define List_1_get_Item_m11660(__this, ___index, method) (( AudioSource_t9 * (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::set_Item(System.Int32,T)
#define List_1_set_Item_m11661(__this, ___index, ___value, method) (( void (*) (List_1_t6 *, int32_t, AudioSource_t9 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
