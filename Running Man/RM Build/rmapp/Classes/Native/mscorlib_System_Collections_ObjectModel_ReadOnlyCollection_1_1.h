﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<AudioPooler/objectSource>
struct IList_1_t2449;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>
struct  ReadOnlyCollection_1_t2450  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::list
	Object_t* ___list_0;
};
