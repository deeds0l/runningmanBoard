﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>
struct Enumerator_t2522;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t30;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t130;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m12599(__this, ___l, method) (( void (*) (Enumerator_t2522 *, List_1_t130 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12600(__this, method) (( Object_t * (*) (Enumerator_t2522 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::Dispose()
#define Enumerator_Dispose_m12601(__this, method) (( void (*) (Enumerator_t2522 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::VerifyState()
#define Enumerator_VerifyState_m12602(__this, method) (( void (*) (Enumerator_t2522 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::MoveNext()
#define Enumerator_MoveNext_m12603(__this, method) (( bool (*) (Enumerator_t2522 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::get_Current()
#define Enumerator_get_Current_m12604(__this, method) (( Transform_t30 * (*) (Enumerator_t2522 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
