﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
struct Enumerator_t2491;
// System.Object
struct Object_t;
// UnityEngine.Component
struct Component_t49;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t332;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m12115(__this, ___l, method) (( void (*) (Enumerator_t2491 *, List_1_t332 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12116(__this, method) (( Object_t * (*) (Enumerator_t2491 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::Dispose()
#define Enumerator_Dispose_m12117(__this, method) (( void (*) (Enumerator_t2491 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::VerifyState()
#define Enumerator_VerifyState_m12118(__this, method) (( void (*) (Enumerator_t2491 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::MoveNext()
#define Enumerator_MoveNext_m12119(__this, method) (( bool (*) (Enumerator_t2491 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::get_Current()
#define Enumerator_get_Current_m12120(__this, method) (( Component_t49 * (*) (Enumerator_t2491 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
