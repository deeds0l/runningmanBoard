﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>
struct U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466;
// System.Object
struct Object_t;

// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::.ctor()
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m11764_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m11764(__this, method) (( void (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m11764_gshared)(__this, method)
// System.Object BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11765_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11765(__this, method) (( Object_t * (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11765_gshared)(__this, method)
// System.Object BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m11766_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m11766(__this, method) (( Object_t * (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m11766_gshared)(__this, method)
// System.Boolean BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::MoveNext()
extern "C" bool U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m11767_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m11767(__this, method) (( bool (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m11767_gshared)(__this, method)
// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::Dispose()
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m11768_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m11768(__this, method) (( void (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m11768_gshared)(__this, method)
// System.Void BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::Reset()
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m11769_gshared (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 * __this, const MethodInfo* method);
#define U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m11769(__this, method) (( void (*) (U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466 *, const MethodInfo*))U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m11769_gshared)(__this, method)
