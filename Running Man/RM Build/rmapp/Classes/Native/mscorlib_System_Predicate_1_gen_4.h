﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip
struct AudioClip_t20;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.AudioClip>
struct  Predicate_1_t2445  : public MulticastDelegate_t216
{
};
