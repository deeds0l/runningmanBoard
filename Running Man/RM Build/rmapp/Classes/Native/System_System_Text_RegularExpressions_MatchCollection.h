﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1067;
// System.Collections.ArrayList
struct ArrayList_t985;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t1077  : public Object_t
{
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t1067 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t985 * ___list_1;
};
