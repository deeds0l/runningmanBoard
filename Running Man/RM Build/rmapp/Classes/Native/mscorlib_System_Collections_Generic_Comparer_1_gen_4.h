﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t2966;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Int32>
struct  Comparer_1_t2966  : public Object_t
{
};
struct Comparer_1_t2966_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::_default
	Comparer_1_t2966 * ____default_0;
};
