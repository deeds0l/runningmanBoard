﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Rigidbody2D>
struct Comparison_1_t2756;
// System.Object
struct Object_t;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t568;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.Rigidbody2D>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m15809(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2756 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11465_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Rigidbody2D>::Invoke(T,T)
#define Comparison_1_Invoke_m15810(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2756 *, Rigidbody2D_t568 *, Rigidbody2D_t568 *, const MethodInfo*))Comparison_1_Invoke_m11466_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Rigidbody2D>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m15811(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2756 *, Rigidbody2D_t568 *, Rigidbody2D_t568 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11467_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Rigidbody2D>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m15812(__this, ___result, method) (( int32_t (*) (Comparison_1_t2756 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11468_gshared)(__this, ___result, method)
