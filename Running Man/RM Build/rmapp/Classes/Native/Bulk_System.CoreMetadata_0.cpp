﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t925_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t925_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t925_0_0_0;
extern const Il2CppType U3CModuleU3E_t925_1_0_0;
struct U3CModuleU3E_t925;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t925_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t925_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t925_0_0_0/* byval_arg */
	, &U3CModuleU3E_t925_1_0_0/* this_arg */
	, &U3CModuleU3E_t925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t925)/* instance_size */
	, sizeof (U3CModuleU3E_t925)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t792_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern const MethodInfo ExtensionAttribute__ctor_m3533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m3533/* method */
	, &ExtensionAttribute_t792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExtensionAttribute_t792_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m3533_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m3659_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m3530_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference ExtensionAttribute_t792_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ExtensionAttribute_t792_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t832_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t792_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t792_0_0_0;
extern const Il2CppType ExtensionAttribute_t792_1_0_0;
extern const Il2CppType Attribute_t539_0_0_0;
struct ExtensionAttribute_t792;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t792_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t792_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ExtensionAttribute_t792_VTable/* vtableMethods */
	, ExtensionAttribute_t792_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExtensionAttribute_t792_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t792_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExtensionAttribute_t792_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &ExtensionAttribute_t792_0_0_0/* byval_arg */
	, &ExtensionAttribute_t792_1_0_0/* this_arg */
	, &ExtensionAttribute_t792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t792)/* instance_size */
	, sizeof (ExtensionAttribute_t792)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t926_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t926_Check_Source_m3738_ParameterInfos[] = 
{
	{"source", 0, 134217729, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::Source(System.Object)
extern const MethodInfo Check_Source_m3738_MethodInfo = 
{
	"Source"/* name */
	, (methodPointerType)&Check_Source_m3738/* method */
	, &Check_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Check_t926_Check_Source_m3738_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t926_Check_SourceAndPredicate_m3739_ParameterInfos[] = 
{
	{"source", 0, 134217730, 0, &Object_t_0_0_0},
	{"predicate", 1, 134217731, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern const MethodInfo Check_SourceAndPredicate_m3739_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m3739/* method */
	, &Check_t926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Check_t926_Check_SourceAndPredicate_m3739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Check_t926_MethodInfos[] =
{
	&Check_Source_m3738_MethodInfo,
	&Check_SourceAndPredicate_m3739_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
static const Il2CppMethodReference Check_t926_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Check_t926_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Check_t926_0_0_0;
extern const Il2CppType Check_t926_1_0_0;
struct Check_t926;
const Il2CppTypeDefinitionMetadata Check_t926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t926_VTable/* vtableMethods */
	, Check_t926_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Check_t926_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t926_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Check_t926_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t926_0_0_0/* byval_arg */
	, &Check_t926_1_0_0/* this_arg */
	, &Check_t926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t926)/* instance_size */
	, sizeof (Check_t926)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_Fallback.h"
// Metadata Definition System.Linq.Enumerable/Fallback
extern TypeInfo Fallback_t927_il2cpp_TypeInfo;
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_FallbackMethodDeclarations.h"
static const MethodInfo* Fallback_t927_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference Fallback_t927_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Fallback_t927_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair Fallback_t927_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Fallback_t927_0_0_0;
extern const Il2CppType Fallback_t927_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
extern TypeInfo Enumerable_t34_il2cpp_TypeInfo;
extern const Il2CppType Enumerable_t34_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Fallback_t927_DefinitionMetadata = 
{
	&Enumerable_t34_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Fallback_t927_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Fallback_t927_VTable/* vtableMethods */
	, Fallback_t927_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo Fallback_t927_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Fallback"/* name */
	, ""/* namespaze */
	, Fallback_t927_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Fallback_t927_0_0_0/* byval_arg */
	, &Fallback_t927_1_0_0/* this_arg */
	, &Fallback_t927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Fallback_t927)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Fallback_t927)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/PredicateOf`1
extern TypeInfo PredicateOf_1_t937_il2cpp_TypeInfo;
extern const Il2CppGenericContainer PredicateOf_1_t937_Il2CppGenericContainer;
extern TypeInfo PredicateOf_1_t937_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PredicateOf_1_t937_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &PredicateOf_1_t937_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* PredicateOf_1_t937_Il2CppGenericParametersArray[1] = 
{
	&PredicateOf_1_t937_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer PredicateOf_1_t937_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&PredicateOf_1_t937_il2cpp_TypeInfo, 1, 0, PredicateOf_1_t937_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/PredicateOf`1::.cctor()
extern const MethodInfo PredicateOf_1__cctor_m3755_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &PredicateOf_1_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PredicateOf_1_t937_gp_0_0_0_0;
extern const Il2CppType PredicateOf_1_t937_gp_0_0_0_0;
static const ParameterInfo PredicateOf_1_t937_PredicateOf_1_U3CAlwaysU3Em__76_m3756_ParameterInfos[] = 
{
	{"t", 0, 134217745, 0, &PredicateOf_1_t937_gp_0_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
// System.Boolean System.Linq.Enumerable/PredicateOf`1::<Always>m__76(T)
extern const MethodInfo PredicateOf_1_U3CAlwaysU3Em__76_m3756_MethodInfo = 
{
	"<Always>m__76"/* name */
	, NULL/* method */
	, &PredicateOf_1_t937_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PredicateOf_1_t937_PredicateOf_1_U3CAlwaysU3Em__76_m3756_ParameterInfos/* parameters */
	, 11/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PredicateOf_1_t937_MethodInfos[] =
{
	&PredicateOf_1__cctor_m3755_MethodInfo,
	&PredicateOf_1_U3CAlwaysU3Em__76_m3756_MethodInfo,
	NULL
};
static const Il2CppMethodReference PredicateOf_1_t937_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool PredicateOf_1_t937_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType PredicateOf_1_t941_0_0_0;
extern const Il2CppGenericMethod PredicateOf_1_U3CAlwaysU3Em__76_m3768_GenericMethod;
extern const Il2CppType Func_2_t942_0_0_0;
extern const Il2CppGenericMethod Func_2__ctor_m3769_GenericMethod;
static Il2CppRGCTXDefinition PredicateOf_1_t937_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&PredicateOf_1_t941_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PredicateOf_1_U3CAlwaysU3Em__76_m3768_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Func_2_t942_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2__ctor_m3769_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType PredicateOf_1_t937_0_0_0;
extern const Il2CppType PredicateOf_1_t937_1_0_0;
struct PredicateOf_1_t937;
const Il2CppTypeDefinitionMetadata PredicateOf_1_t937_DefinitionMetadata = 
{
	&Enumerable_t34_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PredicateOf_1_t937_VTable/* vtableMethods */
	, PredicateOf_1_t937_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, PredicateOf_1_t937_RGCTXData/* rgctxDefinition */
	, 3/* fieldStart */

};
TypeInfo PredicateOf_1_t937_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PredicateOf`1"/* name */
	, ""/* namespaze */
	, PredicateOf_1_t937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PredicateOf_1_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PredicateOf_1_t937_0_0_0/* byval_arg */
	, &PredicateOf_1_t937_1_0_0/* this_arg */
	, &PredicateOf_1_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &PredicateOf_1_t937_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3757_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 13/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 14/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t28_0_0_0;
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 15/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t944_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t944_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 16/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3762_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 17/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3757_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3762_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3762_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761_MethodInfo;
static const Il2CppMethodReference U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3762_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758_MethodInfo,
};
static bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t43_0_0_0;
extern const Il2CppType IEnumerable_t464_0_0_0;
extern const Il2CppType IEnumerable_1_t945_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_InterfacesTypeInfos[] = 
{
	&IEnumerator_t28_0_0_0,
	&IDisposable_t43_0_0_0,
	&IEnumerable_t464_0_0_0,
	&IEnumerable_1_t945_0_0_0,
	&IEnumerator_1_t944_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_InterfacesOffsets[] = 
{
	{ &IEnumerator_t28_0_0_0, 4},
	{ &IDisposable_t43_0_0_0, 6},
	{ &IEnumerable_t464_0_0_0, 7},
	{ &IEnumerable_1_t945_0_0_0, 8},
	{ &IEnumerator_1_t944_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3770_GenericMethod;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t946_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3771_GenericMethod;
extern const Il2CppGenericMethod Func_2_Invoke_m3772_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3770_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t946_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3771_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t945_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t944_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m3772_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_DefinitionMetadata = 
{
	&Enumerable_t34_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_RGCTXData/* rgctxDefinition */
	, 5/* fieldStart */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 12/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern const Il2CppType IEnumerable_1_t947_0_0_0;
extern const Il2CppType IEnumerable_1_t947_0_0_0;
extern const Il2CppType Func_2_t948_0_0_0;
extern const Il2CppType Func_2_t948_0_0_0;
extern const Il2CppType Fallback_t927_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_First_m3749_ParameterInfos[] = 
{
	{"source", 0, 134217732, 0, &IEnumerable_1_t947_0_0_0},
	{"predicate", 1, 134217733, 0, &Func_2_t948_0_0_0},
	{"fallback", 2, 134217734, 0, &Fallback_t927_0_0_0},
};
extern const Il2CppType Enumerable_First_m3749_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_First_m3749_Il2CppGenericContainer;
extern TypeInfo Enumerable_First_m3749_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_First_m3749_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_First_m3749_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_First_m3749_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_First_m3749_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_First_m3749_MethodInfo;
extern const Il2CppGenericContainer Enumerable_First_m3749_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_First_m3749_MethodInfo, 1, 1, Enumerable_First_m3749_Il2CppGenericParametersArray };
extern const Il2CppType IEnumerator_1_t950_0_0_0;
extern const Il2CppGenericMethod Func_2_Invoke_m3773_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_First_m3749_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t947_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t950_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m3773_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern const MethodInfo Enumerable_First_m3749_MethodInfo = 
{
	"First"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_First_m3749_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_First_m3749_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, Enumerable_First_m3749_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_First_m3749_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t951_0_0_0;
extern const Il2CppType IEnumerable_1_t951_0_0_0;
extern const Il2CppType Func_2_t952_0_0_0;
extern const Il2CppType Func_2_t952_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_FirstOrDefault_m3750_ParameterInfos[] = 
{
	{"source", 0, 134217735, 0, &IEnumerable_1_t951_0_0_0},
	{"predicate", 1, 134217736, 0, &Func_2_t952_0_0_0},
};
extern const Il2CppType Enumerable_FirstOrDefault_m3750_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_FirstOrDefault_m3750_Il2CppGenericContainer;
extern TypeInfo Enumerable_FirstOrDefault_m3750_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_FirstOrDefault_m3750_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_FirstOrDefault_m3750_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_FirstOrDefault_m3750_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_FirstOrDefault_m3750_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_FirstOrDefault_m3750_MethodInfo;
extern const Il2CppGenericContainer Enumerable_FirstOrDefault_m3750_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_FirstOrDefault_m3750_MethodInfo, 1, 1, Enumerable_FirstOrDefault_m3750_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_First_TisTSource_t953_m3774_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_FirstOrDefault_m3750_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_First_TisTSource_t953_m3774_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_FirstOrDefault_m3750_MethodInfo = 
{
	"FirstOrDefault"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_FirstOrDefault_m3750_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_FirstOrDefault_m3750_ParameterInfos/* parameters */
	, 5/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, Enumerable_FirstOrDefault_m3750_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_FirstOrDefault_m3750_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t954_0_0_0;
extern const Il2CppType IEnumerable_1_t954_0_0_0;
extern const Il2CppType Func_2_t955_0_0_0;
extern const Il2CppType Func_2_t955_0_0_0;
extern const Il2CppType Fallback_t927_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_Last_m3751_ParameterInfos[] = 
{
	{"source", 0, 134217737, 0, &IEnumerable_1_t954_0_0_0},
	{"predicate", 1, 134217738, 0, &Func_2_t955_0_0_0},
	{"fallback", 2, 134217739, 0, &Fallback_t927_0_0_0},
};
extern const Il2CppType Enumerable_Last_m3751_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_Last_m3751_Il2CppGenericContainer;
extern TypeInfo Enumerable_Last_m3751_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Last_m3751_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Last_m3751_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Last_m3751_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Last_m3751_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Last_m3751_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Last_m3751_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Last_m3751_MethodInfo, 1, 1, Enumerable_Last_m3751_Il2CppGenericParametersArray };
extern const Il2CppType IEnumerator_1_t957_0_0_0;
extern const Il2CppGenericMethod Func_2_Invoke_m3775_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Last_m3751_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t954_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t957_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m3775_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Linq.Enumerable/Fallback)
extern const MethodInfo Enumerable_Last_m3751_MethodInfo = 
{
	"Last"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_Last_m3751_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_Last_m3751_ParameterInfos/* parameters */
	, 6/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, Enumerable_Last_m3751_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Last_m3751_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t958_0_0_0;
extern const Il2CppType IEnumerable_1_t958_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_Last_m3752_ParameterInfos[] = 
{
	{"source", 0, 134217740, 0, &IEnumerable_1_t958_0_0_0},
};
extern const Il2CppType Enumerable_Last_m3752_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_Last_m3752_Il2CppGenericContainer;
extern TypeInfo Enumerable_Last_m3752_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Last_m3752_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Last_m3752_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Last_m3752_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Last_m3752_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Last_m3752_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Last_m3752_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Last_m3752_MethodInfo, 1, 1, Enumerable_Last_m3752_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t960_0_0_0;
extern const Il2CppType IList_1_t961_0_0_0;
extern const Il2CppType PredicateOf_1_t962_0_0_0;
extern const Il2CppGenericMethod Enumerable_Last_TisTSource_t959_m3776_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Last_m3752_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t960_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IList_1_t961_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&PredicateOf_1_t962_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Last_TisTSource_t959_m3776_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_Last_m3752_MethodInfo = 
{
	"Last"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_Last_m3752_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_Last_m3752_ParameterInfos/* parameters */
	, 7/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, Enumerable_Last_m3752_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Last_m3752_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t963_0_0_0;
extern const Il2CppType IEnumerable_1_t963_0_0_0;
extern const Il2CppType Func_2_t964_0_0_0;
extern const Il2CppType Func_2_t964_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_Where_m3753_ParameterInfos[] = 
{
	{"source", 0, 134217741, 0, &IEnumerable_1_t963_0_0_0},
	{"predicate", 1, 134217742, 0, &Func_2_t964_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Where_m3753_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m3753_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Where_m3753_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Where_m3753_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Where_m3753_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m3753_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Where_m3753_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Where_m3753_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Where_m3753_MethodInfo, 1, 1, Enumerable_Where_m3753_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t965_m3777_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m3753_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t965_m3777_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_Where_m3753_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t963_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_Where_m3753_ParameterInfos/* parameters */
	, 8/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, Enumerable_Where_m3753_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m3753_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t966_0_0_0;
extern const Il2CppType IEnumerable_1_t966_0_0_0;
extern const Il2CppType Func_2_t967_0_0_0;
extern const Il2CppType Func_2_t967_0_0_0;
static const ParameterInfo Enumerable_t34_Enumerable_CreateWhereIterator_m3754_ParameterInfos[] = 
{
	{"source", 0, 134217743, 0, &IEnumerable_1_t966_0_0_0},
	{"predicate", 1, 134217744, 0, &Func_2_t967_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m3754_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m3754_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateWhereIterator_m3754_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateWhereIterator_m3754_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateWhereIterator_m3754_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m3754_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateWhereIterator_m3754_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m3754_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateWhereIterator_m3754_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m3754_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t969_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3778_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m3754_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t969_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m3778_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_CreateWhereIterator_m3754_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t34_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t966_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t34_Enumerable_CreateWhereIterator_m3754_ParameterInfos/* parameters */
	, 9/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, Enumerable_CreateWhereIterator_m3754_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m3754_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Enumerable_t34_MethodInfos[] =
{
	&Enumerable_First_m3749_MethodInfo,
	&Enumerable_FirstOrDefault_m3750_MethodInfo,
	&Enumerable_Last_m3751_MethodInfo,
	&Enumerable_Last_m3752_MethodInfo,
	&Enumerable_Where_m3753_MethodInfo,
	&Enumerable_CreateWhereIterator_m3754_MethodInfo,
	NULL
};
static const Il2CppType* Enumerable_t34_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Fallback_t927_0_0_0,
	&PredicateOf_1_t937_0_0_0,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_0_0_0,
};
static const Il2CppMethodReference Enumerable_t34_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Enumerable_t34_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerable_t34_1_0_0;
struct Enumerable_t34;
const Il2CppTypeDefinitionMetadata Enumerable_t34_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t34_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t34_VTable/* vtableMethods */
	, Enumerable_t34_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Enumerable_t34_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t34_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Enumerable_t34_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &Enumerable_t34_0_0_0/* byval_arg */
	, &Enumerable_t34_1_0_0/* this_arg */
	, &Enumerable_t34_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t34)/* instance_size */
	, sizeof (Enumerable_t34)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t939_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Func_2_t939_Il2CppGenericContainer;
extern TypeInfo Func_2_t939_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t939_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t939_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Func_2_t939_gp_TResult_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t939_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t939_Il2CppGenericContainer, NULL, "TResult", 1, 0 };
static const Il2CppGenericParameter* Func_2_t939_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t939_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t939_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Func_2_t939_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Func_2_t939_il2cpp_TypeInfo, 2, 0, Func_2_t939_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Func_2_t939_Func_2__ctor_m3764_ParameterInfos[] = 
{
	{"object", 0, 134217746, 0, &Object_t_0_0_0},
	{"method", 1, 134217747, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Func_2__ctor_m3764_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t939_Func_2__ctor_m3764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t939_gp_0_0_0_0;
extern const Il2CppType Func_2_t939_gp_0_0_0_0;
static const ParameterInfo Func_2_t939_Func_2_Invoke_m3765_ParameterInfos[] = 
{
	{"arg1", 0, 134217748, 0, &Func_2_t939_gp_0_0_0_0},
};
extern const Il2CppType Func_2_t939_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
extern const MethodInfo Func_2_Invoke_m3765_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t939_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t939_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t939_Func_2_Invoke_m3765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t939_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Func_2_t939_Func_2_BeginInvoke_m3766_ParameterInfos[] = 
{
	{"arg1", 0, 134217749, 0, &Func_2_t939_gp_0_0_0_0},
	{"callback", 1, 134217750, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134217751, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Func_2_BeginInvoke_m3766_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t939_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t939_Func_2_BeginInvoke_m3766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Func_2_t939_Func_2_EndInvoke_m3767_ParameterInfos[] = 
{
	{"result", 0, 134217752, 0, &IAsyncResult_t213_0_0_0},
};
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Func_2_EndInvoke_m3767_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t939_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t939_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t939_Func_2_EndInvoke_m3767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Func_2_t939_MethodInfos[] =
{
	&Func_2__ctor_m3764_MethodInfo,
	&Func_2_Invoke_m3765_MethodInfo,
	&Func_2_BeginInvoke_m3766_MethodInfo,
	&Func_2_EndInvoke_m3767_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo Func_2_Invoke_m3765_MethodInfo;
extern const MethodInfo Func_2_BeginInvoke_m3766_MethodInfo;
extern const MethodInfo Func_2_EndInvoke_m3767_MethodInfo;
static const Il2CppMethodReference Func_2_t939_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Func_2_Invoke_m3765_MethodInfo,
	&Func_2_BeginInvoke_m3766_MethodInfo,
	&Func_2_EndInvoke_m3767_MethodInfo,
};
static bool Func_2_t939_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
extern const Il2CppType ISerializable_t428_0_0_0;
static Il2CppInterfaceOffsetPair Func_2_t939_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Func_2_t939_0_0_0;
extern const Il2CppType Func_2_t939_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
struct Func_2_t939;
const Il2CppTypeDefinitionMetadata Func_2_t939_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t939_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Func_2_t939_VTable/* vtableMethods */
	, Func_2_t939_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Func_2_t939_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Func_2_t939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t939_0_0_0/* byval_arg */
	, &Func_2_t939_1_0_0/* this_arg */
	, &Func_2_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_2_t939_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
