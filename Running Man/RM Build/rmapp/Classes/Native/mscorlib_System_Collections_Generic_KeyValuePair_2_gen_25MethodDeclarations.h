﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct KeyValuePair_2_t2947;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18187(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2947 *, String_t*, bool, const MethodInfo*))KeyValuePair_2__ctor_m18094_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m18188(__this, method) (( String_t* (*) (KeyValuePair_2_t2947 *, const MethodInfo*))KeyValuePair_2_get_Key_m18095_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18189(__this, ___value, method) (( void (*) (KeyValuePair_2_t2947 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m18096_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m18190(__this, method) (( bool (*) (KeyValuePair_2_t2947 *, const MethodInfo*))KeyValuePair_2_get_Value_m18097_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18191(__this, ___value, method) (( void (*) (KeyValuePair_2_t2947 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m18098_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m18192(__this, method) (( String_t* (*) (KeyValuePair_2_t2947 *, const MethodInfo*))KeyValuePair_2_ToString_m18099_gshared)(__this, method)
