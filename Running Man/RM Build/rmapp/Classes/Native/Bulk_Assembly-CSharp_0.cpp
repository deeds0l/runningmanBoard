﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// AudioManager
#include "AssemblyU2DCSharp_AudioManager.h"
#ifndef _MSC_VER
#else
#endif
// AudioManager
#include "AssemblyU2DCSharp_AudioManagerMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// AudioPooler
#include "AssemblyU2DCSharp_AudioPooler.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.String
#include "mscorlib_System_String.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPE.h"
// System.Object
#include "mscorlib_System_Object.h"
// MonoManager`1<AudioManager>
#include "AssemblyU2DCSharp_MonoManager_1_genMethodDeclarations.h"
// AudioPooler
#include "AssemblyU2DCSharp_AudioPoolerMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"


// System.Void AudioManager::.ctor()
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1__ctor_m76_MethodInfo_var;
extern "C" void AudioManager__ctor_m0 (AudioManager_t2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		MonoManager_1__ctor_m76_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		MonoManager_1__ctor_m76(__this, /*hidden argument*/MonoManager_1__ctor_m76_MethodInfo_var);
		return;
	}
}
// System.Void AudioManager::Awake()
extern "C" void AudioManager_Awake_m1 (AudioManager_t2 * __this, const MethodInfo* method)
{
	{
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioPooler_CreatePool_m37(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::PlayMusic(System.String,System.Boolean)
extern "C" void AudioManager_PlayMusic_m2 (AudioManager_t2 * __this, String_t* ___p_soundName, bool ___p_bisLooping, const MethodInfo* method)
{
	AudioSource_t9 * V_0 = {0};
	{
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t9 * L_2 = AudioPooler_GetAudioSource_m38(L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		AudioSource_t9 * L_3 = V_0;
		bool L_4 = ___p_bisLooping;
		NullCheck(L_3);
		AudioSource_set_loop_m78(L_3, L_4, /*hidden argument*/NULL);
		AudioPooler_t10 * L_5 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = ___p_soundName;
		AudioSource_t9 * L_7 = V_0;
		bool L_8 = ___p_bisLooping;
		NullCheck(L_5);
		AudioPooler_SearchMusic_m43(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::PlaySound(System.String,System.Boolean)
extern "C" void AudioManager_PlaySound_m3 (AudioManager_t2 * __this, String_t* ___p_soundName, bool ___p_bisLooping, const MethodInfo* method)
{
	AudioSource_t9 * V_0 = {0};
	{
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t9 * L_2 = AudioPooler_GetAudioSource_m38(L_0, L_1, 1, /*hidden argument*/NULL);
		V_0 = L_2;
		AudioSource_t9 * L_3 = V_0;
		bool L_4 = ___p_bisLooping;
		NullCheck(L_3);
		AudioSource_set_loop_m78(L_3, L_4, /*hidden argument*/NULL);
		AudioPooler_t10 * L_5 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = ___p_soundName;
		AudioSource_t9 * L_7 = V_0;
		bool L_8 = ___p_bisLooping;
		NullCheck(L_5);
		AudioPooler_SearchSound_m44(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::StopAudio()
extern "C" void AudioManager_StopAudio_m4 (AudioManager_t2 * __this, const MethodInfo* method)
{
	{
		Debug_Log_m79(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioPooler_ClearAudioSource_m40(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::StopMusic()
extern "C" void AudioManager_StopMusic_m5 (AudioManager_t2 * __this, const MethodInfo* method)
{
	{
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioPooler_ClearMusic_m41(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::StopSoundEffects()
extern "C" void AudioManager_StopSoundEffects_m6 (AudioManager_t2 * __this, const MethodInfo* method)
{
	{
		Debug_Log_m79(NULL /*static, unused*/, (String_t*) &_stringLiteral2, /*hidden argument*/NULL);
		AudioPooler_t10 * L_0 = AudioPooler_get_Instance_m35(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t5 * L_1 = Component_get_gameObject_m77(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioPooler_ClearSoundEffects_m42(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPEMethodDeclarations.h"



// AudioPooler/objectSource
#include "AssemblyU2DCSharp_AudioPooler_objectSource.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/objectSource
#include "AssemblyU2DCSharp_AudioPooler_objectSourceMethodDeclarations.h"

// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void AudioPooler/objectSource::.ctor()
extern "C" void objectSource__ctor_m7 (objectSource_t8 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// AudioPooler/<DisableMusic>c__Iterator0
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableMusicU3Ec__Iterator0.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<DisableMusic>c__Iterator0
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableMusicU3Ec__Iterator0MethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"


// System.Void AudioPooler/<DisableMusic>c__Iterator0::.ctor()
extern "C" void U3CDisableMusicU3Ec__Iterator0__ctor_m8 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean AudioPooler/<DisableMusic>c__Iterator0::MoveNext()
extern TypeInfo* WaitForSeconds_t31_il2cpp_TypeInfo_var;
extern "C" bool U3CDisableMusicU3Ec__Iterator0_MoveNext_m11 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0053;
		}
	}
	{
		goto IL_00ad;
	}

IL_0021:
	{
		AudioSource_t9 * L_2 = (__this->___p_audioSource_0);
		NullCheck(L_2);
		AudioSource_Play_m81(L_2, /*hidden argument*/NULL);
		AudioPooler_t10 * L_3 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_3);
		AudioClip_t20 * L_4 = (L_3->___m_currentAudioClip_4);
		NullCheck(L_4);
		float L_5 = AudioClip_get_length_m82(L_4, /*hidden argument*/NULL);
		WaitForSeconds_t31 * L_6 = (WaitForSeconds_t31 *)il2cpp_codegen_object_new (WaitForSeconds_t31_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m83(L_6, L_5, /*hidden argument*/NULL);
		__this->___U24current_3 = L_6;
		__this->___U24PC_2 = 1;
		goto IL_00af;
	}

IL_0053:
	{
		bool L_7 = (__this->___p_bisLooping_1);
		if (L_7)
		{
			goto IL_009c;
		}
	}
	{
		AudioSource_t9 * L_8 = (__this->___p_audioSource_0);
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m84(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_009c;
		}
	}
	{
		AudioPooler_t10 * L_10 = (__this->___U3CU3Ef__this_6);
		AudioSource_t9 * L_11 = (__this->___p_audioSource_0);
		NullCheck(L_10);
		AudioPooler_ReturnAudioSource_m39(L_10, L_11, 0, /*hidden argument*/NULL);
		AudioPooler_t10 * L_12 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_12);
		objectSource_t8 * L_13 = (L_12->___newObjectSource_5);
		NullCheck(L_13);
		List_1_t6 * L_14 = (L_13->___m_spawnedMusicSource_1);
		AudioSource_t9 * L_15 = (__this->___p_audioSource_0);
		NullCheck(L_14);
		VirtFuncInvoker1< bool, AudioSource_t9 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Remove(!0) */, L_14, L_15);
	}

IL_009c:
	{
		Debug_Log_m79(NULL /*static, unused*/, (String_t*) &_stringLiteral4, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_00ad:
	{
		return 0;
	}

IL_00af:
	{
		return 1;
	}
	// Dead block : IL_00b1: ldloc.1
}
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Dispose()
extern "C" void U3CDisableMusicU3Ec__Iterator0_Dispose_m12 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t32_il2cpp_TypeInfo_var;
extern "C" void U3CDisableMusicU3Ec__Iterator0_Reset_m13 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t32 * L_0 = (NotSupportedException_t32 *)il2cpp_codegen_object_new (NotSupportedException_t32_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m85(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// AudioPooler/<DisableSound>c__Iterator1
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableSoundU3Ec__Iterator1.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<DisableSound>c__Iterator1
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableSoundU3Ec__Iterator1MethodDeclarations.h"



// System.Void AudioPooler/<DisableSound>c__Iterator1::.ctor()
extern "C" void U3CDisableSoundU3Ec__Iterator1__ctor_m14 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean AudioPooler/<DisableSound>c__Iterator1::MoveNext()
extern TypeInfo* WaitForSeconds_t31_il2cpp_TypeInfo_var;
extern "C" bool U3CDisableSoundU3Ec__Iterator1_MoveNext_m17 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0053;
		}
	}
	{
		goto IL_00ad;
	}

IL_0021:
	{
		AudioSource_t9 * L_2 = (__this->___p_audioSource_0);
		NullCheck(L_2);
		AudioSource_Play_m81(L_2, /*hidden argument*/NULL);
		AudioPooler_t10 * L_3 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_3);
		AudioClip_t20 * L_4 = (L_3->___m_currentAudioClip_4);
		NullCheck(L_4);
		float L_5 = AudioClip_get_length_m82(L_4, /*hidden argument*/NULL);
		WaitForSeconds_t31 * L_6 = (WaitForSeconds_t31 *)il2cpp_codegen_object_new (WaitForSeconds_t31_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m83(L_6, L_5, /*hidden argument*/NULL);
		__this->___U24current_3 = L_6;
		__this->___U24PC_2 = 1;
		goto IL_00af;
	}

IL_0053:
	{
		bool L_7 = (__this->___p_bisLooping_1);
		if (L_7)
		{
			goto IL_00a6;
		}
	}
	{
		AudioSource_t9 * L_8 = (__this->___p_audioSource_0);
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m84(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00a6;
		}
	}
	{
		AudioPooler_t10 * L_10 = (__this->___U3CU3Ef__this_6);
		AudioSource_t9 * L_11 = (__this->___p_audioSource_0);
		NullCheck(L_10);
		AudioPooler_ReturnAudioSource_m39(L_10, L_11, 1, /*hidden argument*/NULL);
		AudioPooler_t10 * L_12 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_12);
		objectSource_t8 * L_13 = (L_12->___newObjectSource_5);
		NullCheck(L_13);
		List_1_t6 * L_14 = (L_13->___m_spawnedSoundSource_3);
		AudioSource_t9 * L_15 = (__this->___p_audioSource_0);
		NullCheck(L_14);
		VirtFuncInvoker1< bool, AudioSource_t9 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Remove(!0) */, L_14, L_15);
		Debug_Log_m79(NULL /*static, unused*/, (String_t*) &_stringLiteral5, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00ad:
	{
		return 0;
	}

IL_00af:
	{
		return 1;
	}
	// Dead block : IL_00b1: ldloc.1
}
// System.Void AudioPooler/<DisableSound>c__Iterator1::Dispose()
extern "C" void U3CDisableSoundU3Ec__Iterator1_Dispose_m18 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void AudioPooler/<DisableSound>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t32_il2cpp_TypeInfo_var;
extern "C" void U3CDisableSoundU3Ec__Iterator1_Reset_m19 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t32 * L_0 = (NotSupportedException_t32 *)il2cpp_codegen_object_new (NotSupportedException_t32_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m85(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// AudioPooler/<GetAudioSource>c__AnonStorey6
#include "AssemblyU2DCSharp_AudioPooler_U3CGetAudioSourceU3Ec__AnonSto.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<GetAudioSource>c__AnonStorey6
#include "AssemblyU2DCSharp_AudioPooler_U3CGetAudioSourceU3Ec__AnonStoMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"


// System.Void AudioPooler/<GetAudioSource>c__AnonStorey6::.ctor()
extern "C" void U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20 (U3CGetAudioSourceU3Ec__AnonStorey6_t13 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<GetAudioSource>c__AnonStorey6::<>m__0(AudioPooler/objectSource)
extern "C" bool U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21 (U3CGetAudioSourceU3Ec__AnonStorey6_t13 * __this, objectSource_t8 * ___audio, const MethodInfo* method)
{
	{
		objectSource_t8 * L_0 = ___audio;
		NullCheck(L_0);
		GameObject_t5 * L_1 = (L_0->___m_audioContainer_0);
		GameObject_t5 * L_2 = (__this->___p_audioContainer_0);
		bool L_3 = Object_op_Equality_m86(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
#include "AssemblyU2DCSharp_AudioPooler_U3CReturnAudioSourceU3Ec__Anon.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
#include "AssemblyU2DCSharp_AudioPooler_U3CReturnAudioSourceU3Ec__AnonMethodDeclarations.h"

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"


// System.Void AudioPooler/<ReturnAudioSource>c__AnonStorey7::.ctor()
extern "C" void U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22 (U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<ReturnAudioSource>c__AnonStorey7::<>m__1(AudioPooler/objectSource)
extern "C" bool U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23 (U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * __this, objectSource_t8 * ___audio, const MethodInfo* method)
{
	{
		objectSource_t8 * L_0 = ___audio;
		NullCheck(L_0);
		GameObject_t5 * L_1 = (L_0->___m_audioContainer_0);
		AudioSource_t9 * L_2 = (__this->___p_currentAudioSource_0);
		NullCheck(L_2);
		Transform_t30 * L_3 = Component_get_transform_m87(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t5 * L_4 = Component_get_gameObject_m77(L_3, /*hidden argument*/NULL);
		bool L_5 = Object_op_Equality_m86(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// AudioPooler/<ClearAudioSource>c__AnonStorey8
#include "AssemblyU2DCSharp_AudioPooler_U3CClearAudioSourceU3Ec__AnonS.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<ClearAudioSource>c__AnonStorey8
#include "AssemblyU2DCSharp_AudioPooler_U3CClearAudioSourceU3Ec__AnonSMethodDeclarations.h"



// System.Void AudioPooler/<ClearAudioSource>c__AnonStorey8::.ctor()
extern "C" void U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24 (U3CClearAudioSourceU3Ec__AnonStorey8_t15 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<ClearAudioSource>c__AnonStorey8::<>m__2(AudioPooler/objectSource)
extern "C" bool U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25 (U3CClearAudioSourceU3Ec__AnonStorey8_t15 * __this, objectSource_t8 * ___audio, const MethodInfo* method)
{
	{
		objectSource_t8 * L_0 = ___audio;
		NullCheck(L_0);
		GameObject_t5 * L_1 = (L_0->___m_audioContainer_0);
		GameObject_t5 * L_2 = (__this->___p_audioContainer_0);
		bool L_3 = Object_op_Equality_m86(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// AudioPooler/<ClearMusic>c__AnonStorey9
#include "AssemblyU2DCSharp_AudioPooler_U3CClearMusicU3Ec__AnonStorey9.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<ClearMusic>c__AnonStorey9
#include "AssemblyU2DCSharp_AudioPooler_U3CClearMusicU3Ec__AnonStorey9MethodDeclarations.h"



// System.Void AudioPooler/<ClearMusic>c__AnonStorey9::.ctor()
extern "C" void U3CClearMusicU3Ec__AnonStorey9__ctor_m26 (U3CClearMusicU3Ec__AnonStorey9_t16 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<ClearMusic>c__AnonStorey9::<>m__3(AudioPooler/objectSource)
extern "C" bool U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27 (U3CClearMusicU3Ec__AnonStorey9_t16 * __this, objectSource_t8 * ___audio, const MethodInfo* method)
{
	{
		objectSource_t8 * L_0 = ___audio;
		NullCheck(L_0);
		GameObject_t5 * L_1 = (L_0->___m_audioContainer_0);
		GameObject_t5 * L_2 = (__this->___p_audioContainer_0);
		bool L_3 = Object_op_Equality_m86(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
#include "AssemblyU2DCSharp_AudioPooler_U3CClearSoundEffectsU3Ec__Anon.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
#include "AssemblyU2DCSharp_AudioPooler_U3CClearSoundEffectsU3Ec__AnonMethodDeclarations.h"



// System.Void AudioPooler/<ClearSoundEffects>c__AnonStoreyA::.ctor()
extern "C" void U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28 (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<ClearSoundEffects>c__AnonStoreyA::<>m__4(AudioPooler/objectSource)
extern "C" bool U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29 (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * __this, objectSource_t8 * ___audio, const MethodInfo* method)
{
	{
		objectSource_t8 * L_0 = ___audio;
		NullCheck(L_0);
		GameObject_t5 * L_1 = (L_0->___m_audioContainer_0);
		GameObject_t5 * L_2 = (__this->___p_audioContainer_0);
		bool L_3 = Object_op_Equality_m86(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// AudioPooler/<SearchMusic>c__AnonStoreyB
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchMusicU3Ec__AnonStorey.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<SearchMusic>c__AnonStoreyB
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchMusicU3Ec__AnonStoreyMethodDeclarations.h"

// System.String
#include "mscorlib_System_StringMethodDeclarations.h"


// System.Void AudioPooler/<SearchMusic>c__AnonStoreyB::.ctor()
extern "C" void U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30 (U3CSearchMusicU3Ec__AnonStoreyB_t18 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<SearchMusic>c__AnonStoreyB::<>m__5(UnityEngine.AudioClip)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31 (U3CSearchMusicU3Ec__AnonStoreyB_t18 * __this, AudioClip_t20 * ___clip, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioClip_t20 * L_0 = ___clip;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m88(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___key_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m89(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// AudioPooler/<SearchSound>c__AnonStoreyC
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchSoundU3Ec__AnonStorey.h"
#ifndef _MSC_VER
#else
#endif
// AudioPooler/<SearchSound>c__AnonStoreyC
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchSoundU3Ec__AnonStoreyMethodDeclarations.h"



// System.Void AudioPooler/<SearchSound>c__AnonStoreyC::.ctor()
extern "C" void U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32 (U3CSearchSoundU3Ec__AnonStoreyC_t19 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioPooler/<SearchSound>c__AnonStoreyC::<>m__6(UnityEngine.AudioClip)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33 (U3CSearchSoundU3Ec__AnonStoreyC_t19 * __this, AudioClip_t20 * ___clip, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioClip_t20 * L_0 = ___clip;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m88(L_0, /*hidden argument*/NULL);
		String_t* L_2 = (__this->___key_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m89(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.List`1<AudioPooler/objectSource>
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
#include "System_System_Collections_Generic_Queue_1_gen.h"
// System.Func`2<AudioPooler/objectSource,System.Boolean>
#include "System_Core_System_Func_2_gen.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
#include "mscorlib_System_Collections_Generic_List_1_gen_1.h"
// System.Func`2<UnityEngine.AudioClip,System.Boolean>
#include "System_Core_System_Func_2_gen_0.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// System.Collections.Generic.List`1<AudioPooler/objectSource>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
#include "System_System_Collections_Generic_Queue_1_genMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// System.Func`2<AudioPooler/objectSource,System.Boolean>
#include "System_Core_System_Func_2_genMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
// System.Func`2<UnityEngine.AudioClip,System.Boolean>
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"
struct Object_t33;
struct AudioPooler_t10;
struct Object_t33;
struct Object_t;
// Declaration !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m91_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m91(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m91_gshared)(__this /* static, unused */, method)
// Declaration !!0 UnityEngine.Object::FindObjectOfType<AudioPooler>()
// !!0 UnityEngine.Object::FindObjectOfType<AudioPooler>()
#define Object_FindObjectOfType_TisAudioPooler_t10_m90(__this /* static, unused */, method) (( AudioPooler_t10 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m91_gshared)(__this /* static, unused */, method)
struct Enumerable_t34;
struct objectSource_t8;
struct IEnumerable_1_t35;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t34;
struct Object_t;
struct IEnumerable_1_t36;
// Declaration !!0 System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0 System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" Object_t * Enumerable_Last_TisObject_t_m93_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_Last_TisObject_t_m93(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Last_TisObject_t_m93_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 System.Linq.Enumerable::Last<AudioPooler/objectSource>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0 System.Linq.Enumerable::Last<AudioPooler/objectSource>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Last_TisobjectSource_t8_m92(__this /* static, unused */, p0, method) (( objectSource_t8 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Last_TisObject_t_m93_gshared)(__this /* static, unused */, p0, method)
struct GameObject_t5;
struct AudioSource_t9;
struct GameObject_t5;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m95_gshared (GameObject_t5 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m95(__this, method) (( Object_t * (*) (GameObject_t5 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m95_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
#define GameObject_AddComponent_TisAudioSource_t9_m94(__this, method) (( AudioSource_t9 * (*) (GameObject_t5 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m95_gshared)(__this, method)
struct Enumerable_t34;
struct objectSource_t8;
struct IEnumerable_1_t35;
struct Func_2_t37;
struct Enumerable_t34;
struct Object_t;
struct IEnumerable_1_t36;
struct Func_2_t38;
// Declaration !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t * Enumerable_FirstOrDefault_TisObject_t_m97_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t38 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisObject_t_m97(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t38 *, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m97_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration !!0 System.Linq.Enumerable::FirstOrDefault<AudioPooler/objectSource>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// !!0 System.Linq.Enumerable::FirstOrDefault<AudioPooler/objectSource>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_FirstOrDefault_TisobjectSource_t8_m96(__this /* static, unused */, p0, p1, method) (( objectSource_t8 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t37 *, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m97_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t34;
struct AudioClip_t20;
struct IEnumerable_1_t39;
struct Func_2_t40;
// Declaration !!0 System.Linq.Enumerable::FirstOrDefault<UnityEngine.AudioClip>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// !!0 System.Linq.Enumerable::FirstOrDefault<UnityEngine.AudioClip>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_FirstOrDefault_TisAudioClip_t20_m98(__this /* static, unused */, p0, p1, method) (( AudioClip_t20 * (*) (Object_t * /* static, unused */, Object_t*, Func_2_t40 *, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m97_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void AudioPooler::.ctor()
extern TypeInfo* List_1_t21_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m99_MethodInfo_var;
extern "C" void AudioPooler__ctor_m34 (AudioPooler_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		List_1__ctor_m99_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t21 * L_0 = (List_1_t21 *)il2cpp_codegen_object_new (List_1_t21_il2cpp_TypeInfo_var);
		List_1__ctor_m99(L_0, /*hidden argument*/List_1__ctor_m99_MethodInfo_var);
		__this->___m_objectSourceList_9 = L_0;
		MonoBehaviour__ctor_m100(__this, /*hidden argument*/NULL);
		return;
	}
}
// AudioPooler AudioPooler::get_Instance()
extern TypeInfo* AudioPooler_t10_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisAudioPooler_t10_m90_MethodInfo_var;
extern "C" AudioPooler_t10 * AudioPooler_get_Instance_m35 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioPooler_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Object_FindObjectOfType_TisAudioPooler_t10_m90_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioPooler_t10 * L_0 = ((AudioPooler_t10_StaticFields*)AudioPooler_t10_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m86(NULL /*static, unused*/, L_0, (Object_t33 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		AudioPooler_t10 * L_2 = Object_FindObjectOfType_TisAudioPooler_t10_m90(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisAudioPooler_t10_m90_MethodInfo_var);
		((AudioPooler_t10_StaticFields*)AudioPooler_t10_il2cpp_TypeInfo_var->static_fields)->___instance_2 = L_2;
	}

IL_001a:
	{
		AudioPooler_t10 * L_3 = ((AudioPooler_t10_StaticFields*)AudioPooler_t10_il2cpp_TypeInfo_var->static_fields)->___instance_2;
		return L_3;
	}
}
// System.Void AudioPooler::Awake()
extern TypeInfo* AudioPooler_t10_il2cpp_TypeInfo_var;
extern "C" void AudioPooler_Awake_m36 (AudioPooler_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioPooler_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AudioPooler_t10_StaticFields*)AudioPooler_t10_il2cpp_TypeInfo_var->static_fields)->___instance_2 = __this;
		return;
	}
}
// System.Void AudioPooler::CreatePool(UnityEngine.GameObject)
extern TypeInfo* objectSource_t8_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t6_il2cpp_TypeInfo_var;
extern TypeInfo* Queue_1_t7_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Last_TisobjectSource_t8_m92_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m101_MethodInfo_var;
extern const MethodInfo* Queue_1__ctor_m102_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m103_MethodInfo_var;
extern "C" void AudioPooler_CreatePool_m37 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		objectSource_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		List_1_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Queue_1_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Enumerable_Last_TisobjectSource_t8_m92_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		List_1__ctor_m101_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		Queue_1__ctor_m102_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		Queue_1_Enqueue_m103_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	AudioSource_t9 * V_1 = {0};
	{
		List_1_t21 * L_0 = (__this->___m_objectSourceList_9);
		objectSource_t8 * L_1 = (objectSource_t8 *)il2cpp_codegen_object_new (objectSource_t8_il2cpp_TypeInfo_var);
		objectSource__ctor_m7(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< objectSource_t8 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Add(!0) */, L_0, L_1);
		List_1_t21 * L_2 = (__this->___m_objectSourceList_9);
		objectSource_t8 * L_3 = Enumerable_Last_TisobjectSource_t8_m92(NULL /*static, unused*/, L_2, /*hidden argument*/Enumerable_Last_TisobjectSource_t8_m92_MethodInfo_var);
		__this->___newObjectSource_5 = L_3;
		objectSource_t8 * L_4 = (__this->___newObjectSource_5);
		GameObject_t5 * L_5 = ___p_audioContainer;
		NullCheck(L_4);
		L_4->___m_audioContainer_0 = L_5;
		objectSource_t8 * L_6 = (__this->___newObjectSource_5);
		List_1_t6 * L_7 = (List_1_t6 *)il2cpp_codegen_object_new (List_1_t6_il2cpp_TypeInfo_var);
		List_1__ctor_m101(L_7, /*hidden argument*/List_1__ctor_m101_MethodInfo_var);
		NullCheck(L_6);
		L_6->___m_spawnedMusicSource_1 = L_7;
		objectSource_t8 * L_8 = (__this->___newObjectSource_5);
		Queue_1_t7 * L_9 = (Queue_1_t7 *)il2cpp_codegen_object_new (Queue_1_t7_il2cpp_TypeInfo_var);
		Queue_1__ctor_m102(L_9, /*hidden argument*/Queue_1__ctor_m102_MethodInfo_var);
		NullCheck(L_8);
		L_8->___m_musicSourceQueue_2 = L_9;
		objectSource_t8 * L_10 = (__this->___newObjectSource_5);
		List_1_t6 * L_11 = (List_1_t6 *)il2cpp_codegen_object_new (List_1_t6_il2cpp_TypeInfo_var);
		List_1__ctor_m101(L_11, /*hidden argument*/List_1__ctor_m101_MethodInfo_var);
		NullCheck(L_10);
		L_10->___m_spawnedSoundSource_3 = L_11;
		objectSource_t8 * L_12 = (__this->___newObjectSource_5);
		Queue_1_t7 * L_13 = (Queue_1_t7 *)il2cpp_codegen_object_new (Queue_1_t7_il2cpp_TypeInfo_var);
		Queue_1__ctor_m102(L_13, /*hidden argument*/Queue_1__ctor_m102_MethodInfo_var);
		NullCheck(L_12);
		L_12->___m_soundSourceQueue_4 = L_13;
		V_0 = 0;
		goto IL_00a8;
	}

IL_0074:
	{
		GameObject_t5 * L_14 = ___p_audioContainer;
		NullCheck(L_14);
		AudioSource_t9 * L_15 = GameObject_AddComponent_TisAudioSource_t9_m94(L_14, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var);
		V_1 = L_15;
		objectSource_t8 * L_16 = (__this->___newObjectSource_5);
		NullCheck(L_16);
		Queue_1_t7 * L_17 = (L_16->___m_musicSourceQueue_2);
		AudioSource_t9 * L_18 = V_1;
		NullCheck(L_17);
		Queue_1_Enqueue_m103(L_17, L_18, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
		objectSource_t8 * L_19 = (__this->___newObjectSource_5);
		NullCheck(L_19);
		Queue_1_t7 * L_20 = (L_19->___m_soundSourceQueue_4);
		AudioSource_t9 * L_21 = V_1;
		NullCheck(L_20);
		Queue_1_Enqueue_m103(L_20, L_21, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
		AudioSource_t9 * L_22 = V_1;
		NullCheck(L_22);
		Behaviour_set_enabled_m104(L_22, 0, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00a8:
	{
		int32_t L_24 = V_0;
		int32_t L_25 = (__this->___m_audioPreloadCount_6);
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0074;
		}
	}
	{
		return;
	}
}
// UnityEngine.AudioSource AudioPooler::GetAudioSource(UnityEngine.GameObject,AudioPooler/SOUNDTYPE)
extern TypeInfo* U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t37_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m105_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m103_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m106_MethodInfo_var;
extern "C" AudioSource_t9 * AudioPooler_GetAudioSource_m38 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, int32_t ___p_soundType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Func_2_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_MethodInfo_var = il2cpp_codegen_method_info_from_index(9);
		Func_2__ctor_m105_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		Queue_1_Enqueue_m103_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		Queue_1_Dequeue_m106_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t9 * V_0 = {0};
	AudioSource_t9 * V_1 = {0};
	AudioSource_t9 * V_2 = {0};
	U3CGetAudioSourceU3Ec__AnonStorey6_t13 * V_3 = {0};
	{
		U3CGetAudioSourceU3Ec__AnonStorey6_t13 * L_0 = (U3CGetAudioSourceU3Ec__AnonStorey6_t13 *)il2cpp_codegen_object_new (U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo_var);
		U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20(L_0, /*hidden argument*/NULL);
		V_3 = L_0;
		U3CGetAudioSourceU3Ec__AnonStorey6_t13 * L_1 = V_3;
		GameObject_t5 * L_2 = ___p_audioContainer;
		NullCheck(L_1);
		L_1->___p_audioContainer_0 = L_2;
		List_1_t21 * L_3 = (__this->___m_objectSourceList_9);
		U3CGetAudioSourceU3Ec__AnonStorey6_t13 * L_4 = V_3;
		IntPtr_t L_5 = { (void*)U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21_MethodInfo_var };
		Func_2_t37 * L_6 = (Func_2_t37 *)il2cpp_codegen_object_new (Func_2_t37_il2cpp_TypeInfo_var);
		Func_2__ctor_m105(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m105_MethodInfo_var);
		objectSource_t8 * L_7 = Enumerable_FirstOrDefault_TisobjectSource_t8_m96(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var);
		__this->___newObjectSource_5 = L_7;
		int32_t L_8 = ___p_soundType;
		if (L_8)
		{
			goto IL_0098;
		}
	}
	{
		objectSource_t8 * L_9 = (__this->___newObjectSource_5);
		NullCheck(L_9);
		Queue_1_t7 * L_10 = (L_9->___m_musicSourceQueue_2);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::get_Count() */, L_10);
		if ((((int32_t)L_11) > ((int32_t)0)))
		{
			goto IL_006a;
		}
	}
	{
		U3CGetAudioSourceU3Ec__AnonStorey6_t13 * L_12 = V_3;
		NullCheck(L_12);
		GameObject_t5 * L_13 = (L_12->___p_audioContainer_0);
		NullCheck(L_13);
		AudioSource_t9 * L_14 = GameObject_AddComponent_TisAudioSource_t9_m94(L_13, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var);
		V_1 = L_14;
		objectSource_t8 * L_15 = (__this->___newObjectSource_5);
		NullCheck(L_15);
		Queue_1_t7 * L_16 = (L_15->___m_musicSourceQueue_2);
		AudioSource_t9 * L_17 = V_1;
		NullCheck(L_16);
		Queue_1_Enqueue_m103(L_16, L_17, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
		AudioSource_t9 * L_18 = V_1;
		NullCheck(L_18);
		Behaviour_set_enabled_m104(L_18, 0, /*hidden argument*/NULL);
	}

IL_006a:
	{
		objectSource_t8 * L_19 = (__this->___newObjectSource_5);
		NullCheck(L_19);
		Queue_1_t7 * L_20 = (L_19->___m_musicSourceQueue_2);
		NullCheck(L_20);
		AudioSource_t9 * L_21 = Queue_1_Dequeue_m106(L_20, /*hidden argument*/Queue_1_Dequeue_m106_MethodInfo_var);
		V_0 = L_21;
		AudioSource_t9 * L_22 = V_0;
		NullCheck(L_22);
		Behaviour_set_enabled_m104(L_22, 1, /*hidden argument*/NULL);
		objectSource_t8 * L_23 = (__this->___newObjectSource_5);
		NullCheck(L_23);
		List_1_t6 * L_24 = (L_23->___m_spawnedMusicSource_1);
		AudioSource_t9 * L_25 = V_0;
		NullCheck(L_24);
		VirtActionInvoker1< AudioSource_t9 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Add(!0) */, L_24, L_25);
		goto IL_00fb;
	}

IL_0098:
	{
		objectSource_t8 * L_26 = (__this->___newObjectSource_5);
		NullCheck(L_26);
		Queue_1_t7 * L_27 = (L_26->___m_soundSourceQueue_4);
		NullCheck(L_27);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.Generic.Queue`1<UnityEngine.AudioSource>::get_Count() */, L_27);
		if ((((int32_t)L_28) > ((int32_t)0)))
		{
			goto IL_00d2;
		}
	}
	{
		U3CGetAudioSourceU3Ec__AnonStorey6_t13 * L_29 = V_3;
		NullCheck(L_29);
		GameObject_t5 * L_30 = (L_29->___p_audioContainer_0);
		NullCheck(L_30);
		AudioSource_t9 * L_31 = GameObject_AddComponent_TisAudioSource_t9_m94(L_30, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t9_m94_MethodInfo_var);
		V_2 = L_31;
		objectSource_t8 * L_32 = (__this->___newObjectSource_5);
		NullCheck(L_32);
		Queue_1_t7 * L_33 = (L_32->___m_soundSourceQueue_4);
		AudioSource_t9 * L_34 = V_2;
		NullCheck(L_33);
		Queue_1_Enqueue_m103(L_33, L_34, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
		AudioSource_t9 * L_35 = V_2;
		NullCheck(L_35);
		Behaviour_set_enabled_m104(L_35, 0, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		objectSource_t8 * L_36 = (__this->___newObjectSource_5);
		NullCheck(L_36);
		Queue_1_t7 * L_37 = (L_36->___m_soundSourceQueue_4);
		NullCheck(L_37);
		AudioSource_t9 * L_38 = Queue_1_Dequeue_m106(L_37, /*hidden argument*/Queue_1_Dequeue_m106_MethodInfo_var);
		V_0 = L_38;
		AudioSource_t9 * L_39 = V_0;
		NullCheck(L_39);
		Behaviour_set_enabled_m104(L_39, 1, /*hidden argument*/NULL);
		objectSource_t8 * L_40 = (__this->___newObjectSource_5);
		NullCheck(L_40);
		List_1_t6 * L_41 = (L_40->___m_spawnedSoundSource_3);
		AudioSource_t9 * L_42 = V_0;
		NullCheck(L_41);
		VirtActionInvoker1< AudioSource_t9 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Add(!0) */, L_41, L_42);
	}

IL_00fb:
	{
		AudioSource_t9 * L_43 = V_0;
		return L_43;
	}
}
// System.Void AudioPooler::ReturnAudioSource(UnityEngine.AudioSource,AudioPooler/SOUNDTYPE)
extern TypeInfo* U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t37_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m105_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m103_MethodInfo_var;
extern "C" void AudioPooler_ReturnAudioSource_m39 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_currentAudioSource, int32_t ___p_soundType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Func_2_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_MethodInfo_var = il2cpp_codegen_method_info_from_index(13);
		Func_2__ctor_m105_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		Queue_1_Enqueue_m103_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * V_0 = {0};
	{
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_0 = (U3CReturnAudioSourceU3Ec__AnonStorey7_t14 *)il2cpp_codegen_object_new (U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo_var);
		U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_1 = V_0;
		AudioSource_t9 * L_2 = ___p_currentAudioSource;
		NullCheck(L_1);
		L_1->___p_currentAudioSource_0 = L_2;
		List_1_t21 * L_3 = (__this->___m_objectSourceList_9);
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23_MethodInfo_var };
		Func_2_t37 * L_6 = (Func_2_t37 *)il2cpp_codegen_object_new (Func_2_t37_il2cpp_TypeInfo_var);
		Func_2__ctor_m105(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m105_MethodInfo_var);
		objectSource_t8 * L_7 = Enumerable_FirstOrDefault_TisobjectSource_t8_m96(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var);
		__this->___newObjectSource_5 = L_7;
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_8 = V_0;
		NullCheck(L_8);
		AudioSource_t9 * L_9 = (L_8->___p_currentAudioSource_0);
		NullCheck(L_9);
		Behaviour_set_enabled_m104(L_9, 0, /*hidden argument*/NULL);
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_10 = V_0;
		NullCheck(L_10);
		AudioSource_t9 * L_11 = (L_10->___p_currentAudioSource_0);
		NullCheck(L_11);
		AudioClip_t20 * L_12 = AudioSource_get_clip_m107(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_m88(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m108(NULL /*static, unused*/, (String_t*) &_stringLiteral3, L_13, /*hidden argument*/NULL);
		Debug_Log_m79(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		int32_t L_15 = ___p_soundType;
		if (L_15)
		{
			goto IL_0076;
		}
	}
	{
		objectSource_t8 * L_16 = (__this->___newObjectSource_5);
		NullCheck(L_16);
		Queue_1_t7 * L_17 = (L_16->___m_musicSourceQueue_2);
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_18 = V_0;
		NullCheck(L_18);
		AudioSource_t9 * L_19 = (L_18->___p_currentAudioSource_0);
		NullCheck(L_17);
		Queue_1_Enqueue_m103(L_17, L_19, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
		goto IL_008c;
	}

IL_0076:
	{
		objectSource_t8 * L_20 = (__this->___newObjectSource_5);
		NullCheck(L_20);
		Queue_1_t7 * L_21 = (L_20->___m_soundSourceQueue_4);
		U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * L_22 = V_0;
		NullCheck(L_22);
		AudioSource_t9 * L_23 = (L_22->___p_currentAudioSource_0);
		NullCheck(L_21);
		Queue_1_Enqueue_m103(L_21, L_23, /*hidden argument*/Queue_1_Enqueue_m103_MethodInfo_var);
	}

IL_008c:
	{
		return;
	}
}
// System.Void AudioPooler::ClearAudioSource(UnityEngine.GameObject)
extern TypeInfo* U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t41_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t43_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m105_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m109_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m110_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m111_MethodInfo_var;
extern "C" void AudioPooler_ClearAudioSource_m40 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		Func_2_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_MethodInfo_var = il2cpp_codegen_method_info_from_index(14);
		Func_2__ctor_m105_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		List_1_GetEnumerator_m109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		Enumerator_get_Current_m110_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Enumerator_MoveNext_m111_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t9 * V_0 = {0};
	Enumerator_t41  V_1 = {0};
	AudioSource_t9 * V_2 = {0};
	Enumerator_t41  V_3 = {0};
	U3CClearAudioSourceU3Ec__AnonStorey8_t15 * V_4 = {0};
	Exception_t42 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t42 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CClearAudioSourceU3Ec__AnonStorey8_t15 * L_0 = (U3CClearAudioSourceU3Ec__AnonStorey8_t15 *)il2cpp_codegen_object_new (U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo_var);
		U3CClearAudioSourceU3Ec__AnonStorey8__ctor_m24(L_0, /*hidden argument*/NULL);
		V_4 = L_0;
		U3CClearAudioSourceU3Ec__AnonStorey8_t15 * L_1 = V_4;
		GameObject_t5 * L_2 = ___p_audioContainer;
		NullCheck(L_1);
		L_1->___p_audioContainer_0 = L_2;
		List_1_t21 * L_3 = (__this->___m_objectSourceList_9);
		U3CClearAudioSourceU3Ec__AnonStorey8_t15 * L_4 = V_4;
		IntPtr_t L_5 = { (void*)U3CClearAudioSourceU3Ec__AnonStorey8_U3CU3Em__2_m25_MethodInfo_var };
		Func_2_t37 * L_6 = (Func_2_t37 *)il2cpp_codegen_object_new (Func_2_t37_il2cpp_TypeInfo_var);
		Func_2__ctor_m105(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m105_MethodInfo_var);
		objectSource_t8 * L_7 = Enumerable_FirstOrDefault_TisobjectSource_t8_m96(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var);
		__this->___newObjectSource_5 = L_7;
		objectSource_t8 * L_8 = (__this->___newObjectSource_5);
		NullCheck(L_8);
		List_1_t6 * L_9 = (L_8->___m_spawnedMusicSource_1);
		NullCheck(L_9);
		Enumerator_t41  L_10 = List_1_GetEnumerator_m109(L_9, /*hidden argument*/List_1_GetEnumerator_m109_MethodInfo_var);
		V_1 = L_10;
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0053;
		}

IL_0043:
		{
			AudioSource_t9 * L_11 = Enumerator_get_Current_m110((&V_1), /*hidden argument*/Enumerator_get_Current_m110_MethodInfo_var);
			V_0 = L_11;
			AudioSource_t9 * L_12 = V_0;
			AudioPooler_ReturnAudioSource_m39(__this, L_12, 0, /*hidden argument*/NULL);
		}

IL_0053:
		{
			bool L_13 = Enumerator_MoveNext_m111((&V_1), /*hidden argument*/Enumerator_MoveNext_m111_MethodInfo_var);
			if (L_13)
			{
				goto IL_0043;
			}
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x70, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t42 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Enumerator_t41  L_14 = V_1;
		Enumerator_t41  L_15 = L_14;
		Object_t * L_16 = Box(Enumerator_t41_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t43_il2cpp_TypeInfo_var, L_16);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t42 *)
	}

IL_0070:
	{
		objectSource_t8 * L_17 = (__this->___newObjectSource_5);
		NullCheck(L_17);
		List_1_t6 * L_18 = (L_17->___m_spawnedSoundSource_3);
		NullCheck(L_18);
		Enumerator_t41  L_19 = List_1_GetEnumerator_m109(L_18, /*hidden argument*/List_1_GetEnumerator_m109_MethodInfo_var);
		V_3 = L_19;
	}

IL_0081:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0096;
		}

IL_0086:
		{
			AudioSource_t9 * L_20 = Enumerator_get_Current_m110((&V_3), /*hidden argument*/Enumerator_get_Current_m110_MethodInfo_var);
			V_2 = L_20;
			AudioSource_t9 * L_21 = V_2;
			AudioPooler_ReturnAudioSource_m39(__this, L_21, 1, /*hidden argument*/NULL);
		}

IL_0096:
		{
			bool L_22 = Enumerator_MoveNext_m111((&V_3), /*hidden argument*/Enumerator_MoveNext_m111_MethodInfo_var);
			if (L_22)
			{
				goto IL_0086;
			}
		}

IL_00a2:
		{
			IL2CPP_LEAVE(0xB3, FINALLY_00a7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t42 *)e.ex;
		goto FINALLY_00a7;
	}

FINALLY_00a7:
	{ // begin finally (depth: 1)
		Enumerator_t41  L_23 = V_3;
		Enumerator_t41  L_24 = L_23;
		Object_t * L_25 = Box(Enumerator_t41_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_25);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t43_il2cpp_TypeInfo_var, L_25);
		IL2CPP_END_FINALLY(167)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(167)
	{
		IL2CPP_JUMP_TBL(0xB3, IL_00b3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t42 *)
	}

IL_00b3:
	{
		objectSource_t8 * L_26 = (__this->___newObjectSource_5);
		NullCheck(L_26);
		List_1_t6 * L_27 = (L_26->___m_spawnedMusicSource_1);
		NullCheck(L_27);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear() */, L_27);
		objectSource_t8 * L_28 = (__this->___newObjectSource_5);
		NullCheck(L_28);
		List_1_t6 * L_29 = (L_28->___m_spawnedSoundSource_3);
		NullCheck(L_29);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear() */, L_29);
		return;
	}
}
// System.Void AudioPooler::ClearMusic(UnityEngine.GameObject)
extern TypeInfo* U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t41_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t43_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m105_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m109_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m110_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m111_MethodInfo_var;
extern "C" void AudioPooler_ClearMusic_m41 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Func_2_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_MethodInfo_var = il2cpp_codegen_method_info_from_index(18);
		Func_2__ctor_m105_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		List_1_GetEnumerator_m109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		Enumerator_get_Current_m110_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Enumerator_MoveNext_m111_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t9 * V_0 = {0};
	Enumerator_t41  V_1 = {0};
	U3CClearMusicU3Ec__AnonStorey9_t16 * V_2 = {0};
	Exception_t42 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t42 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CClearMusicU3Ec__AnonStorey9_t16 * L_0 = (U3CClearMusicU3Ec__AnonStorey9_t16 *)il2cpp_codegen_object_new (U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo_var);
		U3CClearMusicU3Ec__AnonStorey9__ctor_m26(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CClearMusicU3Ec__AnonStorey9_t16 * L_1 = V_2;
		GameObject_t5 * L_2 = ___p_audioContainer;
		NullCheck(L_1);
		L_1->___p_audioContainer_0 = L_2;
		List_1_t21 * L_3 = (__this->___m_objectSourceList_9);
		U3CClearMusicU3Ec__AnonStorey9_t16 * L_4 = V_2;
		IntPtr_t L_5 = { (void*)U3CClearMusicU3Ec__AnonStorey9_U3CU3Em__3_m27_MethodInfo_var };
		Func_2_t37 * L_6 = (Func_2_t37 *)il2cpp_codegen_object_new (Func_2_t37_il2cpp_TypeInfo_var);
		Func_2__ctor_m105(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m105_MethodInfo_var);
		objectSource_t8 * L_7 = Enumerable_FirstOrDefault_TisobjectSource_t8_m96(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var);
		__this->___newObjectSource_5 = L_7;
		objectSource_t8 * L_8 = (__this->___newObjectSource_5);
		NullCheck(L_8);
		List_1_t6 * L_9 = (L_8->___m_spawnedMusicSource_1);
		NullCheck(L_9);
		Enumerator_t41  L_10 = List_1_GetEnumerator_m109(L_9, /*hidden argument*/List_1_GetEnumerator_m109_MethodInfo_var);
		V_1 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0040:
		{
			AudioSource_t9 * L_11 = Enumerator_get_Current_m110((&V_1), /*hidden argument*/Enumerator_get_Current_m110_MethodInfo_var);
			V_0 = L_11;
			AudioSource_t9 * L_12 = V_0;
			AudioPooler_ReturnAudioSource_m39(__this, L_12, 0, /*hidden argument*/NULL);
		}

IL_0050:
		{
			bool L_13 = Enumerator_MoveNext_m111((&V_1), /*hidden argument*/Enumerator_MoveNext_m111_MethodInfo_var);
			if (L_13)
			{
				goto IL_0040;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t42 *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_t41  L_14 = V_1;
		Enumerator_t41  L_15 = L_14;
		Object_t * L_16 = Box(Enumerator_t41_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t43_il2cpp_TypeInfo_var, L_16);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t42 *)
	}

IL_006d:
	{
		objectSource_t8 * L_17 = (__this->___newObjectSource_5);
		NullCheck(L_17);
		List_1_t6 * L_18 = (L_17->___m_spawnedMusicSource_1);
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear() */, L_18);
		return;
	}
}
// System.Void AudioPooler::ClearSoundEffects(UnityEngine.GameObject)
extern TypeInfo* U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t37_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t41_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t43_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m105_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m109_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m110_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m111_MethodInfo_var;
extern "C" void AudioPooler_ClearSoundEffects_m42 (AudioPooler_t10 * __this, GameObject_t5 * ___p_audioContainer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		Func_2_t37_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Enumerator_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		IDisposable_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_MethodInfo_var = il2cpp_codegen_method_info_from_index(19);
		Func_2__ctor_m105_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		List_1_GetEnumerator_m109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		Enumerator_get_Current_m110_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Enumerator_MoveNext_m111_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t9 * V_0 = {0};
	Enumerator_t41  V_1 = {0};
	U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * V_2 = {0};
	Exception_t42 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t42 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * L_0 = (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 *)il2cpp_codegen_object_new (U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo_var);
		U3CClearSoundEffectsU3Ec__AnonStoreyA__ctor_m28(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * L_1 = V_2;
		GameObject_t5 * L_2 = ___p_audioContainer;
		NullCheck(L_1);
		L_1->___p_audioContainer_0 = L_2;
		List_1_t21 * L_3 = (__this->___m_objectSourceList_9);
		U3CClearSoundEffectsU3Ec__AnonStoreyA_t17 * L_4 = V_2;
		IntPtr_t L_5 = { (void*)U3CClearSoundEffectsU3Ec__AnonStoreyA_U3CU3Em__4_m29_MethodInfo_var };
		Func_2_t37 * L_6 = (Func_2_t37 *)il2cpp_codegen_object_new (Func_2_t37_il2cpp_TypeInfo_var);
		Func_2__ctor_m105(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m105_MethodInfo_var);
		objectSource_t8 * L_7 = Enumerable_FirstOrDefault_TisobjectSource_t8_m96(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_FirstOrDefault_TisobjectSource_t8_m96_MethodInfo_var);
		__this->___newObjectSource_5 = L_7;
		objectSource_t8 * L_8 = (__this->___newObjectSource_5);
		NullCheck(L_8);
		List_1_t6 * L_9 = (L_8->___m_spawnedSoundSource_3);
		NullCheck(L_9);
		Enumerator_t41  L_10 = List_1_GetEnumerator_m109(L_9, /*hidden argument*/List_1_GetEnumerator_m109_MethodInfo_var);
		V_1 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0040:
		{
			AudioSource_t9 * L_11 = Enumerator_get_Current_m110((&V_1), /*hidden argument*/Enumerator_get_Current_m110_MethodInfo_var);
			V_0 = L_11;
			AudioSource_t9 * L_12 = V_0;
			AudioPooler_ReturnAudioSource_m39(__this, L_12, 1, /*hidden argument*/NULL);
		}

IL_0050:
		{
			bool L_13 = Enumerator_MoveNext_m111((&V_1), /*hidden argument*/Enumerator_MoveNext_m111_MethodInfo_var);
			if (L_13)
			{
				goto IL_0040;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t42 *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_t41  L_14 = V_1;
		Enumerator_t41  L_15 = L_14;
		Object_t * L_16 = Box(Enumerator_t41_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t43_il2cpp_TypeInfo_var, L_16);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t42 *)
	}

IL_006d:
	{
		objectSource_t8 * L_17 = (__this->___newObjectSource_5);
		NullCheck(L_17);
		List_1_t6 * L_18 = (L_17->___m_spawnedSoundSource_3);
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear() */, L_18);
		return;
	}
}
// System.Void AudioPooler::SearchMusic(System.String,UnityEngine.AudioSource,System.Boolean)
extern TypeInfo* U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo_var;
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t40_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1_get_Instance_m112_MethodInfo_var;
extern const MethodInfo* U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m113_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var;
extern "C" void AudioPooler_SearchMusic_m43 (AudioPooler_t10 * __this, String_t* ___key, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Func_2_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		MonoManager_1_get_Instance_m112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_MethodInfo_var = il2cpp_codegen_method_info_from_index(21);
		Func_2__ctor_m113_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	U3CSearchMusicU3Ec__AnonStoreyB_t18 * V_0 = {0};
	{
		U3CSearchMusicU3Ec__AnonStoreyB_t18 * L_0 = (U3CSearchMusicU3Ec__AnonStoreyB_t18 *)il2cpp_codegen_object_new (U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo_var);
		U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSearchMusicU3Ec__AnonStoreyB_t18 * L_1 = V_0;
		String_t* L_2 = ___key;
		NullCheck(L_1);
		L_1->___key_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_3 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_3);
		List_1_t1 * L_4 = (L_3->___m_listClips_3);
		U3CSearchMusicU3Ec__AnonStoreyB_t18 * L_5 = V_0;
		IntPtr_t L_6 = { (void*)U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31_MethodInfo_var };
		Func_2_t40 * L_7 = (Func_2_t40 *)il2cpp_codegen_object_new (Func_2_t40_il2cpp_TypeInfo_var);
		Func_2__ctor_m113(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m113_MethodInfo_var);
		AudioClip_t20 * L_8 = Enumerable_FirstOrDefault_TisAudioClip_t20_m98(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var);
		__this->___m_currentAudioClip_4 = L_8;
		AudioSource_t9 * L_9 = ___p_audioSource;
		bool L_10 = ___p_bisLooping;
		AudioPooler_LookForMusicSource_m45(__this, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioPooler::SearchSound(System.String,UnityEngine.AudioSource,System.Boolean)
extern TypeInfo* U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo_var;
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t40_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1_get_Instance_m112_MethodInfo_var;
extern const MethodInfo* U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m113_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var;
extern "C" void AudioPooler_SearchSound_m44 (AudioPooler_t10 * __this, String_t* ___key, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Func_2_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		MonoManager_1_get_Instance_m112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_MethodInfo_var = il2cpp_codegen_method_info_from_index(24);
		Func_2__ctor_m113_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		s_Il2CppMethodIntialized = true;
	}
	U3CSearchSoundU3Ec__AnonStoreyC_t19 * V_0 = {0};
	{
		U3CSearchSoundU3Ec__AnonStoreyC_t19 * L_0 = (U3CSearchSoundU3Ec__AnonStoreyC_t19 *)il2cpp_codegen_object_new (U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo_var);
		U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSearchSoundU3Ec__AnonStoreyC_t19 * L_1 = V_0;
		String_t* L_2 = ___key;
		NullCheck(L_1);
		L_1->___key_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_3 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_3);
		List_1_t1 * L_4 = (L_3->___m_listClips_3);
		U3CSearchSoundU3Ec__AnonStoreyC_t19 * L_5 = V_0;
		IntPtr_t L_6 = { (void*)U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33_MethodInfo_var };
		Func_2_t40 * L_7 = (Func_2_t40 *)il2cpp_codegen_object_new (Func_2_t40_il2cpp_TypeInfo_var);
		Func_2__ctor_m113(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m113_MethodInfo_var);
		AudioClip_t20 * L_8 = Enumerable_FirstOrDefault_TisAudioClip_t20_m98(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/Enumerable_FirstOrDefault_TisAudioClip_t20_m98_MethodInfo_var);
		__this->___m_currentAudioClip_4 = L_8;
		AudioSource_t9 * L_9 = ___p_audioSource;
		bool L_10 = ___p_bisLooping;
		AudioPooler_LookForSoundSource_m46(__this, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioPooler::LookForMusicSource(UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_LookForMusicSource_m45 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	{
		AudioSource_t9 * L_0 = ___p_audioSource;
		__this->___m_currentAudioSource_3 = L_0;
		AudioSource_t9 * L_1 = (__this->___m_currentAudioSource_3);
		AudioClip_t20 * L_2 = (__this->___m_currentAudioClip_4);
		NullCheck(L_1);
		AudioSource_set_clip_m114(L_1, L_2, /*hidden argument*/NULL);
		AudioSource_t9 * L_3 = ___p_audioSource;
		bool L_4 = ___p_bisLooping;
		Object_t * L_5 = AudioPooler_DisableMusic_m47(__this, L_3, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m115(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioPooler::LookForSoundSource(UnityEngine.AudioSource,System.Boolean)
extern "C" void AudioPooler_LookForSoundSource_m46 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	{
		AudioSource_t9 * L_0 = ___p_audioSource;
		__this->___m_currentAudioSource_3 = L_0;
		AudioSource_t9 * L_1 = (__this->___m_currentAudioSource_3);
		AudioClip_t20 * L_2 = (__this->___m_currentAudioClip_4);
		NullCheck(L_1);
		AudioSource_set_clip_m114(L_1, L_2, /*hidden argument*/NULL);
		AudioSource_t9 * L_3 = ___p_audioSource;
		bool L_4 = ___p_bisLooping;
		Object_t * L_5 = AudioPooler_DisableSound_m48(__this, L_3, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m115(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AudioPooler::DisableMusic(UnityEngine.AudioSource,System.Boolean)
extern TypeInfo* U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioPooler_DisableMusic_m47 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	U3CDisableMusicU3Ec__Iterator0_t11 * V_0 = {0};
	{
		U3CDisableMusicU3Ec__Iterator0_t11 * L_0 = (U3CDisableMusicU3Ec__Iterator0_t11 *)il2cpp_codegen_object_new (U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo_var);
		U3CDisableMusicU3Ec__Iterator0__ctor_m8(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_1 = V_0;
		AudioSource_t9 * L_2 = ___p_audioSource;
		NullCheck(L_1);
		L_1->___p_audioSource_0 = L_2;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_3 = V_0;
		bool L_4 = ___p_bisLooping;
		NullCheck(L_3);
		L_3->___p_bisLooping_1 = L_4;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_5 = V_0;
		AudioSource_t9 * L_6 = ___p_audioSource;
		NullCheck(L_5);
		L_5->___U3CU24U3Ep_audioSource_4 = L_6;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_7 = V_0;
		bool L_8 = ___p_bisLooping;
		NullCheck(L_7);
		L_7->___U3CU24U3Ep_bisLooping_5 = L_8;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CDisableMusicU3Ec__Iterator0_t11 * L_10 = V_0;
		return L_10;
	}
}
// System.Collections.IEnumerator AudioPooler::DisableSound(UnityEngine.AudioSource,System.Boolean)
extern TypeInfo* U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioPooler_DisableSound_m48 (AudioPooler_t10 * __this, AudioSource_t9 * ___p_audioSource, bool ___p_bisLooping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	U3CDisableSoundU3Ec__Iterator1_t12 * V_0 = {0};
	{
		U3CDisableSoundU3Ec__Iterator1_t12 * L_0 = (U3CDisableSoundU3Ec__Iterator1_t12 *)il2cpp_codegen_object_new (U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo_var);
		U3CDisableSoundU3Ec__Iterator1__ctor_m14(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_1 = V_0;
		AudioSource_t9 * L_2 = ___p_audioSource;
		NullCheck(L_1);
		L_1->___p_audioSource_0 = L_2;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_3 = V_0;
		bool L_4 = ___p_bisLooping;
		NullCheck(L_3);
		L_3->___p_bisLooping_1 = L_4;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_5 = V_0;
		AudioSource_t9 * L_6 = ___p_audioSource;
		NullCheck(L_5);
		L_5->___U3CU24U3Ep_audioSource_4 = L_6;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_7 = V_0;
		bool L_8 = ___p_bisLooping;
		NullCheck(L_7);
		L_7->___U3CU24U3Ep_bisLooping_5 = L_8;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CDisableSoundU3Ec__Iterator1_t12 * L_10 = V_0;
		return L_10;
	}
}
// BoardController
#include "AssemblyU2DCSharp_BoardController.h"
#ifndef _MSC_VER
#else
#endif
// BoardController
#include "AssemblyU2DCSharp_BoardControllerMethodDeclarations.h"



// System.Void BoardController::.ctor()
extern "C" void BoardController__ctor_m49 (BoardController_t23 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m100(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardController::Awake()
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1_Create_m116_MethodInfo_var;
extern "C" void BoardController_Awake_m50 (BoardController_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		MonoManager_1_Create_m116_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		MonoManager_1_Create_m116(NULL /*static, unused*/, (Transform_t30 *)NULL, /*hidden argument*/MonoManager_1_Create_m116_MethodInfo_var);
		return;
	}
}
// System.Void BoardController::Stop()
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1_get_Instance_m112_MethodInfo_var;
extern "C" void BoardController_Stop_m51 (BoardController_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		MonoManager_1_get_Instance_m112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_0 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_0);
		AudioManager_StopAudio_m4(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardController::PlaySound(System.Int32)
extern TypeInfo* MonoManager_1_t3_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoManager_1_get_Instance_m112_MethodInfo_var;
extern "C" void BoardController_PlaySound_m52 (BoardController_t23 * __this, int32_t ___sound, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoManager_1_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		MonoManager_1_get_Instance_m112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___sound;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0071;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0086;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_009b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_00b0;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_00c5;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_00da;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_00ef;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_0104;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_0119;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 11)
		{
			goto IL_012e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 12)
		{
			goto IL_0143;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 13)
		{
			goto IL_0158;
		}
	}
	{
		goto IL_016d;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_2 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_2);
		AudioManager_PlaySound_m3(L_2, (String_t*) &_stringLiteral6, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_3 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_3);
		AudioManager_PlaySound_m3(L_3, (String_t*) &_stringLiteral7, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_4 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_4);
		AudioManager_PlayMusic_m2(L_4, (String_t*) &_stringLiteral8, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0086:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_5 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_5);
		AudioManager_PlayMusic_m2(L_5, (String_t*) &_stringLiteral9, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_6 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_6);
		AudioManager_PlayMusic_m2(L_6, (String_t*) &_stringLiteral10, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_7 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_7);
		AudioManager_PlayMusic_m2(L_7, (String_t*) &_stringLiteral11, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_8 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_8);
		AudioManager_PlaySound_m3(L_8, (String_t*) &_stringLiteral12, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_00da:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_9 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_9);
		AudioManager_PlaySound_m3(L_9, (String_t*) &_stringLiteral13, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_10 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_10);
		AudioManager_PlaySound_m3(L_10, (String_t*) &_stringLiteral14, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0104:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_11 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_11);
		AudioManager_PlayMusic_m2(L_11, (String_t*) &_stringLiteral15, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0119:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_12 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_12);
		AudioManager_PlaySound_m3(L_12, (String_t*) &_stringLiteral16, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_012e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_13 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_13);
		AudioManager_PlayMusic_m2(L_13, (String_t*) &_stringLiteral17, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0143:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_14 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_14);
		AudioManager_PlayMusic_m2(L_14, (String_t*) &_stringLiteral18, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_0158:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoManager_1_t3_il2cpp_TypeInfo_var);
		AudioManager_t2 * L_15 = MonoManager_1_get_Instance_m112(NULL /*static, unused*/, /*hidden argument*/MonoManager_1_get_Instance_m112_MethodInfo_var);
		NullCheck(L_15);
		AudioManager_PlayMusic_m2(L_15, (String_t*) &_stringLiteral19, 0, /*hidden argument*/NULL);
		goto IL_016d;
	}

IL_016d:
	{
		return;
	}
}
// DebugUtil
#include "AssemblyU2DCSharp_DebugUtil.h"
#ifndef _MSC_VER
#else
#endif
// DebugUtil
#include "AssemblyU2DCSharp_DebugUtilMethodDeclarations.h"

#include "mscorlib_ArrayTypes.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"


// System.Void DebugUtil::.ctor()
extern "C" void DebugUtil__ctor_m53 (DebugUtil_t24 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugUtil::.cctor()
extern "C" void DebugUtil__cctor_m54 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DebugUtil::log(System.Object,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_log_m55 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___format;
		if (!((String_t*)IsInst(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0021;
		}
	}
	{
		Object_t * L_1 = ___format;
		ObjectU5BU5D_t29* L_2 = ___paramList;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m117(NULL /*static, unused*/, ((String_t*)IsInst(L_1, String_t_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		Debug_Log_m79(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_0021:
	{
		Object_t * L_4 = ___format;
		Debug_Log_m79(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void DebugUtil::warn(System.Object,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_warn_m56 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___format;
		if (!((String_t*)IsInst(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0021;
		}
	}
	{
		Object_t * L_1 = ___format;
		ObjectU5BU5D_t29* L_2 = ___paramList;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m117(NULL /*static, unused*/, ((String_t*)IsInst(L_1, String_t_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m118(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_0021:
	{
		Object_t * L_4 = ___format;
		Debug_LogWarning_m118(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void DebugUtil::error(System.Object,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_error_m57 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___format;
		if (!((String_t*)IsInst(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0021;
		}
	}
	{
		Object_t * L_1 = ___format;
		ObjectU5BU5D_t29* L_2 = ___paramList;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m117(NULL /*static, unused*/, ((String_t*)IsInst(L_1, String_t_il2cpp_TypeInfo_var)), L_2, /*hidden argument*/NULL);
		Debug_LogError_m119(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0027;
	}

IL_0021:
	{
		Object_t * L_4 = ___format;
		Debug_LogError_m119(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void DebugUtil::assert(System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DebugUtil_t24_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_assert_m58 (Object_t * __this /* static, unused */, bool ___condition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		DebugUtil_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		DebugUtil_assert_m60(NULL /*static, unused*/, L_0, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugUtil::assert(System.Boolean,System.String)
extern TypeInfo* DebugUtil_t24_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_assert_m59 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___assertString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebugUtil_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition;
		String_t* L_1 = ___assertString;
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		DebugUtil_assert_m60(NULL /*static, unused*/, L_0, L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugUtil::assert(System.Boolean,System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_assert_m60 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___assertString, bool ___pauseOnFail, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___condition;
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_1 = ___assertString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m108(NULL /*static, unused*/, (String_t*) &_stringLiteral20, L_1, /*hidden argument*/NULL);
		Debug_LogError_m119(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_3 = ___pauseOnFail;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		Debug_Break_m120(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void DebugUtil::Log(System.String)
extern TypeInfo* DebugUtil_t24_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_Log_m61 (Object_t * __this /* static, unused */, String_t* ___p_s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebugUtil_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___p_s;
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		DebugUtil_LogMessage_m62(NULL /*static, unused*/, L_0, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugUtil::LogMessage(System.String,UnityEngine.LogType)
extern TypeInfo* DebugUtil_t24_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTrace_t44_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t45_il2cpp_TypeInfo_var;
extern "C" void DebugUtil_LogMessage_m62 (Object_t * __this /* static, unused */, String_t* ___p_s, int32_t ___p_logType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebugUtil_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		StackTrace_t44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		StringU5BU5D_t45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t44 * V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		bool L_0 = ((DebugUtil_t24_StaticFields*)DebugUtil_t24_il2cpp_TypeInfo_var->static_fields)->___ENABLE_DEBUG_1;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		int32_t L_1 = ___p_logType;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0056;
		}
		if (L_2 == 1)
		{
			goto IL_00b9;
		}
		if (L_2 == 2)
		{
			goto IL_0041;
		}
		if (L_2 == 3)
		{
			goto IL_002c;
		}
		if (L_2 == 4)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_00b9;
	}

IL_002c:
	{
		String_t* L_3 = ___p_s;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m108(NULL /*static, unused*/, (String_t*) &_stringLiteral21, L_3, /*hidden argument*/NULL);
		Debug_Log_m79(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_0041:
	{
		String_t* L_5 = ___p_s;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m108(NULL /*static, unused*/, (String_t*) &_stringLiteral21, L_5, /*hidden argument*/NULL);
		Debug_LogWarning_m118(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_0056:
	{
		StackTrace_t44 * L_7 = (StackTrace_t44 *)il2cpp_codegen_object_new (StackTrace_t44_il2cpp_TypeInfo_var);
		StackTrace__ctor_m121(L_7, 1, /*hidden argument*/NULL);
		V_0 = L_7;
		StringU5BU5D_t45* L_8 = ((StringU5BU5D_t45*)SZArrayNew(StringU5BU5D_t45_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, (String_t*) &_stringLiteral22);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, 0)) = (String_t*)(String_t*) &_stringLiteral22;
		StringU5BU5D_t45* L_9 = L_8;
		String_t* L_10 = ___p_s;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_10);
		*((String_t**)(String_t**)SZArrayLdElema(L_9, 1)) = (String_t*)L_10;
		StringU5BU5D_t45* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, (String_t*) &_stringLiteral23);
		*((String_t**)(String_t**)SZArrayLdElema(L_11, 2)) = (String_t*)(String_t*) &_stringLiteral23;
		StringU5BU5D_t45* L_12 = L_11;
		StackTrace_t44 * L_13 = V_0;
		NullCheck(L_13);
		StackFrame_t46 * L_14 = (StackFrame_t46 *)VirtFuncInvoker1< StackFrame_t46 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_13, 2);
		NullCheck(L_14);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 3)) = (String_t*)L_15;
		StringU5BU5D_t45* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, (String_t*) &_stringLiteral24);
		*((String_t**)(String_t**)SZArrayLdElema(L_16, 4)) = (String_t*)(String_t*) &_stringLiteral24;
		StringU5BU5D_t45* L_17 = L_16;
		StackTrace_t44 * L_18 = V_0;
		NullCheck(L_18);
		StackFrame_t46 * L_19 = (StackFrame_t46 *)VirtFuncInvoker1< StackFrame_t46 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_18, 2);
		NullCheck(L_19);
		MethodBase_t47 * L_20 = (MethodBase_t47 *)VirtFuncInvoker0< MethodBase_t47 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_19);
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_20);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 5);
		ArrayElementTypeCheck (L_17, L_21);
		*((String_t**)(String_t**)SZArrayLdElema(L_17, 5)) = (String_t*)L_21;
		StringU5BU5D_t45* L_22 = L_17;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 6);
		ArrayElementTypeCheck (L_22, (String_t*) &_stringLiteral25);
		*((String_t**)(String_t**)SZArrayLdElema(L_22, 6)) = (String_t*)(String_t*) &_stringLiteral25;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m122(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Debug_LogError_m119(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_00b9:
	{
		goto IL_00be;
	}

IL_00be:
	{
		return;
	}
}
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetup.h"
#ifndef _MSC_VER
#else
#endif
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetupMethodDeclarations.h"

// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycasterMethodDeclarations.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
struct Component_t49;
struct Canvas_t48;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t49;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m124_gshared (Component_t49 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m124(__this, method) (( Object_t * (*) (Component_t49 *, const MethodInfo*))Component_GetComponent_TisObject_t_m124_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t48_m123(__this, method) (( Canvas_t48 * (*) (Component_t49 *, const MethodInfo*))Component_GetComponent_TisObject_t_m124_gshared)(__this, method)
struct Component_t49;
struct CanvasScaler_t50;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.CanvasScaler>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.CanvasScaler>()
#define Component_GetComponent_TisCanvasScaler_t50_m125(__this, method) (( CanvasScaler_t50 * (*) (Component_t49 *, const MethodInfo*))Component_GetComponent_TisObject_t_m124_gshared)(__this, method)
struct Component_t49;
struct GraphicRaycaster_t51;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.GraphicRaycaster>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.GraphicRaycaster>()
#define Component_GetComponent_TisGraphicRaycaster_t51_m126(__this, method) (( GraphicRaycaster_t51 * (*) (Component_t49 *, const MethodInfo*))Component_GetComponent_TisObject_t_m124_gshared)(__this, method)
struct Component_t49;
struct CanvasGroup_t52;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
#define Component_GetComponent_TisCanvasGroup_t52_m127(__this, method) (( CanvasGroup_t52 * (*) (Component_t49 *, const MethodInfo*))Component_GetComponent_TisObject_t_m124_gshared)(__this, method)


// System.Void DefiniteCasinoCanvasSetup::.ctor()
extern "C" void DefiniteCasinoCanvasSetup__ctor_m63 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m100(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefiniteCasinoCanvasSetup::Awake()
extern "C" void DefiniteCasinoCanvasSetup_Awake_m64 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DefiniteCasinoCanvasSetup::ResetToDefiniteCasinoSetup()
extern const MethodInfo* Component_GetComponent_TisCanvas_t48_m123_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvasScaler_t50_m125_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisGraphicRaycaster_t51_m126_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCanvasGroup_t52_m127_MethodInfo_var;
extern "C" void DefiniteCasinoCanvasSetup_ResetToDefiniteCasinoSetup_m65 (DefiniteCasinoCanvasSetup_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCanvas_t48_m123_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		Component_GetComponent_TisCanvasScaler_t50_m125_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		Component_GetComponent_TisGraphicRaycaster_t51_m126_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		Component_GetComponent_TisCanvasGroup_t52_m127_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		s_Il2CppMethodIntialized = true;
	}
	Canvas_t48 * V_0 = {0};
	CanvasScaler_t50 * V_1 = {0};
	GraphicRaycaster_t51 * V_2 = {0};
	CanvasGroup_t52 * V_3 = {0};
	{
		Canvas_t48 * L_0 = Component_GetComponent_TisCanvas_t48_m123(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t48_m123_MethodInfo_var);
		V_0 = L_0;
		Canvas_t48 * L_1 = V_0;
		NullCheck(L_1);
		Canvas_set_renderMode_m128(L_1, 0, /*hidden argument*/NULL);
		Canvas_t48 * L_2 = V_0;
		NullCheck(L_2);
		Canvas_set_pixelPerfect_m129(L_2, 0, /*hidden argument*/NULL);
		CanvasScaler_t50 * L_3 = Component_GetComponent_TisCanvasScaler_t50_m125(__this, /*hidden argument*/Component_GetComponent_TisCanvasScaler_t50_m125_MethodInfo_var);
		V_1 = L_3;
		CanvasScaler_t50 * L_4 = V_1;
		NullCheck(L_4);
		CanvasScaler_set_uiScaleMode_m130(L_4, 1, /*hidden argument*/NULL);
		CanvasScaler_t50 * L_5 = V_1;
		Vector2_t53  L_6 = {0};
		Vector2__ctor_m131(&L_6, (2048.0f), (1536.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		CanvasScaler_set_referenceResolution_m132(L_5, L_6, /*hidden argument*/NULL);
		CanvasScaler_t50 * L_7 = V_1;
		NullCheck(L_7);
		CanvasScaler_set_screenMatchMode_m133(L_7, 0, /*hidden argument*/NULL);
		CanvasScaler_t50 * L_8 = V_1;
		NullCheck(L_8);
		CanvasScaler_set_matchWidthOrHeight_m134(L_8, (0.75f), /*hidden argument*/NULL);
		CanvasScaler_t50 * L_9 = V_1;
		NullCheck(L_9);
		CanvasScaler_set_referencePixelsPerUnit_m135(L_9, (100.0f), /*hidden argument*/NULL);
		GraphicRaycaster_t51 * L_10 = Component_GetComponent_TisGraphicRaycaster_t51_m126(__this, /*hidden argument*/Component_GetComponent_TisGraphicRaycaster_t51_m126_MethodInfo_var);
		V_2 = L_10;
		GraphicRaycaster_t51 * L_11 = V_2;
		NullCheck(L_11);
		GraphicRaycaster_set_ignoreReversedGraphics_m136(L_11, 1, /*hidden argument*/NULL);
		GraphicRaycaster_t51 * L_12 = V_2;
		NullCheck(L_12);
		GraphicRaycaster_set_blockingObjects_m137(L_12, 0, /*hidden argument*/NULL);
		CanvasGroup_t52 * L_13 = Component_GetComponent_TisCanvasGroup_t52_m127(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t52_m127_MethodInfo_var);
		V_3 = L_13;
		CanvasGroup_t52 * L_14 = V_3;
		NullCheck(L_14);
		CanvasGroup_set_alpha_m138(L_14, (1.0f), /*hidden argument*/NULL);
		CanvasGroup_t52 * L_15 = V_3;
		NullCheck(L_15);
		CanvasGroup_set_interactable_m139(L_15, 1, /*hidden argument*/NULL);
		CanvasGroup_t52 * L_16 = V_3;
		NullCheck(L_16);
		CanvasGroup_set_blocksRaycasts_m140(L_16, 1, /*hidden argument*/NULL);
		CanvasGroup_t52 * L_17 = V_3;
		NullCheck(L_17);
		CanvasGroup_set_ignoreParentGroups_m141(L_17, 0, /*hidden argument*/NULL);
		return;
	}
}
// Tools/<WaitForRealTime>c__Iterator5
#include "AssemblyU2DCSharp_Tools_U3CWaitForRealTimeU3Ec__Iterator5.h"
#ifndef _MSC_VER
#else
#endif
// Tools/<WaitForRealTime>c__Iterator5
#include "AssemblyU2DCSharp_Tools_U3CWaitForRealTimeU3Ec__Iterator5MethodDeclarations.h"

// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"


// System.Void Tools/<WaitForRealTime>c__Iterator5::.ctor()
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m80(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean Tools/<WaitForRealTime>c__Iterator5::MoveNext()
extern TypeInfo* Int32_t54_il2cpp_TypeInfo_var;
extern "C" bool U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0050;
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		float L_2 = Time_get_realtimeSinceStartup_m142(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = (__this->___p_delay_0);
		__this->___U3CpauseEndTimeU3E__0_1 = ((float)((float)L_2+(float)L_3));
		goto IL_0050;
	}

IL_0038:
	{
		int32_t L_4 = 0;
		Object_t * L_5 = Box(Int32_t54_il2cpp_TypeInfo_var, &L_4);
		__this->___U24current_3 = L_5;
		__this->___U24PC_2 = 1;
		goto IL_0073;
	}

IL_0050:
	{
		float L_6 = Time_get_realtimeSinceStartup_m142(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = (__this->___U3CpauseEndTimeU3E__0_1);
		if ((((float)L_6) < ((float)L_7)))
		{
			goto IL_0038;
		}
	}
	{
		goto IL_006a;
	}
	// Dead block : IL_0065: br IL_0021

IL_006a:
	{
		__this->___U24PC_2 = (-1);
	}

IL_0071:
	{
		return 0;
	}

IL_0073:
	{
		return 1;
	}
	// Dead block : IL_0075: ldloc.1
}
// System.Void Tools/<WaitForRealTime>c__Iterator5::Dispose()
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void Tools/<WaitForRealTime>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t32_il2cpp_TypeInfo_var;
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t32_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t32 * L_0 = (NotSupportedException_t32 *)il2cpp_codegen_object_new (NotSupportedException_t32_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m85(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Tools
#include "AssemblyU2DCSharp_Tools.h"
#ifndef _MSC_VER
#else
#endif
// Tools
#include "AssemblyU2DCSharp_ToolsMethodDeclarations.h"

// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"


// System.Collections.IEnumerator Tools::WaitForRealTime(System.Single)
extern TypeInfo* U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo_var;
extern "C" Object_t * Tools_WaitForRealTime_m72 (Object_t * __this /* static, unused */, float ___p_delay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForRealTimeU3Ec__Iterator5_t26 * V_0 = {0};
	{
		U3CWaitForRealTimeU3Ec__Iterator5_t26 * L_0 = (U3CWaitForRealTimeU3Ec__Iterator5_t26 *)il2cpp_codegen_object_new (U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo_var);
		U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForRealTimeU3Ec__Iterator5_t26 * L_1 = V_0;
		float L_2 = ___p_delay;
		NullCheck(L_1);
		L_1->___p_delay_0 = L_2;
		U3CWaitForRealTimeU3Ec__Iterator5_t26 * L_3 = V_0;
		float L_4 = ___p_delay;
		NullCheck(L_3);
		L_3->___U3CU24U3Ep_delay_4 = L_4;
		U3CWaitForRealTimeU3Ec__Iterator5_t26 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.GameObject Tools::InstantiateResource(System.String,UnityEngine.Transform)
extern TypeInfo* DebugUtil_t24_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern "C" GameObject_t5 * Tools_InstantiateResource_m73 (Object_t * __this /* static, unused */, String_t* ___p_path, Transform_t30 * ___p_parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebugUtil_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		s_Il2CppMethodIntialized = true;
	}
	Object_t33 * V_0 = {0};
	GameObject_t5 * V_1 = {0};
	{
		String_t* L_0 = ___p_path;
		Object_t33 * L_1 = Resources_Load_m143(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t33 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m86(NULL /*static, unused*/, L_2, (Object_t33 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		Object_t33 * L_4 = V_0;
		bool L_5 = Object_op_Inequality_m144(NULL /*static, unused*/, L_4, (Object_t33 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		DebugUtil_assert_m58(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (GameObject_t5 *)NULL;
	}

IL_0021:
	{
		Object_t33 * L_6 = V_0;
		Object_t33 * L_7 = Object_Instantiate_m145(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = ((GameObject_t5 *)IsInst(L_7, GameObject_t5_il2cpp_TypeInfo_var));
		GameObject_t5 * L_8 = V_1;
		bool L_9 = Object_op_Equality_m86(NULL /*static, unused*/, L_8, (Object_t33 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		GameObject_t5 * L_10 = V_1;
		bool L_11 = Object_op_Inequality_m144(NULL /*static, unused*/, L_10, (Object_t33 *)NULL, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DebugUtil_t24_il2cpp_TypeInfo_var);
		DebugUtil_assert_m58(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return (GameObject_t5 *)NULL;
	}

IL_0047:
	{
		GameObject_t5 * L_12 = V_1;
		GameObject_t5 * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m88(L_13, /*hidden argument*/NULL);
		String_t* L_15 = Tools_RemoveSubstring_m74(NULL /*static, unused*/, L_14, (String_t*) &_stringLiteral29, /*hidden argument*/NULL);
		NullCheck(L_12);
		Object_set_name_m146(L_12, L_15, /*hidden argument*/NULL);
		Transform_t30 * L_16 = ___p_parent;
		bool L_17 = Object_op_Inequality_m144(NULL /*static, unused*/, L_16, (Object_t33 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0075;
		}
	}
	{
		GameObject_t5 * L_18 = V_1;
		NullCheck(L_18);
		Transform_t30 * L_19 = GameObject_get_transform_m147(L_18, /*hidden argument*/NULL);
		Transform_t30 * L_20 = ___p_parent;
		NullCheck(L_19);
		Transform_SetParent_m148(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0075:
	{
		GameObject_t5 * L_21 = V_1;
		return L_21;
	}
}
// System.String Tools::RemoveSubstring(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Tools_RemoveSubstring_m74 (Object_t * __this /* static, unused */, String_t* ___p_mainStr, String_t* ___p_subStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___p_mainStr;
		String_t* L_1 = ___p_subStr;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_3 = String_Replace_m149(L_0, L_1, L_2, /*hidden argument*/NULL);
		___p_mainStr = L_3;
		String_t* L_4 = ___p_mainStr;
		NullCheck(L_4);
		String_t* L_5 = String_Trim_m150(L_4, /*hidden argument*/NULL);
		___p_mainStr = L_5;
		String_t* L_6 = ___p_mainStr;
		return L_6;
	}
}
// System.Boolean Tools::IsMultipleTouch()
extern TypeInfo* Input_t55_il2cpp_TypeInfo_var;
extern "C" bool Tools_IsMultipleTouch_m75 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t55_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m151(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) > ((int32_t)1))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
