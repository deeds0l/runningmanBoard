﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1247;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_t1024  : public Object_t
{
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t1247* ___LegalKeySizesValue_1;
};
