﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<DisableSound>c__Iterator1
struct U3CDisableSoundU3Ec__Iterator1_t12;
// System.Object
struct Object_t;

// System.Void AudioPooler/<DisableSound>c__Iterator1::.ctor()
extern "C" void U3CDisableSoundU3Ec__Iterator1__ctor_m14 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioPooler/<DisableSound>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<DisableSound>c__Iterator1::MoveNext()
extern "C" bool U3CDisableSoundU3Ec__Iterator1_MoveNext_m17 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler/<DisableSound>c__Iterator1::Dispose()
extern "C" void U3CDisableSoundU3Ec__Iterator1_Dispose_m18 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler/<DisableSound>c__Iterator1::Reset()
extern "C" void U3CDisableSoundU3Ec__Iterator1_Reset_m19 (U3CDisableSoundU3Ec__Iterator1_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
