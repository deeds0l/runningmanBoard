﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2979;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t78;
// System.Exception
struct Exception_t42;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m18439_gshared (ArrayReadOnlyList_1_t2979 * __this, ObjectU5BU5D_t29* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m18439(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, ObjectU5BU5D_t29*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m18439_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18440_gshared (ArrayReadOnlyList_1_t2979 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18440(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2979 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18440_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m18441_gshared (ArrayReadOnlyList_1_t2979 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m18441(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2979 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m18441_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m18442_gshared (ArrayReadOnlyList_1_t2979 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m18442(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m18442_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m18443_gshared (ArrayReadOnlyList_1_t2979 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m18443(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2979 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m18443_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m18444_gshared (ArrayReadOnlyList_1_t2979 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m18444(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2979 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m18444_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m18445_gshared (ArrayReadOnlyList_1_t2979 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m18445(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m18445_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m18446_gshared (ArrayReadOnlyList_1_t2979 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m18446(__this, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m18446_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m18447_gshared (ArrayReadOnlyList_1_t2979 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m18447(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2979 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m18447_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m18448_gshared (ArrayReadOnlyList_1_t2979 * __this, ObjectU5BU5D_t29* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m18448(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, ObjectU5BU5D_t29*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m18448_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m18449_gshared (ArrayReadOnlyList_1_t2979 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m18449(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2979 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m18449_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m18450_gshared (ArrayReadOnlyList_1_t2979 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m18450(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2979 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m18450_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m18451_gshared (ArrayReadOnlyList_1_t2979 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m18451(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m18451_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m18452_gshared (ArrayReadOnlyList_1_t2979 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m18452(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2979 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m18452_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m18453_gshared (ArrayReadOnlyList_1_t2979 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m18453(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2979 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m18453_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t42 * ArrayReadOnlyList_1_ReadOnlyError_m18454_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m18454(__this /* static, unused */, method) (( Exception_t42 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m18454_gshared)(__this /* static, unused */, method)
