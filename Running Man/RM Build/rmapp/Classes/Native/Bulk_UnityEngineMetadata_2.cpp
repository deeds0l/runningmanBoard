﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t639_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern const MethodInfo WrapperlessIcall__ctor_m3193_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WrapperlessIcall__ctor_m3193/* method */
	, &WrapperlessIcall_t639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WrapperlessIcall_t639_MethodInfos[] =
{
	&WrapperlessIcall__ctor_m3193_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m3659_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m3530_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference WrapperlessIcall_t639_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool WrapperlessIcall_t639_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t832_0_0_0;
static Il2CppInterfaceOffsetPair WrapperlessIcall_t639_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t639_0_0_0;
extern const Il2CppType WrapperlessIcall_t639_1_0_0;
extern const Il2CppType Attribute_t539_0_0_0;
struct WrapperlessIcall_t639;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t639_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, WrapperlessIcall_t639_VTable/* vtableMethods */
	, WrapperlessIcall_t639_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WrapperlessIcall_t639_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, WrapperlessIcall_t639_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WrapperlessIcall_t639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t639_0_0_0/* byval_arg */
	, &WrapperlessIcall_t639_1_0_0/* this_arg */
	, &WrapperlessIcall_t639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t639)/* instance_size */
	, sizeof (WrapperlessIcall_t639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern const MethodInfo IL2CPPStructAlignmentAttribute__ctor_m3194_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IL2CPPStructAlignmentAttribute__ctor_m3194/* method */
	, &IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IL2CPPStructAlignmentAttribute_t640_MethodInfos[] =
{
	&IL2CPPStructAlignmentAttribute__ctor_m3194_MethodInfo,
	NULL
};
static const Il2CppMethodReference IL2CPPStructAlignmentAttribute_t640_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool IL2CPPStructAlignmentAttribute_t640_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t640_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t640_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t640_1_0_0;
struct IL2CPPStructAlignmentAttribute_t640;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t640_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t640_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t640_VTable/* vtableMethods */
	, IL2CPPStructAlignmentAttribute_t640_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1006/* fieldStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, IL2CPPStructAlignmentAttribute_t640_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 597/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t640_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t640_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t640_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t640)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t640)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t644_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern const MethodInfo AttributeHelperEngine__cctor_m3195_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&AttributeHelperEngine__cctor_m3195/* method */
	, &AttributeHelperEngine_t644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t644_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3196_ParameterInfos[] = 
{
	{"type", 0, 134219150, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const MethodInfo AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3196_MethodInfo = 
{
	"GetParentTypeDisallowingMultipleInclusion"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3196/* method */
	, &AttributeHelperEngine_t644_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t644_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3196_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t644_AttributeHelperEngine_GetRequiredComponents_m3197_ParameterInfos[] = 
{
	{"klass", 0, 134219151, 0, &Type_t_0_0_0},
};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const MethodInfo AttributeHelperEngine_GetRequiredComponents_m3197_MethodInfo = 
{
	"GetRequiredComponents"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetRequiredComponents_m3197/* method */
	, &AttributeHelperEngine_t644_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t644_AttributeHelperEngine_GetRequiredComponents_m3197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t644_AttributeHelperEngine_CheckIsEditorScript_m3198_ParameterInfos[] = 
{
	{"klass", 0, 134219152, 0, &Type_t_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const MethodInfo AttributeHelperEngine_CheckIsEditorScript_m3198_MethodInfo = 
{
	"CheckIsEditorScript"/* name */
	, (methodPointerType)&AttributeHelperEngine_CheckIsEditorScript_m3198/* method */
	, &AttributeHelperEngine_t644_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, AttributeHelperEngine_t644_AttributeHelperEngine_CheckIsEditorScript_m3198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeHelperEngine_t644_MethodInfos[] =
{
	&AttributeHelperEngine__cctor_m3195_MethodInfo,
	&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3196_MethodInfo,
	&AttributeHelperEngine_GetRequiredComponents_m3197_MethodInfo,
	&AttributeHelperEngine_CheckIsEditorScript_m3198_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
static const Il2CppMethodReference AttributeHelperEngine_t644_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AttributeHelperEngine_t644_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t644_0_0_0;
extern const Il2CppType AttributeHelperEngine_t644_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct AttributeHelperEngine_t644;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t644_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t644_VTable/* vtableMethods */
	, AttributeHelperEngine_t644_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1007/* fieldStart */

};
TypeInfo AttributeHelperEngine_t644_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, AttributeHelperEngine_t644_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AttributeHelperEngine_t644_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t644_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t644_1_0_0/* this_arg */
	, &AttributeHelperEngine_t644_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t644)/* instance_size */
	, sizeof (AttributeHelperEngine_t644)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t644_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t417_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern const MethodInfo DisallowMultipleComponent__ctor_m2044_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DisallowMultipleComponent__ctor_m2044/* method */
	, &DisallowMultipleComponent_t417_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DisallowMultipleComponent_t417_MethodInfos[] =
{
	&DisallowMultipleComponent__ctor_m2044_MethodInfo,
	NULL
};
static const Il2CppMethodReference DisallowMultipleComponent_t417_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool DisallowMultipleComponent_t417_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t417_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t417_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t417_1_0_0;
struct DisallowMultipleComponent_t417;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t417_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, DisallowMultipleComponent_t417_VTable/* vtableMethods */
	, DisallowMultipleComponent_t417_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DisallowMultipleComponent_t417_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, DisallowMultipleComponent_t417_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DisallowMultipleComponent_t417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 598/* custom_attributes_cache */
	, &DisallowMultipleComponent_t417_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t417_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t417)/* instance_size */
	, sizeof (DisallowMultipleComponent_t417)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t61_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RequireComponent_t61_RequireComponent__ctor_m157_ParameterInfos[] = 
{
	{"requiredComponent", 0, 134219153, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern const MethodInfo RequireComponent__ctor_m157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RequireComponent__ctor_m157/* method */
	, &RequireComponent_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RequireComponent_t61_RequireComponent__ctor_m157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RequireComponent_t61_MethodInfos[] =
{
	&RequireComponent__ctor_m157_MethodInfo,
	NULL
};
static const Il2CppMethodReference RequireComponent_t61_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RequireComponent_t61_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RequireComponent_t61_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t61_0_0_0;
extern const Il2CppType RequireComponent_t61_1_0_0;
struct RequireComponent_t61;
const Il2CppTypeDefinitionMetadata RequireComponent_t61_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t61_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, RequireComponent_t61_VTable/* vtableMethods */
	, RequireComponent_t61_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1010/* fieldStart */

};
TypeInfo RequireComponent_t61_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, RequireComponent_t61_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RequireComponent_t61_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 599/* custom_attributes_cache */
	, &RequireComponent_t61_0_0_0/* byval_arg */
	, &RequireComponent_t61_1_0_0/* this_arg */
	, &RequireComponent_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t61)/* instance_size */
	, sizeof (RequireComponent_t61)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t408_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AddComponentMenu_t408_AddComponentMenu__ctor_m2006_ParameterInfos[] = 
{
	{"menuName", 0, 134219154, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern const MethodInfo AddComponentMenu__ctor_m2006_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2006/* method */
	, &AddComponentMenu_t408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AddComponentMenu_t408_AddComponentMenu__ctor_m2006_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AddComponentMenu_t408_AddComponentMenu__ctor_m2038_ParameterInfos[] = 
{
	{"menuName", 0, 134219155, 0, &String_t_0_0_0},
	{"order", 1, 134219156, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern const MethodInfo AddComponentMenu__ctor_m2038_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2038/* method */
	, &AddComponentMenu_t408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, AddComponentMenu_t408_AddComponentMenu__ctor_m2038_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddComponentMenu_t408_MethodInfos[] =
{
	&AddComponentMenu__ctor_m2006_MethodInfo,
	&AddComponentMenu__ctor_m2038_MethodInfo,
	NULL
};
static const Il2CppMethodReference AddComponentMenu_t408_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AddComponentMenu_t408_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t408_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t408_0_0_0;
extern const Il2CppType AddComponentMenu_t408_1_0_0;
struct AddComponentMenu_t408;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t408_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AddComponentMenu_t408_VTable/* vtableMethods */
	, AddComponentMenu_t408_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1013/* fieldStart */

};
TypeInfo AddComponentMenu_t408_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, AddComponentMenu_t408_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddComponentMenu_t408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t408_0_0_0/* byval_arg */
	, &AddComponentMenu_t408_1_0_0/* this_arg */
	, &AddComponentMenu_t408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t408)/* instance_size */
	, sizeof (AddComponentMenu_t408)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t416_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern const MethodInfo ExecuteInEditMode__ctor_m2043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecuteInEditMode__ctor_m2043/* method */
	, &ExecuteInEditMode_t416_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecuteInEditMode_t416_MethodInfos[] =
{
	&ExecuteInEditMode__ctor_m2043_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExecuteInEditMode_t416_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ExecuteInEditMode_t416_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t416_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t416_0_0_0;
extern const Il2CppType ExecuteInEditMode_t416_1_0_0;
struct ExecuteInEditMode_t416;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t416_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ExecuteInEditMode_t416_VTable/* vtableMethods */
	, ExecuteInEditMode_t416_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecuteInEditMode_t416_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, ExecuteInEditMode_t416_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecuteInEditMode_t416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t416_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t416_1_0_0/* this_arg */
	, &ExecuteInEditMode_t416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t416)/* instance_size */
	, sizeof (ExecuteInEditMode_t416)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t804_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CastHelper_1_t804_Il2CppGenericContainer;
extern TypeInfo CastHelper_1_t804_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CastHelper_1_t804_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CastHelper_1_t804_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CastHelper_1_t804_Il2CppGenericParametersArray[1] = 
{
	&CastHelper_1_t804_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CastHelper_1_t804_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CastHelper_1_t804_il2cpp_TypeInfo, 1, 0, CastHelper_1_t804_Il2CppGenericParametersArray };
static const MethodInfo* CastHelper_1_t804_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
static const Il2CppMethodReference CastHelper_1_t804_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool CastHelper_1_t804_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t804_0_0_0;
extern const Il2CppType CastHelper_1_t804_1_0_0;
extern const Il2CppType ValueType_t439_0_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, CastHelper_1_t804_VTable/* vtableMethods */
	, CastHelper_1_t804_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */

};
TypeInfo CastHelper_1_t804_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t804_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CastHelper_1_t804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t804_0_0_0/* byval_arg */
	, &CastHelper_1_t804_1_0_0/* this_arg */
	, &CastHelper_1_t804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CastHelper_1_t804_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t645_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern const MethodInfo SetupCoroutine__ctor_m3199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetupCoroutine__ctor_m3199/* method */
	, &SetupCoroutine_t645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t645_SetupCoroutine_InvokeMember_m3200_ParameterInfos[] = 
{
	{"behaviour", 0, 134219157, 0, &Object_t_0_0_0},
	{"name", 1, 134219158, 0, &String_t_0_0_0},
	{"variable", 2, 134219159, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeMember_m3200_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeMember_m3200/* method */
	, &SetupCoroutine_t645_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t645_SetupCoroutine_InvokeMember_m3200_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t645_SetupCoroutine_InvokeStatic_m3201_ParameterInfos[] = 
{
	{"klass", 0, 134219160, 0, &Type_t_0_0_0},
	{"name", 1, 134219161, 0, &String_t_0_0_0},
	{"variable", 2, 134219162, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeStatic_m3201_MethodInfo = 
{
	"InvokeStatic"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeStatic_m3201/* method */
	, &SetupCoroutine_t645_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t645_SetupCoroutine_InvokeStatic_m3201_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetupCoroutine_t645_MethodInfos[] =
{
	&SetupCoroutine__ctor_m3199_MethodInfo,
	&SetupCoroutine_InvokeMember_m3200_MethodInfo,
	&SetupCoroutine_InvokeStatic_m3201_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetupCoroutine_t645_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SetupCoroutine_t645_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t645_0_0_0;
extern const Il2CppType SetupCoroutine_t645_1_0_0;
struct SetupCoroutine_t645;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t645_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t645_VTable/* vtableMethods */
	, SetupCoroutine_t645_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetupCoroutine_t645_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, SetupCoroutine_t645_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetupCoroutine_t645_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t645_0_0_0/* byval_arg */
	, &SetupCoroutine_t645_1_0_0/* this_arg */
	, &SetupCoroutine_t645_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t645)/* instance_size */
	, sizeof (SetupCoroutine_t645)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t646_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WritableAttribute::.ctor()
extern const MethodInfo WritableAttribute__ctor_m3202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WritableAttribute__ctor_m3202/* method */
	, &WritableAttribute_t646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WritableAttribute_t646_MethodInfos[] =
{
	&WritableAttribute__ctor_m3202_MethodInfo,
	NULL
};
static const Il2CppMethodReference WritableAttribute_t646_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool WritableAttribute_t646_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t646_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t646_0_0_0;
extern const Il2CppType WritableAttribute_t646_1_0_0;
struct WritableAttribute_t646;
const Il2CppTypeDefinitionMetadata WritableAttribute_t646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t646_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, WritableAttribute_t646_VTable/* vtableMethods */
	, WritableAttribute_t646_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WritableAttribute_t646_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, WritableAttribute_t646_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WritableAttribute_t646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 600/* custom_attributes_cache */
	, &WritableAttribute_t646_0_0_0/* byval_arg */
	, &WritableAttribute_t646_1_0_0/* this_arg */
	, &WritableAttribute_t646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t646)/* instance_size */
	, sizeof (WritableAttribute_t646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t647_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern const MethodInfo AssemblyIsEditorAssembly__ctor_m3203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyIsEditorAssembly__ctor_m3203/* method */
	, &AssemblyIsEditorAssembly_t647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyIsEditorAssembly_t647_MethodInfos[] =
{
	&AssemblyIsEditorAssembly__ctor_m3203_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyIsEditorAssembly_t647_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyIsEditorAssembly_t647_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t647_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t647_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t647_1_0_0;
struct AssemblyIsEditorAssembly_t647;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t647_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t647_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t647_VTable/* vtableMethods */
	, AssemblyIsEditorAssembly_t647_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyIsEditorAssembly_t647_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, AssemblyIsEditorAssembly_t647_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyIsEditorAssembly_t647_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 601/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t647_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t647_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t647_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t647)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t647)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t648_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
extern const Il2CppType UserProfile_t658_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern const MethodInfo GcUserProfileData_ToUserProfile_m3204_MethodInfo = 
{
	"ToUserProfile"/* name */
	, (methodPointerType)&GcUserProfileData_ToUserProfile_m3204/* method */
	, &GcUserProfileData_t648_il2cpp_TypeInfo/* declaring_type */
	, &UserProfile_t658_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserProfileU5BU5D_t487_1_0_0;
extern const Il2CppType UserProfileU5BU5D_t487_1_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo GcUserProfileData_t648_GcUserProfileData_AddToArray_m3205_ParameterInfos[] = 
{
	{"array", 0, 134219163, 0, &UserProfileU5BU5D_t487_1_0_0},
	{"number", 1, 134219164, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UserProfileU5BU5DU26_t821_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern const MethodInfo GcUserProfileData_AddToArray_m3205_MethodInfo = 
{
	"AddToArray"/* name */
	, (methodPointerType)&GcUserProfileData_AddToArray_m3205/* method */
	, &GcUserProfileData_t648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UserProfileU5BU5DU26_t821_Int32_t54/* invoker_method */
	, GcUserProfileData_t648_GcUserProfileData_AddToArray_m3205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcUserProfileData_t648_MethodInfos[] =
{
	&GcUserProfileData_ToUserProfile_m3204_MethodInfo,
	&GcUserProfileData_AddToArray_m3205_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcUserProfileData_t648_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool GcUserProfileData_t648_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t648_0_0_0;
extern const Il2CppType GcUserProfileData_t648_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, GcUserProfileData_t648_VTable/* vtableMethods */
	, GcUserProfileData_t648_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1017/* fieldStart */

};
TypeInfo GcUserProfileData_t648_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcUserProfileData_t648_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcUserProfileData_t648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t648_0_0_0/* byval_arg */
	, &GcUserProfileData_t648_1_0_0/* this_arg */
	, &GcUserProfileData_t648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t648)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t648)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t649_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
extern const Il2CppType AchievementDescription_t660_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern const MethodInfo GcAchievementDescriptionData_ToAchievementDescription_m3206_MethodInfo = 
{
	"ToAchievementDescription"/* name */
	, (methodPointerType)&GcAchievementDescriptionData_ToAchievementDescription_m3206/* method */
	, &GcAchievementDescriptionData_t649_il2cpp_TypeInfo/* declaring_type */
	, &AchievementDescription_t660_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementDescriptionData_t649_MethodInfos[] =
{
	&GcAchievementDescriptionData_ToAchievementDescription_m3206_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementDescriptionData_t649_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool GcAchievementDescriptionData_t649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t649_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t649_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, GcAchievementDescriptionData_t649_VTable/* vtableMethods */
	, GcAchievementDescriptionData_t649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1021/* fieldStart */

};
TypeInfo GcAchievementDescriptionData_t649_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDescriptionData_t649_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementDescriptionData_t649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t649_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t649_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t649)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t649)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t650_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
extern const Il2CppType Achievement_t659_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern const MethodInfo GcAchievementData_ToAchievement_m3207_MethodInfo = 
{
	"ToAchievement"/* name */
	, (methodPointerType)&GcAchievementData_ToAchievement_m3207/* method */
	, &GcAchievementData_t650_il2cpp_TypeInfo/* declaring_type */
	, &Achievement_t659_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementData_t650_MethodInfos[] =
{
	&GcAchievementData_ToAchievement_m3207_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementData_t650_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool GcAchievementData_t650_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t650_0_0_0;
extern const Il2CppType GcAchievementData_t650_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t650_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, GcAchievementData_t650_VTable/* vtableMethods */
	, GcAchievementData_t650_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1028/* fieldStart */

};
TypeInfo GcAchievementData_t650_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementData_t650_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementData_t650_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t650_0_0_0/* byval_arg */
	, &GcAchievementData_t650_1_0_0/* this_arg */
	, &GcAchievementData_t650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t650_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t650_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t650_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t650)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t650)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t650_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t651_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
extern const Il2CppType Score_t661_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern const MethodInfo GcScoreData_ToScore_m3208_MethodInfo = 
{
	"ToScore"/* name */
	, (methodPointerType)&GcScoreData_ToScore_m3208/* method */
	, &GcScoreData_t651_il2cpp_TypeInfo/* declaring_type */
	, &Score_t661_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcScoreData_t651_MethodInfos[] =
{
	&GcScoreData_ToScore_m3208_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcScoreData_t651_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool GcScoreData_t651_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t651_0_0_0;
extern const Il2CppType GcScoreData_t651_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t651_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, GcScoreData_t651_VTable/* vtableMethods */
	, GcScoreData_t651_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1033/* fieldStart */

};
TypeInfo GcScoreData_t651_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreData_t651_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcScoreData_t651_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t651_0_0_0/* byval_arg */
	, &GcScoreData_t651_1_0_0/* this_arg */
	, &GcScoreData_t651_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t651_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t651_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t651_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t651)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t651)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t651_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t652_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_width()
extern const MethodInfo Resolution_get_width_m3209_MethodInfo = 
{
	"get_width"/* name */
	, (methodPointerType)&Resolution_get_width_m3209/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Resolution_t652_Resolution_set_width_m3210_ParameterInfos[] = 
{
	{"value", 0, 134219165, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern const MethodInfo Resolution_set_width_m3210_MethodInfo = 
{
	"set_width"/* name */
	, (methodPointerType)&Resolution_set_width_m3210/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Resolution_t652_Resolution_set_width_m3210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_height()
extern const MethodInfo Resolution_get_height_m3211_MethodInfo = 
{
	"get_height"/* name */
	, (methodPointerType)&Resolution_get_height_m3211/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Resolution_t652_Resolution_set_height_m3212_ParameterInfos[] = 
{
	{"value", 0, 134219166, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern const MethodInfo Resolution_set_height_m3212_MethodInfo = 
{
	"set_height"/* name */
	, (methodPointerType)&Resolution_set_height_m3212/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Resolution_t652_Resolution_set_height_m3212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern const MethodInfo Resolution_get_refreshRate_m3213_MethodInfo = 
{
	"get_refreshRate"/* name */
	, (methodPointerType)&Resolution_get_refreshRate_m3213/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Resolution_t652_Resolution_set_refreshRate_m3214_ParameterInfos[] = 
{
	{"value", 0, 134219167, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern const MethodInfo Resolution_set_refreshRate_m3214_MethodInfo = 
{
	"set_refreshRate"/* name */
	, (methodPointerType)&Resolution_set_refreshRate_m3214/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Resolution_t652_Resolution_set_refreshRate_m3214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Resolution::ToString()
extern const MethodInfo Resolution_ToString_m3215_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Resolution_ToString_m3215/* method */
	, &Resolution_t652_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Resolution_t652_MethodInfos[] =
{
	&Resolution_get_width_m3209_MethodInfo,
	&Resolution_set_width_m3210_MethodInfo,
	&Resolution_get_height_m3211_MethodInfo,
	&Resolution_set_height_m3212_MethodInfo,
	&Resolution_get_refreshRate_m3213_MethodInfo,
	&Resolution_set_refreshRate_m3214_MethodInfo,
	&Resolution_ToString_m3215_MethodInfo,
	NULL
};
extern const MethodInfo Resolution_get_width_m3209_MethodInfo;
extern const MethodInfo Resolution_set_width_m3210_MethodInfo;
static const PropertyInfo Resolution_t652____width_PropertyInfo = 
{
	&Resolution_t652_il2cpp_TypeInfo/* parent */
	, "width"/* name */
	, &Resolution_get_width_m3209_MethodInfo/* get */
	, &Resolution_set_width_m3210_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_height_m3211_MethodInfo;
extern const MethodInfo Resolution_set_height_m3212_MethodInfo;
static const PropertyInfo Resolution_t652____height_PropertyInfo = 
{
	&Resolution_t652_il2cpp_TypeInfo/* parent */
	, "height"/* name */
	, &Resolution_get_height_m3211_MethodInfo/* get */
	, &Resolution_set_height_m3212_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_refreshRate_m3213_MethodInfo;
extern const MethodInfo Resolution_set_refreshRate_m3214_MethodInfo;
static const PropertyInfo Resolution_t652____refreshRate_PropertyInfo = 
{
	&Resolution_t652_il2cpp_TypeInfo/* parent */
	, "refreshRate"/* name */
	, &Resolution_get_refreshRate_m3213_MethodInfo/* get */
	, &Resolution_set_refreshRate_m3214_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Resolution_t652_PropertyInfos[] =
{
	&Resolution_t652____width_PropertyInfo,
	&Resolution_t652____height_PropertyInfo,
	&Resolution_t652____refreshRate_PropertyInfo,
	NULL
};
extern const MethodInfo Resolution_ToString_m3215_MethodInfo;
static const Il2CppMethodReference Resolution_t652_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&Resolution_ToString_m3215_MethodInfo,
};
static bool Resolution_t652_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t652_0_0_0;
extern const Il2CppType Resolution_t652_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t652_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Resolution_t652_VTable/* vtableMethods */
	, Resolution_t652_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1040/* fieldStart */

};
TypeInfo Resolution_t652_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, Resolution_t652_MethodInfos/* methods */
	, Resolution_t652_PropertyInfos/* properties */
	, NULL/* events */
	, &Resolution_t652_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t652_0_0_0/* byval_arg */
	, &Resolution_t652_1_0_0/* this_arg */
	, &Resolution_t652_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t652)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t652)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t652 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t653_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const MethodInfo* RenderBuffer_t653_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderBuffer_t653_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool RenderBuffer_t653_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t653_0_0_0;
extern const Il2CppType RenderBuffer_t653_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t653_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, RenderBuffer_t653_VTable/* vtableMethods */
	, RenderBuffer_t653_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1043/* fieldStart */

};
TypeInfo RenderBuffer_t653_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, RenderBuffer_t653_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RenderBuffer_t653_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t653_0_0_0/* byval_arg */
	, &RenderBuffer_t653_1_0_0/* this_arg */
	, &RenderBuffer_t653_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t653)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t653)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t653 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t654_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const MethodInfo* CameraClearFlags_t654_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference CameraClearFlags_t654_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool CameraClearFlags_t654_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair CameraClearFlags_t654_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t654_0_0_0;
extern const Il2CppType CameraClearFlags_t654_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t654_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t654_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, CameraClearFlags_t654_VTable/* vtableMethods */
	, CameraClearFlags_t654_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1045/* fieldStart */

};
TypeInfo CameraClearFlags_t654_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, CameraClearFlags_t654_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t654_0_0_0/* byval_arg */
	, &CameraClearFlags_t654_1_0_0/* this_arg */
	, &CameraClearFlags_t654_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t654)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t654)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t655_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const MethodInfo* TextureFormat_t655_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureFormat_t655_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TextureFormat_t655_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureFormat_t655_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t655_0_0_0;
extern const Il2CppType TextureFormat_t655_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t655_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t655_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TextureFormat_t655_VTable/* vtableMethods */
	, TextureFormat_t655_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1051/* fieldStart */

};
TypeInfo TextureFormat_t655_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t655_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t655_0_0_0/* byval_arg */
	, &TextureFormat_t655_1_0_0/* this_arg */
	, &TextureFormat_t655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t655)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t655)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t656_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const MethodInfo* ReflectionProbeBlendInfo_t656_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReflectionProbeBlendInfo_t656_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool ReflectionProbeBlendInfo_t656_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t656_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t656_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t656_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t656_VTable/* vtableMethods */
	, ReflectionProbeBlendInfo_t656_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1096/* fieldStart */

};
TypeInfo ReflectionProbeBlendInfo_t656_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t656_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionProbeBlendInfo_t656_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t656_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t656_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t656_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t656)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t656)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t488_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern const MethodInfo LocalUser__ctor_m3216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m3216/* method */
	, &LocalUser_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IUserProfileU5BU5D_t657_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t657_0_0_0;
static const ParameterInfo LocalUser_t488_LocalUser_SetFriends_m3217_ParameterInfos[] = 
{
	{"friends", 0, 134219168, 0, &IUserProfileU5BU5D_t657_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern const MethodInfo LocalUser_SetFriends_m3217_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m3217/* method */
	, &LocalUser_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LocalUser_t488_LocalUser_SetFriends_m3217_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo LocalUser_t488_LocalUser_SetAuthenticated_m3218_ParameterInfos[] = 
{
	{"value", 0, 134219169, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern const MethodInfo LocalUser_SetAuthenticated_m3218_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m3218/* method */
	, &LocalUser_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, LocalUser_t488_LocalUser_SetAuthenticated_m3218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo LocalUser_t488_LocalUser_SetUnderage_m3219_ParameterInfos[] = 
{
	{"value", 0, 134219170, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern const MethodInfo LocalUser_SetUnderage_m3219_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m3219/* method */
	, &LocalUser_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, LocalUser_t488_LocalUser_SetUnderage_m3219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern const MethodInfo LocalUser_get_authenticated_m3220_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m3220/* method */
	, &LocalUser_t488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LocalUser_t488_MethodInfos[] =
{
	&LocalUser__ctor_m3216_MethodInfo,
	&LocalUser_SetFriends_m3217_MethodInfo,
	&LocalUser_SetAuthenticated_m3218_MethodInfo,
	&LocalUser_SetUnderage_m3219_MethodInfo,
	&LocalUser_get_authenticated_m3220_MethodInfo,
	NULL
};
extern const MethodInfo LocalUser_get_authenticated_m3220_MethodInfo;
static const PropertyInfo LocalUser_t488____authenticated_PropertyInfo = 
{
	&LocalUser_t488_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m3220_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LocalUser_t488_PropertyInfos[] =
{
	&LocalUser_t488____authenticated_PropertyInfo,
	NULL
};
extern const MethodInfo UserProfile_ToString_m3223_MethodInfo;
extern const MethodInfo UserProfile_get_userName_m3227_MethodInfo;
extern const MethodInfo UserProfile_get_id_m3228_MethodInfo;
extern const MethodInfo UserProfile_get_isFriend_m3229_MethodInfo;
extern const MethodInfo UserProfile_get_state_m3230_MethodInfo;
static const Il2CppMethodReference LocalUser_t488_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UserProfile_ToString_m3223_MethodInfo,
	&UserProfile_get_userName_m3227_MethodInfo,
	&UserProfile_get_id_m3228_MethodInfo,
	&UserProfile_get_isFriend_m3229_MethodInfo,
	&UserProfile_get_state_m3230_MethodInfo,
	&LocalUser_get_authenticated_m3220_MethodInfo,
};
static bool LocalUser_t488_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILocalUser_t698_0_0_0;
extern const Il2CppType IUserProfile_t806_0_0_0;
static const Il2CppType* LocalUser_t488_InterfacesTypeInfos[] = 
{
	&ILocalUser_t698_0_0_0,
	&IUserProfile_t806_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t488_InterfacesOffsets[] = 
{
	{ &IUserProfile_t806_0_0_0, 4},
	{ &ILocalUser_t698_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t488_0_0_0;
extern const Il2CppType LocalUser_t488_1_0_0;
struct LocalUser_t488;
const Il2CppTypeDefinitionMetadata LocalUser_t488_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t488_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t488_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t658_0_0_0/* parent */
	, LocalUser_t488_VTable/* vtableMethods */
	, LocalUser_t488_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1098/* fieldStart */

};
TypeInfo LocalUser_t488_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t488_MethodInfos/* methods */
	, LocalUser_t488_PropertyInfos/* properties */
	, NULL/* events */
	, &LocalUser_t488_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t488_0_0_0/* byval_arg */
	, &LocalUser_t488_1_0_0/* this_arg */
	, &LocalUser_t488_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t488)/* instance_size */
	, sizeof (LocalUser_t488)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t658_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern const MethodInfo UserProfile__ctor_m3221_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m3221/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType UserState_t669_0_0_0;
extern const Il2CppType UserState_t669_0_0_0;
extern const Il2CppType Texture2D_t181_0_0_0;
extern const Il2CppType Texture2D_t181_0_0_0;
static const ParameterInfo UserProfile_t658_UserProfile__ctor_m3222_ParameterInfos[] = 
{
	{"name", 0, 134219171, 0, &String_t_0_0_0},
	{"id", 1, 134219172, 0, &String_t_0_0_0},
	{"friend", 2, 134219173, 0, &Boolean_t72_0_0_0},
	{"state", 3, 134219174, 0, &UserState_t669_0_0_0},
	{"image", 4, 134219175, 0, &Texture2D_t181_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern const MethodInfo UserProfile__ctor_m3222_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m3222/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Int32_t54_Object_t/* invoker_method */
	, UserProfile_t658_UserProfile__ctor_m3222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern const MethodInfo UserProfile_ToString_m3223_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m3223/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t658_UserProfile_SetUserName_m3224_ParameterInfos[] = 
{
	{"name", 0, 134219176, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern const MethodInfo UserProfile_SetUserName_m3224_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m3224/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UserProfile_t658_UserProfile_SetUserName_m3224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t658_UserProfile_SetUserID_m3225_ParameterInfos[] = 
{
	{"id", 0, 134219177, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern const MethodInfo UserProfile_SetUserID_m3225_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m3225/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UserProfile_t658_UserProfile_SetUserID_m3225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t181_0_0_0;
static const ParameterInfo UserProfile_t658_UserProfile_SetImage_m3226_ParameterInfos[] = 
{
	{"image", 0, 134219178, 0, &Texture2D_t181_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern const MethodInfo UserProfile_SetImage_m3226_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m3226/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UserProfile_t658_UserProfile_SetImage_m3226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern const MethodInfo UserProfile_get_userName_m3227_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m3227/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern const MethodInfo UserProfile_get_id_m3228_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m3228/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern const MethodInfo UserProfile_get_isFriend_m3229_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m3229/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserState_t669 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern const MethodInfo UserProfile_get_state_m3230_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m3230/* method */
	, &UserProfile_t658_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t669_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t669/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserProfile_t658_MethodInfos[] =
{
	&UserProfile__ctor_m3221_MethodInfo,
	&UserProfile__ctor_m3222_MethodInfo,
	&UserProfile_ToString_m3223_MethodInfo,
	&UserProfile_SetUserName_m3224_MethodInfo,
	&UserProfile_SetUserID_m3225_MethodInfo,
	&UserProfile_SetImage_m3226_MethodInfo,
	&UserProfile_get_userName_m3227_MethodInfo,
	&UserProfile_get_id_m3228_MethodInfo,
	&UserProfile_get_isFriend_m3229_MethodInfo,
	&UserProfile_get_state_m3230_MethodInfo,
	NULL
};
static const PropertyInfo UserProfile_t658____userName_PropertyInfo = 
{
	&UserProfile_t658_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m3227_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t658____id_PropertyInfo = 
{
	&UserProfile_t658_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m3228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t658____isFriend_PropertyInfo = 
{
	&UserProfile_t658_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m3229_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t658____state_PropertyInfo = 
{
	&UserProfile_t658_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m3230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UserProfile_t658_PropertyInfos[] =
{
	&UserProfile_t658____userName_PropertyInfo,
	&UserProfile_t658____id_PropertyInfo,
	&UserProfile_t658____isFriend_PropertyInfo,
	&UserProfile_t658____state_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UserProfile_t658_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UserProfile_ToString_m3223_MethodInfo,
	&UserProfile_get_userName_m3227_MethodInfo,
	&UserProfile_get_id_m3228_MethodInfo,
	&UserProfile_get_isFriend_m3229_MethodInfo,
	&UserProfile_get_state_m3230_MethodInfo,
};
static bool UserProfile_t658_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UserProfile_t658_InterfacesTypeInfos[] = 
{
	&IUserProfile_t806_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t658_InterfacesOffsets[] = 
{
	{ &IUserProfile_t806_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t658_1_0_0;
struct UserProfile_t658;
const Il2CppTypeDefinitionMetadata UserProfile_t658_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t658_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t658_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t658_VTable/* vtableMethods */
	, UserProfile_t658_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1101/* fieldStart */

};
TypeInfo UserProfile_t658_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t658_MethodInfos/* methods */
	, UserProfile_t658_PropertyInfos/* properties */
	, NULL/* events */
	, &UserProfile_t658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t658_0_0_0/* byval_arg */
	, &UserProfile_t658_1_0_0/* this_arg */
	, &UserProfile_t658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t658)/* instance_size */
	, sizeof (UserProfile_t658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t659_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType DateTime_t508_0_0_0;
extern const Il2CppType DateTime_t508_0_0_0;
static const ParameterInfo Achievement_t659_Achievement__ctor_m3231_ParameterInfos[] = 
{
	{"id", 0, 134219179, 0, &String_t_0_0_0},
	{"percentCompleted", 1, 134219180, 0, &Double_t752_0_0_0},
	{"completed", 2, 134219181, 0, &Boolean_t72_0_0_0},
	{"hidden", 3, 134219182, 0, &Boolean_t72_0_0_0},
	{"lastReportedDate", 4, 134219183, 0, &DateTime_t508_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Double_t752_SByte_t73_SByte_t73_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern const MethodInfo Achievement__ctor_m3231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m3231/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Double_t752_SByte_t73_SByte_t73_DateTime_t508/* invoker_method */
	, Achievement_t659_Achievement__ctor_m3231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Achievement_t659_Achievement__ctor_m3232_ParameterInfos[] = 
{
	{"id", 0, 134219184, 0, &String_t_0_0_0},
	{"percent", 1, 134219185, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern const MethodInfo Achievement__ctor_m3232_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m3232/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Double_t752/* invoker_method */
	, Achievement_t659_Achievement__ctor_m3232_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern const MethodInfo Achievement__ctor_m3233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m3233/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern const MethodInfo Achievement_ToString_m3234_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m3234/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern const MethodInfo Achievement_get_id_m3235_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m3235/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 604/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Achievement_t659_Achievement_set_id_m3236_ParameterInfos[] = 
{
	{"value", 0, 134219186, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern const MethodInfo Achievement_set_id_m3236_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m3236/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Achievement_t659_Achievement_set_id_m3236_ParameterInfos/* parameters */
	, 605/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern const MethodInfo Achievement_get_percentCompleted_m3237_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m3237/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752/* invoker_method */
	, NULL/* parameters */
	, 606/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t752_0_0_0;
static const ParameterInfo Achievement_t659_Achievement_set_percentCompleted_m3238_ParameterInfos[] = 
{
	{"value", 0, 134219187, 0, &Double_t752_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Double_t752 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern const MethodInfo Achievement_set_percentCompleted_m3238_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m3238/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Double_t752/* invoker_method */
	, Achievement_t659_Achievement_set_percentCompleted_m3238_ParameterInfos/* parameters */
	, 607/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern const MethodInfo Achievement_get_completed_m3239_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m3239/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern const MethodInfo Achievement_get_hidden_m3240_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m3240/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern const MethodInfo Achievement_get_lastReportedDate_m3241_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m3241/* method */
	, &Achievement_t659_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t508_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t508/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Achievement_t659_MethodInfos[] =
{
	&Achievement__ctor_m3231_MethodInfo,
	&Achievement__ctor_m3232_MethodInfo,
	&Achievement__ctor_m3233_MethodInfo,
	&Achievement_ToString_m3234_MethodInfo,
	&Achievement_get_id_m3235_MethodInfo,
	&Achievement_set_id_m3236_MethodInfo,
	&Achievement_get_percentCompleted_m3237_MethodInfo,
	&Achievement_set_percentCompleted_m3238_MethodInfo,
	&Achievement_get_completed_m3239_MethodInfo,
	&Achievement_get_hidden_m3240_MethodInfo,
	&Achievement_get_lastReportedDate_m3241_MethodInfo,
	NULL
};
extern const MethodInfo Achievement_get_id_m3235_MethodInfo;
extern const MethodInfo Achievement_set_id_m3236_MethodInfo;
static const PropertyInfo Achievement_t659____id_PropertyInfo = 
{
	&Achievement_t659_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m3235_MethodInfo/* get */
	, &Achievement_set_id_m3236_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_percentCompleted_m3237_MethodInfo;
extern const MethodInfo Achievement_set_percentCompleted_m3238_MethodInfo;
static const PropertyInfo Achievement_t659____percentCompleted_PropertyInfo = 
{
	&Achievement_t659_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m3237_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m3238_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_completed_m3239_MethodInfo;
static const PropertyInfo Achievement_t659____completed_PropertyInfo = 
{
	&Achievement_t659_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m3239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_hidden_m3240_MethodInfo;
static const PropertyInfo Achievement_t659____hidden_PropertyInfo = 
{
	&Achievement_t659_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m3240_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_lastReportedDate_m3241_MethodInfo;
static const PropertyInfo Achievement_t659____lastReportedDate_PropertyInfo = 
{
	&Achievement_t659_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m3241_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Achievement_t659_PropertyInfos[] =
{
	&Achievement_t659____id_PropertyInfo,
	&Achievement_t659____percentCompleted_PropertyInfo,
	&Achievement_t659____completed_PropertyInfo,
	&Achievement_t659____hidden_PropertyInfo,
	&Achievement_t659____lastReportedDate_PropertyInfo,
	NULL
};
extern const MethodInfo Achievement_ToString_m3234_MethodInfo;
static const Il2CppMethodReference Achievement_t659_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Achievement_ToString_m3234_MethodInfo,
	&Achievement_get_id_m3235_MethodInfo,
	&Achievement_set_id_m3236_MethodInfo,
	&Achievement_get_percentCompleted_m3237_MethodInfo,
	&Achievement_set_percentCompleted_m3238_MethodInfo,
	&Achievement_get_completed_m3239_MethodInfo,
	&Achievement_get_hidden_m3240_MethodInfo,
	&Achievement_get_lastReportedDate_m3241_MethodInfo,
};
static bool Achievement_t659_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievement_t702_0_0_0;
static const Il2CppType* Achievement_t659_InterfacesTypeInfos[] = 
{
	&IAchievement_t702_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t659_InterfacesOffsets[] = 
{
	{ &IAchievement_t702_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t659_1_0_0;
struct Achievement_t659;
const Il2CppTypeDefinitionMetadata Achievement_t659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t659_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t659_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t659_VTable/* vtableMethods */
	, Achievement_t659_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1106/* fieldStart */

};
TypeInfo Achievement_t659_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t659_MethodInfos/* methods */
	, Achievement_t659_PropertyInfos/* properties */
	, NULL/* events */
	, &Achievement_t659_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t659_0_0_0/* byval_arg */
	, &Achievement_t659_1_0_0/* this_arg */
	, &Achievement_t659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t659)/* instance_size */
	, sizeof (Achievement_t659)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t660_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Texture2D_t181_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AchievementDescription_t660_AchievementDescription__ctor_m3242_ParameterInfos[] = 
{
	{"id", 0, 134219188, 0, &String_t_0_0_0},
	{"title", 1, 134219189, 0, &String_t_0_0_0},
	{"image", 2, 134219190, 0, &Texture2D_t181_0_0_0},
	{"achievedDescription", 3, 134219191, 0, &String_t_0_0_0},
	{"unachievedDescription", 4, 134219192, 0, &String_t_0_0_0},
	{"hidden", 5, 134219193, 0, &Boolean_t72_0_0_0},
	{"points", 6, 134219194, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern const MethodInfo AchievementDescription__ctor_m3242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m3242/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t73_Int32_t54/* invoker_method */
	, AchievementDescription_t660_AchievementDescription__ctor_m3242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern const MethodInfo AchievementDescription_ToString_m3243_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m3243/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t181_0_0_0;
static const ParameterInfo AchievementDescription_t660_AchievementDescription_SetImage_m3244_ParameterInfos[] = 
{
	{"image", 0, 134219195, 0, &Texture2D_t181_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern const MethodInfo AchievementDescription_SetImage_m3244_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m3244/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AchievementDescription_t660_AchievementDescription_SetImage_m3244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern const MethodInfo AchievementDescription_get_id_m3245_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m3245/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 609/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AchievementDescription_t660_AchievementDescription_set_id_m3246_ParameterInfos[] = 
{
	{"value", 0, 134219196, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern const MethodInfo AchievementDescription_set_id_m3246_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m3246/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AchievementDescription_t660_AchievementDescription_set_id_m3246_ParameterInfos/* parameters */
	, 610/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern const MethodInfo AchievementDescription_get_title_m3247_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m3247/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern const MethodInfo AchievementDescription_get_achievedDescription_m3248_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m3248/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern const MethodInfo AchievementDescription_get_unachievedDescription_m3249_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m3249/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern const MethodInfo AchievementDescription_get_hidden_m3250_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m3250/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern const MethodInfo AchievementDescription_get_points_m3251_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m3251/* method */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AchievementDescription_t660_MethodInfos[] =
{
	&AchievementDescription__ctor_m3242_MethodInfo,
	&AchievementDescription_ToString_m3243_MethodInfo,
	&AchievementDescription_SetImage_m3244_MethodInfo,
	&AchievementDescription_get_id_m3245_MethodInfo,
	&AchievementDescription_set_id_m3246_MethodInfo,
	&AchievementDescription_get_title_m3247_MethodInfo,
	&AchievementDescription_get_achievedDescription_m3248_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m3249_MethodInfo,
	&AchievementDescription_get_hidden_m3250_MethodInfo,
	&AchievementDescription_get_points_m3251_MethodInfo,
	NULL
};
extern const MethodInfo AchievementDescription_get_id_m3245_MethodInfo;
extern const MethodInfo AchievementDescription_set_id_m3246_MethodInfo;
static const PropertyInfo AchievementDescription_t660____id_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m3245_MethodInfo/* get */
	, &AchievementDescription_set_id_m3246_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_title_m3247_MethodInfo;
static const PropertyInfo AchievementDescription_t660____title_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m3247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_achievedDescription_m3248_MethodInfo;
static const PropertyInfo AchievementDescription_t660____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m3248_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_unachievedDescription_m3249_MethodInfo;
static const PropertyInfo AchievementDescription_t660____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m3249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_hidden_m3250_MethodInfo;
static const PropertyInfo AchievementDescription_t660____hidden_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m3250_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_points_m3251_MethodInfo;
static const PropertyInfo AchievementDescription_t660____points_PropertyInfo = 
{
	&AchievementDescription_t660_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m3251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AchievementDescription_t660_PropertyInfos[] =
{
	&AchievementDescription_t660____id_PropertyInfo,
	&AchievementDescription_t660____title_PropertyInfo,
	&AchievementDescription_t660____achievedDescription_PropertyInfo,
	&AchievementDescription_t660____unachievedDescription_PropertyInfo,
	&AchievementDescription_t660____hidden_PropertyInfo,
	&AchievementDescription_t660____points_PropertyInfo,
	NULL
};
extern const MethodInfo AchievementDescription_ToString_m3243_MethodInfo;
static const Il2CppMethodReference AchievementDescription_t660_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&AchievementDescription_ToString_m3243_MethodInfo,
	&AchievementDescription_get_id_m3245_MethodInfo,
	&AchievementDescription_set_id_m3246_MethodInfo,
	&AchievementDescription_get_title_m3247_MethodInfo,
	&AchievementDescription_get_achievedDescription_m3248_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m3249_MethodInfo,
	&AchievementDescription_get_hidden_m3250_MethodInfo,
	&AchievementDescription_get_points_m3251_MethodInfo,
};
static bool AchievementDescription_t660_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievementDescription_t807_0_0_0;
static const Il2CppType* AchievementDescription_t660_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t807_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t660_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t807_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t660_1_0_0;
struct AchievementDescription_t660;
const Il2CppTypeDefinitionMetadata AchievementDescription_t660_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t660_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t660_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t660_VTable/* vtableMethods */
	, AchievementDescription_t660_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1111/* fieldStart */

};
TypeInfo AchievementDescription_t660_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t660_MethodInfos/* methods */
	, AchievementDescription_t660_PropertyInfos/* properties */
	, NULL/* events */
	, &AchievementDescription_t660_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t660_0_0_0/* byval_arg */
	, &AchievementDescription_t660_1_0_0/* this_arg */
	, &AchievementDescription_t660_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t660)/* instance_size */
	, sizeof (AchievementDescription_t660)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t661_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo Score_t661_Score__ctor_m3252_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219197, 0, &String_t_0_0_0},
	{"value", 1, 134219198, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern const MethodInfo Score__ctor_m3252_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m3252/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64_t753/* invoker_method */
	, Score_t661_Score__ctor_m3252_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType DateTime_t508_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Score_t661_Score__ctor_m3253_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219199, 0, &String_t_0_0_0},
	{"value", 1, 134219200, 0, &Int64_t753_0_0_0},
	{"userID", 2, 134219201, 0, &String_t_0_0_0},
	{"date", 3, 134219202, 0, &DateTime_t508_0_0_0},
	{"formattedValue", 4, 134219203, 0, &String_t_0_0_0},
	{"rank", 5, 134219204, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Object_t_DateTime_t508_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern const MethodInfo Score__ctor_m3253_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m3253/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64_t753_Object_t_DateTime_t508_Object_t_Int32_t54/* invoker_method */
	, Score_t661_Score__ctor_m3253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern const MethodInfo Score_ToString_m3254_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m3254/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern const MethodInfo Score_get_leaderboardID_m3255_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m3255/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 613/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Score_t661_Score_set_leaderboardID_m3256_ParameterInfos[] = 
{
	{"value", 0, 134219205, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern const MethodInfo Score_set_leaderboardID_m3256_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m3256/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Score_t661_Score_set_leaderboardID_m3256_ParameterInfos/* parameters */
	, 614/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern const MethodInfo Score_get_value_m3257_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m3257/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753/* invoker_method */
	, NULL/* parameters */
	, 615/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
static const ParameterInfo Score_t661_Score_set_value_m3258_ParameterInfos[] = 
{
	{"value", 0, 134219206, 0, &Int64_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern const MethodInfo Score_set_value_m3258_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m3258/* method */
	, &Score_t661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753/* invoker_method */
	, Score_t661_Score_set_value_m3258_ParameterInfos/* parameters */
	, 616/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Score_t661_MethodInfos[] =
{
	&Score__ctor_m3252_MethodInfo,
	&Score__ctor_m3253_MethodInfo,
	&Score_ToString_m3254_MethodInfo,
	&Score_get_leaderboardID_m3255_MethodInfo,
	&Score_set_leaderboardID_m3256_MethodInfo,
	&Score_get_value_m3257_MethodInfo,
	&Score_set_value_m3258_MethodInfo,
	NULL
};
extern const MethodInfo Score_get_leaderboardID_m3255_MethodInfo;
extern const MethodInfo Score_set_leaderboardID_m3256_MethodInfo;
static const PropertyInfo Score_t661____leaderboardID_PropertyInfo = 
{
	&Score_t661_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m3255_MethodInfo/* get */
	, &Score_set_leaderboardID_m3256_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Score_get_value_m3257_MethodInfo;
extern const MethodInfo Score_set_value_m3258_MethodInfo;
static const PropertyInfo Score_t661____value_PropertyInfo = 
{
	&Score_t661_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m3257_MethodInfo/* get */
	, &Score_set_value_m3258_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Score_t661_PropertyInfos[] =
{
	&Score_t661____leaderboardID_PropertyInfo,
	&Score_t661____value_PropertyInfo,
	NULL
};
extern const MethodInfo Score_ToString_m3254_MethodInfo;
static const Il2CppMethodReference Score_t661_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Score_ToString_m3254_MethodInfo,
	&Score_get_leaderboardID_m3255_MethodInfo,
	&Score_set_leaderboardID_m3256_MethodInfo,
	&Score_get_value_m3257_MethodInfo,
	&Score_set_value_m3258_MethodInfo,
};
static bool Score_t661_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IScore_t662_0_0_0;
static const Il2CppType* Score_t661_InterfacesTypeInfos[] = 
{
	&IScore_t662_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t661_InterfacesOffsets[] = 
{
	{ &IScore_t662_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t661_1_0_0;
struct Score_t661;
const Il2CppTypeDefinitionMetadata Score_t661_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t661_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t661_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t661_VTable/* vtableMethods */
	, Score_t661_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1118/* fieldStart */

};
TypeInfo Score_t661_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t661_MethodInfos/* methods */
	, Score_t661_PropertyInfos/* properties */
	, NULL/* events */
	, &Score_t661_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t661_0_0_0/* byval_arg */
	, &Score_t661_1_0_0/* this_arg */
	, &Score_t661_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t661)/* instance_size */
	, sizeof (Score_t661)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t491_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern const MethodInfo Leaderboard__ctor_m3259_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m3259/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern const MethodInfo Leaderboard_ToString_m3260_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m3260/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScore_t662_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_SetLocalUserScore_m3261_ParameterInfos[] = 
{
	{"score", 0, 134219207, 0, &IScore_t662_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern const MethodInfo Leaderboard_SetLocalUserScore_m3261_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m3261/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Leaderboard_t491_Leaderboard_SetLocalUserScore_m3261_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t744_0_0_0;
extern const Il2CppType UInt32_t744_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_SetMaxRange_m3262_ParameterInfos[] = 
{
	{"maxRange", 0, 134219208, 0, &UInt32_t744_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern const MethodInfo Leaderboard_SetMaxRange_m3262_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m3262/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Leaderboard_t491_Leaderboard_SetMaxRange_m3262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScoreU5BU5D_t663_0_0_0;
extern const Il2CppType IScoreU5BU5D_t663_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_SetScores_m3263_ParameterInfos[] = 
{
	{"scores", 0, 134219209, 0, &IScoreU5BU5D_t663_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern const MethodInfo Leaderboard_SetScores_m3263_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m3263/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Leaderboard_t491_Leaderboard_SetScores_m3263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_SetTitle_m3264_ParameterInfos[] = 
{
	{"title", 0, 134219210, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern const MethodInfo Leaderboard_SetTitle_m3264_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m3264/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Leaderboard_t491_Leaderboard_SetTitle_m3264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern const MethodInfo Leaderboard_GetUserFilter_m3265_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m3265/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern const MethodInfo Leaderboard_get_id_m3266_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m3266/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 621/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_set_id_m3267_ParameterInfos[] = 
{
	{"value", 0, 134219211, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern const MethodInfo Leaderboard_set_id_m3267_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m3267/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Leaderboard_t491_Leaderboard_set_id_m3267_ParameterInfos/* parameters */
	, 622/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t670_0_0_0;
extern void* RuntimeInvoker_UserScope_t670 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern const MethodInfo Leaderboard_get_userScope_m3268_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m3268/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t670_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t670/* invoker_method */
	, NULL/* parameters */
	, 623/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t670_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_set_userScope_m3269_ParameterInfos[] = 
{
	{"value", 0, 134219212, 0, &UserScope_t670_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern const MethodInfo Leaderboard_set_userScope_m3269_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m3269/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Leaderboard_t491_Leaderboard_set_userScope_m3269_ParameterInfos/* parameters */
	, 624/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t664_0_0_0;
extern void* RuntimeInvoker_Range_t664 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern const MethodInfo Leaderboard_get_range_m3270_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m3270/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Range_t664_0_0_0/* return_type */
	, RuntimeInvoker_Range_t664/* invoker_method */
	, NULL/* parameters */
	, 625/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t664_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_set_range_m3271_ParameterInfos[] = 
{
	{"value", 0, 134219213, 0, &Range_t664_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Range_t664 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern const MethodInfo Leaderboard_set_range_m3271_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m3271/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Range_t664/* invoker_method */
	, Leaderboard_t491_Leaderboard_set_range_m3271_ParameterInfos/* parameters */
	, 626/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t671_0_0_0;
extern void* RuntimeInvoker_TimeScope_t671 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern const MethodInfo Leaderboard_get_timeScope_m3272_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m3272/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t671_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t671/* invoker_method */
	, NULL/* parameters */
	, 627/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t671_0_0_0;
static const ParameterInfo Leaderboard_t491_Leaderboard_set_timeScope_m3273_ParameterInfos[] = 
{
	{"value", 0, 134219214, 0, &TimeScope_t671_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern const MethodInfo Leaderboard_set_timeScope_m3273_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m3273/* method */
	, &Leaderboard_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Leaderboard_t491_Leaderboard_set_timeScope_m3273_ParameterInfos/* parameters */
	, 628/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Leaderboard_t491_MethodInfos[] =
{
	&Leaderboard__ctor_m3259_MethodInfo,
	&Leaderboard_ToString_m3260_MethodInfo,
	&Leaderboard_SetLocalUserScore_m3261_MethodInfo,
	&Leaderboard_SetMaxRange_m3262_MethodInfo,
	&Leaderboard_SetScores_m3263_MethodInfo,
	&Leaderboard_SetTitle_m3264_MethodInfo,
	&Leaderboard_GetUserFilter_m3265_MethodInfo,
	&Leaderboard_get_id_m3266_MethodInfo,
	&Leaderboard_set_id_m3267_MethodInfo,
	&Leaderboard_get_userScope_m3268_MethodInfo,
	&Leaderboard_set_userScope_m3269_MethodInfo,
	&Leaderboard_get_range_m3270_MethodInfo,
	&Leaderboard_set_range_m3271_MethodInfo,
	&Leaderboard_get_timeScope_m3272_MethodInfo,
	&Leaderboard_set_timeScope_m3273_MethodInfo,
	NULL
};
extern const MethodInfo Leaderboard_get_id_m3266_MethodInfo;
extern const MethodInfo Leaderboard_set_id_m3267_MethodInfo;
static const PropertyInfo Leaderboard_t491____id_PropertyInfo = 
{
	&Leaderboard_t491_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m3266_MethodInfo/* get */
	, &Leaderboard_set_id_m3267_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_userScope_m3268_MethodInfo;
extern const MethodInfo Leaderboard_set_userScope_m3269_MethodInfo;
static const PropertyInfo Leaderboard_t491____userScope_PropertyInfo = 
{
	&Leaderboard_t491_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m3268_MethodInfo/* get */
	, &Leaderboard_set_userScope_m3269_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_range_m3270_MethodInfo;
extern const MethodInfo Leaderboard_set_range_m3271_MethodInfo;
static const PropertyInfo Leaderboard_t491____range_PropertyInfo = 
{
	&Leaderboard_t491_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m3270_MethodInfo/* get */
	, &Leaderboard_set_range_m3271_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_timeScope_m3272_MethodInfo;
extern const MethodInfo Leaderboard_set_timeScope_m3273_MethodInfo;
static const PropertyInfo Leaderboard_t491____timeScope_PropertyInfo = 
{
	&Leaderboard_t491_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m3272_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m3273_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Leaderboard_t491_PropertyInfos[] =
{
	&Leaderboard_t491____id_PropertyInfo,
	&Leaderboard_t491____userScope_PropertyInfo,
	&Leaderboard_t491____range_PropertyInfo,
	&Leaderboard_t491____timeScope_PropertyInfo,
	NULL
};
extern const MethodInfo Leaderboard_ToString_m3260_MethodInfo;
static const Il2CppMethodReference Leaderboard_t491_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Leaderboard_ToString_m3260_MethodInfo,
	&Leaderboard_get_id_m3266_MethodInfo,
	&Leaderboard_get_userScope_m3268_MethodInfo,
	&Leaderboard_get_range_m3270_MethodInfo,
	&Leaderboard_get_timeScope_m3272_MethodInfo,
	&Leaderboard_set_id_m3267_MethodInfo,
	&Leaderboard_set_userScope_m3269_MethodInfo,
	&Leaderboard_set_range_m3271_MethodInfo,
	&Leaderboard_set_timeScope_m3273_MethodInfo,
};
static bool Leaderboard_t491_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILeaderboard_t701_0_0_0;
static const Il2CppType* Leaderboard_t491_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t701_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t491_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t701_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t491_0_0_0;
extern const Il2CppType Leaderboard_t491_1_0_0;
struct Leaderboard_t491;
const Il2CppTypeDefinitionMetadata Leaderboard_t491_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t491_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t491_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t491_VTable/* vtableMethods */
	, Leaderboard_t491_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1124/* fieldStart */

};
TypeInfo Leaderboard_t491_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t491_MethodInfos/* methods */
	, Leaderboard_t491_PropertyInfos/* properties */
	, NULL/* events */
	, &Leaderboard_t491_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t491_0_0_0/* byval_arg */
	, &Leaderboard_t491_1_0_0/* this_arg */
	, &Leaderboard_t491_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t491)/* instance_size */
	, sizeof (Leaderboard_t491)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t665_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HitInfo_t665_HitInfo_SendMessage_m3274_ParameterInfos[] = 
{
	{"name", 0, 134219219, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern const MethodInfo HitInfo_SendMessage_m3274_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m3274/* method */
	, &HitInfo_t665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, HitInfo_t665_HitInfo_SendMessage_m3274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t665_0_0_0;
extern const Il2CppType HitInfo_t665_0_0_0;
extern const Il2CppType HitInfo_t665_0_0_0;
static const ParameterInfo HitInfo_t665_HitInfo_Compare_m3275_ParameterInfos[] = 
{
	{"lhs", 0, 134219220, 0, &HitInfo_t665_0_0_0},
	{"rhs", 1, 134219221, 0, &HitInfo_t665_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_HitInfo_t665_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_Compare_m3275_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m3275/* method */
	, &HitInfo_t665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_HitInfo_t665_HitInfo_t665/* invoker_method */
	, HitInfo_t665_HitInfo_Compare_m3275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t665_0_0_0;
static const ParameterInfo HitInfo_t665_HitInfo_op_Implicit_m3276_ParameterInfos[] = 
{
	{"exists", 0, 134219222, 0, &HitInfo_t665_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_op_Implicit_m3276_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m3276/* method */
	, &HitInfo_t665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_HitInfo_t665/* invoker_method */
	, HitInfo_t665_HitInfo_op_Implicit_m3276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HitInfo_t665_MethodInfos[] =
{
	&HitInfo_SendMessage_m3274_MethodInfo,
	&HitInfo_Compare_m3275_MethodInfo,
	&HitInfo_op_Implicit_m3276_MethodInfo,
	NULL
};
static const Il2CppMethodReference HitInfo_t665_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool HitInfo_t665_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t665_1_0_0;
extern TypeInfo SendMouseEvents_t668_il2cpp_TypeInfo;
extern const Il2CppType SendMouseEvents_t668_0_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t665_DefinitionMetadata = 
{
	&SendMouseEvents_t668_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, HitInfo_t665_VTable/* vtableMethods */
	, HitInfo_t665_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1134/* fieldStart */

};
TypeInfo HitInfo_t665_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t665_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HitInfo_t665_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t665_0_0_0/* byval_arg */
	, &HitInfo_t665_1_0_0/* this_arg */
	, &HitInfo_t665_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t665)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t665)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern const MethodInfo SendMouseEvents__cctor_m3277_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m3277/* method */
	, &SendMouseEvents_t668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo SendMouseEvents_t668_SendMouseEvents_DoSendMouseEvents_m3278_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134219215, 0, &Int32_t54_0_0_0},
	{"skipRTCameras", 1, 134219216, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern const MethodInfo SendMouseEvents_DoSendMouseEvents_m3278_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m3278/* method */
	, &SendMouseEvents_t668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, SendMouseEvents_t668_SendMouseEvents_DoSendMouseEvents_m3278_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType HitInfo_t665_0_0_0;
static const ParameterInfo SendMouseEvents_t668_SendMouseEvents_SendEvents_m3279_ParameterInfos[] = 
{
	{"i", 0, 134219217, 0, &Int32_t54_0_0_0},
	{"hit", 1, 134219218, 0, &HitInfo_t665_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo SendMouseEvents_SendEvents_m3279_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m3279/* method */
	, &SendMouseEvents_t668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_HitInfo_t665/* invoker_method */
	, SendMouseEvents_t668_SendMouseEvents_SendEvents_m3279_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SendMouseEvents_t668_MethodInfos[] =
{
	&SendMouseEvents__cctor_m3277_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m3278_MethodInfo,
	&SendMouseEvents_SendEvents_m3279_MethodInfo,
	NULL
};
static const Il2CppType* SendMouseEvents_t668_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t665_0_0_0,
};
static const Il2CppMethodReference SendMouseEvents_t668_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SendMouseEvents_t668_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t668_1_0_0;
struct SendMouseEvents_t668;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t668_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t668_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t668_VTable/* vtableMethods */
	, SendMouseEvents_t668_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1136/* fieldStart */

};
TypeInfo SendMouseEvents_t668_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t668_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SendMouseEvents_t668_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t668_0_0_0/* byval_arg */
	, &SendMouseEvents_t668_1_0_0/* this_arg */
	, &SendMouseEvents_t668_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t668)/* instance_size */
	, sizeof (SendMouseEvents_t668)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t668_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t805_il2cpp_TypeInfo;
extern const Il2CppType ILocalUser_t698_0_0_0;
extern const Il2CppType Action_1_t481_0_0_0;
extern const Il2CppType Action_1_t481_0_0_0;
static const ParameterInfo ISocialPlatform_t805_ISocialPlatform_Authenticate_m3593_ParameterInfos[] = 
{
	{"user", 0, 134219223, 0, &ILocalUser_t698_0_0_0},
	{"callback", 1, 134219224, 0, &Action_1_t481_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_Authenticate_m3593_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t805_ISocialPlatform_Authenticate_m3593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILocalUser_t698_0_0_0;
extern const Il2CppType Action_1_t481_0_0_0;
static const ParameterInfo ISocialPlatform_t805_ISocialPlatform_LoadFriends_m3594_ParameterInfos[] = 
{
	{"user", 0, 134219225, 0, &ILocalUser_t698_0_0_0},
	{"callback", 1, 134219226, 0, &Action_1_t481_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_LoadFriends_m3594_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t805_ISocialPlatform_LoadFriends_m3594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISocialPlatform_t805_MethodInfos[] =
{
	&ISocialPlatform_Authenticate_m3593_MethodInfo,
	&ISocialPlatform_LoadFriends_m3594_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t805_0_0_0;
extern const Il2CppType ISocialPlatform_t805_1_0_0;
struct ISocialPlatform_t805;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISocialPlatform_t805_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t805_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISocialPlatform_t805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t805_0_0_0/* byval_arg */
	, &ISocialPlatform_t805_1_0_0/* this_arg */
	, &ISocialPlatform_t805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t698_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
extern const MethodInfo ILocalUser_get_authenticated_m3595_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t698_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILocalUser_t698_MethodInfos[] =
{
	&ILocalUser_get_authenticated_m3595_MethodInfo,
	NULL
};
extern const MethodInfo ILocalUser_get_authenticated_m3595_MethodInfo;
static const PropertyInfo ILocalUser_t698____authenticated_PropertyInfo = 
{
	&ILocalUser_t698_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m3595_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILocalUser_t698_PropertyInfos[] =
{
	&ILocalUser_t698____authenticated_PropertyInfo,
	NULL
};
static const Il2CppType* ILocalUser_t698_InterfacesTypeInfos[] = 
{
	&IUserProfile_t806_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t698_1_0_0;
struct ILocalUser_t698;
const Il2CppTypeDefinitionMetadata ILocalUser_t698_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t698_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILocalUser_t698_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t698_MethodInfos/* methods */
	, ILocalUser_t698_PropertyInfos/* properties */
	, NULL/* events */
	, &ILocalUser_t698_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t698_0_0_0/* byval_arg */
	, &ILocalUser_t698_1_0_0/* this_arg */
	, &ILocalUser_t698_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t669_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const MethodInfo* UserState_t669_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserState_t669_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UserState_t669_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserState_t669_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t669_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t669_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UserState_t669_VTable/* vtableMethods */
	, UserState_t669_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1143/* fieldStart */

};
TypeInfo UserState_t669_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t669_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t669_0_0_0/* byval_arg */
	, &UserState_t669_1_0_0/* this_arg */
	, &UserState_t669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t669)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t669)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t806_il2cpp_TypeInfo;
static const MethodInfo* IUserProfile_t806_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t806_1_0_0;
struct IUserProfile_t806;
const Il2CppTypeDefinitionMetadata IUserProfile_t806_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IUserProfile_t806_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t806_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IUserProfile_t806_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t806_0_0_0/* byval_arg */
	, &IUserProfile_t806_1_0_0/* this_arg */
	, &IUserProfile_t806_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t702_il2cpp_TypeInfo;
static const MethodInfo* IAchievement_t702_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t702_1_0_0;
struct IAchievement_t702;
const Il2CppTypeDefinitionMetadata IAchievement_t702_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievement_t702_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t702_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievement_t702_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t702_0_0_0/* byval_arg */
	, &IAchievement_t702_1_0_0/* this_arg */
	, &IAchievement_t702_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t807_il2cpp_TypeInfo;
static const MethodInfo* IAchievementDescription_t807_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t807_1_0_0;
struct IAchievementDescription_t807;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t807_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievementDescription_t807_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t807_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievementDescription_t807_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t807_0_0_0/* byval_arg */
	, &IAchievementDescription_t807_1_0_0/* this_arg */
	, &IAchievementDescription_t807_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t662_il2cpp_TypeInfo;
static const MethodInfo* IScore_t662_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t662_1_0_0;
struct IScore_t662;
const Il2CppTypeDefinitionMetadata IScore_t662_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IScore_t662_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t662_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IScore_t662_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t662_0_0_0/* byval_arg */
	, &IScore_t662_1_0_0/* this_arg */
	, &IScore_t662_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t670_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const MethodInfo* UserScope_t670_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserScope_t670_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UserScope_t670_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserScope_t670_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t670_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t670_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UserScope_t670_VTable/* vtableMethods */
	, UserScope_t670_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1149/* fieldStart */

};
TypeInfo UserScope_t670_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t670_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t670_0_0_0/* byval_arg */
	, &UserScope_t670_1_0_0/* this_arg */
	, &UserScope_t670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t670)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t670)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t671_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const MethodInfo* TimeScope_t671_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TimeScope_t671_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TimeScope_t671_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimeScope_t671_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t671_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t671_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TimeScope_t671_VTable/* vtableMethods */
	, TimeScope_t671_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1152/* fieldStart */

};
TypeInfo TimeScope_t671_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t671_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t671_0_0_0/* byval_arg */
	, &TimeScope_t671_1_0_0/* this_arg */
	, &TimeScope_t671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t671)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t671)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t664_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Range_t664_Range__ctor_m3280_ParameterInfos[] = 
{
	{"fromValue", 0, 134219227, 0, &Int32_t54_0_0_0},
	{"valueCount", 1, 134219228, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern const MethodInfo Range__ctor_m3280_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m3280/* method */
	, &Range_t664_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, Range_t664_Range__ctor_m3280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Range_t664_MethodInfos[] =
{
	&Range__ctor_m3280_MethodInfo,
	NULL
};
static const Il2CppMethodReference Range_t664_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool Range_t664_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t664_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t664_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Range_t664_VTable/* vtableMethods */
	, Range_t664_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1156/* fieldStart */

};
TypeInfo Range_t664_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t664_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Range_t664_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t664_0_0_0/* byval_arg */
	, &Range_t664_1_0_0/* this_arg */
	, &Range_t664_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t664)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t664)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t664 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t701_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
extern const MethodInfo ILeaderboard_get_id_m3596_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t701_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserScope_t670 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
extern const MethodInfo ILeaderboard_get_userScope_m3597_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t701_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t670_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t670/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Range_t664 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
extern const MethodInfo ILeaderboard_get_range_m3598_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t701_il2cpp_TypeInfo/* declaring_type */
	, &Range_t664_0_0_0/* return_type */
	, RuntimeInvoker_Range_t664/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeScope_t671 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
extern const MethodInfo ILeaderboard_get_timeScope_m3599_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t701_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t671_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t671/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILeaderboard_t701_MethodInfos[] =
{
	&ILeaderboard_get_id_m3596_MethodInfo,
	&ILeaderboard_get_userScope_m3597_MethodInfo,
	&ILeaderboard_get_range_m3598_MethodInfo,
	&ILeaderboard_get_timeScope_m3599_MethodInfo,
	NULL
};
extern const MethodInfo ILeaderboard_get_id_m3596_MethodInfo;
static const PropertyInfo ILeaderboard_t701____id_PropertyInfo = 
{
	&ILeaderboard_t701_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m3596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_userScope_m3597_MethodInfo;
static const PropertyInfo ILeaderboard_t701____userScope_PropertyInfo = 
{
	&ILeaderboard_t701_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m3597_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_range_m3598_MethodInfo;
static const PropertyInfo ILeaderboard_t701____range_PropertyInfo = 
{
	&ILeaderboard_t701_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m3598_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_timeScope_m3599_MethodInfo;
static const PropertyInfo ILeaderboard_t701____timeScope_PropertyInfo = 
{
	&ILeaderboard_t701_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m3599_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILeaderboard_t701_PropertyInfos[] =
{
	&ILeaderboard_t701____id_PropertyInfo,
	&ILeaderboard_t701____userScope_PropertyInfo,
	&ILeaderboard_t701____range_PropertyInfo,
	&ILeaderboard_t701____timeScope_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t701_1_0_0;
struct ILeaderboard_t701;
const Il2CppTypeDefinitionMetadata ILeaderboard_t701_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILeaderboard_t701_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t701_MethodInfos/* methods */
	, ILeaderboard_t701_PropertyInfos/* properties */
	, NULL/* events */
	, &ILeaderboard_t701_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t701_0_0_0/* byval_arg */
	, &ILeaderboard_t701_1_0_0/* this_arg */
	, &ILeaderboard_t701_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t672_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern const MethodInfo PropertyAttribute__ctor_m3281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m3281/* method */
	, &PropertyAttribute_t672_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyAttribute_t672_MethodInfos[] =
{
	&PropertyAttribute__ctor_m3281_MethodInfo,
	NULL
};
static const Il2CppMethodReference PropertyAttribute_t672_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool PropertyAttribute_t672_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttribute_t672_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PropertyAttribute_t672_0_0_0;
extern const Il2CppType PropertyAttribute_t672_1_0_0;
struct PropertyAttribute_t672;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t672_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t672_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, PropertyAttribute_t672_VTable/* vtableMethods */
	, PropertyAttribute_t672_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyAttribute_t672_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t672_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropertyAttribute_t672_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 629/* custom_attributes_cache */
	, &PropertyAttribute_t672_0_0_0/* byval_arg */
	, &PropertyAttribute_t672_1_0_0/* this_arg */
	, &PropertyAttribute_t672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t672)/* instance_size */
	, sizeof (PropertyAttribute_t672)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t420_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TooltipAttribute_t420_TooltipAttribute__ctor_m2050_ParameterInfos[] = 
{
	{"tooltip", 0, 134219229, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern const MethodInfo TooltipAttribute__ctor_m2050_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m2050/* method */
	, &TooltipAttribute_t420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TooltipAttribute_t420_TooltipAttribute__ctor_m2050_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TooltipAttribute_t420_MethodInfos[] =
{
	&TooltipAttribute__ctor_m2050_MethodInfo,
	NULL
};
static const Il2CppMethodReference TooltipAttribute_t420_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TooltipAttribute_t420_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t420_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TooltipAttribute_t420_0_0_0;
extern const Il2CppType TooltipAttribute_t420_1_0_0;
struct TooltipAttribute_t420;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t420_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t420_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t672_0_0_0/* parent */
	, TooltipAttribute_t420_VTable/* vtableMethods */
	, TooltipAttribute_t420_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1158/* fieldStart */

};
TypeInfo TooltipAttribute_t420_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t420_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TooltipAttribute_t420_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 630/* custom_attributes_cache */
	, &TooltipAttribute_t420_0_0_0/* byval_arg */
	, &TooltipAttribute_t420_1_0_0/* this_arg */
	, &TooltipAttribute_t420_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t420)/* instance_size */
	, sizeof (TooltipAttribute_t420)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t418_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo SpaceAttribute_t418_SpaceAttribute__ctor_m2048_ParameterInfos[] = 
{
	{"height", 0, 134219230, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern const MethodInfo SpaceAttribute__ctor_m2048_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m2048/* method */
	, &SpaceAttribute_t418_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85/* invoker_method */
	, SpaceAttribute_t418_SpaceAttribute__ctor_m2048_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpaceAttribute_t418_MethodInfos[] =
{
	&SpaceAttribute__ctor_m2048_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpaceAttribute_t418_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SpaceAttribute_t418_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t418_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpaceAttribute_t418_0_0_0;
extern const Il2CppType SpaceAttribute_t418_1_0_0;
struct SpaceAttribute_t418;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t418_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t418_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t672_0_0_0/* parent */
	, SpaceAttribute_t418_VTable/* vtableMethods */
	, SpaceAttribute_t418_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1159/* fieldStart */

};
TypeInfo SpaceAttribute_t418_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t418_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpaceAttribute_t418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 631/* custom_attributes_cache */
	, &SpaceAttribute_t418_0_0_0/* byval_arg */
	, &SpaceAttribute_t418_1_0_0/* this_arg */
	, &SpaceAttribute_t418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t418)/* instance_size */
	, sizeof (SpaceAttribute_t418)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t415_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern const Il2CppType Single_t85_0_0_0;
extern const Il2CppType Single_t85_0_0_0;
static const ParameterInfo RangeAttribute_t415_RangeAttribute__ctor_m2042_ParameterInfos[] = 
{
	{"min", 0, 134219231, 0, &Single_t85_0_0_0},
	{"max", 1, 134219232, 0, &Single_t85_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern const MethodInfo RangeAttribute__ctor_m2042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m2042/* method */
	, &RangeAttribute_t415_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Single_t85_Single_t85/* invoker_method */
	, RangeAttribute_t415_RangeAttribute__ctor_m2042_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RangeAttribute_t415_MethodInfos[] =
{
	&RangeAttribute__ctor_m2042_MethodInfo,
	NULL
};
static const Il2CppMethodReference RangeAttribute_t415_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RangeAttribute_t415_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t415_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RangeAttribute_t415_0_0_0;
extern const Il2CppType RangeAttribute_t415_1_0_0;
struct RangeAttribute_t415;
const Il2CppTypeDefinitionMetadata RangeAttribute_t415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t415_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t672_0_0_0/* parent */
	, RangeAttribute_t415_VTable/* vtableMethods */
	, RangeAttribute_t415_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1160/* fieldStart */

};
TypeInfo RangeAttribute_t415_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t415_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RangeAttribute_t415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 632/* custom_attributes_cache */
	, &RangeAttribute_t415_0_0_0/* byval_arg */
	, &RangeAttribute_t415_1_0_0/* this_arg */
	, &RangeAttribute_t415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t415)/* instance_size */
	, sizeof (RangeAttribute_t415)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t421_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TextAreaAttribute_t421_TextAreaAttribute__ctor_m2053_ParameterInfos[] = 
{
	{"minLines", 0, 134219233, 0, &Int32_t54_0_0_0},
	{"maxLines", 1, 134219234, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TextAreaAttribute__ctor_m2053_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m2053/* method */
	, &TextAreaAttribute_t421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, TextAreaAttribute_t421_TextAreaAttribute__ctor_m2053_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextAreaAttribute_t421_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m2053_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextAreaAttribute_t421_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TextAreaAttribute_t421_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t421_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAreaAttribute_t421_0_0_0;
extern const Il2CppType TextAreaAttribute_t421_1_0_0;
struct TextAreaAttribute_t421;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t421_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t672_0_0_0/* parent */
	, TextAreaAttribute_t421_VTable/* vtableMethods */
	, TextAreaAttribute_t421_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1162/* fieldStart */

};
TypeInfo TextAreaAttribute_t421_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t421_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextAreaAttribute_t421_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 633/* custom_attributes_cache */
	, &TextAreaAttribute_t421_0_0_0/* byval_arg */
	, &TextAreaAttribute_t421_1_0_0/* this_arg */
	, &TextAreaAttribute_t421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t421)/* instance_size */
	, sizeof (TextAreaAttribute_t421)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t419_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern const MethodInfo SelectionBaseAttribute__ctor_m2049_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m2049/* method */
	, &SelectionBaseAttribute_t419_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SelectionBaseAttribute_t419_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m2049_MethodInfo,
	NULL
};
static const Il2CppMethodReference SelectionBaseAttribute_t419_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SelectionBaseAttribute_t419_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t419_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SelectionBaseAttribute_t419_0_0_0;
extern const Il2CppType SelectionBaseAttribute_t419_1_0_0;
struct SelectionBaseAttribute_t419;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t419_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, SelectionBaseAttribute_t419_VTable/* vtableMethods */
	, SelectionBaseAttribute_t419_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SelectionBaseAttribute_t419_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t419_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SelectionBaseAttribute_t419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 634/* custom_attributes_cache */
	, &SelectionBaseAttribute_t419_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t419_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t419)/* instance_size */
	, sizeof (SelectionBaseAttribute_t419)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t673_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
extern const MethodInfo SliderState__ctor_m3282_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m3282/* method */
	, &SliderState_t673_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderState_t673_MethodInfos[] =
{
	&SliderState__ctor_m3282_MethodInfo,
	NULL
};
static const Il2CppMethodReference SliderState_t673_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SliderState_t673_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t673_0_0_0;
extern const Il2CppType SliderState_t673_1_0_0;
struct SliderState_t673;
const Il2CppTypeDefinitionMetadata SliderState_t673_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t673_VTable/* vtableMethods */
	, SliderState_t673_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1164/* fieldStart */

};
TypeInfo SliderState_t673_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t673_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderState_t673_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t673_0_0_0/* byval_arg */
	, &SliderState_t673_1_0_0/* this_arg */
	, &SliderState_t673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t673)/* instance_size */
	, sizeof (SliderState_t673)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t674_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern const MethodInfo StackTraceUtility__ctor_m3283_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m3283/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern const MethodInfo StackTraceUtility__cctor_m3284_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m3284/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_SetProjectFolder_m3285_ParameterInfos[] = 
{
	{"folder", 0, 134219235, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern const MethodInfo StackTraceUtility_SetProjectFolder_m3285_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m3285/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_SetProjectFolder_m3285_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern const MethodInfo StackTraceUtility_ExtractStackTrace_m3286_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m3286/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 635/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_IsSystemStacktraceType_m3287_ParameterInfos[] = 
{
	{"name", 0, 134219236, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern const MethodInfo StackTraceUtility_IsSystemStacktraceType_m3287_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m3287/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_IsSystemStacktraceType_m3287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_ExtractStringFromException_m3288_ParameterInfos[] = 
{
	{"exception", 0, 134219237, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern const MethodInfo StackTraceUtility_ExtractStringFromException_m3288_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m3288/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_ExtractStringFromException_m3288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_1_0_2;
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_2;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_ExtractStringFromExceptionInternal_m3289_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219238, 0, &Object_t_0_0_0},
	{"message", 1, 134219239, 0, &String_t_1_0_2},
	{"stackTrace", 2, 134219240, 0, &String_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StringU26_t882_StringU26_t882 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern const MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m3289_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m3289/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StringU26_t882_StringU26_t882/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_ExtractStringFromExceptionInternal_m3289_ParameterInfos/* parameters */
	, 636/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_PostprocessStacktrace_m3290_ParameterInfos[] = 
{
	{"oldString", 0, 134219241, 0, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219242, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern const MethodInfo StackTraceUtility_PostprocessStacktrace_m3290_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m3290/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_PostprocessStacktrace_m3290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StackTrace_t44_0_0_0;
extern const Il2CppType StackTrace_t44_0_0_0;
static const ParameterInfo StackTraceUtility_t674_StackTraceUtility_ExtractFormattedStackTrace_m3291_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219243, 0, &StackTrace_t44_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern const MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m3291_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m3291/* method */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t674_StackTraceUtility_ExtractFormattedStackTrace_m3291_ParameterInfos/* parameters */
	, 637/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StackTraceUtility_t674_MethodInfos[] =
{
	&StackTraceUtility__ctor_m3283_MethodInfo,
	&StackTraceUtility__cctor_m3284_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m3285_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m3286_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m3287_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m3288_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m3289_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m3290_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m3291_MethodInfo,
	NULL
};
static const Il2CppMethodReference StackTraceUtility_t674_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool StackTraceUtility_t674_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t674_0_0_0;
extern const Il2CppType StackTraceUtility_t674_1_0_0;
struct StackTraceUtility_t674;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t674_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t674_VTable/* vtableMethods */
	, StackTraceUtility_t674_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1167/* fieldStart */

};
TypeInfo StackTraceUtility_t674_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t674_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StackTraceUtility_t674_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t674_0_0_0/* byval_arg */
	, &StackTraceUtility_t674_1_0_0/* this_arg */
	, &StackTraceUtility_t674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t674)/* instance_size */
	, sizeof (StackTraceUtility_t674)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t674_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t368_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
extern const MethodInfo UnityException__ctor_m3292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m3292/* method */
	, &UnityException_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnityException_t368_UnityException__ctor_m3293_ParameterInfos[] = 
{
	{"message", 0, 134219244, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern const MethodInfo UnityException__ctor_m3293_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m3293/* method */
	, &UnityException_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnityException_t368_UnityException__ctor_m3293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t42_0_0_0;
extern const Il2CppType Exception_t42_0_0_0;
static const ParameterInfo UnityException_t368_UnityException__ctor_m3294_ParameterInfos[] = 
{
	{"message", 0, 134219245, 0, &String_t_0_0_0},
	{"innerException", 1, 134219246, 0, &Exception_t42_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern const MethodInfo UnityException__ctor_m3294_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m3294/* method */
	, &UnityException_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, UnityException_t368_UnityException__ctor_m3294_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UnityException_t368_UnityException__ctor_m3295_ParameterInfos[] = 
{
	{"info", 0, 134219247, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134219248, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnityException__ctor_m3295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m3295/* method */
	, &UnityException_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UnityException_t368_UnityException__ctor_m3295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityException_t368_MethodInfos[] =
{
	&UnityException__ctor_m3292_MethodInfo,
	&UnityException__ctor_m3293_MethodInfo,
	&UnityException__ctor_m3294_MethodInfo,
	&UnityException__ctor_m3295_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m3652_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m3653_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m3654_MethodInfo;
extern const MethodInfo Exception_get_Message_m3655_MethodInfo;
extern const MethodInfo Exception_get_Source_m3656_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m3657_MethodInfo;
extern const MethodInfo Exception_GetType_m3658_MethodInfo;
static const Il2CppMethodReference UnityException_t368_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool UnityException_t368_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t428_0_0_0;
extern const Il2CppType _Exception_t824_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t368_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t368_0_0_0;
extern const Il2CppType UnityException_t368_1_0_0;
struct UnityException_t368;
const Il2CppTypeDefinitionMetadata UnityException_t368_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t368_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t42_0_0_0/* parent */
	, UnityException_t368_VTable/* vtableMethods */
	, UnityException_t368_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1168/* fieldStart */

};
TypeInfo UnityException_t368_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t368_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityException_t368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t368_0_0_0/* byval_arg */
	, &UnityException_t368_1_0_0/* this_arg */
	, &UnityException_t368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t368)/* instance_size */
	, sizeof (UnityException_t368)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t675_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern const MethodInfo SharedBetweenAnimatorsAttribute__ctor_m3296_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m3296/* method */
	, &SharedBetweenAnimatorsAttribute_t675_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SharedBetweenAnimatorsAttribute_t675_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m3296_MethodInfo,
	NULL
};
static const Il2CppMethodReference SharedBetweenAnimatorsAttribute_t675_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SharedBetweenAnimatorsAttribute_t675_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t675_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t675_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t675_1_0_0;
struct SharedBetweenAnimatorsAttribute_t675;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t675_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t675_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t675_VTable/* vtableMethods */
	, SharedBetweenAnimatorsAttribute_t675_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t675_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t675_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SharedBetweenAnimatorsAttribute_t675_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 638/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t675_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t675_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t675_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t675)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t675)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t676_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern const MethodInfo StateMachineBehaviour__ctor_m3297_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m3297/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateEnter_m3298_ParameterInfos[] = 
{
	{"animator", 0, 134219249, 0, &Animator_t329_0_0_0},
	{"stateInfo", 1, 134219250, 0, &AnimatorStateInfo_t578_0_0_0},
	{"layerIndex", 2, 134219251, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m3298_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m3298/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateEnter_m3298_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateUpdate_m3299_ParameterInfos[] = 
{
	{"animator", 0, 134219252, 0, &Animator_t329_0_0_0},
	{"stateInfo", 1, 134219253, 0, &AnimatorStateInfo_t578_0_0_0},
	{"layerIndex", 2, 134219254, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m3299_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m3299/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateUpdate_m3299_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateExit_m3300_ParameterInfos[] = 
{
	{"animator", 0, 134219255, 0, &Animator_t329_0_0_0},
	{"stateInfo", 1, 134219256, 0, &AnimatorStateInfo_t578_0_0_0},
	{"layerIndex", 2, 134219257, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateExit_m3300_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m3300/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateExit_m3300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMove_m3301_ParameterInfos[] = 
{
	{"animator", 0, 134219258, 0, &Animator_t329_0_0_0},
	{"stateInfo", 1, 134219259, 0, &AnimatorStateInfo_t578_0_0_0},
	{"layerIndex", 2, 134219260, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMove_m3301_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m3301/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMove_m3301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType AnimatorStateInfo_t578_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateIK_m3302_ParameterInfos[] = 
{
	{"animator", 0, 134219261, 0, &Animator_t329_0_0_0},
	{"stateInfo", 1, 134219262, 0, &AnimatorStateInfo_t578_0_0_0},
	{"layerIndex", 2, 134219263, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateIK_m3302_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m3302/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateIK_m3302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMachineEnter_m3303_ParameterInfos[] = 
{
	{"animator", 0, 134219264, 0, &Animator_t329_0_0_0},
	{"stateMachinePathHash", 1, 134219265, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m3303_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m3303/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMachineEnter_m3303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t329_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMachineExit_m3304_ParameterInfos[] = 
{
	{"animator", 0, 134219266, 0, &Animator_t329_0_0_0},
	{"stateMachinePathHash", 1, 134219267, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m3304_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m3304/* method */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, StateMachineBehaviour_t676_StateMachineBehaviour_OnStateMachineExit_m3304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StateMachineBehaviour_t676_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m3297_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m3298_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m3299_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m3300_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m3301_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m3302_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m3303_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m3304_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m217_MethodInfo;
extern const MethodInfo Object_GetHashCode_m219_MethodInfo;
extern const MethodInfo Object_ToString_m220_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m3298_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m3299_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateExit_m3300_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMove_m3301_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateIK_m3302_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m3303_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m3304_MethodInfo;
static const Il2CppMethodReference StateMachineBehaviour_t676_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m3298_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m3299_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m3300_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m3301_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m3302_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m3303_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m3304_MethodInfo,
};
static bool StateMachineBehaviour_t676_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t676_0_0_0;
extern const Il2CppType StateMachineBehaviour_t676_1_0_0;
extern const Il2CppType ScriptableObject_t480_0_0_0;
struct StateMachineBehaviour_t676;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t676_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t480_0_0_0/* parent */
	, StateMachineBehaviour_t676_VTable/* vtableMethods */
	, StateMachineBehaviour_t676_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StateMachineBehaviour_t676_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t676_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StateMachineBehaviour_t676_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t676_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t676_1_0_0/* this_arg */
	, &StateMachineBehaviour_t676_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t676)/* instance_size */
	, sizeof (StateMachineBehaviour_t676)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t677_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const MethodInfo* DblClickSnapping_t677_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DblClickSnapping_t677_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool DblClickSnapping_t677_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t677_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t677_0_0_0;
extern const Il2CppType DblClickSnapping_t677_1_0_0;
extern TypeInfo TextEditor_t372_il2cpp_TypeInfo;
extern const Il2CppType TextEditor_t372_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t367_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t677_DefinitionMetadata = 
{
	&TextEditor_t372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t677_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, DblClickSnapping_t677_VTable/* vtableMethods */
	, DblClickSnapping_t677_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1170/* fieldStart */

};
TypeInfo DblClickSnapping_t677_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t677_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t677_0_0_0/* byval_arg */
	, &DblClickSnapping_t677_1_0_0/* this_arg */
	, &DblClickSnapping_t677_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t677)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t677)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t678_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const MethodInfo* TextEditOp_t678_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextEditOp_t678_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TextEditOp_t678_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextEditOp_t678_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t678_0_0_0;
extern const Il2CppType TextEditOp_t678_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t678_DefinitionMetadata = 
{
	&TextEditor_t372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t678_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TextEditOp_t678_VTable/* vtableMethods */
	, TextEditOp_t678_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1173/* fieldStart */

};
TypeInfo TextEditOp_t678_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t678_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t678_0_0_0/* byval_arg */
	, &TextEditOp_t678_1_0_0/* this_arg */
	, &TextEditOp_t678_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t678)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t678)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
extern const MethodInfo TextEditor__ctor_m1755_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m1755/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern const MethodInfo TextEditor_ClearCursorPos_m3305_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m3305/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
extern const MethodInfo TextEditor_OnFocus_m1759_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m1759/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
extern const MethodInfo TextEditor_SelectAll_m3306_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m3306/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern const MethodInfo TextEditor_DeleteSelection_m3307_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m3307/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t372_TextEditor_ReplaceSelection_m3308_ParameterInfos[] = 
{
	{"replace", 0, 134219268, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern const MethodInfo TextEditor_ReplaceSelection_m3308_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m3308/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TextEditor_t372_TextEditor_ReplaceSelection_m3308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern const MethodInfo TextEditor_UpdateScrollOffset_m3309_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m3309/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
extern const MethodInfo TextEditor_Copy_m1760_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m1760/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t372_TextEditor_ReplaceNewlinesWithSpaces_m3310_ParameterInfos[] = 
{
	{"value", 0, 134219269, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern const MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m3310_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m3310/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t372_TextEditor_ReplaceNewlinesWithSpaces_m3310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
extern const MethodInfo TextEditor_Paste_m1756_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m1756/* method */
	, &TextEditor_t372_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextEditor_t372_MethodInfos[] =
{
	&TextEditor__ctor_m1755_MethodInfo,
	&TextEditor_ClearCursorPos_m3305_MethodInfo,
	&TextEditor_OnFocus_m1759_MethodInfo,
	&TextEditor_SelectAll_m3306_MethodInfo,
	&TextEditor_DeleteSelection_m3307_MethodInfo,
	&TextEditor_ReplaceSelection_m3308_MethodInfo,
	&TextEditor_UpdateScrollOffset_m3309_MethodInfo,
	&TextEditor_Copy_m1760_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m3310_MethodInfo,
	&TextEditor_Paste_m1756_MethodInfo,
	NULL
};
static const Il2CppType* TextEditor_t372_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t677_0_0_0,
	&TextEditOp_t678_0_0_0,
};
static const Il2CppMethodReference TextEditor_t372_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TextEditor_t372_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t372_1_0_0;
struct TextEditor_t372;
const Il2CppTypeDefinitionMetadata TextEditor_t372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t372_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t372_VTable/* vtableMethods */
	, TextEditor_t372_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1224/* fieldStart */

};
TypeInfo TextEditor_t372_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t372_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextEditor_t372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t372_0_0_0/* byval_arg */
	, &TextEditor_t372_1_0_0/* this_arg */
	, &TextEditor_t372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t372)/* instance_size */
	, sizeof (TextEditor_t372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t372_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t330_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern const Il2CppType Color_t163_0_0_0;
extern const Il2CppType Color_t163_0_0_0;
extern const Il2CppType Color_t163_0_0_0;
static const ParameterInfo TextGenerationSettings_t330_TextGenerationSettings_CompareColors_m3311_ParameterInfos[] = 
{
	{"left", 0, 134219270, 0, &Color_t163_0_0_0},
	{"right", 1, 134219271, 0, &Color_t163_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Color_t163_Color_t163 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo TextGenerationSettings_CompareColors_m3311_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m3311/* method */
	, &TextGenerationSettings_t330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Color_t163_Color_t163/* invoker_method */
	, TextGenerationSettings_t330_TextGenerationSettings_CompareColors_m3311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t53_0_0_0;
extern const Il2CppType Vector2_t53_0_0_0;
extern const Il2CppType Vector2_t53_0_0_0;
static const ParameterInfo TextGenerationSettings_t330_TextGenerationSettings_CompareVector2_m3312_ParameterInfos[] = 
{
	{"left", 0, 134219272, 0, &Vector2_t53_0_0_0},
	{"right", 1, 134219273, 0, &Vector2_t53_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo TextGenerationSettings_CompareVector2_m3312_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m3312/* method */
	, &TextGenerationSettings_t330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Vector2_t53_Vector2_t53/* invoker_method */
	, TextGenerationSettings_t330_TextGenerationSettings_CompareVector2_m3312_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerationSettings_t330_0_0_0;
extern const Il2CppType TextGenerationSettings_t330_0_0_0;
static const ParameterInfo TextGenerationSettings_t330_TextGenerationSettings_Equals_m3313_ParameterInfos[] = 
{
	{"other", 0, 134219274, 0, &TextGenerationSettings_t330_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_TextGenerationSettings_t330 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern const MethodInfo TextGenerationSettings_Equals_m3313_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m3313/* method */
	, &TextGenerationSettings_t330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_TextGenerationSettings_t330/* invoker_method */
	, TextGenerationSettings_t330_TextGenerationSettings_Equals_m3313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextGenerationSettings_t330_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m3311_MethodInfo,
	&TextGenerationSettings_CompareVector2_m3312_MethodInfo,
	&TextGenerationSettings_Equals_m3313_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextGenerationSettings_t330_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool TextGenerationSettings_t330_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t330_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, TextGenerationSettings_t330_VTable/* vtableMethods */
	, TextGenerationSettings_t330_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1248/* fieldStart */

};
TypeInfo TextGenerationSettings_t330_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t330_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextGenerationSettings_t330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t330_0_0_0/* byval_arg */
	, &TextGenerationSettings_t330_1_0_0/* this_arg */
	, &TextGenerationSettings_t330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t330)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t330)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t582_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TrackedReference_t582_TrackedReference_Equals_m3314_ParameterInfos[] = 
{
	{"o", 0, 134219275, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern const MethodInfo TrackedReference_Equals_m3314_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m3314/* method */
	, &TrackedReference_t582_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TrackedReference_t582_TrackedReference_Equals_m3314_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern const MethodInfo TrackedReference_GetHashCode_m3315_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m3315/* method */
	, &TrackedReference_t582_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TrackedReference_t582_0_0_0;
extern const Il2CppType TrackedReference_t582_0_0_0;
extern const Il2CppType TrackedReference_t582_0_0_0;
static const ParameterInfo TrackedReference_t582_TrackedReference_op_Equality_m3316_ParameterInfos[] = 
{
	{"x", 0, 134219276, 0, &TrackedReference_t582_0_0_0},
	{"y", 1, 134219277, 0, &TrackedReference_t582_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern const MethodInfo TrackedReference_op_Equality_m3316_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m3316/* method */
	, &TrackedReference_t582_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, TrackedReference_t582_TrackedReference_op_Equality_m3316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackedReference_t582_MethodInfos[] =
{
	&TrackedReference_Equals_m3314_MethodInfo,
	&TrackedReference_GetHashCode_m3315_MethodInfo,
	&TrackedReference_op_Equality_m3316_MethodInfo,
	NULL
};
extern const MethodInfo TrackedReference_Equals_m3314_MethodInfo;
extern const MethodInfo TrackedReference_GetHashCode_m3315_MethodInfo;
static const Il2CppMethodReference TrackedReference_t582_VTable[] =
{
	&TrackedReference_Equals_m3314_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&TrackedReference_GetHashCode_m3315_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TrackedReference_t582_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t582_1_0_0;
struct TrackedReference_t582;
const Il2CppTypeDefinitionMetadata TrackedReference_t582_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t582_VTable/* vtableMethods */
	, TrackedReference_t582_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1265/* fieldStart */

};
TypeInfo TrackedReference_t582_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackedReference_t582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t582_0_0_0/* byval_arg */
	, &TrackedReference_t582_1_0_0/* this_arg */
	, &TrackedReference_t582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t582_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t582_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t582_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t582)/* instance_size */
	, sizeof (TrackedReference_t582)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t582_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t680_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static const MethodInfo* PersistentListenerMode_t680_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PersistentListenerMode_t680_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool PersistentListenerMode_t680_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t680_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentListenerMode_t680_0_0_0;
extern const Il2CppType PersistentListenerMode_t680_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t680_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, PersistentListenerMode_t680_VTable/* vtableMethods */
	, PersistentListenerMode_t680_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1266/* fieldStart */

};
TypeInfo PersistentListenerMode_t680_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t680_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t680_0_0_0/* byval_arg */
	, &PersistentListenerMode_t680_1_0_0/* this_arg */
	, &PersistentListenerMode_t680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t680)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t680)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t681_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern const MethodInfo ArgumentCache__ctor_m3317_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m3317/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t33_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m3318_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m3318/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Object_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3319_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3319/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern const MethodInfo ArgumentCache_get_intArgument_m3320_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m3320/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern const MethodInfo ArgumentCache_get_floatArgument_m3321_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m3321/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Single_t85_0_0_0/* return_type */
	, RuntimeInvoker_Single_t85/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern const MethodInfo ArgumentCache_get_stringArgument_m3322_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m3322/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern const MethodInfo ArgumentCache_get_boolArgument_m3323_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m3323/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern const MethodInfo ArgumentCache_TidyAssemblyTypeName_m3324_MethodInfo = 
{
	"TidyAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_TidyAssemblyTypeName_m3324/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m3325_MethodInfo = 
{
	"OnBeforeSerialize"/* name */
	, (methodPointerType)&ArgumentCache_OnBeforeSerialize_m3325/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m3326_MethodInfo = 
{
	"OnAfterDeserialize"/* name */
	, (methodPointerType)&ArgumentCache_OnAfterDeserialize_m3326/* method */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgumentCache_t681_MethodInfos[] =
{
	&ArgumentCache__ctor_m3317_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m3318_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3319_MethodInfo,
	&ArgumentCache_get_intArgument_m3320_MethodInfo,
	&ArgumentCache_get_floatArgument_m3321_MethodInfo,
	&ArgumentCache_get_stringArgument_m3322_MethodInfo,
	&ArgumentCache_get_boolArgument_m3323_MethodInfo,
	&ArgumentCache_TidyAssemblyTypeName_m3324_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m3325_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m3326_MethodInfo,
	NULL
};
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m3318_MethodInfo;
static const PropertyInfo ArgumentCache_t681____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m3318_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3319_MethodInfo;
static const PropertyInfo ArgumentCache_t681____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3319_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_intArgument_m3320_MethodInfo;
static const PropertyInfo ArgumentCache_t681____intArgument_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m3320_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_floatArgument_m3321_MethodInfo;
static const PropertyInfo ArgumentCache_t681____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m3321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_stringArgument_m3322_MethodInfo;
static const PropertyInfo ArgumentCache_t681____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m3322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_boolArgument_m3323_MethodInfo;
static const PropertyInfo ArgumentCache_t681____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t681_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m3323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ArgumentCache_t681_PropertyInfos[] =
{
	&ArgumentCache_t681____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t681____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t681____intArgument_PropertyInfo,
	&ArgumentCache_t681____floatArgument_PropertyInfo,
	&ArgumentCache_t681____stringArgument_PropertyInfo,
	&ArgumentCache_t681____boolArgument_PropertyInfo,
	NULL
};
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m3325_MethodInfo;
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m3326_MethodInfo;
static const Il2CppMethodReference ArgumentCache_t681_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m3325_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m3326_MethodInfo,
};
static bool ArgumentCache_t681_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationCallbackReceiver_t425_0_0_0;
static const Il2CppType* ArgumentCache_t681_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t425_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t681_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t681_0_0_0;
extern const Il2CppType ArgumentCache_t681_1_0_0;
struct ArgumentCache_t681;
const Il2CppTypeDefinitionMetadata ArgumentCache_t681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t681_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t681_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t681_VTable/* vtableMethods */
	, ArgumentCache_t681_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1274/* fieldStart */

};
TypeInfo ArgumentCache_t681_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t681_MethodInfos/* methods */
	, ArgumentCache_t681_PropertyInfos/* properties */
	, NULL/* events */
	, &ArgumentCache_t681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t681_0_0_0/* byval_arg */
	, &ArgumentCache_t681_1_0_0/* this_arg */
	, &ArgumentCache_t681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t681)/* instance_size */
	, sizeof (ArgumentCache_t681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t682_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern const MethodInfo BaseInvokableCall__ctor_m3327_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m3327/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t682_BaseInvokableCall__ctor_m3328_ParameterInfos[] = 
{
	{"target", 0, 134219278, 0, &Object_t_0_0_0},
	{"function", 1, 134219279, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall__ctor_m3328_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m3328/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t682_BaseInvokableCall__ctor_m3328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo BaseInvokableCall_t682_BaseInvokableCall_Invoke_m3600_ParameterInfos[] = 
{
	{"args", 0, 134219280, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
extern const MethodInfo BaseInvokableCall_Invoke_m3600_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, BaseInvokableCall_t682_BaseInvokableCall_Invoke_m3600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t682_BaseInvokableCall_ThrowOnInvalidArg_m3601_ParameterInfos[] = 
{
	{"arg", 0, 134219281, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m3601_MethodInfo;
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m3601_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericParametersArray };
extern const Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m3601_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m3601_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m3601_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t682_BaseInvokableCall_ThrowOnInvalidArg_m3601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1570/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m3601_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m3601_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Delegate_t361_0_0_0;
extern const Il2CppType Delegate_t361_0_0_0;
static const ParameterInfo BaseInvokableCall_t682_BaseInvokableCall_AllowInvoke_m3329_ParameterInfos[] = 
{
	{"delegate", 0, 134219282, 0, &Delegate_t361_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern const MethodInfo BaseInvokableCall_AllowInvoke_m3329_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m3329/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, BaseInvokableCall_t682_BaseInvokableCall_AllowInvoke_m3329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t682_BaseInvokableCall_Find_m3602_ParameterInfos[] = 
{
	{"targetObj", 0, 134219283, 0, &Object_t_0_0_0},
	{"method", 1, 134219284, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall_Find_m3602_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t682_BaseInvokableCall_Find_m3602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseInvokableCall_t682_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m3327_MethodInfo,
	&BaseInvokableCall__ctor_m3328_MethodInfo,
	&BaseInvokableCall_Invoke_m3600_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m3601_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m3329_MethodInfo,
	&BaseInvokableCall_Find_m3602_MethodInfo,
	NULL
};
static const Il2CppMethodReference BaseInvokableCall_t682_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
};
static bool BaseInvokableCall_t682_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t682_0_0_0;
extern const Il2CppType BaseInvokableCall_t682_1_0_0;
struct BaseInvokableCall_t682;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t682_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t682_VTable/* vtableMethods */
	, BaseInvokableCall_t682_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BaseInvokableCall_t682_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t682_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseInvokableCall_t682_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t682_0_0_0/* byval_arg */
	, &BaseInvokableCall_t682_1_0_0/* this_arg */
	, &BaseInvokableCall_t682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t682)/* instance_size */
	, sizeof (BaseInvokableCall_t682)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t683_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t683_InvokableCall__ctor_m3330_ParameterInfos[] = 
{
	{"target", 0, 134219285, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219286, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall__ctor_m3330_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m3330/* method */
	, &InvokableCall_t683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, InvokableCall_t683_InvokableCall__ctor_m3330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCall_t683_InvokableCall_Invoke_m3331_ParameterInfos[] = 
{
	{"args", 0, 134219287, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern const MethodInfo InvokableCall_Invoke_m3331_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m3331/* method */
	, &InvokableCall_t683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvokableCall_t683_InvokableCall_Invoke_m3331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t683_InvokableCall_Find_m3332_ParameterInfos[] = 
{
	{"targetObj", 0, 134219288, 0, &Object_t_0_0_0},
	{"method", 1, 134219289, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_Find_m3332_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m3332/* method */
	, &InvokableCall_t683_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, InvokableCall_t683_InvokableCall_Find_m3332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_t683_MethodInfos[] =
{
	&InvokableCall__ctor_m3330_MethodInfo,
	&InvokableCall_Invoke_m3331_MethodInfo,
	&InvokableCall_Find_m3332_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_Invoke_m3331_MethodInfo;
extern const MethodInfo InvokableCall_Find_m3332_MethodInfo;
static const Il2CppMethodReference InvokableCall_t683_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InvokableCall_Invoke_m3331_MethodInfo,
	&InvokableCall_Find_m3332_MethodInfo,
};
static bool InvokableCall_t683_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_t683_0_0_0;
extern const Il2CppType InvokableCall_t683_1_0_0;
struct InvokableCall_t683;
const Il2CppTypeDefinitionMetadata InvokableCall_t683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t682_0_0_0/* parent */
	, InvokableCall_t683_VTable/* vtableMethods */
	, InvokableCall_t683_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1280/* fieldStart */

};
TypeInfo InvokableCall_t683_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t683_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_t683_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t683_0_0_0/* byval_arg */
	, &InvokableCall_t683_1_0_0/* this_arg */
	, &InvokableCall_t683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t683)/* instance_size */
	, sizeof (InvokableCall_t683)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t808_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_1_t808_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t808_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_1_t808_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_1_t808_Il2CppGenericContainer, NULL, "T1", 0, 0 };
static const Il2CppGenericParameter* InvokableCall_1_t808_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t808_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_1_t808_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_1_t808_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t808_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t808_InvokableCall_1__ctor_m3603_ParameterInfos[] = 
{
	{"target", 0, 134219290, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219291, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1__ctor_m3603_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t808_InvokableCall_1__ctor_m3603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t885_0_0_0;
extern const Il2CppType UnityAction_1_t885_0_0_0;
static const ParameterInfo InvokableCall_1_t808_InvokableCall_1__ctor_m3604_ParameterInfos[] = 
{
	{"callback", 0, 134219292, 0, &UnityAction_1_t885_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern const MethodInfo InvokableCall_1__ctor_m3604_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t808_InvokableCall_1__ctor_m3604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCall_1_t808_InvokableCall_1_Invoke_m3605_ParameterInfos[] = 
{
	{"args", 0, 134219293, 0, &ObjectU5BU5D_t29_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
extern const MethodInfo InvokableCall_1_Invoke_m3605_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t808_InvokableCall_1_Invoke_m3605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t808_InvokableCall_1_Find_m3606_ParameterInfos[] = 
{
	{"targetObj", 0, 134219294, 0, &Object_t_0_0_0},
	{"method", 1, 134219295, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1_Find_m3606_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t808_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t808_InvokableCall_1_Find_m3606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_1_t808_MethodInfos[] =
{
	&InvokableCall_1__ctor_m3603_MethodInfo,
	&InvokableCall_1__ctor_m3604_MethodInfo,
	&InvokableCall_1_Invoke_m3605_MethodInfo,
	&InvokableCall_1_Find_m3606_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_1_Invoke_m3605_MethodInfo;
extern const MethodInfo InvokableCall_1_Find_m3606_MethodInfo;
static const Il2CppMethodReference InvokableCall_1_t808_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InvokableCall_1_Invoke_m3605_MethodInfo,
	&InvokableCall_1_Find_m3606_MethodInfo,
};
static bool InvokableCall_1_t808_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t884_m3706_GenericMethod;
extern const Il2CppType InvokableCall_1_t808_gp_0_0_0_0;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m3707_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_1_t808_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_1_t885_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_1_t885_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t884_m3706_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t808_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m3707_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_1_t808_0_0_0;
extern const Il2CppType InvokableCall_1_t808_1_0_0;
struct InvokableCall_1_t808;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t808_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t682_0_0_0/* parent */
	, InvokableCall_1_t808_VTable/* vtableMethods */
	, InvokableCall_1_t808_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_1_t808_RGCTXData/* rgctxDefinition */
	, 1281/* fieldStart */

};
TypeInfo InvokableCall_1_t808_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t808_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_1_t808_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t808_0_0_0/* byval_arg */
	, &InvokableCall_1_t808_1_0_0/* this_arg */
	, &InvokableCall_1_t808_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_1_t808_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t809_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_2_t809_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t809_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t809_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t809_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_2_t809_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t809_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t809_Il2CppGenericContainer, NULL, "T2", 1, 0 };
static const Il2CppGenericParameter* InvokableCall_2_t809_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t809_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t809_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_2_t809_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_2_t809_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t809_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t809_InvokableCall_2__ctor_m3607_ParameterInfos[] = 
{
	{"target", 0, 134219296, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219297, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2__ctor_m3607_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t809_InvokableCall_2__ctor_m3607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCall_2_t809_InvokableCall_2_Invoke_m3608_ParameterInfos[] = 
{
	{"args", 0, 134219298, 0, &ObjectU5BU5D_t29_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
extern const MethodInfo InvokableCall_2_Invoke_m3608_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t809_InvokableCall_2_Invoke_m3608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t809_InvokableCall_2_Find_m3609_ParameterInfos[] = 
{
	{"targetObj", 0, 134219299, 0, &Object_t_0_0_0},
	{"method", 1, 134219300, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2_Find_m3609_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t809_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t809_InvokableCall_2_Find_m3609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_2_t809_MethodInfos[] =
{
	&InvokableCall_2__ctor_m3607_MethodInfo,
	&InvokableCall_2_Invoke_m3608_MethodInfo,
	&InvokableCall_2_Find_m3609_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_2_Invoke_m3608_MethodInfo;
extern const MethodInfo InvokableCall_2_Find_m3609_MethodInfo;
static const Il2CppMethodReference InvokableCall_2_t809_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InvokableCall_2_Invoke_m3608_MethodInfo,
	&InvokableCall_2_Find_m3609_MethodInfo,
};
static bool InvokableCall_2_t809_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_2_t888_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t886_m3708_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t887_m3709_GenericMethod;
extern const Il2CppType InvokableCall_2_t809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t809_gp_1_0_0_0;
extern const Il2CppGenericMethod UnityAction_2_Invoke_m3710_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_2_t809_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_2_t888_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_2_t888_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t886_m3708_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t887_m3709_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t809_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t809_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_2_Invoke_m3710_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_2_t809_0_0_0;
extern const Il2CppType InvokableCall_2_t809_1_0_0;
struct InvokableCall_2_t809;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t809_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t682_0_0_0/* parent */
	, InvokableCall_2_t809_VTable/* vtableMethods */
	, InvokableCall_2_t809_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_2_t809_RGCTXData/* rgctxDefinition */
	, 1282/* fieldStart */

};
TypeInfo InvokableCall_2_t809_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t809_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_2_t809_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t809_0_0_0/* byval_arg */
	, &InvokableCall_2_t809_1_0_0/* this_arg */
	, &InvokableCall_2_t809_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_2_t809_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t810_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_3_t810_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t810_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t810_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t810_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_3_t810_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t810_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t810_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_3_t810_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t810_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t810_Il2CppGenericContainer, NULL, "T3", 2, 0 };
static const Il2CppGenericParameter* InvokableCall_3_t810_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t810_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t810_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t810_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_3_t810_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_3_t810_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t810_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t810_InvokableCall_3__ctor_m3610_ParameterInfos[] = 
{
	{"target", 0, 134219301, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219302, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3__ctor_m3610_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t810_InvokableCall_3__ctor_m3610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCall_3_t810_InvokableCall_3_Invoke_m3611_ParameterInfos[] = 
{
	{"args", 0, 134219303, 0, &ObjectU5BU5D_t29_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
extern const MethodInfo InvokableCall_3_Invoke_m3611_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t810_InvokableCall_3_Invoke_m3611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t810_InvokableCall_3_Find_m3612_ParameterInfos[] = 
{
	{"targetObj", 0, 134219304, 0, &Object_t_0_0_0},
	{"method", 1, 134219305, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3_Find_m3612_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t810_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t810_InvokableCall_3_Find_m3612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_3_t810_MethodInfos[] =
{
	&InvokableCall_3__ctor_m3610_MethodInfo,
	&InvokableCall_3_Invoke_m3611_MethodInfo,
	&InvokableCall_3_Find_m3612_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_3_Invoke_m3611_MethodInfo;
extern const MethodInfo InvokableCall_3_Find_m3612_MethodInfo;
static const Il2CppMethodReference InvokableCall_3_t810_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InvokableCall_3_Invoke_m3611_MethodInfo,
	&InvokableCall_3_Find_m3612_MethodInfo,
};
static bool InvokableCall_3_t810_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_3_t892_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t889_m3711_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t890_m3712_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t891_m3713_GenericMethod;
extern const Il2CppType InvokableCall_3_t810_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t810_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t810_gp_2_0_0_0;
extern const Il2CppGenericMethod UnityAction_3_Invoke_m3714_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_3_t810_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_3_t892_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_3_t892_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t889_m3711_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t890_m3712_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t891_m3713_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t810_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t810_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t810_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_3_Invoke_m3714_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_3_t810_0_0_0;
extern const Il2CppType InvokableCall_3_t810_1_0_0;
struct InvokableCall_3_t810;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t810_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t682_0_0_0/* parent */
	, InvokableCall_3_t810_VTable/* vtableMethods */
	, InvokableCall_3_t810_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_3_t810_RGCTXData/* rgctxDefinition */
	, 1283/* fieldStart */

};
TypeInfo InvokableCall_3_t810_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t810_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_3_t810_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t810_0_0_0/* byval_arg */
	, &InvokableCall_3_t810_1_0_0/* this_arg */
	, &InvokableCall_3_t810_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_3_t810_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t811_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_4_t811_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t811_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t811_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t811_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_4_t811_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t811_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t811_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_4_t811_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t811_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t811_Il2CppGenericContainer, NULL, "T3", 2, 0 };
extern TypeInfo InvokableCall_4_t811_gp_T4_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t811_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t811_Il2CppGenericContainer, NULL, "T4", 3, 0 };
static const Il2CppGenericParameter* InvokableCall_4_t811_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t811_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t811_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t811_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t811_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_4_t811_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_4_t811_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t811_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t811_InvokableCall_4__ctor_m3613_ParameterInfos[] = 
{
	{"target", 0, 134219306, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219307, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4__ctor_m3613_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t811_InvokableCall_4__ctor_m3613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCall_4_t811_InvokableCall_4_Invoke_m3614_ParameterInfos[] = 
{
	{"args", 0, 134219308, 0, &ObjectU5BU5D_t29_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
extern const MethodInfo InvokableCall_4_Invoke_m3614_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t811_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t811_InvokableCall_4_Invoke_m3614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t811_InvokableCall_4_Find_m3615_ParameterInfos[] = 
{
	{"targetObj", 0, 134219309, 0, &Object_t_0_0_0},
	{"method", 1, 134219310, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4_Find_m3615_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t811_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t811_InvokableCall_4_Find_m3615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_4_t811_MethodInfos[] =
{
	&InvokableCall_4__ctor_m3613_MethodInfo,
	&InvokableCall_4_Invoke_m3614_MethodInfo,
	&InvokableCall_4_Find_m3615_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_4_Invoke_m3614_MethodInfo;
extern const MethodInfo InvokableCall_4_Find_m3615_MethodInfo;
static const Il2CppMethodReference InvokableCall_4_t811_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InvokableCall_4_Invoke_m3614_MethodInfo,
	&InvokableCall_4_Find_m3615_MethodInfo,
};
static bool InvokableCall_4_t811_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_4_t897_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t893_m3715_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t894_m3716_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t895_m3717_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT4_t896_m3718_GenericMethod;
extern const Il2CppType InvokableCall_4_t811_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t811_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t811_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t811_gp_3_0_0_0;
extern const Il2CppGenericMethod UnityAction_4_Invoke_m3719_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_4_t811_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_4_t897_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_4_t897_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t893_m3715_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t894_m3716_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t895_m3717_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT4_t896_m3718_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t811_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t811_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t811_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t811_gp_3_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_4_Invoke_m3719_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_4_t811_0_0_0;
extern const Il2CppType InvokableCall_4_t811_1_0_0;
struct InvokableCall_4_t811;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t811_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t682_0_0_0/* parent */
	, InvokableCall_4_t811_VTable/* vtableMethods */
	, InvokableCall_4_t811_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_4_t811_RGCTXData/* rgctxDefinition */
	, 1284/* fieldStart */

};
TypeInfo InvokableCall_4_t811_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t811_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_4_t811_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t811_0_0_0/* byval_arg */
	, &InvokableCall_4_t811_1_0_0/* this_arg */
	, &InvokableCall_4_t811_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_4_t811_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t788_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CachedInvokableCall_1_t788_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t788_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CachedInvokableCall_1_t788_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CachedInvokableCall_1_t788_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CachedInvokableCall_1_t788_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t788_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CachedInvokableCall_1_t788_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CachedInvokableCall_1_t788_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t788_Il2CppGenericParametersArray };
extern const Il2CppType Object_t33_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t788_gp_0_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t788_gp_0_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t788_CachedInvokableCall_1__ctor_m3616_ParameterInfos[] = 
{
	{"target", 0, 134219311, 0, &Object_t33_0_0_0},
	{"theFunction", 1, 134219312, 0, &MethodInfo_t_0_0_0},
	{"argument", 2, 134219313, 0, &CachedInvokableCall_1_t788_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern const MethodInfo CachedInvokableCall_1__ctor_m3616_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t788_CachedInvokableCall_1__ctor_m3616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t788_CachedInvokableCall_1_Invoke_m3617_ParameterInfos[] = 
{
	{"args", 0, 134219314, 0, &ObjectU5BU5D_t29_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
extern const MethodInfo CachedInvokableCall_1_Invoke_m3617_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t788_CachedInvokableCall_1_Invoke_m3617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CachedInvokableCall_1_t788_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m3616_MethodInfo,
	&CachedInvokableCall_1_Invoke_m3617_MethodInfo,
	NULL
};
extern const MethodInfo CachedInvokableCall_1_Invoke_m3617_MethodInfo;
extern const Il2CppGenericMethod InvokableCall_1_Find_m3720_GenericMethod;
static const Il2CppMethodReference CachedInvokableCall_1_t788_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CachedInvokableCall_1_Invoke_m3617_MethodInfo,
	&InvokableCall_1_Find_m3720_GenericMethod,
};
static bool CachedInvokableCall_1_t788_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern const Il2CppGenericMethod InvokableCall_1__ctor_m3721_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1_Invoke_m3722_GenericMethod;
static Il2CppRGCTXDefinition CachedInvokableCall_1_t788_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m3721_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&CachedInvokableCall_1_t788_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1_Invoke_m3722_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CachedInvokableCall_1_t788_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t788_1_0_0;
extern const Il2CppType InvokableCall_1_t899_0_0_0;
struct CachedInvokableCall_1_t788;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t899_0_0_0/* parent */
	, CachedInvokableCall_1_t788_VTable/* vtableMethods */
	, CachedInvokableCall_1_t788_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, CachedInvokableCall_1_t788_RGCTXData/* rgctxDefinition */
	, 1285/* fieldStart */

};
TypeInfo CachedInvokableCall_1_t788_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t788_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CachedInvokableCall_1_t788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t788_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t788_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t788_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t684_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const MethodInfo* UnityEventCallState_t684_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityEventCallState_t684_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UnityEventCallState_t684_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t684_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t684_0_0_0;
extern const Il2CppType UnityEventCallState_t684_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t684_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t684_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UnityEventCallState_t684_VTable/* vtableMethods */
	, UnityEventCallState_t684_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1286/* fieldStart */

};
TypeInfo UnityEventCallState_t684_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t684_0_0_0/* byval_arg */
	, &UnityEventCallState_t684_1_0_0/* this_arg */
	, &UnityEventCallState_t684_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t684)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t684)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t685_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern const MethodInfo PersistentCall__ctor_m3333_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m3333/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern const MethodInfo PersistentCall_get_target_m3334_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m3334/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &Object_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern const MethodInfo PersistentCall_get_methodName_m3335_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m3335/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PersistentListenerMode_t680 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern const MethodInfo PersistentCall_get_mode_m3336_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m3336/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t680_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t680/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern const MethodInfo PersistentCall_get_arguments_m3337_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m3337/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t681_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern const MethodInfo PersistentCall_IsValid_m3338_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m3338/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEventBase_t690_0_0_0;
extern const Il2CppType UnityEventBase_t690_0_0_0;
static const ParameterInfo PersistentCall_t685_PersistentCall_GetRuntimeCall_m3339_ParameterInfos[] = 
{
	{"theEvent", 0, 134219315, 0, &UnityEventBase_t690_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCall_GetRuntimeCall_m3339_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m3339/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t685_PersistentCall_GetRuntimeCall_m3339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t33_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType ArgumentCache_t681_0_0_0;
static const ParameterInfo PersistentCall_t685_PersistentCall_GetObjectCall_m3340_ParameterInfos[] = 
{
	{"target", 0, 134219316, 0, &Object_t33_0_0_0},
	{"method", 1, 134219317, 0, &MethodInfo_t_0_0_0},
	{"arguments", 2, 134219318, 0, &ArgumentCache_t681_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const MethodInfo PersistentCall_GetObjectCall_m3340_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m3340/* method */
	, &PersistentCall_t685_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t685_PersistentCall_GetObjectCall_m3340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCall_t685_MethodInfos[] =
{
	&PersistentCall__ctor_m3333_MethodInfo,
	&PersistentCall_get_target_m3334_MethodInfo,
	&PersistentCall_get_methodName_m3335_MethodInfo,
	&PersistentCall_get_mode_m3336_MethodInfo,
	&PersistentCall_get_arguments_m3337_MethodInfo,
	&PersistentCall_IsValid_m3338_MethodInfo,
	&PersistentCall_GetRuntimeCall_m3339_MethodInfo,
	&PersistentCall_GetObjectCall_m3340_MethodInfo,
	NULL
};
extern const MethodInfo PersistentCall_get_target_m3334_MethodInfo;
static const PropertyInfo PersistentCall_t685____target_PropertyInfo = 
{
	&PersistentCall_t685_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m3334_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_methodName_m3335_MethodInfo;
static const PropertyInfo PersistentCall_t685____methodName_PropertyInfo = 
{
	&PersistentCall_t685_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m3335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_mode_m3336_MethodInfo;
static const PropertyInfo PersistentCall_t685____mode_PropertyInfo = 
{
	&PersistentCall_t685_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m3336_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_arguments_m3337_MethodInfo;
static const PropertyInfo PersistentCall_t685____arguments_PropertyInfo = 
{
	&PersistentCall_t685_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m3337_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PersistentCall_t685_PropertyInfos[] =
{
	&PersistentCall_t685____target_PropertyInfo,
	&PersistentCall_t685____methodName_PropertyInfo,
	&PersistentCall_t685____mode_PropertyInfo,
	&PersistentCall_t685____arguments_PropertyInfo,
	NULL
};
static const Il2CppMethodReference PersistentCall_t685_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool PersistentCall_t685_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t685_0_0_0;
extern const Il2CppType PersistentCall_t685_1_0_0;
struct PersistentCall_t685;
const Il2CppTypeDefinitionMetadata PersistentCall_t685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t685_VTable/* vtableMethods */
	, PersistentCall_t685_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1290/* fieldStart */

};
TypeInfo PersistentCall_t685_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t685_MethodInfos/* methods */
	, PersistentCall_t685_PropertyInfos/* properties */
	, NULL/* events */
	, &PersistentCall_t685_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t685_0_0_0/* byval_arg */
	, &PersistentCall_t685_1_0_0/* this_arg */
	, &PersistentCall_t685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t685)/* instance_size */
	, sizeof (PersistentCall_t685)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t687_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern const MethodInfo PersistentCallGroup__ctor_m3341_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m3341/* method */
	, &PersistentCallGroup_t687_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InvokableCallList_t689_0_0_0;
extern const Il2CppType InvokableCallList_t689_0_0_0;
extern const Il2CppType UnityEventBase_t690_0_0_0;
static const ParameterInfo PersistentCallGroup_t687_PersistentCallGroup_Initialize_m3342_ParameterInfos[] = 
{
	{"invokableList", 0, 134219319, 0, &InvokableCallList_t689_0_0_0},
	{"unityEventBase", 1, 134219320, 0, &UnityEventBase_t690_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCallGroup_Initialize_m3342_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m3342/* method */
	, &PersistentCallGroup_t687_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t687_PersistentCallGroup_Initialize_m3342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCallGroup_t687_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m3341_MethodInfo,
	&PersistentCallGroup_Initialize_m3342_MethodInfo,
	NULL
};
static const Il2CppMethodReference PersistentCallGroup_t687_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool PersistentCallGroup_t687_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t687_0_0_0;
extern const Il2CppType PersistentCallGroup_t687_1_0_0;
struct PersistentCallGroup_t687;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t687_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t687_VTable/* vtableMethods */
	, PersistentCallGroup_t687_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1295/* fieldStart */

};
TypeInfo PersistentCallGroup_t687_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PersistentCallGroup_t687_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t687_0_0_0/* byval_arg */
	, &PersistentCallGroup_t687_1_0_0/* this_arg */
	, &PersistentCallGroup_t687_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t687)/* instance_size */
	, sizeof (PersistentCallGroup_t687)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t689_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern const MethodInfo InvokableCallList__ctor_m3343_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m3343/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t682_0_0_0;
static const ParameterInfo InvokableCallList_t689_InvokableCallList_AddPersistentInvokableCall_m3344_ParameterInfos[] = 
{
	{"call", 0, 134219321, 0, &BaseInvokableCall_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddPersistentInvokableCall_m3344_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m3344/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvokableCallList_t689_InvokableCallList_AddPersistentInvokableCall_m3344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t682_0_0_0;
static const ParameterInfo InvokableCallList_t689_InvokableCallList_AddListener_m3345_ParameterInfos[] = 
{
	{"call", 0, 134219322, 0, &BaseInvokableCall_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddListener_m3345_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m3345/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvokableCallList_t689_InvokableCallList_AddListener_m3345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCallList_t689_InvokableCallList_RemoveListener_m3346_ParameterInfos[] = 
{
	{"targetObj", 0, 134219323, 0, &Object_t_0_0_0},
	{"method", 1, 134219324, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCallList_RemoveListener_m3346_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m3346/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t689_InvokableCallList_RemoveListener_m3346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo InvokableCallList_ClearPersistent_m3347_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m3347/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo InvokableCallList_t689_InvokableCallList_Invoke_m3348_ParameterInfos[] = 
{
	{"parameters", 0, 134219325, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo InvokableCallList_Invoke_m3348_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m3348/* method */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InvokableCallList_t689_InvokableCallList_Invoke_m3348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCallList_t689_MethodInfos[] =
{
	&InvokableCallList__ctor_m3343_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m3344_MethodInfo,
	&InvokableCallList_AddListener_m3345_MethodInfo,
	&InvokableCallList_RemoveListener_m3346_MethodInfo,
	&InvokableCallList_ClearPersistent_m3347_MethodInfo,
	&InvokableCallList_Invoke_m3348_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvokableCallList_t689_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool InvokableCallList_t689_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t689_1_0_0;
struct InvokableCallList_t689;
const Il2CppTypeDefinitionMetadata InvokableCallList_t689_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t689_VTable/* vtableMethods */
	, InvokableCallList_t689_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1296/* fieldStart */

};
TypeInfo InvokableCallList_t689_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t689_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCallList_t689_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t689_0_0_0/* byval_arg */
	, &InvokableCallList_t689_1_0_0/* this_arg */
	, &InvokableCallList_t689_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t689)/* instance_size */
	, sizeof (InvokableCallList_t689)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t690_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern const MethodInfo UnityEventBase__ctor_m3349_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m3349/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_FindMethod_Impl_m3618_ParameterInfos[] = 
{
	{"name", 0, 134219326, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219327, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEventBase_FindMethod_Impl_m3618_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_FindMethod_Impl_m3618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_GetDelegate_m3619_ParameterInfos[] = 
{
	{"target", 0, 134219328, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219329, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_GetDelegate_m3619_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_GetDelegate_m3619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PersistentCall_t685_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_FindMethod_m3350_ParameterInfos[] = 
{
	{"call", 0, 134219330, 0, &PersistentCall_t685_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const MethodInfo UnityEventBase_FindMethod_m3350_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m3350/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_FindMethod_m3350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType PersistentListenerMode_t680_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_FindMethod_m3351_ParameterInfos[] = 
{
	{"name", 0, 134219331, 0, &String_t_0_0_0},
	{"listener", 1, 134219332, 0, &Object_t_0_0_0},
	{"mode", 2, 134219333, 0, &PersistentListenerMode_t680_0_0_0},
	{"argumentType", 3, 134219334, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const MethodInfo UnityEventBase_FindMethod_m3351_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m3351/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_FindMethod_m3351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern const MethodInfo UnityEventBase_DirtyPersistentCalls_m3352_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m3352/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern const MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m3353_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m3353/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t682_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_AddCall_m3354_ParameterInfos[] = 
{
	{"call", 0, 134219335, 0, &BaseInvokableCall_t682_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo UnityEventBase_AddCall_m3354_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m3354/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_AddCall_m3354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_RemoveListener_m3355_ParameterInfos[] = 
{
	{"targetObj", 0, 134219336, 0, &Object_t_0_0_0},
	{"method", 1, 134219337, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_RemoveListener_m3355_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m3355/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_RemoveListener_m3355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_Invoke_m3356_ParameterInfos[] = 
{
	{"parameters", 0, 134219338, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern const MethodInfo UnityEventBase_Invoke_m3356_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m3356/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_Invoke_m3356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern const MethodInfo UnityEventBase_ToString_m2098_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m2098/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo UnityEventBase_t690_UnityEventBase_GetValidMethodInfo_m3357_ParameterInfos[] = 
{
	{"obj", 0, 134219339, 0, &Object_t_0_0_0},
	{"functionName", 1, 134219340, 0, &String_t_0_0_0},
	{"argumentTypes", 2, 134219341, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const MethodInfo UnityEventBase_GetValidMethodInfo_m3357_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m3357/* method */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t690_UnityEventBase_GetValidMethodInfo_m3357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEventBase_t690_MethodInfos[] =
{
	&UnityEventBase__ctor_m3349_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m3618_MethodInfo,
	&UnityEventBase_GetDelegate_m3619_MethodInfo,
	&UnityEventBase_FindMethod_m3350_MethodInfo,
	&UnityEventBase_FindMethod_m3351_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m3352_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m3353_MethodInfo,
	&UnityEventBase_AddCall_m3354_MethodInfo,
	&UnityEventBase_RemoveListener_m3355_MethodInfo,
	&UnityEventBase_Invoke_m3356_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m3357_MethodInfo,
	NULL
};
extern const MethodInfo UnityEventBase_ToString_m2098_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo;
static const Il2CppMethodReference UnityEventBase_t690_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	NULL,
	NULL,
};
static bool UnityEventBase_t690_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UnityEventBase_t690_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t425_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t690_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t690_1_0_0;
struct UnityEventBase_t690;
const Il2CppTypeDefinitionMetadata UnityEventBase_t690_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t690_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t690_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t690_VTable/* vtableMethods */
	, UnityEventBase_t690_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1299/* fieldStart */

};
TypeInfo UnityEventBase_t690_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t690_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEventBase_t690_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t690_0_0_0/* byval_arg */
	, &UnityEventBase_t690_1_0_0/* this_arg */
	, &UnityEventBase_t690_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t690)/* instance_size */
	, sizeof (UnityEventBase_t690)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t166_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern const MethodInfo UnityEvent__ctor_m1590_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m1590/* method */
	, &UnityEvent_t166_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_t166_UnityEvent_FindMethod_Impl_m2123_ParameterInfos[] = 
{
	{"name", 0, 134219342, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219343, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_FindMethod_Impl_m2123_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m2123/* method */
	, &UnityEvent_t166_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t166_UnityEvent_FindMethod_Impl_m2123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_t166_UnityEvent_GetDelegate_m2124_ParameterInfos[] = 
{
	{"target", 0, 134219344, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219345, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_GetDelegate_m2124_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m2124/* method */
	, &UnityEvent_t166_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t166_UnityEvent_GetDelegate_m2124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern const MethodInfo UnityEvent_Invoke_m1592_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m1592/* method */
	, &UnityEvent_t166_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_t166_MethodInfos[] =
{
	&UnityEvent__ctor_m1590_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2123_MethodInfo,
	&UnityEvent_GetDelegate_m2124_MethodInfo,
	&UnityEvent_Invoke_m1592_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_FindMethod_Impl_m2123_MethodInfo;
extern const MethodInfo UnityEvent_GetDelegate_m2124_MethodInfo;
static const Il2CppMethodReference UnityEvent_t166_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2123_MethodInfo,
	&UnityEvent_GetDelegate_m2124_MethodInfo,
};
static bool UnityEvent_t166_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_t166_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t166_0_0_0;
extern const Il2CppType UnityEvent_t166_1_0_0;
struct UnityEvent_t166;
const Il2CppTypeDefinitionMetadata UnityEvent_t166_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t166_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t690_0_0_0/* parent */
	, UnityEvent_t166_VTable/* vtableMethods */
	, UnityEvent_t166_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1303/* fieldStart */

};
TypeInfo UnityEvent_t166_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t166_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_t166_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t166_0_0_0/* byval_arg */
	, &UnityEvent_t166_1_0_0/* this_arg */
	, &UnityEvent_t166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t166)/* instance_size */
	, sizeof (UnityEvent_t166)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t812_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_1_t812_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t812_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_1_t812_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_1_t812_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityEvent_1_t812_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t812_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_1_t812_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_1_t812_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t812_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
extern const MethodInfo UnityEvent_1__ctor_m3620_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t901_0_0_0;
extern const Il2CppType UnityAction_1_t901_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_AddListener_m3621_ParameterInfos[] = 
{
	{"call", 0, 134219346, 0, &UnityAction_1_t901_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_AddListener_m3621_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_AddListener_m3621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t901_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_RemoveListener_m3622_ParameterInfos[] = 
{
	{"call", 0, 134219347, 0, &UnityAction_1_t901_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_RemoveListener_m3622_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_RemoveListener_m3622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_FindMethod_Impl_m3623_ParameterInfos[] = 
{
	{"name", 0, 134219348, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219349, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m3623_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_FindMethod_Impl_m3623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_GetDelegate_m3624_ParameterInfos[] = 
{
	{"target", 0, 134219350, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219351, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_1_GetDelegate_m3624_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_GetDelegate_m3624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t901_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_GetDelegate_m3625_ParameterInfos[] = 
{
	{"action", 0, 134219352, 0, &UnityAction_1_t901_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_GetDelegate_m3625_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_GetDelegate_m3625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEvent_1_t812_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t812_gp_0_0_0_0;
static const ParameterInfo UnityEvent_1_t812_UnityEvent_1_Invoke_m3626_ParameterInfos[] = 
{
	{"arg0", 0, 134219353, 0, &UnityEvent_1_t812_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
extern const MethodInfo UnityEvent_1_Invoke_m3626_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t812_UnityEvent_1_Invoke_m3626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_1_t812_MethodInfos[] =
{
	&UnityEvent_1__ctor_m3620_MethodInfo,
	&UnityEvent_1_AddListener_m3621_MethodInfo,
	&UnityEvent_1_RemoveListener_m3622_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m3623_MethodInfo,
	&UnityEvent_1_GetDelegate_m3624_MethodInfo,
	&UnityEvent_1_GetDelegate_m3625_MethodInfo,
	&UnityEvent_1_Invoke_m3626_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m3623_MethodInfo;
extern const MethodInfo UnityEvent_1_GetDelegate_m3624_MethodInfo;
static const Il2CppMethodReference UnityEvent_1_t812_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m3623_MethodInfo,
	&UnityEvent_1_GetDelegate_m3624_MethodInfo,
};
static bool UnityEvent_1_t812_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t812_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m3723_GenericMethod;
extern const Il2CppType InvokableCall_1_t902_0_0_0;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m3724_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m3725_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_1_t812_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityEvent_1_GetDelegate_m3723_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_1_t812_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t902_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m3724_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m3725_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityEvent_1_t812_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t812_0_0_0;
extern const Il2CppType UnityEvent_1_t812_1_0_0;
struct UnityEvent_1_t812;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t812_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t812_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t690_0_0_0/* parent */
	, UnityEvent_1_t812_VTable/* vtableMethods */
	, UnityEvent_1_t812_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_1_t812_RGCTXData/* rgctxDefinition */
	, 1304/* fieldStart */

};
TypeInfo UnityEvent_1_t812_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t812_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_1_t812_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t812_0_0_0/* byval_arg */
	, &UnityEvent_1_t812_1_0_0/* this_arg */
	, &UnityEvent_1_t812_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_1_t812_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t813_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_2_t813_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t813_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t813_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t813_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_2_t813_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t813_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t813_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityEvent_2_t813_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t813_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t813_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_2_t813_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_2_t813_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t813_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
extern const MethodInfo UnityEvent_2__ctor_m3627_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_2_t813_UnityEvent_2_FindMethod_Impl_m3628_ParameterInfos[] = 
{
	{"name", 0, 134219354, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219355, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m3628_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t813_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t813_UnityEvent_2_FindMethod_Impl_m3628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_2_t813_UnityEvent_2_GetDelegate_m3629_ParameterInfos[] = 
{
	{"target", 0, 134219356, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219357, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_2_GetDelegate_m3629_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t813_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t813_UnityEvent_2_GetDelegate_m3629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_2_t813_MethodInfos[] =
{
	&UnityEvent_2__ctor_m3627_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m3628_MethodInfo,
	&UnityEvent_2_GetDelegate_m3629_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m3628_MethodInfo;
extern const MethodInfo UnityEvent_2_GetDelegate_m3629_MethodInfo;
static const Il2CppMethodReference UnityEvent_2_t813_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m3628_MethodInfo,
	&UnityEvent_2_GetDelegate_m3629_MethodInfo,
};
static bool UnityEvent_2_t813_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t813_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern const Il2CppType UnityEvent_2_t813_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t813_gp_1_0_0_0;
extern const Il2CppType InvokableCall_2_t905_0_0_0;
extern const Il2CppGenericMethod InvokableCall_2__ctor_m3726_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_2_t813_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t813_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t813_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t905_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_2__ctor_m3726_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t813_0_0_0;
extern const Il2CppType UnityEvent_2_t813_1_0_0;
struct UnityEvent_2_t813;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t813_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t813_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t690_0_0_0/* parent */
	, UnityEvent_2_t813_VTable/* vtableMethods */
	, UnityEvent_2_t813_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_2_t813_RGCTXData/* rgctxDefinition */
	, 1305/* fieldStart */

};
TypeInfo UnityEvent_2_t813_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t813_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_2_t813_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t813_0_0_0/* byval_arg */
	, &UnityEvent_2_t813_1_0_0/* this_arg */
	, &UnityEvent_2_t813_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_2_t813_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t814_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_3_t814_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t814_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t814_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t814_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_3_t814_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t814_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t814_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_3_t814_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t814_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t814_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityEvent_3_t814_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t814_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t814_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t814_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_3_t814_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_3_t814_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t814_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
extern const MethodInfo UnityEvent_3__ctor_m3630_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_3_t814_UnityEvent_3_FindMethod_Impl_m3631_ParameterInfos[] = 
{
	{"name", 0, 134219358, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219359, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m3631_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t814_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t814_UnityEvent_3_FindMethod_Impl_m3631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_3_t814_UnityEvent_3_GetDelegate_m3632_ParameterInfos[] = 
{
	{"target", 0, 134219360, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219361, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_3_GetDelegate_m3632_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t814_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t814_UnityEvent_3_GetDelegate_m3632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_3_t814_MethodInfos[] =
{
	&UnityEvent_3__ctor_m3630_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m3631_MethodInfo,
	&UnityEvent_3_GetDelegate_m3632_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m3631_MethodInfo;
extern const MethodInfo UnityEvent_3_GetDelegate_m3632_MethodInfo;
static const Il2CppMethodReference UnityEvent_3_t814_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m3631_MethodInfo,
	&UnityEvent_3_GetDelegate_m3632_MethodInfo,
};
static bool UnityEvent_3_t814_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t814_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern const Il2CppType UnityEvent_3_t814_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t814_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t814_gp_2_0_0_0;
extern const Il2CppType InvokableCall_3_t909_0_0_0;
extern const Il2CppGenericMethod InvokableCall_3__ctor_m3727_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_3_t814_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t814_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t814_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t814_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t909_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_3__ctor_m3727_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t814_0_0_0;
extern const Il2CppType UnityEvent_3_t814_1_0_0;
struct UnityEvent_3_t814;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t814_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t690_0_0_0/* parent */
	, UnityEvent_3_t814_VTable/* vtableMethods */
	, UnityEvent_3_t814_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_3_t814_RGCTXData/* rgctxDefinition */
	, 1306/* fieldStart */

};
TypeInfo UnityEvent_3_t814_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t814_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_3_t814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t814_0_0_0/* byval_arg */
	, &UnityEvent_3_t814_1_0_0/* this_arg */
	, &UnityEvent_3_t814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_3_t814_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t815_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_4_t815_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t815_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t815_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t815_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_4_t815_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t815_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t815_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_4_t815_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t815_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t815_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityEvent_4_t815_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t815_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t815_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityEvent_4_t815_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t815_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t815_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t815_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t815_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_4_t815_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_4_t815_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t815_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
extern const MethodInfo UnityEvent_4__ctor_m3633_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_4_t815_UnityEvent_4_FindMethod_Impl_m3634_ParameterInfos[] = 
{
	{"name", 0, 134219362, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219363, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m3634_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t815_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t815_UnityEvent_4_FindMethod_Impl_m3634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_4_t815_UnityEvent_4_GetDelegate_m3635_ParameterInfos[] = 
{
	{"target", 0, 134219364, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219365, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_4_GetDelegate_m3635_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t815_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t682_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t815_UnityEvent_4_GetDelegate_m3635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_4_t815_MethodInfos[] =
{
	&UnityEvent_4__ctor_m3633_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m3634_MethodInfo,
	&UnityEvent_4_GetDelegate_m3635_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m3634_MethodInfo;
extern const MethodInfo UnityEvent_4_GetDelegate_m3635_MethodInfo;
static const Il2CppMethodReference UnityEvent_4_t815_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&UnityEventBase_ToString_m2098_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2099_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2100_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m3634_MethodInfo,
	&UnityEvent_4_GetDelegate_m3635_MethodInfo,
};
static bool UnityEvent_4_t815_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t815_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t425_0_0_0, 4},
};
extern const Il2CppType UnityEvent_4_t815_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t815_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t815_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t815_gp_3_0_0_0;
extern const Il2CppType InvokableCall_4_t914_0_0_0;
extern const Il2CppGenericMethod InvokableCall_4__ctor_m3728_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_4_t815_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t815_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t815_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t815_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t815_gp_3_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t914_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_4__ctor_m3728_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t815_0_0_0;
extern const Il2CppType UnityEvent_4_t815_1_0_0;
struct UnityEvent_4_t815;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t815_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t690_0_0_0/* parent */
	, UnityEvent_4_t815_VTable/* vtableMethods */
	, UnityEvent_4_t815_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_4_t815_RGCTXData/* rgctxDefinition */
	, 1307/* fieldStart */

};
TypeInfo UnityEvent_4_t815_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t815_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_4_t815_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t815_0_0_0/* byval_arg */
	, &UnityEvent_4_t815_1_0_0/* this_arg */
	, &UnityEvent_4_t815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_4_t815_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t691_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern const MethodInfo UserAuthorizationDialog__ctor_m3358_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m3358/* method */
	, &UserAuthorizationDialog_t691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern const MethodInfo UserAuthorizationDialog_Start_m3359_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m3359/* method */
	, &UserAuthorizationDialog_t691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern const MethodInfo UserAuthorizationDialog_OnGUI_m3360_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m3360/* method */
	, &UserAuthorizationDialog_t691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo UserAuthorizationDialog_t691_UserAuthorizationDialog_DoUserAuthorizationDialog_m3361_ParameterInfos[] = 
{
	{"windowID", 0, 134219366, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern const MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m3361_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m3361/* method */
	, &UserAuthorizationDialog_t691_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, UserAuthorizationDialog_t691_UserAuthorizationDialog_DoUserAuthorizationDialog_m3361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserAuthorizationDialog_t691_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m3358_MethodInfo,
	&UserAuthorizationDialog_Start_m3359_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m3360_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m3361_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserAuthorizationDialog_t691_VTable[] =
{
	&Object_Equals_m217_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m219_MethodInfo,
	&Object_ToString_m220_MethodInfo,
};
static bool UserAuthorizationDialog_t691_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t691_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t691_1_0_0;
extern const Il2CppType MonoBehaviour_t22_0_0_0;
struct UserAuthorizationDialog_t691;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t691_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t22_0_0_0/* parent */
	, UserAuthorizationDialog_t691_VTable/* vtableMethods */
	, UserAuthorizationDialog_t691_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1308/* fieldStart */

};
TypeInfo UserAuthorizationDialog_t691_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t691_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserAuthorizationDialog_t691_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 653/* custom_attributes_cache */
	, &UserAuthorizationDialog_t691_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t691_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t691_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t691)/* instance_size */
	, sizeof (UserAuthorizationDialog_t691)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t692_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t692_DefaultValueAttribute__ctor_m3362_ParameterInfos[] = 
{
	{"value", 0, 134219367, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern const MethodInfo DefaultValueAttribute__ctor_m3362_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m3362/* method */
	, &DefaultValueAttribute_t692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, DefaultValueAttribute_t692_DefaultValueAttribute__ctor_m3362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern const MethodInfo DefaultValueAttribute_get_Value_m3363_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m3363/* method */
	, &DefaultValueAttribute_t692_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t692_DefaultValueAttribute_Equals_m3364_ParameterInfos[] = 
{
	{"obj", 0, 134219368, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern const MethodInfo DefaultValueAttribute_Equals_m3364_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m3364/* method */
	, &DefaultValueAttribute_t692_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, DefaultValueAttribute_t692_DefaultValueAttribute_Equals_m3364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern const MethodInfo DefaultValueAttribute_GetHashCode_m3365_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m3365/* method */
	, &DefaultValueAttribute_t692_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultValueAttribute_t692_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m3362_MethodInfo,
	&DefaultValueAttribute_get_Value_m3363_MethodInfo,
	&DefaultValueAttribute_Equals_m3364_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m3365_MethodInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_get_Value_m3363_MethodInfo;
static const PropertyInfo DefaultValueAttribute_t692____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t692_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m3363_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DefaultValueAttribute_t692_PropertyInfos[] =
{
	&DefaultValueAttribute_t692____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_Equals_m3364_MethodInfo;
extern const MethodInfo DefaultValueAttribute_GetHashCode_m3365_MethodInfo;
static const Il2CppMethodReference DefaultValueAttribute_t692_VTable[] =
{
	&DefaultValueAttribute_Equals_m3364_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m3365_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool DefaultValueAttribute_t692_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t692_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t692_0_0_0;
extern const Il2CppType DefaultValueAttribute_t692_1_0_0;
struct DefaultValueAttribute_t692;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t692_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t692_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, DefaultValueAttribute_t692_VTable/* vtableMethods */
	, DefaultValueAttribute_t692_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1312/* fieldStart */

};
TypeInfo DefaultValueAttribute_t692_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t692_MethodInfos/* methods */
	, DefaultValueAttribute_t692_PropertyInfos/* properties */
	, NULL/* events */
	, &DefaultValueAttribute_t692_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 654/* custom_attributes_cache */
	, &DefaultValueAttribute_t692_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t692_1_0_0/* this_arg */
	, &DefaultValueAttribute_t692_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t692)/* instance_size */
	, sizeof (DefaultValueAttribute_t692)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern const MethodInfo ExcludeFromDocsAttribute__ctor_m3366_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m3366/* method */
	, &ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExcludeFromDocsAttribute_t693_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m3366_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExcludeFromDocsAttribute_t693_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ExcludeFromDocsAttribute_t693_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t693_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t693_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t693_1_0_0;
struct ExcludeFromDocsAttribute_t693;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t693_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t693_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t693_VTable/* vtableMethods */
	, ExcludeFromDocsAttribute_t693_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t693_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 655/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t693_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t693_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t693_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t693)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t693)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormerlySerializedAsAttribute_t409_FormerlySerializedAsAttribute__ctor_m2007_ParameterInfos[] = 
{
	{"oldName", 0, 134219369, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern const MethodInfo FormerlySerializedAsAttribute__ctor_m2007_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m2007/* method */
	, &FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t409_FormerlySerializedAsAttribute__ctor_m2007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormerlySerializedAsAttribute_t409_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m2007_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormerlySerializedAsAttribute_t409_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool FormerlySerializedAsAttribute_t409_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t409_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t409_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t409_1_0_0;
struct FormerlySerializedAsAttribute_t409;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t409_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t409_VTable/* vtableMethods */
	, FormerlySerializedAsAttribute_t409_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1313/* fieldStart */

};
TypeInfo FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t409_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 656/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t409_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t409_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t409)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t409)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t694_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const MethodInfo* TypeInferenceRules_t694_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeInferenceRules_t694_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TypeInferenceRules_t694_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t694_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t694_0_0_0;
extern const Il2CppType TypeInferenceRules_t694_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t694_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t694_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TypeInferenceRules_t694_VTable/* vtableMethods */
	, TypeInferenceRules_t694_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1314/* fieldStart */

};
TypeInfo TypeInferenceRules_t694_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t694_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t694_0_0_0/* byval_arg */
	, &TypeInferenceRules_t694_1_0_0/* this_arg */
	, &TypeInferenceRules_t694_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t694)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t694)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern const Il2CppType TypeInferenceRules_t694_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t695_TypeInferenceRuleAttribute__ctor_m3367_ParameterInfos[] = 
{
	{"rule", 0, 134219370, 0, &TypeInferenceRules_t694_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m3367_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m3367/* method */
	, &TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, TypeInferenceRuleAttribute_t695_TypeInferenceRuleAttribute__ctor_m3367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t695_TypeInferenceRuleAttribute__ctor_m3368_ParameterInfos[] = 
{
	{"rule", 0, 134219371, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m3368_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m3368/* method */
	, &TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t695_TypeInferenceRuleAttribute__ctor_m3368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m3369_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m3369/* method */
	, &TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInferenceRuleAttribute_t695_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m3367_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m3368_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m3369_MethodInfo,
	NULL
};
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m3369_MethodInfo;
static const Il2CppMethodReference TypeInferenceRuleAttribute_t695_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m3369_MethodInfo,
};
static bool TypeInferenceRuleAttribute_t695_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t695_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t695_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t695_1_0_0;
struct TypeInferenceRuleAttribute_t695;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t695_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t695_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t695_VTable/* vtableMethods */
	, TypeInferenceRuleAttribute_t695_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1319/* fieldStart */

};
TypeInfo TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t695_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 657/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t695_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t695_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t695_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t695)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t695)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t506_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern const MethodInfo GenericStack__ctor_m3370_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m3370/* method */
	, &GenericStack_t506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GenericStack_t506_MethodInfos[] =
{
	&GenericStack__ctor_m3370_MethodInfo,
	NULL
};
extern const MethodInfo Stack_GetEnumerator_m3729_MethodInfo;
extern const MethodInfo Stack_get_Count_m3730_MethodInfo;
extern const MethodInfo Stack_get_IsSynchronized_m3731_MethodInfo;
extern const MethodInfo Stack_get_SyncRoot_m3732_MethodInfo;
extern const MethodInfo Stack_CopyTo_m3733_MethodInfo;
extern const MethodInfo Stack_Clear_m3734_MethodInfo;
extern const MethodInfo Stack_Peek_m3735_MethodInfo;
extern const MethodInfo Stack_Pop_m3736_MethodInfo;
extern const MethodInfo Stack_Push_m3737_MethodInfo;
static const Il2CppMethodReference GenericStack_t506_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stack_GetEnumerator_m3729_MethodInfo,
	&Stack_get_Count_m3730_MethodInfo,
	&Stack_get_IsSynchronized_m3731_MethodInfo,
	&Stack_get_SyncRoot_m3732_MethodInfo,
	&Stack_CopyTo_m3733_MethodInfo,
	&Stack_get_Count_m3730_MethodInfo,
	&Stack_get_IsSynchronized_m3731_MethodInfo,
	&Stack_get_SyncRoot_m3732_MethodInfo,
	&Stack_Clear_m3734_MethodInfo,
	&Stack_CopyTo_m3733_MethodInfo,
	&Stack_GetEnumerator_m3729_MethodInfo,
	&Stack_Peek_m3735_MethodInfo,
	&Stack_Pop_m3736_MethodInfo,
	&Stack_Push_m3737_MethodInfo,
};
static bool GenericStack_t506_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t464_0_0_0;
extern const Il2CppType ICloneable_t427_0_0_0;
extern const Il2CppType ICollection_t860_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t506_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICloneable_t427_0_0_0, 5},
	{ &ICollection_t860_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t506_0_0_0;
extern const Il2CppType GenericStack_t506_1_0_0;
extern const Il2CppType Stack_t696_0_0_0;
struct GenericStack_t506;
const Il2CppTypeDefinitionMetadata GenericStack_t506_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t506_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t696_0_0_0/* parent */
	, GenericStack_t506_VTable/* vtableMethods */
	, GenericStack_t506_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericStack_t506_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t506_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericStack_t506_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t506_0_0_0/* byval_arg */
	, &GenericStack_t506_1_0_0/* this_arg */
	, &GenericStack_t506_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t506)/* instance_size */
	, sizeof (GenericStack_t506)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t185_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_t185_UnityAction__ctor_m1753_ParameterInfos[] = 
{
	{"object", 0, 134219372, 0, &Object_t_0_0_0},
	{"method", 1, 134219373, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction__ctor_m1753_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m1753/* method */
	, &UnityAction_t185_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, UnityAction_t185_UnityAction__ctor_m1753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern const MethodInfo UnityAction_Invoke_m3371_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m3371/* method */
	, &UnityAction_t185_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_t185_UnityAction_BeginInvoke_m3372_ParameterInfos[] = 
{
	{"callback", 0, 134219374, 0, &AsyncCallback_t214_0_0_0},
	{"object", 1, 134219375, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_BeginInvoke_m3372_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m3372/* method */
	, &UnityAction_t185_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t185_UnityAction_BeginInvoke_m3372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnityAction_t185_UnityAction_EndInvoke_m3373_ParameterInfos[] = 
{
	{"result", 0, 134219376, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_EndInvoke_m3373_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m3373/* method */
	, &UnityAction_t185_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UnityAction_t185_UnityAction_EndInvoke_m3373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_t185_MethodInfos[] =
{
	&UnityAction__ctor_m1753_MethodInfo,
	&UnityAction_Invoke_m3371_MethodInfo,
	&UnityAction_BeginInvoke_m3372_MethodInfo,
	&UnityAction_EndInvoke_m3373_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo UnityAction_Invoke_m3371_MethodInfo;
extern const MethodInfo UnityAction_BeginInvoke_m3372_MethodInfo;
extern const MethodInfo UnityAction_EndInvoke_m3373_MethodInfo;
static const Il2CppMethodReference UnityAction_t185_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnityAction_Invoke_m3371_MethodInfo,
	&UnityAction_BeginInvoke_m3372_MethodInfo,
	&UnityAction_EndInvoke_m3373_MethodInfo,
};
static bool UnityAction_t185_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_t185_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_t185_0_0_0;
extern const Il2CppType UnityAction_t185_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
struct UnityAction_t185;
const Il2CppTypeDefinitionMetadata UnityAction_t185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t185_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnityAction_t185_VTable/* vtableMethods */
	, UnityAction_t185_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_t185_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t185_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_t185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t185_0_0_0/* byval_arg */
	, &UnityAction_t185_1_0_0/* this_arg */
	, &UnityAction_t185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t185/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t185)/* instance_size */
	, sizeof (UnityAction_t185)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t816_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_1_t816_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t816_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_1_t816_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_1_t816_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityAction_1_t816_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t816_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_1_t816_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_1_t816_il2cpp_TypeInfo, 1, 0, UnityAction_1_t816_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_1_t816_UnityAction_1__ctor_m3636_ParameterInfos[] = 
{
	{"object", 0, 134219377, 0, &Object_t_0_0_0},
	{"method", 1, 134219378, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_1__ctor_m3636_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t816_UnityAction_1__ctor_m3636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t816_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t816_gp_0_0_0_0;
static const ParameterInfo UnityAction_1_t816_UnityAction_1_Invoke_m3637_ParameterInfos[] = 
{
	{"arg0", 0, 134219379, 0, &UnityAction_1_t816_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
extern const MethodInfo UnityAction_1_Invoke_m3637_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t816_UnityAction_1_Invoke_m3637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t816_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_1_t816_UnityAction_1_BeginInvoke_m3638_ParameterInfos[] = 
{
	{"arg0", 0, 134219380, 0, &UnityAction_1_t816_gp_0_0_0_0},
	{"callback", 1, 134219381, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134219382, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_1_BeginInvoke_m3638_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t816_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t816_UnityAction_1_BeginInvoke_m3638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnityAction_1_t816_UnityAction_1_EndInvoke_m3639_ParameterInfos[] = 
{
	{"result", 0, 134219383, 0, &IAsyncResult_t213_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_1_EndInvoke_m3639_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t816_UnityAction_1_EndInvoke_m3639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_1_t816_MethodInfos[] =
{
	&UnityAction_1__ctor_m3636_MethodInfo,
	&UnityAction_1_Invoke_m3637_MethodInfo,
	&UnityAction_1_BeginInvoke_m3638_MethodInfo,
	&UnityAction_1_EndInvoke_m3639_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_1_Invoke_m3637_MethodInfo;
extern const MethodInfo UnityAction_1_BeginInvoke_m3638_MethodInfo;
extern const MethodInfo UnityAction_1_EndInvoke_m3639_MethodInfo;
static const Il2CppMethodReference UnityAction_1_t816_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnityAction_1_Invoke_m3637_MethodInfo,
	&UnityAction_1_BeginInvoke_m3638_MethodInfo,
	&UnityAction_1_EndInvoke_m3639_MethodInfo,
};
static bool UnityAction_1_t816_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t816_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_1_t816_0_0_0;
extern const Il2CppType UnityAction_1_t816_1_0_0;
struct UnityAction_1_t816;
const Il2CppTypeDefinitionMetadata UnityAction_1_t816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t816_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnityAction_1_t816_VTable/* vtableMethods */
	, UnityAction_1_t816_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_1_t816_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t816_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_1_t816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t816_0_0_0/* byval_arg */
	, &UnityAction_1_t816_1_0_0/* this_arg */
	, &UnityAction_1_t816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_1_t816_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t817_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_2_t817_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t817_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t817_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t817_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_2_t817_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t817_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t817_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityAction_2_t817_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t817_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t817_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_2_t817_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_2_t817_il2cpp_TypeInfo, 2, 0, UnityAction_2_t817_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_2_t817_UnityAction_2__ctor_m3640_ParameterInfos[] = 
{
	{"object", 0, 134219384, 0, &Object_t_0_0_0},
	{"method", 1, 134219385, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_2__ctor_m3640_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t817_UnityAction_2__ctor_m3640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t817_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t817_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t817_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t817_gp_1_0_0_0;
static const ParameterInfo UnityAction_2_t817_UnityAction_2_Invoke_m3641_ParameterInfos[] = 
{
	{"arg0", 0, 134219386, 0, &UnityAction_2_t817_gp_0_0_0_0},
	{"arg1", 1, 134219387, 0, &UnityAction_2_t817_gp_1_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
extern const MethodInfo UnityAction_2_Invoke_m3641_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t817_UnityAction_2_Invoke_m3641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t817_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t817_gp_1_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_2_t817_UnityAction_2_BeginInvoke_m3642_ParameterInfos[] = 
{
	{"arg0", 0, 134219388, 0, &UnityAction_2_t817_gp_0_0_0_0},
	{"arg1", 1, 134219389, 0, &UnityAction_2_t817_gp_1_0_0_0},
	{"callback", 2, 134219390, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134219391, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_2_BeginInvoke_m3642_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t817_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t817_UnityAction_2_BeginInvoke_m3642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnityAction_2_t817_UnityAction_2_EndInvoke_m3643_ParameterInfos[] = 
{
	{"result", 0, 134219392, 0, &IAsyncResult_t213_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_2_EndInvoke_m3643_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t817_UnityAction_2_EndInvoke_m3643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_2_t817_MethodInfos[] =
{
	&UnityAction_2__ctor_m3640_MethodInfo,
	&UnityAction_2_Invoke_m3641_MethodInfo,
	&UnityAction_2_BeginInvoke_m3642_MethodInfo,
	&UnityAction_2_EndInvoke_m3643_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_2_Invoke_m3641_MethodInfo;
extern const MethodInfo UnityAction_2_BeginInvoke_m3642_MethodInfo;
extern const MethodInfo UnityAction_2_EndInvoke_m3643_MethodInfo;
static const Il2CppMethodReference UnityAction_2_t817_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnityAction_2_Invoke_m3641_MethodInfo,
	&UnityAction_2_BeginInvoke_m3642_MethodInfo,
	&UnityAction_2_EndInvoke_m3643_MethodInfo,
};
static bool UnityAction_2_t817_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_2_t817_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_2_t817_0_0_0;
extern const Il2CppType UnityAction_2_t817_1_0_0;
struct UnityAction_2_t817;
const Il2CppTypeDefinitionMetadata UnityAction_2_t817_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t817_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnityAction_2_t817_VTable/* vtableMethods */
	, UnityAction_2_t817_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_2_t817_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t817_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_2_t817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t817_0_0_0/* byval_arg */
	, &UnityAction_2_t817_1_0_0/* this_arg */
	, &UnityAction_2_t817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_2_t817_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t818_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_3_t818_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t818_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t818_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t818_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_3_t818_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t818_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t818_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_3_t818_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t818_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t818_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityAction_3_t818_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t818_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t818_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t818_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_3_t818_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_3_t818_il2cpp_TypeInfo, 3, 0, UnityAction_3_t818_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_3_t818_UnityAction_3__ctor_m3644_ParameterInfos[] = 
{
	{"object", 0, 134219393, 0, &Object_t_0_0_0},
	{"method", 1, 134219394, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_3__ctor_m3644_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t818_UnityAction_3__ctor_m3644_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t818_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_2_0_0_0;
static const ParameterInfo UnityAction_3_t818_UnityAction_3_Invoke_m3645_ParameterInfos[] = 
{
	{"arg0", 0, 134219395, 0, &UnityAction_3_t818_gp_0_0_0_0},
	{"arg1", 1, 134219396, 0, &UnityAction_3_t818_gp_1_0_0_0},
	{"arg2", 2, 134219397, 0, &UnityAction_3_t818_gp_2_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
extern const MethodInfo UnityAction_3_Invoke_m3645_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t818_UnityAction_3_Invoke_m3645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t818_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t818_gp_2_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_3_t818_UnityAction_3_BeginInvoke_m3646_ParameterInfos[] = 
{
	{"arg0", 0, 134219398, 0, &UnityAction_3_t818_gp_0_0_0_0},
	{"arg1", 1, 134219399, 0, &UnityAction_3_t818_gp_1_0_0_0},
	{"arg2", 2, 134219400, 0, &UnityAction_3_t818_gp_2_0_0_0},
	{"callback", 3, 134219401, 0, &AsyncCallback_t214_0_0_0},
	{"object", 4, 134219402, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_3_BeginInvoke_m3646_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t818_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t818_UnityAction_3_BeginInvoke_m3646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnityAction_3_t818_UnityAction_3_EndInvoke_m3647_ParameterInfos[] = 
{
	{"result", 0, 134219403, 0, &IAsyncResult_t213_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_3_EndInvoke_m3647_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t818_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t818_UnityAction_3_EndInvoke_m3647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_3_t818_MethodInfos[] =
{
	&UnityAction_3__ctor_m3644_MethodInfo,
	&UnityAction_3_Invoke_m3645_MethodInfo,
	&UnityAction_3_BeginInvoke_m3646_MethodInfo,
	&UnityAction_3_EndInvoke_m3647_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_3_Invoke_m3645_MethodInfo;
extern const MethodInfo UnityAction_3_BeginInvoke_m3646_MethodInfo;
extern const MethodInfo UnityAction_3_EndInvoke_m3647_MethodInfo;
static const Il2CppMethodReference UnityAction_3_t818_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnityAction_3_Invoke_m3645_MethodInfo,
	&UnityAction_3_BeginInvoke_m3646_MethodInfo,
	&UnityAction_3_EndInvoke_m3647_MethodInfo,
};
static bool UnityAction_3_t818_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t818_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_3_t818_0_0_0;
extern const Il2CppType UnityAction_3_t818_1_0_0;
struct UnityAction_3_t818;
const Il2CppTypeDefinitionMetadata UnityAction_3_t818_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t818_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnityAction_3_t818_VTable/* vtableMethods */
	, UnityAction_3_t818_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_3_t818_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t818_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_3_t818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t818_0_0_0/* byval_arg */
	, &UnityAction_3_t818_1_0_0/* this_arg */
	, &UnityAction_3_t818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_3_t818_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t819_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_4_t819_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t819_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t819_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t819_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_4_t819_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t819_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t819_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_4_t819_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t819_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t819_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityAction_4_t819_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t819_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t819_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityAction_4_t819_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t819_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t819_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t819_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t819_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_4_t819_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_4_t819_il2cpp_TypeInfo, 4, 0, UnityAction_4_t819_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_4_t819_UnityAction_4__ctor_m3648_ParameterInfos[] = 
{
	{"object", 0, 134219404, 0, &Object_t_0_0_0},
	{"method", 1, 134219405, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_4__ctor_m3648_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t819_UnityAction_4__ctor_m3648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t819_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_3_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_3_0_0_0;
static const ParameterInfo UnityAction_4_t819_UnityAction_4_Invoke_m3649_ParameterInfos[] = 
{
	{"arg0", 0, 134219406, 0, &UnityAction_4_t819_gp_0_0_0_0},
	{"arg1", 1, 134219407, 0, &UnityAction_4_t819_gp_1_0_0_0},
	{"arg2", 2, 134219408, 0, &UnityAction_4_t819_gp_2_0_0_0},
	{"arg3", 3, 134219409, 0, &UnityAction_4_t819_gp_3_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
extern const MethodInfo UnityAction_4_Invoke_m3649_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t819_UnityAction_4_Invoke_m3649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t819_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t819_gp_3_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_4_t819_UnityAction_4_BeginInvoke_m3650_ParameterInfos[] = 
{
	{"arg0", 0, 134219410, 0, &UnityAction_4_t819_gp_0_0_0_0},
	{"arg1", 1, 134219411, 0, &UnityAction_4_t819_gp_1_0_0_0},
	{"arg2", 2, 134219412, 0, &UnityAction_4_t819_gp_2_0_0_0},
	{"arg3", 3, 134219413, 0, &UnityAction_4_t819_gp_3_0_0_0},
	{"callback", 4, 134219414, 0, &AsyncCallback_t214_0_0_0},
	{"object", 5, 134219415, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_4_BeginInvoke_m3650_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t819_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t819_UnityAction_4_BeginInvoke_m3650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo UnityAction_4_t819_UnityAction_4_EndInvoke_m3651_ParameterInfos[] = 
{
	{"result", 0, 134219416, 0, &IAsyncResult_t213_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_4_EndInvoke_m3651_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t819_UnityAction_4_EndInvoke_m3651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_4_t819_MethodInfos[] =
{
	&UnityAction_4__ctor_m3648_MethodInfo,
	&UnityAction_4_Invoke_m3649_MethodInfo,
	&UnityAction_4_BeginInvoke_m3650_MethodInfo,
	&UnityAction_4_EndInvoke_m3651_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_4_Invoke_m3649_MethodInfo;
extern const MethodInfo UnityAction_4_BeginInvoke_m3650_MethodInfo;
extern const MethodInfo UnityAction_4_EndInvoke_m3651_MethodInfo;
static const Il2CppMethodReference UnityAction_4_t819_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&UnityAction_4_Invoke_m3649_MethodInfo,
	&UnityAction_4_BeginInvoke_m3650_MethodInfo,
	&UnityAction_4_EndInvoke_m3651_MethodInfo,
};
static bool UnityAction_4_t819_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t819_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_4_t819_0_0_0;
extern const Il2CppType UnityAction_4_t819_1_0_0;
struct UnityAction_4_t819;
const Il2CppTypeDefinitionMetadata UnityAction_4_t819_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t819_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, UnityAction_4_t819_VTable/* vtableMethods */
	, UnityAction_4_t819_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_4_t819_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t819_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_4_t819_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t819_0_0_0/* byval_arg */
	, &UnityAction_4_t819_1_0_0/* this_arg */
	, &UnityAction_4_t819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_4_t819_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
