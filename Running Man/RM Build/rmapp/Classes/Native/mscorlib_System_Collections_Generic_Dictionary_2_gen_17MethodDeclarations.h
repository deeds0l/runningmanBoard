﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2779;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t3186;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t2783;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t2787;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2565;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int64>
struct IDictionary_2_t3190;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct KeyValuePair_2U5BU5D_t3191;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct IEnumerator_1_t3192;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor()
extern "C" void Dictionary_2__ctor_m16108_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m16108(__this, method) (( void (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2__ctor_m16108_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16110_gshared (Dictionary_2_t2779 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16110(__this, ___comparer, method) (( void (*) (Dictionary_2_t2779 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16110_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m16112_gshared (Dictionary_2_t2779 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m16112(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2779 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16112_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16114_gshared (Dictionary_2_t2779 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16114(__this, ___capacity, method) (( void (*) (Dictionary_2_t2779 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16114_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16116_gshared (Dictionary_2_t2779 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16116(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2779 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16116_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16118_gshared (Dictionary_2_t2779 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16118(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2779 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m16118_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16120_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16120(__this, method) (( Object_t* (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16120_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16122_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16122(__this, method) (( Object_t* (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16122_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16124_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16124(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16124_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16126_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16126(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16126_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16128_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16128(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16128_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16130_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16130(__this, ___key, method) (( bool (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16130_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16132_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16132(__this, ___key, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16132_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16134_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16134(__this, method) (( bool (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16134_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16136_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16136(__this, method) (( Object_t * (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16136_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16138_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16138(__this, method) (( bool (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16138_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16140_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2_t2780  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16140(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2779 *, KeyValuePair_2_t2780 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16140_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16142_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2_t2780  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16142(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2779 *, KeyValuePair_2_t2780 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16142_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16144_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2U5BU5D_t3191* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16144(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2779 *, KeyValuePair_2U5BU5D_t3191*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16144_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16146_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2_t2780  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16146(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2779 *, KeyValuePair_2_t2780 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16146_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16148_gshared (Dictionary_2_t2779 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16148(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2779 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16148_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16150_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16150(__this, method) (( Object_t * (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16150_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16152_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16152(__this, method) (( Object_t* (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16152_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16154_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16154(__this, method) (( Object_t * (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16154_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16156_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16156(__this, method) (( int32_t (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_get_Count_m16156_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Item(TKey)
extern "C" int64_t Dictionary_2_get_Item_m16158_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16158(__this, ___key, method) (( int64_t (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m16158_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16160_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16160(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_set_Item_m16160_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16162_gshared (Dictionary_2_t2779 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16162(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2779 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16162_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16164_gshared (Dictionary_2_t2779 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16164(__this, ___size, method) (( void (*) (Dictionary_2_t2779 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16164_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16166_gshared (Dictionary_2_t2779 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16166(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2779 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16166_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2780  Dictionary_2_make_pair_m16168_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16168(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2780  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_make_pair_m16168_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m16170_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16170(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_key_m16170_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_value(TKey,TValue)
extern "C" int64_t Dictionary_2_pick_value_m16172_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16172(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_value_m16172_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16174_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2U5BU5D_t3191* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16174(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2779 *, KeyValuePair_2U5BU5D_t3191*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16174_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Resize()
extern "C" void Dictionary_2_Resize_m16176_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16176(__this, method) (( void (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_Resize_m16176_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16178_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16178(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_Add_m16178_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Clear()
extern "C" void Dictionary_2_Clear_m16180_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16180(__this, method) (( void (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_Clear_m16180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16182_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16182(__this, ___key, method) (( bool (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m16182_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16184_gshared (Dictionary_2_t2779 * __this, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16184(__this, ___value, method) (( bool (*) (Dictionary_2_t2779 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m16184_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16186_gshared (Dictionary_2_t2779 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16186(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2779 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m16186_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16188_gshared (Dictionary_2_t2779 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16188(__this, ___sender, method) (( void (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16188_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16190_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16190(__this, ___key, method) (( bool (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m16190_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16192_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, int64_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16192(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2779 *, Object_t *, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m16192_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Keys()
extern "C" KeyCollection_t2783 * Dictionary_2_get_Keys_m16194_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16194(__this, method) (( KeyCollection_t2783 * (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_get_Keys_m16194_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Values()
extern "C" ValueCollection_t2787 * Dictionary_2_get_Values_m16196_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16196(__this, method) (( ValueCollection_t2787 * (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_get_Values_m16196_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m16198_gshared (Dictionary_2_t2779 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16198(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16198_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTValue(System.Object)
extern "C" int64_t Dictionary_2_ToTValue_m16200_gshared (Dictionary_2_t2779 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16200(__this, ___value, method) (( int64_t (*) (Dictionary_2_t2779 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16200_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16202_gshared (Dictionary_2_t2779 * __this, KeyValuePair_2_t2780  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16202(__this, ___pair, method) (( bool (*) (Dictionary_2_t2779 *, KeyValuePair_2_t2780 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16202_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t2785  Dictionary_2_GetEnumerator_m16204_gshared (Dictionary_2_t2779 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16204(__this, method) (( Enumerator_t2785  (*) (Dictionary_2_t2779 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16204_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m16206_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16206(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16206_gshared)(__this /* static, unused */, ___key, ___value, method)
