﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>
struct ReadOnlyCollection_1_t2721;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t515;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.GUILayoutEntry>
struct IList_1_t2720;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2719;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutEntry>
struct IEnumerator_1_t3155;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m15180(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2721 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m11356_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15181(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2721 *, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11357_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15182(__this, method) (( void (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11358_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15183(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11359_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15184(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11360_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15185(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11361_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15186(__this, ___index, method) (( GUILayoutEntry_t515 * (*) (ReadOnlyCollection_1_t2721 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11362_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15187(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11363_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15188(__this, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11364_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15189(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2721 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11365_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15190(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11366_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m15191(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2721 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m11367_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m15192(__this, method) (( void (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m11368_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m15193(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m11369_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15194(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2721 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11370_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m15195(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m11371_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m15196(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2721 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m11372_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15197(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11373_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15198(__this, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11374_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15199(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11375_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15200(__this, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11376_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15201(__this, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11377_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m15202(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2721 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m11378_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m15203(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2721 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m11379_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::Contains(T)
#define ReadOnlyCollection_1_Contains_m15204(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2721 *, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m11380_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m15205(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2721 *, GUILayoutEntryU5BU5D_t2719*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m11381_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m15206(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m11382_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m15207(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2721 *, GUILayoutEntry_t515 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m11383_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::get_Count()
#define ReadOnlyCollection_1_get_Count_m15208(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2721 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m11384_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m15209(__this, ___index, method) (( GUILayoutEntry_t515 * (*) (ReadOnlyCollection_1_t2721 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m11385_gshared)(__this, ___index, method)
