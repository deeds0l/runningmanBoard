﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>
struct ReadOnlyCollection_1_t3013;
// System.Security.Policy.StrongName
struct StrongName_t1764;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.Security.Policy.StrongName>
struct IList_1_t1760;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t3012;
// System.Collections.Generic.IEnumerator`1<System.Security.Policy.StrongName>
struct IEnumerator_1_t3284;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m18646(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3013 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m11356_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m18647(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3013 *, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11357_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m18648(__this, method) (( void (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11358_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m18649(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11359_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m18650(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11360_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m18651(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11361_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m18652(__this, ___index, method) (( StrongName_t1764 * (*) (ReadOnlyCollection_1_t3013 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11362_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m18653(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11363_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18654(__this, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11364_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m18655(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3013 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11365_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m18656(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11366_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m18657(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3013 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m11367_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m18658(__this, method) (( void (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m11368_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m18659(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m11369_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m18660(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3013 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11370_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m18661(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m11371_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m18662(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3013 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m11372_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m18663(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11373_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m18664(__this, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11374_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m18665(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11375_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m18666(__this, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11376_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m18667(__this, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11377_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m18668(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3013 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m11378_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m18669(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3013 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m11379_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::Contains(T)
#define ReadOnlyCollection_1_Contains_m18670(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3013 *, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m11380_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m18671(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3013 *, StrongNameU5BU5D_t3012*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m11381_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m18672(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m11382_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m18673(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3013 *, StrongName_t1764 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m11383_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::get_Count()
#define ReadOnlyCollection_1_get_Count_m18674(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3013 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m11384_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Security.Policy.StrongName>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m18675(__this, ___index, method) (( StrongName_t1764 * (*) (ReadOnlyCollection_1_t3013 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m11385_gshared)(__this, ___index, method)
