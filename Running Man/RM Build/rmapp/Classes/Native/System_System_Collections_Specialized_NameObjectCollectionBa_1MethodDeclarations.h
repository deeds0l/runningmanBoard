﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t984;
// System.Object
struct Object_t;
// System.Collections.Specialized.NameObjectCollectionBase
struct NameObjectCollectionBase_t982;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;

// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::.ctor(System.Collections.Specialized.NameObjectCollectionBase)
extern "C" void KeysCollection__ctor_m3830 (KeysCollection_t984 * __this, NameObjectCollectionBase_t982 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m3831 (KeysCollection_t984 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeysCollection_System_Collections_ICollection_get_IsSynchronized_m3832 (KeysCollection_t984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeysCollection_System_Collections_ICollection_get_SyncRoot_m3833 (KeysCollection_t984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::get_Count()
extern "C" int32_t KeysCollection_get_Count_m3834 (KeysCollection_t984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Specialized.NameObjectCollectionBase/KeysCollection::GetEnumerator()
extern "C" Object_t * KeysCollection_GetEnumerator_m3835 (KeysCollection_t984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
