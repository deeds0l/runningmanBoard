﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt64>
struct InternalEnumerator_1_t2820;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16652_gshared (InternalEnumerator_1_t2820 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16652(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2820 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16652_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16653_gshared (InternalEnumerator_1_t2820 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16653(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2820 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16653_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16654_gshared (InternalEnumerator_1_t2820 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16654(__this, method) (( void (*) (InternalEnumerator_1_t2820 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16654_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16655_gshared (InternalEnumerator_1_t2820 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16655(__this, method) (( bool (*) (InternalEnumerator_1_t2820 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16655_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C" uint64_t InternalEnumerator_1_get_Current_m16656_gshared (InternalEnumerator_1_t2820 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16656(__this, method) (( uint64_t (*) (InternalEnumerator_1_t2820 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16656_gshared)(__this, method)
