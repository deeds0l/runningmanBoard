﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Guid>
struct Comparer_1_t3025;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Guid>
struct  Comparer_1_t3025  : public Object_t
{
};
struct Comparer_1_t3025_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Guid>::_default
	Comparer_1_t3025 * ____default_0;
};
