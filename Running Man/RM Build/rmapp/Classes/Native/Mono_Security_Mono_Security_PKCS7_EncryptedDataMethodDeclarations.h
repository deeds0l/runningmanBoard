﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1234;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1233;
// System.Byte[]
struct ByteU5BU5D_t546;
// Mono.Security.ASN1
struct ASN1_t1150;

// System.Void Mono.Security.PKCS7/EncryptedData::.ctor()
extern "C" void EncryptedData__ctor_m5077 (EncryptedData_t1234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/EncryptedData::.ctor(Mono.Security.ASN1)
extern "C" void EncryptedData__ctor_m5078 (EncryptedData_t1234 * __this, ASN1_t1150 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/EncryptedData::get_EncryptionAlgorithm()
extern "C" ContentInfo_t1233 * EncryptedData_get_EncryptionAlgorithm_m5079 (EncryptedData_t1234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/EncryptedData::get_EncryptedContent()
extern "C" ByteU5BU5D_t546* EncryptedData_get_EncryptedContent_m5080 (EncryptedData_t1234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
