﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t2547;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m12881_gshared (Transform_1_t2547 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m12881(__this, ___object, ___method, method) (( void (*) (Transform_1_t2547 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m12881_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t2536  Transform_1_Invoke_m12882_gshared (Transform_1_t2547 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Transform_1_Invoke_m12882(__this, ___key, ___value, method) (( KeyValuePair_2_t2536  (*) (Transform_1_t2547 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m12882_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m12883_gshared (Transform_1_t2547 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t214 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m12883(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2547 *, int32_t, Object_t *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m12883_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t2536  Transform_1_EndInvoke_m12884_gshared (Transform_1_t2547 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m12884(__this, ___result, method) (( KeyValuePair_2_t2536  (*) (Transform_1_t2547 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m12884_gshared)(__this, ___result, method)
