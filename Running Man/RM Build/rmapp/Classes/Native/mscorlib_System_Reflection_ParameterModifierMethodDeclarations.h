﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t1584;
struct ParameterModifier_t1584_marshaled;

void ParameterModifier_t1584_marshal(const ParameterModifier_t1584& unmarshaled, ParameterModifier_t1584_marshaled& marshaled);
void ParameterModifier_t1584_marshal_back(const ParameterModifier_t1584_marshaled& marshaled, ParameterModifier_t1584& unmarshaled);
void ParameterModifier_t1584_marshal_cleanup(ParameterModifier_t1584_marshaled& marshaled);
