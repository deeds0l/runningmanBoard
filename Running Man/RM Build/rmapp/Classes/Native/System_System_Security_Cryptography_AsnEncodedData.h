﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.Oid
struct Oid_t1026;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t528;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.AsnEncodedData
struct  AsnEncodedData_t1025  : public Object_t
{
	// System.Security.Cryptography.Oid System.Security.Cryptography.AsnEncodedData::_oid
	Oid_t1026 * ____oid_0;
	// System.Byte[] System.Security.Cryptography.AsnEncodedData::_raw
	ByteU5BU5D_t546* ____raw_1;
};
struct AsnEncodedData_t1025_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Cryptography.AsnEncodedData::<>f__switch$mapA
	Dictionary_2_t528 * ___U3CU3Ef__switchU24mapA_2;
};
