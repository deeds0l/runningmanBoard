﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2761;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t708;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3181;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t378;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m15907_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15907(__this, method) (( void (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1__ctor_m15907_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15908_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15908(__this, method) (( bool (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15908_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15909_gshared (Collection_1_t2761 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15909(__this, ___array, ___index, method) (( void (*) (Collection_1_t2761 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15909_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15910_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15910(__this, method) (( Object_t * (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15910_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15911_gshared (Collection_1_t2761 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15911(__this, ___value, method) (( int32_t (*) (Collection_1_t2761 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15911_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15912_gshared (Collection_1_t2761 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15912(__this, ___value, method) (( bool (*) (Collection_1_t2761 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15912_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15913_gshared (Collection_1_t2761 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15913(__this, ___value, method) (( int32_t (*) (Collection_1_t2761 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15913_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15914_gshared (Collection_1_t2761 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15914(__this, ___index, ___value, method) (( void (*) (Collection_1_t2761 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15914_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15915_gshared (Collection_1_t2761 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15915(__this, ___value, method) (( void (*) (Collection_1_t2761 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15915_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15916_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15916(__this, method) (( bool (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15916_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15917_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15917(__this, method) (( Object_t * (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15917_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15918_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15918(__this, method) (( bool (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15918_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15919_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15919(__this, method) (( bool (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15919_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15920_gshared (Collection_1_t2761 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15920(__this, ___index, method) (( Object_t * (*) (Collection_1_t2761 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15920_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15921_gshared (Collection_1_t2761 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15921(__this, ___index, ___value, method) (( void (*) (Collection_1_t2761 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15921_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m15922_gshared (Collection_1_t2761 * __this, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_Add_m15922(__this, ___item, method) (( void (*) (Collection_1_t2761 *, UICharInfo_t377 , const MethodInfo*))Collection_1_Add_m15922_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m15923_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15923(__this, method) (( void (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_Clear_m15923_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m15924_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15924(__this, method) (( void (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_ClearItems_m15924_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m15925_gshared (Collection_1_t2761 * __this, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15925(__this, ___item, method) (( bool (*) (Collection_1_t2761 *, UICharInfo_t377 , const MethodInfo*))Collection_1_Contains_m15925_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15926_gshared (Collection_1_t2761 * __this, UICharInfoU5BU5D_t708* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15926(__this, ___array, ___index, method) (( void (*) (Collection_1_t2761 *, UICharInfoU5BU5D_t708*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15926_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15927_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15927(__this, method) (( Object_t* (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_GetEnumerator_m15927_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15928_gshared (Collection_1_t2761 * __this, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15928(__this, ___item, method) (( int32_t (*) (Collection_1_t2761 *, UICharInfo_t377 , const MethodInfo*))Collection_1_IndexOf_m15928_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15929_gshared (Collection_1_t2761 * __this, int32_t ___index, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15929(__this, ___index, ___item, method) (( void (*) (Collection_1_t2761 *, int32_t, UICharInfo_t377 , const MethodInfo*))Collection_1_Insert_m15929_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15930_gshared (Collection_1_t2761 * __this, int32_t ___index, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15930(__this, ___index, ___item, method) (( void (*) (Collection_1_t2761 *, int32_t, UICharInfo_t377 , const MethodInfo*))Collection_1_InsertItem_m15930_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m15931_gshared (Collection_1_t2761 * __this, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15931(__this, ___item, method) (( bool (*) (Collection_1_t2761 *, UICharInfo_t377 , const MethodInfo*))Collection_1_Remove_m15931_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15932_gshared (Collection_1_t2761 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15932(__this, ___index, method) (( void (*) (Collection_1_t2761 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15932_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15933_gshared (Collection_1_t2761 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15933(__this, ___index, method) (( void (*) (Collection_1_t2761 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15933_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15934_gshared (Collection_1_t2761 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15934(__this, method) (( int32_t (*) (Collection_1_t2761 *, const MethodInfo*))Collection_1_get_Count_m15934_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t377  Collection_1_get_Item_m15935_gshared (Collection_1_t2761 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15935(__this, ___index, method) (( UICharInfo_t377  (*) (Collection_1_t2761 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15935_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15936_gshared (Collection_1_t2761 * __this, int32_t ___index, UICharInfo_t377  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15936(__this, ___index, ___value, method) (( void (*) (Collection_1_t2761 *, int32_t, UICharInfo_t377 , const MethodInfo*))Collection_1_set_Item_m15936_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15937_gshared (Collection_1_t2761 * __this, int32_t ___index, UICharInfo_t377  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15937(__this, ___index, ___item, method) (( void (*) (Collection_1_t2761 *, int32_t, UICharInfo_t377 , const MethodInfo*))Collection_1_SetItem_m15937_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15938_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15938(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15938_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t377  Collection_1_ConvertItem_m15939_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15939(__this /* static, unused */, ___item, method) (( UICharInfo_t377  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15939_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15940_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15940(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15940_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15941_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15941(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15941_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15942_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15942(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15942_gshared)(__this /* static, unused */, ___list, method)
