﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
void* RuntimeInvoker_Char_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
void* RuntimeInvoker_UInt16_t371 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t744 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
void* RuntimeInvoker_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
void* RuntimeInvoker_UInt64_t756 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
void* RuntimeInvoker_MoveDirection_t133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
void* RuntimeInvoker_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t137  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t137  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
void* RuntimeInvoker_InputButton_t140 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
void* RuntimeInvoker_InputMode_t151 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_LayerMask_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t158  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t158  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
void* RuntimeInvoker_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
void* RuntimeInvoker_ColorTweenMode_t159 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
void* RuntimeInvoker_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t175  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t175  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
void* RuntimeInvoker_FontStyle_t445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
void* RuntimeInvoker_TextAnchor_t388 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
void* RuntimeInvoker_HorizontalWrapMode_t446 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
void* RuntimeInvoker_VerticalWrapMode_t447 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t220  (*Func)(void* obj, const MethodInfo* method);
	Rect_t220  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
void* RuntimeInvoker_BlockingObjects_t189 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
void* RuntimeInvoker_Type_t194 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
void* RuntimeInvoker_FillMethod_t195 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
void* RuntimeInvoker_ContentType_t205 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
void* RuntimeInvoker_LineType_t208 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
void* RuntimeInvoker_InputType_t206 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
void* RuntimeInvoker_TouchScreenKeyboardType_t370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
void* RuntimeInvoker_CharacterValidation_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
void* RuntimeInvoker_Mode_t231 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
void* RuntimeInvoker_Navigation_t232 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t232  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t232  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
void* RuntimeInvoker_Direction_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
void* RuntimeInvoker_Axis_t238 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
void* RuntimeInvoker_MovementType_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Bounds_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t247  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t247  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
void* RuntimeInvoker_Transition_t248 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
void* RuntimeInvoker_SpriteState_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t252  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t252  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
void* RuntimeInvoker_SelectionState_t249 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
void* RuntimeInvoker_Direction_t254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
void* RuntimeInvoker_Axis_t256 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
void* RuntimeInvoker_AspectMode_t269 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
void* RuntimeInvoker_ScaleMode_t271 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
void* RuntimeInvoker_ScreenMatchMode_t272 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
void* RuntimeInvoker_Unit_t273 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
void* RuntimeInvoker_FitMode_t274 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
void* RuntimeInvoker_Corner_t276 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
void* RuntimeInvoker_Axis_t277 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
void* RuntimeInvoker_Constraint_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t530 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t529 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Matrix4x4_t383 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
void* RuntimeInvoker_RuntimePlatform_t476 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t654 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t653  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t653  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
void* RuntimeInvoker_TouchPhase_t558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
void* RuntimeInvoker_Quaternion_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t362  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t362  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t475 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t578 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t578  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t578  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t579 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t579  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t579  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
void* RuntimeInvoker_RenderMode_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
void* RuntimeInvoker_SourceID_t610 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
void* RuntimeInvoker_AppID_t609 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
void* RuntimeInvoker_NetworkID_t611 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
void* RuntimeInvoker_NodeID_t612 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t669 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t670 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t664  (*Func)(void* obj, const MethodInfo* method);
	Range_t664  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t671 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
void* RuntimeInvoker_PersistentListenerMode_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t989 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t994 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t1014 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1052 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t1059 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t1060 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t1084 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t1106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1106  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1106  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
void* RuntimeInvoker_Category_t1091 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t1087 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1227 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1254 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t1267 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t1268 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t1270 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t1289 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t1287 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t1363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t1303 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t1302 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t1316 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t1288 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t1168 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1168  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1168  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t1282 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t1879 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t1591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t1371 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1371  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1371  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1415 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
void* RuntimeInvoker_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t1565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1871  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1871  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t1571 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t1568 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1372  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1372  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t1587 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t1562 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t1566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t1583 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t726  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t726  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t1698 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t1715 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1715  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1715  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t1718 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t1721 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t1733 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t1831 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t1829 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
void* RuntimeInvoker_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTimeOffset_t768  (*Func)(void* obj, const MethodInfo* method);
	DateTimeOffset_t768  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t1868 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Guid_t769 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t769  (*Func)(void* obj, const MethodInfo* method);
	Guid_t769  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
void* RuntimeInvoker_Link_t1459 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1459  (*Func)(void* obj, const MethodInfo* method);
	Link_t1459  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
void* RuntimeInvoker_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t349  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t349  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
void* RuntimeInvoker_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t323  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t323  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t225  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t225  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t375  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t375  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t377  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t377  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t650  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t650  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t651  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t651  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t580  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t580  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1584  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1584  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t665  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t665  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1045  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1045  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t1099 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1099  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1099  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1135  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1135  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1392  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1392  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t1469 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1469  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1469  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t1476 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1476  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1476  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2462 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2462  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2462  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2486 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2486  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
void* RuntimeInvoker_Enumerator_t2597 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2597  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2597  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
void* RuntimeInvoker_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2593  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2593  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_6.h"
void* RuntimeInvoker_Enumerator_t2596 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2596  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2596  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9.h"
void* RuntimeInvoker_Enumerator_t2600 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2600  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2600  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
void* RuntimeInvoker_Enumerator_t2429 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2429  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2429  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5.h"
void* RuntimeInvoker_Enumerator_t2544 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2544  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2544  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
void* RuntimeInvoker_Enumerator_t2541 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2541  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2541  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
void* RuntimeInvoker_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2536  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2536  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"
void* RuntimeInvoker_Enumerator_t2495 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2495  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2495  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1.h"
void* RuntimeInvoker_Enumerator_t2540 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2540  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2540  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
void* RuntimeInvoker_Enumerator_t2571 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2571  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2571  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
void* RuntimeInvoker_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2567  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2567  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_4.h"
void* RuntimeInvoker_Enumerator_t2570 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2570  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2570  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
void* RuntimeInvoker_Enumerator_t2574 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2574  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2574  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"
void* RuntimeInvoker_Enumerator_t2611 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2611  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2611  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"
void* RuntimeInvoker_Enumerator_t2759 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2759  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2759  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"
void* RuntimeInvoker_Enumerator_t2768 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2768  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2768  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
void* RuntimeInvoker_Enumerator_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2785  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2785  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
void* RuntimeInvoker_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2780  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2780  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16.h"
void* RuntimeInvoker_Enumerator_t2784 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2784  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2784  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_18.h"
void* RuntimeInvoker_Enumerator_t2788 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2788  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2788  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
void* RuntimeInvoker_Enumerator_t2823 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2823  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2823  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
void* RuntimeInvoker_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2818  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2818  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19.h"
void* RuntimeInvoker_Enumerator_t2822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2822  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2822  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_21.h"
void* RuntimeInvoker_Enumerator_t2826 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2826  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2826  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
void* RuntimeInvoker_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2839  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2839  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
void* RuntimeInvoker_Enumerator_t2868 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2868  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2868  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"
void* RuntimeInvoker_Enumerator_t2867 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2867  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2867  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_25.h"
void* RuntimeInvoker_Enumerator_t2871 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2871  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2871  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20.h"
void* RuntimeInvoker_Enumerator_t2937 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2937  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2937  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"
void* RuntimeInvoker_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2933  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2933  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_27.h"
void* RuntimeInvoker_Enumerator_t2936 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2936  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2936  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_29.h"
void* RuntimeInvoker_Enumerator_t2940 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2940  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2940  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
void* RuntimeInvoker_Enumerator_t2960 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2960  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2960  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
void* RuntimeInvoker_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2956  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2956  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_30.h"
void* RuntimeInvoker_Enumerator_t2959 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2959  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2959  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"
void* RuntimeInvoker_Enumerator_t2963 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2963  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2963  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t369_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt16_t371 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t369_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t369_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt64_t756 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t369_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t369_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ColorU26_t450 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t163 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t53 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RectU26_t823 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t220 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t220 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t71_SphericalHarmonicsL2U26_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t541 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t541 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t138 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_QuaternionU26_t827 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t362 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t362 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Matrix4x4U26_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t383 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t383 *)args[0], method);
	return NULL;
}

struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
void* RuntimeInvoker_Void_t71_StringU26_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t137  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_LayerMask_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t158  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t158 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t220  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t220 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Navigation_t232 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t232  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t232 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t175  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t175 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SpriteState_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t252  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t252 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t651  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t651 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Bounds_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t247  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t247 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Quaternion_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t362  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t362 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Range_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t664  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t664 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Interval_t1106 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1106  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1106 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RSAParameters_t1168 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1168  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1168 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Void_t71_DSAParameters_t1170 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1170  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1170 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t726  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t726 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t71_MonoEnumInfo_t1842 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1842  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1842 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
void* RuntimeInvoker_Void_t71_ColorTween_t162 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t162  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t162 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t1051  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Link_t1459 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1459  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1459 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1147  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1147 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t349  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t349 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t323  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t323 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t375  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t377  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t650  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t650 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Keyframe_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t580  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t580 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1584  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1584 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t665  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t665 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1045  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1045 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Mark_t1099 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1099  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1099 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1135  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1135 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_TableRange_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1392  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1392 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Slot_t1469 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1469  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1469 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Slot_t1476 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1476  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1476 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t225  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
void* RuntimeInvoker_Boolean_t72_LayoutRebuilder_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t288  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t288 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Bounds_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t247  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t247 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Boolean_t72_Ray_t346 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t346  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t346 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t665  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t665 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_Boolean_t72_TextGenerationSettings_t330 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t330  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t330 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Interval_t1106 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1106  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1106 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t768  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t768 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Guid_t769 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t769  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t769 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1051  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t137  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Link_t1459 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1459  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1459 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1147  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1147 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t349  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t349 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t323  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t323 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t375  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t377  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t650  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t650 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t651  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t651 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Keyframe_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t580  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t580 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1584  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1584 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1045  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1045 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Mark_t1099 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1099  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1099 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1135  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_TableRange_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1392  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1392 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Slot_t1469 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1469  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1469 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Slot_t1476 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1476  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1476 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t225  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t1168_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1168  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1168  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t73_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1170_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1170  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1170  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t718 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t718  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t718 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2593  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2593 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2536  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2536 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2567  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2567 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2780  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2780 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2818  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2818 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2839  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2839 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2933  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2933 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2956  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2956 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t718 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t718  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t718 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t1303_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t1210_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t448_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2593  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2593 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2536  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2536 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2567  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2567 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2780  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2780 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2818  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2818 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2839  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2839 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2933  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2933 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2956  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2956 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen.h"
void* RuntimeInvoker_Boolean_t72_Nullable_1_t1924 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1924  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1924 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
void* RuntimeInvoker_FramePressState_t141_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_LayerMask_t158 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t158  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t158 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t158_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t158  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t158  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t220_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t220  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t220  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
void* RuntimeInvoker_Touch_t322_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t322  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t322  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t1057_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Interval_t1106_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1106  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1106  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t1406_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t1831_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t768  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t768 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Guid_t769 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t769  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t769 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1051  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t137_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t137  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t137  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t137  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Link_t1459_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1459  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1459  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Link_t1459 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1459  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1459 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1147  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1147 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t349_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t349  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t349  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t349  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t349 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t323_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t323  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t323  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t323  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t323 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t375_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t375  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t375  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t375  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t377_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t377  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t377  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t377  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t650_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t650  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t650  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t650  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t650 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t651_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t651  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t651  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t651  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t651 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t580_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t580  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t580  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Keyframe_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t580  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t580 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t1584_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1584  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1584  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1584  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1584 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t665_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t665  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t665  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t665  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t665 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t1045_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1045  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1045  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1045  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1045 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t1099_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1099  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1099  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Mark_t1099 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1099  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1099 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t1135_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1135  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1135  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1135  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1135 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t1392_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1392  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1392  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_TableRange_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1392  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1392 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1469_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1469  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1469  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Slot_t1469 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1469  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1469 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1476_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1476  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1476  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Slot_t1476 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1476  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1476 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t225  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t225_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t225  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t225  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t163_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t327  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_DecimalU26_t2069 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t755 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2536  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2536  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2536  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2536 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2567_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2567  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2567  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2567  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2567 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2593_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2593  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2593  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2593  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2593 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2780_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2780  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2780  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2780  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2780 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2818_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2818  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2818  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2818  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2818 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2839_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2839  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2839  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2839  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2839 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2933_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2933  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2933  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2933  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2933 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2956  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2956  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2956  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2956 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Interval_t1106 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1106  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1106 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t369_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t362_QuaternionU26_t827 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t362  (*Func)(void* obj, Quaternion_t362 * p1, const MethodInfo* method);
	Quaternion_t362  ret = ((Func)method->method)(obj, (Quaternion_t362 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Matrix4x4U26_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Matrix4x4_t383 * p1, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, (Matrix4x4_t383 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1170_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1170  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1170  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t330_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t330  (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	TextGenerationSettings_t330  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t220_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t220  (*Func)(void* obj, Rect_t220  p1, const MethodInfo* method);
	Rect_t220  ret = ((Func)method->method)(obj, *((Rect_t220 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Vector2_t53  p1, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Color_t163  p1, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
void* RuntimeInvoker_Color32_t335_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t335  (*Func)(void* obj, Color_t163  p1, const MethodInfo* method);
	Color32_t335  ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t163_Color32_t335 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, Color32_t335  p1, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, *((Color32_t335 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t362_Quaternion_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t362  (*Func)(void* obj, Quaternion_t362  p1, const MethodInfo* method);
	Quaternion_t362  ret = ((Func)method->method)(obj, *((Quaternion_t362 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Matrix4x4_t383 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Matrix4x4_t383  p1, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((Matrix4x4_t383 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t346_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t346  (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	Ray_t346  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t330_TextGenerationSettings_t330 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t330  (*Func)(void* obj, TextGenerationSettings_t330  p1, const MethodInfo* method);
	TextGenerationSettings_t330  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t330 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t1831_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, TimeSpan_t1051  p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, TimeSpan_t1051  p1, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, DateTime_t508  p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t371_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t744_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t756 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_MonoMethodInfo_t1579_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1579  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1579  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t1571_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t1565_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t1211 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RaycastResult_t137_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t137  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t137  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
struct Object_t;
void* RuntimeInvoker_EditState_t212_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t138  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector4_t327_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t1065_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1052_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Category_t1091_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t1138_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t508_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t755_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t755  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t1879_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1371 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1371  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1371 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t1371_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1371  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1371  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1372  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1372 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t726  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t726 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1871  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1871 *)args[0]), method);
	return ret;
}

struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t1576_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1576  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1576  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t1591_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIVertex_t225_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t225  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t225  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ColorTween_t162 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t162  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t162 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UICharInfo_t377_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t377  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t377  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UILineInfo_t375_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t375  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t375  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2536_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2536  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2536  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2567_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2567  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2567  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2593  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2593  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2780_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2780  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2780  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2818_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2818  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2818  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2839_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2839  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2839  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2933_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2933  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2933  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2956_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2956  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2956  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Byte_t367_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int16_t448_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UInt16_t371_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt16_t371_UInt16_t371 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UInt16_t371_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t371_UInt16_t371_UInt16_t371 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_RegexOptionsU26_t1211_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t367_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ByteU26_t1360_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t1051  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_ByteU26_t1360_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint8_t*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UInt16U26_t2046_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t487;
#include "UnityEngine_ArrayTypes.h"
void* RuntimeInvoker_Void_t71_UserProfileU5BU5DU26_t821_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t487** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t487**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_RectU26_t823 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t220 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t220 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_DecimalU26_t2069_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t755 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t755 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t338;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t71_StringBuilderU26_t2387_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t338 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t338 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t29;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t29** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t29**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2493;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2493** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2493**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t224;
void* RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t224** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t224**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t708;
void* RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t708** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t708**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t709;
void* RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t709** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t709**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t71_GcAchievementDescriptionData_t649_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t649  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t649 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t71_GcUserProfileData_t648_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t648  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t648 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t220  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t220 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t327  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t327 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t665  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t665 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t137  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t137 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Link_t1459 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1459  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1459 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1147  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1147 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t349  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t349 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t323  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t323 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t53  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t53 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t375  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t375 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t377  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t377 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t138  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t138 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t650  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t650 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t651  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t651 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Keyframe_t580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t580  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t580 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1584  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1584 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1045  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1045 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Mark_t1099 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1099  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1099 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1135  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1135 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_TableRange_t1392 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1392  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1392 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Slot_t1469 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1469  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1469 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Slot_t1476 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1476  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1476 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t508  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t508 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t755  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t755 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t1051  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t225  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t225 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t1051  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int64_t753_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SingleU26_t831_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_SingleU26_t831_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2536  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2536 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2567  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2567 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2593 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2780  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2780 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2818  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2818 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2839  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2839 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2933  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2933 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2956  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2956 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ColorU26_t450_SphericalHarmonicsL2U26_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163 * p1, SphericalHarmonicsL2_t541 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t163 *)args[0], (SphericalHarmonicsL2_t541 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Matrix4x4U26_t828_Matrix4x4U26_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t383 * p1, Matrix4x4_t383 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t383 *)args[0], (Matrix4x4_t383 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_BoundsU26_t829_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t247 * p1, Vector3_t138 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t247 *)args[0], (Vector3_t138 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163_SphericalHarmonicsL2U26_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, SphericalHarmonicsL2_t541 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), (SphericalHarmonicsL2_t541 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2U26_t825_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53 * p1, Vector2_t53  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t53 *)args[0], *((Vector2_t53 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_ColorU26_t450_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t163 * p1, Color_t163  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t163 *)args[0], *((Color_t163 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Matrix4x4_t383_Matrix4x4U26_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t383  p1, Matrix4x4_t383 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t383 *)args[0]), (Matrix4x4_t383 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3_t138_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Ray_t346_SingleU26_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t346  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t346 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t508  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_NavigationU26_t3380_Navigation_t232 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t232 * p1, Navigation_t232  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t232 *)args[0], *((Navigation_t232 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_ColorBlockU26_t3381_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t175 * p1, ColorBlock_t175  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t175 *)args[0], *((ColorBlock_t175 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_SpriteStateU26_t3382_SpriteState_t252 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t252 * p1, SpriteState_t252  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t252 *)args[0], *((SpriteState_t252 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t72_BoneWeight_t493_BoneWeight_t493 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t493  p1, BoneWeight_t493  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t493 *)args[0]), *((BoneWeight_t493 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector3_t138_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Quaternion_t362_Quaternion_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t362  p1, Quaternion_t362  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t362 *)args[0]), *((Quaternion_t362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Rect_t220_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t220  p1, Rect_t220  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t220 *)args[0]), *((Rect_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Matrix4x4_t383_Matrix4x4_t383 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t383  p1, Matrix4x4_t383  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t383 *)args[0]), *((Matrix4x4_t383 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Bounds_t247_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t247  p1, Vector3_t138  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t247 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Bounds_t247_Bounds_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t247  p1, Bounds_t247  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t247 *)args[0]), *((Bounds_t247 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector4_t327_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t327  p1, Vector4_t327  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), *((Vector4_t327 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_SphericalHarmonicsL2_t541_SphericalHarmonicsL2_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t541  p1, SphericalHarmonicsL2_t541  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t541 *)args[0]), *((SphericalHarmonicsL2_t541 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_HitInfo_t665_HitInfo_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t665  p1, HitInfo_t665  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t665 *)args[0]), *((HitInfo_t665 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Color_t163_Color_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t163  p1, Color_t163  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), *((Color_t163 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Decimal_t755_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t755  p1, Decimal_t755  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), *((Decimal_t755 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t508  p1, DateTime_t508  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((DateTime_t508 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1051  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_RaycastResult_t137_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t137  p1, RaycastResult_t137  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), *((RaycastResult_t137 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UIVertex_t225_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t225  p1, UIVertex_t225  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), *((UIVertex_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UICharInfo_t377_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t377  p1, UICharInfo_t377  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), *((UICharInfo_t377 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_UILineInfo_t375_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t375  p1, UILineInfo_t375  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), *((UILineInfo_t375 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_DateTimeOffset_t768_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t768  p1, DateTimeOffset_t768  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t768 *)args[0]), *((DateTimeOffset_t768 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Guid_t769_Guid_t769 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t769  p1, Guid_t769  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t769 *)args[0]), *((Guid_t769 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t369_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t367_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2956  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2956  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t752_Double_t752_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_UInt64U26_t2035 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_Int64U26_t2029 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_DecimalU26_t2069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, Decimal_t755 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], (Decimal_t755 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_Double_t752_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t133_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t163_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_MonoMethodInfoU26_t2374 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1579 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1579 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t137  p1, RaycastResult_t137  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), *((RaycastResult_t137 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastHit_t323_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t323  p1, RaycastHit_t323  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t323 *)args[0]), *((RaycastHit_t323 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Double_t752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Decimal_t755_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755  p1, Decimal_t755  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), *((Decimal_t755 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t448_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_IntPtr_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t508  p1, DateTime_t508  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((DateTime_t508 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_DateTime_t508_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, DateTime_t508  p1, int32_t p2, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1051  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t225  p1, UIVertex_t225  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), *((UIVertex_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t377  p1, UICharInfo_t377  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), *((UICharInfo_t377 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t375  p1, UILineInfo_t375  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), *((UILineInfo_t375 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DateTimeOffset_t768_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t768  p1, DateTimeOffset_t768  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t768 *)args[0]), *((DateTimeOffset_t768 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Guid_t769_Guid_t769 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t769  p1, Guid_t769  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t769 *)args[0]), *((Guid_t769 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_BoundsU26_t829_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t247 * p1, Vector3_t138 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t247 *)args[0], (Vector3_t138 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2593  p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2593 *)args[0]), *((KeyValuePair_2_t2593 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t53 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t53 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_ColorU26_t450 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t163 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t163 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct UriFormatException_t1137;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t71_Object_t_UriFormatExceptionU26_t1213 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1137 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1137 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t744_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t29;
struct Object_t;
void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t29** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t29**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_MonoEventInfoU26_t2373 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1576 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1576 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_MonoEnumInfoU26_t2402 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1842 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1842 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_ObjectU26_t863_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector2_t53_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector2_t53  p1, float p2, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector3_t138_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Vector3_t138_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Vector3_t138  p1, float p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t163_Color_t163_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, Color_t163  p1, float p2, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Quaternion_t362_Quaternion_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t362  p1, Quaternion_t362  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t362 *)args[0]), *((Quaternion_t362 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Bounds_t247_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t247  p1, Vector3_t138  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t247 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Vector4_t327_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t327  p1, Vector4_t327  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), *((Vector4_t327 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Vector4_t327_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Vector4_t327  p1, float p2, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t541_SphericalHarmonicsL2_t541_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t541  (*Func)(void* obj, SphericalHarmonicsL2_t541  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t541  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t541 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t541_Single_t85_SphericalHarmonicsL2_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t541  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t541  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t541  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t541 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t726  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t726 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct IPAddress_t1012;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t72_Object_t_IPAddressU26_t1207 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1012 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1012 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t1013;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t72_Object_t_IPv6AddressU26_t1208 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1013 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1013 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int64U26_t2029 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_UInt32U26_t2032 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_UInt64U26_t2035 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_ByteU26_t1360 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_SByteU26_t2040 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int16U26_t2043 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_UInt16U26_t2046 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_DoubleU26_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1372  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1372 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t508  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t508 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t753_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t755  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t755 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_ObjectU26_t863_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_KeyValuePair_2U26_t3387 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (KeyValuePair_2_t2593 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t53  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_TextGenerationSettings_t330 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t330  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t330 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_DateTime_t508_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t508  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t753_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_BoundsU26_t829_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Bounds_t247 * p1, Vector3_t138 * p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, (Bounds_t247 *)args[0], (Vector3_t138 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t752_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_IntPtr_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Vector4_t327_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Vector4_t327  p1, Rect_t220  p2, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), *((Rect_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector2_t53_Rect_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector2_t53  p1, Rect_t220  p2, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Rect_t220 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Vector3_t138_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Quaternion_t362_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Quaternion_t362  p1, Vector3_t138  p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Quaternion_t362 *)args[0]), *((Vector3_t138 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Matrix4x4_t383_Matrix4x4_t383 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Matrix4x4_t383  p1, Matrix4x4_t383  p2, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((Matrix4x4_t383 *)args[0]), *((Matrix4x4_t383 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Matrix4x4_t383_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Matrix4x4_t383  p1, Vector4_t327  p2, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((Matrix4x4_t383 *)args[0]), *((Vector4_t327 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t327_Vector4_t327_Vector4_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t327  (*Func)(void* obj, Vector4_t327  p1, Vector4_t327  p2, const MethodInfo* method);
	Vector4_t327  ret = ((Func)method->method)(obj, *((Vector4_t327 *)args[0]), *((Vector4_t327 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t541_SphericalHarmonicsL2_t541_SphericalHarmonicsL2_t541 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t541  (*Func)(void* obj, SphericalHarmonicsL2_t541  p1, SphericalHarmonicsL2_t541  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t541  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t541 *)args[0]), *((SphericalHarmonicsL2_t541 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t755_Decimal_t755_Decimal_t755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, Decimal_t755  p1, Decimal_t755  p2, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, *((Decimal_t755 *)args[0]), *((Decimal_t755 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t508_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, DateTime_t508  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, TimeSpan_t1051  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((TimeSpan_t1051 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1051_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1051  (*Func)(void* obj, DateTime_t508  p1, TimeSpan_t1051  p2, const MethodInfo* method);
	TimeSpan_t1051  ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((TimeSpan_t1051 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t29;
void* RuntimeInvoker_Int32_t54_Object_t_ObjectU5BU5DU26_t2023 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t29** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t29**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t53  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
struct Object_t;
void* RuntimeInvoker_GCHandle_t1609_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1609  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1609  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2933_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2933  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2933  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t_TextGenerationSettings_t330 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t330  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t330 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t1521_IntPtr_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2536  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2536  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2567_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2567  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2567  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t369_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2780_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2780  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	KeyValuePair_2_t2780  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2818_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2818  (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2818  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t220_Object_t_RectU26_t823 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t220  (*Func)(void* obj, Object_t * p1, Rect_t220 * p2, const MethodInfo* method);
	Rect_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t220 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t138_Object_t_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Object_t * p1, Vector3_t138 * p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t138 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t346_Object_t_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t346  (*Func)(void* obj, Object_t * p1, Vector3_t138 * p2, const MethodInfo* method);
	Ray_t346  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t138 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t367_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t1512_Object_t_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t138_Object_t_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t346_Object_t_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t346  (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, const MethodInfo* method);
	Ray_t346  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t448_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t371_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t744_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t756_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t752_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2839_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2839  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	KeyValuePair_2_t2839  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2593  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	KeyValuePair_2_t2593  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t138 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t138 *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t978;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t978 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t978 **)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t220_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t220  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t220  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1221_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1170 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1170  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1170 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t755_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1417_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t508_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1147  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1147  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2593  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2593  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int16_t448_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct PointerEventData_t143;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
void* RuntimeInvoker_Boolean_t72_Int32_t54_PointerEventDataU26_t440_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t143 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t143 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int16_t448_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t42;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t72_Int32_t54_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t42 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t42 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t744_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ObjectU5BU5D_t29;
void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t29** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t29**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2493;
void* RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2493** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2493**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t224;
void* RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t224** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t224**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t708;
void* RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t708** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t708**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t709;
void* RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t709** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t709**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Void_t71_SByte_t73_ByteU5BU5DU26_t1361_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t546** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t546**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_SByte_t73_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_IntPtr_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Byte_t367_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Byte_t367_Byte_t367 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32U26_t449_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t756_Int64_t753_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t1912;
struct StringU5BU5D_t45;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int64U5BU5DU26_t2416_StringU5BU5DU26_t2417 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1912** p2, StringU5BU5D_t45** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1912**)args[1], (StringU5BU5D_t45**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t369_Object_t_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3_t138_Color_t163_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138  p1, Color_t163  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Color_t163 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3U26_t822_ColorU26_t450_SphericalHarmonicsL2U26_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138 * p1, Color_t163 * p2, SphericalHarmonicsL2_t541 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t138 *)args[0], (Color_t163 *)args[1], (SphericalHarmonicsL2_t541 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_RayU26_t830_BoundsU26_t829_SingleU26_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t346 * p1, Bounds_t247 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t346 *)args[0], (Bounds_t247 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Double_t752_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3_t138_Color_t163_SphericalHarmonicsL2U26_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138  p1, Color_t163  p2, SphericalHarmonicsL2_t541 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Color_t163 *)args[1]), (SphericalHarmonicsL2_t541 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector3_t138_Quaternion_t362_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t138  p1, Quaternion_t362  p2, Vector3_t138  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Quaternion_t362 *)args[1]), *((Vector3_t138 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_DateTime_t508_DateTime_t508_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t508  p1, DateTime_t508  p2, TimeSpan_t1051  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((DateTime_t508 *)args[1]), *((TimeSpan_t1051 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_IntPtr_t_Int64_t753_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t133_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UserProfileU5BU5D_t487;
struct Object_t;
void* RuntimeInvoker_Void_t71_UserProfileU5BU5DU26_t821_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t487** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t487**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t71_Object_t_MonoPropertyInfoU26_t2375_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1580 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1580 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_AnimatorStateInfo_t578_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t578  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t578 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_DecimalU26_t2069_DecimalU26_t2069 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, Decimal_t755 * p2, Decimal_t755 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], (Decimal_t755 *)args[1], (Decimal_t755 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_RenderBufferU26_t834_RenderBufferU26_t834 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t653 * p2, RenderBuffer_t653 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t653 *)args[1], (RenderBuffer_t653 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1052_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t753_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Void_t71_Object_t_StringU26_t882_StringU26_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t546;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Void_t71_Object_t_ByteU5BU5DU26_t1361_ByteU5BU5DU26_t1361 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t546** p2, ByteU5BU5D_t546** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t546**)args[1], (ByteU5BU5D_t546**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t72_Object_t_MonoIOStatU26_t2371_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1520 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1520 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t138_Vector3_t138_Vector3_t138_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t138  (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, float p3, const MethodInfo* method);
	Vector3_t138  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t163_Color_t163_Color_t163_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t163  (*Func)(void* obj, Color_t163  p1, Color_t163  p2, float p3, const MethodInfo* method);
	Color_t163  ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), *((Color_t163 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t369_Object_t_Int32U26_t449_CharU26_t1212 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Vector3U26_t822_QuaternionU26_t827_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Vector3_t138 * p1, Quaternion_t362 * p2, Vector3_t138 * p3, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, (Vector3_t138 *)args[0], (Quaternion_t362 *)args[1], (Vector3_t138 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t346_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t346  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t346 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Vector3_t138_Quaternion_t362_Vector3_t138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, Vector3_t138  p1, Quaternion_t362  p2, Vector3_t138  p3, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Quaternion_t362 *)args[1]), *((Vector3_t138 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t73_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Object_t_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t53 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t53 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t367_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t53_Rect_t220_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Rect_t220  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Rect_t220 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t448_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t137  p1, RaycastResult_t137  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), *((RaycastResult_t137 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t225  p1, UIVertex_t225  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), *((UIVertex_t225 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t377  p1, UICharInfo_t377  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), *((UICharInfo_t377 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t375  p1, UILineInfo_t375  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), *((UILineInfo_t375 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t371_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t73_Object_t_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t826_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t527 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t527 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t744_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t29;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t29** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t29**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Vector2U26_t825_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t53 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t53 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t756_UInt16_t371_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, uint16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t29;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_ObjectU5BU5DU26_t2023 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t29** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t29**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t753_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t726  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t726 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t726  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t726 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_IntPtr_t_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t42 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t42 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Touch_t322_BooleanU26_t441_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t322  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t322 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1052_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t752_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t755_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t755  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t755  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t508_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Module_t1549;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t449_ModuleU26_t2372 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1549 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1549 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t449_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t1657;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t726_ISurrogateSelectorU26_t2384 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t726  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t726 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t53_Vector2_t53_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t53  (*Func)(void* obj, Vector2_t53  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t53  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t371_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t756_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t756_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t85_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t216;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2075 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t216 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t216 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t42 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t42 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t1106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1106  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1106 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t726_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t726  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t726 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t508_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t508  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t755_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t755  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t755 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t137_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t137  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t163_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t163  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t163 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t225  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t53_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t53  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t377_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t377  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t375_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t375  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t1086_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163_Single_t85_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32U26_t449_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector2_t53_Vector2_t53_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_UInt16_t371 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Ray_t346_RaycastHitU26_t851_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t346  p1, RaycastHit_t323 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t346 *)args[0]), (RaycastHit_t323 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_SByte_t73_Object_t_Int32_t54_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32U26_t449_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_CharU26_t1212_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Single_t85_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t1886;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1886** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1886**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73_Int32U26_t449_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73_Int64U26_t2029_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73_UInt32U26_t2032_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73_SByteU26_t2040_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73_Int16U26_t2043_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t42 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t42 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_DecimalU26_t2069_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t755 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t755 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_IntPtr_t_Int64_t753_Int32_t54_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_RaycastResult_t137_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t137  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t137 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_UIVertex_t225_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t225  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t225 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_UICharInfo_t377_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t377  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t377 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_UILineInfo_t375_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t375  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t375 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t349_Vector2_t53_Vector2_t53_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t349  (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t349  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_IntPtr_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t73_SByte_t73_Object_t_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Single_t85_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t725;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t725 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t725 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t822_Vector3U26_t822_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t138 * p1, Vector3_t138 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t138 *)args[0], (Vector3_t138 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t138_Vector3_t138_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t53_Vector2_t53_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_Int64U26_t2029 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_UInt32U26_t2032 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_ByteU26_t1360 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_UInt16U26_t2046 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_DoubleU26_t2066 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2U26_t825_Object_t_Object_t_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53 * p1, Object_t * p2, Object_t * p3, Vector2_t53 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t53 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t53 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Object_t_Object_t_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t1409;
struct Level2MapU5BU5D_t1410;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_ContractionU5BU5DU26_t2233_Level2MapU5BU5DU26_t2234 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1409** p3, Level2MapU5BU5D_t1410** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1409**)args[2], (Level2MapU5BU5D_t1410**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2_t53_Object_t_Object_t_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53  p1, Object_t * p2, Object_t * p3, Vector2_t53 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t53 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t53 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Vector2_t53_Object_t_Vector3U26_t822 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, Object_t * p3, Vector3_t138 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), (Object_t *)args[2], (Vector3_t138 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Vector2_t53_Object_t_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t53  p2, Object_t * p3, Vector2_t53 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t53 *)args[1]), (Object_t *)args[2], (Vector2_t53 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t830_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t346 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t346 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_Object_t_BooleanU26_t441_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ILayoutElement_t333;
void* RuntimeInvoker_Single_t85_Object_t_Object_t_Single_t85_ILayoutElementU26_t455 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Single_t85_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Vector2_t53_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t53  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t53 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t508_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t508  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t508  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t137_RaycastResult_t137_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t137  p1, RaycastResult_t137  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t137 *)args[0]), *((RaycastResult_t137 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastHit_t323_RaycastHit_t323_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t323  p1, RaycastHit_t323  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t323 *)args[0]), *((RaycastHit_t323 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t225_UIVertex_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t225  p1, UIVertex_t225  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t225 *)args[0]), *((UIVertex_t225 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t377_UICharInfo_t377_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t377  p1, UICharInfo_t377  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t377 *)args[0]), *((UICharInfo_t377 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t375_UILineInfo_t375_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t375  p1, UILineInfo_t375  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t375 *)args[0]), *((UILineInfo_t375 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t508_Nullable_1_t1924_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t508  p1, Nullable_1_t1924  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t508 *)args[0]), *((Nullable_1_t1924 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t753_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t726_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t726  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t726 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2593  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2593 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Color_t163_Single_t85_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t163  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t163 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t753_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Single_t85_Single_t85_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Double_t752_SByte_t73_SByte_t73_DateTime_t508 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t508  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t508 *)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector3U26_t822_Vector3U26_t822_RaycastHitU26_t851_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t138 * p1, Vector3_t138 * p2, RaycastHit_t323 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t138 *)args[0], (Vector3_t138 *)args[1], (RaycastHit_t323 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Object_t_SByte_t73_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t42 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t42 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t1886;
void* RuntimeInvoker_Void_t71_Byte_t367_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1886** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1886**)args[4], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t72_Vector3_t138_Vector3_t138_RaycastHitU26_t851_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t138  p1, Vector3_t138  p2, RaycastHit_t323 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t138 *)args[0]), *((Vector3_t138 *)args[1]), (RaycastHit_t323 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_CharU26_t1212_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Single_t85_Single_t85_Single_t85_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441_SByte_t73_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Single_t85_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Object_t_Int32U26_t449_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t42 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t42 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t725;
void* RuntimeInvoker_Void_t71_Byte_t367_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t725 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t725 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_DecimalU26_t2069_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t755 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t755 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Object_t_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_IntPtr_t_Object_t_Int32_t54_Int32_t54_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_RectU26_t823_Object_t_Int32_t54_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t220 * p2, Object_t * p3, int32_t p4, Vector2_t53 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t220 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t53 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Void_t71_Object_t_Int32U26_t449_ByteU26_t1360_Int32U26_t449_ByteU5BU5DU26_t1361 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t546** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t546**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Rect_t220_Object_t_Int32_t54_Vector2U26_t825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t220  p2, Object_t * p3, int32_t p4, Vector2_t53 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t220 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t53 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_Object_t_BooleanU26_t441_BooleanU26_t441_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t725;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t725 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t725 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32U26_t449_Object_t_Object_t_BooleanU26_t441_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Int32_t54_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int16_t448_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_SByte_t73_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t725;
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t725 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t725 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Color32_t335_Int32_t54_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t335  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t335 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t85_Single_t85_Single_t85_SingleU26_t831_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t383_Single_t85_Single_t85_Single_t85_Single_t85_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t383  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t383  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Int32_t54_Int32_t54_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1403 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1403 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t349_Vector2_t53_Vector2_t53_Single_t85_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t349  (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t349  ret = ((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt64U2AU26_t2408_Int32U2AU26_t2409_CharU2AU26_t2410_CharU2AU26_t2410_Int64U2AU26_t2411_Int32U2AU26_t2409 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73_SByte_t73_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_MonoIOErrorU26_t2370 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Int32_t54_CharU26_t1212_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_Int32U26_t449_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t42 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t42 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_Int64U26_t2029_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t42 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t42 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_UInt32U26_t2032_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t42 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t42 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_UInt64U26_t2035_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t42 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t42 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Object_t_SByte_t73_DoubleU26_t2066_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t42 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t42 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t825_Vector2U26_t825_Single_t85_Int32_t54_Single_t85_Single_t85 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t53 * p1, Vector2_t53 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t53 *)args[0], (Vector2_t53 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_DecoderFallbackBufferU26_t2390 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1773 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1773 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32U26_t449_Int32U26_t449_Int32U26_t449_BooleanU26_t441_StringU26_t882 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t1394;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t1394;
void* RuntimeInvoker_Void_t71_Object_t_CodePointIndexerU26_t2235_ByteU2AU26_t2052_ByteU2AU26_t2052_CodePointIndexerU26_t2235_ByteU2AU26_t2052 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1394 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1394 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1394 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1394 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_UIVertex_t225_Vector2_t53_Vector2_t53_Vector2_t53_Vector2_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t225  p2, Vector2_t53  p3, Vector2_t53  p4, Vector2_t53  p5, Vector2_t53  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t225 *)args[1]), *((Vector2_t53 *)args[2]), *((Vector2_t53 *)args[3]), *((Vector2_t53 *)args[4]), *((Vector2_t53 *)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_Object_t_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1773 ** p2, ByteU5BU5D_t546** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1773 **)args[1], (ByteU5BU5D_t546**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Object_t_DateTime_t508_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t508  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t508 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Object_t_SByte_t73_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54_Object_t_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1403 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1403 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t_Object_t_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753_Object_t_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_SByte_t73_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_UInt32U26_t2032_Int32_t54_UInt32U26_t2032_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_SByte_t73_SByte_t73_SByte_t73_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2U26_t825_Vector2U26_t825_Single_t85_Int32_t54_Single_t85_Single_t85_RaycastHit2DU26_t852 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53 * p1, Vector2_t53 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t349 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t53 *)args[0], (Vector2_t53 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t349 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Vector2_t53_Vector2_t53_Single_t85_Int32_t54_Single_t85_Single_t85_RaycastHit2DU26_t852 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t53  p1, Vector2_t53  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t349 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t53 *)args[0]), *((Vector2_t53 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t349 *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_SByte_t73_SByte_t73_SByte_t73_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_CharU26_t1212_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Object_t_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1403 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1403 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_ByteU2AU26_t2052_ByteU2AU26_t2052_DoubleU2AU26_t2053_UInt16U2AU26_t2054_UInt16U2AU26_t2054_UInt16U2AU26_t2054_UInt16U2AU26_t2054 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Int32_t54_DateTimeU26_t2392_DateTimeOffsetU26_t2393_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t508 * p4, DateTimeOffset_t768 * p5, int8_t p6, Exception_t42 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t508 *)args[3], (DateTimeOffset_t768 *)args[4], *((int8_t*)args[5]), (Exception_t42 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t1782;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t222;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_EncoderFallbackBufferU26_t2388_CharU5BU5DU26_t2389 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1782 ** p6, CharU5BU5D_t222** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1782 **)args[5], (CharU5BU5D_t222**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t_Int32_t54_Single_t85_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int64_t753_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t29;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_ObjectU5BU5DU26_t2023_Object_t_Object_t_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t29** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t29**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct MethodBase_t47;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t72_Int32_t54_SByte_t73_MethodBaseU26_t2369_Int32U26_t449_Int32U26_t449_StringU26_t882_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t47 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t47 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int16_t448_Int32_t54_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1403 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1403 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Object_t_Int32_t54_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1403 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1403 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Int32_t54_Object_t_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1403 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1403 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1773 ** p6, ByteU5BU5D_t546** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1773 **)args[5], (ByteU5BU5D_t546**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_Object_t_Int64_t753_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1773 ** p2, ByteU5BU5D_t546** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1773 **)args[1], (ByteU5BU5D_t546**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Int32U26_t449_BooleanU26_t441_BooleanU26_t441_Int32U26_t449_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_SByte_t73_Int32U26_t449_BooleanU26_t441_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54_BooleanU26_t441_BooleanU26_t441_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1773 ** p7, ByteU5BU5D_t546** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1773 **)args[6], (ByteU5BU5D_t546**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1396;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Object_t_SByte_t73_Int32_t54_ContractionU26_t2237_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1396 ** p8, Context_t1403 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1396 **)args[7], (Context_t1403 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t42;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Int32_t54_DateTimeU26_t2392_SByte_t73_BooleanU26_t441_SByte_t73_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t508 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t42 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t508 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t42 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1396;
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Int32_t54_Object_t_SByte_t73_Int32_t54_ContractionU26_t2237_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1396 ** p9, Context_t1403 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1396 **)args[8], (Context_t1403 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t_Int32_t54_UInt32U26_t2032_UInt32U26_t2032_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1773 ** p8, ByteU5BU5D_t546** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1773 **)args[7], (ByteU5BU5D_t546**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_Int32_t54_BooleanU26_t441_BooleanU26_t441_SByte_t73_SByte_t73_ContextU26_t2236 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1403 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1403 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t1773;
struct ByteU5BU5D_t546;
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Int32_t54_UInt32U26_t2032_UInt32U26_t2032_Object_t_DecoderFallbackBufferU26_t2390_ByteU5BU5DU26_t1361_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1773 ** p9, ByteU5BU5D_t546** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1773 **)args[8], (ByteU5BU5D_t546**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_SByte_t73_DateTimeU26_t2392_DateTimeOffsetU26_t2393_Object_t_Int32_t54_SByte_t73_BooleanU26_t441_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t508 * p5, DateTimeOffset_t768 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t508 *)args[4], (DateTimeOffset_t768 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int16_t448_Object_t_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73_SByte_t73_SByte_t73_Int16_t448_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t448_Object_t_Int32_t54_Int32_t54_Int32_t54_SByte_t73_SByte_t73_SByte_t73_SByte_t73_Int16_t448_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Color_t163_Int32_t54_Single_t85_Single_t85_Int32_t54_SByte_t73_SByte_t73_Int32_t54_Int32_t54_Int32_t54_Int32_t54_SByte_t73_Int32_t54_Vector2_t53_Vector2_t53_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t163  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t53  p16, Vector2_t53  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t163 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t53 *)args[15]), *((Vector2_t53 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Color_t163_Int32_t54_Single_t85_Single_t85_Int32_t54_SByte_t73_SByte_t73_Int32_t54_Int32_t54_Int32_t54_Int32_t54_SByte_t73_Int32_t54_Single_t85_Single_t85_Single_t85_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t163  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t163 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_ColorU26_t450_Int32_t54_Single_t85_Single_t85_Int32_t54_SByte_t73_SByte_t73_Int32_t54_Int32_t54_Int32_t54_Int32_t54_SByte_t73_Int32_t54_Single_t85_Single_t85_Single_t85_Single_t85_SByte_t73 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t163 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t163 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

