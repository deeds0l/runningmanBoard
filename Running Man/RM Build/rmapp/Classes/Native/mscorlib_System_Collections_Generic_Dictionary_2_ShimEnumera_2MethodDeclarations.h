﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t2791;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2779;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16289_gshared (ShimEnumerator_t2791 * __this, Dictionary_2_t2779 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16289(__this, ___host, method) (( void (*) (ShimEnumerator_t2791 *, Dictionary_2_t2779 *, const MethodInfo*))ShimEnumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16290_gshared (ShimEnumerator_t2791 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16290(__this, method) (( bool (*) (ShimEnumerator_t2791 *, const MethodInfo*))ShimEnumerator_MoveNext_m16290_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C" DictionaryEntry_t1147  ShimEnumerator_get_Entry_m16291_gshared (ShimEnumerator_t2791 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16291(__this, method) (( DictionaryEntry_t1147  (*) (ShimEnumerator_t2791 *, const MethodInfo*))ShimEnumerator_get_Entry_m16291_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16292_gshared (ShimEnumerator_t2791 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16292(__this, method) (( Object_t * (*) (ShimEnumerator_t2791 *, const MethodInfo*))ShimEnumerator_get_Key_m16292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16293_gshared (ShimEnumerator_t2791 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16293(__this, method) (( Object_t * (*) (ShimEnumerator_t2791 *, const MethodInfo*))ShimEnumerator_get_Value_m16293_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16294_gshared (ShimEnumerator_t2791 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16294(__this, method) (( Object_t * (*) (ShimEnumerator_t2791 *, const MethodInfo*))ShimEnumerator_get_Current_m16294_gshared)(__this, method)
