﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.BaseRaycaster>
struct IList_1_t2505;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseRaycaster>
struct  ReadOnlyCollection_1_t2506  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseRaycaster>::list
	Object_t* ___list_0;
};
