﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t1903;
struct U24ArrayTypeU24256_t1903_marshaled;

void U24ArrayTypeU24256_t1903_marshal(const U24ArrayTypeU24256_t1903& unmarshaled, U24ArrayTypeU24256_t1903_marshaled& marshaled);
void U24ArrayTypeU24256_t1903_marshal_back(const U24ArrayTypeU24256_t1903_marshaled& marshaled, U24ArrayTypeU24256_t1903& unmarshaled);
void U24ArrayTypeU24256_t1903_marshal_cleanup(U24ArrayTypeU24256_t1903_marshaled& marshaled);
