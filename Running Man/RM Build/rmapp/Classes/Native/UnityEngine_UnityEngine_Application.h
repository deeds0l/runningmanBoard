﻿#pragma once
#include <stdint.h>
// UnityEngine.Application/LogCallback
struct LogCallback_t551;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Application
struct  Application_t552  : public Object_t
{
};
struct Application_t552_StaticFields{
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandler
	LogCallback_t551 * ___s_LogCallbackHandler_0;
	// UnityEngine.Application/LogCallback UnityEngine.Application::s_LogCallbackHandlerThreaded
	LogCallback_t551 * ___s_LogCallbackHandlerThreaded_1;
};
