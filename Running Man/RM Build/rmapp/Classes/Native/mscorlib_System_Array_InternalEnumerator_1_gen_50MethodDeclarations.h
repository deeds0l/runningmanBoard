﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>
struct InternalEnumerator_1_t2894;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17644_gshared (InternalEnumerator_1_t2894 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17644(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2894 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17644_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17645_gshared (InternalEnumerator_1_t2894 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17645(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2894 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17645_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17646_gshared (InternalEnumerator_1_t2894 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17646(__this, method) (( void (*) (InternalEnumerator_1_t2894 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17646_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17647_gshared (InternalEnumerator_1_t2894 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17647(__this, method) (( bool (*) (InternalEnumerator_1_t2894 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17647_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern "C" ParameterModifier_t1584  InternalEnumerator_1_get_Current_m17648_gshared (InternalEnumerator_1_t2894 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17648(__this, method) (( ParameterModifier_t1584  (*) (InternalEnumerator_1_t2894 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17648_gshared)(__this, method)
