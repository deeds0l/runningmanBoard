﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t420  : public PropertyAttribute_t672
{
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;
};
