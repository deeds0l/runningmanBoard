﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2775;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m16101_gshared (DefaultComparer_t2775 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16101(__this, method) (( void (*) (DefaultComparer_t2775 *, const MethodInfo*))DefaultComparer__ctor_m16101_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16102_gshared (DefaultComparer_t2775 * __this, UILineInfo_t375  ___x, UILineInfo_t375  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16102(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2775 *, UILineInfo_t375 , UILineInfo_t375 , const MethodInfo*))DefaultComparer_Compare_m16102_gshared)(__this, ___x, ___y, method)
