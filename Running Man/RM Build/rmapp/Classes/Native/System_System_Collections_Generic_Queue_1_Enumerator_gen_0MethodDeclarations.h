﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>
struct Enumerator_t2463;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
struct Queue_1_t7;

// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::.ctor(System.Collections.Generic.Queue`1<T>)
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m11732(__this, ___q, method) (( void (*) (Enumerator_t2463 *, Queue_1_t7 *, const MethodInfo*))Enumerator__ctor_m11727_gshared)(__this, ___q, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11733(__this, method) (( Object_t * (*) (Enumerator_t2463 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11728_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::Dispose()
#define Enumerator_Dispose_m11734(__this, method) (( void (*) (Enumerator_t2463 *, const MethodInfo*))Enumerator_Dispose_m11729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::MoveNext()
#define Enumerator_MoveNext_m11735(__this, method) (( bool (*) (Enumerator_t2463 *, const MethodInfo*))Enumerator_MoveNext_m11730_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::get_Current()
#define Enumerator_get_Current_m11736(__this, method) (( AudioSource_t9 * (*) (Enumerator_t2463 *, const MethodInfo*))Enumerator_get_Current_m11731_gshared)(__this, method)
