﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t787;
// UnityEngine.Object
struct Object_t33;
struct Object_t33_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t29;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m3520(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t787 *, Object_t33 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m17792_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m17793(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t787 *, ObjectU5BU5D_t29*, const MethodInfo*))CachedInvokableCall_1_Invoke_m17794_gshared)(__this, ___args, method)
