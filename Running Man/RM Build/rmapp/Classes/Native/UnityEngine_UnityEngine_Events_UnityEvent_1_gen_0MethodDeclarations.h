﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t161;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t324;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t682;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C" void UnityEvent_1__ctor_m1586_gshared (UnityEvent_1_t161 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m1586(__this, method) (( void (*) (UnityEvent_1_t161 *, const MethodInfo*))UnityEvent_1__ctor_m1586_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m1589_gshared (UnityEvent_1_t161 * __this, UnityAction_1_t324 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m1589(__this, ___call, method) (( void (*) (UnityEvent_1_t161 *, UnityAction_1_t324 *, const MethodInfo*))UnityEvent_1_AddListener_m1589_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m13054_gshared (UnityEvent_1_t161 * __this, UnityAction_1_t324 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m13054(__this, ___call, method) (( void (*) (UnityEvent_1_t161 *, UnityAction_1_t324 *, const MethodInfo*))UnityEvent_1_RemoveListener_m13054_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2118_gshared (UnityEvent_1_t161 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m2118(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t161 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2118_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t682 * UnityEvent_1_GetDelegate_m2119_gshared (UnityEvent_1_t161 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m2119(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t682 * (*) (UnityEvent_1_t161 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m2119_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t682 * UnityEvent_1_GetDelegate_m13055_gshared (Object_t * __this /* static, unused */, UnityAction_1_t324 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m13055(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t682 * (*) (Object_t * /* static, unused */, UnityAction_1_t324 *, const MethodInfo*))UnityEvent_1_GetDelegate_m13055_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m1588_gshared (UnityEvent_1_t161 * __this, Color_t163  ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m1588(__this, ___arg0, method) (( void (*) (UnityEvent_1_t161 *, Color_t163 , const MethodInfo*))UnityEvent_1_Invoke_m1588_gshared)(__this, ___arg0, method)
