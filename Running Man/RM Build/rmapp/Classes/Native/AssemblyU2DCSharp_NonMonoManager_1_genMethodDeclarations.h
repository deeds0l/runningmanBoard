﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// NonMonoManager`1<System.Object>
struct NonMonoManager_1_t2470;
// System.Object
struct Object_t;

// System.Void NonMonoManager`1<System.Object>::.ctor()
extern "C" void NonMonoManager_1__ctor_m11787_gshared (NonMonoManager_1_t2470 * __this, const MethodInfo* method);
#define NonMonoManager_1__ctor_m11787(__this, method) (( void (*) (NonMonoManager_1_t2470 *, const MethodInfo*))NonMonoManager_1__ctor_m11787_gshared)(__this, method)
// System.Void NonMonoManager`1<System.Object>::.cctor()
extern "C" void NonMonoManager_1__cctor_m11788_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define NonMonoManager_1__cctor_m11788(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))NonMonoManager_1__cctor_m11788_gshared)(__this /* static, unused */, method)
// T NonMonoManager`1<System.Object>::get_Instance()
extern "C" Object_t * NonMonoManager_1_get_Instance_m11789_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define NonMonoManager_1_get_Instance_m11789(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))NonMonoManager_1_get_Instance_m11789_gshared)(__this /* static, unused */, method)
// System.Void NonMonoManager`1<System.Object>::Create()
extern "C" void NonMonoManager_1_Create_m11790_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define NonMonoManager_1_Create_m11790(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))NonMonoManager_1_Create_m11790_gshared)(__this /* static, unused */, method)
// System.Void NonMonoManager`1<System.Object>::Destroy()
extern "C" void NonMonoManager_1_Destroy_m11791_gshared (NonMonoManager_1_t2470 * __this, const MethodInfo* method);
#define NonMonoManager_1_Destroy_m11791(__this, method) (( void (*) (NonMonoManager_1_t2470 *, const MethodInfo*))NonMonoManager_1_Destroy_m11791_gshared)(__this, method)
