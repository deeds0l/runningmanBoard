﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t265;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct  Predicate_1_t267  : public MulticastDelegate_t216
{
};
