﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t421;

// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C" void TextAreaAttribute__ctor_m2053 (TextAreaAttribute_t421 * __this, int32_t ___minLines, int32_t ___maxLines, const MethodInfo* method) IL2CPP_METHOD_ATTR;
