﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AudioSource
struct AudioSource_t9;
// UnityEngine.AudioClip
struct AudioClip_t20;

// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C" AudioClip_t20 * AudioSource_get_clip_m107 (AudioSource_t9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" void AudioSource_set_clip_m114 (AudioSource_t9 * __this, AudioClip_t20 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C" void AudioSource_Play_m2846 (AudioSource_t9 * __this, uint64_t ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C" void AudioSource_Play_m81 (AudioSource_t9 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern "C" void AudioSource_set_loop_m78 (AudioSource_t9 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
