﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t2950;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1018;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20MethodDeclarations.h"
#define Enumerator__ctor_m18221(__this, ___dictionary, method) (( void (*) (Enumerator_t2950 *, Dictionary_2_t1018 *, const MethodInfo*))Enumerator__ctor_m18119_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18222(__this, method) (( Object_t * (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18120_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18223(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18224(__this, method) (( Object_t * (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18225(__this, method) (( Object_t * (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m18226(__this, method) (( bool (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_MoveNext_m18124_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m18227(__this, method) (( KeyValuePair_2_t2947  (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_get_Current_m18125_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18228(__this, method) (( String_t* (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_get_CurrentKey_m18126_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18229(__this, method) (( bool (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_get_CurrentValue_m18127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m18230(__this, method) (( void (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_VerifyState_m18128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18231(__this, method) (( void (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_VerifyCurrent_m18129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m18232(__this, method) (( void (*) (Enumerator_t2950 *, const MethodInfo*))Enumerator_Dispose_m18130_gshared)(__this, method)
