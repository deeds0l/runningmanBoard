﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t598;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>
struct  ResponseDelegate_1_t711  : public MulticastDelegate_t216
{
};
