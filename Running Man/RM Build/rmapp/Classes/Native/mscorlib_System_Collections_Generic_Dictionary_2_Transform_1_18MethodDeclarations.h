﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t2734;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_10MethodDeclarations.h"
#define Transform_1__ctor_m15472(__this, ___object, ___method, method) (( void (*) (Transform_1_t2734 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m13540_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m15473(__this, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Transform_1_t2734 *, String_t*, String_t*, const MethodInfo*))Transform_1_Invoke_m13541_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m15474(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2734 *, String_t*, String_t*, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m13542_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m15475(__this, ___result, method) (( DictionaryEntry_t1147  (*) (Transform_1_t2734 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m13543_gshared)(__this, ___result, method)
