﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<AudioPooler/objectSource>
struct List_1_t21;
// System.Object
struct Object_t;
// AudioPooler/objectSource
struct objectSource_t8;
// System.Collections.Generic.IEnumerator`1<AudioPooler/objectSource>
struct IEnumerator_1_t3035;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<AudioPooler/objectSource>
struct ICollection_1_t3036;
// System.Collections.Generic.IEnumerable`1<AudioPooler/objectSource>
struct IEnumerable_1_t35;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>
struct ReadOnlyCollection_1_t2450;
// AudioPooler/objectSource[]
struct objectSourceU5BU5D_t2448;
// System.Predicate`1<AudioPooler/objectSource>
struct Predicate_1_t2451;
// System.Comparison`1<AudioPooler/objectSource>
struct Comparison_1_t2453;
// System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m99(__this, method) (( void (*) (List_1_t21 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::.ctor(System.Int32)
#define List_1__ctor_m11513(__this, ___capacity, method) (( void (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::.cctor()
#define List_1__cctor_m11514(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11515(__this, method) (( Object_t* (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m11516(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t21 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m11517(__this, method) (( Object_t * (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m11518(__this, ___item, method) (( int32_t (*) (List_1_t21 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m11519(__this, ___item, method) (( bool (*) (List_1_t21 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m11520(__this, ___item, method) (( int32_t (*) (List_1_t21 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m11521(__this, ___index, ___item, method) (( void (*) (List_1_t21 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m11522(__this, ___item, method) (( void (*) (List_1_t21 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11523(__this, method) (( bool (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m11524(__this, method) (( bool (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m11525(__this, method) (( Object_t * (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m11526(__this, method) (( bool (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m11527(__this, method) (( bool (*) (List_1_t21 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m11528(__this, ___index, method) (( Object_t * (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m11529(__this, ___index, ___value, method) (( void (*) (List_1_t21 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Add(T)
#define List_1_Add_m11530(__this, ___item, method) (( void (*) (List_1_t21 *, objectSource_t8 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m11531(__this, ___newCount, method) (( void (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m11532(__this, ___collection, method) (( void (*) (List_1_t21 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m11533(__this, ___enumerable, method) (( void (*) (List_1_t21 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m11534(__this, ___collection, method) (( void (*) (List_1_t21 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AudioPooler/objectSource>::AsReadOnly()
#define List_1_AsReadOnly_m11535(__this, method) (( ReadOnlyCollection_1_t2450 * (*) (List_1_t21 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Clear()
#define List_1_Clear_m11536(__this, method) (( void (*) (List_1_t21 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::Contains(T)
#define List_1_Contains_m11537(__this, ___item, method) (( bool (*) (List_1_t21 *, objectSource_t8 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m11538(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t21 *, objectSourceU5BU5D_t2448*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<AudioPooler/objectSource>::Find(System.Predicate`1<T>)
#define List_1_Find_m11539(__this, ___match, method) (( objectSource_t8 * (*) (List_1_t21 *, Predicate_1_t2451 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m11540(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2451 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m11541(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t21 *, int32_t, int32_t, Predicate_1_t2451 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AudioPooler/objectSource>::GetEnumerator()
#define List_1_GetEnumerator_m11542(__this, method) (( Enumerator_t2452  (*) (List_1_t21 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::IndexOf(T)
#define List_1_IndexOf_m11543(__this, ___item, method) (( int32_t (*) (List_1_t21 *, objectSource_t8 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m11544(__this, ___start, ___delta, method) (( void (*) (List_1_t21 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m11545(__this, ___index, method) (( void (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Insert(System.Int32,T)
#define List_1_Insert_m11546(__this, ___index, ___item, method) (( void (*) (List_1_t21 *, int32_t, objectSource_t8 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m11547(__this, ___collection, method) (( void (*) (List_1_t21 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<AudioPooler/objectSource>::Remove(T)
#define List_1_Remove_m11548(__this, ___item, method) (( bool (*) (List_1_t21 *, objectSource_t8 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m11549(__this, ___match, method) (( int32_t (*) (List_1_t21 *, Predicate_1_t2451 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m11550(__this, ___index, method) (( void (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Reverse()
#define List_1_Reverse_m11551(__this, method) (( void (*) (List_1_t21 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Sort()
#define List_1_Sort_m11552(__this, method) (( void (*) (List_1_t21 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m11553(__this, ___comparison, method) (( void (*) (List_1_t21 *, Comparison_1_t2453 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<AudioPooler/objectSource>::ToArray()
#define List_1_ToArray_m11554(__this, method) (( objectSourceU5BU5D_t2448* (*) (List_1_t21 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::TrimExcess()
#define List_1_TrimExcess_m11555(__this, method) (( void (*) (List_1_t21 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::get_Capacity()
#define List_1_get_Capacity_m11556(__this, method) (( int32_t (*) (List_1_t21 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m11557(__this, ___value, method) (( void (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<AudioPooler/objectSource>::get_Count()
#define List_1_get_Count_m11558(__this, method) (( int32_t (*) (List_1_t21 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<AudioPooler/objectSource>::get_Item(System.Int32)
#define List_1_get_Item_m11559(__this, ___index, method) (( objectSource_t8 * (*) (List_1_t21 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<AudioPooler/objectSource>::set_Item(System.Int32,T)
#define List_1_set_Item_m11560(__this, ___index, ___value, method) (( void (*) (List_1_t21 *, int32_t, objectSource_t8 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
