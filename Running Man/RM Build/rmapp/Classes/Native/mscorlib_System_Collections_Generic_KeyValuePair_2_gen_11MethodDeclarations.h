﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct KeyValuePair_2_t2715;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t511;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15083(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2715 *, int32_t, LayoutCache_t511 *, const MethodInfo*))KeyValuePair_2__ctor_m12803_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
#define KeyValuePair_2_get_Key_m15084(__this, method) (( int32_t (*) (KeyValuePair_2_t2715 *, const MethodInfo*))KeyValuePair_2_get_Key_m12804_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15085(__this, ___value, method) (( void (*) (KeyValuePair_2_t2715 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m12805_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
#define KeyValuePair_2_get_Value_m15086(__this, method) (( LayoutCache_t511 * (*) (KeyValuePair_2_t2715 *, const MethodInfo*))KeyValuePair_2_get_Value_m12806_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15087(__this, ___value, method) (( void (*) (KeyValuePair_2_t2715 *, LayoutCache_t511 *, const MethodInfo*))KeyValuePair_2_set_Value_m12807_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
#define KeyValuePair_2_ToString_m15088(__this, method) (( String_t* (*) (KeyValuePair_2_t2715 *, const MethodInfo*))KeyValuePair_2_ToString_m12808_gshared)(__this, method)
