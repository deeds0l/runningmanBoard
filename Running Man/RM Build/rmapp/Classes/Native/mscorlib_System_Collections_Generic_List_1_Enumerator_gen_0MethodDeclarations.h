﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct Enumerator_t732;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t492;
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct List_1_t489;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m14997(__this, ___l, method) (( void (*) (Enumerator_t732 *, List_1_t489 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14998(__this, method) (( Object_t * (*) (Enumerator_t732 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Dispose()
#define Enumerator_Dispose_m14999(__this, method) (( void (*) (Enumerator_t732 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::VerifyState()
#define Enumerator_VerifyState_m15000(__this, method) (( void (*) (Enumerator_t732 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::MoveNext()
#define Enumerator_MoveNext_m3378(__this, method) (( bool (*) (Enumerator_t732 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Current()
#define Enumerator_get_Current_m3377(__this, method) (( GcLeaderboard_t492 * (*) (Enumerator_t732 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
