﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
extern TypeInfo* DebuggableAttribute_t932_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t400_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibVersionAttribute_t1616_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultDependencyAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var;
extern TypeInfo* StringFreezingAttribute_t1596_il2cpp_TypeInfo_var;
void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggableAttribute_t932_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1864);
		NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		AssemblyProductAttribute_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		TypeLibVersionAttribute_t1616_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3226);
		RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		DefaultDependencyAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3227);
		AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(367);
		AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1865);
		AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(365);
		AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		StringFreezingAttribute_t1596_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3228);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 21;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggableAttribute_t932 * tmp;
		tmp = (DebuggableAttribute_t932 *)il2cpp_codegen_object_new (DebuggableAttribute_t932_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m3744(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t929 * tmp;
		tmp = (SatelliteContractVersionAttribute_t929 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m3741(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t931 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t931 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m3743(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t400 * tmp;
		tmp = (AssemblyProductAttribute_t400 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t400_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m1982(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t397 * tmp;
		tmp = (AssemblyDescriptionAttribute_t397 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m1979(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, true, NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t399 * tmp;
		tmp = (AssemblyCompanyAttribute_t399 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m1981(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		TypeLibVersionAttribute_t1616 * tmp;
		tmp = (TypeLibVersionAttribute_t1616 *)il2cpp_codegen_object_new (TypeLibVersionAttribute_t1616_il2cpp_TypeInfo_var);
		TypeLibVersionAttribute__ctor_m8497(tmp, 2, 0, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t56 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t56 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m152(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		DefaultDependencyAttribute_t1593 * tmp;
		tmp = (DefaultDependencyAttribute_t1593 *)il2cpp_codegen_object_new (DefaultDependencyAttribute_t1593_il2cpp_TypeInfo_var);
		DefaultDependencyAttribute__ctor_m8462(tmp, 1, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t934 * tmp;
		tmp = (AssemblyKeyFileAttribute_t934 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m3746(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t403 * tmp;
		tmp = (AssemblyTitleAttribute_t403 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m1985(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t935 * tmp;
		tmp = (AssemblyDelaySignAttribute_t935 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m3747(tmp, true, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t930 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t930 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m3742(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t401 * tmp;
		tmp = (AssemblyCopyrightAttribute_t401 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m1983(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t404 * tmp;
		tmp = (AssemblyFileVersionAttribute_t404 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m1986(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t933 * tmp;
		tmp = (CompilationRelaxationsAttribute_t933 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m3745(tmp, 8, NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t928 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t928 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m3740(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[19] = (Il2CppObject*)tmp;
	}
	{
		StringFreezingAttribute_t1596 * tmp;
		tmp = (StringFreezingAttribute_t1596 *)il2cpp_codegen_object_new (StringFreezingAttribute_t1596_il2cpp_TypeInfo_var);
		StringFreezingAttribute__ctor_m8463(tmp, NULL);
		cache->attributes[20] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object__ctor_m80(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_Finalize_m218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m3394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ValueType_t439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern const Il2CppType* _Attribute_t832_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Attribute_t539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Attribute_t832_0_0_0_var = il2cpp_codegen_type_from_index(1306);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Attribute_t832_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
extern const Il2CppType* Attribute_t539_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
void _Attribute_t832_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Attribute_t539_0_0_0_var = il2cpp_codegen_type_from_index(1307);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("917B14D0-2D9E-38B8-92A9-381ACF52F7C0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Attribute_t539_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Int32_t54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IFormattable_t74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void IConvertible_t75_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IComparable_t76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializableAttribute_t1366_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4124, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AttributeUsageAttribute_t803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ComVisibleAttribute_t402_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 5597, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Int64_t753_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UInt32_t744_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt32_t744_CustomAttributesCacheGenerator_UInt32_Parse_m6028(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt32_t744_CustomAttributesCacheGenerator_UInt32_Parse_m6029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt32_t744_CustomAttributesCacheGenerator_UInt32_TryParse_m4840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt32_t744_CustomAttributesCacheGenerator_UInt32_TryParse_m3461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CLSCompliantAttribute_t936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UInt64_t756_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt64_t756_CustomAttributesCacheGenerator_UInt64_Parse_m6052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt64_t756_CustomAttributesCacheGenerator_UInt64_Parse_m6054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt64_t756_CustomAttributesCacheGenerator_UInt64_TryParse_m3445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Byte_t367_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SByte_t73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void SByte_t73_CustomAttributesCacheGenerator_SByte_Parse_m6104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void SByte_t73_CustomAttributesCacheGenerator_SByte_Parse_m6105(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void SByte_t73_CustomAttributesCacheGenerator_SByte_TryParse_m6106(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Int16_t448_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt16_t371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt16_t371_CustomAttributesCacheGenerator_UInt16_Parse_m6159(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt16_t371_CustomAttributesCacheGenerator_UInt16_Parse_m6160(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt16_t371_CustomAttributesCacheGenerator_UInt16_TryParse_m6161(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UInt16_t371_CustomAttributesCacheGenerator_UInt16_TryParse_m6162(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void IEnumerator_t28_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("496B0ABF-CDEE-11D3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IEnumerable_t464_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("496B0ABE-CDEE-11d3-88E8-00902754C43A"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern TypeInfo* DispIdAttribute_t1608_il2cpp_TypeInfo_var;
void IEnumerable_t464_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m10447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DispIdAttribute_t1608_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3234);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DispIdAttribute_t1608 * tmp;
		tmp = (DispIdAttribute_t1608 *)il2cpp_codegen_object_new (DispIdAttribute_t1608_il2cpp_TypeInfo_var);
		DispIdAttribute__ctor_m8469(tmp, -4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IDisposable_t43_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Char_t369_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String__ctor_m6196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m6219(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m6220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Split_m3502_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
extern TypeInfo* MonoDocumentationNoteAttribute_t1388_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m6224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoDocumentationNoteAttribute_t1388_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3235);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoDocumentationNoteAttribute_t1388 * tmp;
		tmp = (MonoDocumentationNoteAttribute_t1388 *)il2cpp_codegen_object_new (MonoDocumentationNoteAttribute_t1388_il2cpp_TypeInfo_var);
		MonoDocumentationNoteAttribute__ctor_m6732(tmp, il2cpp_codegen_string_new_wrapper("code should be moved to managed"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m6225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m4749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m3403_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m4842_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m4747_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m117_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m5860_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m6256_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m1496_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m122_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_GetHashCode_m6265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICloneable_t427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Single_t85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Single_t85_CustomAttributesCacheGenerator_Single_IsNaN_m6301(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Double_t752_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Double_t752_CustomAttributesCacheGenerator_Double_IsNaN_m6329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
extern TypeInfo* DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3236);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t1379 * tmp;
		tmp = (DecimalConstantAttribute_t1379 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m6717(tmp, 0, 255, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3236);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t1379 * tmp;
		tmp = (DecimalConstantAttribute_t1379 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m6717(tmp, 0, 0, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_MinusOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3236);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t1379 * tmp;
		tmp = (DecimalConstantAttribute_t1379 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m6717(tmp, 0, 255, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_One(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3236);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t1379 * tmp;
		tmp = (DecimalConstantAttribute_t1379 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t1379_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m6717(tmp, 0, 0, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal__ctor_m6341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal__ctor_m6343(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_Compare_m6374(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Boolean_t72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m3420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m6448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m6449(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m6452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m6455(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m3421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6458(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6459(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6460(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ISerializable_t428_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m6464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m6471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m6479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m6480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MulticastDelegate_t216_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void Delegate_t361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Delegate_t361_CustomAttributesCacheGenerator_Delegate_Combine_m6500(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Delegate_t361_CustomAttributesCacheGenerator_Delegate_t361_Delegate_Combine_m6500_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_GetName_m6508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_IsDefined_m5868(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m6510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_Parse_m4824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToString_m237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToString_m225(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6516(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6517(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Enum_t77_CustomAttributesCacheGenerator_Enum_Format_m6526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m6540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Length_m4685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m6549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m4690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m6552(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m6553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m6554_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m6555_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m6565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6569(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6571(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6572(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m6580_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m6583_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6584(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m6584_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m6585_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6587(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6588(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6589(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Clear_m5811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m4832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m6593(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m6594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m6595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m5806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m5835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6605(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m6611(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m6624(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Resize_m10479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m6625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t1957_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t1957_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m10520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m10527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m10530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICollection_t860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void IList_t861_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void IList_1_t1959_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Void_t71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Type_t1961_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Type_t1961_0_0_0_var = il2cpp_codegen_type_from_index(3237);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Type_t1961_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m6662(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m6681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10581(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m6691_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MemberInfo_t1962_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MemberInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MemberInfo_t1962_0_0_0_var = il2cpp_codegen_type_from_index(3238);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_MemberInfo_t1962_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICustomAttributeProvider_t1925_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
extern const Il2CppType* MemberInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _MemberInfo_t1962_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemberInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(3110);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("f7102fa9-cabb-3a74-a6da-b4567ef1b079"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(MemberInfo_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void IReflect_t1963_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Type
#include "mscorlib_System_Type.h"
extern const Il2CppType* Type_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void _Type_t1961_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_0_0_0_var = il2cpp_codegen_type_from_index(256);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Type_t_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Exception_t824_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void Exception_t42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Exception_t824_0_0_0_var = il2cpp_codegen_type_from_index(1224);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Exception_t824_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _Exception_t824_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("b36b5c63-42ef-38bc-a07e-0b34c98f164a"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t1372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t1372_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m6702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t1371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t1371_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m6707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ParamArrayAttribute_t68_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2048, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OutAttribute_t1373_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ObsoleteAttribute_t410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 6140, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DllImportAttribute_t1374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t1375_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 10496, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t1375_CustomAttributesCacheGenerator_MarshalType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t1375_CustomAttributesCacheGenerator_MarshalTypeRef(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void InAttribute_t1376_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ConditionalAttribute_t67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 68, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GuidAttribute_t396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 5149, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ComImportAttribute_t1377_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1028, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OptionalAttribute_t1378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void CompilerGeneratedAttribute_t59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void InternalsVisibleToAttribute_t791_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void RuntimeCompatibilityAttribute_t56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DebuggerHiddenAttribute_t58_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 224, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DefaultMemberAttribute_t423_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1036, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t1379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2304, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t1379_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m6717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FieldOffsetAttribute_t1380_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RuntimeArgumentHandle_t1381_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsyncCallback_t214_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IAsyncResult_t213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypedReference_t1382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MarshalByRefObject_t1022_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Locale_t1386_CustomAttributesCacheGenerator_Locale_t1386_Locale_GetText_m6729_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t1387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void MonoDocumentationNoteAttribute_t1388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandleZeroOrMinusOneIsInvalid_t1389_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m6733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeWaitHandle_t1391_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m6735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SortKey_t1411_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void X509CertificateCollection_t1438_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t1441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void ASN1_t1435_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void SmallXmlParser_t1453_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
extern const Il2CppType* CollectionDebuggerView_2_t1965_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
void Dictionary_2_t1969_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t1965_0_0_0_var = il2cpp_codegen_type_from_index(3240);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t1965_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Dictionary_2_t1969_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Dictionary_2_t1969_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m10674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t1965_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
void KeyCollection_t1972_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t1965_0_0_0_var = il2cpp_codegen_type_from_index(3240);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t1965_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t1965_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
void ValueCollection_t1974_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t1965_0_0_0_var = il2cpp_codegen_type_from_index(3240);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t1965_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void IDictionary_2_t1981_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void KeyNotFoundException_t1460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
void KeyValuePair_2_t1983_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("{value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m7455(tmp, il2cpp_codegen_string_new_wrapper("[{key}]"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_1_t1964_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void List_1_t1984_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_1_t1964_0_0_0_var = il2cpp_codegen_type_from_index(3243);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_1_t1964_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Collection_1_t1986_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ReadOnlyCollection_1_t1987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
extern const Il2CppType* CollectionDebuggerView_t1467_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArrayList_t985_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t1467_0_0_0_var = il2cpp_codegen_type_from_index(3244);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t1467_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void ArrayListWrapper_t1462_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void SynchronizedArrayListWrapper_t1463_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void ReadOnlyArrayListWrapper_t1465_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void BitArray_t1129_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CaseInsensitiveComparer_t1157_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CaseInsensitiveHashCodeProvider_t1158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use StringComparer instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CollectionBase_t1041_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Comparer_t1468_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DictionaryEntry_t1147_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("{_value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m7455(tmp, il2cpp_codegen_string_new_wrapper("[{_key}]"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t1467_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t1467_0_0_0_var = il2cpp_codegen_type_from_index(3244);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t1467_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m7378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m7381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4719(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_Clear_m7397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_Remove_m7400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m7404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialize equalityComparer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_t975____comparer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_t975____hcp_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t1467_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
void HashKeys_t1473_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t1467_0_0_0_var = il2cpp_codegen_type_from_index(3244);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t1467_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t1467_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
void HashValues_t1474_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t1467_0_0_0_var = il2cpp_codegen_type_from_index(3244);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t1467_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IComparer_t980_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void IDictionary_t1081_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IDictionaryEnumerator_t1146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IEqualityComparer_t987_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IHashCodeProvider_t986_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Please use IEqualityComparer instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SortedList_t1163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t1467_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var;
void Stack_t696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t1467_0_0_0_var = il2cpp_codegen_type_from_index(3244);
		DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3242);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3241);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t1486 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t1486 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t1486_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m7457(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t1467_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t1484 * tmp;
		tmp = (DebuggerDisplayAttribute_t1484 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t1484_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m7454(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyHashAlgorithm_t1481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyVersionCompatibility_t1482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void SuppressMessageAttribute_t800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("CODE_ANALYSIS"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DebuggableAttribute_t932_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 3, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DebuggingModes_t1483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DebuggerDisplayAttribute_t1484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4509, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DebuggerStepThroughAttribute_t1485_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 108, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DebuggerTypeProxyAttribute_t1486_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 13, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StackFrame_t46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with MS.NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void StackTrace_t44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Calendar_t1488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CompareInfo_t1357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CompareOptions_t1492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CultureInfo_t750_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CultureInfo_t750_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CultureInfo_t750_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void DateTimeFormatFlags_t1496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DateTimeFormatInfo_t1494_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DateTimeStyles_t1497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DaylightTime_t1498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void GregorianCalendar_t1499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GregorianCalendarTypes_t1500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void NumberFormatInfo_t1493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void NumberStyles_t1501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void TextInfo_t1408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("IDeserializationCallback isn't implemented."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void TextInfo_t1408_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7650(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TextInfo_t1408_CustomAttributesCacheGenerator_TextInfo_t1408____CultureName_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnicodeCategory_t1210_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IsolatedStorageException_t1503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void BinaryReader_t1505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m7680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m7683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m7684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m7685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Directory_t1506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DirectoryInfo_t1507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DirectoryNotFoundException_t1509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EndOfStreamException_t1510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void File_t1511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void FileAccess_t1161_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileAttributes_t1512_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileMode_t1513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileNotFoundException_t1514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileOptions_t1515_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void FileShare_t1516_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileStream_t1351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileSystemInfo_t1508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FileSystemInfo_t1508_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m7764(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IOException_t1358_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void MemoryStream_t742_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Path_t1184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void Path_t1184_CustomAttributesCacheGenerator_InvalidPathChars(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("see GetInvalidPathChars and GetInvalidFileNameChars methods."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void PathTooLongException_t1524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SeekOrigin_t1364_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Stream_t1294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StreamReader_t1529_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StreamWriter_t1530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringReader_t741_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TextReader_t1456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TextWriter_t1183_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyBuilder_t1988_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void AssemblyBuilder_t1538_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyBuilder_t1988_0_0_0_var = il2cpp_codegen_type_from_index(3245);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_AssemblyBuilder_t1988_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorBuilder_t1989_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void ConstructorBuilder_t1541_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorBuilder_t1989_0_0_0_var = il2cpp_codegen_type_from_index(3246);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_ConstructorBuilder_t1989_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void ConstructorBuilder_t1541_CustomAttributesCacheGenerator_ConstructorBuilder_t1541____CallingConvention_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EnumBuilder_t1990_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void EnumBuilder_t1542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EnumBuilder_t1990_0_0_0_var = il2cpp_codegen_type_from_index(3247);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_EnumBuilder_t1990_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EnumBuilder_t1542_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m8003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldBuilder_t1991_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void FieldBuilder_t1544_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldBuilder_t1991_0_0_0_var = il2cpp_codegen_type_from_index(3248);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_FieldBuilder_t1991_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m8039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m8042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m8083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m8084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m8085(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t1546_GenericTypeParameterBuilder_MakeGenericType_m8085_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBuilder_t1992_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodBuilder_t1545_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBuilder_t1992_0_0_0_var = il2cpp_codegen_type_from_index(3249);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_MethodBuilder_t1992_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void MethodBuilder_t1545_CustomAttributesCacheGenerator_MethodBuilder_Equals_m8101(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void MethodBuilder_t1545_CustomAttributesCacheGenerator_MethodBuilder_t1545_MethodBuilder_MakeGenericMethod_m8104_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ModuleBuilder_t1993_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void ModuleBuilder_t1548_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ModuleBuilder_t1993_0_0_0_var = il2cpp_codegen_type_from_index(3250);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_ModuleBuilder_t1993_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterBuilder_t1994_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void ParameterBuilder_t1550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterBuilder_t1994_0_0_0_var = il2cpp_codegen_type_from_index(3251);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_ParameterBuilder_t1994_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyBuilder_t1995_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void PropertyBuilder_t1551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyBuilder_t1995_0_0_0_var = il2cpp_codegen_type_from_index(3252);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_PropertyBuilder_t1995_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _TypeBuilder_t1996_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_TypeBuilder_t1996_0_0_0_var = il2cpp_codegen_type_from_index(3253);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_TypeBuilder_t1996_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m8148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m8167(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_t1539_TypeBuilder_MakeGenericType_m8167_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m8174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m8175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m8176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("arrays"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void UnmanagedMarshal_t1543_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("An alternate API is available: Emit the MarshalAs custom attribute instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AmbiguousMatchException_t1556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Assembly_t1997_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Assembly_t1164_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Assembly_t1997_0_0_0_var = il2cpp_codegen_type_from_index(3254);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Assembly_t1997_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyCompanyAttribute_t399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyConfigurationAttribute_t398_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyCopyrightAttribute_t401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyDefaultAliasAttribute_t930_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyDelaySignAttribute_t935_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyDescriptionAttribute_t397_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void AssemblyFileVersionAttribute_t404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyInformationalVersionAttribute_t928_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyKeyFileAttribute_t934_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyName_t1998_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void AssemblyName_t1561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyName_t1998_0_0_0_var = il2cpp_codegen_type_from_index(3255);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_AssemblyName_t1998_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyNameFlags_t1562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyProductAttribute_t400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyTitleAttribute_t403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void AssemblyTrademarkAttribute_t405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Binder_t781_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void Default_t1563_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m8228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void BindingFlags_t1564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CallingConventions_t1565_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorInfo_t1999_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void ConstructorInfo_t632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorInfo_t1999_0_0_0_var = il2cpp_codegen_type_from_index(3256);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_ConstructorInfo_t1999_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ConstructorInfo_t632_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m3491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3257);
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t1485 * tmp;
		tmp = (DebuggerStepThroughAttribute_t1485 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m7456(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorInfo_t632____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EventAttributes_t1566_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EventInfo_t2000_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void EventInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EventInfo_t2000_0_0_0_var = il2cpp_codegen_type_from_index(3258);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_EventInfo_t2000_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void FieldAttributes_t1568_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldInfo_t2001_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldInfo_t2001_0_0_0_var = il2cpp_codegen_type_from_index(3259);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_FieldInfo_t2001_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m8261(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3257);
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t1485 * tmp;
		tmp = (DebuggerStepThroughAttribute_t1485 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m7456(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MemberTypes_t1570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void MethodAttributes_t1571_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBase_t2002_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void MethodBase_t47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBase_t2002_0_0_0_var = il2cpp_codegen_type_from_index(3260);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_MethodBase_t2002_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var;
void MethodBase_t47_CustomAttributesCacheGenerator_MethodBase_Invoke_m8278(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3257);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t1485 * tmp;
		tmp = (DebuggerStepThroughAttribute_t1485 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m7456(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodBase_t47_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m8283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodImplAttributes_t1572_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodInfo_t2003_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodInfo_t2003_0_0_0_var = il2cpp_codegen_type_from_index(3261);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_MethodInfo_t2003_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m8290_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m8291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Missing_t1573_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void Missing_t1573_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Module_t2004_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
void Module_t1549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Module_t2004_0_0_0_var = il2cpp_codegen_type_from_index(3262);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Module_t2004_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void PInfo_t1581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void ParameterAttributes_t1583_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterInfo_t2005_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void ParameterInfo_t775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterInfo_t2005_0_0_0_var = il2cpp_codegen_type_from_index(3263);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_ParameterInfo_t2005_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ParameterModifier_t1584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Pointer_t1585_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ProcessorArchitecture_t1586_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void PropertyAttributes_t1587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyInfo_t2006_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyInfo_t2006_0_0_0_var = il2cpp_codegen_type_from_index(3264);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_PropertyInfo_t2006_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m8447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3257);
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t1485 * tmp;
		tmp = (DebuggerStepThroughAttribute_t1485 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m7456(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m8448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3257);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t1485 * tmp;
		tmp = (DebuggerStepThroughAttribute_t1485 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t1485_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m7456(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StrongNameKeyPair_t1560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TargetException_t1588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TargetInvocationException_t1589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TargetParameterCountException_t1590_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void TypeAttributes_t1591_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void NeutralResourcesLanguageAttribute_t931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SatelliteContractVersionAttribute_t929_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CompilationRelaxations_t1592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void CompilationRelaxationsAttribute_t933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 71, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DefaultDependencyAttribute_t1593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IsVolatile_t1594_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void StringFreezingAttribute_t1596_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m8464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m8465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ReliabilityContractAttribute_t1600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1133, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ActivationArguments_t1601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CallingConvention_t1602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CharSet_t1603_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ClassInterfaceAttribute_t1604_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 5, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ClassInterfaceType_t1605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ComDefaultInterfaceAttribute_t1606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ComInterfaceType_t1607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DispIdAttribute_t1608_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 960, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void GCHandle_t1609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void GCHandleType_t1610_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void InterfaceTypeAttribute_t1611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
extern TypeInfo* SuppressUnmanagedCodeSecurityAttribute_t1769_il2cpp_TypeInfo_var;
void Marshal_t1612_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressUnmanagedCodeSecurityAttribute_t1769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3265);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressUnmanagedCodeSecurityAttribute_t1769 * tmp;
		tmp = (SuppressUnmanagedCodeSecurityAttribute_t1769 *)il2cpp_codegen_object_new (SuppressUnmanagedCodeSecurityAttribute_t1769_il2cpp_TypeInfo_var);
		SuppressUnmanagedCodeSecurityAttribute__ctor_m9271(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MarshalDirectiveException_t1613_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void PreserveSigAttribute_t1614_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle__ctor_m8487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Close_m8488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m8489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m8490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m8491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Dispose_m8492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Dispose_m8493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m10954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m8494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m10955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeLibImportClassAttribute_t1615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeLibVersionAttribute_t1616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnmanagedType_t1617_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Activator
#include "mscorlib_System_Activator.h"
extern const Il2CppType* Activator_t1810_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _Activator_t2007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Activator_t1810_0_0_0_var = il2cpp_codegen_type_from_index(3266);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Activator_t1810_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
extern const Il2CppType* Assembly_t1164_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _Assembly_t1997_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Assembly_t1164_0_0_0_var = il2cpp_codegen_type_from_index(3267);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Assembly_t1164_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
extern const Il2CppType* AssemblyBuilder_t1538_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _AssemblyBuilder_t1988_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyBuilder_t1538_0_0_0_var = il2cpp_codegen_type_from_index(2989);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(AssemblyBuilder_t1538_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
extern const Il2CppType* AssemblyName_t1561_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void _AssemblyName_t1998_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyName_t1561_0_0_0_var = il2cpp_codegen_type_from_index(3000);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(AssemblyName_t1561_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
extern const Il2CppType* ConstructorBuilder_t1541_0_0_0_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
void _ConstructorBuilder_t1989_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorBuilder_t1541_0_0_0_var = il2cpp_codegen_type_from_index(3268);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(ConstructorBuilder_t1541_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern const Il2CppType* ConstructorInfo_t632_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void _ConstructorInfo_t1999_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorInfo_t632_0_0_0_var = il2cpp_codegen_type_from_index(1039);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(ConstructorInfo_t632_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
extern const Il2CppType* EnumBuilder_t1542_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
void _EnumBuilder_t1990_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EnumBuilder_t1542_0_0_0_var = il2cpp_codegen_type_from_index(2877);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(EnumBuilder_t1542_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
extern const Il2CppType* EventInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _EventInfo_t2000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2872);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(EventInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
extern const Il2CppType* FieldBuilder_t1544_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _FieldBuilder_t1991_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldBuilder_t1544_0_0_0_var = il2cpp_codegen_type_from_index(3269);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(FieldBuilder_t1544_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
extern const Il2CppType* FieldInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _FieldInfo_t2001_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(1029);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(FieldInfo_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
extern const Il2CppType* MethodBase_t47_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _MethodBase_t2002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBase_t47_0_0_0_var = il2cpp_codegen_type_from_index(2992);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(MethodBase_t47_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
extern const Il2CppType* MethodBuilder_t1545_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _MethodBuilder_t1992_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBuilder_t1545_0_0_0_var = il2cpp_codegen_type_from_index(3270);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(MethodBuilder_t1545_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _MethodInfo_t2003_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(1070);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(MethodInfo_t_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
extern const Il2CppType* Module_t1549_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _Module_t2004_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Module_t1549_0_0_0_var = il2cpp_codegen_type_from_index(2985);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Module_t1549_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
extern const Il2CppType* ModuleBuilder_t1548_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
void _ModuleBuilder_t1993_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ModuleBuilder_t1548_0_0_0_var = il2cpp_codegen_type_from_index(2988);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(ModuleBuilder_t1548_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
extern const Il2CppType* ParameterBuilder_t1550_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
void _ParameterBuilder_t1994_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterBuilder_t1550_0_0_0_var = il2cpp_codegen_type_from_index(3271);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(ParameterBuilder_t1550_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern const Il2CppType* ParameterInfo_t775_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _ParameterInfo_t2005_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterInfo_t775_0_0_0_var = il2cpp_codegen_type_from_index(2987);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(ParameterInfo_t775_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilder.h"
extern const Il2CppType* PropertyBuilder_t1551_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
void _PropertyBuilder_t1995_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyBuilder_t1551_0_0_0_var = il2cpp_codegen_type_from_index(3272);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("15F9A479-9397-3A63-ACBD-F51977FB0F02"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(PropertyBuilder_t1551_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
extern const Il2CppType* PropertyInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
void _PropertyInfo_t2006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(1026);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(PropertyInfo_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
extern const Il2CppType* Thread_t1634_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void _Thread_t2008_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1634_0_0_0_var = il2cpp_codegen_type_from_index(2843);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(Thread_t1634_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
extern const Il2CppType* TypeBuilder_t1539_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t396_il2cpp_TypeInfo_var;
void _TypeBuilder_t1996_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeBuilder_t1539_0_0_0_var = il2cpp_codegen_type_from_index(2875);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3233);
		InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3232);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		GuidAttribute_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t1615 * tmp;
		tmp = (TypeLibImportClassAttribute_t1615 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m8496(tmp, il2cpp_codegen_type_get_object(TypeBuilder_t1539_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t1611 * tmp;
		tmp = (InterfaceTypeAttribute_t1611 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t1611_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m8480(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t396 * tmp;
		tmp = (GuidAttribute_t396 *)il2cpp_codegen_object_new (GuidAttribute_t396_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m1978(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IActivator_t1618_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IConstructionCallMessage_t1918_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UrlAttribute_t1624_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UrlAttribute_t1624_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m8508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UrlAttribute_t1624_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m8509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ChannelServices_t1628_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void ChannelServices_t1628_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m8513(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void CrossAppDomainSink_t1631_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IChannel_t1919_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IChannelReceiver_t1933_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IChannelSender_t2009_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Context_t1632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ContextAttribute_t1625_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IContextAttribute_t1931_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IContextProperty_t1920_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IContributeClientContextSink_t2010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IContributeServerContextSink_t2011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t1635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t1635_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m8543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t1635_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m8544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsyncResult_t1642_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ConstructionCall_t1643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ConstructionCall_t1643_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t1645_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t1645_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Header_t1648_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMessage_t1641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMessageCtrl_t1640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMessageSink_t1218_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMethodCallMessage_t1922_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMethodMessage_t1653_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IMethodReturnMessage_t1921_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IRemotingFormatter_t2012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void LogicalCallContext_t1650_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodCall_t1644_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MethodCall_t1644_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void MethodDictionary_t1646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MethodDictionary_t1646_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MethodDictionary_t1646_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RemotingSurrogateSelector_t1658_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ReturnMessage_t1659_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ProxyAttribute_t1660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ProxyAttribute_t1660_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m8680(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ProxyAttribute_t1660_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m8681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RealProxy_t1661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ITrackingHandler_t1936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TrackingServices_t1665_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ActivatedClientTypeEntry_t1666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IChannelInfo_t1672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IEnvoyInfo_t1674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IRemotingTypeInfo_t1673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ObjRef_t1669_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ObjRef_t1669_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void ObjRef_t1669_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m8718(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RemotingConfiguration_t1675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RemotingException_t1676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RemotingServices_t1678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void RemotingServices_t1678_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m8738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void RemotingServices_t1678_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m8742(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeEntry_t1667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void WellKnownObjectMode_t1683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void BinaryFormatter_t1677_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void BinaryFormatter_t1677_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void BinaryFormatter_t1677_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m8774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FormatterAssemblyStyle_t1696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FormatterTypeStyle_t1697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeFilterLevel_t1698_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FormatterConverter_t1699_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FormatterServices_t1700_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IDeserializationCallback_t1206_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IFormatter_t2014_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void IFormatterConverter_t1716_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IObjectReference_t1941_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ISerializationSurrogate_t1708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ISurrogateSelector_t1657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ObjectManager_t1694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OnDeserializedAttribute_t1709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void OnDeserializingAttribute_t1710_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void OnSerializedAttribute_t1711_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void OnSerializingAttribute_t1712_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializationBinder_t1689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializationEntry_t1715_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializationException_t1160_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializationInfo_t725_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void SerializationInfo_t725_CustomAttributesCacheGenerator_SerializationInfo__ctor_m8878(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void SerializationInfo_t725_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m8884(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SerializationInfoEnumerator_t1717_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StreamingContext_t726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void StreamingContextStates_t1718_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("X509ContentType.SerializedCert isn't supported (anywhere in the class)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m4949(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use the Issuer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_GetName_m4950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use the Subject property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Equals_m4941(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Import_m4771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Reset_m4773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void X509KeyStorageFlags_t1209_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsymmetricAlgorithm_t1024_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsymmetricKeyExchangeFormatter_t1719_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsymmetricSignatureDeformatter_t1299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AsymmetricSignatureFormatter_t1301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CipherMode_t1363_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CryptoConfig_t1175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void CryptoConfig_t1175_CustomAttributesCacheGenerator_CryptoConfig_t1175_CryptoConfig_CreateFromName_m4805_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CryptographicException_t1171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CryptographicUnexpectedOperationException_t1176_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CspParameters_t1342_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CspProviderFlags_t1721_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DES_t1353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DESCryptoServiceProvider_t1724_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSA_t1148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1169_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1169____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSAParameters_t1170_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSASignatureDeformatter_t1349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DSASignatureFormatter_t1725_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMAC_t1346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACMD5_t1726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACRIPEMD160_t1727_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACSHA1_t1345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACSHA256_t1728_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACSHA384_t1729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HMACSHA512_t1730_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HashAlgorithm_t1241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICryptoTransform_t1272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICspAsymmetricAlgorithm_t2015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void KeyedHashAlgorithm_t1265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MACTripleDES_t1731_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MD5_t1347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MD5CryptoServiceProvider_t1732_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void PaddingMode_t1733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RC2_t1354_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RC2CryptoServiceProvider_t1734_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RIPEMD160_t1736_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RIPEMD160Managed_t1737_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSA_t1149_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1166_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1166_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1166____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSAPKCS1KeyExchangeFormatter_t1359_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureDeformatter_t1350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureFormatter_t1739_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RSAParameters_t1168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Rijndael_t1356_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RijndaelManaged_t1740_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RijndaelManagedTransform_t1742_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA1_t1180_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA1CryptoServiceProvider_t1744_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA1Managed_t1745_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA256_t1348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA256Managed_t1746_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA384_t1747_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA384Managed_t1749_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA512_t1750_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SHA512Managed_t1751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SignatureDescription_t1753_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SymmetricAlgorithm_t1248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ToBase64Transform_t1756_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TripleDES_t1355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TripleDESCryptoServiceProvider_t1757_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StrongNamePublicKeyBlob_t1759_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ApplicationTrust_t1761_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void Evidence_t1558_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Evidence_t1558_CustomAttributesCacheGenerator_Evidence_Equals_m9224(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Evidence_t1558_CustomAttributesCacheGenerator_Evidence_GetHashCode_m9226(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Hash_t1763_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IIdentityPermissionFactory_t2017_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StrongName_t1764_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IPrincipal_t1803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void PrincipalPolicy_t1765_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IPermission_t1767_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ISecurityEncodable_t2018_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SecurityElement_t1451_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SecurityException_t1768_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SecurityException_t1768_CustomAttributesCacheGenerator_SecurityException_t1768____Demanded_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void SecuritySafeCriticalAttribute_t794_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SuppressUnmanagedCodeSecurityAttribute_t1769_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 5188, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnverifiableCodeAttribute_t1770_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ASCIIEncoding_t1771_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m9286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m9287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m9288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("we have simple override to match method signature."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Decoder_t1504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Decoder_t1504_CustomAttributesCacheGenerator_Decoder_t1504____Fallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Decoder_t1504_CustomAttributesCacheGenerator_Decoder_t1504____FallbackBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void DecoderReplacementFallback_t1777_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m9311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void EncoderReplacementFallback_t1784_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m9341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705_Encoding_InvokeI18N_m9372_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_GetByteCount_m9388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_GetBytes_m9389(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____IsReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____DecoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____EncoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void StringBuilder_t338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_t338_StringBuilder_AppendFormat_m5805_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_t338_StringBuilder_AppendFormat_m9420_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m9430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m9431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m9440(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m9442(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m9450(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m9451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m9463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m9464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m9465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m9466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m9467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void UTF8Encoding_t1794_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("EncoderFallback is not handled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m9476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m9481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m9497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnicodeEncoding_t1796_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m9505(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m9508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m9512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EventResetMode_t1797_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EventWaitHandle_t1798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void ExecutionContext_t1638_CustomAttributesCacheGenerator_ExecutionContext__ctor_m9524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void ExecutionContext_t1638_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m9525(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Interlocked_t1799_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m9526(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ManualResetEvent_t1293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Monitor_t1800_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Monitor_t1800_CustomAttributesCacheGenerator_Monitor_Exit_m4709(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Mutex_t1633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Mutex_t1633_CustomAttributesCacheGenerator_Mutex__ctor_m9527(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Mutex_t1633_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m9530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SynchronizationLockException_t1802_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Thread_t2008_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Thread_t2008_0_0_0_var = il2cpp_codegen_type_from_index(3273);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Thread_t2008_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator_local_slots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator__ec(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m9540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator_Thread_Finalize_m9551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m9554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Thread_t1634_CustomAttributesCacheGenerator_Thread_GetHashCode_m9555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ThreadAbortException_t1804_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ThreadInterruptedException_t1805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void ThreadState_t1806_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ThreadStateException_t1807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void WaitHandle_t1340_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void WaitHandle_t1340_CustomAttributesCacheGenerator_WaitHandle_t1340____Handle_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("In the profiles > 2.x, use SafeHandle instead of Handle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AccessViolationException_t1808_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ActivationContext_t1809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void ActivationContext_t1809_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m9575(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization support"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Activator_t2007_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Activator_t1810_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Activator_t2007_0_0_0_var = il2cpp_codegen_type_from_index(3275);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3231);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t1606 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t1606 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m8468(tmp, il2cpp_codegen_type_get_object(_Activator_t2007_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void Activator_t1810_CustomAttributesCacheGenerator_Activator_t1810_Activator_CreateInstance_m9580_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
void AppDomain_t1811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void AppDomain_t1811_CustomAttributesCacheGenerator_type_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void AppDomain_t1811_CustomAttributesCacheGenerator_assembly_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void AppDomain_t1811_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void AppDomain_t1811_CustomAttributesCacheGenerator__principal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AppDomainManager_t1812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AppDomainSetup_t1819_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3229);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t1604 * tmp;
		tmp = (ClassInterfaceAttribute_t1604 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t1604_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m8467(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ApplicationException_t1820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ApplicationIdentity_t1813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void ApplicationIdentity_t1813_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArgumentException_t387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArgumentNullException_t749_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArgumentOutOfRangeException_t751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArithmeticException_t1341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ArrayTypeMismatchException_t1821_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyLoadEventArgs_t1822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AttributeTargets_t1823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Buffer_t1824_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void CharEnumerator_t1825_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ContextBoundObject_t1826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9657(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9658(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9672(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9678(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9681(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9691(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9692(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9715(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9718(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9719(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9720(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9729(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9731(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9733(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9746(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9768(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9772(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9775(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9779(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9791(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9805(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9806(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9807(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9808(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m3429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9809(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m3393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9810(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9815(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9819(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m3392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9830(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9833(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9834(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m3430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
void Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9836(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DBNull_t1827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DateTimeKind_t1829_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void DateTimeOffset_t768_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6730(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DayOfWeek_t1831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DivideByZeroException_t1834_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void DllNotFoundException_t1835_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EntryPointNotFoundException_t1837_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void MonoEnumInfo_t1842_CustomAttributesCacheGenerator_cache(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Environment_t1845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SpecialFolder_t1843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EventArgs_t1249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ExecutionEngineException_t1846_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FieldAccessException_t1847_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FlagsAttribute_t406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 16, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void FormatException_t743_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void GC_t1849_CustomAttributesCacheGenerator_GC_SuppressFinalize_m5812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Guid_t769_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ICustomFormatter_t1926_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IFormatProvider_t1909_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void IndexOutOfRangeException_t738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void InvalidCastException_t1850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void InvalidOperationException_t1159_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void LoaderOptimization_t1851_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void LoaderOptimization_t1851_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m4930(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void LoaderOptimization_t1851_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m4930(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Math_t1852_CustomAttributesCacheGenerator_Math_Max_m5816(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Math_t1852_CustomAttributesCacheGenerator_Math_Min_m10029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void Math_t1852_CustomAttributesCacheGenerator_Math_Sqrt_m10036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MemberAccessException_t1848_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MethodAccessException_t1853_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MissingFieldException_t1854_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MissingMemberException_t1855_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MissingMethodException_t1856_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MulticastNotSupportedException_t1862_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void NonSerializedAttribute_t1863_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void NotImplementedException_t1165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void NotSupportedException_t32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void NullReferenceException_t727_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var;
void NumberFormatter_t1865_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3274);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t1876 * tmp;
		tmp = (ThreadStaticAttribute_t1876 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t1876_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m10272(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ObjectDisposedException_t1344_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OperatingSystem_t1844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OutOfMemoryException_t1866_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void OverflowException_t1867_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void PlatformID_t1868_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Random_t614_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void RankException_t1869_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ResolveEventArgs_t1870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t1387_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t1871_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		MonoTODOAttribute_t1387_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3239);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t1387 * tmp;
		tmp = (MonoTODOAttribute_t1387 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1387_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m6731(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t1871_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m10254(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringComparer_t734_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringComparison_t1874_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void StringSplitOptions_t1875_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void SystemException_t1181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ThreadStaticAttribute_t1876_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TimeSpan_t1051_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TimeZone_t1877_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeCode_t1879_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeInitializationException_t1880_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeLoadException_t1836_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnauthorizedAccessException_t1881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m10338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m10339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3230);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t1600 * tmp;
		tmp = (ReliabilityContractAttribute_t1600 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t1600_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m8466(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void Version_t1006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void WeakReference_t1670_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void MemberFilter_t1370_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void TypeFilter_t1574_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void HeaderHandler_t1887_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AppDomainInitializer_t1818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void AssemblyLoadEventHandler_t1814_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void EventHandler_t1816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void ResolveEventHandler_t1815_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
void UnhandledExceptionEventHandler_t1817_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1908_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_mscorlib_Assembly_AttributeGenerators[939] = 
{
	NULL,
	g_mscorlib_Assembly_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator_Object__ctor_m80,
	Object_t_CustomAttributesCacheGenerator_Object_Finalize_m218,
	Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m3394,
	ValueType_t439_CustomAttributesCacheGenerator,
	Attribute_t539_CustomAttributesCacheGenerator,
	_Attribute_t832_CustomAttributesCacheGenerator,
	Int32_t54_CustomAttributesCacheGenerator,
	IFormattable_t74_CustomAttributesCacheGenerator,
	IConvertible_t75_CustomAttributesCacheGenerator,
	IComparable_t76_CustomAttributesCacheGenerator,
	SerializableAttribute_t1366_CustomAttributesCacheGenerator,
	AttributeUsageAttribute_t803_CustomAttributesCacheGenerator,
	ComVisibleAttribute_t402_CustomAttributesCacheGenerator,
	Int64_t753_CustomAttributesCacheGenerator,
	UInt32_t744_CustomAttributesCacheGenerator,
	UInt32_t744_CustomAttributesCacheGenerator_UInt32_Parse_m6028,
	UInt32_t744_CustomAttributesCacheGenerator_UInt32_Parse_m6029,
	UInt32_t744_CustomAttributesCacheGenerator_UInt32_TryParse_m4840,
	UInt32_t744_CustomAttributesCacheGenerator_UInt32_TryParse_m3461,
	CLSCompliantAttribute_t936_CustomAttributesCacheGenerator,
	UInt64_t756_CustomAttributesCacheGenerator,
	UInt64_t756_CustomAttributesCacheGenerator_UInt64_Parse_m6052,
	UInt64_t756_CustomAttributesCacheGenerator_UInt64_Parse_m6054,
	UInt64_t756_CustomAttributesCacheGenerator_UInt64_TryParse_m3445,
	Byte_t367_CustomAttributesCacheGenerator,
	SByte_t73_CustomAttributesCacheGenerator,
	SByte_t73_CustomAttributesCacheGenerator_SByte_Parse_m6104,
	SByte_t73_CustomAttributesCacheGenerator_SByte_Parse_m6105,
	SByte_t73_CustomAttributesCacheGenerator_SByte_TryParse_m6106,
	Int16_t448_CustomAttributesCacheGenerator,
	UInt16_t371_CustomAttributesCacheGenerator,
	UInt16_t371_CustomAttributesCacheGenerator_UInt16_Parse_m6159,
	UInt16_t371_CustomAttributesCacheGenerator_UInt16_Parse_m6160,
	UInt16_t371_CustomAttributesCacheGenerator_UInt16_TryParse_m6161,
	UInt16_t371_CustomAttributesCacheGenerator_UInt16_TryParse_m6162,
	IEnumerator_t28_CustomAttributesCacheGenerator,
	IEnumerable_t464_CustomAttributesCacheGenerator,
	IEnumerable_t464_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m10447,
	IDisposable_t43_CustomAttributesCacheGenerator,
	Char_t369_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator_String__ctor_m6196,
	String_t_CustomAttributesCacheGenerator_String_Equals_m6219,
	String_t_CustomAttributesCacheGenerator_String_Equals_m6220,
	String_t_CustomAttributesCacheGenerator_String_t_String_Split_m3502_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_Split_m6224,
	String_t_CustomAttributesCacheGenerator_String_Split_m6225,
	String_t_CustomAttributesCacheGenerator_String_Split_m4749,
	String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m3403_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m4842_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m4747_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m117_Arg1_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m5860_Arg2_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m6256_Arg3_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m1496_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m122_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_GetHashCode_m6265,
	ICloneable_t427_CustomAttributesCacheGenerator,
	Single_t85_CustomAttributesCacheGenerator,
	Single_t85_CustomAttributesCacheGenerator_Single_IsNaN_m6301,
	Double_t752_CustomAttributesCacheGenerator,
	Double_t752_CustomAttributesCacheGenerator_Double_IsNaN_m6329,
	Decimal_t755_CustomAttributesCacheGenerator,
	Decimal_t755_CustomAttributesCacheGenerator_MinValue,
	Decimal_t755_CustomAttributesCacheGenerator_MaxValue,
	Decimal_t755_CustomAttributesCacheGenerator_MinusOne,
	Decimal_t755_CustomAttributesCacheGenerator_One,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal__ctor_m6341,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal__ctor_m6343,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_Compare_m6374,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6401,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6403,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6405,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Explicit_m6407,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6409,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6411,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6413,
	Decimal_t755_CustomAttributesCacheGenerator_Decimal_op_Implicit_m6415,
	Boolean_t72_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m3420,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m6448,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m6449,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m6452,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m6455,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m3512,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m3421,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6458,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6459,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6460,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m6461,
	ISerializable_t428_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m6464,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m6471,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m6479,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m6480,
	MulticastDelegate_t216_CustomAttributesCacheGenerator,
	Delegate_t361_CustomAttributesCacheGenerator,
	Delegate_t361_CustomAttributesCacheGenerator_Delegate_Combine_m6500,
	Delegate_t361_CustomAttributesCacheGenerator_Delegate_t361_Delegate_Combine_m6500_Arg0_ParameterInfo,
	Enum_t77_CustomAttributesCacheGenerator,
	Enum_t77_CustomAttributesCacheGenerator_Enum_GetName_m6508,
	Enum_t77_CustomAttributesCacheGenerator_Enum_IsDefined_m5868,
	Enum_t77_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m6510,
	Enum_t77_CustomAttributesCacheGenerator_Enum_Parse_m4824,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToString_m237,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToString_m225,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6514,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6515,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6516,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6517,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6518,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6519,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6520,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6521,
	Enum_t77_CustomAttributesCacheGenerator_Enum_ToObject_m6522,
	Enum_t77_CustomAttributesCacheGenerator_Enum_Format_m6526,
	Array_t_CustomAttributesCacheGenerator,
	Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m6540,
	Array_t_CustomAttributesCacheGenerator_Array_get_Length_m4685,
	Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m6549,
	Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m4690,
	Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m6552,
	Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m6553,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m6554_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m6555_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m6565,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6569,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6570,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6571,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6572,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6573,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6574,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m6580_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m6583_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m6584,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m6584_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m6585,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m6585_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6586,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6587,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6588,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m6589,
	Array_t_CustomAttributesCacheGenerator_Array_Clear_m5811,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m4832,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m6593,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m6594,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m6595,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6596,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6597,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m6598,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6600,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6601,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m6602,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m5806,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m5835,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6604,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6605,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6606,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6607,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6608,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6609,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6610,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m6611,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10464,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10465,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10466,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10467,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10468,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10469,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10470,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10471,
	Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m6624,
	Array_t_CustomAttributesCacheGenerator_Array_Resize_m10479,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10490,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10491,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10492,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10493,
	Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m6625,
	Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo,
	ArrayReadOnlyList_1_t1957_CustomAttributesCacheGenerator,
	ArrayReadOnlyList_1_t1957_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m10520,
	U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m10527,
	U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10528,
	U3CGetEnumeratorU3Ec__Iterator0_t1958_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m10530,
	ICollection_t860_CustomAttributesCacheGenerator,
	IList_t861_CustomAttributesCacheGenerator,
	IList_1_t1959_CustomAttributesCacheGenerator,
	Void_t71_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m6662,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6678,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6679,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m6680,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m6681,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10581,
	Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m6691_Arg0_ParameterInfo,
	MemberInfo_t_CustomAttributesCacheGenerator,
	ICustomAttributeProvider_t1925_CustomAttributesCacheGenerator,
	_MemberInfo_t1962_CustomAttributesCacheGenerator,
	IReflect_t1963_CustomAttributesCacheGenerator,
	_Type_t1961_CustomAttributesCacheGenerator,
	Exception_t42_CustomAttributesCacheGenerator,
	_Exception_t824_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t1372_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t1372_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m6702,
	RuntimeTypeHandle_t1371_CustomAttributesCacheGenerator,
	RuntimeTypeHandle_t1371_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m6707,
	ParamArrayAttribute_t68_CustomAttributesCacheGenerator,
	OutAttribute_t1373_CustomAttributesCacheGenerator,
	ObsoleteAttribute_t410_CustomAttributesCacheGenerator,
	DllImportAttribute_t1374_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t1375_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t1375_CustomAttributesCacheGenerator_MarshalType,
	MarshalAsAttribute_t1375_CustomAttributesCacheGenerator_MarshalTypeRef,
	InAttribute_t1376_CustomAttributesCacheGenerator,
	ConditionalAttribute_t67_CustomAttributesCacheGenerator,
	GuidAttribute_t396_CustomAttributesCacheGenerator,
	ComImportAttribute_t1377_CustomAttributesCacheGenerator,
	OptionalAttribute_t1378_CustomAttributesCacheGenerator,
	CompilerGeneratedAttribute_t59_CustomAttributesCacheGenerator,
	InternalsVisibleToAttribute_t791_CustomAttributesCacheGenerator,
	RuntimeCompatibilityAttribute_t56_CustomAttributesCacheGenerator,
	DebuggerHiddenAttribute_t58_CustomAttributesCacheGenerator,
	DefaultMemberAttribute_t423_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t1379_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t1379_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m6717,
	FieldOffsetAttribute_t1380_CustomAttributesCacheGenerator,
	RuntimeArgumentHandle_t1381_CustomAttributesCacheGenerator,
	AsyncCallback_t214_CustomAttributesCacheGenerator,
	IAsyncResult_t213_CustomAttributesCacheGenerator,
	TypedReference_t1382_CustomAttributesCacheGenerator,
	MarshalByRefObject_t1022_CustomAttributesCacheGenerator,
	Locale_t1386_CustomAttributesCacheGenerator_Locale_t1386_Locale_GetText_m6729_Arg1_ParameterInfo,
	MonoTODOAttribute_t1387_CustomAttributesCacheGenerator,
	MonoDocumentationNoteAttribute_t1388_CustomAttributesCacheGenerator,
	SafeHandleZeroOrMinusOneIsInvalid_t1389_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m6733,
	SafeWaitHandle_t1391_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m6735,
	MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3,
	MSCompatUnicodeTable_t1401_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4,
	SortKey_t1411_CustomAttributesCacheGenerator,
	PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	PKCS12_t1439_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509CertificateCollection_t1438_CustomAttributesCacheGenerator,
	X509ExtensionCollection_t1441_CustomAttributesCacheGenerator,
	ASN1_t1435_CustomAttributesCacheGenerator,
	SmallXmlParser_t1453_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18,
	Dictionary_2_t1969_CustomAttributesCacheGenerator,
	Dictionary_2_t1969_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	Dictionary_2_t1969_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m10674,
	KeyCollection_t1972_CustomAttributesCacheGenerator,
	ValueCollection_t1974_CustomAttributesCacheGenerator,
	IDictionary_2_t1981_CustomAttributesCacheGenerator,
	KeyNotFoundException_t1460_CustomAttributesCacheGenerator,
	KeyValuePair_2_t1983_CustomAttributesCacheGenerator,
	List_1_t1984_CustomAttributesCacheGenerator,
	Collection_1_t1986_CustomAttributesCacheGenerator,
	ReadOnlyCollection_1_t1987_CustomAttributesCacheGenerator,
	ArrayList_t985_CustomAttributesCacheGenerator,
	ArrayListWrapper_t1462_CustomAttributesCacheGenerator,
	SynchronizedArrayListWrapper_t1463_CustomAttributesCacheGenerator,
	ReadOnlyArrayListWrapper_t1465_CustomAttributesCacheGenerator,
	BitArray_t1129_CustomAttributesCacheGenerator,
	CaseInsensitiveComparer_t1157_CustomAttributesCacheGenerator,
	CaseInsensitiveHashCodeProvider_t1158_CustomAttributesCacheGenerator,
	CollectionBase_t1041_CustomAttributesCacheGenerator,
	Comparer_t1468_CustomAttributesCacheGenerator,
	DictionaryEntry_t1147_CustomAttributesCacheGenerator,
	Hashtable_t975_CustomAttributesCacheGenerator,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m7378,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4680,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m7381,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4681,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable__ctor_m4719,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_Clear_m7397,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_Remove_m7400,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m7404,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_t975____comparer_PropertyInfo,
	Hashtable_t975_CustomAttributesCacheGenerator_Hashtable_t975____hcp_PropertyInfo,
	HashKeys_t1473_CustomAttributesCacheGenerator,
	HashValues_t1474_CustomAttributesCacheGenerator,
	IComparer_t980_CustomAttributesCacheGenerator,
	IDictionary_t1081_CustomAttributesCacheGenerator,
	IDictionaryEnumerator_t1146_CustomAttributesCacheGenerator,
	IEqualityComparer_t987_CustomAttributesCacheGenerator,
	IHashCodeProvider_t986_CustomAttributesCacheGenerator,
	SortedList_t1163_CustomAttributesCacheGenerator,
	Stack_t696_CustomAttributesCacheGenerator,
	AssemblyHashAlgorithm_t1481_CustomAttributesCacheGenerator,
	AssemblyVersionCompatibility_t1482_CustomAttributesCacheGenerator,
	SuppressMessageAttribute_t800_CustomAttributesCacheGenerator,
	DebuggableAttribute_t932_CustomAttributesCacheGenerator,
	DebuggingModes_t1483_CustomAttributesCacheGenerator,
	DebuggerDisplayAttribute_t1484_CustomAttributesCacheGenerator,
	DebuggerStepThroughAttribute_t1485_CustomAttributesCacheGenerator,
	DebuggerTypeProxyAttribute_t1486_CustomAttributesCacheGenerator,
	StackFrame_t46_CustomAttributesCacheGenerator,
	StackTrace_t44_CustomAttributesCacheGenerator,
	Calendar_t1488_CustomAttributesCacheGenerator,
	CompareInfo_t1357_CustomAttributesCacheGenerator,
	CompareOptions_t1492_CustomAttributesCacheGenerator,
	CultureInfo_t750_CustomAttributesCacheGenerator,
	CultureInfo_t750_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19,
	CultureInfo_t750_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A,
	DateTimeFormatFlags_t1496_CustomAttributesCacheGenerator,
	DateTimeFormatInfo_t1494_CustomAttributesCacheGenerator,
	DateTimeStyles_t1497_CustomAttributesCacheGenerator,
	DaylightTime_t1498_CustomAttributesCacheGenerator,
	GregorianCalendar_t1499_CustomAttributesCacheGenerator,
	GregorianCalendarTypes_t1500_CustomAttributesCacheGenerator,
	NumberFormatInfo_t1493_CustomAttributesCacheGenerator,
	NumberStyles_t1501_CustomAttributesCacheGenerator,
	TextInfo_t1408_CustomAttributesCacheGenerator,
	TextInfo_t1408_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7650,
	TextInfo_t1408_CustomAttributesCacheGenerator_TextInfo_t1408____CultureName_PropertyInfo,
	UnicodeCategory_t1210_CustomAttributesCacheGenerator,
	IsolatedStorageException_t1503_CustomAttributesCacheGenerator,
	BinaryReader_t1505_CustomAttributesCacheGenerator,
	BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m7680,
	BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m7683,
	BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m7684,
	BinaryReader_t1505_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m7685,
	Directory_t1506_CustomAttributesCacheGenerator,
	DirectoryInfo_t1507_CustomAttributesCacheGenerator,
	DirectoryNotFoundException_t1509_CustomAttributesCacheGenerator,
	EndOfStreamException_t1510_CustomAttributesCacheGenerator,
	File_t1511_CustomAttributesCacheGenerator,
	FileAccess_t1161_CustomAttributesCacheGenerator,
	FileAttributes_t1512_CustomAttributesCacheGenerator,
	FileMode_t1513_CustomAttributesCacheGenerator,
	FileNotFoundException_t1514_CustomAttributesCacheGenerator,
	FileOptions_t1515_CustomAttributesCacheGenerator,
	FileShare_t1516_CustomAttributesCacheGenerator,
	FileStream_t1351_CustomAttributesCacheGenerator,
	FileSystemInfo_t1508_CustomAttributesCacheGenerator,
	FileSystemInfo_t1508_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m7764,
	IOException_t1358_CustomAttributesCacheGenerator,
	MemoryStream_t742_CustomAttributesCacheGenerator,
	Path_t1184_CustomAttributesCacheGenerator,
	Path_t1184_CustomAttributesCacheGenerator_InvalidPathChars,
	PathTooLongException_t1524_CustomAttributesCacheGenerator,
	SeekOrigin_t1364_CustomAttributesCacheGenerator,
	Stream_t1294_CustomAttributesCacheGenerator,
	StreamReader_t1529_CustomAttributesCacheGenerator,
	StreamWriter_t1530_CustomAttributesCacheGenerator,
	StringReader_t741_CustomAttributesCacheGenerator,
	TextReader_t1456_CustomAttributesCacheGenerator,
	TextWriter_t1183_CustomAttributesCacheGenerator,
	AssemblyBuilder_t1538_CustomAttributesCacheGenerator,
	ConstructorBuilder_t1541_CustomAttributesCacheGenerator,
	ConstructorBuilder_t1541_CustomAttributesCacheGenerator_ConstructorBuilder_t1541____CallingConvention_PropertyInfo,
	EnumBuilder_t1542_CustomAttributesCacheGenerator,
	EnumBuilder_t1542_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m8003,
	FieldBuilder_t1544_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m8039,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m8042,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m8083,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m8084,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m8085,
	GenericTypeParameterBuilder_t1546_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t1546_GenericTypeParameterBuilder_MakeGenericType_m8085_Arg0_ParameterInfo,
	MethodBuilder_t1545_CustomAttributesCacheGenerator,
	MethodBuilder_t1545_CustomAttributesCacheGenerator_MethodBuilder_Equals_m8101,
	MethodBuilder_t1545_CustomAttributesCacheGenerator_MethodBuilder_t1545_MethodBuilder_MakeGenericMethod_m8104_Arg0_ParameterInfo,
	ModuleBuilder_t1548_CustomAttributesCacheGenerator,
	ParameterBuilder_t1550_CustomAttributesCacheGenerator,
	PropertyBuilder_t1551_CustomAttributesCacheGenerator,
	TypeBuilder_t1539_CustomAttributesCacheGenerator,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m8148,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m8167,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_t1539_TypeBuilder_MakeGenericType_m8167_Arg0_ParameterInfo,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m8174,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m8175,
	TypeBuilder_t1539_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m8176,
	UnmanagedMarshal_t1543_CustomAttributesCacheGenerator,
	AmbiguousMatchException_t1556_CustomAttributesCacheGenerator,
	Assembly_t1164_CustomAttributesCacheGenerator,
	AssemblyCompanyAttribute_t399_CustomAttributesCacheGenerator,
	AssemblyConfigurationAttribute_t398_CustomAttributesCacheGenerator,
	AssemblyCopyrightAttribute_t401_CustomAttributesCacheGenerator,
	AssemblyDefaultAliasAttribute_t930_CustomAttributesCacheGenerator,
	AssemblyDelaySignAttribute_t935_CustomAttributesCacheGenerator,
	AssemblyDescriptionAttribute_t397_CustomAttributesCacheGenerator,
	AssemblyFileVersionAttribute_t404_CustomAttributesCacheGenerator,
	AssemblyInformationalVersionAttribute_t928_CustomAttributesCacheGenerator,
	AssemblyKeyFileAttribute_t934_CustomAttributesCacheGenerator,
	AssemblyName_t1561_CustomAttributesCacheGenerator,
	AssemblyNameFlags_t1562_CustomAttributesCacheGenerator,
	AssemblyProductAttribute_t400_CustomAttributesCacheGenerator,
	AssemblyTitleAttribute_t403_CustomAttributesCacheGenerator,
	AssemblyTrademarkAttribute_t405_CustomAttributesCacheGenerator,
	Binder_t781_CustomAttributesCacheGenerator,
	Default_t1563_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m8228,
	BindingFlags_t1564_CustomAttributesCacheGenerator,
	CallingConventions_t1565_CustomAttributesCacheGenerator,
	ConstructorInfo_t632_CustomAttributesCacheGenerator,
	ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorName,
	ConstructorInfo_t632_CustomAttributesCacheGenerator_TypeConstructorName,
	ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m3491,
	ConstructorInfo_t632_CustomAttributesCacheGenerator_ConstructorInfo_t632____MemberType_PropertyInfo,
	EventAttributes_t1566_CustomAttributesCacheGenerator,
	EventInfo_t_CustomAttributesCacheGenerator,
	FieldAttributes_t1568_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m8261,
	MemberTypes_t1570_CustomAttributesCacheGenerator,
	MethodAttributes_t1571_CustomAttributesCacheGenerator,
	MethodBase_t47_CustomAttributesCacheGenerator,
	MethodBase_t47_CustomAttributesCacheGenerator_MethodBase_Invoke_m8278,
	MethodBase_t47_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m8283,
	MethodImplAttributes_t1572_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m8290_Arg0_ParameterInfo,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m8291,
	Missing_t1573_CustomAttributesCacheGenerator,
	Missing_t1573_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297,
	Module_t1549_CustomAttributesCacheGenerator,
	PInfo_t1581_CustomAttributesCacheGenerator,
	ParameterAttributes_t1583_CustomAttributesCacheGenerator,
	ParameterInfo_t775_CustomAttributesCacheGenerator,
	ParameterModifier_t1584_CustomAttributesCacheGenerator,
	Pointer_t1585_CustomAttributesCacheGenerator,
	ProcessorArchitecture_t1586_CustomAttributesCacheGenerator,
	PropertyAttributes_t1587_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m8447,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m8448,
	StrongNameKeyPair_t1560_CustomAttributesCacheGenerator,
	TargetException_t1588_CustomAttributesCacheGenerator,
	TargetInvocationException_t1589_CustomAttributesCacheGenerator,
	TargetParameterCountException_t1590_CustomAttributesCacheGenerator,
	TypeAttributes_t1591_CustomAttributesCacheGenerator,
	NeutralResourcesLanguageAttribute_t931_CustomAttributesCacheGenerator,
	SatelliteContractVersionAttribute_t929_CustomAttributesCacheGenerator,
	CompilationRelaxations_t1592_CustomAttributesCacheGenerator,
	CompilationRelaxationsAttribute_t933_CustomAttributesCacheGenerator,
	DefaultDependencyAttribute_t1593_CustomAttributesCacheGenerator,
	IsVolatile_t1594_CustomAttributesCacheGenerator,
	StringFreezingAttribute_t1596_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m8464,
	CriticalFinalizerObject_t1599_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m8465,
	ReliabilityContractAttribute_t1600_CustomAttributesCacheGenerator,
	ActivationArguments_t1601_CustomAttributesCacheGenerator,
	CallingConvention_t1602_CustomAttributesCacheGenerator,
	CharSet_t1603_CustomAttributesCacheGenerator,
	ClassInterfaceAttribute_t1604_CustomAttributesCacheGenerator,
	ClassInterfaceType_t1605_CustomAttributesCacheGenerator,
	ComDefaultInterfaceAttribute_t1606_CustomAttributesCacheGenerator,
	ComInterfaceType_t1607_CustomAttributesCacheGenerator,
	DispIdAttribute_t1608_CustomAttributesCacheGenerator,
	GCHandle_t1609_CustomAttributesCacheGenerator,
	GCHandleType_t1610_CustomAttributesCacheGenerator,
	InterfaceTypeAttribute_t1611_CustomAttributesCacheGenerator,
	Marshal_t1612_CustomAttributesCacheGenerator,
	MarshalDirectiveException_t1613_CustomAttributesCacheGenerator,
	PreserveSigAttribute_t1614_CustomAttributesCacheGenerator,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle__ctor_m8487,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Close_m8488,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m8489,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m8490,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m8491,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Dispose_m8492,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_Dispose_m8493,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m10954,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m8494,
	SafeHandle_t1390_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m10955,
	TypeLibImportClassAttribute_t1615_CustomAttributesCacheGenerator,
	TypeLibVersionAttribute_t1616_CustomAttributesCacheGenerator,
	UnmanagedType_t1617_CustomAttributesCacheGenerator,
	_Activator_t2007_CustomAttributesCacheGenerator,
	_Assembly_t1997_CustomAttributesCacheGenerator,
	_AssemblyBuilder_t1988_CustomAttributesCacheGenerator,
	_AssemblyName_t1998_CustomAttributesCacheGenerator,
	_ConstructorBuilder_t1989_CustomAttributesCacheGenerator,
	_ConstructorInfo_t1999_CustomAttributesCacheGenerator,
	_EnumBuilder_t1990_CustomAttributesCacheGenerator,
	_EventInfo_t2000_CustomAttributesCacheGenerator,
	_FieldBuilder_t1991_CustomAttributesCacheGenerator,
	_FieldInfo_t2001_CustomAttributesCacheGenerator,
	_MethodBase_t2002_CustomAttributesCacheGenerator,
	_MethodBuilder_t1992_CustomAttributesCacheGenerator,
	_MethodInfo_t2003_CustomAttributesCacheGenerator,
	_Module_t2004_CustomAttributesCacheGenerator,
	_ModuleBuilder_t1993_CustomAttributesCacheGenerator,
	_ParameterBuilder_t1994_CustomAttributesCacheGenerator,
	_ParameterInfo_t2005_CustomAttributesCacheGenerator,
	_PropertyBuilder_t1995_CustomAttributesCacheGenerator,
	_PropertyInfo_t2006_CustomAttributesCacheGenerator,
	_Thread_t2008_CustomAttributesCacheGenerator,
	_TypeBuilder_t1996_CustomAttributesCacheGenerator,
	IActivator_t1618_CustomAttributesCacheGenerator,
	IConstructionCallMessage_t1918_CustomAttributesCacheGenerator,
	UrlAttribute_t1624_CustomAttributesCacheGenerator,
	UrlAttribute_t1624_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m8508,
	UrlAttribute_t1624_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m8509,
	ChannelServices_t1628_CustomAttributesCacheGenerator,
	ChannelServices_t1628_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m8513,
	CrossAppDomainSink_t1631_CustomAttributesCacheGenerator,
	IChannel_t1919_CustomAttributesCacheGenerator,
	IChannelReceiver_t1933_CustomAttributesCacheGenerator,
	IChannelSender_t2009_CustomAttributesCacheGenerator,
	Context_t1632_CustomAttributesCacheGenerator,
	ContextAttribute_t1625_CustomAttributesCacheGenerator,
	IContextAttribute_t1931_CustomAttributesCacheGenerator,
	IContextProperty_t1920_CustomAttributesCacheGenerator,
	IContributeClientContextSink_t2010_CustomAttributesCacheGenerator,
	IContributeServerContextSink_t2011_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t1635_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t1635_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m8543,
	SynchronizationAttribute_t1635_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m8544,
	AsyncResult_t1642_CustomAttributesCacheGenerator,
	ConstructionCall_t1643_CustomAttributesCacheGenerator,
	ConstructionCall_t1643_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20,
	ConstructionCallDictionary_t1645_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23,
	ConstructionCallDictionary_t1645_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24,
	Header_t1648_CustomAttributesCacheGenerator,
	IMessage_t1641_CustomAttributesCacheGenerator,
	IMessageCtrl_t1640_CustomAttributesCacheGenerator,
	IMessageSink_t1218_CustomAttributesCacheGenerator,
	IMethodCallMessage_t1922_CustomAttributesCacheGenerator,
	IMethodMessage_t1653_CustomAttributesCacheGenerator,
	IMethodReturnMessage_t1921_CustomAttributesCacheGenerator,
	IRemotingFormatter_t2012_CustomAttributesCacheGenerator,
	LogicalCallContext_t1650_CustomAttributesCacheGenerator,
	MethodCall_t1644_CustomAttributesCacheGenerator,
	MethodCall_t1644_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F,
	MethodDictionary_t1646_CustomAttributesCacheGenerator,
	MethodDictionary_t1646_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21,
	MethodDictionary_t1646_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22,
	RemotingSurrogateSelector_t1658_CustomAttributesCacheGenerator,
	ReturnMessage_t1659_CustomAttributesCacheGenerator,
	ProxyAttribute_t1660_CustomAttributesCacheGenerator,
	ProxyAttribute_t1660_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m8680,
	ProxyAttribute_t1660_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m8681,
	RealProxy_t1661_CustomAttributesCacheGenerator,
	ITrackingHandler_t1936_CustomAttributesCacheGenerator,
	TrackingServices_t1665_CustomAttributesCacheGenerator,
	ActivatedClientTypeEntry_t1666_CustomAttributesCacheGenerator,
	IChannelInfo_t1672_CustomAttributesCacheGenerator,
	IEnvoyInfo_t1674_CustomAttributesCacheGenerator,
	IRemotingTypeInfo_t1673_CustomAttributesCacheGenerator,
	ObjRef_t1669_CustomAttributesCacheGenerator,
	ObjRef_t1669_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26,
	ObjRef_t1669_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m8718,
	RemotingConfiguration_t1675_CustomAttributesCacheGenerator,
	RemotingException_t1676_CustomAttributesCacheGenerator,
	RemotingServices_t1678_CustomAttributesCacheGenerator,
	RemotingServices_t1678_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m8738,
	RemotingServices_t1678_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m8742,
	TypeEntry_t1667_CustomAttributesCacheGenerator,
	WellKnownObjectMode_t1683_CustomAttributesCacheGenerator,
	BinaryFormatter_t1677_CustomAttributesCacheGenerator,
	BinaryFormatter_t1677_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField,
	BinaryFormatter_t1677_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m8774,
	FormatterAssemblyStyle_t1696_CustomAttributesCacheGenerator,
	FormatterTypeStyle_t1697_CustomAttributesCacheGenerator,
	TypeFilterLevel_t1698_CustomAttributesCacheGenerator,
	FormatterConverter_t1699_CustomAttributesCacheGenerator,
	FormatterServices_t1700_CustomAttributesCacheGenerator,
	IDeserializationCallback_t1206_CustomAttributesCacheGenerator,
	IFormatter_t2014_CustomAttributesCacheGenerator,
	IFormatterConverter_t1716_CustomAttributesCacheGenerator,
	IObjectReference_t1941_CustomAttributesCacheGenerator,
	ISerializationSurrogate_t1708_CustomAttributesCacheGenerator,
	ISurrogateSelector_t1657_CustomAttributesCacheGenerator,
	ObjectManager_t1694_CustomAttributesCacheGenerator,
	OnDeserializedAttribute_t1709_CustomAttributesCacheGenerator,
	OnDeserializingAttribute_t1710_CustomAttributesCacheGenerator,
	OnSerializedAttribute_t1711_CustomAttributesCacheGenerator,
	OnSerializingAttribute_t1712_CustomAttributesCacheGenerator,
	SerializationBinder_t1689_CustomAttributesCacheGenerator,
	SerializationEntry_t1715_CustomAttributesCacheGenerator,
	SerializationException_t1160_CustomAttributesCacheGenerator,
	SerializationInfo_t725_CustomAttributesCacheGenerator,
	SerializationInfo_t725_CustomAttributesCacheGenerator_SerializationInfo__ctor_m8878,
	SerializationInfo_t725_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m8884,
	SerializationInfoEnumerator_t1717_CustomAttributesCacheGenerator,
	StreamingContext_t726_CustomAttributesCacheGenerator,
	StreamingContextStates_t1718_CustomAttributesCacheGenerator,
	X509Certificate_t1037_CustomAttributesCacheGenerator,
	X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m4949,
	X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_GetName_m4950,
	X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Equals_m4941,
	X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Import_m4771,
	X509Certificate_t1037_CustomAttributesCacheGenerator_X509Certificate_Reset_m4773,
	X509KeyStorageFlags_t1209_CustomAttributesCacheGenerator,
	AsymmetricAlgorithm_t1024_CustomAttributesCacheGenerator,
	AsymmetricKeyExchangeFormatter_t1719_CustomAttributesCacheGenerator,
	AsymmetricSignatureDeformatter_t1299_CustomAttributesCacheGenerator,
	AsymmetricSignatureFormatter_t1301_CustomAttributesCacheGenerator,
	CipherMode_t1363_CustomAttributesCacheGenerator,
	CryptoConfig_t1175_CustomAttributesCacheGenerator,
	CryptoConfig_t1175_CustomAttributesCacheGenerator_CryptoConfig_t1175_CryptoConfig_CreateFromName_m4805_Arg1_ParameterInfo,
	CryptographicException_t1171_CustomAttributesCacheGenerator,
	CryptographicUnexpectedOperationException_t1176_CustomAttributesCacheGenerator,
	CspParameters_t1342_CustomAttributesCacheGenerator,
	CspProviderFlags_t1721_CustomAttributesCacheGenerator,
	DES_t1353_CustomAttributesCacheGenerator,
	DESCryptoServiceProvider_t1724_CustomAttributesCacheGenerator,
	DSA_t1148_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1169_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1169_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1169____PublicOnly_PropertyInfo,
	DSAParameters_t1170_CustomAttributesCacheGenerator,
	DSASignatureDeformatter_t1349_CustomAttributesCacheGenerator,
	DSASignatureFormatter_t1725_CustomAttributesCacheGenerator,
	HMAC_t1346_CustomAttributesCacheGenerator,
	HMACMD5_t1726_CustomAttributesCacheGenerator,
	HMACRIPEMD160_t1727_CustomAttributesCacheGenerator,
	HMACSHA1_t1345_CustomAttributesCacheGenerator,
	HMACSHA256_t1728_CustomAttributesCacheGenerator,
	HMACSHA384_t1729_CustomAttributesCacheGenerator,
	HMACSHA512_t1730_CustomAttributesCacheGenerator,
	HashAlgorithm_t1241_CustomAttributesCacheGenerator,
	ICryptoTransform_t1272_CustomAttributesCacheGenerator,
	ICspAsymmetricAlgorithm_t2015_CustomAttributesCacheGenerator,
	KeyedHashAlgorithm_t1265_CustomAttributesCacheGenerator,
	MACTripleDES_t1731_CustomAttributesCacheGenerator,
	MD5_t1347_CustomAttributesCacheGenerator,
	MD5CryptoServiceProvider_t1732_CustomAttributesCacheGenerator,
	PaddingMode_t1733_CustomAttributesCacheGenerator,
	RC2_t1354_CustomAttributesCacheGenerator,
	RC2CryptoServiceProvider_t1734_CustomAttributesCacheGenerator,
	RIPEMD160_t1736_CustomAttributesCacheGenerator,
	RIPEMD160Managed_t1737_CustomAttributesCacheGenerator,
	RSA_t1149_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1166_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1166_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1166____PublicOnly_PropertyInfo,
	RSAPKCS1KeyExchangeFormatter_t1359_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureDeformatter_t1350_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureFormatter_t1739_CustomAttributesCacheGenerator,
	RSAParameters_t1168_CustomAttributesCacheGenerator,
	Rijndael_t1356_CustomAttributesCacheGenerator,
	RijndaelManaged_t1740_CustomAttributesCacheGenerator,
	RijndaelManagedTransform_t1742_CustomAttributesCacheGenerator,
	SHA1_t1180_CustomAttributesCacheGenerator,
	SHA1CryptoServiceProvider_t1744_CustomAttributesCacheGenerator,
	SHA1Managed_t1745_CustomAttributesCacheGenerator,
	SHA256_t1348_CustomAttributesCacheGenerator,
	SHA256Managed_t1746_CustomAttributesCacheGenerator,
	SHA384_t1747_CustomAttributesCacheGenerator,
	SHA384Managed_t1749_CustomAttributesCacheGenerator,
	SHA512_t1750_CustomAttributesCacheGenerator,
	SHA512Managed_t1751_CustomAttributesCacheGenerator,
	SignatureDescription_t1753_CustomAttributesCacheGenerator,
	SymmetricAlgorithm_t1248_CustomAttributesCacheGenerator,
	ToBase64Transform_t1756_CustomAttributesCacheGenerator,
	TripleDES_t1355_CustomAttributesCacheGenerator,
	TripleDESCryptoServiceProvider_t1757_CustomAttributesCacheGenerator,
	StrongNamePublicKeyBlob_t1759_CustomAttributesCacheGenerator,
	ApplicationTrust_t1761_CustomAttributesCacheGenerator,
	Evidence_t1558_CustomAttributesCacheGenerator,
	Evidence_t1558_CustomAttributesCacheGenerator_Evidence_Equals_m9224,
	Evidence_t1558_CustomAttributesCacheGenerator_Evidence_GetHashCode_m9226,
	Hash_t1763_CustomAttributesCacheGenerator,
	IIdentityPermissionFactory_t2017_CustomAttributesCacheGenerator,
	StrongName_t1764_CustomAttributesCacheGenerator,
	IPrincipal_t1803_CustomAttributesCacheGenerator,
	PrincipalPolicy_t1765_CustomAttributesCacheGenerator,
	IPermission_t1767_CustomAttributesCacheGenerator,
	ISecurityEncodable_t2018_CustomAttributesCacheGenerator,
	SecurityElement_t1451_CustomAttributesCacheGenerator,
	SecurityException_t1768_CustomAttributesCacheGenerator,
	SecurityException_t1768_CustomAttributesCacheGenerator_SecurityException_t1768____Demanded_PropertyInfo,
	SecuritySafeCriticalAttribute_t794_CustomAttributesCacheGenerator,
	SuppressUnmanagedCodeSecurityAttribute_t1769_CustomAttributesCacheGenerator,
	UnverifiableCodeAttribute_t1770_CustomAttributesCacheGenerator,
	ASCIIEncoding_t1771_CustomAttributesCacheGenerator,
	ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m9286,
	ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m9287,
	ASCIIEncoding_t1771_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m9288,
	Decoder_t1504_CustomAttributesCacheGenerator,
	Decoder_t1504_CustomAttributesCacheGenerator_Decoder_t1504____Fallback_PropertyInfo,
	Decoder_t1504_CustomAttributesCacheGenerator_Decoder_t1504____FallbackBuffer_PropertyInfo,
	DecoderReplacementFallback_t1777_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m9311,
	EncoderReplacementFallback_t1784_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m9341,
	Encoding_t705_CustomAttributesCacheGenerator,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705_Encoding_InvokeI18N_m9372_Arg1_ParameterInfo,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_GetByteCount_m9388,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_GetBytes_m9389,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____IsReadOnly_PropertyInfo,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____DecoderFallback_PropertyInfo,
	Encoding_t705_CustomAttributesCacheGenerator_Encoding_t705____EncoderFallback_PropertyInfo,
	StringBuilder_t338_CustomAttributesCacheGenerator,
	StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1464,
	StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1463,
	StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_t338_StringBuilder_AppendFormat_m5805_Arg1_ParameterInfo,
	StringBuilder_t338_CustomAttributesCacheGenerator_StringBuilder_t338_StringBuilder_AppendFormat_m9420_Arg2_ParameterInfo,
	UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m9430,
	UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m9431,
	UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m9440,
	UTF32Encoding_t1789_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m9442,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m9450,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m9451,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m9463,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m9464,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m9465,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m9466,
	UTF7Encoding_t1792_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m9467,
	UTF8Encoding_t1794_CustomAttributesCacheGenerator,
	UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m9476,
	UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m9481,
	UTF8Encoding_t1794_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m9497,
	UnicodeEncoding_t1796_CustomAttributesCacheGenerator,
	UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m9505,
	UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m9508,
	UnicodeEncoding_t1796_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m9512,
	EventResetMode_t1797_CustomAttributesCacheGenerator,
	EventWaitHandle_t1798_CustomAttributesCacheGenerator,
	ExecutionContext_t1638_CustomAttributesCacheGenerator_ExecutionContext__ctor_m9524,
	ExecutionContext_t1638_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m9525,
	Interlocked_t1799_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m9526,
	ManualResetEvent_t1293_CustomAttributesCacheGenerator,
	Monitor_t1800_CustomAttributesCacheGenerator,
	Monitor_t1800_CustomAttributesCacheGenerator_Monitor_Exit_m4709,
	Mutex_t1633_CustomAttributesCacheGenerator,
	Mutex_t1633_CustomAttributesCacheGenerator_Mutex__ctor_m9527,
	Mutex_t1633_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m9530,
	SynchronizationLockException_t1802_CustomAttributesCacheGenerator,
	Thread_t1634_CustomAttributesCacheGenerator,
	Thread_t1634_CustomAttributesCacheGenerator_local_slots,
	Thread_t1634_CustomAttributesCacheGenerator__ec,
	Thread_t1634_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m9540,
	Thread_t1634_CustomAttributesCacheGenerator_Thread_Finalize_m9551,
	Thread_t1634_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m9554,
	Thread_t1634_CustomAttributesCacheGenerator_Thread_GetHashCode_m9555,
	ThreadAbortException_t1804_CustomAttributesCacheGenerator,
	ThreadInterruptedException_t1805_CustomAttributesCacheGenerator,
	ThreadState_t1806_CustomAttributesCacheGenerator,
	ThreadStateException_t1807_CustomAttributesCacheGenerator,
	WaitHandle_t1340_CustomAttributesCacheGenerator,
	WaitHandle_t1340_CustomAttributesCacheGenerator_WaitHandle_t1340____Handle_PropertyInfo,
	AccessViolationException_t1808_CustomAttributesCacheGenerator,
	ActivationContext_t1809_CustomAttributesCacheGenerator,
	ActivationContext_t1809_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m9575,
	Activator_t1810_CustomAttributesCacheGenerator,
	Activator_t1810_CustomAttributesCacheGenerator_Activator_t1810_Activator_CreateInstance_m9580_Arg1_ParameterInfo,
	AppDomain_t1811_CustomAttributesCacheGenerator,
	AppDomain_t1811_CustomAttributesCacheGenerator_type_resolve_in_progress,
	AppDomain_t1811_CustomAttributesCacheGenerator_assembly_resolve_in_progress,
	AppDomain_t1811_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly,
	AppDomain_t1811_CustomAttributesCacheGenerator__principal,
	AppDomainManager_t1812_CustomAttributesCacheGenerator,
	AppDomainSetup_t1819_CustomAttributesCacheGenerator,
	ApplicationException_t1820_CustomAttributesCacheGenerator,
	ApplicationIdentity_t1813_CustomAttributesCacheGenerator,
	ApplicationIdentity_t1813_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9601,
	ArgumentException_t387_CustomAttributesCacheGenerator,
	ArgumentNullException_t749_CustomAttributesCacheGenerator,
	ArgumentOutOfRangeException_t751_CustomAttributesCacheGenerator,
	ArithmeticException_t1341_CustomAttributesCacheGenerator,
	ArrayTypeMismatchException_t1821_CustomAttributesCacheGenerator,
	AssemblyLoadEventArgs_t1822_CustomAttributesCacheGenerator,
	AttributeTargets_t1823_CustomAttributesCacheGenerator,
	Buffer_t1824_CustomAttributesCacheGenerator,
	CharEnumerator_t1825_CustomAttributesCacheGenerator,
	ContextBoundObject_t1826_CustomAttributesCacheGenerator,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9653,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9656,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9657,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToBoolean_m9658,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9667,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9671,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9672,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToByte_m9673,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9678,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9681,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9682,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToChar_m9683,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9691,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9692,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9693,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDateTime_m9694,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9701,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9704,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9705,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDecimal_m9706,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9715,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9718,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9719,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToDouble_m9720,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9729,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9731,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9732,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt16_m9733,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9743,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9746,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9747,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt32_m9748,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9757,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9761,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9762,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToInt64_m9763,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9766,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9767,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9768,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9769,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9770,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9771,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9772,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9773,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9774,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9775,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9776,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9777,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9778,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSByte_m9779,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9787,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9790,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9791,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToSingle_m9792,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9796,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9797,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9798,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9799,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9800,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9801,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9802,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9803,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9804,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9805,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9806,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9807,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9808,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m3429,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt16_m9809,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m3393,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9810,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9811,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9812,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9813,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9814,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9815,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9816,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9817,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9818,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9819,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9820,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9821,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m3392,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt32_m9822,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9823,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9824,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9825,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9826,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9827,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9828,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9829,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9830,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9831,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9832,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9833,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9834,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9835,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m3430,
	Convert_t737_CustomAttributesCacheGenerator_Convert_ToUInt64_m9836,
	DBNull_t1827_CustomAttributesCacheGenerator,
	DateTimeKind_t1829_CustomAttributesCacheGenerator,
	DateTimeOffset_t768_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9934,
	DayOfWeek_t1831_CustomAttributesCacheGenerator,
	DivideByZeroException_t1834_CustomAttributesCacheGenerator,
	DllNotFoundException_t1835_CustomAttributesCacheGenerator,
	EntryPointNotFoundException_t1837_CustomAttributesCacheGenerator,
	MonoEnumInfo_t1842_CustomAttributesCacheGenerator_cache,
	Environment_t1845_CustomAttributesCacheGenerator,
	SpecialFolder_t1843_CustomAttributesCacheGenerator,
	EventArgs_t1249_CustomAttributesCacheGenerator,
	ExecutionEngineException_t1846_CustomAttributesCacheGenerator,
	FieldAccessException_t1847_CustomAttributesCacheGenerator,
	FlagsAttribute_t406_CustomAttributesCacheGenerator,
	FormatException_t743_CustomAttributesCacheGenerator,
	GC_t1849_CustomAttributesCacheGenerator_GC_SuppressFinalize_m5812,
	Guid_t769_CustomAttributesCacheGenerator,
	ICustomFormatter_t1926_CustomAttributesCacheGenerator,
	IFormatProvider_t1909_CustomAttributesCacheGenerator,
	IndexOutOfRangeException_t738_CustomAttributesCacheGenerator,
	InvalidCastException_t1850_CustomAttributesCacheGenerator,
	InvalidOperationException_t1159_CustomAttributesCacheGenerator,
	LoaderOptimization_t1851_CustomAttributesCacheGenerator,
	LoaderOptimization_t1851_CustomAttributesCacheGenerator_DomainMask,
	LoaderOptimization_t1851_CustomAttributesCacheGenerator_DisallowBindings,
	Math_t1852_CustomAttributesCacheGenerator_Math_Max_m5816,
	Math_t1852_CustomAttributesCacheGenerator_Math_Min_m10029,
	Math_t1852_CustomAttributesCacheGenerator_Math_Sqrt_m10036,
	MemberAccessException_t1848_CustomAttributesCacheGenerator,
	MethodAccessException_t1853_CustomAttributesCacheGenerator,
	MissingFieldException_t1854_CustomAttributesCacheGenerator,
	MissingMemberException_t1855_CustomAttributesCacheGenerator,
	MissingMethodException_t1856_CustomAttributesCacheGenerator,
	MulticastNotSupportedException_t1862_CustomAttributesCacheGenerator,
	NonSerializedAttribute_t1863_CustomAttributesCacheGenerator,
	NotImplementedException_t1165_CustomAttributesCacheGenerator,
	NotSupportedException_t32_CustomAttributesCacheGenerator,
	NullReferenceException_t727_CustomAttributesCacheGenerator,
	NumberFormatter_t1865_CustomAttributesCacheGenerator_threadNumberFormatter,
	ObjectDisposedException_t1344_CustomAttributesCacheGenerator,
	OperatingSystem_t1844_CustomAttributesCacheGenerator,
	OutOfMemoryException_t1866_CustomAttributesCacheGenerator,
	OverflowException_t1867_CustomAttributesCacheGenerator,
	PlatformID_t1868_CustomAttributesCacheGenerator,
	Random_t614_CustomAttributesCacheGenerator,
	RankException_t1869_CustomAttributesCacheGenerator,
	ResolveEventArgs_t1870_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t1871_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t1871_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m10254,
	StringComparer_t734_CustomAttributesCacheGenerator,
	StringComparison_t1874_CustomAttributesCacheGenerator,
	StringSplitOptions_t1875_CustomAttributesCacheGenerator,
	SystemException_t1181_CustomAttributesCacheGenerator,
	ThreadStaticAttribute_t1876_CustomAttributesCacheGenerator,
	TimeSpan_t1051_CustomAttributesCacheGenerator,
	TimeZone_t1877_CustomAttributesCacheGenerator,
	TypeCode_t1879_CustomAttributesCacheGenerator,
	TypeInitializationException_t1880_CustomAttributesCacheGenerator,
	TypeLoadException_t1836_CustomAttributesCacheGenerator,
	UnauthorizedAccessException_t1881_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m10338,
	UnhandledExceptionEventArgs_t1882_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m10339,
	Version_t1006_CustomAttributesCacheGenerator,
	WeakReference_t1670_CustomAttributesCacheGenerator,
	MemberFilter_t1370_CustomAttributesCacheGenerator,
	TypeFilter_t1574_CustomAttributesCacheGenerator,
	HeaderHandler_t1887_CustomAttributesCacheGenerator,
	AppDomainInitializer_t1818_CustomAttributesCacheGenerator,
	AssemblyLoadEventHandler_t1814_CustomAttributesCacheGenerator,
	EventHandler_t1816_CustomAttributesCacheGenerator,
	ResolveEventHandler_t1815_CustomAttributesCacheGenerator,
	UnhandledExceptionEventHandler_t1817_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t1908_CustomAttributesCacheGenerator,
};
