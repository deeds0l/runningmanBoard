﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct Enumerator_t2541;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2535;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m12833_gshared (Enumerator_t2541 * __this, Dictionary_2_t2535 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m12833(__this, ___dictionary, method) (( void (*) (Enumerator_t2541 *, Dictionary_2_t2535 *, const MethodInfo*))Enumerator__ctor_m12833_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12834(__this, method) (( Object_t * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1147  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836(__this, method) (( Object_t * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837(__this, method) (( Object_t * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12838_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12838(__this, method) (( bool (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_MoveNext_m12838_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2536  Enumerator_get_Current_m12839_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12839(__this, method) (( KeyValuePair_2_t2536  (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_get_Current_m12839_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m12840_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m12840(__this, method) (( int32_t (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_get_CurrentKey_m12840_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m12841_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m12841(__this, method) (( Object_t * (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_get_CurrentValue_m12841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m12842_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m12842(__this, method) (( void (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_VerifyState_m12842_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m12843_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m12843(__this, method) (( void (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_VerifyCurrent_m12843_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m12844_gshared (Enumerator_t2541 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12844(__this, method) (( void (*) (Enumerator_t2541 *, const MethodInfo*))Enumerator_Dispose_m12844_gshared)(__this, method)
