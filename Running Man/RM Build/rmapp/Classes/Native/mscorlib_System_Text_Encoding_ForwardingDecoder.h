﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t705;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t1786  : public Decoder_t1504
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t705 * ___encoding_2;
};
