﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t355;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t588;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t176  : public Object_t33
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t588 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t176_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t355 * ___textureRebuilt_2;
};
