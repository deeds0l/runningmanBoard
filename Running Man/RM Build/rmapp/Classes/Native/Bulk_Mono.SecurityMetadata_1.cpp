﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t1322_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate__ctor_m5751_ParameterInfos[] = 
{
	{"context", 0, 134218531, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218532, 0, &ByteU5BU5D_t546_0_0_0},
};
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificate__ctor_m5751_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificate__ctor_m5751/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate__ctor_m5751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Update()
extern const MethodInfo TlsServerCertificate_Update_m5752_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificate_Update_m5752/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m5753_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsSsl3_m5753/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsTls1()
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m5754_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsTls1_m5754/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1035_0_0_0;
extern const Il2CppType X509Certificate_t1035_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate_checkCertificateUsage_m5755_ParameterInfos[] = 
{
	{"cert", 0, 134218533, 0, &X509Certificate_t1035_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkCertificateUsage_m5755_MethodInfo = 
{
	"checkCertificateUsage"/* name */
	, (methodPointerType)&TlsServerCertificate_checkCertificateUsage_m5755/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate_checkCertificateUsage_m5755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1174_0_0_0;
extern const Il2CppType X509CertificateCollection_t1174_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate_validateCertificates_m5756_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, 0, &X509CertificateCollection_t1174_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo TlsServerCertificate_validateCertificates_m5756_MethodInfo = 
{
	"validateCertificates"/* name */
	, (methodPointerType)&TlsServerCertificate_validateCertificates_m5756/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate_validateCertificates_m5756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1035_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate_checkServerIdentity_m5757_ParameterInfos[] = 
{
	{"cert", 0, 134218535, 0, &X509Certificate_t1035_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkServerIdentity(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkServerIdentity_m5757_MethodInfo = 
{
	"checkServerIdentity"/* name */
	, (methodPointerType)&TlsServerCertificate_checkServerIdentity_m5757/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate_checkServerIdentity_m5757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate_checkDomainName_m5758_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkDomainName(System.String)
extern const MethodInfo TlsServerCertificate_checkDomainName_m5758_MethodInfo = 
{
	"checkDomainName"/* name */
	, (methodPointerType)&TlsServerCertificate_checkDomainName_m5758/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate_checkDomainName_m5758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1322_TlsServerCertificate_Match_m5759_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, 0, &String_t_0_0_0},
	{"pattern", 1, 134218538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Match(System.String,System.String)
extern const MethodInfo TlsServerCertificate_Match_m5759_MethodInfo = 
{
	"Match"/* name */
	, (methodPointerType)&TlsServerCertificate_Match_m5759/* method */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1322_TlsServerCertificate_Match_m5759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificate_t1322_MethodInfos[] =
{
	&TlsServerCertificate__ctor_m5751_MethodInfo,
	&TlsServerCertificate_Update_m5752_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m5753_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m5754_MethodInfo,
	&TlsServerCertificate_checkCertificateUsage_m5755_MethodInfo,
	&TlsServerCertificate_validateCertificates_m5756_MethodInfo,
	&TlsServerCertificate_checkServerIdentity_m5757_MethodInfo,
	&TlsServerCertificate_checkDomainName_m5758_MethodInfo,
	&TlsServerCertificate_Match_m5759_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
extern const MethodInfo Stream_Dispose_m5923_MethodInfo;
extern const MethodInfo TlsStream_get_CanRead_m5692_MethodInfo;
extern const MethodInfo TlsStream_get_CanSeek_m5693_MethodInfo;
extern const MethodInfo TlsStream_get_CanWrite_m5691_MethodInfo;
extern const MethodInfo TlsStream_get_Length_m5696_MethodInfo;
extern const MethodInfo TlsStream_get_Position_m5694_MethodInfo;
extern const MethodInfo TlsStream_set_Position_m5695_MethodInfo;
extern const MethodInfo Stream_Dispose_m5876_MethodInfo;
extern const MethodInfo Stream_Close_m5875_MethodInfo;
extern const MethodInfo TlsStream_Flush_m5709_MethodInfo;
extern const MethodInfo TlsStream_Read_m5712_MethodInfo;
extern const MethodInfo Stream_ReadByte_m5924_MethodInfo;
extern const MethodInfo TlsStream_Seek_m5711_MethodInfo;
extern const MethodInfo TlsStream_SetLength_m5710_MethodInfo;
extern const MethodInfo TlsStream_Write_m5713_MethodInfo;
extern const MethodInfo Stream_WriteByte_m5925_MethodInfo;
extern const MethodInfo Stream_BeginRead_m5926_MethodInfo;
extern const MethodInfo Stream_BeginWrite_m5927_MethodInfo;
extern const MethodInfo Stream_EndRead_m5928_MethodInfo;
extern const MethodInfo Stream_EndWrite_m5929_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m5754_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m5753_MethodInfo;
extern const MethodInfo TlsServerCertificate_Update_m5752_MethodInfo;
extern const MethodInfo HandshakeMessage_EncodeMessage_m5722_MethodInfo;
static const Il2CppMethodReference TlsServerCertificate_t1322_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m5754_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m5753_MethodInfo,
	&TlsServerCertificate_Update_m5752_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerCertificate_t1322_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t43_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t1322_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificate_t1322_0_0_0;
extern const Il2CppType TlsServerCertificate_t1322_1_0_0;
extern const Il2CppType HandshakeMessage_t1296_0_0_0;
struct TlsServerCertificate_t1322;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t1322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t1322_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerCertificate_t1322_VTable/* vtableMethods */
	, TlsServerCertificate_t1322_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 458/* fieldStart */

};
TypeInfo TlsServerCertificate_t1322_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificate_t1322_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificate_t1322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t1322_0_0_0/* byval_arg */
	, &TlsServerCertificate_t1322_1_0_0/* this_arg */
	, &TlsServerCertificate_t1322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t1322)/* instance_size */
	, sizeof (TlsServerCertificate_t1322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t1323_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerCertificateRequest_t1323_TlsServerCertificateRequest__ctor_m5760_ParameterInfos[] = 
{
	{"context", 0, 134218539, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218540, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificateRequest__ctor_m5760_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificateRequest__ctor_m5760/* method */
	, &TlsServerCertificateRequest_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerCertificateRequest_t1323_TlsServerCertificateRequest__ctor_m5760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::Update()
extern const MethodInfo TlsServerCertificateRequest_Update_m5761_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_Update_m5761/* method */
	, &TlsServerCertificateRequest_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m5762_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsSsl3_m5762/* method */
	, &TlsServerCertificateRequest_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsTls1()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m5763_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsTls1_m5763/* method */
	, &TlsServerCertificateRequest_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificateRequest_t1323_MethodInfos[] =
{
	&TlsServerCertificateRequest__ctor_m5760_MethodInfo,
	&TlsServerCertificateRequest_Update_m5761_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m5762_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m5763_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m5763_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m5762_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_Update_m5761_MethodInfo;
static const Il2CppMethodReference TlsServerCertificateRequest_t1323_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m5763_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m5762_MethodInfo,
	&TlsServerCertificateRequest_Update_m5761_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerCertificateRequest_t1323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t1323_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificateRequest_t1323_0_0_0;
extern const Il2CppType TlsServerCertificateRequest_t1323_1_0_0;
struct TlsServerCertificateRequest_t1323;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t1323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t1323_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerCertificateRequest_t1323_VTable/* vtableMethods */
	, TlsServerCertificateRequest_t1323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 459/* fieldStart */

};
TypeInfo TlsServerCertificateRequest_t1323_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificateRequest_t1323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificateRequest_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t1323_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t1323_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t1323)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t1323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t1324_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerFinished_t1324_TlsServerFinished__ctor_m5764_ParameterInfos[] = 
{
	{"context", 0, 134218541, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218542, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerFinished__ctor_m5764_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerFinished__ctor_m5764/* method */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerFinished_t1324_TlsServerFinished__ctor_m5764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.cctor()
extern const MethodInfo TlsServerFinished__cctor_m5765_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TlsServerFinished__cctor_m5765/* method */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Update()
extern const MethodInfo TlsServerFinished_Update_m5766_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerFinished_Update_m5766/* method */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsSsl3()
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m5767_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsSsl3_m5767/* method */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsTls1()
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m5768_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsTls1_m5768/* method */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerFinished_t1324_MethodInfos[] =
{
	&TlsServerFinished__ctor_m5764_MethodInfo,
	&TlsServerFinished__cctor_m5765_MethodInfo,
	&TlsServerFinished_Update_m5766_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m5767_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m5768_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m5768_MethodInfo;
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m5767_MethodInfo;
extern const MethodInfo TlsServerFinished_Update_m5766_MethodInfo;
static const Il2CppMethodReference TlsServerFinished_t1324_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m5768_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m5767_MethodInfo,
	&TlsServerFinished_Update_m5766_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerFinished_t1324_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t1324_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerFinished_t1324_0_0_0;
extern const Il2CppType TlsServerFinished_t1324_1_0_0;
struct TlsServerFinished_t1324;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t1324_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t1324_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerFinished_t1324_VTable/* vtableMethods */
	, TlsServerFinished_t1324_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 461/* fieldStart */

};
TypeInfo TlsServerFinished_t1324_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerFinished_t1324_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerFinished_t1324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t1324_0_0_0/* byval_arg */
	, &TlsServerFinished_t1324_1_0_0/* this_arg */
	, &TlsServerFinished_t1324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t1324)/* instance_size */
	, sizeof (TlsServerFinished_t1324)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t1324_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t1325_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerHello_t1325_TlsServerHello__ctor_m5769_ParameterInfos[] = 
{
	{"context", 0, 134218543, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218544, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHello__ctor_m5769_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHello__ctor_m5769/* method */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerHello_t1325_TlsServerHello__ctor_m5769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::Update()
extern const MethodInfo TlsServerHello_Update_m5770_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerHello_Update_m5770/* method */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsSsl3()
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m5771_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsSsl3_m5771/* method */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsTls1()
extern const MethodInfo TlsServerHello_ProcessAsTls1_m5772_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsTls1_m5772/* method */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int16_t448_0_0_0;
extern const Il2CppType Int16_t448_0_0_0;
static const ParameterInfo TlsServerHello_t1325_TlsServerHello_processProtocol_m5773_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, 0, &Int16_t448_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::processProtocol(System.Int16)
extern const MethodInfo TlsServerHello_processProtocol_m5773_MethodInfo = 
{
	"processProtocol"/* name */
	, (methodPointerType)&TlsServerHello_processProtocol_m5773/* method */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448/* invoker_method */
	, TlsServerHello_t1325_TlsServerHello_processProtocol_m5773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHello_t1325_MethodInfos[] =
{
	&TlsServerHello__ctor_m5769_MethodInfo,
	&TlsServerHello_Update_m5770_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m5771_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m5772_MethodInfo,
	&TlsServerHello_processProtocol_m5773_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHello_ProcessAsTls1_m5772_MethodInfo;
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m5771_MethodInfo;
extern const MethodInfo TlsServerHello_Update_m5770_MethodInfo;
static const Il2CppMethodReference TlsServerHello_t1325_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m5772_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m5771_MethodInfo,
	&TlsServerHello_Update_m5770_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerHello_t1325_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t1325_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHello_t1325_0_0_0;
extern const Il2CppType TlsServerHello_t1325_1_0_0;
struct TlsServerHello_t1325;
const Il2CppTypeDefinitionMetadata TlsServerHello_t1325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t1325_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerHello_t1325_VTable/* vtableMethods */
	, TlsServerHello_t1325_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 462/* fieldStart */

};
TypeInfo TlsServerHello_t1325_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHello_t1325_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHello_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t1325_0_0_0/* byval_arg */
	, &TlsServerHello_t1325_1_0_0/* this_arg */
	, &TlsServerHello_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t1325)/* instance_size */
	, sizeof (TlsServerHello_t1325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t1326_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerHelloDone_t1326_TlsServerHelloDone__ctor_m5774_ParameterInfos[] = 
{
	{"context", 0, 134218546, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218547, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHelloDone__ctor_m5774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHelloDone__ctor_m5774/* method */
	, &TlsServerHelloDone_t1326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerHelloDone_t1326_TlsServerHelloDone__ctor_m5774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsSsl3()
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m5775_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsSsl3_m5775/* method */
	, &TlsServerHelloDone_t1326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsTls1()
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m5776_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsTls1_m5776/* method */
	, &TlsServerHelloDone_t1326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHelloDone_t1326_MethodInfos[] =
{
	&TlsServerHelloDone__ctor_m5774_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m5775_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m5776_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m5776_MethodInfo;
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m5775_MethodInfo;
extern const MethodInfo HandshakeMessage_Update_m5721_MethodInfo;
static const Il2CppMethodReference TlsServerHelloDone_t1326_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m5776_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m5775_MethodInfo,
	&HandshakeMessage_Update_m5721_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerHelloDone_t1326_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t1326_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHelloDone_t1326_0_0_0;
extern const Il2CppType TlsServerHelloDone_t1326_1_0_0;
struct TlsServerHelloDone_t1326;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t1326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t1326_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerHelloDone_t1326_VTable/* vtableMethods */
	, TlsServerHelloDone_t1326_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TlsServerHelloDone_t1326_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHelloDone_t1326_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHelloDone_t1326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t1326_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t1326_1_0_0/* this_arg */
	, &TlsServerHelloDone_t1326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t1326)/* instance_size */
	, sizeof (TlsServerHelloDone_t1326)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t1327_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
extern const Il2CppType Context_t1271_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo TlsServerKeyExchange_t1327_TlsServerKeyExchange__ctor_m5777_ParameterInfos[] = 
{
	{"context", 0, 134218548, 0, &Context_t1271_0_0_0},
	{"buffer", 1, 134218549, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerKeyExchange__ctor_m5777_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerKeyExchange__ctor_m5777/* method */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TlsServerKeyExchange_t1327_TlsServerKeyExchange__ctor_m5777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::Update()
extern const MethodInfo TlsServerKeyExchange_Update_m5778_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerKeyExchange_Update_m5778/* method */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsSsl3()
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m5779_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsSsl3_m5779/* method */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsTls1()
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m5780_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsTls1_m5780/* method */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::verifySignature()
extern const MethodInfo TlsServerKeyExchange_verifySignature_m5781_MethodInfo = 
{
	"verifySignature"/* name */
	, (methodPointerType)&TlsServerKeyExchange_verifySignature_m5781/* method */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerKeyExchange_t1327_MethodInfos[] =
{
	&TlsServerKeyExchange__ctor_m5777_MethodInfo,
	&TlsServerKeyExchange_Update_m5778_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m5779_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m5780_MethodInfo,
	&TlsServerKeyExchange_verifySignature_m5781_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m5780_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m5779_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_Update_m5778_MethodInfo;
static const Il2CppMethodReference TlsServerKeyExchange_t1327_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Stream_Dispose_m5923_MethodInfo,
	&TlsStream_get_CanRead_m5692_MethodInfo,
	&TlsStream_get_CanSeek_m5693_MethodInfo,
	&TlsStream_get_CanWrite_m5691_MethodInfo,
	&TlsStream_get_Length_m5696_MethodInfo,
	&TlsStream_get_Position_m5694_MethodInfo,
	&TlsStream_set_Position_m5695_MethodInfo,
	&Stream_Dispose_m5876_MethodInfo,
	&Stream_Close_m5875_MethodInfo,
	&TlsStream_Flush_m5709_MethodInfo,
	&TlsStream_Read_m5712_MethodInfo,
	&Stream_ReadByte_m5924_MethodInfo,
	&TlsStream_Seek_m5711_MethodInfo,
	&TlsStream_SetLength_m5710_MethodInfo,
	&TlsStream_Write_m5713_MethodInfo,
	&Stream_WriteByte_m5925_MethodInfo,
	&Stream_BeginRead_m5926_MethodInfo,
	&Stream_BeginWrite_m5927_MethodInfo,
	&Stream_EndRead_m5928_MethodInfo,
	&Stream_EndWrite_m5929_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m5780_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m5779_MethodInfo,
	&TlsServerKeyExchange_Update_m5778_MethodInfo,
	&HandshakeMessage_EncodeMessage_m5722_MethodInfo,
};
static bool TlsServerKeyExchange_t1327_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t1327_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerKeyExchange_t1327_0_0_0;
extern const Il2CppType TlsServerKeyExchange_t1327_1_0_0;
struct TlsServerKeyExchange_t1327;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t1327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t1327_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1296_0_0_0/* parent */
	, TlsServerKeyExchange_t1327_VTable/* vtableMethods */
	, TlsServerKeyExchange_t1327_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 466/* fieldStart */

};
TypeInfo TlsServerKeyExchange_t1327_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerKeyExchange_t1327_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerKeyExchange_t1327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t1327_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t1327_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t1327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t1327)/* instance_size */
	, sizeof (TlsServerKeyExchange_t1327)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1328_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t1328_PrimalityTest__ctor_m5782_ParameterInfos[] = 
{
	{"object", 0, 134218550, 0, &Object_t_0_0_0},
	{"method", 1, 134218551, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m5782_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m5782/* method */
	, &PrimalityTest_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t1328_PrimalityTest__ctor_m5782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1222_0_0_0;
extern const Il2CppType BigInteger_t1222_0_0_0;
extern const Il2CppType ConfidenceFactor_t1227_0_0_0;
extern const Il2CppType ConfidenceFactor_t1227_0_0_0;
static const ParameterInfo PrimalityTest_t1328_PrimalityTest_Invoke_m5783_ParameterInfos[] = 
{
	{"bi", 0, 134218552, 0, &BigInteger_t1222_0_0_0},
	{"confidence", 1, 134218553, 0, &ConfidenceFactor_t1227_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m5783_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m5783/* method */
	, &PrimalityTest_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Int32_t54/* invoker_method */
	, PrimalityTest_t1328_PrimalityTest_Invoke_m5783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1222_0_0_0;
extern const Il2CppType ConfidenceFactor_t1227_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t1328_PrimalityTest_BeginInvoke_m5784_ParameterInfos[] = 
{
	{"bi", 0, 134218554, 0, &BigInteger_t1222_0_0_0},
	{"confidence", 1, 134218555, 0, &ConfidenceFactor_t1227_0_0_0},
	{"callback", 2, 134218556, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134218557, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m5784_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m5784/* method */
	, &PrimalityTest_t1328_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t1328_PrimalityTest_BeginInvoke_m5784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo PrimalityTest_t1328_PrimalityTest_EndInvoke_m5785_ParameterInfos[] = 
{
	{"result", 0, 134218558, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m5785_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m5785/* method */
	, &PrimalityTest_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, PrimalityTest_t1328_PrimalityTest_EndInvoke_m5785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t1328_MethodInfos[] =
{
	&PrimalityTest__ctor_m5782_MethodInfo,
	&PrimalityTest_Invoke_m5783_MethodInfo,
	&PrimalityTest_BeginInvoke_m5784_MethodInfo,
	&PrimalityTest_EndInvoke_m5785_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m5783_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m5784_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m5785_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t1328_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&PrimalityTest_Invoke_m5783_MethodInfo,
	&PrimalityTest_BeginInvoke_m5784_MethodInfo,
	&PrimalityTest_EndInvoke_m5785_MethodInfo,
};
static bool PrimalityTest_t1328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
extern const Il2CppType ISerializable_t428_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t1328_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTest_t1328_0_0_0;
extern const Il2CppType PrimalityTest_t1328_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
struct PrimalityTest_t1328;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1328_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, PrimalityTest_t1328_VTable/* vtableMethods */
	, PrimalityTest_t1328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t1328_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t1328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t1328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1328_0_0_0/* byval_arg */
	, &PrimalityTest_t1328_1_0_0/* this_arg */
	, &PrimalityTest_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1328/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1328)/* instance_size */
	, sizeof (PrimalityTest_t1328)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t1306_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1306_CertificateValidationCallback__ctor_m5786_ParameterInfos[] = 
{
	{"object", 0, 134218559, 0, &Object_t_0_0_0},
	{"method", 1, 134218560, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback__ctor_m5786_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback__ctor_m5786/* method */
	, &CertificateValidationCallback_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback_t1306_CertificateValidationCallback__ctor_m5786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1306_CertificateValidationCallback_Invoke_m5787_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, 0, &X509Certificate_t1037_0_0_0},
	{"certificateErrors", 1, 134218562, 0, &Int32U5BU5D_t1082_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
extern const MethodInfo CertificateValidationCallback_Invoke_m5787_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_Invoke_m5787/* method */
	, &CertificateValidationCallback_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1306_CertificateValidationCallback_Invoke_m5787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1306_CertificateValidationCallback_BeginInvoke_m5788_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, 0, &X509Certificate_t1037_0_0_0},
	{"certificateErrors", 1, 134218564, 0, &Int32U5BU5D_t1082_0_0_0},
	{"callback", 2, 134218565, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134218566, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[],System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m5788_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_BeginInvoke_m5788/* method */
	, &CertificateValidationCallback_t1306_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1306_CertificateValidationCallback_BeginInvoke_m5788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1306_CertificateValidationCallback_EndInvoke_m5789_ParameterInfos[] = 
{
	{"result", 0, 134218567, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback_EndInvoke_m5789_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_EndInvoke_m5789/* method */
	, &CertificateValidationCallback_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, CertificateValidationCallback_t1306_CertificateValidationCallback_EndInvoke_m5789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback_t1306_MethodInfos[] =
{
	&CertificateValidationCallback__ctor_m5786_MethodInfo,
	&CertificateValidationCallback_Invoke_m5787_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m5788_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m5789_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback_Invoke_m5787_MethodInfo;
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m5788_MethodInfo;
extern const MethodInfo CertificateValidationCallback_EndInvoke_m5789_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback_t1306_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&CertificateValidationCallback_Invoke_m5787_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m5788_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m5789_MethodInfo,
};
static bool CertificateValidationCallback_t1306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t1306_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback_t1306_0_0_0;
extern const Il2CppType CertificateValidationCallback_t1306_1_0_0;
struct CertificateValidationCallback_t1306;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t1306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t1306_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, CertificateValidationCallback_t1306_VTable/* vtableMethods */
	, CertificateValidationCallback_t1306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback_t1306_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback_t1306_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback_t1306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t1306_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t1306_1_0_0/* this_arg */
	, &CertificateValidationCallback_t1306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t1306/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t1306)/* instance_size */
	, sizeof (CertificateValidationCallback_t1306)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t1307_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1307_CertificateValidationCallback2__ctor_m5790_ParameterInfos[] = 
{
	{"object", 0, 134218568, 0, &Object_t_0_0_0},
	{"method", 1, 134218569, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback2__ctor_m5790_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback2__ctor_m5790/* method */
	, &CertificateValidationCallback2_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback2_t1307_CertificateValidationCallback2__ctor_m5790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1174_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1307_CertificateValidationCallback2_Invoke_m5791_ParameterInfos[] = 
{
	{"collection", 0, 134218570, 0, &X509CertificateCollection_t1174_0_0_0},
};
extern const Il2CppType ValidationResult_t1305_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::Invoke(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo CertificateValidationCallback2_Invoke_m5791_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_Invoke_m5791/* method */
	, &CertificateValidationCallback2_t1307_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1305_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1307_CertificateValidationCallback2_Invoke_m5791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1174_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1307_CertificateValidationCallback2_BeginInvoke_m5792_ParameterInfos[] = 
{
	{"collection", 0, 134218571, 0, &X509CertificateCollection_t1174_0_0_0},
	{"callback", 1, 134218572, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134218573, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::BeginInvoke(Mono.Security.X509.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m5792_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_BeginInvoke_m5792/* method */
	, &CertificateValidationCallback2_t1307_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1307_CertificateValidationCallback2_BeginInvoke_m5792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1307_CertificateValidationCallback2_EndInvoke_m5793_ParameterInfos[] = 
{
	{"result", 0, 134218574, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m5793_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_EndInvoke_m5793/* method */
	, &CertificateValidationCallback2_t1307_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1305_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1307_CertificateValidationCallback2_EndInvoke_m5793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback2_t1307_MethodInfos[] =
{
	&CertificateValidationCallback2__ctor_m5790_MethodInfo,
	&CertificateValidationCallback2_Invoke_m5791_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m5792_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m5793_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback2_Invoke_m5791_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m5792_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m5793_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback2_t1307_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&CertificateValidationCallback2_Invoke_m5791_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m5792_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m5793_MethodInfo,
};
static bool CertificateValidationCallback2_t1307_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t1307_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback2_t1307_0_0_0;
extern const Il2CppType CertificateValidationCallback2_t1307_1_0_0;
struct CertificateValidationCallback2_t1307;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t1307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t1307_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, CertificateValidationCallback2_t1307_VTable/* vtableMethods */
	, CertificateValidationCallback2_t1307_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback2_t1307_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback2_t1307_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback2_t1307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t1307_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t1307_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t1307/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t1307)/* instance_size */
	, sizeof (CertificateValidationCallback2_t1307)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t1290_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1290_CertificateSelectionCallback__ctor_m5794_ParameterInfos[] = 
{
	{"object", 0, 134218575, 0, &Object_t_0_0_0},
	{"method", 1, 134218576, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateSelectionCallback__ctor_m5794_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateSelectionCallback__ctor_m5794/* method */
	, &CertificateSelectionCallback_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, CertificateSelectionCallback_t1290_CertificateSelectionCallback__ctor_m5794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1008_0_0_0;
extern const Il2CppType X509CertificateCollection_t1008_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1008_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1290_CertificateSelectionCallback_Invoke_m5795_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, 0, &X509CertificateCollection_t1008_0_0_0},
	{"serverCertificate", 1, 134218578, 0, &X509Certificate_t1037_0_0_0},
	{"targetHost", 2, 134218579, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, 0, &X509CertificateCollection_t1008_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern const MethodInfo CertificateSelectionCallback_Invoke_m5795_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_Invoke_m5795/* method */
	, &CertificateSelectionCallback_t1290_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1037_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1290_CertificateSelectionCallback_Invoke_m5795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1008_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1008_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1290_CertificateSelectionCallback_BeginInvoke_m5796_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, 0, &X509CertificateCollection_t1008_0_0_0},
	{"serverCertificate", 1, 134218582, 0, &X509Certificate_t1037_0_0_0},
	{"targetHost", 2, 134218583, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, 0, &X509CertificateCollection_t1008_0_0_0},
	{"callback", 4, 134218585, 0, &AsyncCallback_t214_0_0_0},
	{"object", 5, 134218586, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateSelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m5796_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_BeginInvoke_m5796/* method */
	, &CertificateSelectionCallback_t1290_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1290_CertificateSelectionCallback_BeginInvoke_m5796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1290_CertificateSelectionCallback_EndInvoke_m5797_ParameterInfos[] = 
{
	{"result", 0, 134218587, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m5797_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_EndInvoke_m5797/* method */
	, &CertificateSelectionCallback_t1290_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1037_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1290_CertificateSelectionCallback_EndInvoke_m5797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateSelectionCallback_t1290_MethodInfos[] =
{
	&CertificateSelectionCallback__ctor_m5794_MethodInfo,
	&CertificateSelectionCallback_Invoke_m5795_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m5796_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m5797_MethodInfo,
	NULL
};
extern const MethodInfo CertificateSelectionCallback_Invoke_m5795_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m5796_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m5797_MethodInfo;
static const Il2CppMethodReference CertificateSelectionCallback_t1290_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&CertificateSelectionCallback_Invoke_m5795_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m5796_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m5797_MethodInfo,
};
static bool CertificateSelectionCallback_t1290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t1290_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateSelectionCallback_t1290_0_0_0;
extern const Il2CppType CertificateSelectionCallback_t1290_1_0_0;
struct CertificateSelectionCallback_t1290;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t1290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t1290_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, CertificateSelectionCallback_t1290_VTable/* vtableMethods */
	, CertificateSelectionCallback_t1290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateSelectionCallback_t1290_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateSelectionCallback_t1290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateSelectionCallback_t1290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t1290_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t1290_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t1290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t1290/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t1290)/* instance_size */
	, sizeof (CertificateSelectionCallback_t1290)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback__ctor_m5798_ParameterInfos[] = 
{
	{"object", 0, 134218588, 0, &Object_t_0_0_0},
	{"method", 1, 134218589, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrivateKeySelectionCallback__ctor_m5798_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback__ctor_m5798/* method */
	, &PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback__ctor_m5798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_Invoke_m5799_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, 0, &X509Certificate_t1037_0_0_0},
	{"targetHost", 1, 134218591, 0, &String_t_0_0_0},
};
extern const Il2CppType AsymmetricAlgorithm_t1024_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m5799_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_Invoke_m5799/* method */
	, &PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1024_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_Invoke_m5799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_BeginInvoke_m5800_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, 0, &X509Certificate_t1037_0_0_0},
	{"targetHost", 1, 134218593, 0, &String_t_0_0_0},
	{"callback", 2, 134218594, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134218595, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.AsyncCallback,System.Object)
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m5800_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_BeginInvoke_m5800/* method */
	, &PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_BeginInvoke_m5800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_EndInvoke_m5801_ParameterInfos[] = 
{
	{"result", 0, 134218596, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m5801_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_EndInvoke_m5801/* method */
	, &PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1024_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1291_PrivateKeySelectionCallback_EndInvoke_m5801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrivateKeySelectionCallback_t1291_MethodInfos[] =
{
	&PrivateKeySelectionCallback__ctor_m5798_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m5799_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m5800_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m5801_MethodInfo,
	NULL
};
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m5799_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m5800_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m5801_MethodInfo;
static const Il2CppMethodReference PrivateKeySelectionCallback_t1291_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m5799_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m5800_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m5801_MethodInfo,
};
static bool PrivateKeySelectionCallback_t1291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t1291_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeySelectionCallback_t1291_0_0_0;
extern const Il2CppType PrivateKeySelectionCallback_t1291_1_0_0;
struct PrivateKeySelectionCallback_t1291;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t1291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t1291_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, PrivateKeySelectionCallback_t1291_VTable/* vtableMethods */
	, PrivateKeySelectionCallback_t1291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, PrivateKeySelectionCallback_t1291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrivateKeySelectionCallback_t1291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t1291_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t1291_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t1291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1291/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t1291)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t1291)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1329_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t1329_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU243132_t1329_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU243132_t1329_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1329_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t1329_1_0_0;
extern const Il2CppType ValueType_t439_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1338_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1338_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1329_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU243132_t1329_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t1329_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t1329_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t1329_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1329_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1329_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1329_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1329_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1329_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1329)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1329)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1329_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1330_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t1330_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t1330_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24256_t1330_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1330_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1330_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1330_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24256_t1330_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t1330_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t1330_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t1330_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t1330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1330_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1330_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1330_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1330_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1330_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1330)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1330)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1330_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1331_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t1331_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t1331_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2420_t1331_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1331_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1331_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1331_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2420_t1331_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t1331_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t1331_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t1331_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t1331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1331_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1331_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1331_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1331_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1331_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1331)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1331)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1331_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1332_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t1332_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t1332_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2432_t1332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1332_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1332_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1332_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2432_t1332_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t1332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t1332_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t1332_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t1332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1332_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1332_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1332_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1332_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1332_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1332)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1332)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1332_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1333_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t1333_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t1333_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2448_t1333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1333_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1333_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1333_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2448_t1333_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t1333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t1333_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t1333_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t1333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1333_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1333_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1333_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1333_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1333_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1333)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1333)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1333_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1334_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t1334_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t1334_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2464_t1334_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1334_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1334_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1334_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2464_t1334_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t1334_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t1334_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t1334_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t1334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1334_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1334_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1334_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1334_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1334_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1334)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1334)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1334_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1335_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1335_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1335_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2412_t1335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1335_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1335_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1335_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2412_t1335_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1335_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1335_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1335_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1335_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1335_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1335_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1335)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1335)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1335_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1336_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t1336_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t1336_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2416_t1336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1336_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1336_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1336_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2416_t1336_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t1336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t1336_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t1336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t1336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1336_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1336_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1336_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1336_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1336_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1336)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1336)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1336_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t1337_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU244_t1337_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU244_t1337_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU244_t1337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU244_t1337_0_0_0;
extern const Il2CppType U24ArrayTypeU244_t1337_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t1337_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU244_t1337_VTable/* vtableMethods */
	, U24ArrayTypeU244_t1337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU244_t1337_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU244_t1337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU244_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t1337_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t1337_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t1337_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1337_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1337_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t1337)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t1337)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t1337_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1338_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1338_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t1329_0_0_0,
	&U24ArrayTypeU24256_t1330_0_0_0,
	&U24ArrayTypeU2420_t1331_0_0_0,
	&U24ArrayTypeU2432_t1332_0_0_0,
	&U24ArrayTypeU2448_t1333_0_0_0,
	&U24ArrayTypeU2464_t1334_0_0_0,
	&U24ArrayTypeU2412_t1335_0_0_0,
	&U24ArrayTypeU2416_t1336_0_0_0,
	&U24ArrayTypeU244_t1337_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1338_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1338_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1338;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1338_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1338_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 468/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1338_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1338_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 38/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1338_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1338_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1338)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1338_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
