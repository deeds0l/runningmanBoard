﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
struct  CostDelegate_t1108  : public MulticastDelegate_t216
{
};
