﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Tools/<WaitForRealTime>c__Iterator5
struct U3CWaitForRealTimeU3Ec__Iterator5_t26;
// System.Object
struct Object_t;

// System.Void Tools/<WaitForRealTime>c__Iterator5::.ctor()
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5__ctor_m66 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Tools/<WaitForRealTime>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tools/<WaitForRealTime>c__Iterator5::MoveNext()
extern "C" bool U3CWaitForRealTimeU3Ec__Iterator5_MoveNext_m69 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tools/<WaitForRealTime>c__Iterator5::Dispose()
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Tools/<WaitForRealTime>c__Iterator5::Reset()
extern "C" void U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71 (U3CWaitForRealTimeU3Ec__Iterator5_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
