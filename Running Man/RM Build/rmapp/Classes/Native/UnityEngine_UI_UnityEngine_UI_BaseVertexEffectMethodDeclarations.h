﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t299;
// UnityEngine.UI.Graphic
struct Graphic_t188;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t227;

// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern "C" void BaseVertexEffect__ctor_m1431 (BaseVertexEffect_t299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern "C" Graphic_t188 * BaseVertexEffect_get_graphic_m1432 (BaseVertexEffect_t299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern "C" void BaseVertexEffect_OnEnable_m1433 (BaseVertexEffect_t299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern "C" void BaseVertexEffect_OnDisable_m1434 (BaseVertexEffect_t299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
