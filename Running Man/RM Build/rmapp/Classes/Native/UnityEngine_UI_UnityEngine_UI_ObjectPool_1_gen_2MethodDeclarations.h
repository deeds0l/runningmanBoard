﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t296;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t297;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t332;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m1974(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t296 *, UnityAction_1_t297 *, UnityAction_1_t297 *, const MethodInfo*))ObjectPool_1__ctor_m11987_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m14839(__this, method) (( int32_t (*) (ObjectPool_1_t296 *, const MethodInfo*))ObjectPool_1_get_countAll_m11989_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m14840(__this, ___value, method) (( void (*) (ObjectPool_1_t296 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11991_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m14841(__this, method) (( int32_t (*) (ObjectPool_1_t296 *, const MethodInfo*))ObjectPool_1_get_countActive_m11993_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m14842(__this, method) (( int32_t (*) (ObjectPool_1_t296 *, const MethodInfo*))ObjectPool_1_get_countInactive_m11995_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m1975(__this, method) (( List_1_t332 * (*) (ObjectPool_1_t296 *, const MethodInfo*))ObjectPool_1_Get_m11997_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m1976(__this, ___element, method) (( void (*) (ObjectPool_1_t296 *, List_1_t332 *, const MethodInfo*))ObjectPool_1_Release_m11999_gshared)(__this, ___element, method)
