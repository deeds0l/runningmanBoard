﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.UnverifiableCodeAttribute
struct UnverifiableCodeAttribute_t1770;

// System.Void System.Security.UnverifiableCodeAttribute::.ctor()
extern "C" void UnverifiableCodeAttribute__ctor_m9272 (UnverifiableCodeAttribute_t1770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
