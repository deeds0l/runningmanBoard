﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoAsyncCall
struct MonoAsyncCall_t1857;

// System.Void System.MonoAsyncCall::.ctor()
extern "C" void MonoAsyncCall__ctor_m10057 (MonoAsyncCall_t1857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
