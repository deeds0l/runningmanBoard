﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t1093;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern "C" void LinkRef__ctor_m4318 (LinkRef_t1093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
