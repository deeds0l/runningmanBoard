﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1295;
// System.IO.Stream
struct Stream_t1294;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t42;
// System.Threading.WaitHandle
struct WaitHandle_t1340;
// System.AsyncCallback
struct AsyncCallback_t214;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m5473 (ReceiveRecordAsyncResult_t1295 * __this, AsyncCallback_t214 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t546* ___initialBuffer, Stream_t1294 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t1294 * ReceiveRecordAsyncResult_get_Record_m5474 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t546* ReceiveRecordAsyncResult_get_ResultingBuffer_m5475 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t546* ReceiveRecordAsyncResult_get_InitialBuffer_m5476 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m5477 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t42 * ReceiveRecordAsyncResult_get_AsyncException_m5478 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m5479 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1340 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m5480 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m5481 (ReceiveRecordAsyncResult_t1295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5482 (ReceiveRecordAsyncResult_t1295 * __this, Exception_t42 * ___ex, ByteU5BU5D_t546* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5483 (ReceiveRecordAsyncResult_t1295 * __this, Exception_t42 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m5484 (ReceiveRecordAsyncResult_t1295 * __this, ByteU5BU5D_t546* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
