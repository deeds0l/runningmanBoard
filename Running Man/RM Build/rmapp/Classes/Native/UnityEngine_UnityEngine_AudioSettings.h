﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t569;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AudioSettings
struct  AudioSettings_t570  : public Object_t
{
};
struct AudioSettings_t570_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t569 * ___OnAudioConfigurationChanged_0;
};
