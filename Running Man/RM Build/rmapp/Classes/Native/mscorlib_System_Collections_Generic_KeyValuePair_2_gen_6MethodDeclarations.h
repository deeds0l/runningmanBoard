﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_t2593;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m13480_gshared (KeyValuePair_2_t2593 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m13480(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2593 *, Object_t *, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m13481_gshared (KeyValuePair_2_t2593 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m13481(__this, method) (( Object_t * (*) (KeyValuePair_2_t2593 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m13482_gshared (KeyValuePair_2_t2593 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m13482(__this, ___value, method) (( void (*) (KeyValuePair_2_t2593 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m13483_gshared (KeyValuePair_2_t2593 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m13483(__this, method) (( Object_t * (*) (KeyValuePair_2_t2593 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m13484_gshared (KeyValuePair_2_t2593 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m13484(__this, ___value, method) (( void (*) (KeyValuePair_2_t2593 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m13485_gshared (KeyValuePair_2_t2593 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m13485(__this, method) (( String_t* (*) (KeyValuePair_2_t2593 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
