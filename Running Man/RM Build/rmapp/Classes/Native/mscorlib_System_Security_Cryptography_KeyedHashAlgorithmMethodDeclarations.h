﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1265;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void System.Security.Cryptography.KeyedHashAlgorithm::.ctor()
extern "C" void KeyedHashAlgorithm__ctor_m5852 (KeyedHashAlgorithm_t1265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Finalize()
extern "C" void KeyedHashAlgorithm_Finalize_m5921 (KeyedHashAlgorithm_t1265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::get_Key()
extern "C" ByteU5BU5D_t546* KeyedHashAlgorithm_get_Key_m8996 (KeyedHashAlgorithm_t1265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::set_Key(System.Byte[])
extern "C" void KeyedHashAlgorithm_set_Key_m8997 (KeyedHashAlgorithm_t1265 * __this, ByteU5BU5D_t546* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Dispose(System.Boolean)
extern "C" void KeyedHashAlgorithm_Dispose_m5922 (KeyedHashAlgorithm_t1265 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::ZeroizeKey()
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m8998 (KeyedHashAlgorithm_t1265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
