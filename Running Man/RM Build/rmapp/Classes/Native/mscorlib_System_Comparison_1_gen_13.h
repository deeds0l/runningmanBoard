﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.GameObject>
struct  Comparison_1_t2529  : public MulticastDelegate_t216
{
};
