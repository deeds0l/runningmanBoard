﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t58;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C" void DebuggerHiddenAttribute__ctor_m155 (DebuggerHiddenAttribute_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
