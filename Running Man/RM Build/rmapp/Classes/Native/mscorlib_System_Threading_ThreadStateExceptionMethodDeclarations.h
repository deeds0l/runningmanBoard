﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Threading.ThreadStateException
struct ThreadStateException_t1807;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadStateException::.ctor()
extern "C" void ThreadStateException__ctor_m9560 (ThreadStateException_t1807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadStateException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadStateException__ctor_m9561 (ThreadStateException_t1807 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
