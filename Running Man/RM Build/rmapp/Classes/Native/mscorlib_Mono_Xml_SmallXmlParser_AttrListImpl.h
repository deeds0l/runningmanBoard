﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t985;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Xml.SmallXmlParser/AttrListImpl
struct  AttrListImpl_t1454  : public Object_t
{
	// System.Collections.ArrayList Mono.Xml.SmallXmlParser/AttrListImpl::attrNames
	ArrayList_t985 * ___attrNames_0;
	// System.Collections.ArrayList Mono.Xml.SmallXmlParser/AttrListImpl::attrValues
	ArrayList_t985 * ___attrValues_1;
};
