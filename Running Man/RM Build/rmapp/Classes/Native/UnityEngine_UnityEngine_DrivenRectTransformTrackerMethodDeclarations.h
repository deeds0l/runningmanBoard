﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t241;
// UnityEngine.Object
struct Object_t33;
struct Object_t33_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t183;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m1847 (DrivenRectTransformTracker_t241 * __this, Object_t33 * ___driver, RectTransform_t183 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m1845 (DrivenRectTransformTracker_t241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
