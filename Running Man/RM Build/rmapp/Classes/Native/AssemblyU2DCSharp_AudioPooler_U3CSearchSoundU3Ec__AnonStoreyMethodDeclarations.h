﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<SearchSound>c__AnonStoreyC
struct U3CSearchSoundU3Ec__AnonStoreyC_t19;
// UnityEngine.AudioClip
struct AudioClip_t20;

// System.Void AudioPooler/<SearchSound>c__AnonStoreyC::.ctor()
extern "C" void U3CSearchSoundU3Ec__AnonStoreyC__ctor_m32 (U3CSearchSoundU3Ec__AnonStoreyC_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<SearchSound>c__AnonStoreyC::<>m__6(UnityEngine.AudioClip)
extern "C" bool U3CSearchSoundU3Ec__AnonStoreyC_U3CU3Em__6_m33 (U3CSearchSoundU3Ec__AnonStoreyC_t19 * __this, AudioClip_t20 * ___clip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
