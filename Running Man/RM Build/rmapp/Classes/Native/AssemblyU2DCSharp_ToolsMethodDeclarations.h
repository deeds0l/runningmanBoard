﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Tools
struct Tools_t27;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.GameObject
struct GameObject_t5;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t30;

// System.Collections.IEnumerator Tools::WaitForRealTime(System.Single)
extern "C" Object_t * Tools_WaitForRealTime_m72 (Object_t * __this /* static, unused */, float ___p_delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Tools::InstantiateResource(System.String,UnityEngine.Transform)
extern "C" GameObject_t5 * Tools_InstantiateResource_m73 (Object_t * __this /* static, unused */, String_t* ___p_path, Transform_t30 * ___p_parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Tools::RemoveSubstring(System.String,System.String)
extern "C" String_t* Tools_RemoveSubstring_m74 (Object_t * __this /* static, unused */, String_t* ___p_mainStr, String_t* ___p_subStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tools::IsMultipleTouch()
extern "C" bool Tools_IsMultipleTouch_m75 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
