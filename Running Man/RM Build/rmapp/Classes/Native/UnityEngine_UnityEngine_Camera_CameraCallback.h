﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t156;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t553  : public MulticastDelegate_t216
{
};
