﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct U24ArrayTypeU2464_t1334;
struct U24ArrayTypeU2464_t1334_marshaled;

void U24ArrayTypeU2464_t1334_marshal(const U24ArrayTypeU2464_t1334& unmarshaled, U24ArrayTypeU2464_t1334_marshaled& marshaled);
void U24ArrayTypeU2464_t1334_marshal_back(const U24ArrayTypeU2464_t1334_marshaled& marshaled, U24ArrayTypeU2464_t1334& unmarshaled);
void U24ArrayTypeU2464_t1334_marshal_cleanup(U24ArrayTypeU2464_t1334_marshaled& marshaled);
