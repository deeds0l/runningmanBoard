﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.IntervalCollection/Enumerator
struct Enumerator_t1107;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t861;

// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
extern "C" void Enumerator__ctor_m4429 (Enumerator_t1107 * __this, Object_t * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m4430 (Enumerator_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m4431 (Enumerator_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
extern "C" void Enumerator_Reset_m4432 (Enumerator_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
