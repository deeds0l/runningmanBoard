﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern "C" void BaseCanvas_1_get_Instance_m11745_gshared ();
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseCanvas_1__ctor_m11743_gshared ();
void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseCanvas_1__cctor_m11744_gshared ();
extern "C" void BaseCanvas_1_Create_m11746_gshared ();
void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseCanvas_1_Awake_m11747_gshared ();
extern "C" void BaseCanvas_1_OnDestroy_m11748_gshared ();
extern "C" void BaseCanvas_1_Open_m11749_gshared ();
extern "C" void BaseCanvas_1_Close_m11750_gshared ();
extern "C" void BaseOverlay_1__ctor_m11751_gshared ();
extern "C" void BaseOverlay_1_Awake_m11752_gshared ();
extern "C" void BaseOverlay_1_Open_m11753_gshared ();
extern "C" void BaseOverlay_1_Close_m11754_gshared ();
extern "C" void BaseOverlay_1_OnClose_m11755_gshared ();
extern "C" void BaseOverlay_1_CancelClose_m11756_gshared ();
extern "C" void BaseOverlay_1_CloseWithDelay_m11757_gshared ();
void* RuntimeInvoker_Void_t71_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseOverlay_1_CloseWithDelayCoroutine_m11758_gshared ();
void* RuntimeInvoker_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseOverlay_1_SetSortingOrder_m11759_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void BaseOverlay_1_FadeIn_m11760_gshared ();
extern "C" void BaseOverlay_1_FadeOut_m11761_gshared ();
extern "C" void BaseOverlay_1_FadeInCoroutine_m11762_gshared ();
extern "C" void BaseOverlay_1_FadeOutCoroutine_m11763_gshared ();
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11765_gshared ();
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m11766_gshared ();
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m11764_gshared ();
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m11767_gshared ();
void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m11768_gshared ();
extern "C" void U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m11769_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11771_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m11772_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3__ctor_m11770_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m11773_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m11774_gshared ();
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_Reset_m11775_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11777_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m11778_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m11776_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m11779_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m11780_gshared ();
extern "C" void U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m11781_gshared ();
extern "C" void MonoManager_1_get_Instance_m11267_gshared ();
extern "C" void MonoManager_1__ctor_m11264_gshared ();
extern "C" void MonoManager_1__cctor_m11266_gshared ();
extern "C" void MonoManager_1_Create_m11268_gshared ();
extern "C" void MonoManager_1_Awake_m11270_gshared ();
extern "C" void MonoManager_1_OnDestroy_m11271_gshared ();
extern "C" void MonoManager_1_Destroy_m11273_gshared ();
extern "C" void NonMonoManager_1_get_Instance_m11789_gshared ();
extern "C" void NonMonoManager_1__ctor_m11787_gshared ();
extern "C" void NonMonoManager_1__cctor_m11788_gshared ();
extern "C" void NonMonoManager_1_Create_m11790_gshared ();
extern "C" void NonMonoManager_1_Destroy_m11791_gshared ();
extern "C" void Tools_InstantiateCanvas_TisObject_t_m18821_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Tools_InstantiateManager_TisObject_t_m18763_gshared ();
extern "C" void Tools_InstantiateResource_TisObject_t_m18762_gshared ();
extern "C" void ExecuteEvents_ValidateEventData_TisObject_t_m1469_gshared ();
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_Execute_TisObject_t_m1451_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ExecuteHierarchy_TisObject_t_m1538_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_ShouldSendToComponent_TisObject_t_m18826_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_GetEventList_TisObject_t_m18823_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ExecuteEvents_CanHandleEvent_TisObject_t_m18851_gshared ();
extern "C" void ExecuteEvents_GetEventHandler_TisObject_t_m1511_gshared ();
extern "C" void EventFunction_1__ctor_m11887_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_Invoke_m11889_gshared ();
extern "C" void EventFunction_1_BeginInvoke_m11891_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void EventFunction_1_EndInvoke_m11893_gshared ();
extern "C" void SetPropertyUtility_SetClass_TisObject_t_m1675_gshared ();
void* RuntimeInvoker_Boolean_t72_ObjectU26_t863_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisObject_t_m1952_gshared ();
void* RuntimeInvoker_Void_t71_ObjectU26_t863_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_Count_m13079_gshared ();
void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_get_IsReadOnly_m13081_gshared ();
extern "C" void IndexedSet_1_get_Item_m13089_gshared ();
void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_set_Item_m13091_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1__ctor_m13063_gshared ();
extern "C" void IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared ();
extern "C" void IndexedSet_1_Add_m13067_gshared ();
extern "C" void IndexedSet_1_Remove_m13069_gshared ();
extern "C" void IndexedSet_1_GetEnumerator_m13071_gshared ();
extern "C" void IndexedSet_1_Clear_m13073_gshared ();
extern "C" void IndexedSet_1_Contains_m13075_gshared ();
extern "C" void IndexedSet_1_CopyTo_m13077_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_IndexOf_m13083_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void IndexedSet_1_Insert_m13085_gshared ();
extern "C" void IndexedSet_1_RemoveAt_m13087_gshared ();
extern "C" void IndexedSet_1_RemoveAll_m13092_gshared ();
extern "C" void IndexedSet_1_Sort_m13093_gshared ();
extern "C" void ObjectPool_1_get_countAll_m11989_gshared ();
extern "C" void ObjectPool_1_set_countAll_m11991_gshared ();
extern "C" void ObjectPool_1_get_countActive_m11993_gshared ();
extern "C" void ObjectPool_1_get_countInactive_m11995_gshared ();
extern "C" void ObjectPool_1__ctor_m11987_gshared ();
extern "C" void ObjectPool_1_Get_m11997_gshared ();
extern "C" void ObjectPool_1_Release_m11999_gshared ();
extern "C" void ScriptableObject_CreateInstance_TisObject_t_m19019_gshared ();
extern "C" void Object_FindObjectOfType_TisObject_t_m91_gshared ();
extern "C" void Component_GetComponent_TisObject_t_m124_gshared ();
extern "C" void Component_GetComponentsInChildren_TisObject_t_m19017_gshared ();
void* RuntimeInvoker_Void_t71_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Component_GetComponentsInChildren_TisObject_t_m1967_gshared ();
extern "C" void Component_GetComponents_TisObject_t_m1449_gshared ();
extern "C" void GameObject_GetComponent_TisObject_t_m1559_gshared ();
extern "C" void GameObject_GetComponents_TisObject_t_m18825_gshared ();
extern "C" void GameObject_GetComponentsInChildren_TisObject_t_m19018_gshared ();
extern "C" void GameObject_GetComponentsInParent_TisObject_t_m1612_gshared ();
extern "C" void GameObject_AddComponent_TisObject_t_m95_gshared ();
extern "C" void ResponseBase_ParseJSONList_TisObject_t_m3433_gshared ();
extern "C" void NetworkMatch_ProcessMatchResponse_TisObject_t_m3440_gshared ();
extern "C" void ResponseDelegate_1__ctor_m16791_gshared ();
extern "C" void ResponseDelegate_1_Invoke_m16793_gshared ();
extern "C" void ResponseDelegate_1_BeginInvoke_m16795_gshared ();
extern "C" void ResponseDelegate_1_EndInvoke_m16797_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m16799_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m16800_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m16798_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m16801_gshared ();
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m16802_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Keys_m16942_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Values_m16948_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Item_m16950_gshared ();
extern "C" void ThreadSafeDictionary_2_set_Item_m16952_gshared ();
extern "C" void ThreadSafeDictionary_2_get_Count_m16962_gshared ();
extern "C" void ThreadSafeDictionary_2_get_IsReadOnly_m16964_gshared ();
extern "C" void ThreadSafeDictionary_2__ctor_m16932_gshared ();
extern "C" void ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934_gshared ();
extern "C" void ThreadSafeDictionary_2_Get_m16936_gshared ();
extern "C" void ThreadSafeDictionary_2_AddValue_m16938_gshared ();
extern "C" void ThreadSafeDictionary_2_Add_m16940_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m16944_gshared ();
extern "C" void ThreadSafeDictionary_2_TryGetValue_m16946_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Add_m16954_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_Clear_m16956_gshared ();
extern "C" void ThreadSafeDictionary_2_Contains_m16958_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void ThreadSafeDictionary_2_CopyTo_m16960_gshared ();
extern "C" void ThreadSafeDictionary_2_Remove_m16966_gshared ();
extern "C" void ThreadSafeDictionary_2_GetEnumerator_m16968_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2__ctor_m16925_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_Invoke_m16927_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_BeginInvoke_m16929_gshared ();
extern "C" void ThreadSafeDictionaryValueFactory_2_EndInvoke_m16931_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m18850_gshared ();
extern "C" void InvokableCall_1__ctor_m12464_gshared ();
extern "C" void InvokableCall_1__ctor_m12465_gshared ();
extern "C" void InvokableCall_1_Invoke_m12466_gshared ();
extern "C" void InvokableCall_1_Find_m12467_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InvokableCall_2__ctor_m17754_gshared ();
extern "C" void InvokableCall_2_Invoke_m17755_gshared ();
extern "C" void InvokableCall_2_Find_m17756_gshared ();
extern "C" void InvokableCall_3__ctor_m17761_gshared ();
extern "C" void InvokableCall_3_Invoke_m17762_gshared ();
extern "C" void InvokableCall_3_Find_m17763_gshared ();
extern "C" void InvokableCall_4__ctor_m17768_gshared ();
extern "C" void InvokableCall_4_Invoke_m17769_gshared ();
extern "C" void InvokableCall_4_Find_m17770_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m17775_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m17776_gshared ();
extern "C" void UnityEvent_1__ctor_m12454_gshared ();
extern "C" void UnityEvent_1_AddListener_m12456_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m12458_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m12459_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m12460_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m12462_gshared ();
extern "C" void UnityEvent_1_Invoke_m12463_gshared ();
extern "C" void UnityEvent_2__ctor_m17977_gshared ();
extern "C" void UnityEvent_2_FindMethod_Impl_m17978_gshared ();
extern "C" void UnityEvent_2_GetDelegate_m17979_gshared ();
extern "C" void UnityEvent_3__ctor_m17980_gshared ();
extern "C" void UnityEvent_3_FindMethod_Impl_m17981_gshared ();
extern "C" void UnityEvent_3_GetDelegate_m17982_gshared ();
extern "C" void UnityEvent_4__ctor_m17983_gshared ();
extern "C" void UnityEvent_4_FindMethod_Impl_m17984_gshared ();
extern "C" void UnityEvent_4_GetDelegate_m17985_gshared ();
extern "C" void UnityAction_1__ctor_m12016_gshared ();
extern "C" void UnityAction_1_Invoke_m12017_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m12018_gshared ();
extern "C" void UnityAction_1_EndInvoke_m12019_gshared ();
extern "C" void UnityAction_2__ctor_m17757_gshared ();
extern "C" void UnityAction_2_Invoke_m17758_gshared ();
extern "C" void UnityAction_2_BeginInvoke_m17759_gshared ();
extern "C" void UnityAction_2_EndInvoke_m17760_gshared ();
extern "C" void UnityAction_3__ctor_m17764_gshared ();
extern "C" void UnityAction_3_Invoke_m17765_gshared ();
extern "C" void UnityAction_3_BeginInvoke_m17766_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_3_EndInvoke_m17767_gshared ();
extern "C" void UnityAction_4__ctor_m17771_gshared ();
extern "C" void UnityAction_4_Invoke_m17772_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_BeginInvoke_m17773_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_4_EndInvoke_m17774_gshared ();
extern "C" void Enumerable_First_TisObject_t_m18820_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerable_FirstOrDefault_TisObject_t_m97_gshared ();
extern "C" void Enumerable_Last_TisObject_t_m18819_gshared ();
extern "C" void Enumerable_Last_TisObject_t_m93_gshared ();
extern "C" void Enumerable_Where_TisObject_t_m1921_gshared ();
extern "C" void Enumerable_CreateWhereIterator_TisObject_t_m19016_gshared ();
extern "C" void PredicateOf_1__cctor_m11605_gshared ();
extern "C" void PredicateOf_1_U3CAlwaysU3Em__76_m11606_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14713_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m14714_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m14712_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m14715_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14716_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m14717_gshared ();
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m14718_gshared ();
extern "C" void Func_2__ctor_m17986_gshared ();
extern "C" void Func_2_Invoke_m17987_gshared ();
extern "C" void Func_2_BeginInvoke_m17988_gshared ();
extern "C" void Func_2_EndInvoke_m17989_gshared ();
extern "C" void Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708_gshared ();
extern "C" void Queue_1_System_Collections_ICollection_get_SyncRoot_m11710_gshared ();
extern "C" void Queue_1_get_Count_m11724_gshared ();
extern "C" void Queue_1__ctor_m11704_gshared ();
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m11706_gshared ();
extern "C" void Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712_gshared ();
extern "C" void Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714_gshared ();
extern "C" void Queue_1_CopyTo_m11716_gshared ();
extern "C" void Queue_1_Dequeue_m11717_gshared ();
extern "C" void Queue_1_Peek_m11719_gshared ();
extern "C" void Queue_1_Enqueue_m11720_gshared ();
extern "C" void Queue_1_SetCapacity_m11722_gshared ();
extern "C" void Queue_1_GetEnumerator_m11726_gshared ();
void* RuntimeInvoker_Enumerator_t2462 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m11728_gshared ();
extern "C" void Enumerator_get_Current_m11731_gshared ();
extern "C" void Enumerator__ctor_m11727_gshared ();
extern "C" void Enumerator_Dispose_m11729_gshared ();
extern "C" void Enumerator_MoveNext_m11730_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_IsSynchronized_m12001_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_get_SyncRoot_m12002_gshared ();
extern "C" void Stack_1_get_Count_m12009_gshared ();
extern "C" void Stack_1__ctor_m12000_gshared ();
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m12003_gshared ();
extern "C" void Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12004_gshared ();
extern "C" void Stack_1_System_Collections_IEnumerable_GetEnumerator_m12005_gshared ();
extern "C" void Stack_1_Peek_m12006_gshared ();
extern "C" void Stack_1_Pop_m12007_gshared ();
extern "C" void Stack_1_Push_m12008_gshared ();
extern "C" void Stack_1_GetEnumerator_m12010_gshared ();
void* RuntimeInvoker_Enumerator_t2486 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m12012_gshared ();
extern "C" void Enumerator_get_Current_m12015_gshared ();
extern "C" void Enumerator__ctor_m12011_gshared ();
extern "C" void Enumerator_Dispose_m12013_gshared ();
extern "C" void Enumerator_MoveNext_m12014_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m18774_gshared ();
extern "C" void Array_InternalArray__ICollection_Add_TisObject_t_m18766_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisObject_t_m18769_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisObject_t_m18767_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisObject_t_m18768_gshared ();
extern "C" void Array_InternalArray__Insert_TisObject_t_m18771_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisObject_t_m18770_gshared ();
extern "C" void Array_InternalArray__get_Item_TisObject_t_m18765_gshared ();
extern "C" void Array_InternalArray__set_Item_TisObject_t_m18773_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_get_swapper_TisObject_t_m18779_gshared ();
extern "C" void Array_Sort_TisObject_t_m19320_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m19321_gshared ();
extern "C" void Array_Sort_TisObject_t_m19322_gshared ();
extern "C" void Array_Sort_TisObject_t_TisObject_t_m19323_gshared ();
extern "C" void Array_Sort_TisObject_t_m10410_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m19324_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m18778_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_TisObject_t_m18777_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisObject_t_m19325_gshared ();
extern "C" void Array_Sort_TisObject_t_m18817_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_TisObject_t_m18780_gshared ();
extern "C" void Array_compare_TisObject_t_m18814_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_qsort_TisObject_t_m18816_gshared ();
extern "C" void Array_swap_TisObject_t_TisObject_t_m18815_gshared ();
extern "C" void Array_swap_TisObject_t_m18818_gshared ();
extern "C" void Array_Resize_TisObject_t_m18776_gshared ();
void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisObject_t_m18775_gshared ();
void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_TrueForAll_TisObject_t_m19326_gshared ();
extern "C" void Array_ForEach_TisObject_t_m19327_gshared ();
extern "C" void Array_ConvertAll_TisObject_t_TisObject_t_m19328_gshared ();
extern "C" void Array_FindLastIndex_TisObject_t_m19330_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m19331_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindLastIndex_TisObject_t_m19329_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_FindIndex_TisObject_t_m19333_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m19334_gshared ();
extern "C" void Array_FindIndex_TisObject_t_m19332_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m19336_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m19337_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m19338_gshared ();
extern "C" void Array_BinarySearch_TisObject_t_m19335_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m10412_gshared ();
extern "C" void Array_IndexOf_TisObject_t_m19339_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisObject_t_m10409_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_LastIndexOf_TisObject_t_m19341_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m19340_gshared ();
extern "C" void Array_LastIndexOf_TisObject_t_m19342_gshared ();
extern "C" void Array_FindAll_TisObject_t_m19343_gshared ();
extern "C" void Array_Exists_TisObject_t_m19344_gshared ();
extern "C" void Array_AsReadOnly_TisObject_t_m19345_gshared ();
extern "C" void Array_Find_TisObject_t_m19346_gshared ();
extern "C" void Array_FindLast_TisObject_t_m19347_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m11349_gshared ();
extern "C" void InternalEnumerator_1__ctor_m11345_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m11347_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m11348_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Item_m18441_gshared ();
extern "C" void ArrayReadOnlyList_1_set_Item_m18442_gshared ();
extern "C" void ArrayReadOnlyList_1_get_Count_m18443_gshared ();
extern "C" void ArrayReadOnlyList_1_get_IsReadOnly_m18444_gshared ();
extern "C" void ArrayReadOnlyList_1__ctor_m18439_gshared ();
extern "C" void ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18440_gshared ();
extern "C" void ArrayReadOnlyList_1_Add_m18445_gshared ();
extern "C" void ArrayReadOnlyList_1_Clear_m18446_gshared ();
extern "C" void ArrayReadOnlyList_1_Contains_m18447_gshared ();
extern "C" void ArrayReadOnlyList_1_CopyTo_m18448_gshared ();
extern "C" void ArrayReadOnlyList_1_GetEnumerator_m18449_gshared ();
extern "C" void ArrayReadOnlyList_1_IndexOf_m18450_gshared ();
extern "C" void ArrayReadOnlyList_1_Insert_m18451_gshared ();
extern "C" void ArrayReadOnlyList_1_Remove_m18452_gshared ();
extern "C" void ArrayReadOnlyList_1_RemoveAt_m18453_gshared ();
extern "C" void ArrayReadOnlyList_1_ReadOnlyError_m18454_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18456_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18457_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m18455_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18458_gshared ();
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18459_gshared ();
extern "C" void Comparer_1_get_Default_m11442_gshared ();
extern "C" void Comparer_1__ctor_m11439_gshared ();
extern "C" void Comparer_1__cctor_m11440_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m11441_gshared ();
extern "C" void DefaultComparer__ctor_m11443_gshared ();
extern "C" void DefaultComparer_Compare_m11444_gshared ();
extern "C" void GenericComparer_1__ctor_m18490_gshared ();
extern "C" void GenericComparer_1_Compare_m18491_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13344_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13346_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m13348_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13350_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13358_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13360_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13362_gshared ();
extern "C" void Dictionary_2_get_Count_m13380_gshared ();
extern "C" void Dictionary_2_get_Item_m13382_gshared ();
extern "C" void Dictionary_2_set_Item_m13384_gshared ();
extern "C" void Dictionary_2_get_Keys_m13418_gshared ();
extern "C" void Dictionary_2_get_Values_m13420_gshared ();
extern "C" void Dictionary_2__ctor_m13332_gshared ();
extern "C" void Dictionary_2__ctor_m13334_gshared ();
extern "C" void Dictionary_2__ctor_m13336_gshared ();
extern "C" void Dictionary_2__ctor_m13338_gshared ();
extern "C" void Dictionary_2__ctor_m13340_gshared ();
extern "C" void Dictionary_2__ctor_m13342_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13352_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m13354_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13356_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13364_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13366_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13368_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13370_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13372_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13374_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13376_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13378_gshared ();
extern "C" void Dictionary_2_Init_m13386_gshared ();
extern "C" void Dictionary_2_InitArrays_m13388_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m13390_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18951_gshared ();
extern "C" void Dictionary_2_make_pair_m13392_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m13394_gshared ();
extern "C" void Dictionary_2_pick_value_m13396_gshared ();
extern "C" void Dictionary_2_CopyTo_m13398_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18952_gshared ();
extern "C" void Dictionary_2_Resize_m13400_gshared ();
extern "C" void Dictionary_2_Add_m13402_gshared ();
extern "C" void Dictionary_2_Clear_m13404_gshared ();
extern "C" void Dictionary_2_ContainsKey_m13406_gshared ();
extern "C" void Dictionary_2_ContainsValue_m13408_gshared ();
extern "C" void Dictionary_2_GetObjectData_m13410_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m13412_gshared ();
extern "C" void Dictionary_2_Remove_m13414_gshared ();
extern "C" void Dictionary_2_TryGetValue_m13416_gshared ();
extern "C" void Dictionary_2_ToTKey_m13422_gshared ();
extern "C" void Dictionary_2_ToTValue_m13424_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m13426_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m13428_gshared ();
void* RuntimeInvoker_Enumerator_t2597 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m13430_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Entry_m13550_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator_get_Key_m13551_gshared ();
extern "C" void ShimEnumerator_get_Value_m13552_gshared ();
extern "C" void ShimEnumerator_get_Current_m13553_gshared ();
extern "C" void ShimEnumerator__ctor_m13548_gshared ();
extern "C" void ShimEnumerator_MoveNext_m13549_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared ();
extern "C" void Enumerator_get_Current_m13511_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_CurrentKey_m13512_gshared ();
extern "C" void Enumerator_get_CurrentValue_m13513_gshared ();
extern "C" void Enumerator__ctor_m13505_gshared ();
extern "C" void Enumerator_MoveNext_m13510_gshared ();
extern "C" void Enumerator_VerifyState_m13514_gshared ();
extern "C" void Enumerator_VerifyCurrent_m13515_gshared ();
extern "C" void Enumerator_Dispose_m13516_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13494_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13495_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m13496_gshared ();
extern "C" void KeyCollection_get_Count_m13499_gshared ();
extern "C" void KeyCollection__ctor_m13486_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13487_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13488_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13489_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13490_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13491_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m13492_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13493_gshared ();
extern "C" void KeyCollection_CopyTo_m13497_gshared ();
extern "C" void KeyCollection_GetEnumerator_m13498_gshared ();
void* RuntimeInvoker_Enumerator_t2596 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13501_gshared ();
extern "C" void Enumerator_get_Current_m13504_gshared ();
extern "C" void Enumerator__ctor_m13500_gshared ();
extern "C" void Enumerator_Dispose_m13502_gshared ();
extern "C" void Enumerator_MoveNext_m13503_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13529_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13530_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m13531_gshared ();
extern "C" void ValueCollection_get_Count_m13534_gshared ();
extern "C" void ValueCollection__ctor_m13521_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13522_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13523_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13524_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13525_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13526_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m13527_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13528_gshared ();
extern "C" void ValueCollection_CopyTo_m13532_gshared ();
extern "C" void ValueCollection_GetEnumerator_m13533_gshared ();
void* RuntimeInvoker_Enumerator_t2600 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13536_gshared ();
extern "C" void Enumerator_get_Current_m13539_gshared ();
extern "C" void Enumerator__ctor_m13535_gshared ();
extern "C" void Enumerator_Dispose_m13537_gshared ();
extern "C" void Enumerator_MoveNext_m13538_gshared ();
extern "C" void Transform_1__ctor_m13517_gshared ();
extern "C" void Transform_1_Invoke_m13518_gshared ();
extern "C" void Transform_1_BeginInvoke_m13519_gshared ();
extern "C" void Transform_1_EndInvoke_m13520_gshared ();
extern "C" void EqualityComparer_1_get_Default_m11426_gshared ();
extern "C" void EqualityComparer_1__ctor_m11422_gshared ();
extern "C" void EqualityComparer_1__cctor_m11423_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m11424_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m11425_gshared ();
extern "C" void DefaultComparer__ctor_m11432_gshared ();
extern "C" void DefaultComparer_GetHashCode_m11433_gshared ();
extern "C" void DefaultComparer_Equals_m11434_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m18492_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18493_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m18494_gshared ();
extern "C" void KeyValuePair_2_get_Key_m13481_gshared ();
extern "C" void KeyValuePair_2_set_Key_m13482_gshared ();
extern "C" void KeyValuePair_2_get_Value_m13483_gshared ();
extern "C" void KeyValuePair_2_set_Value_m13484_gshared ();
extern "C" void KeyValuePair_2__ctor_m13480_gshared ();
extern "C" void KeyValuePair_2_ToString_m13485_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m3675_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m3676_gshared ();
extern "C" void List_1_get_Capacity_m11339_gshared ();
extern "C" void List_1_set_Capacity_m11341_gshared ();
extern "C" void List_1_get_Count_m3669_gshared ();
extern "C" void List_1_get_Item_m3692_gshared ();
extern "C" void List_1_set_Item_m3693_gshared ();
extern "C" void List_1__ctor_m3449_gshared ();
extern "C" void List_1__ctor_m11276_gshared ();
extern "C" void List_1__cctor_m11278_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m3672_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m3677_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m3679_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m3680_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m3681_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m3682_gshared ();
extern "C" void List_1_Add_m3685_gshared ();
extern "C" void List_1_GrowIfNeeded_m11296_gshared ();
extern "C" void List_1_AddCollection_m11298_gshared ();
extern "C" void List_1_AddEnumerable_m11300_gshared ();
extern "C" void List_1_AddRange_m11302_gshared ();
extern "C" void List_1_AsReadOnly_m11304_gshared ();
extern "C" void List_1_Clear_m3678_gshared ();
extern "C" void List_1_Contains_m3686_gshared ();
extern "C" void List_1_CopyTo_m3687_gshared ();
extern "C" void List_1_Find_m11309_gshared ();
extern "C" void List_1_CheckMatch_m11311_gshared ();
extern "C" void List_1_GetIndex_m11313_gshared ();
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GetEnumerator_m11315_gshared ();
void* RuntimeInvoker_Enumerator_t2429 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m3690_gshared ();
extern "C" void List_1_Shift_m11318_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckIndex_m11320_gshared ();
extern "C" void List_1_Insert_m3691_gshared ();
extern "C" void List_1_CheckCollection_m11323_gshared ();
extern "C" void List_1_Remove_m3688_gshared ();
extern "C" void List_1_RemoveAll_m11326_gshared ();
extern "C" void List_1_RemoveAt_m3683_gshared ();
extern "C" void List_1_Reverse_m11329_gshared ();
extern "C" void List_1_Sort_m11331_gshared ();
extern "C" void List_1_Sort_m11333_gshared ();
extern "C" void List_1_ToArray_m11335_gshared ();
extern "C" void List_1_TrimExcess_m11337_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared ();
extern "C" void Enumerator_get_Current_m11355_gshared ();
extern "C" void Enumerator__ctor_m11350_gshared ();
extern "C" void Enumerator_Dispose_m11352_gshared ();
extern "C" void Enumerator_VerifyState_m11353_gshared ();
extern "C" void Enumerator_MoveNext_m11354_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11387_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m11395_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m11396_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m11397_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m11398_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m11399_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m11400_gshared ();
extern "C" void Collection_1_get_Count_m11413_gshared ();
extern "C" void Collection_1_get_Item_m11414_gshared ();
extern "C" void Collection_1_set_Item_m11415_gshared ();
extern "C" void Collection_1__ctor_m11386_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m11388_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m11389_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m11390_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m11391_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m11392_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m11393_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m11394_gshared ();
extern "C" void Collection_1_Add_m11401_gshared ();
extern "C" void Collection_1_Clear_m11402_gshared ();
extern "C" void Collection_1_ClearItems_m11403_gshared ();
extern "C" void Collection_1_Contains_m11404_gshared ();
extern "C" void Collection_1_CopyTo_m11405_gshared ();
extern "C" void Collection_1_GetEnumerator_m11406_gshared ();
extern "C" void Collection_1_IndexOf_m11407_gshared ();
extern "C" void Collection_1_Insert_m11408_gshared ();
extern "C" void Collection_1_InsertItem_m11409_gshared ();
extern "C" void Collection_1_Remove_m11410_gshared ();
extern "C" void Collection_1_RemoveAt_m11411_gshared ();
extern "C" void Collection_1_RemoveItem_m11412_gshared ();
extern "C" void Collection_1_SetItem_m11416_gshared ();
extern "C" void Collection_1_IsValidItem_m11417_gshared ();
extern "C" void Collection_1_ConvertItem_m11418_gshared ();
extern "C" void Collection_1_CheckWritable_m11419_gshared ();
extern "C" void Collection_1_IsSynchronized_m11420_gshared ();
extern "C" void Collection_1_IsFixedSize_m11421_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11362_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11363_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11364_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11374_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11375_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11376_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11377_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m11378_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m11379_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m11384_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m11385_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m11356_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11357_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11358_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11359_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11360_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11361_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11365_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11366_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m11367_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m11368_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m11369_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11370_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m11371_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m11372_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11373_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m11380_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m11381_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m11382_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m11383_gshared ();
extern "C" void MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m19381_gshared ();
extern "C" void MonoProperty_StaticGetterAdapterFrame_TisObject_t_m19382_gshared ();
extern "C" void Getter_2__ctor_m18550_gshared ();
extern "C" void Getter_2_Invoke_m18551_gshared ();
extern "C" void Getter_2_BeginInvoke_m18552_gshared ();
extern "C" void Getter_2_EndInvoke_m18553_gshared ();
extern "C" void StaticGetter_1__ctor_m18554_gshared ();
extern "C" void StaticGetter_1_Invoke_m18555_gshared ();
extern "C" void StaticGetter_1_BeginInvoke_m18556_gshared ();
extern "C" void StaticGetter_1_EndInvoke_m18557_gshared ();
extern "C" void Activator_CreateInstance_TisObject_t_m18822_gshared ();
extern "C" void Action_1__ctor_m13652_gshared ();
extern "C" void Action_1_Invoke_m13654_gshared ();
extern "C" void Action_1_BeginInvoke_m13656_gshared ();
extern "C" void Action_1_EndInvoke_m13658_gshared ();
extern "C" void Comparison_1__ctor_m11465_gshared ();
extern "C" void Comparison_1_Invoke_m11466_gshared ();
extern "C" void Comparison_1_BeginInvoke_m11467_gshared ();
extern "C" void Comparison_1_EndInvoke_m11468_gshared ();
extern "C" void Converter_2__ctor_m18435_gshared ();
extern "C" void Converter_2_Invoke_m18436_gshared ();
extern "C" void Converter_2_BeginInvoke_m18437_gshared ();
extern "C" void Converter_2_EndInvoke_m18438_gshared ();
extern "C" void Predicate_1__ctor_m11435_gshared ();
extern "C" void Predicate_1_Invoke_m11436_gshared ();
extern "C" void Predicate_1_BeginInvoke_m11437_gshared ();
extern "C" void Predicate_1_EndInvoke_m11438_gshared ();
extern "C" void Func_2__ctor_m11608_gshared ();
extern "C" void Comparison_1__ctor_m1454_gshared ();
extern "C" void List_1_Sort_m1460_gshared ();
extern "C" void List_1__ctor_m1505_gshared ();
extern "C" void Dictionary_2__ctor_m12701_gshared ();
extern "C" void Dictionary_2_get_Values_m12788_gshared ();
extern "C" void ValueCollection_GetEnumerator_m12861_gshared ();
void* RuntimeInvoker_Enumerator_t2544 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m12867_gshared ();
extern "C" void Enumerator_MoveNext_m12866_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m12795_gshared ();
void* RuntimeInvoker_Enumerator_t2541 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator_get_Current_m12839_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Value_m12806_gshared ();
extern "C" void KeyValuePair_2_get_Key_m12804_gshared ();
extern "C" void Enumerator_MoveNext_m12838_gshared ();
extern "C" void KeyValuePair_2_ToString_m12808_gshared ();
extern "C" void Comparison_1__ctor_m1581_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t323_m1575_gshared ();
extern "C" void UnityEvent_1__ctor_m1586_gshared ();
extern "C" void UnityEvent_1_Invoke_m1588_gshared ();
void* RuntimeInvoker_Void_t71_Color_t163 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m1589_gshared ();
extern "C" void TweenRunner_1__ctor_m1615_gshared ();
extern "C" void TweenRunner_1_Init_m1616_gshared ();
extern "C" void UnityAction_1__ctor_m1644_gshared ();
extern "C" void TweenRunner_1_StartTween_m1645_gshared ();
void* RuntimeInvoker_Void_t71_ColorTween_t162 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_get_Capacity_m1649_gshared ();
extern "C" void List_1_set_Capacity_m1650_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisInt32_t54_m1677_gshared ();
void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisByte_t367_m1679_gshared ();
void* RuntimeInvoker_Boolean_t72_ByteU26_t1360_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSingle_t85_m1681_gshared ();
void* RuntimeInvoker_Boolean_t72_SingleU26_t831_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m1744_gshared ();
extern "C" void SetPropertyUtility_SetStruct_TisUInt16_t371_m1739_gshared ();
void* RuntimeInvoker_Boolean_t72_UInt16U26_t2046_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_ToArray_m1801_gshared ();
extern "C" void UnityEvent_1__ctor_m1840_gshared ();
extern "C" void UnityEvent_1_Invoke_m1846_gshared ();
extern "C" void UnityEvent_1__ctor_m1851_gshared ();
extern "C" void UnityAction_1__ctor_m1852_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m1853_gshared ();
extern "C" void UnityEvent_1_AddListener_m1854_gshared ();
extern "C" void UnityEvent_1_Invoke_m1860_gshared ();
void* RuntimeInvoker_Void_t71_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisNavigation_t232_m1876_gshared ();
void* RuntimeInvoker_Boolean_t72_NavigationU26_t3380_Navigation_t232 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisColorBlock_t175_m1878_gshared ();
void* RuntimeInvoker_Boolean_t72_ColorBlockU26_t3381_ColorBlock_t175 (const MethodInfo* method, void* obj, void** args);
extern "C" void SetPropertyUtility_SetStruct_TisSpriteState_t252_m1879_gshared ();
void* RuntimeInvoker_Boolean_t72_SpriteStateU26_t3382_SpriteState_t252 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1__ctor_m14597_gshared ();
extern "C" void UnityEvent_1_Invoke_m14606_gshared ();
void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisInt32_t54_m1937_gshared ();
void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisVector2_t53_m1939_gshared ();
void* RuntimeInvoker_Void_t71_Vector2U26_t825_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisSingle_t85_m1946_gshared ();
void* RuntimeInvoker_Void_t71_SingleU26_t831_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void LayoutGroup_SetProperty_TisByte_t367_m1948_gshared ();
void* RuntimeInvoker_Void_t71_ByteU26_t1360_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2__ctor_m14814_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2118_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2119_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2127_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2128_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m2129_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m2130_gshared ();
extern "C" void UnityEvent_1_FindMethod_Impl_m14602_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m14603_gshared ();
extern "C" void List_1__ctor_m3423_gshared ();
extern "C" void List_1__ctor_m3424_gshared ();
extern "C" void List_1__ctor_m3425_gshared ();
extern "C" void Dictionary_2__ctor_m16542_gshared ();
extern "C" void Dictionary_2__ctor_m17276_gshared ();
extern "C" void CachedInvokableCall_1__ctor_m3517_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m3518_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1__ctor_m17792_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m17992_gshared ();
extern "C" void Dictionary_2__ctor_m13097_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t54_m4820_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m10414_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m10415_gshared ();
extern "C" void GenericComparer_1__ctor_m10416_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m10417_gshared ();
extern "C" void Nullable_1__ctor_m10418_gshared ();
void* RuntimeInvoker_Void_t71_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_get_HasValue_m10419_gshared ();
extern "C" void Nullable_1_get_Value_m10420_gshared ();
void* RuntimeInvoker_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericComparer_1__ctor_m10421_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m10422_gshared ();
extern "C" void GenericComparer_1__ctor_m10423_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m10424_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt32_t54_m18782_gshared ();
void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt32_t54_m18783_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt32_t54_m18784_gshared ();
void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt32_t54_m18785_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt32_t54_m18786_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt32_t54_m18787_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt32_t54_m18788_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt32_t54_m18790_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t54_m18791_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDouble_t752_m18793_gshared ();
void* RuntimeInvoker_Double_t752_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDouble_t752_m18794_gshared ();
void* RuntimeInvoker_Void_t71_Double_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDouble_t752_m18795_gshared ();
void* RuntimeInvoker_Boolean_t72_Double_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDouble_t752_m18796_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDouble_t752_m18797_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDouble_t752_m18798_gshared ();
void* RuntimeInvoker_Int32_t54_Double_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDouble_t752_m18799_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Double_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDouble_t752_m18801_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t752_m18802_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt16_t371_m18804_gshared ();
void* RuntimeInvoker_UInt16_t371_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt16_t371_m18805_gshared ();
void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt16_t371_m18806_gshared ();
void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt16_t371_m18807_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt16_t371_m18808_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt16_t371_m18809_gshared ();
void* RuntimeInvoker_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUInt16_t371_m18810_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUInt16_t371_m18812_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t371_m18813_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastResult_t137_m18828_gshared ();
void* RuntimeInvoker_RaycastResult_t137_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastResult_t137_m18829_gshared ();
void* RuntimeInvoker_Void_t71_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastResult_t137_m18830_gshared ();
void* RuntimeInvoker_Boolean_t72_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t137_m18831_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastResult_t137_m18832_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastResult_t137_m18833_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastResult_t137_m18834_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastResult_t137_m18836_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t137_m18837_gshared ();
extern "C" void Array_Resize_TisRaycastResult_t137_m18839_gshared ();
void* RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisRaycastResult_t137_m18838_gshared ();
void* RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisRaycastResult_t137_m18840_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_RaycastResult_t137_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisRaycastResult_t137_m18842_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t137_TisRaycastResult_t137_m18841_gshared ();
extern "C" void Array_get_swapper_TisRaycastResult_t137_m18843_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t137_TisRaycastResult_t137_m18844_gshared ();
extern "C" void Array_compare_TisRaycastResult_t137_m18845_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisRaycastResult_t137_TisRaycastResult_t137_m18846_gshared ();
extern "C" void Array_Sort_TisRaycastResult_t137_m18848_gshared ();
extern "C" void Array_qsort_TisRaycastResult_t137_m18847_gshared ();
extern "C" void Array_swap_TisRaycastResult_t137_m18849_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2536_m18853_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2536_m18854_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2536_m18855_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2536_m18856_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2536_m18857_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2536_m18858_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2536_m18859_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2536 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2536_m18861_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2536_m18862_gshared ();
extern "C" void Array_InternalArray__get_Item_TisLink_t1459_m18864_gshared ();
void* RuntimeInvoker_Link_t1459_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisLink_t1459_m18865_gshared ();
void* RuntimeInvoker_Void_t71_Link_t1459 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisLink_t1459_m18866_gshared ();
void* RuntimeInvoker_Boolean_t72_Link_t1459 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLink_t1459_m18867_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisLink_t1459_m18868_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisLink_t1459_m18869_gshared ();
void* RuntimeInvoker_Int32_t54_Link_t1459 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisLink_t1459_m18870_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Link_t1459 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisLink_t1459_m18872_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1459_m18873_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m18875_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m18874_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m18876_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18878_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18877_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDictionaryEntry_t1147_m18880_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1147_m18881_gshared ();
void* RuntimeInvoker_Void_t71_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1147_m18882_gshared ();
void* RuntimeInvoker_Boolean_t72_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1147_m18883_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1147_m18884_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDictionaryEntry_t1147_m18885_gshared ();
void* RuntimeInvoker_Int32_t54_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDictionaryEntry_t1147_m18886_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDictionaryEntry_t1147_m18888_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1147_m18889_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18890_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2536_m18892_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2536_TisObject_t_m18891_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2536_TisKeyValuePair_2_t2536_m18893_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit2D_t349_m18895_gshared ();
void* RuntimeInvoker_RaycastHit2D_t349_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit2D_t349_m18896_gshared ();
void* RuntimeInvoker_Void_t71_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t349_m18897_gshared ();
void* RuntimeInvoker_Boolean_t72_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t349_m18898_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t349_m18899_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit2D_t349_m18900_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit2D_t349_m18901_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit2D_t349_m18903_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t349_m18904_gshared ();
extern "C" void Array_InternalArray__get_Item_TisRaycastHit_t323_m18906_gshared ();
void* RuntimeInvoker_RaycastHit_t323_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisRaycastHit_t323_m18907_gshared ();
void* RuntimeInvoker_Void_t71_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisRaycastHit_t323_m18908_gshared ();
void* RuntimeInvoker_Boolean_t72_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t323_m18909_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisRaycastHit_t323_m18910_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisRaycastHit_t323_m18911_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisRaycastHit_t323_m18912_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisRaycastHit_t323_m18914_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t323_m18915_gshared ();
extern "C" void Array_Sort_TisRaycastHit_t323_m18916_gshared ();
extern "C" void Array_qsort_TisRaycastHit_t323_m18917_gshared ();
extern "C" void Array_swap_TisRaycastHit_t323_m18918_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t163_m18919_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2567_m18921_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2567_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2567_m18922_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2567_m18923_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2567_m18924_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2567_m18925_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2567_m18926_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2567_m18927_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2567_m18929_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2567_m18930_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18932_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18931_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m18934_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m18933_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m18935_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18936_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2567_m18938_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2567_TisObject_t_m18937_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2567_TisKeyValuePair_2_t2567_m18939_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2593_m18941_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2593_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2593_m18942_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2593_m18943_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2593_m18944_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2593_m18945_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2593_m18946_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2593_m18947_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2593_m18949_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2593_m18950_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18953_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2593_m18955_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisObject_t_m18954_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisKeyValuePair_2_t2593_m18956_gshared ();
extern "C" void Array_Resize_TisUIVertex_t225_m18958_gshared ();
void* RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUIVertex_t225_m18957_gshared ();
void* RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUIVertex_t225_m18959_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_UIVertex_t225_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUIVertex_t225_m18961_gshared ();
extern "C" void Array_Sort_TisUIVertex_t225_TisUIVertex_t225_m18960_gshared ();
extern "C" void Array_get_swapper_TisUIVertex_t225_m18962_gshared ();
extern "C" void Array_qsort_TisUIVertex_t225_TisUIVertex_t225_m18963_gshared ();
extern "C" void Array_compare_TisUIVertex_t225_m18964_gshared ();
void* RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUIVertex_t225_TisUIVertex_t225_m18965_gshared ();
extern "C" void Array_Sort_TisUIVertex_t225_m18967_gshared ();
extern "C" void Array_qsort_TisUIVertex_t225_m18966_gshared ();
extern "C" void Array_swap_TisUIVertex_t225_m18968_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector2_t53_m18970_gshared ();
void* RuntimeInvoker_Vector2_t53_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector2_t53_m18971_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisVector2_t53_m18972_gshared ();
void* RuntimeInvoker_Boolean_t72_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector2_t53_m18973_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector2_t53_m18974_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector2_t53_m18975_gshared ();
void* RuntimeInvoker_Int32_t54_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector2_t53_m18976_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector2_t53_m18978_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t53_m18979_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUILineInfo_t375_m18981_gshared ();
void* RuntimeInvoker_UILineInfo_t375_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUILineInfo_t375_m18982_gshared ();
void* RuntimeInvoker_Void_t71_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUILineInfo_t375_m18983_gshared ();
void* RuntimeInvoker_Boolean_t72_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t375_m18984_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUILineInfo_t375_m18985_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUILineInfo_t375_m18986_gshared ();
void* RuntimeInvoker_Int32_t54_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUILineInfo_t375_m18987_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUILineInfo_t375_m18989_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t375_m18990_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUICharInfo_t377_m18992_gshared ();
void* RuntimeInvoker_UICharInfo_t377_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUICharInfo_t377_m18993_gshared ();
void* RuntimeInvoker_Void_t71_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUICharInfo_t377_m18994_gshared ();
void* RuntimeInvoker_Boolean_t72_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t377_m18995_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUICharInfo_t377_m18996_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUICharInfo_t377_m18997_gshared ();
void* RuntimeInvoker_Int32_t54_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUICharInfo_t377_m18998_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUICharInfo_t377_m19000_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t377_m19001_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t85_m19002_gshared ();
extern "C" void Array_InternalArray__get_Item_TisVector3_t138_m19004_gshared ();
void* RuntimeInvoker_Vector3_t138_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisVector3_t138_m19005_gshared ();
void* RuntimeInvoker_Void_t71_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisVector3_t138_m19006_gshared ();
void* RuntimeInvoker_Boolean_t72_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisVector3_t138_m19007_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisVector3_t138_m19008_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisVector3_t138_m19009_gshared ();
void* RuntimeInvoker_Int32_t54_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisVector3_t138_m19010_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisVector3_t138_m19012_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t138_m19013_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t53_m19014_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisByte_t367_m19015_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcAchievementData_t650_m19021_gshared ();
void* RuntimeInvoker_GcAchievementData_t650_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcAchievementData_t650_m19022_gshared ();
void* RuntimeInvoker_Void_t71_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcAchievementData_t650_m19023_gshared ();
void* RuntimeInvoker_Boolean_t72_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t650_m19024_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcAchievementData_t650_m19025_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcAchievementData_t650_m19026_gshared ();
void* RuntimeInvoker_Int32_t54_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcAchievementData_t650_m19027_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcAchievementData_t650_m19029_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t650_m19030_gshared ();
extern "C" void Array_InternalArray__get_Item_TisGcScoreData_t651_m19032_gshared ();
void* RuntimeInvoker_GcScoreData_t651_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisGcScoreData_t651_m19033_gshared ();
void* RuntimeInvoker_Void_t71_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisGcScoreData_t651_m19034_gshared ();
void* RuntimeInvoker_Boolean_t72_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t651_m19035_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisGcScoreData_t651_m19036_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisGcScoreData_t651_m19037_gshared ();
void* RuntimeInvoker_Int32_t54_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisGcScoreData_t651_m19038_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisGcScoreData_t651_m19040_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t651_m19041_gshared ();
extern "C" void Array_InternalArray__get_Item_TisByte_t367_m19043_gshared ();
void* RuntimeInvoker_Byte_t367_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisByte_t367_m19044_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisByte_t367_m19045_gshared ();
void* RuntimeInvoker_Boolean_t72_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisByte_t367_m19046_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisByte_t367_m19047_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisByte_t367_m19048_gshared ();
void* RuntimeInvoker_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisByte_t367_m19049_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisByte_t367_m19051_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t367_m19052_gshared ();
extern "C" void Array_InternalArray__get_Item_TisIntPtr_t_m19054_gshared ();
void* RuntimeInvoker_IntPtr_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisIntPtr_t_m19055_gshared ();
void* RuntimeInvoker_Void_t71_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisIntPtr_t_m19056_gshared ();
void* RuntimeInvoker_Boolean_t72_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m19057_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisIntPtr_t_m19058_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisIntPtr_t_m19059_gshared ();
void* RuntimeInvoker_Int32_t54_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisIntPtr_t_m19060_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisIntPtr_t_m19062_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m19063_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSingle_t85_m19065_gshared ();
void* RuntimeInvoker_Single_t85_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSingle_t85_m19066_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSingle_t85_m19067_gshared ();
void* RuntimeInvoker_Boolean_t72_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSingle_t85_m19068_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSingle_t85_m19069_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSingle_t85_m19070_gshared ();
void* RuntimeInvoker_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSingle_t85_m19071_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSingle_t85_m19073_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t85_m19074_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyframe_t580_m19076_gshared ();
void* RuntimeInvoker_Keyframe_t580_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyframe_t580_m19077_gshared ();
void* RuntimeInvoker_Void_t71_Keyframe_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyframe_t580_m19078_gshared ();
void* RuntimeInvoker_Boolean_t72_Keyframe_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyframe_t580_m19079_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyframe_t580_m19080_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyframe_t580_m19081_gshared ();
void* RuntimeInvoker_Int32_t54_Keyframe_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyframe_t580_m19082_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Keyframe_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyframe_t580_m19084_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t580_m19085_gshared ();
extern "C" void Array_Resize_TisUICharInfo_t377_m19087_gshared ();
void* RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUICharInfo_t377_m19086_gshared ();
void* RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUICharInfo_t377_m19088_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_UICharInfo_t377_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUICharInfo_t377_m19090_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t377_TisUICharInfo_t377_m19089_gshared ();
extern "C" void Array_get_swapper_TisUICharInfo_t377_m19091_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t377_TisUICharInfo_t377_m19092_gshared ();
extern "C" void Array_compare_TisUICharInfo_t377_m19093_gshared ();
void* RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUICharInfo_t377_TisUICharInfo_t377_m19094_gshared ();
extern "C" void Array_Sort_TisUICharInfo_t377_m19096_gshared ();
extern "C" void Array_qsort_TisUICharInfo_t377_m19095_gshared ();
extern "C" void Array_swap_TisUICharInfo_t377_m19097_gshared ();
extern "C" void Array_Resize_TisUILineInfo_t375_m19099_gshared ();
void* RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Resize_TisUILineInfo_t375_m19098_gshared ();
void* RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_IndexOf_TisUILineInfo_t375_m19100_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_UILineInfo_t375_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_Sort_TisUILineInfo_t375_m19102_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t375_TisUILineInfo_t375_m19101_gshared ();
extern "C" void Array_get_swapper_TisUILineInfo_t375_m19103_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t375_TisUILineInfo_t375_m19104_gshared ();
extern "C" void Array_compare_TisUILineInfo_t375_m19105_gshared ();
void* RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_swap_TisUILineInfo_t375_TisUILineInfo_t375_m19106_gshared ();
extern "C" void Array_Sort_TisUILineInfo_t375_m19108_gshared ();
extern "C" void Array_qsort_TisUILineInfo_t375_m19107_gshared ();
extern "C" void Array_swap_TisUILineInfo_t375_m19109_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2780_m19111_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2780_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2780_m19112_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2780_m19113_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2780_m19114_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2780_m19115_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2780_m19116_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2780_m19117_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2780_m19119_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2780_m19120_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt64_t753_m19122_gshared ();
void* RuntimeInvoker_Int64_t753_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt64_t753_m19123_gshared ();
void* RuntimeInvoker_Void_t71_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisInt64_t753_m19124_gshared ();
void* RuntimeInvoker_Boolean_t72_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt64_t753_m19125_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt64_t753_m19126_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt64_t753_m19127_gshared ();
void* RuntimeInvoker_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisInt64_t753_m19128_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisInt64_t753_m19130_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t753_m19131_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19133_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19132_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t753_m19135_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t753_TisObject_t_m19134_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt64_t753_TisInt64_t753_m19136_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19137_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2780_m19139_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2780_TisObject_t_m19138_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2780_TisKeyValuePair_2_t2780_m19140_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2818_m19142_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2818_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2818_m19143_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2818_m19144_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2818_m19145_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2818_m19146_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2818_m19147_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2818_m19148_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2818_m19150_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2818_m19151_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt64_t756_m19153_gshared ();
void* RuntimeInvoker_UInt64_t756_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt64_t756_m19154_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt64_t756_m19155_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt64_t756_m19156_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt64_t756_m19157_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt64_t756_m19158_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt64_t756_m19159_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt64_t756_m19161_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t756_m19162_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t756_m19164_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t756_TisObject_t_m19163_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisUInt64_t756_TisUInt64_t756_m19165_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19167_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19166_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19168_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2818_m19170_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2818_TisObject_t_m19169_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2818_TisKeyValuePair_2_t2818_m19171_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2839_m19173_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2839_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2839_m19174_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2839_m19175_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2839_m19176_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2839_m19177_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2839_m19178_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2839_m19179_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2839_m19181_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2839_m19182_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19184_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19183_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2593_m19186_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisObject_t_m19185_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisKeyValuePair_2_t2593_m19187_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19188_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2839_m19190_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2839_TisObject_t_m19189_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2839_TisKeyValuePair_2_t2839_m19191_gshared ();
extern "C" void Array_InternalArray__get_Item_TisParameterModifier_t1584_m19193_gshared ();
void* RuntimeInvoker_ParameterModifier_t1584_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisParameterModifier_t1584_m19194_gshared ();
void* RuntimeInvoker_Void_t71_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisParameterModifier_t1584_m19195_gshared ();
void* RuntimeInvoker_Boolean_t72_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t1584_m19196_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisParameterModifier_t1584_m19197_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisParameterModifier_t1584_m19198_gshared ();
void* RuntimeInvoker_Int32_t54_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisParameterModifier_t1584_m19199_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisParameterModifier_t1584_m19201_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t1584_m19202_gshared ();
extern "C" void Array_InternalArray__get_Item_TisHitInfo_t665_m19204_gshared ();
void* RuntimeInvoker_HitInfo_t665_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisHitInfo_t665_m19205_gshared ();
void* RuntimeInvoker_Void_t71_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisHitInfo_t665_m19206_gshared ();
void* RuntimeInvoker_Boolean_t72_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisHitInfo_t665_m19207_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisHitInfo_t665_m19208_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisHitInfo_t665_m19209_gshared ();
void* RuntimeInvoker_Int32_t54_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisHitInfo_t665_m19210_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisHitInfo_t665_m19212_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t665_m19213_gshared ();
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t54_m19214_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2933_m19216_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2933_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2933_m19217_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2933_m19218_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2933_m19219_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2933_m19220_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2933_m19221_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2933_m19222_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2933_m19224_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2933_m19225_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19227_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19226_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisByte_t367_m19229_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t367_TisObject_t_m19228_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisByte_t367_TisByte_t367_m19230_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19231_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2933_m19233_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2933_TisObject_t_m19232_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2933_TisKeyValuePair_2_t2933_m19234_gshared ();
extern "C" void Array_InternalArray__get_Item_TisX509ChainStatus_t1045_m19236_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1045_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1045_m19237_gshared ();
void* RuntimeInvoker_Void_t71_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1045_m19238_gshared ();
void* RuntimeInvoker_Boolean_t72_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1045_m19239_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1045_m19240_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisX509ChainStatus_t1045_m19241_gshared ();
void* RuntimeInvoker_Int32_t54_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisX509ChainStatus_t1045_m19242_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisX509ChainStatus_t1045_m19244_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1045_m19245_gshared ();
extern "C" void Array_InternalArray__get_Item_TisKeyValuePair_2_t2956_m19247_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2956_m19248_gshared ();
void* RuntimeInvoker_Void_t71_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2956_m19249_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2956_m19250_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2956_m19251_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisKeyValuePair_2_t2956_m19252_gshared ();
void* RuntimeInvoker_Int32_t54_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisKeyValuePair_2_t2956_m19253_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisKeyValuePair_2_t2956_m19255_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2956_m19256_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m19258_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m19257_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m19259_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19260_gshared ();
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2956_m19262_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2956_TisObject_t_m19261_gshared ();
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2956_TisKeyValuePair_2_t2956_m19263_gshared ();
extern "C" void Array_BinarySearch_TisInt32_t54_m19264_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__get_Item_TisMark_t1099_m19266_gshared ();
void* RuntimeInvoker_Mark_t1099_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisMark_t1099_m19267_gshared ();
void* RuntimeInvoker_Void_t71_Mark_t1099 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisMark_t1099_m19268_gshared ();
void* RuntimeInvoker_Boolean_t72_Mark_t1099 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisMark_t1099_m19269_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisMark_t1099_m19270_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisMark_t1099_m19271_gshared ();
void* RuntimeInvoker_Int32_t54_Mark_t1099 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisMark_t1099_m19272_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Mark_t1099 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisMark_t1099_m19274_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1099_m19275_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUriScheme_t1135_m19277_gshared ();
void* RuntimeInvoker_UriScheme_t1135_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUriScheme_t1135_m19278_gshared ();
void* RuntimeInvoker_Void_t71_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisUriScheme_t1135_m19279_gshared ();
void* RuntimeInvoker_Boolean_t72_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1135_m19280_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUriScheme_t1135_m19281_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUriScheme_t1135_m19282_gshared ();
void* RuntimeInvoker_Int32_t54_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisUriScheme_t1135_m19283_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisUriScheme_t1135_m19285_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1135_m19286_gshared ();
extern "C" void Array_InternalArray__get_Item_TisUInt32_t744_m19288_gshared ();
void* RuntimeInvoker_UInt32_t744_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisUInt32_t744_m19289_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisUInt32_t744_m19290_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisUInt32_t744_m19291_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisUInt32_t744_m19292_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisUInt32_t744_m19293_gshared ();
extern "C" void Array_InternalArray__Insert_TisUInt32_t744_m19294_gshared ();
extern "C" void Array_InternalArray__set_Item_TisUInt32_t744_m19296_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t744_m19297_gshared ();
extern "C" void Array_InternalArray__get_Item_TisInt16_t448_m19299_gshared ();
void* RuntimeInvoker_Int16_t448_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisInt16_t448_m19300_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisInt16_t448_m19301_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisInt16_t448_m19302_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisInt16_t448_m19303_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisInt16_t448_m19304_gshared ();
extern "C" void Array_InternalArray__Insert_TisInt16_t448_m19305_gshared ();
extern "C" void Array_InternalArray__set_Item_TisInt16_t448_m19307_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t448_m19308_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSByte_t73_m19310_gshared ();
void* RuntimeInvoker_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSByte_t73_m19311_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisSByte_t73_m19312_gshared ();
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSByte_t73_m19313_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSByte_t73_m19314_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSByte_t73_m19315_gshared ();
extern "C" void Array_InternalArray__Insert_TisSByte_t73_m19316_gshared ();
extern "C" void Array_InternalArray__set_Item_TisSByte_t73_m19318_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t73_m19319_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTableRange_t1392_m19349_gshared ();
void* RuntimeInvoker_TableRange_t1392_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTableRange_t1392_m19350_gshared ();
void* RuntimeInvoker_Void_t71_TableRange_t1392 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisTableRange_t1392_m19351_gshared ();
void* RuntimeInvoker_Boolean_t72_TableRange_t1392 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTableRange_t1392_m19352_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTableRange_t1392_m19353_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTableRange_t1392_m19354_gshared ();
void* RuntimeInvoker_Int32_t54_TableRange_t1392 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTableRange_t1392_m19355_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_TableRange_t1392 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTableRange_t1392_m19357_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1392_m19358_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1469_m19360_gshared ();
void* RuntimeInvoker_Slot_t1469_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1469_m19361_gshared ();
void* RuntimeInvoker_Void_t71_Slot_t1469 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1469_m19362_gshared ();
void* RuntimeInvoker_Boolean_t72_Slot_t1469 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1469_m19363_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1469_m19364_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1469_m19365_gshared ();
void* RuntimeInvoker_Int32_t54_Slot_t1469 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t1469_m19366_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Slot_t1469 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t1469_m19368_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1469_m19369_gshared ();
extern "C" void Array_InternalArray__get_Item_TisSlot_t1476_m19371_gshared ();
void* RuntimeInvoker_Slot_t1476_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisSlot_t1476_m19372_gshared ();
void* RuntimeInvoker_Void_t71_Slot_t1476 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisSlot_t1476_m19373_gshared ();
void* RuntimeInvoker_Boolean_t72_Slot_t1476 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisSlot_t1476_m19374_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisSlot_t1476_m19375_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisSlot_t1476_m19376_gshared ();
void* RuntimeInvoker_Int32_t54_Slot_t1476 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisSlot_t1476_m19377_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Slot_t1476 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisSlot_t1476_m19379_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1476_m19380_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDateTime_t508_m19384_gshared ();
void* RuntimeInvoker_DateTime_t508_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t508_m19385_gshared ();
void* RuntimeInvoker_Void_t71_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDateTime_t508_m19386_gshared ();
void* RuntimeInvoker_Boolean_t72_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t508_m19387_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDateTime_t508_m19388_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDateTime_t508_m19389_gshared ();
void* RuntimeInvoker_Int32_t54_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDateTime_t508_m19390_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDateTime_t508_m19392_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t508_m19393_gshared ();
extern "C" void Array_InternalArray__get_Item_TisDecimal_t755_m19395_gshared ();
void* RuntimeInvoker_Decimal_t755_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t755_m19396_gshared ();
void* RuntimeInvoker_Void_t71_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Contains_TisDecimal_t755_m19397_gshared ();
void* RuntimeInvoker_Boolean_t72_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t755_m19398_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisDecimal_t755_m19399_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisDecimal_t755_m19400_gshared ();
void* RuntimeInvoker_Int32_t54_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisDecimal_t755_m19401_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisDecimal_t755_m19403_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t755_m19404_gshared ();
extern "C" void Array_InternalArray__get_Item_TisTimeSpan_t1051_m19406_gshared ();
void* RuntimeInvoker_TimeSpan_t1051_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t1051_m19407_gshared ();
extern "C" void Array_InternalArray__ICollection_Contains_TisTimeSpan_t1051_m19408_gshared ();
void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1051_m19409_gshared ();
extern "C" void Array_InternalArray__ICollection_Remove_TisTimeSpan_t1051_m19410_gshared ();
extern "C" void Array_InternalArray__IndexOf_TisTimeSpan_t1051_m19411_gshared ();
void* RuntimeInvoker_Int32_t54_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t1051_m19412_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t1051_m19414_gshared ();
extern "C" void Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1051_m19415_gshared ();
extern "C" void InternalEnumerator_1__ctor_m11445_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11446_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m11447_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m11448_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m11449_gshared ();
extern "C" void InternalEnumerator_1__ctor_m11450_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11451_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m11452_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m11453_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m11454_gshared ();
void* RuntimeInvoker_Double_t752 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m11456_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11458_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m11460_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m11462_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m11464_gshared ();
void* RuntimeInvoker_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_Invoke_m11610_gshared ();
void* RuntimeInvoker_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m11612_gshared ();
extern "C" void Func_2_EndInvoke_m11614_gshared ();
extern "C" void Comparison_1_Invoke_m11884_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m11885_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t137_RaycastResult_t137_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m11886_gshared ();
extern "C" void List_1__ctor_m12128_gshared ();
extern "C" void List_1__cctor_m12129_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12130_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12131_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m12132_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m12133_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m12134_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m12135_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m12136_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m12137_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12138_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m12139_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m12140_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m12141_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m12142_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m12143_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m12144_gshared ();
extern "C" void List_1_Add_m12145_gshared ();
extern "C" void List_1_GrowIfNeeded_m12146_gshared ();
extern "C" void List_1_AddCollection_m12147_gshared ();
extern "C" void List_1_AddEnumerable_m12148_gshared ();
extern "C" void List_1_AddRange_m12149_gshared ();
extern "C" void List_1_AsReadOnly_m12150_gshared ();
extern "C" void List_1_Clear_m12151_gshared ();
extern "C" void List_1_Contains_m12152_gshared ();
extern "C" void List_1_CopyTo_m12153_gshared ();
extern "C" void List_1_Find_m12154_gshared ();
void* RuntimeInvoker_RaycastResult_t137_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m12155_gshared ();
extern "C" void List_1_GetIndex_m12156_gshared ();
extern "C" void List_1_GetEnumerator_m12157_gshared ();
void* RuntimeInvoker_Enumerator_t2495 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m12158_gshared ();
extern "C" void List_1_Shift_m12159_gshared ();
extern "C" void List_1_CheckIndex_m12160_gshared ();
extern "C" void List_1_Insert_m12161_gshared ();
extern "C" void List_1_CheckCollection_m12162_gshared ();
extern "C" void List_1_Remove_m12163_gshared ();
extern "C" void List_1_RemoveAll_m12164_gshared ();
extern "C" void List_1_RemoveAt_m12165_gshared ();
extern "C" void List_1_Reverse_m12166_gshared ();
extern "C" void List_1_Sort_m12167_gshared ();
extern "C" void List_1_ToArray_m12168_gshared ();
extern "C" void List_1_TrimExcess_m12169_gshared ();
extern "C" void List_1_get_Capacity_m12170_gshared ();
extern "C" void List_1_set_Capacity_m12171_gshared ();
extern "C" void List_1_get_Count_m12172_gshared ();
extern "C" void List_1_get_Item_m12173_gshared ();
extern "C" void List_1_set_Item_m12174_gshared ();
extern "C" void InternalEnumerator_1__ctor_m12175_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12176_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m12177_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m12178_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m12179_gshared ();
void* RuntimeInvoker_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Enumerator__ctor_m12180_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m12181_gshared ();
extern "C" void Enumerator_Dispose_m12182_gshared ();
extern "C" void Enumerator_VerifyState_m12183_gshared ();
extern "C" void Enumerator_MoveNext_m12184_gshared ();
extern "C" void Enumerator_get_Current_m12185_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m12186_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12187_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12188_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12189_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12190_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12191_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12192_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12193_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12194_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12195_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12196_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m12197_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m12198_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m12199_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12200_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m12201_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m12202_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12203_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12204_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12205_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12206_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12207_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m12208_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m12209_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m12210_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m12211_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m12212_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m12213_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m12214_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m12215_gshared ();
extern "C" void Collection_1__ctor_m12216_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12217_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12218_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m12219_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m12220_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m12221_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m12222_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m12223_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m12224_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m12225_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m12226_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m12227_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m12228_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m12229_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m12230_gshared ();
extern "C" void Collection_1_Add_m12231_gshared ();
extern "C" void Collection_1_Clear_m12232_gshared ();
extern "C" void Collection_1_ClearItems_m12233_gshared ();
extern "C" void Collection_1_Contains_m12234_gshared ();
extern "C" void Collection_1_CopyTo_m12235_gshared ();
extern "C" void Collection_1_GetEnumerator_m12236_gshared ();
extern "C" void Collection_1_IndexOf_m12237_gshared ();
extern "C" void Collection_1_Insert_m12238_gshared ();
extern "C" void Collection_1_InsertItem_m12239_gshared ();
extern "C" void Collection_1_Remove_m12240_gshared ();
extern "C" void Collection_1_RemoveAt_m12241_gshared ();
extern "C" void Collection_1_RemoveItem_m12242_gshared ();
extern "C" void Collection_1_get_Count_m12243_gshared ();
extern "C" void Collection_1_get_Item_m12244_gshared ();
extern "C" void Collection_1_set_Item_m12245_gshared ();
extern "C" void Collection_1_SetItem_m12246_gshared ();
extern "C" void Collection_1_IsValidItem_m12247_gshared ();
extern "C" void Collection_1_ConvertItem_m12248_gshared ();
extern "C" void Collection_1_CheckWritable_m12249_gshared ();
extern "C" void Collection_1_IsSynchronized_m12250_gshared ();
extern "C" void Collection_1_IsFixedSize_m12251_gshared ();
extern "C" void EqualityComparer_1__ctor_m12252_gshared ();
extern "C" void EqualityComparer_1__cctor_m12253_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m12254_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m12255_gshared ();
extern "C" void EqualityComparer_1_get_Default_m12256_gshared ();
extern "C" void DefaultComparer__ctor_m12257_gshared ();
extern "C" void DefaultComparer_GetHashCode_m12258_gshared ();
extern "C" void DefaultComparer_Equals_m12259_gshared ();
void* RuntimeInvoker_Boolean_t72_RaycastResult_t137_RaycastResult_t137 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m12260_gshared ();
extern "C" void Predicate_1_Invoke_m12261_gshared ();
extern "C" void Predicate_1_BeginInvoke_m12262_gshared ();
void* RuntimeInvoker_Object_t_RaycastResult_t137_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m12263_gshared ();
extern "C" void Comparer_1__ctor_m12264_gshared ();
extern "C" void Comparer_1__cctor_m12265_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m12266_gshared ();
extern "C" void Comparer_1_get_Default_m12267_gshared ();
extern "C" void DefaultComparer__ctor_m12268_gshared ();
extern "C" void DefaultComparer_Compare_m12269_gshared ();
extern "C" void Dictionary_2__ctor_m12703_gshared ();
extern "C" void Dictionary_2__ctor_m12705_gshared ();
extern "C" void Dictionary_2__ctor_m12707_gshared ();
extern "C" void Dictionary_2__ctor_m12709_gshared ();
extern "C" void Dictionary_2__ctor_m12711_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m12713_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m12715_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m12717_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m12719_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m12721_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m12723_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m12725_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12727_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12729_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12731_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12733_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12735_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12737_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12739_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m12741_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12743_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12745_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12747_gshared ();
extern "C" void Dictionary_2_get_Count_m12749_gshared ();
extern "C" void Dictionary_2_get_Item_m12751_gshared ();
extern "C" void Dictionary_2_set_Item_m12753_gshared ();
extern "C" void Dictionary_2_Init_m12755_gshared ();
extern "C" void Dictionary_2_InitArrays_m12757_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m12759_gshared ();
extern "C" void Dictionary_2_make_pair_m12761_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m12763_gshared ();
void* RuntimeInvoker_Int32_t54_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m12765_gshared ();
void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m12767_gshared ();
extern "C" void Dictionary_2_Resize_m12769_gshared ();
extern "C" void Dictionary_2_Add_m12771_gshared ();
extern "C" void Dictionary_2_Clear_m12773_gshared ();
extern "C" void Dictionary_2_ContainsKey_m12775_gshared ();
extern "C" void Dictionary_2_ContainsValue_m12777_gshared ();
extern "C" void Dictionary_2_GetObjectData_m12779_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m12781_gshared ();
extern "C" void Dictionary_2_Remove_m12783_gshared ();
extern "C" void Dictionary_2_TryGetValue_m12785_gshared ();
void* RuntimeInvoker_Boolean_t72_Int32_t54_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m12787_gshared ();
extern "C" void Dictionary_2_ToTKey_m12790_gshared ();
extern "C" void Dictionary_2_ToTValue_m12792_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m12794_gshared ();
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m12797_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m12798_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12799_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m12800_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m12801_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m12802_gshared ();
extern "C" void KeyValuePair_2__ctor_m12803_gshared ();
extern "C" void KeyValuePair_2_set_Key_m12805_gshared ();
extern "C" void KeyValuePair_2_set_Value_m12807_gshared ();
extern "C" void InternalEnumerator_1__ctor_m12809_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12810_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m12811_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m12812_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m12813_gshared ();
void* RuntimeInvoker_Link_t1459 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m12814_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m12815_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m12816_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m12817_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m12818_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m12819_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m12820_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m12821_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m12822_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m12823_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m12824_gshared ();
extern "C" void KeyCollection_CopyTo_m12825_gshared ();
extern "C" void KeyCollection_GetEnumerator_m12826_gshared ();
void* RuntimeInvoker_Enumerator_t2540 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m12827_gshared ();
extern "C" void Enumerator__ctor_m12828_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m12829_gshared ();
extern "C" void Enumerator_Dispose_m12830_gshared ();
extern "C" void Enumerator_MoveNext_m12831_gshared ();
extern "C" void Enumerator_get_Current_m12832_gshared ();
extern "C" void Enumerator__ctor_m12833_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared ();
extern "C" void Enumerator_get_CurrentKey_m12840_gshared ();
extern "C" void Enumerator_get_CurrentValue_m12841_gshared ();
extern "C" void Enumerator_VerifyState_m12842_gshared ();
extern "C" void Enumerator_VerifyCurrent_m12843_gshared ();
extern "C" void Enumerator_Dispose_m12844_gshared ();
extern "C" void Transform_1__ctor_m12845_gshared ();
extern "C" void Transform_1_Invoke_m12846_gshared ();
extern "C" void Transform_1_BeginInvoke_m12847_gshared ();
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m12848_gshared ();
extern "C" void ValueCollection__ctor_m12849_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m12850_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m12851_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m12852_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m12853_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m12854_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m12855_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m12856_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m12857_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m12858_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m12859_gshared ();
extern "C" void ValueCollection_CopyTo_m12860_gshared ();
extern "C" void ValueCollection_get_Count_m12862_gshared ();
extern "C" void Enumerator__ctor_m12863_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m12864_gshared ();
extern "C" void Enumerator_Dispose_m12865_gshared ();
extern "C" void Transform_1__ctor_m12868_gshared ();
extern "C" void Transform_1_Invoke_m12869_gshared ();
extern "C" void Transform_1_BeginInvoke_m12870_gshared ();
extern "C" void Transform_1_EndInvoke_m12871_gshared ();
extern "C" void Transform_1__ctor_m12872_gshared ();
extern "C" void Transform_1_Invoke_m12873_gshared ();
extern "C" void Transform_1_BeginInvoke_m12874_gshared ();
extern "C" void Transform_1_EndInvoke_m12875_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m12876_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12877_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m12878_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m12879_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m12880_gshared ();
extern "C" void Transform_1__ctor_m12881_gshared ();
extern "C" void Transform_1_Invoke_m12882_gshared ();
extern "C" void Transform_1_BeginInvoke_m12883_gshared ();
extern "C" void Transform_1_EndInvoke_m12884_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2536_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m12885_gshared ();
extern "C" void ShimEnumerator_MoveNext_m12886_gshared ();
extern "C" void ShimEnumerator_get_Entry_m12887_gshared ();
extern "C" void ShimEnumerator_get_Key_m12888_gshared ();
extern "C" void ShimEnumerator_get_Value_m12889_gshared ();
extern "C" void ShimEnumerator_get_Current_m12890_gshared ();
extern "C" void EqualityComparer_1__ctor_m12891_gshared ();
extern "C" void EqualityComparer_1__cctor_m12892_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m12893_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m12894_gshared ();
extern "C" void EqualityComparer_1_get_Default_m12895_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m12896_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m12897_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m12898_gshared ();
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m12899_gshared ();
extern "C" void DefaultComparer_GetHashCode_m12900_gshared ();
extern "C" void DefaultComparer_Equals_m12901_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13041_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13042_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13043_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13044_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13045_gshared ();
void* RuntimeInvoker_RaycastHit2D_t349 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_Invoke_m13046_gshared ();
void* RuntimeInvoker_Int32_t54_RaycastHit_t323_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_BeginInvoke_m13047_gshared ();
void* RuntimeInvoker_Object_t_RaycastHit_t323_RaycastHit_t323_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m13048_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13049_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13050_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13051_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13052_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13053_gshared ();
void* RuntimeInvoker_RaycastHit_t323 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_RemoveListener_m13054_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m13055_gshared ();
extern "C" void UnityAction_1_Invoke_m13056_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m13057_gshared ();
void* RuntimeInvoker_Object_t_Color_t163_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m13058_gshared ();
extern "C" void InvokableCall_1__ctor_m13059_gshared ();
extern "C" void InvokableCall_1__ctor_m13060_gshared ();
extern "C" void InvokableCall_1_Invoke_m13061_gshared ();
extern "C" void InvokableCall_1_Find_m13062_gshared ();
extern "C" void Dictionary_2__ctor_m13094_gshared ();
extern "C" void Dictionary_2__ctor_m13095_gshared ();
extern "C" void Dictionary_2__ctor_m13096_gshared ();
extern "C" void Dictionary_2__ctor_m13098_gshared ();
extern "C" void Dictionary_2__ctor_m13099_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m13102_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m13103_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m13104_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m13105_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m13106_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m13114_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117_gshared ();
extern "C" void Dictionary_2_get_Count_m13118_gshared ();
extern "C" void Dictionary_2_get_Item_m13119_gshared ();
extern "C" void Dictionary_2_set_Item_m13120_gshared ();
extern "C" void Dictionary_2_Init_m13121_gshared ();
extern "C" void Dictionary_2_InitArrays_m13122_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m13123_gshared ();
extern "C" void Dictionary_2_make_pair_m13124_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2567_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m13125_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m13126_gshared ();
void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m13127_gshared ();
extern "C" void Dictionary_2_Resize_m13128_gshared ();
extern "C" void Dictionary_2_Add_m13129_gshared ();
extern "C" void Dictionary_2_Clear_m13130_gshared ();
extern "C" void Dictionary_2_ContainsKey_m13131_gshared ();
extern "C" void Dictionary_2_ContainsValue_m13132_gshared ();
extern "C" void Dictionary_2_GetObjectData_m13133_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m13134_gshared ();
extern "C" void Dictionary_2_Remove_m13135_gshared ();
extern "C" void Dictionary_2_TryGetValue_m13136_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m13137_gshared ();
extern "C" void Dictionary_2_get_Values_m13138_gshared ();
extern "C" void Dictionary_2_ToTKey_m13139_gshared ();
extern "C" void Dictionary_2_ToTValue_m13140_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m13141_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m13142_gshared ();
void* RuntimeInvoker_Enumerator_t2571 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m13143_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m13144_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13145_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13146_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13147_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13148_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2567 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m13149_gshared ();
extern "C" void KeyValuePair_2_get_Key_m13150_gshared ();
extern "C" void KeyValuePair_2_set_Key_m13151_gshared ();
extern "C" void KeyValuePair_2_get_Value_m13152_gshared ();
extern "C" void KeyValuePair_2_set_Value_m13153_gshared ();
extern "C" void KeyValuePair_2_ToString_m13154_gshared ();
extern "C" void KeyCollection__ctor_m13155_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13156_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13157_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13158_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13159_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13160_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m13161_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13162_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13163_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13164_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m13165_gshared ();
extern "C" void KeyCollection_CopyTo_m13166_gshared ();
extern "C" void KeyCollection_GetEnumerator_m13167_gshared ();
void* RuntimeInvoker_Enumerator_t2570 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m13168_gshared ();
extern "C" void Enumerator__ctor_m13169_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13170_gshared ();
extern "C" void Enumerator_Dispose_m13171_gshared ();
extern "C" void Enumerator_MoveNext_m13172_gshared ();
extern "C" void Enumerator_get_Current_m13173_gshared ();
extern "C" void Enumerator__ctor_m13174_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13175_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13176_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13177_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13178_gshared ();
extern "C" void Enumerator_MoveNext_m13179_gshared ();
extern "C" void Enumerator_get_Current_m13180_gshared ();
extern "C" void Enumerator_get_CurrentKey_m13181_gshared ();
extern "C" void Enumerator_get_CurrentValue_m13182_gshared ();
extern "C" void Enumerator_VerifyState_m13183_gshared ();
extern "C" void Enumerator_VerifyCurrent_m13184_gshared ();
extern "C" void Enumerator_Dispose_m13185_gshared ();
extern "C" void Transform_1__ctor_m13186_gshared ();
extern "C" void Transform_1_Invoke_m13187_gshared ();
extern "C" void Transform_1_BeginInvoke_m13188_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m13189_gshared ();
extern "C" void ValueCollection__ctor_m13190_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13191_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13192_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13193_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13194_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13195_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m13196_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13197_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13198_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13199_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m13200_gshared ();
extern "C" void ValueCollection_CopyTo_m13201_gshared ();
extern "C" void ValueCollection_GetEnumerator_m13202_gshared ();
void* RuntimeInvoker_Enumerator_t2574 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m13203_gshared ();
extern "C" void Enumerator__ctor_m13204_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13205_gshared ();
extern "C" void Enumerator_Dispose_m13206_gshared ();
extern "C" void Enumerator_MoveNext_m13207_gshared ();
extern "C" void Enumerator_get_Current_m13208_gshared ();
extern "C" void Transform_1__ctor_m13209_gshared ();
extern "C" void Transform_1_Invoke_m13210_gshared ();
extern "C" void Transform_1_BeginInvoke_m13211_gshared ();
extern "C" void Transform_1_EndInvoke_m13212_gshared ();
extern "C" void Transform_1__ctor_m13213_gshared ();
extern "C" void Transform_1_Invoke_m13214_gshared ();
extern "C" void Transform_1_BeginInvoke_m13215_gshared ();
extern "C" void Transform_1_EndInvoke_m13216_gshared ();
extern "C" void Transform_1__ctor_m13217_gshared ();
extern "C" void Transform_1_Invoke_m13218_gshared ();
extern "C" void Transform_1_BeginInvoke_m13219_gshared ();
extern "C" void Transform_1_EndInvoke_m13220_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2567_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m13221_gshared ();
extern "C" void ShimEnumerator_MoveNext_m13222_gshared ();
extern "C" void ShimEnumerator_get_Entry_m13223_gshared ();
extern "C" void ShimEnumerator_get_Key_m13224_gshared ();
extern "C" void ShimEnumerator_get_Value_m13225_gshared ();
extern "C" void ShimEnumerator_get_Current_m13226_gshared ();
extern "C" void InternalEnumerator_1__ctor_m13475_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13476_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m13477_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m13478_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m13479_gshared ();
extern "C" void Transform_1__ctor_m13540_gshared ();
extern "C" void Transform_1_Invoke_m13541_gshared ();
extern "C" void Transform_1_BeginInvoke_m13542_gshared ();
extern "C" void Transform_1_EndInvoke_m13543_gshared ();
extern "C" void Transform_1__ctor_m13544_gshared ();
extern "C" void Transform_1_Invoke_m13545_gshared ();
extern "C" void Transform_1_BeginInvoke_m13546_gshared ();
extern "C" void Transform_1_EndInvoke_m13547_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__cctor_m13718_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13719_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13720_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m13722_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m13723_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m13724_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m13725_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m13726_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13727_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m13728_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m13729_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m13730_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m13731_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m13732_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m13733_gshared ();
extern "C" void List_1_Add_m13734_gshared ();
void* RuntimeInvoker_Void_t71_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_GrowIfNeeded_m13735_gshared ();
extern "C" void List_1_AddCollection_m13736_gshared ();
extern "C" void List_1_AddEnumerable_m13737_gshared ();
extern "C" void List_1_AddRange_m13738_gshared ();
extern "C" void List_1_AsReadOnly_m13739_gshared ();
extern "C" void List_1_Clear_m13740_gshared ();
extern "C" void List_1_Contains_m13741_gshared ();
void* RuntimeInvoker_Boolean_t72_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CopyTo_m13742_gshared ();
extern "C" void List_1_Find_m13743_gshared ();
void* RuntimeInvoker_UIVertex_t225_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m13744_gshared ();
extern "C" void List_1_GetIndex_m13745_gshared ();
extern "C" void List_1_GetEnumerator_m13746_gshared ();
void* RuntimeInvoker_Enumerator_t2611 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m13747_gshared ();
void* RuntimeInvoker_Int32_t54_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_Shift_m13748_gshared ();
extern "C" void List_1_CheckIndex_m13749_gshared ();
extern "C" void List_1_Insert_m13750_gshared ();
void* RuntimeInvoker_Void_t71_Int32_t54_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckCollection_m13751_gshared ();
extern "C" void List_1_Remove_m13752_gshared ();
extern "C" void List_1_RemoveAll_m13753_gshared ();
extern "C" void List_1_RemoveAt_m13754_gshared ();
extern "C" void List_1_Reverse_m13755_gshared ();
extern "C" void List_1_Sort_m13756_gshared ();
extern "C" void List_1_Sort_m13757_gshared ();
extern "C" void List_1_TrimExcess_m13758_gshared ();
extern "C" void List_1_get_Count_m13759_gshared ();
extern "C" void List_1_get_Item_m13760_gshared ();
void* RuntimeInvoker_UIVertex_t225_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_set_Item_m13761_gshared ();
extern "C" void Enumerator__ctor_m13697_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m13698_gshared ();
extern "C" void Enumerator_Dispose_m13699_gshared ();
extern "C" void Enumerator_VerifyState_m13700_gshared ();
extern "C" void Enumerator_MoveNext_m13701_gshared ();
extern "C" void Enumerator_get_Current_m13702_gshared ();
void* RuntimeInvoker_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void ReadOnlyCollection_1__ctor_m13663_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13664_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13665_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13666_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13667_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13668_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13669_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13670_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13671_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13672_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13673_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m13674_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m13675_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m13676_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m13677_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m13678_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m13679_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m13680_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m13681_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m13682_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m13683_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m13684_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m13685_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m13686_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m13687_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m13688_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m13689_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m13690_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m13691_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m13692_gshared ();
extern "C" void Collection_1__ctor_m13765_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13766_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13767_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m13768_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m13769_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m13770_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m13771_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m13772_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m13773_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m13774_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m13775_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m13776_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m13777_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m13778_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m13779_gshared ();
extern "C" void Collection_1_Add_m13780_gshared ();
extern "C" void Collection_1_Clear_m13781_gshared ();
extern "C" void Collection_1_ClearItems_m13782_gshared ();
extern "C" void Collection_1_Contains_m13783_gshared ();
extern "C" void Collection_1_CopyTo_m13784_gshared ();
extern "C" void Collection_1_GetEnumerator_m13785_gshared ();
extern "C" void Collection_1_IndexOf_m13786_gshared ();
extern "C" void Collection_1_Insert_m13787_gshared ();
extern "C" void Collection_1_InsertItem_m13788_gshared ();
extern "C" void Collection_1_Remove_m13789_gshared ();
extern "C" void Collection_1_RemoveAt_m13790_gshared ();
extern "C" void Collection_1_RemoveItem_m13791_gshared ();
extern "C" void Collection_1_get_Count_m13792_gshared ();
extern "C" void Collection_1_get_Item_m13793_gshared ();
extern "C" void Collection_1_set_Item_m13794_gshared ();
extern "C" void Collection_1_SetItem_m13795_gshared ();
extern "C" void Collection_1_IsValidItem_m13796_gshared ();
extern "C" void Collection_1_ConvertItem_m13797_gshared ();
extern "C" void Collection_1_CheckWritable_m13798_gshared ();
extern "C" void Collection_1_IsSynchronized_m13799_gshared ();
extern "C" void Collection_1_IsFixedSize_m13800_gshared ();
extern "C" void EqualityComparer_1__ctor_m13801_gshared ();
extern "C" void EqualityComparer_1__cctor_m13802_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m13803_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m13804_gshared ();
extern "C" void EqualityComparer_1_get_Default_m13805_gshared ();
extern "C" void DefaultComparer__ctor_m13806_gshared ();
extern "C" void DefaultComparer_GetHashCode_m13807_gshared ();
extern "C" void DefaultComparer_Equals_m13808_gshared ();
void* RuntimeInvoker_Boolean_t72_UIVertex_t225_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m13693_gshared ();
extern "C" void Predicate_1_Invoke_m13694_gshared ();
extern "C" void Predicate_1_BeginInvoke_m13695_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m13696_gshared ();
extern "C" void Comparer_1__ctor_m13809_gshared ();
extern "C" void Comparer_1__cctor_m13810_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m13811_gshared ();
extern "C" void Comparer_1_get_Default_m13812_gshared ();
extern "C" void DefaultComparer__ctor_m13813_gshared ();
extern "C" void DefaultComparer_Compare_m13814_gshared ();
void* RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m13703_gshared ();
extern "C" void Comparison_1_Invoke_m13704_gshared ();
extern "C" void Comparison_1_BeginInvoke_m13705_gshared ();
void* RuntimeInvoker_Object_t_UIVertex_t225_UIVertex_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m13706_gshared ();
extern "C" void TweenRunner_1_Start_m13815_gshared ();
void* RuntimeInvoker_Object_t_ColorTween_t162 (const MethodInfo* method, void* obj, void** args);
extern "C" void U3CStartU3Ec__Iterator0__ctor_m13816_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13817_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13818_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m13819_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m13820_gshared ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m13821_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14270_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14272_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14273_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14274_gshared ();
void* RuntimeInvoker_Vector2_t53 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14280_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14281_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14282_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14283_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14284_gshared ();
void* RuntimeInvoker_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m14285_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14287_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14288_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14289_gshared ();
void* RuntimeInvoker_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_GetDelegate_m14297_gshared ();
extern "C" void UnityAction_1_Invoke_m14298_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m14299_gshared ();
void* RuntimeInvoker_Object_t_Single_t85_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m14300_gshared ();
extern "C" void InvokableCall_1__ctor_m14301_gshared ();
extern "C" void InvokableCall_1__ctor_m14302_gshared ();
extern "C" void InvokableCall_1_Invoke_m14303_gshared ();
extern "C" void InvokableCall_1_Find_m14304_gshared ();
extern "C" void InternalEnumerator_1__ctor_m14305_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m14307_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m14308_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m14309_gshared ();
void* RuntimeInvoker_Vector3_t138 (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityEvent_1_AddListener_m14310_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m14311_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m14312_gshared ();
extern "C" void UnityAction_1__ctor_m14313_gshared ();
extern "C" void UnityAction_1_Invoke_m14314_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m14315_gshared ();
void* RuntimeInvoker_Object_t_Vector2_t53_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m14316_gshared ();
extern "C" void InvokableCall_1__ctor_m14317_gshared ();
extern "C" void InvokableCall_1__ctor_m14318_gshared ();
extern "C" void InvokableCall_1_Invoke_m14319_gshared ();
extern "C" void InvokableCall_1_Find_m14320_gshared ();
extern "C" void UnityEvent_1_AddListener_m14599_gshared ();
extern "C" void UnityEvent_1_RemoveListener_m14601_gshared ();
extern "C" void UnityEvent_1_GetDelegate_m14605_gshared ();
extern "C" void UnityAction_1__ctor_m14607_gshared ();
extern "C" void UnityAction_1_Invoke_m14608_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m14609_gshared ();
void* RuntimeInvoker_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m14610_gshared ();
extern "C" void InvokableCall_1__ctor_m14611_gshared ();
extern "C" void InvokableCall_1__ctor_m14612_gshared ();
extern "C" void InvokableCall_1_Invoke_m14613_gshared ();
extern "C" void InvokableCall_1_Find_m14614_gshared ();
extern "C" void Func_2_Invoke_m14816_gshared ();
void* RuntimeInvoker_Single_t85_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Func_2_BeginInvoke_m14818_gshared ();
extern "C" void Func_2_EndInvoke_m14820_gshared ();
extern "C" void Action_1__ctor_m14863_gshared ();
extern "C" void Action_1_Invoke_m14865_gshared ();
extern "C" void Action_1_BeginInvoke_m14867_gshared ();
extern "C" void Action_1_EndInvoke_m14869_gshared ();
extern "C" void InternalEnumerator_1__ctor_m15005_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15006_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15007_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15008_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15009_gshared ();
void* RuntimeInvoker_GcAchievementData_t650 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15015_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15016_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15017_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15018_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15019_gshared ();
void* RuntimeInvoker_GcScoreData_t651 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15518_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15519_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15520_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15521_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15522_gshared ();
void* RuntimeInvoker_Byte_t367 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15716_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15717_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15718_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15719_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15720_gshared ();
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15813_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15814_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15815_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15816_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15817_gshared ();
void* RuntimeInvoker_Single_t85 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m15818_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15819_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m15820_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m15821_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m15822_gshared ();
void* RuntimeInvoker_Keyframe_t580 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1__ctor_m15823_gshared ();
extern "C" void List_1__cctor_m15824_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15825_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15826_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15827_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15828_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15829_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15830_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15831_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15832_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15833_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15834_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15835_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15836_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15837_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15838_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15839_gshared ();
extern "C" void List_1_Add_m15840_gshared ();
extern "C" void List_1_GrowIfNeeded_m15841_gshared ();
extern "C" void List_1_AddCollection_m15842_gshared ();
extern "C" void List_1_AddEnumerable_m15843_gshared ();
extern "C" void List_1_AddRange_m15844_gshared ();
extern "C" void List_1_AsReadOnly_m15845_gshared ();
extern "C" void List_1_Clear_m15846_gshared ();
extern "C" void List_1_Contains_m15847_gshared ();
extern "C" void List_1_CopyTo_m15848_gshared ();
extern "C" void List_1_Find_m15849_gshared ();
void* RuntimeInvoker_UICharInfo_t377_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m15850_gshared ();
extern "C" void List_1_GetIndex_m15851_gshared ();
extern "C" void List_1_GetEnumerator_m15852_gshared ();
void* RuntimeInvoker_Enumerator_t2759 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15853_gshared ();
extern "C" void List_1_Shift_m15854_gshared ();
extern "C" void List_1_CheckIndex_m15855_gshared ();
extern "C" void List_1_Insert_m15856_gshared ();
extern "C" void List_1_CheckCollection_m15857_gshared ();
extern "C" void List_1_Remove_m15858_gshared ();
extern "C" void List_1_RemoveAll_m15859_gshared ();
extern "C" void List_1_RemoveAt_m15860_gshared ();
extern "C" void List_1_Reverse_m15861_gshared ();
extern "C" void List_1_Sort_m15862_gshared ();
extern "C" void List_1_Sort_m15863_gshared ();
extern "C" void List_1_ToArray_m15864_gshared ();
extern "C" void List_1_TrimExcess_m15865_gshared ();
extern "C" void List_1_get_Capacity_m15866_gshared ();
extern "C" void List_1_set_Capacity_m15867_gshared ();
extern "C" void List_1_get_Count_m15868_gshared ();
extern "C" void List_1_get_Item_m15869_gshared ();
extern "C" void List_1_set_Item_m15870_gshared ();
extern "C" void Enumerator__ctor_m15871_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m15872_gshared ();
extern "C" void Enumerator_Dispose_m15873_gshared ();
extern "C" void Enumerator_VerifyState_m15874_gshared ();
extern "C" void Enumerator_MoveNext_m15875_gshared ();
extern "C" void Enumerator_get_Current_m15876_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m15877_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15878_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15879_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15880_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15881_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15882_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15883_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15884_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15885_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15886_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15887_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m15888_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15889_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m15890_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15891_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15892_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15893_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15894_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15895_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15896_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15897_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15898_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m15899_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15900_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m15901_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m15902_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m15903_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m15904_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m15905_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m15906_gshared ();
extern "C" void Collection_1__ctor_m15907_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15908_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15909_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m15910_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m15911_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m15912_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m15913_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m15914_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m15915_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m15916_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m15917_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m15918_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m15919_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m15920_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m15921_gshared ();
extern "C" void Collection_1_Add_m15922_gshared ();
extern "C" void Collection_1_Clear_m15923_gshared ();
extern "C" void Collection_1_ClearItems_m15924_gshared ();
extern "C" void Collection_1_Contains_m15925_gshared ();
extern "C" void Collection_1_CopyTo_m15926_gshared ();
extern "C" void Collection_1_GetEnumerator_m15927_gshared ();
extern "C" void Collection_1_IndexOf_m15928_gshared ();
extern "C" void Collection_1_Insert_m15929_gshared ();
extern "C" void Collection_1_InsertItem_m15930_gshared ();
extern "C" void Collection_1_Remove_m15931_gshared ();
extern "C" void Collection_1_RemoveAt_m15932_gshared ();
extern "C" void Collection_1_RemoveItem_m15933_gshared ();
extern "C" void Collection_1_get_Count_m15934_gshared ();
extern "C" void Collection_1_get_Item_m15935_gshared ();
extern "C" void Collection_1_set_Item_m15936_gshared ();
extern "C" void Collection_1_SetItem_m15937_gshared ();
extern "C" void Collection_1_IsValidItem_m15938_gshared ();
extern "C" void Collection_1_ConvertItem_m15939_gshared ();
extern "C" void Collection_1_CheckWritable_m15940_gshared ();
extern "C" void Collection_1_IsSynchronized_m15941_gshared ();
extern "C" void Collection_1_IsFixedSize_m15942_gshared ();
extern "C" void EqualityComparer_1__ctor_m15943_gshared ();
extern "C" void EqualityComparer_1__cctor_m15944_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15945_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15946_gshared ();
extern "C" void EqualityComparer_1_get_Default_m15947_gshared ();
extern "C" void DefaultComparer__ctor_m15948_gshared ();
extern "C" void DefaultComparer_GetHashCode_m15949_gshared ();
extern "C" void DefaultComparer_Equals_m15950_gshared ();
void* RuntimeInvoker_Boolean_t72_UICharInfo_t377_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m15951_gshared ();
extern "C" void Predicate_1_Invoke_m15952_gshared ();
extern "C" void Predicate_1_BeginInvoke_m15953_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t377_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m15954_gshared ();
extern "C" void Comparer_1__ctor_m15955_gshared ();
extern "C" void Comparer_1__cctor_m15956_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m15957_gshared ();
extern "C" void Comparer_1_get_Default_m15958_gshared ();
extern "C" void DefaultComparer__ctor_m15959_gshared ();
extern "C" void DefaultComparer_Compare_m15960_gshared ();
void* RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m15961_gshared ();
extern "C" void Comparison_1_Invoke_m15962_gshared ();
extern "C" void Comparison_1_BeginInvoke_m15963_gshared ();
void* RuntimeInvoker_Object_t_UICharInfo_t377_UICharInfo_t377_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m15964_gshared ();
extern "C" void List_1__ctor_m15965_gshared ();
extern "C" void List_1__cctor_m15966_gshared ();
extern "C" void List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15967_gshared ();
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15968_gshared ();
extern "C" void List_1_System_Collections_IEnumerable_GetEnumerator_m15969_gshared ();
extern "C" void List_1_System_Collections_IList_Add_m15970_gshared ();
extern "C" void List_1_System_Collections_IList_Contains_m15971_gshared ();
extern "C" void List_1_System_Collections_IList_IndexOf_m15972_gshared ();
extern "C" void List_1_System_Collections_IList_Insert_m15973_gshared ();
extern "C" void List_1_System_Collections_IList_Remove_m15974_gshared ();
extern "C" void List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15975_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_IsSynchronized_m15976_gshared ();
extern "C" void List_1_System_Collections_ICollection_get_SyncRoot_m15977_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsFixedSize_m15978_gshared ();
extern "C" void List_1_System_Collections_IList_get_IsReadOnly_m15979_gshared ();
extern "C" void List_1_System_Collections_IList_get_Item_m15980_gshared ();
extern "C" void List_1_System_Collections_IList_set_Item_m15981_gshared ();
extern "C" void List_1_Add_m15982_gshared ();
extern "C" void List_1_GrowIfNeeded_m15983_gshared ();
extern "C" void List_1_AddCollection_m15984_gshared ();
extern "C" void List_1_AddEnumerable_m15985_gshared ();
extern "C" void List_1_AddRange_m15986_gshared ();
extern "C" void List_1_AsReadOnly_m15987_gshared ();
extern "C" void List_1_Clear_m15988_gshared ();
extern "C" void List_1_Contains_m15989_gshared ();
extern "C" void List_1_CopyTo_m15990_gshared ();
extern "C" void List_1_Find_m15991_gshared ();
void* RuntimeInvoker_UILineInfo_t375_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_CheckMatch_m15992_gshared ();
extern "C" void List_1_GetIndex_m15993_gshared ();
extern "C" void List_1_GetEnumerator_m15994_gshared ();
void* RuntimeInvoker_Enumerator_t2768 (const MethodInfo* method, void* obj, void** args);
extern "C" void List_1_IndexOf_m15995_gshared ();
extern "C" void List_1_Shift_m15996_gshared ();
extern "C" void List_1_CheckIndex_m15997_gshared ();
extern "C" void List_1_Insert_m15998_gshared ();
extern "C" void List_1_CheckCollection_m15999_gshared ();
extern "C" void List_1_Remove_m16000_gshared ();
extern "C" void List_1_RemoveAll_m16001_gshared ();
extern "C" void List_1_RemoveAt_m16002_gshared ();
extern "C" void List_1_Reverse_m16003_gshared ();
extern "C" void List_1_Sort_m16004_gshared ();
extern "C" void List_1_Sort_m16005_gshared ();
extern "C" void List_1_ToArray_m16006_gshared ();
extern "C" void List_1_TrimExcess_m16007_gshared ();
extern "C" void List_1_get_Capacity_m16008_gshared ();
extern "C" void List_1_set_Capacity_m16009_gshared ();
extern "C" void List_1_get_Count_m16010_gshared ();
extern "C" void List_1_get_Item_m16011_gshared ();
extern "C" void List_1_set_Item_m16012_gshared ();
extern "C" void Enumerator__ctor_m16013_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16014_gshared ();
extern "C" void Enumerator_Dispose_m16015_gshared ();
extern "C" void Enumerator_VerifyState_m16016_gshared ();
extern "C" void Enumerator_MoveNext_m16017_gshared ();
extern "C" void Enumerator_get_Current_m16018_gshared ();
extern "C" void ReadOnlyCollection_1__ctor_m16019_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16020_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16021_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16022_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16023_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16024_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16025_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16026_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16027_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16028_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16029_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Add_m16030_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m16031_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Contains_m16032_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16033_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m16034_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m16035_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16036_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16037_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16038_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16039_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16040_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_get_Item_m16041_gshared ();
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m16042_gshared ();
extern "C" void ReadOnlyCollection_1_Contains_m16043_gshared ();
extern "C" void ReadOnlyCollection_1_CopyTo_m16044_gshared ();
extern "C" void ReadOnlyCollection_1_GetEnumerator_m16045_gshared ();
extern "C" void ReadOnlyCollection_1_IndexOf_m16046_gshared ();
extern "C" void ReadOnlyCollection_1_get_Count_m16047_gshared ();
extern "C" void ReadOnlyCollection_1_get_Item_m16048_gshared ();
extern "C" void Collection_1__ctor_m16049_gshared ();
extern "C" void Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16050_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16051_gshared ();
extern "C" void Collection_1_System_Collections_IEnumerable_GetEnumerator_m16052_gshared ();
extern "C" void Collection_1_System_Collections_IList_Add_m16053_gshared ();
extern "C" void Collection_1_System_Collections_IList_Contains_m16054_gshared ();
extern "C" void Collection_1_System_Collections_IList_IndexOf_m16055_gshared ();
extern "C" void Collection_1_System_Collections_IList_Insert_m16056_gshared ();
extern "C" void Collection_1_System_Collections_IList_Remove_m16057_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_IsSynchronized_m16058_gshared ();
extern "C" void Collection_1_System_Collections_ICollection_get_SyncRoot_m16059_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsFixedSize_m16060_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_IsReadOnly_m16061_gshared ();
extern "C" void Collection_1_System_Collections_IList_get_Item_m16062_gshared ();
extern "C" void Collection_1_System_Collections_IList_set_Item_m16063_gshared ();
extern "C" void Collection_1_Add_m16064_gshared ();
extern "C" void Collection_1_Clear_m16065_gshared ();
extern "C" void Collection_1_ClearItems_m16066_gshared ();
extern "C" void Collection_1_Contains_m16067_gshared ();
extern "C" void Collection_1_CopyTo_m16068_gshared ();
extern "C" void Collection_1_GetEnumerator_m16069_gshared ();
extern "C" void Collection_1_IndexOf_m16070_gshared ();
extern "C" void Collection_1_Insert_m16071_gshared ();
extern "C" void Collection_1_InsertItem_m16072_gshared ();
extern "C" void Collection_1_Remove_m16073_gshared ();
extern "C" void Collection_1_RemoveAt_m16074_gshared ();
extern "C" void Collection_1_RemoveItem_m16075_gshared ();
extern "C" void Collection_1_get_Count_m16076_gshared ();
extern "C" void Collection_1_get_Item_m16077_gshared ();
extern "C" void Collection_1_set_Item_m16078_gshared ();
extern "C" void Collection_1_SetItem_m16079_gshared ();
extern "C" void Collection_1_IsValidItem_m16080_gshared ();
extern "C" void Collection_1_ConvertItem_m16081_gshared ();
extern "C" void Collection_1_CheckWritable_m16082_gshared ();
extern "C" void Collection_1_IsSynchronized_m16083_gshared ();
extern "C" void Collection_1_IsFixedSize_m16084_gshared ();
extern "C" void EqualityComparer_1__ctor_m16085_gshared ();
extern "C" void EqualityComparer_1__cctor_m16086_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16087_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16088_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16089_gshared ();
extern "C" void DefaultComparer__ctor_m16090_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16091_gshared ();
extern "C" void DefaultComparer_Equals_m16092_gshared ();
void* RuntimeInvoker_Boolean_t72_UILineInfo_t375_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1__ctor_m16093_gshared ();
extern "C" void Predicate_1_Invoke_m16094_gshared ();
extern "C" void Predicate_1_BeginInvoke_m16095_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t375_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Predicate_1_EndInvoke_m16096_gshared ();
extern "C" void Comparer_1__ctor_m16097_gshared ();
extern "C" void Comparer_1__cctor_m16098_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m16099_gshared ();
extern "C" void Comparer_1_get_Default_m16100_gshared ();
extern "C" void DefaultComparer__ctor_m16101_gshared ();
extern "C" void DefaultComparer_Compare_m16102_gshared ();
void* RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1__ctor_m16103_gshared ();
extern "C" void Comparison_1_Invoke_m16104_gshared ();
extern "C" void Comparison_1_BeginInvoke_m16105_gshared ();
void* RuntimeInvoker_Object_t_UILineInfo_t375_UILineInfo_t375_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparison_1_EndInvoke_m16106_gshared ();
extern "C" void Dictionary_2__ctor_m16108_gshared ();
extern "C" void Dictionary_2__ctor_m16110_gshared ();
extern "C" void Dictionary_2__ctor_m16112_gshared ();
extern "C" void Dictionary_2__ctor_m16114_gshared ();
extern "C" void Dictionary_2__ctor_m16116_gshared ();
extern "C" void Dictionary_2__ctor_m16118_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16120_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16122_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16124_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16126_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16128_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16130_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16132_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16134_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16136_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16138_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16140_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16142_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16144_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16146_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16148_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16150_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16152_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16154_gshared ();
extern "C" void Dictionary_2_get_Count_m16156_gshared ();
extern "C" void Dictionary_2_get_Item_m16158_gshared ();
void* RuntimeInvoker_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m16160_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m16162_gshared ();
extern "C" void Dictionary_2_InitArrays_m16164_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16166_gshared ();
extern "C" void Dictionary_2_make_pair_m16168_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2780_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16170_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16172_gshared ();
void* RuntimeInvoker_Int64_t753_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16174_gshared ();
extern "C" void Dictionary_2_Resize_m16176_gshared ();
extern "C" void Dictionary_2_Add_m16178_gshared ();
extern "C" void Dictionary_2_Clear_m16180_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16182_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16184_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16186_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16188_gshared ();
extern "C" void Dictionary_2_Remove_m16190_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16192_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_Int64U26_t2029 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m16194_gshared ();
extern "C" void Dictionary_2_get_Values_m16196_gshared ();
extern "C" void Dictionary_2_ToTKey_m16198_gshared ();
extern "C" void Dictionary_2_ToTValue_m16200_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16202_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16204_gshared ();
void* RuntimeInvoker_Enumerator_t2785 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16206_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16207_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16208_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16209_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16210_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16211_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2780 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m16212_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16213_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16214_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16215_gshared ();
void* RuntimeInvoker_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Value_m16216_gshared ();
extern "C" void KeyValuePair_2_ToString_m16217_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16218_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16219_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16220_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16221_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16222_gshared ();
extern "C" void KeyCollection__ctor_m16223_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16224_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16225_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16226_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16227_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16228_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16229_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16230_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16231_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16232_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16233_gshared ();
extern "C" void KeyCollection_CopyTo_m16234_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16235_gshared ();
void* RuntimeInvoker_Enumerator_t2784 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16236_gshared ();
extern "C" void Enumerator__ctor_m16237_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16238_gshared ();
extern "C" void Enumerator_Dispose_m16239_gshared ();
extern "C" void Enumerator_MoveNext_m16240_gshared ();
extern "C" void Enumerator_get_Current_m16241_gshared ();
extern "C" void Enumerator__ctor_m16242_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16243_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246_gshared ();
extern "C" void Enumerator_MoveNext_m16247_gshared ();
extern "C" void Enumerator_get_Current_m16248_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16249_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16250_gshared ();
extern "C" void Enumerator_VerifyState_m16251_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16252_gshared ();
extern "C" void Enumerator_Dispose_m16253_gshared ();
extern "C" void Transform_1__ctor_m16254_gshared ();
extern "C" void Transform_1_Invoke_m16255_gshared ();
extern "C" void Transform_1_BeginInvoke_m16256_gshared ();
void* RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16257_gshared ();
extern "C" void ValueCollection__ctor_m16258_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16259_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16260_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16261_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16262_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16263_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16264_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16265_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16266_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16267_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16268_gshared ();
extern "C" void ValueCollection_CopyTo_m16269_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16270_gshared ();
void* RuntimeInvoker_Enumerator_t2788 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m16271_gshared ();
extern "C" void Enumerator__ctor_m16272_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16273_gshared ();
extern "C" void Enumerator_Dispose_m16274_gshared ();
extern "C" void Enumerator_MoveNext_m16275_gshared ();
extern "C" void Enumerator_get_Current_m16276_gshared ();
extern "C" void Transform_1__ctor_m16277_gshared ();
extern "C" void Transform_1_Invoke_m16278_gshared ();
extern "C" void Transform_1_BeginInvoke_m16279_gshared ();
extern "C" void Transform_1_EndInvoke_m16280_gshared ();
extern "C" void Transform_1__ctor_m16281_gshared ();
extern "C" void Transform_1_Invoke_m16282_gshared ();
extern "C" void Transform_1_BeginInvoke_m16283_gshared ();
extern "C" void Transform_1_EndInvoke_m16284_gshared ();
extern "C" void Transform_1__ctor_m16285_gshared ();
extern "C" void Transform_1_Invoke_m16286_gshared ();
extern "C" void Transform_1_BeginInvoke_m16287_gshared ();
extern "C" void Transform_1_EndInvoke_m16288_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2780_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16289_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16290_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16291_gshared ();
extern "C" void ShimEnumerator_get_Key_m16292_gshared ();
extern "C" void ShimEnumerator_get_Value_m16293_gshared ();
extern "C" void ShimEnumerator_get_Current_m16294_gshared ();
extern "C" void EqualityComparer_1__ctor_m16295_gshared ();
extern "C" void EqualityComparer_1__cctor_m16296_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16297_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16298_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16299_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16300_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16301_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16302_gshared ();
void* RuntimeInvoker_Boolean_t72_Int64_t753_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m16303_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16304_gshared ();
extern "C" void DefaultComparer_Equals_m16305_gshared ();
extern "C" void Dictionary_2__ctor_m16544_gshared ();
extern "C" void Dictionary_2__ctor_m16546_gshared ();
extern "C" void Dictionary_2__ctor_m16548_gshared ();
extern "C" void Dictionary_2__ctor_m16550_gshared ();
extern "C" void Dictionary_2__ctor_m16552_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16554_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16556_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m16558_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16560_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16562_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m16564_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16566_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16568_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16570_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16574_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16576_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16578_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16580_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16582_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16584_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16586_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16588_gshared ();
extern "C" void Dictionary_2_get_Count_m16590_gshared ();
extern "C" void Dictionary_2_get_Item_m16592_gshared ();
void* RuntimeInvoker_Object_t_Int64_t753 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_set_Item_m16594_gshared ();
void* RuntimeInvoker_Void_t71_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m16596_gshared ();
extern "C" void Dictionary_2_InitArrays_m16598_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m16600_gshared ();
extern "C" void Dictionary_2_make_pair_m16602_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2818_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m16604_gshared ();
void* RuntimeInvoker_UInt64_t756_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m16606_gshared ();
void* RuntimeInvoker_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m16608_gshared ();
extern "C" void Dictionary_2_Resize_m16610_gshared ();
extern "C" void Dictionary_2_Add_m16612_gshared ();
extern "C" void Dictionary_2_Clear_m16614_gshared ();
extern "C" void Dictionary_2_ContainsKey_m16616_gshared ();
extern "C" void Dictionary_2_ContainsValue_m16618_gshared ();
extern "C" void Dictionary_2_GetObjectData_m16620_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m16622_gshared ();
extern "C" void Dictionary_2_Remove_m16624_gshared ();
extern "C" void Dictionary_2_TryGetValue_m16626_gshared ();
void* RuntimeInvoker_Boolean_t72_Int64_t753_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m16628_gshared ();
extern "C" void Dictionary_2_get_Values_m16630_gshared ();
extern "C" void Dictionary_2_ToTKey_m16632_gshared ();
void* RuntimeInvoker_UInt64_t756_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_ToTValue_m16634_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m16636_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m16638_gshared ();
void* RuntimeInvoker_Enumerator_t2823 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m16640_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m16641_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16642_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16643_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16644_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16645_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2818 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m16646_gshared ();
extern "C" void KeyValuePair_2_get_Key_m16647_gshared ();
void* RuntimeInvoker_UInt64_t756 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_set_Key_m16648_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16649_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16650_gshared ();
extern "C" void KeyValuePair_2_ToString_m16651_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16652_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16653_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16654_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16655_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16656_gshared ();
extern "C" void KeyCollection__ctor_m16657_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16658_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16659_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16660_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16661_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16662_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m16663_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16664_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16665_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16666_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m16667_gshared ();
extern "C" void KeyCollection_CopyTo_m16668_gshared ();
extern "C" void KeyCollection_GetEnumerator_m16669_gshared ();
void* RuntimeInvoker_Enumerator_t2822 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m16670_gshared ();
extern "C" void Enumerator__ctor_m16671_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16672_gshared ();
extern "C" void Enumerator_Dispose_m16673_gshared ();
extern "C" void Enumerator_MoveNext_m16674_gshared ();
extern "C" void Enumerator_get_Current_m16675_gshared ();
extern "C" void Enumerator__ctor_m16676_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16677_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16678_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16679_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16680_gshared ();
extern "C" void Enumerator_MoveNext_m16681_gshared ();
extern "C" void Enumerator_get_Current_m16682_gshared ();
extern "C" void Enumerator_get_CurrentKey_m16683_gshared ();
extern "C" void Enumerator_get_CurrentValue_m16684_gshared ();
extern "C" void Enumerator_VerifyState_m16685_gshared ();
extern "C" void Enumerator_VerifyCurrent_m16686_gshared ();
extern "C" void Enumerator_Dispose_m16687_gshared ();
extern "C" void Transform_1__ctor_m16688_gshared ();
extern "C" void Transform_1_Invoke_m16689_gshared ();
extern "C" void Transform_1_BeginInvoke_m16690_gshared ();
void* RuntimeInvoker_Object_t_Int64_t753_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m16691_gshared ();
extern "C" void ValueCollection__ctor_m16692_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16693_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16694_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16695_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16696_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16697_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m16698_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16699_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16700_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16701_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m16702_gshared ();
extern "C" void ValueCollection_CopyTo_m16703_gshared ();
extern "C" void ValueCollection_GetEnumerator_m16704_gshared ();
void* RuntimeInvoker_Enumerator_t2826 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m16705_gshared ();
extern "C" void Enumerator__ctor_m16706_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m16707_gshared ();
extern "C" void Enumerator_Dispose_m16708_gshared ();
extern "C" void Enumerator_MoveNext_m16709_gshared ();
extern "C" void Enumerator_get_Current_m16710_gshared ();
extern "C" void Transform_1__ctor_m16711_gshared ();
extern "C" void Transform_1_Invoke_m16712_gshared ();
extern "C" void Transform_1_BeginInvoke_m16713_gshared ();
extern "C" void Transform_1_EndInvoke_m16714_gshared ();
extern "C" void Transform_1__ctor_m16715_gshared ();
extern "C" void Transform_1_Invoke_m16716_gshared ();
extern "C" void Transform_1_BeginInvoke_m16717_gshared ();
extern "C" void Transform_1_EndInvoke_m16718_gshared ();
extern "C" void Transform_1__ctor_m16719_gshared ();
extern "C" void Transform_1_Invoke_m16720_gshared ();
extern "C" void Transform_1_BeginInvoke_m16721_gshared ();
extern "C" void Transform_1_EndInvoke_m16722_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2818_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m16723_gshared ();
extern "C" void ShimEnumerator_MoveNext_m16724_gshared ();
extern "C" void ShimEnumerator_get_Entry_m16725_gshared ();
extern "C" void ShimEnumerator_get_Key_m16726_gshared ();
extern "C" void ShimEnumerator_get_Value_m16727_gshared ();
extern "C" void ShimEnumerator_get_Current_m16728_gshared ();
extern "C" void EqualityComparer_1__ctor_m16729_gshared ();
extern "C" void EqualityComparer_1__cctor_m16730_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16731_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16732_gshared ();
extern "C" void EqualityComparer_1_get_Default_m16733_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m16734_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m16735_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m16736_gshared ();
extern "C" void DefaultComparer__ctor_m16737_gshared ();
extern "C" void DefaultComparer_GetHashCode_m16738_gshared ();
extern "C" void DefaultComparer_Equals_m16739_gshared ();
extern "C" void InternalEnumerator_1__ctor_m16914_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16915_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m16916_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m16917_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m16918_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2839 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m16919_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2_get_Key_m16920_gshared ();
extern "C" void KeyValuePair_2_set_Key_m16921_gshared ();
extern "C" void KeyValuePair_2_get_Value_m16922_gshared ();
extern "C" void KeyValuePair_2_set_Value_m16923_gshared ();
extern "C" void KeyValuePair_2_ToString_m16924_gshared ();
extern "C" void Dictionary_2__ctor_m17278_gshared ();
extern "C" void Dictionary_2__ctor_m17280_gshared ();
extern "C" void Dictionary_2__ctor_m17282_gshared ();
extern "C" void Dictionary_2__ctor_m17284_gshared ();
extern "C" void Dictionary_2__ctor_m17286_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17288_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17290_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m17292_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17294_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17296_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m17298_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17300_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17302_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17304_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17306_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17308_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17310_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17312_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17314_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17316_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17318_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17320_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17322_gshared ();
extern "C" void Dictionary_2_get_Count_m17324_gshared ();
extern "C" void Dictionary_2_get_Item_m17326_gshared ();
extern "C" void Dictionary_2_set_Item_m17328_gshared ();
extern "C" void Dictionary_2_Init_m17330_gshared ();
extern "C" void Dictionary_2_InitArrays_m17332_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m17334_gshared ();
extern "C" void Dictionary_2_make_pair_m17336_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2839_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m17338_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m17340_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2593_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m17342_gshared ();
extern "C" void Dictionary_2_Resize_m17344_gshared ();
extern "C" void Dictionary_2_Add_m17346_gshared ();
extern "C" void Dictionary_2_Clear_m17348_gshared ();
extern "C" void Dictionary_2_ContainsKey_m17350_gshared ();
extern "C" void Dictionary_2_ContainsValue_m17352_gshared ();
extern "C" void Dictionary_2_GetObjectData_m17354_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m17356_gshared ();
extern "C" void Dictionary_2_Remove_m17358_gshared ();
extern "C" void Dictionary_2_TryGetValue_m17360_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_KeyValuePair_2U26_t3387 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m17362_gshared ();
extern "C" void Dictionary_2_get_Values_m17364_gshared ();
extern "C" void Dictionary_2_ToTKey_m17366_gshared ();
extern "C" void Dictionary_2_ToTValue_m17368_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m17370_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m17372_gshared ();
void* RuntimeInvoker_Enumerator_t2868 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m17374_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection__ctor_m17375_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17376_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17377_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17378_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17379_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17380_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m17381_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17382_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17383_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17384_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m17385_gshared ();
extern "C" void KeyCollection_CopyTo_m17386_gshared ();
extern "C" void KeyCollection_GetEnumerator_m17387_gshared ();
void* RuntimeInvoker_Enumerator_t2867 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m17388_gshared ();
extern "C" void Enumerator__ctor_m17389_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17390_gshared ();
extern "C" void Enumerator_Dispose_m17391_gshared ();
extern "C" void Enumerator_MoveNext_m17392_gshared ();
extern "C" void Enumerator_get_Current_m17393_gshared ();
extern "C" void Enumerator__ctor_m17394_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17395_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17396_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17397_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17398_gshared ();
extern "C" void Enumerator_MoveNext_m17399_gshared ();
extern "C" void Enumerator_get_Current_m17400_gshared ();
extern "C" void Enumerator_get_CurrentKey_m17401_gshared ();
extern "C" void Enumerator_get_CurrentValue_m17402_gshared ();
extern "C" void Enumerator_VerifyState_m17403_gshared ();
extern "C" void Enumerator_VerifyCurrent_m17404_gshared ();
extern "C" void Enumerator_Dispose_m17405_gshared ();
extern "C" void Transform_1__ctor_m17406_gshared ();
extern "C" void Transform_1_Invoke_m17407_gshared ();
extern "C" void Transform_1_BeginInvoke_m17408_gshared ();
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m17409_gshared ();
extern "C" void ValueCollection__ctor_m17410_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17411_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17412_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17413_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17414_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17415_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m17416_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17417_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17418_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17419_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m17420_gshared ();
extern "C" void ValueCollection_CopyTo_m17421_gshared ();
extern "C" void ValueCollection_GetEnumerator_m17422_gshared ();
void* RuntimeInvoker_Enumerator_t2871 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m17423_gshared ();
extern "C" void Enumerator__ctor_m17424_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m17425_gshared ();
extern "C" void Enumerator_Dispose_m17426_gshared ();
extern "C" void Enumerator_MoveNext_m17427_gshared ();
extern "C" void Enumerator_get_Current_m17428_gshared ();
extern "C" void Transform_1__ctor_m17429_gshared ();
extern "C" void Transform_1_Invoke_m17430_gshared ();
extern "C" void Transform_1_BeginInvoke_m17431_gshared ();
extern "C" void Transform_1_EndInvoke_m17432_gshared ();
extern "C" void Transform_1__ctor_m17433_gshared ();
extern "C" void Transform_1_Invoke_m17434_gshared ();
extern "C" void Transform_1_BeginInvoke_m17435_gshared ();
extern "C" void Transform_1_EndInvoke_m17436_gshared ();
extern "C" void Transform_1__ctor_m17437_gshared ();
extern "C" void Transform_1_Invoke_m17438_gshared ();
extern "C" void Transform_1_BeginInvoke_m17439_gshared ();
extern "C" void Transform_1_EndInvoke_m17440_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2839_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m17441_gshared ();
extern "C" void ShimEnumerator_MoveNext_m17442_gshared ();
extern "C" void ShimEnumerator_get_Entry_m17443_gshared ();
extern "C" void ShimEnumerator_get_Key_m17444_gshared ();
extern "C" void ShimEnumerator_get_Value_m17445_gshared ();
extern "C" void ShimEnumerator_get_Current_m17446_gshared ();
extern "C" void EqualityComparer_1__ctor_m17447_gshared ();
extern "C" void EqualityComparer_1__cctor_m17448_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17449_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17450_gshared ();
extern "C" void EqualityComparer_1_get_Default_m17451_gshared ();
extern "C" void DefaultComparer__ctor_m17452_gshared ();
extern "C" void DefaultComparer_GetHashCode_m17453_gshared ();
extern "C" void DefaultComparer_Equals_m17454_gshared ();
void* RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593_KeyValuePair_2_t2593 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17644_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17645_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17646_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17647_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17648_gshared ();
void* RuntimeInvoker_ParameterModifier_t1584 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m17649_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17650_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m17651_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m17652_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m17653_gshared ();
void* RuntimeInvoker_HitInfo_t665 (const MethodInfo* method, void* obj, void** args);
extern "C" void CachedInvokableCall_1_Invoke_m17777_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m17778_gshared ();
extern "C" void InvokableCall_1__ctor_m17779_gshared ();
extern "C" void InvokableCall_1__ctor_m17780_gshared ();
extern "C" void InvokableCall_1_Invoke_m17781_gshared ();
extern "C" void InvokableCall_1_Find_m17782_gshared ();
extern "C" void UnityAction_1__ctor_m17783_gshared ();
extern "C" void UnityAction_1_Invoke_m17784_gshared ();
extern "C" void UnityAction_1_BeginInvoke_m17785_gshared ();
void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void UnityAction_1_EndInvoke_m17786_gshared ();
extern "C" void CachedInvokableCall_1_Invoke_m17794_gshared ();
extern "C" void Dictionary_2__ctor_m17991_gshared ();
extern "C" void Dictionary_2__ctor_m17994_gshared ();
extern "C" void Dictionary_2__ctor_m17996_gshared ();
extern "C" void Dictionary_2__ctor_m17998_gshared ();
extern "C" void Dictionary_2__ctor_m18000_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18002_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18004_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m18006_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18008_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18010_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m18012_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18014_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18016_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18018_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18020_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18022_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18024_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18026_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18028_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18030_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18032_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18034_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18036_gshared ();
extern "C" void Dictionary_2_get_Count_m18038_gshared ();
extern "C" void Dictionary_2_get_Item_m18040_gshared ();
extern "C" void Dictionary_2_set_Item_m18042_gshared ();
void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_Init_m18044_gshared ();
extern "C" void Dictionary_2_InitArrays_m18046_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m18048_gshared ();
extern "C" void Dictionary_2_make_pair_m18050_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2933_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m18052_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m18054_gshared ();
void* RuntimeInvoker_Byte_t367_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_CopyTo_m18056_gshared ();
extern "C" void Dictionary_2_Resize_m18058_gshared ();
extern "C" void Dictionary_2_Add_m18060_gshared ();
extern "C" void Dictionary_2_Clear_m18062_gshared ();
extern "C" void Dictionary_2_ContainsKey_m18064_gshared ();
extern "C" void Dictionary_2_ContainsValue_m18066_gshared ();
extern "C" void Dictionary_2_GetObjectData_m18068_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m18070_gshared ();
extern "C" void Dictionary_2_Remove_m18072_gshared ();
extern "C" void Dictionary_2_TryGetValue_m18074_gshared ();
void* RuntimeInvoker_Boolean_t72_Object_t_ByteU26_t1360 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m18076_gshared ();
extern "C" void Dictionary_2_get_Values_m18078_gshared ();
extern "C" void Dictionary_2_ToTKey_m18080_gshared ();
extern "C" void Dictionary_2_ToTValue_m18082_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m18084_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m18086_gshared ();
void* RuntimeInvoker_Enumerator_t2937 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m18088_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18089_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18090_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18091_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18092_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18093_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2933 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m18094_gshared ();
extern "C" void KeyValuePair_2_get_Key_m18095_gshared ();
extern "C" void KeyValuePair_2_set_Key_m18096_gshared ();
extern "C" void KeyValuePair_2_get_Value_m18097_gshared ();
extern "C" void KeyValuePair_2_set_Value_m18098_gshared ();
extern "C" void KeyValuePair_2_ToString_m18099_gshared ();
extern "C" void KeyCollection__ctor_m18100_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18101_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18102_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18103_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18104_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18105_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18106_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18107_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18108_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18109_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m18110_gshared ();
extern "C" void KeyCollection_CopyTo_m18111_gshared ();
extern "C" void KeyCollection_GetEnumerator_m18112_gshared ();
void* RuntimeInvoker_Enumerator_t2936 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m18113_gshared ();
extern "C" void Enumerator__ctor_m18114_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18115_gshared ();
extern "C" void Enumerator_Dispose_m18116_gshared ();
extern "C" void Enumerator_MoveNext_m18117_gshared ();
extern "C" void Enumerator_get_Current_m18118_gshared ();
extern "C" void Enumerator__ctor_m18119_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18120_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123_gshared ();
extern "C" void Enumerator_MoveNext_m18124_gshared ();
extern "C" void Enumerator_get_Current_m18125_gshared ();
extern "C" void Enumerator_get_CurrentKey_m18126_gshared ();
extern "C" void Enumerator_get_CurrentValue_m18127_gshared ();
extern "C" void Enumerator_VerifyState_m18128_gshared ();
extern "C" void Enumerator_VerifyCurrent_m18129_gshared ();
extern "C" void Enumerator_Dispose_m18130_gshared ();
extern "C" void Transform_1__ctor_m18131_gshared ();
extern "C" void Transform_1_Invoke_m18132_gshared ();
extern "C" void Transform_1_BeginInvoke_m18133_gshared ();
void* RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m18134_gshared ();
extern "C" void ValueCollection__ctor_m18135_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18136_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18137_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18138_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18139_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18140_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m18141_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18142_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18143_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18144_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m18145_gshared ();
extern "C" void ValueCollection_CopyTo_m18146_gshared ();
extern "C" void ValueCollection_GetEnumerator_m18147_gshared ();
void* RuntimeInvoker_Enumerator_t2940 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m18148_gshared ();
extern "C" void Enumerator__ctor_m18149_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18150_gshared ();
extern "C" void Enumerator_Dispose_m18151_gshared ();
extern "C" void Enumerator_MoveNext_m18152_gshared ();
extern "C" void Enumerator_get_Current_m18153_gshared ();
extern "C" void Transform_1__ctor_m18154_gshared ();
extern "C" void Transform_1_Invoke_m18155_gshared ();
extern "C" void Transform_1_BeginInvoke_m18156_gshared ();
extern "C" void Transform_1_EndInvoke_m18157_gshared ();
extern "C" void Transform_1__ctor_m18158_gshared ();
extern "C" void Transform_1_Invoke_m18159_gshared ();
extern "C" void Transform_1_BeginInvoke_m18160_gshared ();
extern "C" void Transform_1_EndInvoke_m18161_gshared ();
extern "C" void Transform_1__ctor_m18162_gshared ();
extern "C" void Transform_1_Invoke_m18163_gshared ();
extern "C" void Transform_1_BeginInvoke_m18164_gshared ();
extern "C" void Transform_1_EndInvoke_m18165_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2933_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m18166_gshared ();
extern "C" void ShimEnumerator_MoveNext_m18167_gshared ();
extern "C" void ShimEnumerator_get_Entry_m18168_gshared ();
extern "C" void ShimEnumerator_get_Key_m18169_gshared ();
extern "C" void ShimEnumerator_get_Value_m18170_gshared ();
extern "C" void ShimEnumerator_get_Current_m18171_gshared ();
extern "C" void EqualityComparer_1__ctor_m18172_gshared ();
extern "C" void EqualityComparer_1__cctor_m18173_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18174_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18175_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18176_gshared ();
extern "C" void GenericEqualityComparer_1__ctor_m18177_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18178_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m18179_gshared ();
void* RuntimeInvoker_Boolean_t72_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void DefaultComparer__ctor_m18180_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18181_gshared ();
extern "C" void DefaultComparer_Equals_m18182_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18238_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18239_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18240_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18241_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18242_gshared ();
void* RuntimeInvoker_X509ChainStatus_t1045 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2__ctor_m18253_gshared ();
extern "C" void Dictionary_2__ctor_m18254_gshared ();
extern "C" void Dictionary_2__ctor_m18255_gshared ();
extern "C" void Dictionary_2__ctor_m18256_gshared ();
extern "C" void Dictionary_2__ctor_m18257_gshared ();
extern "C" void Dictionary_2__ctor_m18258_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18259_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18260_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_get_Item_m18261_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18262_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18263_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Contains_m18264_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18265_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18266_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18267_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18268_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18269_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18270_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18271_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18272_gshared ();
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18273_gshared ();
extern "C" void Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18274_gshared ();
extern "C" void Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18275_gshared ();
extern "C" void Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18276_gshared ();
extern "C" void Dictionary_2_get_Count_m18277_gshared ();
extern "C" void Dictionary_2_get_Item_m18278_gshared ();
extern "C" void Dictionary_2_set_Item_m18279_gshared ();
extern "C" void Dictionary_2_Init_m18280_gshared ();
extern "C" void Dictionary_2_InitArrays_m18281_gshared ();
extern "C" void Dictionary_2_CopyToCheck_m18282_gshared ();
extern "C" void Dictionary_2_make_pair_m18283_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_key_m18284_gshared ();
void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_pick_value_m18285_gshared ();
extern "C" void Dictionary_2_CopyTo_m18286_gshared ();
extern "C" void Dictionary_2_Resize_m18287_gshared ();
extern "C" void Dictionary_2_Add_m18288_gshared ();
extern "C" void Dictionary_2_Clear_m18289_gshared ();
extern "C" void Dictionary_2_ContainsKey_m18290_gshared ();
extern "C" void Dictionary_2_ContainsValue_m18291_gshared ();
extern "C" void Dictionary_2_GetObjectData_m18292_gshared ();
extern "C" void Dictionary_2_OnDeserialization_m18293_gshared ();
extern "C" void Dictionary_2_Remove_m18294_gshared ();
extern "C" void Dictionary_2_TryGetValue_m18295_gshared ();
void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_get_Keys_m18296_gshared ();
extern "C" void Dictionary_2_get_Values_m18297_gshared ();
extern "C" void Dictionary_2_ToTKey_m18298_gshared ();
extern "C" void Dictionary_2_ToTValue_m18299_gshared ();
extern "C" void Dictionary_2_ContainsKeyValuePair_m18300_gshared ();
extern "C" void Dictionary_2_GetEnumerator_m18301_gshared ();
void* RuntimeInvoker_Enumerator_t2960 (const MethodInfo* method, void* obj, void** args);
extern "C" void Dictionary_2_U3CCopyToU3Em__0_m18302_gshared ();
void* RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18303_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18304_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18305_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18306_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18307_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2956 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyValuePair_2__ctor_m18308_gshared ();
extern "C" void KeyValuePair_2_get_Key_m18309_gshared ();
extern "C" void KeyValuePair_2_set_Key_m18310_gshared ();
extern "C" void KeyValuePair_2_get_Value_m18311_gshared ();
extern "C" void KeyValuePair_2_set_Value_m18312_gshared ();
extern "C" void KeyValuePair_2_ToString_m18313_gshared ();
extern "C" void KeyCollection__ctor_m18314_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18315_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18316_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18317_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18318_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18319_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m18320_gshared ();
extern "C" void KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18321_gshared ();
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18322_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18323_gshared ();
extern "C" void KeyCollection_System_Collections_ICollection_get_SyncRoot_m18324_gshared ();
extern "C" void KeyCollection_CopyTo_m18325_gshared ();
extern "C" void KeyCollection_GetEnumerator_m18326_gshared ();
void* RuntimeInvoker_Enumerator_t2959 (const MethodInfo* method, void* obj, void** args);
extern "C" void KeyCollection_get_Count_m18327_gshared ();
extern "C" void Enumerator__ctor_m18328_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18329_gshared ();
extern "C" void Enumerator_Dispose_m18330_gshared ();
extern "C" void Enumerator_MoveNext_m18331_gshared ();
extern "C" void Enumerator_get_Current_m18332_gshared ();
extern "C" void Enumerator__ctor_m18333_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18334_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18335_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18336_gshared ();
extern "C" void Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18337_gshared ();
extern "C" void Enumerator_MoveNext_m18338_gshared ();
extern "C" void Enumerator_get_Current_m18339_gshared ();
extern "C" void Enumerator_get_CurrentKey_m18340_gshared ();
extern "C" void Enumerator_get_CurrentValue_m18341_gshared ();
extern "C" void Enumerator_VerifyState_m18342_gshared ();
extern "C" void Enumerator_VerifyCurrent_m18343_gshared ();
extern "C" void Enumerator_Dispose_m18344_gshared ();
extern "C" void Transform_1__ctor_m18345_gshared ();
extern "C" void Transform_1_Invoke_m18346_gshared ();
extern "C" void Transform_1_BeginInvoke_m18347_gshared ();
void* RuntimeInvoker_Object_t_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void Transform_1_EndInvoke_m18348_gshared ();
extern "C" void ValueCollection__ctor_m18349_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18350_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18351_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18352_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18353_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18354_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m18355_gshared ();
extern "C" void ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18356_gshared ();
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18357_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18358_gshared ();
extern "C" void ValueCollection_System_Collections_ICollection_get_SyncRoot_m18359_gshared ();
extern "C" void ValueCollection_CopyTo_m18360_gshared ();
extern "C" void ValueCollection_GetEnumerator_m18361_gshared ();
void* RuntimeInvoker_Enumerator_t2963 (const MethodInfo* method, void* obj, void** args);
extern "C" void ValueCollection_get_Count_m18362_gshared ();
extern "C" void Enumerator__ctor_m18363_gshared ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m18364_gshared ();
extern "C" void Enumerator_Dispose_m18365_gshared ();
extern "C" void Enumerator_MoveNext_m18366_gshared ();
extern "C" void Enumerator_get_Current_m18367_gshared ();
extern "C" void Transform_1__ctor_m18368_gshared ();
extern "C" void Transform_1_Invoke_m18369_gshared ();
extern "C" void Transform_1_BeginInvoke_m18370_gshared ();
extern "C" void Transform_1_EndInvoke_m18371_gshared ();
extern "C" void Transform_1__ctor_m18372_gshared ();
extern "C" void Transform_1_Invoke_m18373_gshared ();
extern "C" void Transform_1_BeginInvoke_m18374_gshared ();
extern "C" void Transform_1_EndInvoke_m18375_gshared ();
void* RuntimeInvoker_KeyValuePair_2_t2956_Object_t (const MethodInfo* method, void* obj, void** args);
extern "C" void ShimEnumerator__ctor_m18376_gshared ();
extern "C" void ShimEnumerator_MoveNext_m18377_gshared ();
extern "C" void ShimEnumerator_get_Entry_m18378_gshared ();
extern "C" void ShimEnumerator_get_Key_m18379_gshared ();
extern "C" void ShimEnumerator_get_Value_m18380_gshared ();
extern "C" void ShimEnumerator_get_Current_m18381_gshared ();
extern "C" void Comparer_1__ctor_m18382_gshared ();
extern "C" void Comparer_1__cctor_m18383_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m18384_gshared ();
extern "C" void Comparer_1_get_Default_m18385_gshared ();
extern "C" void GenericComparer_1__ctor_m18386_gshared ();
extern "C" void GenericComparer_1_Compare_m18387_gshared ();
extern "C" void DefaultComparer__ctor_m18388_gshared ();
extern "C" void DefaultComparer_Compare_m18389_gshared ();
extern "C" void InternalEnumerator_1__ctor_m18390_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18391_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18392_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18393_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18394_gshared ();
void* RuntimeInvoker_Mark_t1099 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18395_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18396_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18397_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18398_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18399_gshared ();
void* RuntimeInvoker_UriScheme_t1135 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18400_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18401_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18402_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18403_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18404_gshared ();
void* RuntimeInvoker_UInt32_t744 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18425_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18426_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18427_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18428_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18429_gshared ();
void* RuntimeInvoker_Int16_t448 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18430_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18431_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18432_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18433_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18434_gshared ();
void* RuntimeInvoker_SByte_t73 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18465_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18466_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18467_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18468_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18469_gshared ();
void* RuntimeInvoker_TableRange_t1392 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18495_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18496_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18497_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18498_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18499_gshared ();
void* RuntimeInvoker_Slot_t1469 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18500_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18501_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18502_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18503_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18504_gshared ();
void* RuntimeInvoker_Slot_t1476 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18573_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18574_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18575_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18576_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18577_gshared ();
void* RuntimeInvoker_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18578_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18579_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18580_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18581_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18582_gshared ();
void* RuntimeInvoker_Decimal_t755 (const MethodInfo* method, void* obj, void** args);
extern "C" void InternalEnumerator_1__ctor_m18583_gshared ();
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18584_gshared ();
extern "C" void InternalEnumerator_1_Dispose_m18585_gshared ();
extern "C" void InternalEnumerator_1_MoveNext_m18586_gshared ();
extern "C" void InternalEnumerator_1_get_Current_m18587_gshared ();
extern "C" void GenericComparer_1_Compare_m18690_gshared ();
void* RuntimeInvoker_Int32_t54_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m18691_gshared ();
extern "C" void Comparer_1__cctor_m18692_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m18693_gshared ();
extern "C" void Comparer_1_get_Default_m18694_gshared ();
extern "C" void DefaultComparer__ctor_m18695_gshared ();
extern "C" void DefaultComparer_Compare_m18696_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18697_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m18698_gshared ();
void* RuntimeInvoker_Boolean_t72_DateTime_t508_DateTime_t508 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m18699_gshared ();
extern "C" void EqualityComparer_1__cctor_m18700_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18701_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18702_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18703_gshared ();
extern "C" void DefaultComparer__ctor_m18704_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18705_gshared ();
extern "C" void DefaultComparer_Equals_m18706_gshared ();
extern "C" void GenericComparer_1_Compare_m18707_gshared ();
void* RuntimeInvoker_Int32_t54_DateTimeOffset_t768_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m18708_gshared ();
extern "C" void Comparer_1__cctor_m18709_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m18710_gshared ();
extern "C" void Comparer_1_get_Default_m18711_gshared ();
extern "C" void DefaultComparer__ctor_m18712_gshared ();
extern "C" void DefaultComparer_Compare_m18713_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18714_gshared ();
void* RuntimeInvoker_Int32_t54_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m18715_gshared ();
void* RuntimeInvoker_Boolean_t72_DateTimeOffset_t768_DateTimeOffset_t768 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m18716_gshared ();
extern "C" void EqualityComparer_1__cctor_m18717_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18718_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18719_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18720_gshared ();
extern "C" void DefaultComparer__ctor_m18721_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18722_gshared ();
extern "C" void DefaultComparer_Equals_m18723_gshared ();
extern "C" void Nullable_1_Equals_m18724_gshared ();
extern "C" void Nullable_1_Equals_m18725_gshared ();
void* RuntimeInvoker_Boolean_t72_Nullable_1_t1924 (const MethodInfo* method, void* obj, void** args);
extern "C" void Nullable_1_GetHashCode_m18726_gshared ();
extern "C" void Nullable_1_ToString_m18727_gshared ();
extern "C" void GenericComparer_1_Compare_m18728_gshared ();
void* RuntimeInvoker_Int32_t54_Guid_t769_Guid_t769 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m18729_gshared ();
extern "C" void Comparer_1__cctor_m18730_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m18731_gshared ();
extern "C" void Comparer_1_get_Default_m18732_gshared ();
extern "C" void DefaultComparer__ctor_m18733_gshared ();
extern "C" void DefaultComparer_Compare_m18734_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18735_gshared ();
void* RuntimeInvoker_Int32_t54_Guid_t769 (const MethodInfo* method, void* obj, void** args);
extern "C" void GenericEqualityComparer_1_Equals_m18736_gshared ();
void* RuntimeInvoker_Boolean_t72_Guid_t769_Guid_t769 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m18737_gshared ();
extern "C" void EqualityComparer_1__cctor_m18738_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18739_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18740_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18741_gshared ();
extern "C" void DefaultComparer__ctor_m18742_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18743_gshared ();
extern "C" void DefaultComparer_Equals_m18744_gshared ();
extern "C" void GenericComparer_1_Compare_m18745_gshared ();
void* RuntimeInvoker_Int32_t54_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void Comparer_1__ctor_m18746_gshared ();
extern "C" void Comparer_1__cctor_m18747_gshared ();
extern "C" void Comparer_1_System_Collections_IComparer_Compare_m18748_gshared ();
extern "C" void Comparer_1_get_Default_m18749_gshared ();
extern "C" void DefaultComparer__ctor_m18750_gshared ();
extern "C" void DefaultComparer_Compare_m18751_gshared ();
extern "C" void GenericEqualityComparer_1_GetHashCode_m18752_gshared ();
extern "C" void GenericEqualityComparer_1_Equals_m18753_gshared ();
void* RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051 (const MethodInfo* method, void* obj, void** args);
extern "C" void EqualityComparer_1__ctor_m18754_gshared ();
extern "C" void EqualityComparer_1__cctor_m18755_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18756_gshared ();
extern "C" void EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18757_gshared ();
extern "C" void EqualityComparer_1_get_Default_m18758_gshared ();
extern "C" void DefaultComparer__ctor_m18759_gshared ();
extern "C" void DefaultComparer_GetHashCode_m18760_gshared ();
extern "C" void DefaultComparer_Equals_m18761_gshared ();
extern const Il2CppGenericInst GenInst_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_AudioManager_t2_0_0_0;
extern const Il2CppGenericInst GenInst_objectSource_t8_0_0_0;
extern const Il2CppGenericInst GenInst_AudioPooler_t10_0_0_0;
extern const Il2CppGenericInst GenInst_AudioSource_t9_0_0_0;
extern const Il2CppGenericInst GenInst_objectSource_t8_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_AudioClip_t20_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_AudioClip_t20_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t48_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasScaler_t50_0_0_0;
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t51_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasGroup_t52_0_0_0;
extern const Il2CppGenericInst GenInst_BaseCanvas_1_t60_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CFadeInCoroutineU3Ec__Iterator3_t65_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CFadeOutCoroutineU3Ec__Iterator4_t66_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_BaseOverlay_1_t63_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_MonoManager_1_t69_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_NonMonoManager_1_t70_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tools_InstantiateCanvas_m214_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tools_InstantiateManager_m215_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Tools_InstantiateResource_m216_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInputModule_t101_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t137_0_0_0;
extern const Il2CppGenericInst GenInst_IDeselectHandler_t316_0_0_0;
extern const Il2CppGenericInst GenInst_ISelectHandler_t315_0_0_0;
extern const Il2CppGenericInst GenInst_BaseEventData_t102_0_0_0;
extern const Il2CppGenericInst GenInst_Entry_t108_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t303_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t304_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t305_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t306_0_0_0;
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t307_0_0_0;
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t308_0_0_0;
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t309_0_0_0;
extern const Il2CppGenericInst GenInst_IDragHandler_t310_0_0_0;
extern const Il2CppGenericInst GenInst_IEndDragHandler_t311_0_0_0;
extern const Il2CppGenericInst GenInst_IDropHandler_t312_0_0_0;
extern const Il2CppGenericInst GenInst_IScrollHandler_t313_0_0_0;
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t314_0_0_0;
extern const Il2CppGenericInst GenInst_IMoveHandler_t317_0_0_0;
extern const Il2CppGenericInst GenInst_ISubmitHandler_t318_0_0_0;
extern const Il2CppGenericInst GenInst_ICancelHandler_t319_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t321_0_0_0;
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t407_0_0_0;
extern const Il2CppGenericInst GenInst_Transform_t30_0_0_0;
extern const Il2CppGenericInst GenInst_PointerEventData_t143_0_0_0;
extern const Il2CppGenericInst GenInst_AxisEventData_t139_0_0_0;
extern const Il2CppGenericInst GenInst_BaseRaycaster_t136_0_0_0;
extern const Il2CppGenericInst GenInst_Camera_t156_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_t5_0_0_0;
extern const Il2CppGenericInst GenInst_EventSystem_t104_0_0_0;
extern const Il2CppGenericInst GenInst_ButtonState_t146_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_PointerEventData_t143_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteRenderer_t345_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit_t323_0_0_0;
extern const Il2CppGenericInst GenInst_Color_t163_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t325_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t176_0_0_0_List_1_t354_0_0_0;
extern const Il2CppGenericInst GenInst_Text_t223_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t176_0_0_0;
extern const Il2CppGenericInst GenInst_ColorTween_t162_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t227_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t225_0_0_0;
extern const Il2CppGenericInst GenInst_RectTransform_t183_0_0_0;
extern const Il2CppGenericInst GenInst_CanvasRenderer_t184_0_0_0;
extern const Il2CppGenericInst GenInst_Component_t49_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t188_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t48_0_0_0_IndexedSet_1_t366_0_0_0;
extern const Il2CppGenericInst GenInst_Sprite_t201_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t194_0_0_0;
extern const Il2CppGenericInst GenInst_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_FillMethod_t195_0_0_0;
extern const Il2CppGenericInst GenInst_Single_t85_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_SubmitEvent_t209_0_0_0;
extern const Il2CppGenericInst GenInst_OnChangeEvent_t211_0_0_0;
extern const Il2CppGenericInst GenInst_OnValidateInput_t215_0_0_0;
extern const Il2CppGenericInst GenInst_ContentType_t205_0_0_0;
extern const Il2CppGenericInst GenInst_LineType_t208_0_0_0;
extern const Il2CppGenericInst GenInst_InputType_t206_0_0_0;
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t370_0_0_0;
extern const Il2CppGenericInst GenInst_CharacterValidation_t207_0_0_0;
extern const Il2CppGenericInst GenInst_Char_t369_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t375_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t377_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutElement_t283_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t235_0_0_0;
extern const Il2CppGenericInst GenInst_Vector2_t53_0_0_0;
extern const Il2CppGenericInst GenInst_Selectable_t169_0_0_0;
extern const Il2CppGenericInst GenInst_Navigation_t232_0_0_0;
extern const Il2CppGenericInst GenInst_Transition_t248_0_0_0;
extern const Il2CppGenericInst GenInst_ColorBlock_t175_0_0_0;
extern const Il2CppGenericInst GenInst_SpriteState_t252_0_0_0;
extern const Il2CppGenericInst GenInst_AnimationTriggers_t164_0_0_0;
extern const Il2CppGenericInst GenInst_Animator_t329_0_0_0;
extern const Il2CppGenericInst GenInst_Direction_t254_0_0_0;
extern const Il2CppGenericInst GenInst_Image_t203_0_0_0;
extern const Il2CppGenericInst GenInst_MatEntry_t258_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t265_0_0_0;
extern const Il2CppGenericInst GenInst_Toggle_t265_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_AspectMode_t269_0_0_0;
extern const Il2CppGenericInst GenInst_FitMode_t274_0_0_0;
extern const Il2CppGenericInst GenInst_Corner_t276_0_0_0;
extern const Il2CppGenericInst GenInst_Axis_t277_0_0_0;
extern const Il2CppGenericInst GenInst_Constraint_t278_0_0_0;
extern const Il2CppGenericInst GenInst_RectOffset_t284_0_0_0;
extern const Il2CppGenericInst GenInst_TextAnchor_t388_0_0_0;
extern const Il2CppGenericInst GenInst_ILayoutElement_t333_0_0_0_Single_t85_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t334_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t332_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m2011_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2012_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2014_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m2015_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m2016_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_TweenRunner_1_t413_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t288_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t422_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t422_gp_0_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_ObjectPool_1_t424_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GcLeaderboard_t492_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_LayoutCache_t511_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t515_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t513_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0;
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t546_0_0_0;
extern const Il2CppGenericInst GenInst_Rigidbody2D_t568_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t604_0_0_0;
extern const Il2CppGenericInst GenInst_MatchDesc_t606_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t611_0_0_0_NetworkAccessToken_t613_0_0_0;
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t598_0_0_0;
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t600_0_0_0;
extern const Il2CppGenericInst GenInst_BasicResponse_t595_0_0_0;
extern const Il2CppGenericInst GenInst_ListMatchResponse_t608_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t718_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t631_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t720_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t629_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t721_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t767_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t630_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t770_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorInfo_t632_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayer_t499_0_0_0;
extern const Il2CppGenericInst GenInst_PersistentCall_t685_0_0_0;
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t682_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t728_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t730_0_0_0;
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t663_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t657_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3541_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3542_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3543_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m3545_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m3546_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m3547_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m3551_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t753_0_0_0;
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t798_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m3553_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t801_gp_0_0_0_0_ThreadSafeDictionary_2_t801_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t801_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t801_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t871_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t229_0_0_0_TextEditOp_t678_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_1_t808_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t809_gp_0_0_0_0_InvokableCall_2_t809_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t809_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_2_t809_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t810_gp_0_0_0_0_InvokableCall_3_t810_gp_1_0_0_0_InvokableCall_3_t810_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t810_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t810_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_3_t810_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t811_gp_0_0_0_0_InvokableCall_4_t811_gp_1_0_0_0_InvokableCall_4_t811_gp_2_0_0_0_InvokableCall_4_t811_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t811_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t811_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t811_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_InvokableCall_4_t811_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t788_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_1_t812_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_2_t813_gp_0_0_0_0_UnityEvent_2_t813_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_3_t814_gp_0_0_0_0_UnityEvent_3_t814_gp_1_0_0_0_UnityEvent_3_t814_gp_2_0_0_0;
extern const Il2CppGenericInst GenInst_UnityEvent_4_t815_gp_0_0_0_0_UnityEvent_4_t815_gp_1_0_0_0_UnityEvent_4_t815_gp_2_0_0_0_UnityEvent_4_t815_gp_3_0_0_0;
extern const Il2CppGenericInst GenInst_PredicateOf_1_t937_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_PredicateOf_1_t937_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_First_m3749_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_First_m3749_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m3750_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m3750_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3751_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3751_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3752_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3753_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_Where_m3753_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3754_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3754_gp_0_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1186_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Queue_1_t1185_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1188_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Stack_1_t1187_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_StrongName_t1764_0_0_0;
extern const Il2CppGenericInst GenInst_DateTime_t508_0_0_0;
extern const Il2CppGenericInst GenInst_DateTimeOffset_t768_0_0_0;
extern const Il2CppGenericInst GenInst_TimeSpan_t1051_0_0_0;
extern const Il2CppGenericInst GenInst_Guid_t769_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t753_0_0_0;
extern const Il2CppGenericInst GenInst_UInt32_t744_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t367_0_0_0;
extern const Il2CppGenericInst GenInst_SByte_t73_0_0_0;
extern const Il2CppGenericInst GenInst_Int16_t448_0_0_0;
extern const Il2CppGenericInst GenInst_UInt16_t371_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1955_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Double_t752_0_0_0;
extern const Il2CppGenericInst GenInst_Decimal_t755_0_0_0;
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t1956_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1958_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1957_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m10452_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10464_gp_0_0_0_0_Array_Sort_m10464_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10465_gp_0_0_0_0_Array_Sort_m10465_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10466_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10466_gp_0_0_0_0_Array_Sort_m10466_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10467_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10467_gp_0_0_0_0_Array_Sort_m10467_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10468_gp_0_0_0_0_Array_Sort_m10468_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10469_gp_0_0_0_0_Array_Sort_m10469_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10470_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10470_gp_0_0_0_0_Array_Sort_m10470_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10471_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10471_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10471_gp_0_0_0_0_Array_Sort_m10471_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10472_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Sort_m10473_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m10474_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m10474_gp_0_0_0_0_Array_qsort_m10474_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_compare_m10475_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_qsort_m10476_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Resize_m10479_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m10481_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ForEach_m10482_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m10483_gp_0_0_0_0_Array_ConvertAll_m10483_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10484_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10485_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10486_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10487_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10488_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10489_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10490_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10491_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10492_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10493_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10494_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10495_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10496_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10497_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10498_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10499_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindAll_m10500_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Exists_m10501_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m10502_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_Find_m10503_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Array_FindLast_m10504_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IList_1_t1959_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ICollection_1_t1960_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Nullable_1_t1928_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t1967_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Comparer_1_t1966_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1968_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1970_gp_0_0_0_0_ShimEnumerator_t1970_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1971_gp_0_0_0_0_Enumerator_t1971_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2261_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1973_gp_0_0_0_0_Enumerator_t1973_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1973_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t1972_gp_0_0_0_0_KeyCollection_t1972_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t1972_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t1972_gp_0_0_0_0_KeyCollection_t1972_gp_1_0_0_0_KeyCollection_t1972_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_KeyCollection_t1972_gp_0_0_0_0_KeyCollection_t1972_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1975_gp_0_0_0_0_Enumerator_t1975_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1975_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t1974_gp_0_0_0_0_ValueCollection_t1974_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t1974_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t1974_gp_0_0_0_0_ValueCollection_t1974_gp_1_0_0_0_ValueCollection_t1974_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_ValueCollection_t1974_gp_1_0_0_0_ValueCollection_t1974_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0_Dictionary_2_t1969_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2298_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0_Dictionary_2_t1969_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m10653_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0_Dictionary_2_t1969_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m10658_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m10658_gp_0_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0_Dictionary_2_t1969_gp_1_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1147_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Dictionary_2_t1969_gp_0_0_0_0_Dictionary_2_t1969_gp_1_0_0_0_KeyValuePair_2_t2298_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2298_0_0_0_KeyValuePair_2_t2298_0_0_0;
extern const Il2CppGenericInst GenInst_DefaultComparer_t1978_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1977_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t1979_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1981_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1981_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3388_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t1981_gp_0_0_0_0_IDictionary_2_t1981_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1983_gp_0_0_0_0_KeyValuePair_2_t1983_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_Enumerator_t1985_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t1984_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Collection_1_t1986_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t1987_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m10934_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m10934_gp_1_0_0_0;
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m10935_gp_0_0_0_0;
extern const Il2CppGenericInst GenInst_Version_t1006_0_0_0;
extern const Il2CppGenericInst GenInst_IReflect_t1963_0_0_0;
extern const Il2CppGenericInst GenInst__Type_t1961_0_0_0;
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppGenericInst GenInst__MemberInfo_t1962_0_0_0;
extern const Il2CppGenericInst GenInst_IFormattable_t74_0_0_0;
extern const Il2CppGenericInst GenInst_IConvertible_t75_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_t76_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2025_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2026_0_0_0;
extern const Il2CppGenericInst GenInst_ValueType_t439_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2067_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2068_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2047_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2048_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2055_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2056_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t72_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t367_0_0_0;
extern const Il2CppGenericInst GenInst_IEnumerable_t464_0_0_0;
extern const Il2CppGenericInst GenInst_ICloneable_t427_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2059_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2060_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536_0_0_0;
extern const Il2CppGenericInst GenInst_Link_t1459_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Object_t_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Object_t_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2536_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_PointerEventData_t143_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t343_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastHit2D_t349_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2567_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t54_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t54_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t54_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t54_0_0_0_KeyValuePair_2_t2567_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t325_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_ICanvasElement_t325_0_0_0_Int32_t54_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2593_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0;
extern const Il2CppGenericInst GenInst_Font_t176_0_0_0_List_1_t354_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_List_1_t354_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2603_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t188_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_Graphic_t188_0_0_0_Int32_t54_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Canvas_t48_0_0_0_IndexedSet_1_t366_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_IndexedSet_1_t366_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2635_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2639_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2643_0_0_0;
extern const Il2CppGenericInst GenInst_Enum_t77_0_0_0;
extern const Il2CppGenericInst GenInst_Vector3_t138_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t85_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t33_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievementDescription_t807_0_0_0;
extern const Il2CppGenericInst GenInst_IAchievement_t702_0_0_0;
extern const Il2CppGenericInst GenInst_IScore_t662_0_0_0;
extern const Il2CppGenericInst GenInst_IUserProfile_t806_0_0_0;
extern const Il2CppGenericInst GenInst_AchievementDescription_t660_0_0_0;
extern const Il2CppGenericInst GenInst_UserProfile_t658_0_0_0;
extern const Il2CppGenericInst GenInst_GcAchievementData_t650_0_0_0;
extern const Il2CppGenericInst GenInst_Achievement_t659_0_0_0;
extern const Il2CppGenericInst GenInst_GcScoreData_t651_0_0_0;
extern const Il2CppGenericInst GenInst_Score_t661_0_0_0;
extern const Il2CppGenericInst GenInst_GUILayoutOption_t519_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_LayoutCache_t511_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_LayoutCache_t511_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2715_0_0_0;
extern const Il2CppGenericInst GenInst_GUIStyle_t513_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t513_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2726_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t54_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2730_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t739_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2038_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2039_0_0_0;
extern const Il2CppGenericInst GenInst_Behaviour_t394_0_0_0;
extern const Il2CppGenericInst GenInst_Display_t557_0_0_0;
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0;
extern const Il2CppGenericInst GenInst_ISerializable_t428_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2064_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2065_0_0_0;
extern const Il2CppGenericInst GenInst_Keyframe_t580_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t753_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2780_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2030_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2031_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t753_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t753_0_0_0_Int64_t753_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t753_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t753_0_0_0_KeyValuePair_2_t2780_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t753_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2795_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2036_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2037_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_Object_t_0_0_0_UInt64_t756_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_Object_t_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_Object_t_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2818_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t611_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkID_t611_0_0_0_NetworkAccessToken_t613_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t613_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2833_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t631_0_0_0;
extern const Il2CppGenericInst GenInst_GetDelegate_t629_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t720_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t767_0_0_0;
extern const Il2CppGenericInst GenInst_IDictionary_2_t721_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2839_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t631_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2847_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t720_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2851_0_0_0;
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t721_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2855_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t629_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0_KeyValuePair_2_t2593_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2593_0_0_0_KeyValuePair_2_t2839_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t767_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1999_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBase_t47_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBase_t2002_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterInfo_t775_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterInfo_t2005_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyInfo_t2006_0_0_0;
extern const Il2CppGenericInst GenInst__FieldInfo_t2001_0_0_0;
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t417_0_0_0;
extern const Il2CppGenericInst GenInst_Attribute_t539_0_0_0;
extern const Il2CppGenericInst GenInst__Attribute_t832_0_0_0;
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t416_0_0_0;
extern const Il2CppGenericInst GenInst_RequireComponent_t61_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterModifier_t1584_0_0_0;
extern const Il2CppGenericInst GenInst_HitInfo_t665_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t229_0_0_0;
extern const Il2CppGenericInst GenInst_Event_t229_0_0_0_TextEditOp_t678_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_TextEditOp_t678_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2900_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2933_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t367_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t367_0_0_0_Byte_t367_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t367_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Byte_t367_0_0_0_KeyValuePair_2_t2933_0_0_0;
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t72_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2947_0_0_0;
extern const Il2CppGenericInst GenInst_X509Certificate_t1037_0_0_0;
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1206_0_0_0;
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1045_0_0_0;
extern const Il2CppGenericInst GenInst_Capture_t1070_0_0_0;
extern const Il2CppGenericInst GenInst_Group_t1073_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2956_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Int32_t54_0_0_0_Int32_t54_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Int32_t54_0_0_0_DictionaryEntry_t1147_0_0_0;
extern const Il2CppGenericInst GenInst_Int32_t54_0_0_0_Int32_t54_0_0_0_KeyValuePair_2_t2956_0_0_0;
extern const Il2CppGenericInst GenInst_Mark_t1099_0_0_0;
extern const Il2CppGenericInst GenInst_UriScheme_t1135_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2033_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2034_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1222_0_0_0;
extern const Il2CppGenericInst GenInst_KeySizes_t1343_0_0_0;
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1315_0_0_0;
extern const Il2CppGenericInst GenInst_Delegate_t361_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2044_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2045_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2041_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2042_0_0_0;
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0;
extern const Il2CppGenericInst GenInst__MethodInfo_t2003_0_0_0;
extern const Il2CppGenericInst GenInst_TableRange_t1392_0_0_0;
extern const Il2CppGenericInst GenInst_TailoringInfo_t1395_0_0_0;
extern const Il2CppGenericInst GenInst_Contraction_t1396_0_0_0;
extern const Il2CppGenericInst GenInst_Level2Map_t1398_0_0_0;
extern const Il2CppGenericInst GenInst_BigInteger_t1418_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t1469_0_0_0;
extern const Il2CppGenericInst GenInst_Slot_t1476_0_0_0;
extern const Il2CppGenericInst GenInst_StackFrame_t46_0_0_0;
extern const Il2CppGenericInst GenInst_Calendar_t1488_0_0_0;
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1548_0_0_0;
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1993_0_0_0;
extern const Il2CppGenericInst GenInst_Module_t1549_0_0_0;
extern const Il2CppGenericInst GenInst__Module_t2004_0_0_0;
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1550_0_0_0;
extern const Il2CppGenericInst GenInst__ParameterBuilder_t1994_0_0_0;
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1546_0_0_0;
extern const Il2CppGenericInst GenInst_MethodBuilder_t1545_0_0_0;
extern const Il2CppGenericInst GenInst__MethodBuilder_t1992_0_0_0;
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1541_0_0_0;
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1989_0_0_0;
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1551_0_0_0;
extern const Il2CppGenericInst GenInst__PropertyBuilder_t1995_0_0_0;
extern const Il2CppGenericInst GenInst_FieldBuilder_t1544_0_0_0;
extern const Il2CppGenericInst GenInst__FieldBuilder_t1991_0_0_0;
extern const Il2CppGenericInst GenInst_Header_t1648_0_0_0;
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1936_0_0_0;
extern const Il2CppGenericInst GenInst_IContextAttribute_t1931_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2394_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2395_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2070_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2071_0_0_0;
extern const Il2CppGenericInst GenInst_IComparable_1_t2414_0_0_0;
extern const Il2CppGenericInst GenInst_IEquatable_1_t2415_0_0_0;
extern const Il2CppGenericInst GenInst_TypeTag_t1686_0_0_0;
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0;
extern const Il2CppGenericInst GenInst_RaycastResult_t137_0_0_0_RaycastResult_t137_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2536_0_0_0_KeyValuePair_2_t2536_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2567_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2567_0_0_0_KeyValuePair_2_t2567_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2593_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2593_0_0_0_KeyValuePair_2_t2593_0_0_0;
extern const Il2CppGenericInst GenInst_UIVertex_t225_0_0_0_UIVertex_t225_0_0_0;
extern const Il2CppGenericInst GenInst_UICharInfo_t377_0_0_0_UICharInfo_t377_0_0_0;
extern const Il2CppGenericInst GenInst_UILineInfo_t375_0_0_0_UILineInfo_t375_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t753_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Int64_t753_0_0_0_Int64_t753_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2780_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2780_0_0_0_KeyValuePair_2_t2780_0_0_0;
extern const Il2CppGenericInst GenInst_UInt64_t756_0_0_0_UInt64_t756_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2818_0_0_0_KeyValuePair_2_t2818_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2839_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2839_0_0_0_KeyValuePair_2_t2839_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t367_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_Byte_t367_0_0_0_Byte_t367_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2933_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2933_0_0_0_KeyValuePair_2_t2933_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2956_0_0_0_Object_t_0_0_0;
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2956_0_0_0_KeyValuePair_2_t2956_0_0_0;
const methodPointerType g_Il2CppMethodPointers[3038] = 
{
	NULL/* 0*/,
	(methodPointerType)&BaseCanvas_1_get_Instance_m11745_gshared/* 1*/,
	(methodPointerType)&BaseCanvas_1__ctor_m11743_gshared/* 2*/,
	(methodPointerType)&BaseCanvas_1__cctor_m11744_gshared/* 3*/,
	(methodPointerType)&BaseCanvas_1_Create_m11746_gshared/* 4*/,
	(methodPointerType)&BaseCanvas_1_Awake_m11747_gshared/* 5*/,
	(methodPointerType)&BaseCanvas_1_OnDestroy_m11748_gshared/* 6*/,
	(methodPointerType)&BaseCanvas_1_Open_m11749_gshared/* 7*/,
	(methodPointerType)&BaseCanvas_1_Close_m11750_gshared/* 8*/,
	(methodPointerType)&BaseOverlay_1__ctor_m11751_gshared/* 9*/,
	(methodPointerType)&BaseOverlay_1_Awake_m11752_gshared/* 10*/,
	(methodPointerType)&BaseOverlay_1_Open_m11753_gshared/* 11*/,
	(methodPointerType)&BaseOverlay_1_Close_m11754_gshared/* 12*/,
	(methodPointerType)&BaseOverlay_1_OnClose_m11755_gshared/* 13*/,
	(methodPointerType)&BaseOverlay_1_CancelClose_m11756_gshared/* 14*/,
	(methodPointerType)&BaseOverlay_1_CloseWithDelay_m11757_gshared/* 15*/,
	(methodPointerType)&BaseOverlay_1_CloseWithDelayCoroutine_m11758_gshared/* 16*/,
	(methodPointerType)&BaseOverlay_1_SetSortingOrder_m11759_gshared/* 17*/,
	(methodPointerType)&BaseOverlay_1_FadeIn_m11760_gshared/* 18*/,
	(methodPointerType)&BaseOverlay_1_FadeOut_m11761_gshared/* 19*/,
	(methodPointerType)&BaseOverlay_1_FadeInCoroutine_m11762_gshared/* 20*/,
	(methodPointerType)&BaseOverlay_1_FadeOutCoroutine_m11763_gshared/* 21*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11765_gshared/* 22*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m11766_gshared/* 23*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2__ctor_m11764_gshared/* 24*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_MoveNext_m11767_gshared/* 25*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m11768_gshared/* 26*/,
	(methodPointerType)&U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m11769_gshared/* 27*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11771_gshared/* 28*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m11772_gshared/* 29*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3__ctor_m11770_gshared/* 30*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m11773_gshared/* 31*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m11774_gshared/* 32*/,
	(methodPointerType)&U3CFadeInCoroutineU3Ec__Iterator3_Reset_m11775_gshared/* 33*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11777_gshared/* 34*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m11778_gshared/* 35*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4__ctor_m11776_gshared/* 36*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4_MoveNext_m11779_gshared/* 37*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m11780_gshared/* 38*/,
	(methodPointerType)&U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m11781_gshared/* 39*/,
	(methodPointerType)&MonoManager_1_get_Instance_m11267_gshared/* 40*/,
	(methodPointerType)&MonoManager_1__ctor_m11264_gshared/* 41*/,
	(methodPointerType)&MonoManager_1__cctor_m11266_gshared/* 42*/,
	(methodPointerType)&MonoManager_1_Create_m11268_gshared/* 43*/,
	(methodPointerType)&MonoManager_1_Awake_m11270_gshared/* 44*/,
	(methodPointerType)&MonoManager_1_OnDestroy_m11271_gshared/* 45*/,
	(methodPointerType)&MonoManager_1_Destroy_m11273_gshared/* 46*/,
	(methodPointerType)&NonMonoManager_1_get_Instance_m11789_gshared/* 47*/,
	(methodPointerType)&NonMonoManager_1__ctor_m11787_gshared/* 48*/,
	(methodPointerType)&NonMonoManager_1__cctor_m11788_gshared/* 49*/,
	(methodPointerType)&NonMonoManager_1_Create_m11790_gshared/* 50*/,
	(methodPointerType)&NonMonoManager_1_Destroy_m11791_gshared/* 51*/,
	(methodPointerType)&Tools_InstantiateCanvas_TisObject_t_m18821_gshared/* 52*/,
	(methodPointerType)&Tools_InstantiateManager_TisObject_t_m18763_gshared/* 53*/,
	(methodPointerType)&Tools_InstantiateResource_TisObject_t_m18762_gshared/* 54*/,
	(methodPointerType)&ExecuteEvents_ValidateEventData_TisObject_t_m1469_gshared/* 55*/,
	(methodPointerType)&ExecuteEvents_Execute_TisObject_t_m1451_gshared/* 56*/,
	(methodPointerType)&ExecuteEvents_ExecuteHierarchy_TisObject_t_m1538_gshared/* 57*/,
	(methodPointerType)&ExecuteEvents_ShouldSendToComponent_TisObject_t_m18826_gshared/* 58*/,
	(methodPointerType)&ExecuteEvents_GetEventList_TisObject_t_m18823_gshared/* 59*/,
	(methodPointerType)&ExecuteEvents_CanHandleEvent_TisObject_t_m18851_gshared/* 60*/,
	(methodPointerType)&ExecuteEvents_GetEventHandler_TisObject_t_m1511_gshared/* 61*/,
	(methodPointerType)&EventFunction_1__ctor_m11887_gshared/* 62*/,
	(methodPointerType)&EventFunction_1_Invoke_m11889_gshared/* 63*/,
	(methodPointerType)&EventFunction_1_BeginInvoke_m11891_gshared/* 64*/,
	(methodPointerType)&EventFunction_1_EndInvoke_m11893_gshared/* 65*/,
	(methodPointerType)&SetPropertyUtility_SetClass_TisObject_t_m1675_gshared/* 66*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisObject_t_m1952_gshared/* 67*/,
	(methodPointerType)&IndexedSet_1_get_Count_m13079_gshared/* 68*/,
	(methodPointerType)&IndexedSet_1_get_IsReadOnly_m13081_gshared/* 69*/,
	(methodPointerType)&IndexedSet_1_get_Item_m13089_gshared/* 70*/,
	(methodPointerType)&IndexedSet_1_set_Item_m13091_gshared/* 71*/,
	(methodPointerType)&IndexedSet_1__ctor_m13063_gshared/* 72*/,
	(methodPointerType)&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared/* 73*/,
	(methodPointerType)&IndexedSet_1_Add_m13067_gshared/* 74*/,
	(methodPointerType)&IndexedSet_1_Remove_m13069_gshared/* 75*/,
	(methodPointerType)&IndexedSet_1_GetEnumerator_m13071_gshared/* 76*/,
	(methodPointerType)&IndexedSet_1_Clear_m13073_gshared/* 77*/,
	(methodPointerType)&IndexedSet_1_Contains_m13075_gshared/* 78*/,
	(methodPointerType)&IndexedSet_1_CopyTo_m13077_gshared/* 79*/,
	(methodPointerType)&IndexedSet_1_IndexOf_m13083_gshared/* 80*/,
	(methodPointerType)&IndexedSet_1_Insert_m13085_gshared/* 81*/,
	(methodPointerType)&IndexedSet_1_RemoveAt_m13087_gshared/* 82*/,
	(methodPointerType)&IndexedSet_1_RemoveAll_m13092_gshared/* 83*/,
	(methodPointerType)&IndexedSet_1_Sort_m13093_gshared/* 84*/,
	(methodPointerType)&ObjectPool_1_get_countAll_m11989_gshared/* 85*/,
	(methodPointerType)&ObjectPool_1_set_countAll_m11991_gshared/* 86*/,
	(methodPointerType)&ObjectPool_1_get_countActive_m11993_gshared/* 87*/,
	(methodPointerType)&ObjectPool_1_get_countInactive_m11995_gshared/* 88*/,
	(methodPointerType)&ObjectPool_1__ctor_m11987_gshared/* 89*/,
	(methodPointerType)&ObjectPool_1_Get_m11997_gshared/* 90*/,
	(methodPointerType)&ObjectPool_1_Release_m11999_gshared/* 91*/,
	(methodPointerType)&ScriptableObject_CreateInstance_TisObject_t_m19019_gshared/* 92*/,
	(methodPointerType)&Object_FindObjectOfType_TisObject_t_m91_gshared/* 93*/,
	(methodPointerType)&Component_GetComponent_TisObject_t_m124_gshared/* 94*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m19017_gshared/* 95*/,
	(methodPointerType)&Component_GetComponentsInChildren_TisObject_t_m1967_gshared/* 96*/,
	(methodPointerType)&Component_GetComponents_TisObject_t_m1449_gshared/* 97*/,
	(methodPointerType)&GameObject_GetComponent_TisObject_t_m1559_gshared/* 98*/,
	(methodPointerType)&GameObject_GetComponents_TisObject_t_m18825_gshared/* 99*/,
	(methodPointerType)&GameObject_GetComponentsInChildren_TisObject_t_m19018_gshared/* 100*/,
	(methodPointerType)&GameObject_GetComponentsInParent_TisObject_t_m1612_gshared/* 101*/,
	(methodPointerType)&GameObject_AddComponent_TisObject_t_m95_gshared/* 102*/,
	(methodPointerType)&ResponseBase_ParseJSONList_TisObject_t_m3433_gshared/* 103*/,
	(methodPointerType)&NetworkMatch_ProcessMatchResponse_TisObject_t_m3440_gshared/* 104*/,
	(methodPointerType)&ResponseDelegate_1__ctor_m16791_gshared/* 105*/,
	(methodPointerType)&ResponseDelegate_1_Invoke_m16793_gshared/* 106*/,
	(methodPointerType)&ResponseDelegate_1_BeginInvoke_m16795_gshared/* 107*/,
	(methodPointerType)&ResponseDelegate_1_EndInvoke_m16797_gshared/* 108*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m16799_gshared/* 109*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m16800_gshared/* 110*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m16798_gshared/* 111*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m16801_gshared/* 112*/,
	(methodPointerType)&U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m16802_gshared/* 113*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Keys_m16942_gshared/* 114*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Values_m16948_gshared/* 115*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Item_m16950_gshared/* 116*/,
	(methodPointerType)&ThreadSafeDictionary_2_set_Item_m16952_gshared/* 117*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_Count_m16962_gshared/* 118*/,
	(methodPointerType)&ThreadSafeDictionary_2_get_IsReadOnly_m16964_gshared/* 119*/,
	(methodPointerType)&ThreadSafeDictionary_2__ctor_m16932_gshared/* 120*/,
	(methodPointerType)&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934_gshared/* 121*/,
	(methodPointerType)&ThreadSafeDictionary_2_Get_m16936_gshared/* 122*/,
	(methodPointerType)&ThreadSafeDictionary_2_AddValue_m16938_gshared/* 123*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m16940_gshared/* 124*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m16944_gshared/* 125*/,
	(methodPointerType)&ThreadSafeDictionary_2_TryGetValue_m16946_gshared/* 126*/,
	(methodPointerType)&ThreadSafeDictionary_2_Add_m16954_gshared/* 127*/,
	(methodPointerType)&ThreadSafeDictionary_2_Clear_m16956_gshared/* 128*/,
	(methodPointerType)&ThreadSafeDictionary_2_Contains_m16958_gshared/* 129*/,
	(methodPointerType)&ThreadSafeDictionary_2_CopyTo_m16960_gshared/* 130*/,
	(methodPointerType)&ThreadSafeDictionary_2_Remove_m16966_gshared/* 131*/,
	(methodPointerType)&ThreadSafeDictionary_2_GetEnumerator_m16968_gshared/* 132*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2__ctor_m16925_gshared/* 133*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_Invoke_m16927_gshared/* 134*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m16929_gshared/* 135*/,
	(methodPointerType)&ThreadSafeDictionaryValueFactory_2_EndInvoke_m16931_gshared/* 136*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m18850_gshared/* 137*/,
	(methodPointerType)&InvokableCall_1__ctor_m12464_gshared/* 138*/,
	(methodPointerType)&InvokableCall_1__ctor_m12465_gshared/* 139*/,
	(methodPointerType)&InvokableCall_1_Invoke_m12466_gshared/* 140*/,
	(methodPointerType)&InvokableCall_1_Find_m12467_gshared/* 141*/,
	(methodPointerType)&InvokableCall_2__ctor_m17754_gshared/* 142*/,
	(methodPointerType)&InvokableCall_2_Invoke_m17755_gshared/* 143*/,
	(methodPointerType)&InvokableCall_2_Find_m17756_gshared/* 144*/,
	(methodPointerType)&InvokableCall_3__ctor_m17761_gshared/* 145*/,
	(methodPointerType)&InvokableCall_3_Invoke_m17762_gshared/* 146*/,
	(methodPointerType)&InvokableCall_3_Find_m17763_gshared/* 147*/,
	(methodPointerType)&InvokableCall_4__ctor_m17768_gshared/* 148*/,
	(methodPointerType)&InvokableCall_4_Invoke_m17769_gshared/* 149*/,
	(methodPointerType)&InvokableCall_4_Find_m17770_gshared/* 150*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m17775_gshared/* 151*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m17776_gshared/* 152*/,
	(methodPointerType)&UnityEvent_1__ctor_m12454_gshared/* 153*/,
	(methodPointerType)&UnityEvent_1_AddListener_m12456_gshared/* 154*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m12458_gshared/* 155*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m12459_gshared/* 156*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m12460_gshared/* 157*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m12462_gshared/* 158*/,
	(methodPointerType)&UnityEvent_1_Invoke_m12463_gshared/* 159*/,
	(methodPointerType)&UnityEvent_2__ctor_m17977_gshared/* 160*/,
	(methodPointerType)&UnityEvent_2_FindMethod_Impl_m17978_gshared/* 161*/,
	(methodPointerType)&UnityEvent_2_GetDelegate_m17979_gshared/* 162*/,
	(methodPointerType)&UnityEvent_3__ctor_m17980_gshared/* 163*/,
	(methodPointerType)&UnityEvent_3_FindMethod_Impl_m17981_gshared/* 164*/,
	(methodPointerType)&UnityEvent_3_GetDelegate_m17982_gshared/* 165*/,
	(methodPointerType)&UnityEvent_4__ctor_m17983_gshared/* 166*/,
	(methodPointerType)&UnityEvent_4_FindMethod_Impl_m17984_gshared/* 167*/,
	(methodPointerType)&UnityEvent_4_GetDelegate_m17985_gshared/* 168*/,
	(methodPointerType)&UnityAction_1__ctor_m12016_gshared/* 169*/,
	(methodPointerType)&UnityAction_1_Invoke_m12017_gshared/* 170*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m12018_gshared/* 171*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m12019_gshared/* 172*/,
	(methodPointerType)&UnityAction_2__ctor_m17757_gshared/* 173*/,
	(methodPointerType)&UnityAction_2_Invoke_m17758_gshared/* 174*/,
	(methodPointerType)&UnityAction_2_BeginInvoke_m17759_gshared/* 175*/,
	(methodPointerType)&UnityAction_2_EndInvoke_m17760_gshared/* 176*/,
	(methodPointerType)&UnityAction_3__ctor_m17764_gshared/* 177*/,
	(methodPointerType)&UnityAction_3_Invoke_m17765_gshared/* 178*/,
	(methodPointerType)&UnityAction_3_BeginInvoke_m17766_gshared/* 179*/,
	(methodPointerType)&UnityAction_3_EndInvoke_m17767_gshared/* 180*/,
	(methodPointerType)&UnityAction_4__ctor_m17771_gshared/* 181*/,
	(methodPointerType)&UnityAction_4_Invoke_m17772_gshared/* 182*/,
	(methodPointerType)&UnityAction_4_BeginInvoke_m17773_gshared/* 183*/,
	(methodPointerType)&UnityAction_4_EndInvoke_m17774_gshared/* 184*/,
	(methodPointerType)&Enumerable_First_TisObject_t_m18820_gshared/* 185*/,
	(methodPointerType)&Enumerable_FirstOrDefault_TisObject_t_m97_gshared/* 186*/,
	(methodPointerType)&Enumerable_Last_TisObject_t_m18819_gshared/* 187*/,
	(methodPointerType)&Enumerable_Last_TisObject_t_m93_gshared/* 188*/,
	(methodPointerType)&Enumerable_Where_TisObject_t_m1921_gshared/* 189*/,
	(methodPointerType)&Enumerable_CreateWhereIterator_TisObject_t_m19016_gshared/* 190*/,
	(methodPointerType)&PredicateOf_1__cctor_m11605_gshared/* 191*/,
	(methodPointerType)&PredicateOf_1_U3CAlwaysU3Em__76_m11606_gshared/* 192*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14713_gshared/* 193*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m14714_gshared/* 194*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m14712_gshared/* 195*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m14715_gshared/* 196*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14716_gshared/* 197*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m14717_gshared/* 198*/,
	(methodPointerType)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m14718_gshared/* 199*/,
	(methodPointerType)&Func_2__ctor_m17986_gshared/* 200*/,
	(methodPointerType)&Func_2_Invoke_m17987_gshared/* 201*/,
	(methodPointerType)&Func_2_BeginInvoke_m17988_gshared/* 202*/,
	(methodPointerType)&Func_2_EndInvoke_m17989_gshared/* 203*/,
	(methodPointerType)&Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708_gshared/* 204*/,
	(methodPointerType)&Queue_1_System_Collections_ICollection_get_SyncRoot_m11710_gshared/* 205*/,
	(methodPointerType)&Queue_1_get_Count_m11724_gshared/* 206*/,
	(methodPointerType)&Queue_1__ctor_m11704_gshared/* 207*/,
	(methodPointerType)&Queue_1_System_Collections_ICollection_CopyTo_m11706_gshared/* 208*/,
	(methodPointerType)&Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712_gshared/* 209*/,
	(methodPointerType)&Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714_gshared/* 210*/,
	(methodPointerType)&Queue_1_CopyTo_m11716_gshared/* 211*/,
	(methodPointerType)&Queue_1_Dequeue_m11717_gshared/* 212*/,
	(methodPointerType)&Queue_1_Peek_m11719_gshared/* 213*/,
	(methodPointerType)&Queue_1_Enqueue_m11720_gshared/* 214*/,
	(methodPointerType)&Queue_1_SetCapacity_m11722_gshared/* 215*/,
	(methodPointerType)&Queue_1_GetEnumerator_m11726_gshared/* 216*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m11728_gshared/* 217*/,
	(methodPointerType)&Enumerator_get_Current_m11731_gshared/* 218*/,
	(methodPointerType)&Enumerator__ctor_m11727_gshared/* 219*/,
	(methodPointerType)&Enumerator_Dispose_m11729_gshared/* 220*/,
	(methodPointerType)&Enumerator_MoveNext_m11730_gshared/* 221*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m12001_gshared/* 222*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m12002_gshared/* 223*/,
	(methodPointerType)&Stack_1_get_Count_m12009_gshared/* 224*/,
	(methodPointerType)&Stack_1__ctor_m12000_gshared/* 225*/,
	(methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m12003_gshared/* 226*/,
	(methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12004_gshared/* 227*/,
	(methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m12005_gshared/* 228*/,
	(methodPointerType)&Stack_1_Peek_m12006_gshared/* 229*/,
	(methodPointerType)&Stack_1_Pop_m12007_gshared/* 230*/,
	(methodPointerType)&Stack_1_Push_m12008_gshared/* 231*/,
	(methodPointerType)&Stack_1_GetEnumerator_m12010_gshared/* 232*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m12012_gshared/* 233*/,
	(methodPointerType)&Enumerator_get_Current_m12015_gshared/* 234*/,
	(methodPointerType)&Enumerator__ctor_m12011_gshared/* 235*/,
	(methodPointerType)&Enumerator_Dispose_m12013_gshared/* 236*/,
	(methodPointerType)&Enumerator_MoveNext_m12014_gshared/* 237*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m18774_gshared/* 238*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisObject_t_m18766_gshared/* 239*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisObject_t_m18769_gshared/* 240*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisObject_t_m18767_gshared/* 241*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisObject_t_m18768_gshared/* 242*/,
	(methodPointerType)&Array_InternalArray__Insert_TisObject_t_m18771_gshared/* 243*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisObject_t_m18770_gshared/* 244*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisObject_t_m18765_gshared/* 245*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisObject_t_m18773_gshared/* 246*/,
	(methodPointerType)&Array_get_swapper_TisObject_t_m18779_gshared/* 247*/,
	(methodPointerType)&Array_Sort_TisObject_t_m19320_gshared/* 248*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m19321_gshared/* 249*/,
	(methodPointerType)&Array_Sort_TisObject_t_m19322_gshared/* 250*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m19323_gshared/* 251*/,
	(methodPointerType)&Array_Sort_TisObject_t_m10410_gshared/* 252*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m19324_gshared/* 253*/,
	(methodPointerType)&Array_Sort_TisObject_t_m18778_gshared/* 254*/,
	(methodPointerType)&Array_Sort_TisObject_t_TisObject_t_m18777_gshared/* 255*/,
	(methodPointerType)&Array_Sort_TisObject_t_m19325_gshared/* 256*/,
	(methodPointerType)&Array_Sort_TisObject_t_m18817_gshared/* 257*/,
	(methodPointerType)&Array_qsort_TisObject_t_TisObject_t_m18780_gshared/* 258*/,
	(methodPointerType)&Array_compare_TisObject_t_m18814_gshared/* 259*/,
	(methodPointerType)&Array_qsort_TisObject_t_m18816_gshared/* 260*/,
	(methodPointerType)&Array_swap_TisObject_t_TisObject_t_m18815_gshared/* 261*/,
	(methodPointerType)&Array_swap_TisObject_t_m18818_gshared/* 262*/,
	(methodPointerType)&Array_Resize_TisObject_t_m18776_gshared/* 263*/,
	(methodPointerType)&Array_Resize_TisObject_t_m18775_gshared/* 264*/,
	(methodPointerType)&Array_TrueForAll_TisObject_t_m19326_gshared/* 265*/,
	(methodPointerType)&Array_ForEach_TisObject_t_m19327_gshared/* 266*/,
	(methodPointerType)&Array_ConvertAll_TisObject_t_TisObject_t_m19328_gshared/* 267*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m19330_gshared/* 268*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m19331_gshared/* 269*/,
	(methodPointerType)&Array_FindLastIndex_TisObject_t_m19329_gshared/* 270*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m19333_gshared/* 271*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m19334_gshared/* 272*/,
	(methodPointerType)&Array_FindIndex_TisObject_t_m19332_gshared/* 273*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m19336_gshared/* 274*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m19337_gshared/* 275*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m19338_gshared/* 276*/,
	(methodPointerType)&Array_BinarySearch_TisObject_t_m19335_gshared/* 277*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m10412_gshared/* 278*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m19339_gshared/* 279*/,
	(methodPointerType)&Array_IndexOf_TisObject_t_m10409_gshared/* 280*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m19341_gshared/* 281*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m19340_gshared/* 282*/,
	(methodPointerType)&Array_LastIndexOf_TisObject_t_m19342_gshared/* 283*/,
	(methodPointerType)&Array_FindAll_TisObject_t_m19343_gshared/* 284*/,
	(methodPointerType)&Array_Exists_TisObject_t_m19344_gshared/* 285*/,
	(methodPointerType)&Array_AsReadOnly_TisObject_t_m19345_gshared/* 286*/,
	(methodPointerType)&Array_Find_TisObject_t_m19346_gshared/* 287*/,
	(methodPointerType)&Array_FindLast_TisObject_t_m19347_gshared/* 288*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared/* 289*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m11349_gshared/* 290*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m11345_gshared/* 291*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m11347_gshared/* 292*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m11348_gshared/* 293*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Item_m18441_gshared/* 294*/,
	(methodPointerType)&ArrayReadOnlyList_1_set_Item_m18442_gshared/* 295*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_Count_m18443_gshared/* 296*/,
	(methodPointerType)&ArrayReadOnlyList_1_get_IsReadOnly_m18444_gshared/* 297*/,
	(methodPointerType)&ArrayReadOnlyList_1__ctor_m18439_gshared/* 298*/,
	(methodPointerType)&ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18440_gshared/* 299*/,
	(methodPointerType)&ArrayReadOnlyList_1_Add_m18445_gshared/* 300*/,
	(methodPointerType)&ArrayReadOnlyList_1_Clear_m18446_gshared/* 301*/,
	(methodPointerType)&ArrayReadOnlyList_1_Contains_m18447_gshared/* 302*/,
	(methodPointerType)&ArrayReadOnlyList_1_CopyTo_m18448_gshared/* 303*/,
	(methodPointerType)&ArrayReadOnlyList_1_GetEnumerator_m18449_gshared/* 304*/,
	(methodPointerType)&ArrayReadOnlyList_1_IndexOf_m18450_gshared/* 305*/,
	(methodPointerType)&ArrayReadOnlyList_1_Insert_m18451_gshared/* 306*/,
	(methodPointerType)&ArrayReadOnlyList_1_Remove_m18452_gshared/* 307*/,
	(methodPointerType)&ArrayReadOnlyList_1_RemoveAt_m18453_gshared/* 308*/,
	(methodPointerType)&ArrayReadOnlyList_1_ReadOnlyError_m18454_gshared/* 309*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18456_gshared/* 310*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18457_gshared/* 311*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0__ctor_m18455_gshared/* 312*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18458_gshared/* 313*/,
	(methodPointerType)&U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18459_gshared/* 314*/,
	(methodPointerType)&Comparer_1_get_Default_m11442_gshared/* 315*/,
	(methodPointerType)&Comparer_1__ctor_m11439_gshared/* 316*/,
	(methodPointerType)&Comparer_1__cctor_m11440_gshared/* 317*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m11441_gshared/* 318*/,
	(methodPointerType)&DefaultComparer__ctor_m11443_gshared/* 319*/,
	(methodPointerType)&DefaultComparer_Compare_m11444_gshared/* 320*/,
	(methodPointerType)&GenericComparer_1__ctor_m18490_gshared/* 321*/,
	(methodPointerType)&GenericComparer_1_Compare_m18491_gshared/* 322*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13344_gshared/* 323*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13346_gshared/* 324*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m13348_gshared/* 325*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m13350_gshared/* 326*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13358_gshared/* 327*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13360_gshared/* 328*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13362_gshared/* 329*/,
	(methodPointerType)&Dictionary_2_get_Count_m13380_gshared/* 330*/,
	(methodPointerType)&Dictionary_2_get_Item_m13382_gshared/* 331*/,
	(methodPointerType)&Dictionary_2_set_Item_m13384_gshared/* 332*/,
	(methodPointerType)&Dictionary_2_get_Keys_m13418_gshared/* 333*/,
	(methodPointerType)&Dictionary_2_get_Values_m13420_gshared/* 334*/,
	(methodPointerType)&Dictionary_2__ctor_m13332_gshared/* 335*/,
	(methodPointerType)&Dictionary_2__ctor_m13334_gshared/* 336*/,
	(methodPointerType)&Dictionary_2__ctor_m13336_gshared/* 337*/,
	(methodPointerType)&Dictionary_2__ctor_m13338_gshared/* 338*/,
	(methodPointerType)&Dictionary_2__ctor_m13340_gshared/* 339*/,
	(methodPointerType)&Dictionary_2__ctor_m13342_gshared/* 340*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m13352_gshared/* 341*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m13354_gshared/* 342*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m13356_gshared/* 343*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13364_gshared/* 344*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13366_gshared/* 345*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13368_gshared/* 346*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13370_gshared/* 347*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m13372_gshared/* 348*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13374_gshared/* 349*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13376_gshared/* 350*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13378_gshared/* 351*/,
	(methodPointerType)&Dictionary_2_Init_m13386_gshared/* 352*/,
	(methodPointerType)&Dictionary_2_InitArrays_m13388_gshared/* 353*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m13390_gshared/* 354*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18951_gshared/* 355*/,
	(methodPointerType)&Dictionary_2_make_pair_m13392_gshared/* 356*/,
	(methodPointerType)&Dictionary_2_pick_key_m13394_gshared/* 357*/,
	(methodPointerType)&Dictionary_2_pick_value_m13396_gshared/* 358*/,
	(methodPointerType)&Dictionary_2_CopyTo_m13398_gshared/* 359*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18952_gshared/* 360*/,
	(methodPointerType)&Dictionary_2_Resize_m13400_gshared/* 361*/,
	(methodPointerType)&Dictionary_2_Add_m13402_gshared/* 362*/,
	(methodPointerType)&Dictionary_2_Clear_m13404_gshared/* 363*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m13406_gshared/* 364*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m13408_gshared/* 365*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m13410_gshared/* 366*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m13412_gshared/* 367*/,
	(methodPointerType)&Dictionary_2_Remove_m13414_gshared/* 368*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m13416_gshared/* 369*/,
	(methodPointerType)&Dictionary_2_ToTKey_m13422_gshared/* 370*/,
	(methodPointerType)&Dictionary_2_ToTValue_m13424_gshared/* 371*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m13426_gshared/* 372*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m13428_gshared/* 373*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m13430_gshared/* 374*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m13550_gshared/* 375*/,
	(methodPointerType)&ShimEnumerator_get_Key_m13551_gshared/* 376*/,
	(methodPointerType)&ShimEnumerator_get_Value_m13552_gshared/* 377*/,
	(methodPointerType)&ShimEnumerator_get_Current_m13553_gshared/* 378*/,
	(methodPointerType)&ShimEnumerator__ctor_m13548_gshared/* 379*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m13549_gshared/* 380*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared/* 381*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared/* 382*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared/* 383*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared/* 384*/,
	(methodPointerType)&Enumerator_get_Current_m13511_gshared/* 385*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m13512_gshared/* 386*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m13513_gshared/* 387*/,
	(methodPointerType)&Enumerator__ctor_m13505_gshared/* 388*/,
	(methodPointerType)&Enumerator_MoveNext_m13510_gshared/* 389*/,
	(methodPointerType)&Enumerator_VerifyState_m13514_gshared/* 390*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m13515_gshared/* 391*/,
	(methodPointerType)&Enumerator_Dispose_m13516_gshared/* 392*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13494_gshared/* 393*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13495_gshared/* 394*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m13496_gshared/* 395*/,
	(methodPointerType)&KeyCollection_get_Count_m13499_gshared/* 396*/,
	(methodPointerType)&KeyCollection__ctor_m13486_gshared/* 397*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13487_gshared/* 398*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13488_gshared/* 399*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13489_gshared/* 400*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13490_gshared/* 401*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13491_gshared/* 402*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m13492_gshared/* 403*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13493_gshared/* 404*/,
	(methodPointerType)&KeyCollection_CopyTo_m13497_gshared/* 405*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m13498_gshared/* 406*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13501_gshared/* 407*/,
	(methodPointerType)&Enumerator_get_Current_m13504_gshared/* 408*/,
	(methodPointerType)&Enumerator__ctor_m13500_gshared/* 409*/,
	(methodPointerType)&Enumerator_Dispose_m13502_gshared/* 410*/,
	(methodPointerType)&Enumerator_MoveNext_m13503_gshared/* 411*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13529_gshared/* 412*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13530_gshared/* 413*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m13531_gshared/* 414*/,
	(methodPointerType)&ValueCollection_get_Count_m13534_gshared/* 415*/,
	(methodPointerType)&ValueCollection__ctor_m13521_gshared/* 416*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13522_gshared/* 417*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13523_gshared/* 418*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13524_gshared/* 419*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13525_gshared/* 420*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13526_gshared/* 421*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m13527_gshared/* 422*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13528_gshared/* 423*/,
	(methodPointerType)&ValueCollection_CopyTo_m13532_gshared/* 424*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m13533_gshared/* 425*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13536_gshared/* 426*/,
	(methodPointerType)&Enumerator_get_Current_m13539_gshared/* 427*/,
	(methodPointerType)&Enumerator__ctor_m13535_gshared/* 428*/,
	(methodPointerType)&Enumerator_Dispose_m13537_gshared/* 429*/,
	(methodPointerType)&Enumerator_MoveNext_m13538_gshared/* 430*/,
	(methodPointerType)&Transform_1__ctor_m13517_gshared/* 431*/,
	(methodPointerType)&Transform_1_Invoke_m13518_gshared/* 432*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13519_gshared/* 433*/,
	(methodPointerType)&Transform_1_EndInvoke_m13520_gshared/* 434*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m11426_gshared/* 435*/,
	(methodPointerType)&EqualityComparer_1__ctor_m11422_gshared/* 436*/,
	(methodPointerType)&EqualityComparer_1__cctor_m11423_gshared/* 437*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m11424_gshared/* 438*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m11425_gshared/* 439*/,
	(methodPointerType)&DefaultComparer__ctor_m11432_gshared/* 440*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m11433_gshared/* 441*/,
	(methodPointerType)&DefaultComparer_Equals_m11434_gshared/* 442*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m18492_gshared/* 443*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18493_gshared/* 444*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18494_gshared/* 445*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m13481_gshared/* 446*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m13482_gshared/* 447*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m13483_gshared/* 448*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m13484_gshared/* 449*/,
	(methodPointerType)&KeyValuePair_2__ctor_m13480_gshared/* 450*/,
	(methodPointerType)&KeyValuePair_2_ToString_m13485_gshared/* 451*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared/* 452*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared/* 453*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared/* 454*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared/* 455*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared/* 456*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m3675_gshared/* 457*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m3676_gshared/* 458*/,
	(methodPointerType)&List_1_get_Capacity_m11339_gshared/* 459*/,
	(methodPointerType)&List_1_set_Capacity_m11341_gshared/* 460*/,
	(methodPointerType)&List_1_get_Count_m3669_gshared/* 461*/,
	(methodPointerType)&List_1_get_Item_m3692_gshared/* 462*/,
	(methodPointerType)&List_1_set_Item_m3693_gshared/* 463*/,
	(methodPointerType)&List_1__ctor_m3449_gshared/* 464*/,
	(methodPointerType)&List_1__ctor_m11276_gshared/* 465*/,
	(methodPointerType)&List_1__cctor_m11278_gshared/* 466*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared/* 467*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m3672_gshared/* 468*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared/* 469*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m3677_gshared/* 470*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m3679_gshared/* 471*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m3680_gshared/* 472*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m3681_gshared/* 473*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m3682_gshared/* 474*/,
	(methodPointerType)&List_1_Add_m3685_gshared/* 475*/,
	(methodPointerType)&List_1_GrowIfNeeded_m11296_gshared/* 476*/,
	(methodPointerType)&List_1_AddCollection_m11298_gshared/* 477*/,
	(methodPointerType)&List_1_AddEnumerable_m11300_gshared/* 478*/,
	(methodPointerType)&List_1_AddRange_m11302_gshared/* 479*/,
	(methodPointerType)&List_1_AsReadOnly_m11304_gshared/* 480*/,
	(methodPointerType)&List_1_Clear_m3678_gshared/* 481*/,
	(methodPointerType)&List_1_Contains_m3686_gshared/* 482*/,
	(methodPointerType)&List_1_CopyTo_m3687_gshared/* 483*/,
	(methodPointerType)&List_1_Find_m11309_gshared/* 484*/,
	(methodPointerType)&List_1_CheckMatch_m11311_gshared/* 485*/,
	(methodPointerType)&List_1_GetIndex_m11313_gshared/* 486*/,
	(methodPointerType)&List_1_GetEnumerator_m11315_gshared/* 487*/,
	(methodPointerType)&List_1_IndexOf_m3690_gshared/* 488*/,
	(methodPointerType)&List_1_Shift_m11318_gshared/* 489*/,
	(methodPointerType)&List_1_CheckIndex_m11320_gshared/* 490*/,
	(methodPointerType)&List_1_Insert_m3691_gshared/* 491*/,
	(methodPointerType)&List_1_CheckCollection_m11323_gshared/* 492*/,
	(methodPointerType)&List_1_Remove_m3688_gshared/* 493*/,
	(methodPointerType)&List_1_RemoveAll_m11326_gshared/* 494*/,
	(methodPointerType)&List_1_RemoveAt_m3683_gshared/* 495*/,
	(methodPointerType)&List_1_Reverse_m11329_gshared/* 496*/,
	(methodPointerType)&List_1_Sort_m11331_gshared/* 497*/,
	(methodPointerType)&List_1_Sort_m11333_gshared/* 498*/,
	(methodPointerType)&List_1_ToArray_m11335_gshared/* 499*/,
	(methodPointerType)&List_1_TrimExcess_m11337_gshared/* 500*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared/* 501*/,
	(methodPointerType)&Enumerator_get_Current_m11355_gshared/* 502*/,
	(methodPointerType)&Enumerator__ctor_m11350_gshared/* 503*/,
	(methodPointerType)&Enumerator_Dispose_m11352_gshared/* 504*/,
	(methodPointerType)&Enumerator_VerifyState_m11353_gshared/* 505*/,
	(methodPointerType)&Enumerator_MoveNext_m11354_gshared/* 506*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11387_gshared/* 507*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m11395_gshared/* 508*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m11396_gshared/* 509*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m11397_gshared/* 510*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m11398_gshared/* 511*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m11399_gshared/* 512*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m11400_gshared/* 513*/,
	(methodPointerType)&Collection_1_get_Count_m11413_gshared/* 514*/,
	(methodPointerType)&Collection_1_get_Item_m11414_gshared/* 515*/,
	(methodPointerType)&Collection_1_set_Item_m11415_gshared/* 516*/,
	(methodPointerType)&Collection_1__ctor_m11386_gshared/* 517*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m11388_gshared/* 518*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m11389_gshared/* 519*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m11390_gshared/* 520*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m11391_gshared/* 521*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m11392_gshared/* 522*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m11393_gshared/* 523*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m11394_gshared/* 524*/,
	(methodPointerType)&Collection_1_Add_m11401_gshared/* 525*/,
	(methodPointerType)&Collection_1_Clear_m11402_gshared/* 526*/,
	(methodPointerType)&Collection_1_ClearItems_m11403_gshared/* 527*/,
	(methodPointerType)&Collection_1_Contains_m11404_gshared/* 528*/,
	(methodPointerType)&Collection_1_CopyTo_m11405_gshared/* 529*/,
	(methodPointerType)&Collection_1_GetEnumerator_m11406_gshared/* 530*/,
	(methodPointerType)&Collection_1_IndexOf_m11407_gshared/* 531*/,
	(methodPointerType)&Collection_1_Insert_m11408_gshared/* 532*/,
	(methodPointerType)&Collection_1_InsertItem_m11409_gshared/* 533*/,
	(methodPointerType)&Collection_1_Remove_m11410_gshared/* 534*/,
	(methodPointerType)&Collection_1_RemoveAt_m11411_gshared/* 535*/,
	(methodPointerType)&Collection_1_RemoveItem_m11412_gshared/* 536*/,
	(methodPointerType)&Collection_1_SetItem_m11416_gshared/* 537*/,
	(methodPointerType)&Collection_1_IsValidItem_m11417_gshared/* 538*/,
	(methodPointerType)&Collection_1_ConvertItem_m11418_gshared/* 539*/,
	(methodPointerType)&Collection_1_CheckWritable_m11419_gshared/* 540*/,
	(methodPointerType)&Collection_1_IsSynchronized_m11420_gshared/* 541*/,
	(methodPointerType)&Collection_1_IsFixedSize_m11421_gshared/* 542*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11362_gshared/* 543*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11363_gshared/* 544*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11364_gshared/* 545*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11374_gshared/* 546*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11375_gshared/* 547*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11376_gshared/* 548*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11377_gshared/* 549*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m11378_gshared/* 550*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m11379_gshared/* 551*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m11384_gshared/* 552*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m11385_gshared/* 553*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m11356_gshared/* 554*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11357_gshared/* 555*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11358_gshared/* 556*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11359_gshared/* 557*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11360_gshared/* 558*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11361_gshared/* 559*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11365_gshared/* 560*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11366_gshared/* 561*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m11367_gshared/* 562*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m11368_gshared/* 563*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m11369_gshared/* 564*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11370_gshared/* 565*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m11371_gshared/* 566*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m11372_gshared/* 567*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11373_gshared/* 568*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m11380_gshared/* 569*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m11381_gshared/* 570*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m11382_gshared/* 571*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m11383_gshared/* 572*/,
	(methodPointerType)&MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m19381_gshared/* 573*/,
	(methodPointerType)&MonoProperty_StaticGetterAdapterFrame_TisObject_t_m19382_gshared/* 574*/,
	(methodPointerType)&Getter_2__ctor_m18550_gshared/* 575*/,
	(methodPointerType)&Getter_2_Invoke_m18551_gshared/* 576*/,
	(methodPointerType)&Getter_2_BeginInvoke_m18552_gshared/* 577*/,
	(methodPointerType)&Getter_2_EndInvoke_m18553_gshared/* 578*/,
	(methodPointerType)&StaticGetter_1__ctor_m18554_gshared/* 579*/,
	(methodPointerType)&StaticGetter_1_Invoke_m18555_gshared/* 580*/,
	(methodPointerType)&StaticGetter_1_BeginInvoke_m18556_gshared/* 581*/,
	(methodPointerType)&StaticGetter_1_EndInvoke_m18557_gshared/* 582*/,
	(methodPointerType)&Activator_CreateInstance_TisObject_t_m18822_gshared/* 583*/,
	(methodPointerType)&Action_1__ctor_m13652_gshared/* 584*/,
	(methodPointerType)&Action_1_Invoke_m13654_gshared/* 585*/,
	(methodPointerType)&Action_1_BeginInvoke_m13656_gshared/* 586*/,
	(methodPointerType)&Action_1_EndInvoke_m13658_gshared/* 587*/,
	(methodPointerType)&Comparison_1__ctor_m11465_gshared/* 588*/,
	(methodPointerType)&Comparison_1_Invoke_m11466_gshared/* 589*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m11467_gshared/* 590*/,
	(methodPointerType)&Comparison_1_EndInvoke_m11468_gshared/* 591*/,
	(methodPointerType)&Converter_2__ctor_m18435_gshared/* 592*/,
	(methodPointerType)&Converter_2_Invoke_m18436_gshared/* 593*/,
	(methodPointerType)&Converter_2_BeginInvoke_m18437_gshared/* 594*/,
	(methodPointerType)&Converter_2_EndInvoke_m18438_gshared/* 595*/,
	(methodPointerType)&Predicate_1__ctor_m11435_gshared/* 596*/,
	(methodPointerType)&Predicate_1_Invoke_m11436_gshared/* 597*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m11437_gshared/* 598*/,
	(methodPointerType)&Predicate_1_EndInvoke_m11438_gshared/* 599*/,
	(methodPointerType)&Func_2__ctor_m11608_gshared/* 600*/,
	(methodPointerType)&Comparison_1__ctor_m1454_gshared/* 601*/,
	(methodPointerType)&List_1_Sort_m1460_gshared/* 602*/,
	(methodPointerType)&List_1__ctor_m1505_gshared/* 603*/,
	(methodPointerType)&Dictionary_2__ctor_m12701_gshared/* 604*/,
	(methodPointerType)&Dictionary_2_get_Values_m12788_gshared/* 605*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m12861_gshared/* 606*/,
	(methodPointerType)&Enumerator_get_Current_m12867_gshared/* 607*/,
	(methodPointerType)&Enumerator_MoveNext_m12866_gshared/* 608*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m12795_gshared/* 609*/,
	(methodPointerType)&Enumerator_get_Current_m12839_gshared/* 610*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m12806_gshared/* 611*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m12804_gshared/* 612*/,
	(methodPointerType)&Enumerator_MoveNext_m12838_gshared/* 613*/,
	(methodPointerType)&KeyValuePair_2_ToString_m12808_gshared/* 614*/,
	(methodPointerType)&Comparison_1__ctor_m1581_gshared/* 615*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t323_m1575_gshared/* 616*/,
	(methodPointerType)&UnityEvent_1__ctor_m1586_gshared/* 617*/,
	(methodPointerType)&UnityEvent_1_Invoke_m1588_gshared/* 618*/,
	(methodPointerType)&UnityEvent_1_AddListener_m1589_gshared/* 619*/,
	(methodPointerType)&TweenRunner_1__ctor_m1615_gshared/* 620*/,
	(methodPointerType)&TweenRunner_1_Init_m1616_gshared/* 621*/,
	(methodPointerType)&UnityAction_1__ctor_m1644_gshared/* 622*/,
	(methodPointerType)&TweenRunner_1_StartTween_m1645_gshared/* 623*/,
	(methodPointerType)&List_1_get_Capacity_m1649_gshared/* 624*/,
	(methodPointerType)&List_1_set_Capacity_m1650_gshared/* 625*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisInt32_t54_m1677_gshared/* 626*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisByte_t367_m1679_gshared/* 627*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSingle_t85_m1681_gshared/* 628*/,
	(methodPointerType)&List_1__ctor_m1744_gshared/* 629*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisUInt16_t371_m1739_gshared/* 630*/,
	(methodPointerType)&List_1_ToArray_m1801_gshared/* 631*/,
	(methodPointerType)&UnityEvent_1__ctor_m1840_gshared/* 632*/,
	(methodPointerType)&UnityEvent_1_Invoke_m1846_gshared/* 633*/,
	(methodPointerType)&UnityEvent_1__ctor_m1851_gshared/* 634*/,
	(methodPointerType)&UnityAction_1__ctor_m1852_gshared/* 635*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m1853_gshared/* 636*/,
	(methodPointerType)&UnityEvent_1_AddListener_m1854_gshared/* 637*/,
	(methodPointerType)&UnityEvent_1_Invoke_m1860_gshared/* 638*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisNavigation_t232_m1876_gshared/* 639*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisColorBlock_t175_m1878_gshared/* 640*/,
	(methodPointerType)&SetPropertyUtility_SetStruct_TisSpriteState_t252_m1879_gshared/* 641*/,
	(methodPointerType)&UnityEvent_1__ctor_m14597_gshared/* 642*/,
	(methodPointerType)&UnityEvent_1_Invoke_m14606_gshared/* 643*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisInt32_t54_m1937_gshared/* 644*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisVector2_t53_m1939_gshared/* 645*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisSingle_t85_m1946_gshared/* 646*/,
	(methodPointerType)&LayoutGroup_SetProperty_TisByte_t367_m1948_gshared/* 647*/,
	(methodPointerType)&Func_2__ctor_m14814_gshared/* 648*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2118_gshared/* 649*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2119_gshared/* 650*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2127_gshared/* 651*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2128_gshared/* 652*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m2129_gshared/* 653*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m2130_gshared/* 654*/,
	(methodPointerType)&UnityEvent_1_FindMethod_Impl_m14602_gshared/* 655*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m14603_gshared/* 656*/,
	(methodPointerType)&List_1__ctor_m3423_gshared/* 657*/,
	(methodPointerType)&List_1__ctor_m3424_gshared/* 658*/,
	(methodPointerType)&List_1__ctor_m3425_gshared/* 659*/,
	(methodPointerType)&Dictionary_2__ctor_m16542_gshared/* 660*/,
	(methodPointerType)&Dictionary_2__ctor_m17276_gshared/* 661*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m3517_gshared/* 662*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m3518_gshared/* 663*/,
	(methodPointerType)&CachedInvokableCall_1__ctor_m17792_gshared/* 664*/,
	(methodPointerType)&Dictionary_2__ctor_m17992_gshared/* 665*/,
	(methodPointerType)&Dictionary_2__ctor_m13097_gshared/* 666*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t54_m4820_gshared/* 667*/,
	(methodPointerType)&GenericComparer_1__ctor_m10414_gshared/* 668*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m10415_gshared/* 669*/,
	(methodPointerType)&GenericComparer_1__ctor_m10416_gshared/* 670*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m10417_gshared/* 671*/,
	(methodPointerType)&Nullable_1__ctor_m10418_gshared/* 672*/,
	(methodPointerType)&Nullable_1_get_HasValue_m10419_gshared/* 673*/,
	(methodPointerType)&Nullable_1_get_Value_m10420_gshared/* 674*/,
	(methodPointerType)&GenericComparer_1__ctor_m10421_gshared/* 675*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m10422_gshared/* 676*/,
	(methodPointerType)&GenericComparer_1__ctor_m10423_gshared/* 677*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m10424_gshared/* 678*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt32_t54_m18782_gshared/* 679*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt32_t54_m18783_gshared/* 680*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt32_t54_m18784_gshared/* 681*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt32_t54_m18785_gshared/* 682*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt32_t54_m18786_gshared/* 683*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt32_t54_m18787_gshared/* 684*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt32_t54_m18788_gshared/* 685*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt32_t54_m18790_gshared/* 686*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt32_t54_m18791_gshared/* 687*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDouble_t752_m18793_gshared/* 688*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDouble_t752_m18794_gshared/* 689*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDouble_t752_m18795_gshared/* 690*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDouble_t752_m18796_gshared/* 691*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDouble_t752_m18797_gshared/* 692*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDouble_t752_m18798_gshared/* 693*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDouble_t752_m18799_gshared/* 694*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDouble_t752_m18801_gshared/* 695*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDouble_t752_m18802_gshared/* 696*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt16_t371_m18804_gshared/* 697*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt16_t371_m18805_gshared/* 698*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt16_t371_m18806_gshared/* 699*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt16_t371_m18807_gshared/* 700*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt16_t371_m18808_gshared/* 701*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt16_t371_m18809_gshared/* 702*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt16_t371_m18810_gshared/* 703*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt16_t371_m18812_gshared/* 704*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t371_m18813_gshared/* 705*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastResult_t137_m18828_gshared/* 706*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastResult_t137_m18829_gshared/* 707*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastResult_t137_m18830_gshared/* 708*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastResult_t137_m18831_gshared/* 709*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastResult_t137_m18832_gshared/* 710*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastResult_t137_m18833_gshared/* 711*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastResult_t137_m18834_gshared/* 712*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastResult_t137_m18836_gshared/* 713*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastResult_t137_m18837_gshared/* 714*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t137_m18839_gshared/* 715*/,
	(methodPointerType)&Array_Resize_TisRaycastResult_t137_m18838_gshared/* 716*/,
	(methodPointerType)&Array_IndexOf_TisRaycastResult_t137_m18840_gshared/* 717*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t137_m18842_gshared/* 718*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t137_TisRaycastResult_t137_m18841_gshared/* 719*/,
	(methodPointerType)&Array_get_swapper_TisRaycastResult_t137_m18843_gshared/* 720*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t137_TisRaycastResult_t137_m18844_gshared/* 721*/,
	(methodPointerType)&Array_compare_TisRaycastResult_t137_m18845_gshared/* 722*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t137_TisRaycastResult_t137_m18846_gshared/* 723*/,
	(methodPointerType)&Array_Sort_TisRaycastResult_t137_m18848_gshared/* 724*/,
	(methodPointerType)&Array_qsort_TisRaycastResult_t137_m18847_gshared/* 725*/,
	(methodPointerType)&Array_swap_TisRaycastResult_t137_m18849_gshared/* 726*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2536_m18853_gshared/* 727*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2536_m18854_gshared/* 728*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2536_m18855_gshared/* 729*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2536_m18856_gshared/* 730*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2536_m18857_gshared/* 731*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2536_m18858_gshared/* 732*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2536_m18859_gshared/* 733*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2536_m18861_gshared/* 734*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2536_m18862_gshared/* 735*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisLink_t1459_m18864_gshared/* 736*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisLink_t1459_m18865_gshared/* 737*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisLink_t1459_m18866_gshared/* 738*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisLink_t1459_m18867_gshared/* 739*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisLink_t1459_m18868_gshared/* 740*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisLink_t1459_m18869_gshared/* 741*/,
	(methodPointerType)&Array_InternalArray__Insert_TisLink_t1459_m18870_gshared/* 742*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisLink_t1459_m18872_gshared/* 743*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t1459_m18873_gshared/* 744*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m18875_gshared/* 745*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m18874_gshared/* 746*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m18876_gshared/* 747*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18878_gshared/* 748*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18877_gshared/* 749*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDictionaryEntry_t1147_m18880_gshared/* 750*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDictionaryEntry_t1147_m18881_gshared/* 751*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDictionaryEntry_t1147_m18882_gshared/* 752*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDictionaryEntry_t1147_m18883_gshared/* 753*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDictionaryEntry_t1147_m18884_gshared/* 754*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDictionaryEntry_t1147_m18885_gshared/* 755*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDictionaryEntry_t1147_m18886_gshared/* 756*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDictionaryEntry_t1147_m18888_gshared/* 757*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDictionaryEntry_t1147_m18889_gshared/* 758*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18890_gshared/* 759*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2536_m18892_gshared/* 760*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2536_TisObject_t_m18891_gshared/* 761*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2536_TisKeyValuePair_2_t2536_m18893_gshared/* 762*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit2D_t349_m18895_gshared/* 763*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit2D_t349_m18896_gshared/* 764*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit2D_t349_m18897_gshared/* 765*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit2D_t349_m18898_gshared/* 766*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit2D_t349_m18899_gshared/* 767*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit2D_t349_m18900_gshared/* 768*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit2D_t349_m18901_gshared/* 769*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit2D_t349_m18903_gshared/* 770*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit2D_t349_m18904_gshared/* 771*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisRaycastHit_t323_m18906_gshared/* 772*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisRaycastHit_t323_m18907_gshared/* 773*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisRaycastHit_t323_m18908_gshared/* 774*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisRaycastHit_t323_m18909_gshared/* 775*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisRaycastHit_t323_m18910_gshared/* 776*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisRaycastHit_t323_m18911_gshared/* 777*/,
	(methodPointerType)&Array_InternalArray__Insert_TisRaycastHit_t323_m18912_gshared/* 778*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisRaycastHit_t323_m18914_gshared/* 779*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisRaycastHit_t323_m18915_gshared/* 780*/,
	(methodPointerType)&Array_Sort_TisRaycastHit_t323_m18916_gshared/* 781*/,
	(methodPointerType)&Array_qsort_TisRaycastHit_t323_m18917_gshared/* 782*/,
	(methodPointerType)&Array_swap_TisRaycastHit_t323_m18918_gshared/* 783*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisColor_t163_m18919_gshared/* 784*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2567_m18921_gshared/* 785*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2567_m18922_gshared/* 786*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2567_m18923_gshared/* 787*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2567_m18924_gshared/* 788*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2567_m18925_gshared/* 789*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2567_m18926_gshared/* 790*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2567_m18927_gshared/* 791*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2567_m18929_gshared/* 792*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2567_m18930_gshared/* 793*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m18932_gshared/* 794*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m18931_gshared/* 795*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m18934_gshared/* 796*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m18933_gshared/* 797*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m18935_gshared/* 798*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18936_gshared/* 799*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2567_m18938_gshared/* 800*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2567_TisObject_t_m18937_gshared/* 801*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2567_TisKeyValuePair_2_t2567_m18939_gshared/* 802*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2593_m18941_gshared/* 803*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2593_m18942_gshared/* 804*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2593_m18943_gshared/* 805*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2593_m18944_gshared/* 806*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2593_m18945_gshared/* 807*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2593_m18946_gshared/* 808*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2593_m18947_gshared/* 809*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2593_m18949_gshared/* 810*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2593_m18950_gshared/* 811*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m18953_gshared/* 812*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2593_m18955_gshared/* 813*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisObject_t_m18954_gshared/* 814*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisKeyValuePair_2_t2593_m18956_gshared/* 815*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t225_m18958_gshared/* 816*/,
	(methodPointerType)&Array_Resize_TisUIVertex_t225_m18957_gshared/* 817*/,
	(methodPointerType)&Array_IndexOf_TisUIVertex_t225_m18959_gshared/* 818*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t225_m18961_gshared/* 819*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t225_TisUIVertex_t225_m18960_gshared/* 820*/,
	(methodPointerType)&Array_get_swapper_TisUIVertex_t225_m18962_gshared/* 821*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t225_TisUIVertex_t225_m18963_gshared/* 822*/,
	(methodPointerType)&Array_compare_TisUIVertex_t225_m18964_gshared/* 823*/,
	(methodPointerType)&Array_swap_TisUIVertex_t225_TisUIVertex_t225_m18965_gshared/* 824*/,
	(methodPointerType)&Array_Sort_TisUIVertex_t225_m18967_gshared/* 825*/,
	(methodPointerType)&Array_qsort_TisUIVertex_t225_m18966_gshared/* 826*/,
	(methodPointerType)&Array_swap_TisUIVertex_t225_m18968_gshared/* 827*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector2_t53_m18970_gshared/* 828*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector2_t53_m18971_gshared/* 829*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector2_t53_m18972_gshared/* 830*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector2_t53_m18973_gshared/* 831*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector2_t53_m18974_gshared/* 832*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector2_t53_m18975_gshared/* 833*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector2_t53_m18976_gshared/* 834*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector2_t53_m18978_gshared/* 835*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector2_t53_m18979_gshared/* 836*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUILineInfo_t375_m18981_gshared/* 837*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUILineInfo_t375_m18982_gshared/* 838*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUILineInfo_t375_m18983_gshared/* 839*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUILineInfo_t375_m18984_gshared/* 840*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUILineInfo_t375_m18985_gshared/* 841*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUILineInfo_t375_m18986_gshared/* 842*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUILineInfo_t375_m18987_gshared/* 843*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUILineInfo_t375_m18989_gshared/* 844*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUILineInfo_t375_m18990_gshared/* 845*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUICharInfo_t377_m18992_gshared/* 846*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUICharInfo_t377_m18993_gshared/* 847*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUICharInfo_t377_m18994_gshared/* 848*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUICharInfo_t377_m18995_gshared/* 849*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUICharInfo_t377_m18996_gshared/* 850*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUICharInfo_t377_m18997_gshared/* 851*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUICharInfo_t377_m18998_gshared/* 852*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUICharInfo_t377_m19000_gshared/* 853*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUICharInfo_t377_m19001_gshared/* 854*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t85_m19002_gshared/* 855*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisVector3_t138_m19004_gshared/* 856*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisVector3_t138_m19005_gshared/* 857*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisVector3_t138_m19006_gshared/* 858*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisVector3_t138_m19007_gshared/* 859*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisVector3_t138_m19008_gshared/* 860*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisVector3_t138_m19009_gshared/* 861*/,
	(methodPointerType)&Array_InternalArray__Insert_TisVector3_t138_m19010_gshared/* 862*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisVector3_t138_m19012_gshared/* 863*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisVector3_t138_m19013_gshared/* 864*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t53_m19014_gshared/* 865*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisByte_t367_m19015_gshared/* 866*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcAchievementData_t650_m19021_gshared/* 867*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcAchievementData_t650_m19022_gshared/* 868*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t650_m19023_gshared/* 869*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t650_m19024_gshared/* 870*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t650_m19025_gshared/* 871*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcAchievementData_t650_m19026_gshared/* 872*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcAchievementData_t650_m19027_gshared/* 873*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcAchievementData_t650_m19029_gshared/* 874*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t650_m19030_gshared/* 875*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisGcScoreData_t651_m19032_gshared/* 876*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisGcScoreData_t651_m19033_gshared/* 877*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisGcScoreData_t651_m19034_gshared/* 878*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t651_m19035_gshared/* 879*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisGcScoreData_t651_m19036_gshared/* 880*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisGcScoreData_t651_m19037_gshared/* 881*/,
	(methodPointerType)&Array_InternalArray__Insert_TisGcScoreData_t651_m19038_gshared/* 882*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisGcScoreData_t651_m19040_gshared/* 883*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t651_m19041_gshared/* 884*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisByte_t367_m19043_gshared/* 885*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisByte_t367_m19044_gshared/* 886*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisByte_t367_m19045_gshared/* 887*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisByte_t367_m19046_gshared/* 888*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisByte_t367_m19047_gshared/* 889*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisByte_t367_m19048_gshared/* 890*/,
	(methodPointerType)&Array_InternalArray__Insert_TisByte_t367_m19049_gshared/* 891*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisByte_t367_m19051_gshared/* 892*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisByte_t367_m19052_gshared/* 893*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisIntPtr_t_m19054_gshared/* 894*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisIntPtr_t_m19055_gshared/* 895*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisIntPtr_t_m19056_gshared/* 896*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisIntPtr_t_m19057_gshared/* 897*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisIntPtr_t_m19058_gshared/* 898*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisIntPtr_t_m19059_gshared/* 899*/,
	(methodPointerType)&Array_InternalArray__Insert_TisIntPtr_t_m19060_gshared/* 900*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisIntPtr_t_m19062_gshared/* 901*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisIntPtr_t_m19063_gshared/* 902*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSingle_t85_m19065_gshared/* 903*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSingle_t85_m19066_gshared/* 904*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSingle_t85_m19067_gshared/* 905*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSingle_t85_m19068_gshared/* 906*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSingle_t85_m19069_gshared/* 907*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSingle_t85_m19070_gshared/* 908*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSingle_t85_m19071_gshared/* 909*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSingle_t85_m19073_gshared/* 910*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSingle_t85_m19074_gshared/* 911*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyframe_t580_m19076_gshared/* 912*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyframe_t580_m19077_gshared/* 913*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyframe_t580_m19078_gshared/* 914*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyframe_t580_m19079_gshared/* 915*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyframe_t580_m19080_gshared/* 916*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyframe_t580_m19081_gshared/* 917*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyframe_t580_m19082_gshared/* 918*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyframe_t580_m19084_gshared/* 919*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyframe_t580_m19085_gshared/* 920*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t377_m19087_gshared/* 921*/,
	(methodPointerType)&Array_Resize_TisUICharInfo_t377_m19086_gshared/* 922*/,
	(methodPointerType)&Array_IndexOf_TisUICharInfo_t377_m19088_gshared/* 923*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t377_m19090_gshared/* 924*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t377_TisUICharInfo_t377_m19089_gshared/* 925*/,
	(methodPointerType)&Array_get_swapper_TisUICharInfo_t377_m19091_gshared/* 926*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t377_TisUICharInfo_t377_m19092_gshared/* 927*/,
	(methodPointerType)&Array_compare_TisUICharInfo_t377_m19093_gshared/* 928*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t377_TisUICharInfo_t377_m19094_gshared/* 929*/,
	(methodPointerType)&Array_Sort_TisUICharInfo_t377_m19096_gshared/* 930*/,
	(methodPointerType)&Array_qsort_TisUICharInfo_t377_m19095_gshared/* 931*/,
	(methodPointerType)&Array_swap_TisUICharInfo_t377_m19097_gshared/* 932*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t375_m19099_gshared/* 933*/,
	(methodPointerType)&Array_Resize_TisUILineInfo_t375_m19098_gshared/* 934*/,
	(methodPointerType)&Array_IndexOf_TisUILineInfo_t375_m19100_gshared/* 935*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t375_m19102_gshared/* 936*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t375_TisUILineInfo_t375_m19101_gshared/* 937*/,
	(methodPointerType)&Array_get_swapper_TisUILineInfo_t375_m19103_gshared/* 938*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t375_TisUILineInfo_t375_m19104_gshared/* 939*/,
	(methodPointerType)&Array_compare_TisUILineInfo_t375_m19105_gshared/* 940*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t375_TisUILineInfo_t375_m19106_gshared/* 941*/,
	(methodPointerType)&Array_Sort_TisUILineInfo_t375_m19108_gshared/* 942*/,
	(methodPointerType)&Array_qsort_TisUILineInfo_t375_m19107_gshared/* 943*/,
	(methodPointerType)&Array_swap_TisUILineInfo_t375_m19109_gshared/* 944*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2780_m19111_gshared/* 945*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2780_m19112_gshared/* 946*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2780_m19113_gshared/* 947*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2780_m19114_gshared/* 948*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2780_m19115_gshared/* 949*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2780_m19116_gshared/* 950*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2780_m19117_gshared/* 951*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2780_m19119_gshared/* 952*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2780_m19120_gshared/* 953*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt64_t753_m19122_gshared/* 954*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt64_t753_m19123_gshared/* 955*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt64_t753_m19124_gshared/* 956*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt64_t753_m19125_gshared/* 957*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt64_t753_m19126_gshared/* 958*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt64_t753_m19127_gshared/* 959*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt64_t753_m19128_gshared/* 960*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt64_t753_m19130_gshared/* 961*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt64_t753_m19131_gshared/* 962*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19133_gshared/* 963*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19132_gshared/* 964*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt64_t753_m19135_gshared/* 965*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t753_TisObject_t_m19134_gshared/* 966*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt64_t753_TisInt64_t753_m19136_gshared/* 967*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19137_gshared/* 968*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2780_m19139_gshared/* 969*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2780_TisObject_t_m19138_gshared/* 970*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2780_TisKeyValuePair_2_t2780_m19140_gshared/* 971*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2818_m19142_gshared/* 972*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2818_m19143_gshared/* 973*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2818_m19144_gshared/* 974*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2818_m19145_gshared/* 975*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2818_m19146_gshared/* 976*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2818_m19147_gshared/* 977*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2818_m19148_gshared/* 978*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2818_m19150_gshared/* 979*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2818_m19151_gshared/* 980*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt64_t756_m19153_gshared/* 981*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt64_t756_m19154_gshared/* 982*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt64_t756_m19155_gshared/* 983*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt64_t756_m19156_gshared/* 984*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt64_t756_m19157_gshared/* 985*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt64_t756_m19158_gshared/* 986*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt64_t756_m19159_gshared/* 987*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt64_t756_m19161_gshared/* 988*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt64_t756_m19162_gshared/* 989*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisUInt64_t756_m19164_gshared/* 990*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t756_TisObject_t_m19163_gshared/* 991*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisUInt64_t756_TisUInt64_t756_m19165_gshared/* 992*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19167_gshared/* 993*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19166_gshared/* 994*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19168_gshared/* 995*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2818_m19170_gshared/* 996*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2818_TisObject_t_m19169_gshared/* 997*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2818_TisKeyValuePair_2_t2818_m19171_gshared/* 998*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2839_m19173_gshared/* 999*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2839_m19174_gshared/* 1000*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2839_m19175_gshared/* 1001*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2839_m19176_gshared/* 1002*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2839_m19177_gshared/* 1003*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2839_m19178_gshared/* 1004*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2839_m19179_gshared/* 1005*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2839_m19181_gshared/* 1006*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2839_m19182_gshared/* 1007*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19184_gshared/* 1008*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19183_gshared/* 1009*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2593_m19186_gshared/* 1010*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisObject_t_m19185_gshared/* 1011*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2593_TisKeyValuePair_2_t2593_m19187_gshared/* 1012*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19188_gshared/* 1013*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2839_m19190_gshared/* 1014*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2839_TisObject_t_m19189_gshared/* 1015*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2839_TisKeyValuePair_2_t2839_m19191_gshared/* 1016*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisParameterModifier_t1584_m19193_gshared/* 1017*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisParameterModifier_t1584_m19194_gshared/* 1018*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisParameterModifier_t1584_m19195_gshared/* 1019*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisParameterModifier_t1584_m19196_gshared/* 1020*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisParameterModifier_t1584_m19197_gshared/* 1021*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisParameterModifier_t1584_m19198_gshared/* 1022*/,
	(methodPointerType)&Array_InternalArray__Insert_TisParameterModifier_t1584_m19199_gshared/* 1023*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisParameterModifier_t1584_m19201_gshared/* 1024*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisParameterModifier_t1584_m19202_gshared/* 1025*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisHitInfo_t665_m19204_gshared/* 1026*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisHitInfo_t665_m19205_gshared/* 1027*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisHitInfo_t665_m19206_gshared/* 1028*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisHitInfo_t665_m19207_gshared/* 1029*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisHitInfo_t665_m19208_gshared/* 1030*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisHitInfo_t665_m19209_gshared/* 1031*/,
	(methodPointerType)&Array_InternalArray__Insert_TisHitInfo_t665_m19210_gshared/* 1032*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisHitInfo_t665_m19212_gshared/* 1033*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisHitInfo_t665_m19213_gshared/* 1034*/,
	(methodPointerType)&BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t54_m19214_gshared/* 1035*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2933_m19216_gshared/* 1036*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2933_m19217_gshared/* 1037*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2933_m19218_gshared/* 1038*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2933_m19219_gshared/* 1039*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2933_m19220_gshared/* 1040*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2933_m19221_gshared/* 1041*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2933_m19222_gshared/* 1042*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2933_m19224_gshared/* 1043*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2933_m19225_gshared/* 1044*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m19227_gshared/* 1045*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m19226_gshared/* 1046*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisByte_t367_m19229_gshared/* 1047*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t367_TisObject_t_m19228_gshared/* 1048*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisByte_t367_TisByte_t367_m19230_gshared/* 1049*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19231_gshared/* 1050*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2933_m19233_gshared/* 1051*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2933_TisObject_t_m19232_gshared/* 1052*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2933_TisKeyValuePair_2_t2933_m19234_gshared/* 1053*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisX509ChainStatus_t1045_m19236_gshared/* 1054*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisX509ChainStatus_t1045_m19237_gshared/* 1055*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisX509ChainStatus_t1045_m19238_gshared/* 1056*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisX509ChainStatus_t1045_m19239_gshared/* 1057*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisX509ChainStatus_t1045_m19240_gshared/* 1058*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisX509ChainStatus_t1045_m19241_gshared/* 1059*/,
	(methodPointerType)&Array_InternalArray__Insert_TisX509ChainStatus_t1045_m19242_gshared/* 1060*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisX509ChainStatus_t1045_m19244_gshared/* 1061*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisX509ChainStatus_t1045_m19245_gshared/* 1062*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisKeyValuePair_2_t2956_m19247_gshared/* 1063*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t2956_m19248_gshared/* 1064*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t2956_m19249_gshared/* 1065*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t2956_m19250_gshared/* 1066*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t2956_m19251_gshared/* 1067*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisKeyValuePair_2_t2956_m19252_gshared/* 1068*/,
	(methodPointerType)&Array_InternalArray__Insert_TisKeyValuePair_2_t2956_m19253_gshared/* 1069*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisKeyValuePair_2_t2956_m19255_gshared/* 1070*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t2956_m19256_gshared/* 1071*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisInt32_t54_m19258_gshared/* 1072*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisObject_t_m19257_gshared/* 1073*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisInt32_t54_TisInt32_t54_m19259_gshared/* 1074*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1147_TisDictionaryEntry_t1147_m19260_gshared/* 1075*/,
	(methodPointerType)&Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t2956_m19262_gshared/* 1076*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2956_TisObject_t_m19261_gshared/* 1077*/,
	(methodPointerType)&Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t2956_TisKeyValuePair_2_t2956_m19263_gshared/* 1078*/,
	(methodPointerType)&Array_BinarySearch_TisInt32_t54_m19264_gshared/* 1079*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisMark_t1099_m19266_gshared/* 1080*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisMark_t1099_m19267_gshared/* 1081*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisMark_t1099_m19268_gshared/* 1082*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisMark_t1099_m19269_gshared/* 1083*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisMark_t1099_m19270_gshared/* 1084*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisMark_t1099_m19271_gshared/* 1085*/,
	(methodPointerType)&Array_InternalArray__Insert_TisMark_t1099_m19272_gshared/* 1086*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisMark_t1099_m19274_gshared/* 1087*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisMark_t1099_m19275_gshared/* 1088*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUriScheme_t1135_m19277_gshared/* 1089*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUriScheme_t1135_m19278_gshared/* 1090*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUriScheme_t1135_m19279_gshared/* 1091*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUriScheme_t1135_m19280_gshared/* 1092*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUriScheme_t1135_m19281_gshared/* 1093*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUriScheme_t1135_m19282_gshared/* 1094*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUriScheme_t1135_m19283_gshared/* 1095*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUriScheme_t1135_m19285_gshared/* 1096*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUriScheme_t1135_m19286_gshared/* 1097*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisUInt32_t744_m19288_gshared/* 1098*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisUInt32_t744_m19289_gshared/* 1099*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisUInt32_t744_m19290_gshared/* 1100*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisUInt32_t744_m19291_gshared/* 1101*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisUInt32_t744_m19292_gshared/* 1102*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisUInt32_t744_m19293_gshared/* 1103*/,
	(methodPointerType)&Array_InternalArray__Insert_TisUInt32_t744_m19294_gshared/* 1104*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisUInt32_t744_m19296_gshared/* 1105*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt32_t744_m19297_gshared/* 1106*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisInt16_t448_m19299_gshared/* 1107*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisInt16_t448_m19300_gshared/* 1108*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisInt16_t448_m19301_gshared/* 1109*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisInt16_t448_m19302_gshared/* 1110*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisInt16_t448_m19303_gshared/* 1111*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisInt16_t448_m19304_gshared/* 1112*/,
	(methodPointerType)&Array_InternalArray__Insert_TisInt16_t448_m19305_gshared/* 1113*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisInt16_t448_m19307_gshared/* 1114*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisInt16_t448_m19308_gshared/* 1115*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSByte_t73_m19310_gshared/* 1116*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSByte_t73_m19311_gshared/* 1117*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSByte_t73_m19312_gshared/* 1118*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSByte_t73_m19313_gshared/* 1119*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSByte_t73_m19314_gshared/* 1120*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSByte_t73_m19315_gshared/* 1121*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSByte_t73_m19316_gshared/* 1122*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSByte_t73_m19318_gshared/* 1123*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSByte_t73_m19319_gshared/* 1124*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTableRange_t1392_m19349_gshared/* 1125*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTableRange_t1392_m19350_gshared/* 1126*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTableRange_t1392_m19351_gshared/* 1127*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTableRange_t1392_m19352_gshared/* 1128*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTableRange_t1392_m19353_gshared/* 1129*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTableRange_t1392_m19354_gshared/* 1130*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTableRange_t1392_m19355_gshared/* 1131*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTableRange_t1392_m19357_gshared/* 1132*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTableRange_t1392_m19358_gshared/* 1133*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1469_m19360_gshared/* 1134*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1469_m19361_gshared/* 1135*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1469_m19362_gshared/* 1136*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1469_m19363_gshared/* 1137*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1469_m19364_gshared/* 1138*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1469_m19365_gshared/* 1139*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1469_m19366_gshared/* 1140*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1469_m19368_gshared/* 1141*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1469_m19369_gshared/* 1142*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisSlot_t1476_m19371_gshared/* 1143*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisSlot_t1476_m19372_gshared/* 1144*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisSlot_t1476_m19373_gshared/* 1145*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisSlot_t1476_m19374_gshared/* 1146*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisSlot_t1476_m19375_gshared/* 1147*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisSlot_t1476_m19376_gshared/* 1148*/,
	(methodPointerType)&Array_InternalArray__Insert_TisSlot_t1476_m19377_gshared/* 1149*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisSlot_t1476_m19379_gshared/* 1150*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisSlot_t1476_m19380_gshared/* 1151*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDateTime_t508_m19384_gshared/* 1152*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDateTime_t508_m19385_gshared/* 1153*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDateTime_t508_m19386_gshared/* 1154*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDateTime_t508_m19387_gshared/* 1155*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDateTime_t508_m19388_gshared/* 1156*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDateTime_t508_m19389_gshared/* 1157*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDateTime_t508_m19390_gshared/* 1158*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDateTime_t508_m19392_gshared/* 1159*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t508_m19393_gshared/* 1160*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisDecimal_t755_m19395_gshared/* 1161*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisDecimal_t755_m19396_gshared/* 1162*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisDecimal_t755_m19397_gshared/* 1163*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisDecimal_t755_m19398_gshared/* 1164*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisDecimal_t755_m19399_gshared/* 1165*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisDecimal_t755_m19400_gshared/* 1166*/,
	(methodPointerType)&Array_InternalArray__Insert_TisDecimal_t755_m19401_gshared/* 1167*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisDecimal_t755_m19403_gshared/* 1168*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t755_m19404_gshared/* 1169*/,
	(methodPointerType)&Array_InternalArray__get_Item_TisTimeSpan_t1051_m19406_gshared/* 1170*/,
	(methodPointerType)&Array_InternalArray__ICollection_Add_TisTimeSpan_t1051_m19407_gshared/* 1171*/,
	(methodPointerType)&Array_InternalArray__ICollection_Contains_TisTimeSpan_t1051_m19408_gshared/* 1172*/,
	(methodPointerType)&Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1051_m19409_gshared/* 1173*/,
	(methodPointerType)&Array_InternalArray__ICollection_Remove_TisTimeSpan_t1051_m19410_gshared/* 1174*/,
	(methodPointerType)&Array_InternalArray__IndexOf_TisTimeSpan_t1051_m19411_gshared/* 1175*/,
	(methodPointerType)&Array_InternalArray__Insert_TisTimeSpan_t1051_m19412_gshared/* 1176*/,
	(methodPointerType)&Array_InternalArray__set_Item_TisTimeSpan_t1051_m19414_gshared/* 1177*/,
	(methodPointerType)&Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1051_m19415_gshared/* 1178*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m11445_gshared/* 1179*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11446_gshared/* 1180*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m11447_gshared/* 1181*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m11448_gshared/* 1182*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m11449_gshared/* 1183*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m11450_gshared/* 1184*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11451_gshared/* 1185*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m11452_gshared/* 1186*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m11453_gshared/* 1187*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m11454_gshared/* 1188*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m11456_gshared/* 1189*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11458_gshared/* 1190*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m11460_gshared/* 1191*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m11462_gshared/* 1192*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m11464_gshared/* 1193*/,
	(methodPointerType)&Func_2_Invoke_m11610_gshared/* 1194*/,
	(methodPointerType)&Func_2_BeginInvoke_m11612_gshared/* 1195*/,
	(methodPointerType)&Func_2_EndInvoke_m11614_gshared/* 1196*/,
	(methodPointerType)&Comparison_1_Invoke_m11884_gshared/* 1197*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m11885_gshared/* 1198*/,
	(methodPointerType)&Comparison_1_EndInvoke_m11886_gshared/* 1199*/,
	(methodPointerType)&List_1__ctor_m12128_gshared/* 1200*/,
	(methodPointerType)&List_1__cctor_m12129_gshared/* 1201*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12130_gshared/* 1202*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m12131_gshared/* 1203*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m12132_gshared/* 1204*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m12133_gshared/* 1205*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m12134_gshared/* 1206*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m12135_gshared/* 1207*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m12136_gshared/* 1208*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m12137_gshared/* 1209*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12138_gshared/* 1210*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m12139_gshared/* 1211*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m12140_gshared/* 1212*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m12141_gshared/* 1213*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m12142_gshared/* 1214*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m12143_gshared/* 1215*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m12144_gshared/* 1216*/,
	(methodPointerType)&List_1_Add_m12145_gshared/* 1217*/,
	(methodPointerType)&List_1_GrowIfNeeded_m12146_gshared/* 1218*/,
	(methodPointerType)&List_1_AddCollection_m12147_gshared/* 1219*/,
	(methodPointerType)&List_1_AddEnumerable_m12148_gshared/* 1220*/,
	(methodPointerType)&List_1_AddRange_m12149_gshared/* 1221*/,
	(methodPointerType)&List_1_AsReadOnly_m12150_gshared/* 1222*/,
	(methodPointerType)&List_1_Clear_m12151_gshared/* 1223*/,
	(methodPointerType)&List_1_Contains_m12152_gshared/* 1224*/,
	(methodPointerType)&List_1_CopyTo_m12153_gshared/* 1225*/,
	(methodPointerType)&List_1_Find_m12154_gshared/* 1226*/,
	(methodPointerType)&List_1_CheckMatch_m12155_gshared/* 1227*/,
	(methodPointerType)&List_1_GetIndex_m12156_gshared/* 1228*/,
	(methodPointerType)&List_1_GetEnumerator_m12157_gshared/* 1229*/,
	(methodPointerType)&List_1_IndexOf_m12158_gshared/* 1230*/,
	(methodPointerType)&List_1_Shift_m12159_gshared/* 1231*/,
	(methodPointerType)&List_1_CheckIndex_m12160_gshared/* 1232*/,
	(methodPointerType)&List_1_Insert_m12161_gshared/* 1233*/,
	(methodPointerType)&List_1_CheckCollection_m12162_gshared/* 1234*/,
	(methodPointerType)&List_1_Remove_m12163_gshared/* 1235*/,
	(methodPointerType)&List_1_RemoveAll_m12164_gshared/* 1236*/,
	(methodPointerType)&List_1_RemoveAt_m12165_gshared/* 1237*/,
	(methodPointerType)&List_1_Reverse_m12166_gshared/* 1238*/,
	(methodPointerType)&List_1_Sort_m12167_gshared/* 1239*/,
	(methodPointerType)&List_1_ToArray_m12168_gshared/* 1240*/,
	(methodPointerType)&List_1_TrimExcess_m12169_gshared/* 1241*/,
	(methodPointerType)&List_1_get_Capacity_m12170_gshared/* 1242*/,
	(methodPointerType)&List_1_set_Capacity_m12171_gshared/* 1243*/,
	(methodPointerType)&List_1_get_Count_m12172_gshared/* 1244*/,
	(methodPointerType)&List_1_get_Item_m12173_gshared/* 1245*/,
	(methodPointerType)&List_1_set_Item_m12174_gshared/* 1246*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m12175_gshared/* 1247*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12176_gshared/* 1248*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m12177_gshared/* 1249*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m12178_gshared/* 1250*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m12179_gshared/* 1251*/,
	(methodPointerType)&Enumerator__ctor_m12180_gshared/* 1252*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m12181_gshared/* 1253*/,
	(methodPointerType)&Enumerator_Dispose_m12182_gshared/* 1254*/,
	(methodPointerType)&Enumerator_VerifyState_m12183_gshared/* 1255*/,
	(methodPointerType)&Enumerator_MoveNext_m12184_gshared/* 1256*/,
	(methodPointerType)&Enumerator_get_Current_m12185_gshared/* 1257*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m12186_gshared/* 1258*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m12187_gshared/* 1259*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m12188_gshared/* 1260*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m12189_gshared/* 1261*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m12190_gshared/* 1262*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m12191_gshared/* 1263*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m12192_gshared/* 1264*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m12193_gshared/* 1265*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12194_gshared/* 1266*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m12195_gshared/* 1267*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m12196_gshared/* 1268*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m12197_gshared/* 1269*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m12198_gshared/* 1270*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m12199_gshared/* 1271*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m12200_gshared/* 1272*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m12201_gshared/* 1273*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m12202_gshared/* 1274*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m12203_gshared/* 1275*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m12204_gshared/* 1276*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12205_gshared/* 1277*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m12206_gshared/* 1278*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m12207_gshared/* 1279*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m12208_gshared/* 1280*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m12209_gshared/* 1281*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m12210_gshared/* 1282*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m12211_gshared/* 1283*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m12212_gshared/* 1284*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m12213_gshared/* 1285*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m12214_gshared/* 1286*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m12215_gshared/* 1287*/,
	(methodPointerType)&Collection_1__ctor_m12216_gshared/* 1288*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12217_gshared/* 1289*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m12218_gshared/* 1290*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m12219_gshared/* 1291*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m12220_gshared/* 1292*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m12221_gshared/* 1293*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m12222_gshared/* 1294*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m12223_gshared/* 1295*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m12224_gshared/* 1296*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m12225_gshared/* 1297*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m12226_gshared/* 1298*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m12227_gshared/* 1299*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m12228_gshared/* 1300*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m12229_gshared/* 1301*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m12230_gshared/* 1302*/,
	(methodPointerType)&Collection_1_Add_m12231_gshared/* 1303*/,
	(methodPointerType)&Collection_1_Clear_m12232_gshared/* 1304*/,
	(methodPointerType)&Collection_1_ClearItems_m12233_gshared/* 1305*/,
	(methodPointerType)&Collection_1_Contains_m12234_gshared/* 1306*/,
	(methodPointerType)&Collection_1_CopyTo_m12235_gshared/* 1307*/,
	(methodPointerType)&Collection_1_GetEnumerator_m12236_gshared/* 1308*/,
	(methodPointerType)&Collection_1_IndexOf_m12237_gshared/* 1309*/,
	(methodPointerType)&Collection_1_Insert_m12238_gshared/* 1310*/,
	(methodPointerType)&Collection_1_InsertItem_m12239_gshared/* 1311*/,
	(methodPointerType)&Collection_1_Remove_m12240_gshared/* 1312*/,
	(methodPointerType)&Collection_1_RemoveAt_m12241_gshared/* 1313*/,
	(methodPointerType)&Collection_1_RemoveItem_m12242_gshared/* 1314*/,
	(methodPointerType)&Collection_1_get_Count_m12243_gshared/* 1315*/,
	(methodPointerType)&Collection_1_get_Item_m12244_gshared/* 1316*/,
	(methodPointerType)&Collection_1_set_Item_m12245_gshared/* 1317*/,
	(methodPointerType)&Collection_1_SetItem_m12246_gshared/* 1318*/,
	(methodPointerType)&Collection_1_IsValidItem_m12247_gshared/* 1319*/,
	(methodPointerType)&Collection_1_ConvertItem_m12248_gshared/* 1320*/,
	(methodPointerType)&Collection_1_CheckWritable_m12249_gshared/* 1321*/,
	(methodPointerType)&Collection_1_IsSynchronized_m12250_gshared/* 1322*/,
	(methodPointerType)&Collection_1_IsFixedSize_m12251_gshared/* 1323*/,
	(methodPointerType)&EqualityComparer_1__ctor_m12252_gshared/* 1324*/,
	(methodPointerType)&EqualityComparer_1__cctor_m12253_gshared/* 1325*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m12254_gshared/* 1326*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m12255_gshared/* 1327*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m12256_gshared/* 1328*/,
	(methodPointerType)&DefaultComparer__ctor_m12257_gshared/* 1329*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m12258_gshared/* 1330*/,
	(methodPointerType)&DefaultComparer_Equals_m12259_gshared/* 1331*/,
	(methodPointerType)&Predicate_1__ctor_m12260_gshared/* 1332*/,
	(methodPointerType)&Predicate_1_Invoke_m12261_gshared/* 1333*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m12262_gshared/* 1334*/,
	(methodPointerType)&Predicate_1_EndInvoke_m12263_gshared/* 1335*/,
	(methodPointerType)&Comparer_1__ctor_m12264_gshared/* 1336*/,
	(methodPointerType)&Comparer_1__cctor_m12265_gshared/* 1337*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m12266_gshared/* 1338*/,
	(methodPointerType)&Comparer_1_get_Default_m12267_gshared/* 1339*/,
	(methodPointerType)&DefaultComparer__ctor_m12268_gshared/* 1340*/,
	(methodPointerType)&DefaultComparer_Compare_m12269_gshared/* 1341*/,
	(methodPointerType)&Dictionary_2__ctor_m12703_gshared/* 1342*/,
	(methodPointerType)&Dictionary_2__ctor_m12705_gshared/* 1343*/,
	(methodPointerType)&Dictionary_2__ctor_m12707_gshared/* 1344*/,
	(methodPointerType)&Dictionary_2__ctor_m12709_gshared/* 1345*/,
	(methodPointerType)&Dictionary_2__ctor_m12711_gshared/* 1346*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m12713_gshared/* 1347*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m12715_gshared/* 1348*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m12717_gshared/* 1349*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m12719_gshared/* 1350*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m12721_gshared/* 1351*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m12723_gshared/* 1352*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m12725_gshared/* 1353*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12727_gshared/* 1354*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12729_gshared/* 1355*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12731_gshared/* 1356*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12733_gshared/* 1357*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12735_gshared/* 1358*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12737_gshared/* 1359*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12739_gshared/* 1360*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m12741_gshared/* 1361*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12743_gshared/* 1362*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12745_gshared/* 1363*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12747_gshared/* 1364*/,
	(methodPointerType)&Dictionary_2_get_Count_m12749_gshared/* 1365*/,
	(methodPointerType)&Dictionary_2_get_Item_m12751_gshared/* 1366*/,
	(methodPointerType)&Dictionary_2_set_Item_m12753_gshared/* 1367*/,
	(methodPointerType)&Dictionary_2_Init_m12755_gshared/* 1368*/,
	(methodPointerType)&Dictionary_2_InitArrays_m12757_gshared/* 1369*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m12759_gshared/* 1370*/,
	(methodPointerType)&Dictionary_2_make_pair_m12761_gshared/* 1371*/,
	(methodPointerType)&Dictionary_2_pick_key_m12763_gshared/* 1372*/,
	(methodPointerType)&Dictionary_2_pick_value_m12765_gshared/* 1373*/,
	(methodPointerType)&Dictionary_2_CopyTo_m12767_gshared/* 1374*/,
	(methodPointerType)&Dictionary_2_Resize_m12769_gshared/* 1375*/,
	(methodPointerType)&Dictionary_2_Add_m12771_gshared/* 1376*/,
	(methodPointerType)&Dictionary_2_Clear_m12773_gshared/* 1377*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m12775_gshared/* 1378*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m12777_gshared/* 1379*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m12779_gshared/* 1380*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m12781_gshared/* 1381*/,
	(methodPointerType)&Dictionary_2_Remove_m12783_gshared/* 1382*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m12785_gshared/* 1383*/,
	(methodPointerType)&Dictionary_2_get_Keys_m12787_gshared/* 1384*/,
	(methodPointerType)&Dictionary_2_ToTKey_m12790_gshared/* 1385*/,
	(methodPointerType)&Dictionary_2_ToTValue_m12792_gshared/* 1386*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m12794_gshared/* 1387*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m12797_gshared/* 1388*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m12798_gshared/* 1389*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12799_gshared/* 1390*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m12800_gshared/* 1391*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m12801_gshared/* 1392*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m12802_gshared/* 1393*/,
	(methodPointerType)&KeyValuePair_2__ctor_m12803_gshared/* 1394*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m12805_gshared/* 1395*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m12807_gshared/* 1396*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m12809_gshared/* 1397*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12810_gshared/* 1398*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m12811_gshared/* 1399*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m12812_gshared/* 1400*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m12813_gshared/* 1401*/,
	(methodPointerType)&KeyCollection__ctor_m12814_gshared/* 1402*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m12815_gshared/* 1403*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m12816_gshared/* 1404*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m12817_gshared/* 1405*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m12818_gshared/* 1406*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m12819_gshared/* 1407*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m12820_gshared/* 1408*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m12821_gshared/* 1409*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m12822_gshared/* 1410*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m12823_gshared/* 1411*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m12824_gshared/* 1412*/,
	(methodPointerType)&KeyCollection_CopyTo_m12825_gshared/* 1413*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m12826_gshared/* 1414*/,
	(methodPointerType)&KeyCollection_get_Count_m12827_gshared/* 1415*/,
	(methodPointerType)&Enumerator__ctor_m12828_gshared/* 1416*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m12829_gshared/* 1417*/,
	(methodPointerType)&Enumerator_Dispose_m12830_gshared/* 1418*/,
	(methodPointerType)&Enumerator_MoveNext_m12831_gshared/* 1419*/,
	(methodPointerType)&Enumerator_get_Current_m12832_gshared/* 1420*/,
	(methodPointerType)&Enumerator__ctor_m12833_gshared/* 1421*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared/* 1422*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared/* 1423*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared/* 1424*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared/* 1425*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m12840_gshared/* 1426*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m12841_gshared/* 1427*/,
	(methodPointerType)&Enumerator_VerifyState_m12842_gshared/* 1428*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m12843_gshared/* 1429*/,
	(methodPointerType)&Enumerator_Dispose_m12844_gshared/* 1430*/,
	(methodPointerType)&Transform_1__ctor_m12845_gshared/* 1431*/,
	(methodPointerType)&Transform_1_Invoke_m12846_gshared/* 1432*/,
	(methodPointerType)&Transform_1_BeginInvoke_m12847_gshared/* 1433*/,
	(methodPointerType)&Transform_1_EndInvoke_m12848_gshared/* 1434*/,
	(methodPointerType)&ValueCollection__ctor_m12849_gshared/* 1435*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m12850_gshared/* 1436*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m12851_gshared/* 1437*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m12852_gshared/* 1438*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m12853_gshared/* 1439*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m12854_gshared/* 1440*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m12855_gshared/* 1441*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m12856_gshared/* 1442*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m12857_gshared/* 1443*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m12858_gshared/* 1444*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m12859_gshared/* 1445*/,
	(methodPointerType)&ValueCollection_CopyTo_m12860_gshared/* 1446*/,
	(methodPointerType)&ValueCollection_get_Count_m12862_gshared/* 1447*/,
	(methodPointerType)&Enumerator__ctor_m12863_gshared/* 1448*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m12864_gshared/* 1449*/,
	(methodPointerType)&Enumerator_Dispose_m12865_gshared/* 1450*/,
	(methodPointerType)&Transform_1__ctor_m12868_gshared/* 1451*/,
	(methodPointerType)&Transform_1_Invoke_m12869_gshared/* 1452*/,
	(methodPointerType)&Transform_1_BeginInvoke_m12870_gshared/* 1453*/,
	(methodPointerType)&Transform_1_EndInvoke_m12871_gshared/* 1454*/,
	(methodPointerType)&Transform_1__ctor_m12872_gshared/* 1455*/,
	(methodPointerType)&Transform_1_Invoke_m12873_gshared/* 1456*/,
	(methodPointerType)&Transform_1_BeginInvoke_m12874_gshared/* 1457*/,
	(methodPointerType)&Transform_1_EndInvoke_m12875_gshared/* 1458*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m12876_gshared/* 1459*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12877_gshared/* 1460*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m12878_gshared/* 1461*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m12879_gshared/* 1462*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m12880_gshared/* 1463*/,
	(methodPointerType)&Transform_1__ctor_m12881_gshared/* 1464*/,
	(methodPointerType)&Transform_1_Invoke_m12882_gshared/* 1465*/,
	(methodPointerType)&Transform_1_BeginInvoke_m12883_gshared/* 1466*/,
	(methodPointerType)&Transform_1_EndInvoke_m12884_gshared/* 1467*/,
	(methodPointerType)&ShimEnumerator__ctor_m12885_gshared/* 1468*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m12886_gshared/* 1469*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m12887_gshared/* 1470*/,
	(methodPointerType)&ShimEnumerator_get_Key_m12888_gshared/* 1471*/,
	(methodPointerType)&ShimEnumerator_get_Value_m12889_gshared/* 1472*/,
	(methodPointerType)&ShimEnumerator_get_Current_m12890_gshared/* 1473*/,
	(methodPointerType)&EqualityComparer_1__ctor_m12891_gshared/* 1474*/,
	(methodPointerType)&EqualityComparer_1__cctor_m12892_gshared/* 1475*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m12893_gshared/* 1476*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m12894_gshared/* 1477*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m12895_gshared/* 1478*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m12896_gshared/* 1479*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m12897_gshared/* 1480*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m12898_gshared/* 1481*/,
	(methodPointerType)&DefaultComparer__ctor_m12899_gshared/* 1482*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m12900_gshared/* 1483*/,
	(methodPointerType)&DefaultComparer_Equals_m12901_gshared/* 1484*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13041_gshared/* 1485*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13042_gshared/* 1486*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13043_gshared/* 1487*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13044_gshared/* 1488*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13045_gshared/* 1489*/,
	(methodPointerType)&Comparison_1_Invoke_m13046_gshared/* 1490*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m13047_gshared/* 1491*/,
	(methodPointerType)&Comparison_1_EndInvoke_m13048_gshared/* 1492*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13049_gshared/* 1493*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13050_gshared/* 1494*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13051_gshared/* 1495*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13052_gshared/* 1496*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13053_gshared/* 1497*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m13054_gshared/* 1498*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m13055_gshared/* 1499*/,
	(methodPointerType)&UnityAction_1_Invoke_m13056_gshared/* 1500*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m13057_gshared/* 1501*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m13058_gshared/* 1502*/,
	(methodPointerType)&InvokableCall_1__ctor_m13059_gshared/* 1503*/,
	(methodPointerType)&InvokableCall_1__ctor_m13060_gshared/* 1504*/,
	(methodPointerType)&InvokableCall_1_Invoke_m13061_gshared/* 1505*/,
	(methodPointerType)&InvokableCall_1_Find_m13062_gshared/* 1506*/,
	(methodPointerType)&Dictionary_2__ctor_m13094_gshared/* 1507*/,
	(methodPointerType)&Dictionary_2__ctor_m13095_gshared/* 1508*/,
	(methodPointerType)&Dictionary_2__ctor_m13096_gshared/* 1509*/,
	(methodPointerType)&Dictionary_2__ctor_m13098_gshared/* 1510*/,
	(methodPointerType)&Dictionary_2__ctor_m13099_gshared/* 1511*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m13100_gshared/* 1512*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m13101_gshared/* 1513*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m13102_gshared/* 1514*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m13103_gshared/* 1515*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m13104_gshared/* 1516*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m13105_gshared/* 1517*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m13106_gshared/* 1518*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13107_gshared/* 1519*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13108_gshared/* 1520*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13109_gshared/* 1521*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13110_gshared/* 1522*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13111_gshared/* 1523*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13112_gshared/* 1524*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13113_gshared/* 1525*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m13114_gshared/* 1526*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13115_gshared/* 1527*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13116_gshared/* 1528*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13117_gshared/* 1529*/,
	(methodPointerType)&Dictionary_2_get_Count_m13118_gshared/* 1530*/,
	(methodPointerType)&Dictionary_2_get_Item_m13119_gshared/* 1531*/,
	(methodPointerType)&Dictionary_2_set_Item_m13120_gshared/* 1532*/,
	(methodPointerType)&Dictionary_2_Init_m13121_gshared/* 1533*/,
	(methodPointerType)&Dictionary_2_InitArrays_m13122_gshared/* 1534*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m13123_gshared/* 1535*/,
	(methodPointerType)&Dictionary_2_make_pair_m13124_gshared/* 1536*/,
	(methodPointerType)&Dictionary_2_pick_key_m13125_gshared/* 1537*/,
	(methodPointerType)&Dictionary_2_pick_value_m13126_gshared/* 1538*/,
	(methodPointerType)&Dictionary_2_CopyTo_m13127_gshared/* 1539*/,
	(methodPointerType)&Dictionary_2_Resize_m13128_gshared/* 1540*/,
	(methodPointerType)&Dictionary_2_Add_m13129_gshared/* 1541*/,
	(methodPointerType)&Dictionary_2_Clear_m13130_gshared/* 1542*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m13131_gshared/* 1543*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m13132_gshared/* 1544*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m13133_gshared/* 1545*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m13134_gshared/* 1546*/,
	(methodPointerType)&Dictionary_2_Remove_m13135_gshared/* 1547*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m13136_gshared/* 1548*/,
	(methodPointerType)&Dictionary_2_get_Keys_m13137_gshared/* 1549*/,
	(methodPointerType)&Dictionary_2_get_Values_m13138_gshared/* 1550*/,
	(methodPointerType)&Dictionary_2_ToTKey_m13139_gshared/* 1551*/,
	(methodPointerType)&Dictionary_2_ToTValue_m13140_gshared/* 1552*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m13141_gshared/* 1553*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m13142_gshared/* 1554*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m13143_gshared/* 1555*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13144_gshared/* 1556*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13145_gshared/* 1557*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13146_gshared/* 1558*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13147_gshared/* 1559*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13148_gshared/* 1560*/,
	(methodPointerType)&KeyValuePair_2__ctor_m13149_gshared/* 1561*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m13150_gshared/* 1562*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m13151_gshared/* 1563*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m13152_gshared/* 1564*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m13153_gshared/* 1565*/,
	(methodPointerType)&KeyValuePair_2_ToString_m13154_gshared/* 1566*/,
	(methodPointerType)&KeyCollection__ctor_m13155_gshared/* 1567*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m13156_gshared/* 1568*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m13157_gshared/* 1569*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m13158_gshared/* 1570*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m13159_gshared/* 1571*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m13160_gshared/* 1572*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m13161_gshared/* 1573*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m13162_gshared/* 1574*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m13163_gshared/* 1575*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m13164_gshared/* 1576*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m13165_gshared/* 1577*/,
	(methodPointerType)&KeyCollection_CopyTo_m13166_gshared/* 1578*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m13167_gshared/* 1579*/,
	(methodPointerType)&KeyCollection_get_Count_m13168_gshared/* 1580*/,
	(methodPointerType)&Enumerator__ctor_m13169_gshared/* 1581*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13170_gshared/* 1582*/,
	(methodPointerType)&Enumerator_Dispose_m13171_gshared/* 1583*/,
	(methodPointerType)&Enumerator_MoveNext_m13172_gshared/* 1584*/,
	(methodPointerType)&Enumerator_get_Current_m13173_gshared/* 1585*/,
	(methodPointerType)&Enumerator__ctor_m13174_gshared/* 1586*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13175_gshared/* 1587*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13176_gshared/* 1588*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13177_gshared/* 1589*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13178_gshared/* 1590*/,
	(methodPointerType)&Enumerator_MoveNext_m13179_gshared/* 1591*/,
	(methodPointerType)&Enumerator_get_Current_m13180_gshared/* 1592*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m13181_gshared/* 1593*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m13182_gshared/* 1594*/,
	(methodPointerType)&Enumerator_VerifyState_m13183_gshared/* 1595*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m13184_gshared/* 1596*/,
	(methodPointerType)&Enumerator_Dispose_m13185_gshared/* 1597*/,
	(methodPointerType)&Transform_1__ctor_m13186_gshared/* 1598*/,
	(methodPointerType)&Transform_1_Invoke_m13187_gshared/* 1599*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13188_gshared/* 1600*/,
	(methodPointerType)&Transform_1_EndInvoke_m13189_gshared/* 1601*/,
	(methodPointerType)&ValueCollection__ctor_m13190_gshared/* 1602*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m13191_gshared/* 1603*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m13192_gshared/* 1604*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m13193_gshared/* 1605*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m13194_gshared/* 1606*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m13195_gshared/* 1607*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m13196_gshared/* 1608*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m13197_gshared/* 1609*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m13198_gshared/* 1610*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m13199_gshared/* 1611*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m13200_gshared/* 1612*/,
	(methodPointerType)&ValueCollection_CopyTo_m13201_gshared/* 1613*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m13202_gshared/* 1614*/,
	(methodPointerType)&ValueCollection_get_Count_m13203_gshared/* 1615*/,
	(methodPointerType)&Enumerator__ctor_m13204_gshared/* 1616*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13205_gshared/* 1617*/,
	(methodPointerType)&Enumerator_Dispose_m13206_gshared/* 1618*/,
	(methodPointerType)&Enumerator_MoveNext_m13207_gshared/* 1619*/,
	(methodPointerType)&Enumerator_get_Current_m13208_gshared/* 1620*/,
	(methodPointerType)&Transform_1__ctor_m13209_gshared/* 1621*/,
	(methodPointerType)&Transform_1_Invoke_m13210_gshared/* 1622*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13211_gshared/* 1623*/,
	(methodPointerType)&Transform_1_EndInvoke_m13212_gshared/* 1624*/,
	(methodPointerType)&Transform_1__ctor_m13213_gshared/* 1625*/,
	(methodPointerType)&Transform_1_Invoke_m13214_gshared/* 1626*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13215_gshared/* 1627*/,
	(methodPointerType)&Transform_1_EndInvoke_m13216_gshared/* 1628*/,
	(methodPointerType)&Transform_1__ctor_m13217_gshared/* 1629*/,
	(methodPointerType)&Transform_1_Invoke_m13218_gshared/* 1630*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13219_gshared/* 1631*/,
	(methodPointerType)&Transform_1_EndInvoke_m13220_gshared/* 1632*/,
	(methodPointerType)&ShimEnumerator__ctor_m13221_gshared/* 1633*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m13222_gshared/* 1634*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m13223_gshared/* 1635*/,
	(methodPointerType)&ShimEnumerator_get_Key_m13224_gshared/* 1636*/,
	(methodPointerType)&ShimEnumerator_get_Value_m13225_gshared/* 1637*/,
	(methodPointerType)&ShimEnumerator_get_Current_m13226_gshared/* 1638*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m13475_gshared/* 1639*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13476_gshared/* 1640*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m13477_gshared/* 1641*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m13478_gshared/* 1642*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m13479_gshared/* 1643*/,
	(methodPointerType)&Transform_1__ctor_m13540_gshared/* 1644*/,
	(methodPointerType)&Transform_1_Invoke_m13541_gshared/* 1645*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13542_gshared/* 1646*/,
	(methodPointerType)&Transform_1_EndInvoke_m13543_gshared/* 1647*/,
	(methodPointerType)&Transform_1__ctor_m13544_gshared/* 1648*/,
	(methodPointerType)&Transform_1_Invoke_m13545_gshared/* 1649*/,
	(methodPointerType)&Transform_1_BeginInvoke_m13546_gshared/* 1650*/,
	(methodPointerType)&Transform_1_EndInvoke_m13547_gshared/* 1651*/,
	(methodPointerType)&List_1__cctor_m13718_gshared/* 1652*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13719_gshared/* 1653*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m13720_gshared/* 1654*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared/* 1655*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m13722_gshared/* 1656*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m13723_gshared/* 1657*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m13724_gshared/* 1658*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m13725_gshared/* 1659*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m13726_gshared/* 1660*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13727_gshared/* 1661*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m13728_gshared/* 1662*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m13729_gshared/* 1663*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m13730_gshared/* 1664*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m13731_gshared/* 1665*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m13732_gshared/* 1666*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m13733_gshared/* 1667*/,
	(methodPointerType)&List_1_Add_m13734_gshared/* 1668*/,
	(methodPointerType)&List_1_GrowIfNeeded_m13735_gshared/* 1669*/,
	(methodPointerType)&List_1_AddCollection_m13736_gshared/* 1670*/,
	(methodPointerType)&List_1_AddEnumerable_m13737_gshared/* 1671*/,
	(methodPointerType)&List_1_AddRange_m13738_gshared/* 1672*/,
	(methodPointerType)&List_1_AsReadOnly_m13739_gshared/* 1673*/,
	(methodPointerType)&List_1_Clear_m13740_gshared/* 1674*/,
	(methodPointerType)&List_1_Contains_m13741_gshared/* 1675*/,
	(methodPointerType)&List_1_CopyTo_m13742_gshared/* 1676*/,
	(methodPointerType)&List_1_Find_m13743_gshared/* 1677*/,
	(methodPointerType)&List_1_CheckMatch_m13744_gshared/* 1678*/,
	(methodPointerType)&List_1_GetIndex_m13745_gshared/* 1679*/,
	(methodPointerType)&List_1_GetEnumerator_m13746_gshared/* 1680*/,
	(methodPointerType)&List_1_IndexOf_m13747_gshared/* 1681*/,
	(methodPointerType)&List_1_Shift_m13748_gshared/* 1682*/,
	(methodPointerType)&List_1_CheckIndex_m13749_gshared/* 1683*/,
	(methodPointerType)&List_1_Insert_m13750_gshared/* 1684*/,
	(methodPointerType)&List_1_CheckCollection_m13751_gshared/* 1685*/,
	(methodPointerType)&List_1_Remove_m13752_gshared/* 1686*/,
	(methodPointerType)&List_1_RemoveAll_m13753_gshared/* 1687*/,
	(methodPointerType)&List_1_RemoveAt_m13754_gshared/* 1688*/,
	(methodPointerType)&List_1_Reverse_m13755_gshared/* 1689*/,
	(methodPointerType)&List_1_Sort_m13756_gshared/* 1690*/,
	(methodPointerType)&List_1_Sort_m13757_gshared/* 1691*/,
	(methodPointerType)&List_1_TrimExcess_m13758_gshared/* 1692*/,
	(methodPointerType)&List_1_get_Count_m13759_gshared/* 1693*/,
	(methodPointerType)&List_1_get_Item_m13760_gshared/* 1694*/,
	(methodPointerType)&List_1_set_Item_m13761_gshared/* 1695*/,
	(methodPointerType)&Enumerator__ctor_m13697_gshared/* 1696*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m13698_gshared/* 1697*/,
	(methodPointerType)&Enumerator_Dispose_m13699_gshared/* 1698*/,
	(methodPointerType)&Enumerator_VerifyState_m13700_gshared/* 1699*/,
	(methodPointerType)&Enumerator_MoveNext_m13701_gshared/* 1700*/,
	(methodPointerType)&Enumerator_get_Current_m13702_gshared/* 1701*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m13663_gshared/* 1702*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13664_gshared/* 1703*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13665_gshared/* 1704*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13666_gshared/* 1705*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13667_gshared/* 1706*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13668_gshared/* 1707*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13669_gshared/* 1708*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13670_gshared/* 1709*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13671_gshared/* 1710*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13672_gshared/* 1711*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13673_gshared/* 1712*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m13674_gshared/* 1713*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m13675_gshared/* 1714*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m13676_gshared/* 1715*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m13677_gshared/* 1716*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m13678_gshared/* 1717*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m13679_gshared/* 1718*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m13680_gshared/* 1719*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m13681_gshared/* 1720*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m13682_gshared/* 1721*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m13683_gshared/* 1722*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m13684_gshared/* 1723*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m13685_gshared/* 1724*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m13686_gshared/* 1725*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m13687_gshared/* 1726*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m13688_gshared/* 1727*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m13689_gshared/* 1728*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m13690_gshared/* 1729*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m13691_gshared/* 1730*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m13692_gshared/* 1731*/,
	(methodPointerType)&Collection_1__ctor_m13765_gshared/* 1732*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13766_gshared/* 1733*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m13767_gshared/* 1734*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m13768_gshared/* 1735*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m13769_gshared/* 1736*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m13770_gshared/* 1737*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m13771_gshared/* 1738*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m13772_gshared/* 1739*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m13773_gshared/* 1740*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m13774_gshared/* 1741*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m13775_gshared/* 1742*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m13776_gshared/* 1743*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m13777_gshared/* 1744*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m13778_gshared/* 1745*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m13779_gshared/* 1746*/,
	(methodPointerType)&Collection_1_Add_m13780_gshared/* 1747*/,
	(methodPointerType)&Collection_1_Clear_m13781_gshared/* 1748*/,
	(methodPointerType)&Collection_1_ClearItems_m13782_gshared/* 1749*/,
	(methodPointerType)&Collection_1_Contains_m13783_gshared/* 1750*/,
	(methodPointerType)&Collection_1_CopyTo_m13784_gshared/* 1751*/,
	(methodPointerType)&Collection_1_GetEnumerator_m13785_gshared/* 1752*/,
	(methodPointerType)&Collection_1_IndexOf_m13786_gshared/* 1753*/,
	(methodPointerType)&Collection_1_Insert_m13787_gshared/* 1754*/,
	(methodPointerType)&Collection_1_InsertItem_m13788_gshared/* 1755*/,
	(methodPointerType)&Collection_1_Remove_m13789_gshared/* 1756*/,
	(methodPointerType)&Collection_1_RemoveAt_m13790_gshared/* 1757*/,
	(methodPointerType)&Collection_1_RemoveItem_m13791_gshared/* 1758*/,
	(methodPointerType)&Collection_1_get_Count_m13792_gshared/* 1759*/,
	(methodPointerType)&Collection_1_get_Item_m13793_gshared/* 1760*/,
	(methodPointerType)&Collection_1_set_Item_m13794_gshared/* 1761*/,
	(methodPointerType)&Collection_1_SetItem_m13795_gshared/* 1762*/,
	(methodPointerType)&Collection_1_IsValidItem_m13796_gshared/* 1763*/,
	(methodPointerType)&Collection_1_ConvertItem_m13797_gshared/* 1764*/,
	(methodPointerType)&Collection_1_CheckWritable_m13798_gshared/* 1765*/,
	(methodPointerType)&Collection_1_IsSynchronized_m13799_gshared/* 1766*/,
	(methodPointerType)&Collection_1_IsFixedSize_m13800_gshared/* 1767*/,
	(methodPointerType)&EqualityComparer_1__ctor_m13801_gshared/* 1768*/,
	(methodPointerType)&EqualityComparer_1__cctor_m13802_gshared/* 1769*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m13803_gshared/* 1770*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m13804_gshared/* 1771*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m13805_gshared/* 1772*/,
	(methodPointerType)&DefaultComparer__ctor_m13806_gshared/* 1773*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m13807_gshared/* 1774*/,
	(methodPointerType)&DefaultComparer_Equals_m13808_gshared/* 1775*/,
	(methodPointerType)&Predicate_1__ctor_m13693_gshared/* 1776*/,
	(methodPointerType)&Predicate_1_Invoke_m13694_gshared/* 1777*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m13695_gshared/* 1778*/,
	(methodPointerType)&Predicate_1_EndInvoke_m13696_gshared/* 1779*/,
	(methodPointerType)&Comparer_1__ctor_m13809_gshared/* 1780*/,
	(methodPointerType)&Comparer_1__cctor_m13810_gshared/* 1781*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m13811_gshared/* 1782*/,
	(methodPointerType)&Comparer_1_get_Default_m13812_gshared/* 1783*/,
	(methodPointerType)&DefaultComparer__ctor_m13813_gshared/* 1784*/,
	(methodPointerType)&DefaultComparer_Compare_m13814_gshared/* 1785*/,
	(methodPointerType)&Comparison_1__ctor_m13703_gshared/* 1786*/,
	(methodPointerType)&Comparison_1_Invoke_m13704_gshared/* 1787*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m13705_gshared/* 1788*/,
	(methodPointerType)&Comparison_1_EndInvoke_m13706_gshared/* 1789*/,
	(methodPointerType)&TweenRunner_1_Start_m13815_gshared/* 1790*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0__ctor_m13816_gshared/* 1791*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13817_gshared/* 1792*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13818_gshared/* 1793*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_MoveNext_m13819_gshared/* 1794*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Dispose_m13820_gshared/* 1795*/,
	(methodPointerType)&U3CStartU3Ec__Iterator0_Reset_m13821_gshared/* 1796*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14270_gshared/* 1797*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14271_gshared/* 1798*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14272_gshared/* 1799*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14273_gshared/* 1800*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14274_gshared/* 1801*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14280_gshared/* 1802*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14281_gshared/* 1803*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14282_gshared/* 1804*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14283_gshared/* 1805*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14284_gshared/* 1806*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14285_gshared/* 1807*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14286_gshared/* 1808*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14287_gshared/* 1809*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14288_gshared/* 1810*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14289_gshared/* 1811*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m14297_gshared/* 1812*/,
	(methodPointerType)&UnityAction_1_Invoke_m14298_gshared/* 1813*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m14299_gshared/* 1814*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m14300_gshared/* 1815*/,
	(methodPointerType)&InvokableCall_1__ctor_m14301_gshared/* 1816*/,
	(methodPointerType)&InvokableCall_1__ctor_m14302_gshared/* 1817*/,
	(methodPointerType)&InvokableCall_1_Invoke_m14303_gshared/* 1818*/,
	(methodPointerType)&InvokableCall_1_Find_m14304_gshared/* 1819*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m14305_gshared/* 1820*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14306_gshared/* 1821*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m14307_gshared/* 1822*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m14308_gshared/* 1823*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m14309_gshared/* 1824*/,
	(methodPointerType)&UnityEvent_1_AddListener_m14310_gshared/* 1825*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m14311_gshared/* 1826*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m14312_gshared/* 1827*/,
	(methodPointerType)&UnityAction_1__ctor_m14313_gshared/* 1828*/,
	(methodPointerType)&UnityAction_1_Invoke_m14314_gshared/* 1829*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m14315_gshared/* 1830*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m14316_gshared/* 1831*/,
	(methodPointerType)&InvokableCall_1__ctor_m14317_gshared/* 1832*/,
	(methodPointerType)&InvokableCall_1__ctor_m14318_gshared/* 1833*/,
	(methodPointerType)&InvokableCall_1_Invoke_m14319_gshared/* 1834*/,
	(methodPointerType)&InvokableCall_1_Find_m14320_gshared/* 1835*/,
	(methodPointerType)&UnityEvent_1_AddListener_m14599_gshared/* 1836*/,
	(methodPointerType)&UnityEvent_1_RemoveListener_m14601_gshared/* 1837*/,
	(methodPointerType)&UnityEvent_1_GetDelegate_m14605_gshared/* 1838*/,
	(methodPointerType)&UnityAction_1__ctor_m14607_gshared/* 1839*/,
	(methodPointerType)&UnityAction_1_Invoke_m14608_gshared/* 1840*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m14609_gshared/* 1841*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m14610_gshared/* 1842*/,
	(methodPointerType)&InvokableCall_1__ctor_m14611_gshared/* 1843*/,
	(methodPointerType)&InvokableCall_1__ctor_m14612_gshared/* 1844*/,
	(methodPointerType)&InvokableCall_1_Invoke_m14613_gshared/* 1845*/,
	(methodPointerType)&InvokableCall_1_Find_m14614_gshared/* 1846*/,
	(methodPointerType)&Func_2_Invoke_m14816_gshared/* 1847*/,
	(methodPointerType)&Func_2_BeginInvoke_m14818_gshared/* 1848*/,
	(methodPointerType)&Func_2_EndInvoke_m14820_gshared/* 1849*/,
	(methodPointerType)&Action_1__ctor_m14863_gshared/* 1850*/,
	(methodPointerType)&Action_1_Invoke_m14865_gshared/* 1851*/,
	(methodPointerType)&Action_1_BeginInvoke_m14867_gshared/* 1852*/,
	(methodPointerType)&Action_1_EndInvoke_m14869_gshared/* 1853*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15005_gshared/* 1854*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15006_gshared/* 1855*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15007_gshared/* 1856*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15008_gshared/* 1857*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15009_gshared/* 1858*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15015_gshared/* 1859*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15016_gshared/* 1860*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15017_gshared/* 1861*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15018_gshared/* 1862*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15019_gshared/* 1863*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15518_gshared/* 1864*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15519_gshared/* 1865*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15520_gshared/* 1866*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15521_gshared/* 1867*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15522_gshared/* 1868*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15716_gshared/* 1869*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15717_gshared/* 1870*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15718_gshared/* 1871*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15719_gshared/* 1872*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15720_gshared/* 1873*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15813_gshared/* 1874*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15814_gshared/* 1875*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15815_gshared/* 1876*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15816_gshared/* 1877*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15817_gshared/* 1878*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m15818_gshared/* 1879*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15819_gshared/* 1880*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m15820_gshared/* 1881*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m15821_gshared/* 1882*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m15822_gshared/* 1883*/,
	(methodPointerType)&List_1__ctor_m15823_gshared/* 1884*/,
	(methodPointerType)&List_1__cctor_m15824_gshared/* 1885*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15825_gshared/* 1886*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15826_gshared/* 1887*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15827_gshared/* 1888*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15828_gshared/* 1889*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15829_gshared/* 1890*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15830_gshared/* 1891*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15831_gshared/* 1892*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15832_gshared/* 1893*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15833_gshared/* 1894*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15834_gshared/* 1895*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15835_gshared/* 1896*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15836_gshared/* 1897*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15837_gshared/* 1898*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15838_gshared/* 1899*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15839_gshared/* 1900*/,
	(methodPointerType)&List_1_Add_m15840_gshared/* 1901*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15841_gshared/* 1902*/,
	(methodPointerType)&List_1_AddCollection_m15842_gshared/* 1903*/,
	(methodPointerType)&List_1_AddEnumerable_m15843_gshared/* 1904*/,
	(methodPointerType)&List_1_AddRange_m15844_gshared/* 1905*/,
	(methodPointerType)&List_1_AsReadOnly_m15845_gshared/* 1906*/,
	(methodPointerType)&List_1_Clear_m15846_gshared/* 1907*/,
	(methodPointerType)&List_1_Contains_m15847_gshared/* 1908*/,
	(methodPointerType)&List_1_CopyTo_m15848_gshared/* 1909*/,
	(methodPointerType)&List_1_Find_m15849_gshared/* 1910*/,
	(methodPointerType)&List_1_CheckMatch_m15850_gshared/* 1911*/,
	(methodPointerType)&List_1_GetIndex_m15851_gshared/* 1912*/,
	(methodPointerType)&List_1_GetEnumerator_m15852_gshared/* 1913*/,
	(methodPointerType)&List_1_IndexOf_m15853_gshared/* 1914*/,
	(methodPointerType)&List_1_Shift_m15854_gshared/* 1915*/,
	(methodPointerType)&List_1_CheckIndex_m15855_gshared/* 1916*/,
	(methodPointerType)&List_1_Insert_m15856_gshared/* 1917*/,
	(methodPointerType)&List_1_CheckCollection_m15857_gshared/* 1918*/,
	(methodPointerType)&List_1_Remove_m15858_gshared/* 1919*/,
	(methodPointerType)&List_1_RemoveAll_m15859_gshared/* 1920*/,
	(methodPointerType)&List_1_RemoveAt_m15860_gshared/* 1921*/,
	(methodPointerType)&List_1_Reverse_m15861_gshared/* 1922*/,
	(methodPointerType)&List_1_Sort_m15862_gshared/* 1923*/,
	(methodPointerType)&List_1_Sort_m15863_gshared/* 1924*/,
	(methodPointerType)&List_1_ToArray_m15864_gshared/* 1925*/,
	(methodPointerType)&List_1_TrimExcess_m15865_gshared/* 1926*/,
	(methodPointerType)&List_1_get_Capacity_m15866_gshared/* 1927*/,
	(methodPointerType)&List_1_set_Capacity_m15867_gshared/* 1928*/,
	(methodPointerType)&List_1_get_Count_m15868_gshared/* 1929*/,
	(methodPointerType)&List_1_get_Item_m15869_gshared/* 1930*/,
	(methodPointerType)&List_1_set_Item_m15870_gshared/* 1931*/,
	(methodPointerType)&Enumerator__ctor_m15871_gshared/* 1932*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15872_gshared/* 1933*/,
	(methodPointerType)&Enumerator_Dispose_m15873_gshared/* 1934*/,
	(methodPointerType)&Enumerator_VerifyState_m15874_gshared/* 1935*/,
	(methodPointerType)&Enumerator_MoveNext_m15875_gshared/* 1936*/,
	(methodPointerType)&Enumerator_get_Current_m15876_gshared/* 1937*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m15877_gshared/* 1938*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15878_gshared/* 1939*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15879_gshared/* 1940*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15880_gshared/* 1941*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15881_gshared/* 1942*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15882_gshared/* 1943*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15883_gshared/* 1944*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15884_gshared/* 1945*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15885_gshared/* 1946*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15886_gshared/* 1947*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15887_gshared/* 1948*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m15888_gshared/* 1949*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m15889_gshared/* 1950*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m15890_gshared/* 1951*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15891_gshared/* 1952*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m15892_gshared/* 1953*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m15893_gshared/* 1954*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15894_gshared/* 1955*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15895_gshared/* 1956*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15896_gshared/* 1957*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15897_gshared/* 1958*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15898_gshared/* 1959*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m15899_gshared/* 1960*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m15900_gshared/* 1961*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m15901_gshared/* 1962*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m15902_gshared/* 1963*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m15903_gshared/* 1964*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m15904_gshared/* 1965*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m15905_gshared/* 1966*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m15906_gshared/* 1967*/,
	(methodPointerType)&Collection_1__ctor_m15907_gshared/* 1968*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15908_gshared/* 1969*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m15909_gshared/* 1970*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m15910_gshared/* 1971*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m15911_gshared/* 1972*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m15912_gshared/* 1973*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m15913_gshared/* 1974*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m15914_gshared/* 1975*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m15915_gshared/* 1976*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m15916_gshared/* 1977*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m15917_gshared/* 1978*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m15918_gshared/* 1979*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m15919_gshared/* 1980*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m15920_gshared/* 1981*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m15921_gshared/* 1982*/,
	(methodPointerType)&Collection_1_Add_m15922_gshared/* 1983*/,
	(methodPointerType)&Collection_1_Clear_m15923_gshared/* 1984*/,
	(methodPointerType)&Collection_1_ClearItems_m15924_gshared/* 1985*/,
	(methodPointerType)&Collection_1_Contains_m15925_gshared/* 1986*/,
	(methodPointerType)&Collection_1_CopyTo_m15926_gshared/* 1987*/,
	(methodPointerType)&Collection_1_GetEnumerator_m15927_gshared/* 1988*/,
	(methodPointerType)&Collection_1_IndexOf_m15928_gshared/* 1989*/,
	(methodPointerType)&Collection_1_Insert_m15929_gshared/* 1990*/,
	(methodPointerType)&Collection_1_InsertItem_m15930_gshared/* 1991*/,
	(methodPointerType)&Collection_1_Remove_m15931_gshared/* 1992*/,
	(methodPointerType)&Collection_1_RemoveAt_m15932_gshared/* 1993*/,
	(methodPointerType)&Collection_1_RemoveItem_m15933_gshared/* 1994*/,
	(methodPointerType)&Collection_1_get_Count_m15934_gshared/* 1995*/,
	(methodPointerType)&Collection_1_get_Item_m15935_gshared/* 1996*/,
	(methodPointerType)&Collection_1_set_Item_m15936_gshared/* 1997*/,
	(methodPointerType)&Collection_1_SetItem_m15937_gshared/* 1998*/,
	(methodPointerType)&Collection_1_IsValidItem_m15938_gshared/* 1999*/,
	(methodPointerType)&Collection_1_ConvertItem_m15939_gshared/* 2000*/,
	(methodPointerType)&Collection_1_CheckWritable_m15940_gshared/* 2001*/,
	(methodPointerType)&Collection_1_IsSynchronized_m15941_gshared/* 2002*/,
	(methodPointerType)&Collection_1_IsFixedSize_m15942_gshared/* 2003*/,
	(methodPointerType)&EqualityComparer_1__ctor_m15943_gshared/* 2004*/,
	(methodPointerType)&EqualityComparer_1__cctor_m15944_gshared/* 2005*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15945_gshared/* 2006*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15946_gshared/* 2007*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m15947_gshared/* 2008*/,
	(methodPointerType)&DefaultComparer__ctor_m15948_gshared/* 2009*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m15949_gshared/* 2010*/,
	(methodPointerType)&DefaultComparer_Equals_m15950_gshared/* 2011*/,
	(methodPointerType)&Predicate_1__ctor_m15951_gshared/* 2012*/,
	(methodPointerType)&Predicate_1_Invoke_m15952_gshared/* 2013*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m15953_gshared/* 2014*/,
	(methodPointerType)&Predicate_1_EndInvoke_m15954_gshared/* 2015*/,
	(methodPointerType)&Comparer_1__ctor_m15955_gshared/* 2016*/,
	(methodPointerType)&Comparer_1__cctor_m15956_gshared/* 2017*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m15957_gshared/* 2018*/,
	(methodPointerType)&Comparer_1_get_Default_m15958_gshared/* 2019*/,
	(methodPointerType)&DefaultComparer__ctor_m15959_gshared/* 2020*/,
	(methodPointerType)&DefaultComparer_Compare_m15960_gshared/* 2021*/,
	(methodPointerType)&Comparison_1__ctor_m15961_gshared/* 2022*/,
	(methodPointerType)&Comparison_1_Invoke_m15962_gshared/* 2023*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m15963_gshared/* 2024*/,
	(methodPointerType)&Comparison_1_EndInvoke_m15964_gshared/* 2025*/,
	(methodPointerType)&List_1__ctor_m15965_gshared/* 2026*/,
	(methodPointerType)&List_1__cctor_m15966_gshared/* 2027*/,
	(methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15967_gshared/* 2028*/,
	(methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m15968_gshared/* 2029*/,
	(methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m15969_gshared/* 2030*/,
	(methodPointerType)&List_1_System_Collections_IList_Add_m15970_gshared/* 2031*/,
	(methodPointerType)&List_1_System_Collections_IList_Contains_m15971_gshared/* 2032*/,
	(methodPointerType)&List_1_System_Collections_IList_IndexOf_m15972_gshared/* 2033*/,
	(methodPointerType)&List_1_System_Collections_IList_Insert_m15973_gshared/* 2034*/,
	(methodPointerType)&List_1_System_Collections_IList_Remove_m15974_gshared/* 2035*/,
	(methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15975_gshared/* 2036*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m15976_gshared/* 2037*/,
	(methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m15977_gshared/* 2038*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m15978_gshared/* 2039*/,
	(methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m15979_gshared/* 2040*/,
	(methodPointerType)&List_1_System_Collections_IList_get_Item_m15980_gshared/* 2041*/,
	(methodPointerType)&List_1_System_Collections_IList_set_Item_m15981_gshared/* 2042*/,
	(methodPointerType)&List_1_Add_m15982_gshared/* 2043*/,
	(methodPointerType)&List_1_GrowIfNeeded_m15983_gshared/* 2044*/,
	(methodPointerType)&List_1_AddCollection_m15984_gshared/* 2045*/,
	(methodPointerType)&List_1_AddEnumerable_m15985_gshared/* 2046*/,
	(methodPointerType)&List_1_AddRange_m15986_gshared/* 2047*/,
	(methodPointerType)&List_1_AsReadOnly_m15987_gshared/* 2048*/,
	(methodPointerType)&List_1_Clear_m15988_gshared/* 2049*/,
	(methodPointerType)&List_1_Contains_m15989_gshared/* 2050*/,
	(methodPointerType)&List_1_CopyTo_m15990_gshared/* 2051*/,
	(methodPointerType)&List_1_Find_m15991_gshared/* 2052*/,
	(methodPointerType)&List_1_CheckMatch_m15992_gshared/* 2053*/,
	(methodPointerType)&List_1_GetIndex_m15993_gshared/* 2054*/,
	(methodPointerType)&List_1_GetEnumerator_m15994_gshared/* 2055*/,
	(methodPointerType)&List_1_IndexOf_m15995_gshared/* 2056*/,
	(methodPointerType)&List_1_Shift_m15996_gshared/* 2057*/,
	(methodPointerType)&List_1_CheckIndex_m15997_gshared/* 2058*/,
	(methodPointerType)&List_1_Insert_m15998_gshared/* 2059*/,
	(methodPointerType)&List_1_CheckCollection_m15999_gshared/* 2060*/,
	(methodPointerType)&List_1_Remove_m16000_gshared/* 2061*/,
	(methodPointerType)&List_1_RemoveAll_m16001_gshared/* 2062*/,
	(methodPointerType)&List_1_RemoveAt_m16002_gshared/* 2063*/,
	(methodPointerType)&List_1_Reverse_m16003_gshared/* 2064*/,
	(methodPointerType)&List_1_Sort_m16004_gshared/* 2065*/,
	(methodPointerType)&List_1_Sort_m16005_gshared/* 2066*/,
	(methodPointerType)&List_1_ToArray_m16006_gshared/* 2067*/,
	(methodPointerType)&List_1_TrimExcess_m16007_gshared/* 2068*/,
	(methodPointerType)&List_1_get_Capacity_m16008_gshared/* 2069*/,
	(methodPointerType)&List_1_set_Capacity_m16009_gshared/* 2070*/,
	(methodPointerType)&List_1_get_Count_m16010_gshared/* 2071*/,
	(methodPointerType)&List_1_get_Item_m16011_gshared/* 2072*/,
	(methodPointerType)&List_1_set_Item_m16012_gshared/* 2073*/,
	(methodPointerType)&Enumerator__ctor_m16013_gshared/* 2074*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16014_gshared/* 2075*/,
	(methodPointerType)&Enumerator_Dispose_m16015_gshared/* 2076*/,
	(methodPointerType)&Enumerator_VerifyState_m16016_gshared/* 2077*/,
	(methodPointerType)&Enumerator_MoveNext_m16017_gshared/* 2078*/,
	(methodPointerType)&Enumerator_get_Current_m16018_gshared/* 2079*/,
	(methodPointerType)&ReadOnlyCollection_1__ctor_m16019_gshared/* 2080*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16020_gshared/* 2081*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16021_gshared/* 2082*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16022_gshared/* 2083*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16023_gshared/* 2084*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16024_gshared/* 2085*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16025_gshared/* 2086*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16026_gshared/* 2087*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16027_gshared/* 2088*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16028_gshared/* 2089*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16029_gshared/* 2090*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Add_m16030_gshared/* 2091*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Clear_m16031_gshared/* 2092*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Contains_m16032_gshared/* 2093*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16033_gshared/* 2094*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Insert_m16034_gshared/* 2095*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_Remove_m16035_gshared/* 2096*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16036_gshared/* 2097*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16037_gshared/* 2098*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16038_gshared/* 2099*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16039_gshared/* 2100*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16040_gshared/* 2101*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_get_Item_m16041_gshared/* 2102*/,
	(methodPointerType)&ReadOnlyCollection_1_System_Collections_IList_set_Item_m16042_gshared/* 2103*/,
	(methodPointerType)&ReadOnlyCollection_1_Contains_m16043_gshared/* 2104*/,
	(methodPointerType)&ReadOnlyCollection_1_CopyTo_m16044_gshared/* 2105*/,
	(methodPointerType)&ReadOnlyCollection_1_GetEnumerator_m16045_gshared/* 2106*/,
	(methodPointerType)&ReadOnlyCollection_1_IndexOf_m16046_gshared/* 2107*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Count_m16047_gshared/* 2108*/,
	(methodPointerType)&ReadOnlyCollection_1_get_Item_m16048_gshared/* 2109*/,
	(methodPointerType)&Collection_1__ctor_m16049_gshared/* 2110*/,
	(methodPointerType)&Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16050_gshared/* 2111*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_CopyTo_m16051_gshared/* 2112*/,
	(methodPointerType)&Collection_1_System_Collections_IEnumerable_GetEnumerator_m16052_gshared/* 2113*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Add_m16053_gshared/* 2114*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Contains_m16054_gshared/* 2115*/,
	(methodPointerType)&Collection_1_System_Collections_IList_IndexOf_m16055_gshared/* 2116*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Insert_m16056_gshared/* 2117*/,
	(methodPointerType)&Collection_1_System_Collections_IList_Remove_m16057_gshared/* 2118*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_IsSynchronized_m16058_gshared/* 2119*/,
	(methodPointerType)&Collection_1_System_Collections_ICollection_get_SyncRoot_m16059_gshared/* 2120*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsFixedSize_m16060_gshared/* 2121*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_IsReadOnly_m16061_gshared/* 2122*/,
	(methodPointerType)&Collection_1_System_Collections_IList_get_Item_m16062_gshared/* 2123*/,
	(methodPointerType)&Collection_1_System_Collections_IList_set_Item_m16063_gshared/* 2124*/,
	(methodPointerType)&Collection_1_Add_m16064_gshared/* 2125*/,
	(methodPointerType)&Collection_1_Clear_m16065_gshared/* 2126*/,
	(methodPointerType)&Collection_1_ClearItems_m16066_gshared/* 2127*/,
	(methodPointerType)&Collection_1_Contains_m16067_gshared/* 2128*/,
	(methodPointerType)&Collection_1_CopyTo_m16068_gshared/* 2129*/,
	(methodPointerType)&Collection_1_GetEnumerator_m16069_gshared/* 2130*/,
	(methodPointerType)&Collection_1_IndexOf_m16070_gshared/* 2131*/,
	(methodPointerType)&Collection_1_Insert_m16071_gshared/* 2132*/,
	(methodPointerType)&Collection_1_InsertItem_m16072_gshared/* 2133*/,
	(methodPointerType)&Collection_1_Remove_m16073_gshared/* 2134*/,
	(methodPointerType)&Collection_1_RemoveAt_m16074_gshared/* 2135*/,
	(methodPointerType)&Collection_1_RemoveItem_m16075_gshared/* 2136*/,
	(methodPointerType)&Collection_1_get_Count_m16076_gshared/* 2137*/,
	(methodPointerType)&Collection_1_get_Item_m16077_gshared/* 2138*/,
	(methodPointerType)&Collection_1_set_Item_m16078_gshared/* 2139*/,
	(methodPointerType)&Collection_1_SetItem_m16079_gshared/* 2140*/,
	(methodPointerType)&Collection_1_IsValidItem_m16080_gshared/* 2141*/,
	(methodPointerType)&Collection_1_ConvertItem_m16081_gshared/* 2142*/,
	(methodPointerType)&Collection_1_CheckWritable_m16082_gshared/* 2143*/,
	(methodPointerType)&Collection_1_IsSynchronized_m16083_gshared/* 2144*/,
	(methodPointerType)&Collection_1_IsFixedSize_m16084_gshared/* 2145*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16085_gshared/* 2146*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16086_gshared/* 2147*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16087_gshared/* 2148*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16088_gshared/* 2149*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16089_gshared/* 2150*/,
	(methodPointerType)&DefaultComparer__ctor_m16090_gshared/* 2151*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16091_gshared/* 2152*/,
	(methodPointerType)&DefaultComparer_Equals_m16092_gshared/* 2153*/,
	(methodPointerType)&Predicate_1__ctor_m16093_gshared/* 2154*/,
	(methodPointerType)&Predicate_1_Invoke_m16094_gshared/* 2155*/,
	(methodPointerType)&Predicate_1_BeginInvoke_m16095_gshared/* 2156*/,
	(methodPointerType)&Predicate_1_EndInvoke_m16096_gshared/* 2157*/,
	(methodPointerType)&Comparer_1__ctor_m16097_gshared/* 2158*/,
	(methodPointerType)&Comparer_1__cctor_m16098_gshared/* 2159*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m16099_gshared/* 2160*/,
	(methodPointerType)&Comparer_1_get_Default_m16100_gshared/* 2161*/,
	(methodPointerType)&DefaultComparer__ctor_m16101_gshared/* 2162*/,
	(methodPointerType)&DefaultComparer_Compare_m16102_gshared/* 2163*/,
	(methodPointerType)&Comparison_1__ctor_m16103_gshared/* 2164*/,
	(methodPointerType)&Comparison_1_Invoke_m16104_gshared/* 2165*/,
	(methodPointerType)&Comparison_1_BeginInvoke_m16105_gshared/* 2166*/,
	(methodPointerType)&Comparison_1_EndInvoke_m16106_gshared/* 2167*/,
	(methodPointerType)&Dictionary_2__ctor_m16108_gshared/* 2168*/,
	(methodPointerType)&Dictionary_2__ctor_m16110_gshared/* 2169*/,
	(methodPointerType)&Dictionary_2__ctor_m16112_gshared/* 2170*/,
	(methodPointerType)&Dictionary_2__ctor_m16114_gshared/* 2171*/,
	(methodPointerType)&Dictionary_2__ctor_m16116_gshared/* 2172*/,
	(methodPointerType)&Dictionary_2__ctor_m16118_gshared/* 2173*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16120_gshared/* 2174*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16122_gshared/* 2175*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16124_gshared/* 2176*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16126_gshared/* 2177*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16128_gshared/* 2178*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16130_gshared/* 2179*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16132_gshared/* 2180*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16134_gshared/* 2181*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16136_gshared/* 2182*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16138_gshared/* 2183*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16140_gshared/* 2184*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16142_gshared/* 2185*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16144_gshared/* 2186*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16146_gshared/* 2187*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16148_gshared/* 2188*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16150_gshared/* 2189*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16152_gshared/* 2190*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16154_gshared/* 2191*/,
	(methodPointerType)&Dictionary_2_get_Count_m16156_gshared/* 2192*/,
	(methodPointerType)&Dictionary_2_get_Item_m16158_gshared/* 2193*/,
	(methodPointerType)&Dictionary_2_set_Item_m16160_gshared/* 2194*/,
	(methodPointerType)&Dictionary_2_Init_m16162_gshared/* 2195*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16164_gshared/* 2196*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16166_gshared/* 2197*/,
	(methodPointerType)&Dictionary_2_make_pair_m16168_gshared/* 2198*/,
	(methodPointerType)&Dictionary_2_pick_key_m16170_gshared/* 2199*/,
	(methodPointerType)&Dictionary_2_pick_value_m16172_gshared/* 2200*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16174_gshared/* 2201*/,
	(methodPointerType)&Dictionary_2_Resize_m16176_gshared/* 2202*/,
	(methodPointerType)&Dictionary_2_Add_m16178_gshared/* 2203*/,
	(methodPointerType)&Dictionary_2_Clear_m16180_gshared/* 2204*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16182_gshared/* 2205*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16184_gshared/* 2206*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16186_gshared/* 2207*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16188_gshared/* 2208*/,
	(methodPointerType)&Dictionary_2_Remove_m16190_gshared/* 2209*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16192_gshared/* 2210*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16194_gshared/* 2211*/,
	(methodPointerType)&Dictionary_2_get_Values_m16196_gshared/* 2212*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16198_gshared/* 2213*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16200_gshared/* 2214*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16202_gshared/* 2215*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16204_gshared/* 2216*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16206_gshared/* 2217*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16207_gshared/* 2218*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16208_gshared/* 2219*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16209_gshared/* 2220*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16210_gshared/* 2221*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16211_gshared/* 2222*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16212_gshared/* 2223*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16213_gshared/* 2224*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16214_gshared/* 2225*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16215_gshared/* 2226*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16216_gshared/* 2227*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16217_gshared/* 2228*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16218_gshared/* 2229*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16219_gshared/* 2230*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16220_gshared/* 2231*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16221_gshared/* 2232*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16222_gshared/* 2233*/,
	(methodPointerType)&KeyCollection__ctor_m16223_gshared/* 2234*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16224_gshared/* 2235*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16225_gshared/* 2236*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16226_gshared/* 2237*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16227_gshared/* 2238*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16228_gshared/* 2239*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16229_gshared/* 2240*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16230_gshared/* 2241*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16231_gshared/* 2242*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16232_gshared/* 2243*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16233_gshared/* 2244*/,
	(methodPointerType)&KeyCollection_CopyTo_m16234_gshared/* 2245*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16235_gshared/* 2246*/,
	(methodPointerType)&KeyCollection_get_Count_m16236_gshared/* 2247*/,
	(methodPointerType)&Enumerator__ctor_m16237_gshared/* 2248*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16238_gshared/* 2249*/,
	(methodPointerType)&Enumerator_Dispose_m16239_gshared/* 2250*/,
	(methodPointerType)&Enumerator_MoveNext_m16240_gshared/* 2251*/,
	(methodPointerType)&Enumerator_get_Current_m16241_gshared/* 2252*/,
	(methodPointerType)&Enumerator__ctor_m16242_gshared/* 2253*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16243_gshared/* 2254*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244_gshared/* 2255*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245_gshared/* 2256*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246_gshared/* 2257*/,
	(methodPointerType)&Enumerator_MoveNext_m16247_gshared/* 2258*/,
	(methodPointerType)&Enumerator_get_Current_m16248_gshared/* 2259*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16249_gshared/* 2260*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16250_gshared/* 2261*/,
	(methodPointerType)&Enumerator_VerifyState_m16251_gshared/* 2262*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16252_gshared/* 2263*/,
	(methodPointerType)&Enumerator_Dispose_m16253_gshared/* 2264*/,
	(methodPointerType)&Transform_1__ctor_m16254_gshared/* 2265*/,
	(methodPointerType)&Transform_1_Invoke_m16255_gshared/* 2266*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16256_gshared/* 2267*/,
	(methodPointerType)&Transform_1_EndInvoke_m16257_gshared/* 2268*/,
	(methodPointerType)&ValueCollection__ctor_m16258_gshared/* 2269*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16259_gshared/* 2270*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16260_gshared/* 2271*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16261_gshared/* 2272*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16262_gshared/* 2273*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16263_gshared/* 2274*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16264_gshared/* 2275*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16265_gshared/* 2276*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16266_gshared/* 2277*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16267_gshared/* 2278*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16268_gshared/* 2279*/,
	(methodPointerType)&ValueCollection_CopyTo_m16269_gshared/* 2280*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16270_gshared/* 2281*/,
	(methodPointerType)&ValueCollection_get_Count_m16271_gshared/* 2282*/,
	(methodPointerType)&Enumerator__ctor_m16272_gshared/* 2283*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16273_gshared/* 2284*/,
	(methodPointerType)&Enumerator_Dispose_m16274_gshared/* 2285*/,
	(methodPointerType)&Enumerator_MoveNext_m16275_gshared/* 2286*/,
	(methodPointerType)&Enumerator_get_Current_m16276_gshared/* 2287*/,
	(methodPointerType)&Transform_1__ctor_m16277_gshared/* 2288*/,
	(methodPointerType)&Transform_1_Invoke_m16278_gshared/* 2289*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16279_gshared/* 2290*/,
	(methodPointerType)&Transform_1_EndInvoke_m16280_gshared/* 2291*/,
	(methodPointerType)&Transform_1__ctor_m16281_gshared/* 2292*/,
	(methodPointerType)&Transform_1_Invoke_m16282_gshared/* 2293*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16283_gshared/* 2294*/,
	(methodPointerType)&Transform_1_EndInvoke_m16284_gshared/* 2295*/,
	(methodPointerType)&Transform_1__ctor_m16285_gshared/* 2296*/,
	(methodPointerType)&Transform_1_Invoke_m16286_gshared/* 2297*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16287_gshared/* 2298*/,
	(methodPointerType)&Transform_1_EndInvoke_m16288_gshared/* 2299*/,
	(methodPointerType)&ShimEnumerator__ctor_m16289_gshared/* 2300*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16290_gshared/* 2301*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16291_gshared/* 2302*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16292_gshared/* 2303*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16293_gshared/* 2304*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16294_gshared/* 2305*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16295_gshared/* 2306*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16296_gshared/* 2307*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16297_gshared/* 2308*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16298_gshared/* 2309*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16299_gshared/* 2310*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16300_gshared/* 2311*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16301_gshared/* 2312*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16302_gshared/* 2313*/,
	(methodPointerType)&DefaultComparer__ctor_m16303_gshared/* 2314*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16304_gshared/* 2315*/,
	(methodPointerType)&DefaultComparer_Equals_m16305_gshared/* 2316*/,
	(methodPointerType)&Dictionary_2__ctor_m16544_gshared/* 2317*/,
	(methodPointerType)&Dictionary_2__ctor_m16546_gshared/* 2318*/,
	(methodPointerType)&Dictionary_2__ctor_m16548_gshared/* 2319*/,
	(methodPointerType)&Dictionary_2__ctor_m16550_gshared/* 2320*/,
	(methodPointerType)&Dictionary_2__ctor_m16552_gshared/* 2321*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16554_gshared/* 2322*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16556_gshared/* 2323*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m16558_gshared/* 2324*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m16560_gshared/* 2325*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m16562_gshared/* 2326*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m16564_gshared/* 2327*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m16566_gshared/* 2328*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16568_gshared/* 2329*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16570_gshared/* 2330*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16572_gshared/* 2331*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16574_gshared/* 2332*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16576_gshared/* 2333*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16578_gshared/* 2334*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16580_gshared/* 2335*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m16582_gshared/* 2336*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16584_gshared/* 2337*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16586_gshared/* 2338*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16588_gshared/* 2339*/,
	(methodPointerType)&Dictionary_2_get_Count_m16590_gshared/* 2340*/,
	(methodPointerType)&Dictionary_2_get_Item_m16592_gshared/* 2341*/,
	(methodPointerType)&Dictionary_2_set_Item_m16594_gshared/* 2342*/,
	(methodPointerType)&Dictionary_2_Init_m16596_gshared/* 2343*/,
	(methodPointerType)&Dictionary_2_InitArrays_m16598_gshared/* 2344*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m16600_gshared/* 2345*/,
	(methodPointerType)&Dictionary_2_make_pair_m16602_gshared/* 2346*/,
	(methodPointerType)&Dictionary_2_pick_key_m16604_gshared/* 2347*/,
	(methodPointerType)&Dictionary_2_pick_value_m16606_gshared/* 2348*/,
	(methodPointerType)&Dictionary_2_CopyTo_m16608_gshared/* 2349*/,
	(methodPointerType)&Dictionary_2_Resize_m16610_gshared/* 2350*/,
	(methodPointerType)&Dictionary_2_Add_m16612_gshared/* 2351*/,
	(methodPointerType)&Dictionary_2_Clear_m16614_gshared/* 2352*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m16616_gshared/* 2353*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m16618_gshared/* 2354*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m16620_gshared/* 2355*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m16622_gshared/* 2356*/,
	(methodPointerType)&Dictionary_2_Remove_m16624_gshared/* 2357*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m16626_gshared/* 2358*/,
	(methodPointerType)&Dictionary_2_get_Keys_m16628_gshared/* 2359*/,
	(methodPointerType)&Dictionary_2_get_Values_m16630_gshared/* 2360*/,
	(methodPointerType)&Dictionary_2_ToTKey_m16632_gshared/* 2361*/,
	(methodPointerType)&Dictionary_2_ToTValue_m16634_gshared/* 2362*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m16636_gshared/* 2363*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m16638_gshared/* 2364*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m16640_gshared/* 2365*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16641_gshared/* 2366*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16642_gshared/* 2367*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16643_gshared/* 2368*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16644_gshared/* 2369*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16645_gshared/* 2370*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16646_gshared/* 2371*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16647_gshared/* 2372*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16648_gshared/* 2373*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16649_gshared/* 2374*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16650_gshared/* 2375*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16651_gshared/* 2376*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16652_gshared/* 2377*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16653_gshared/* 2378*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16654_gshared/* 2379*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16655_gshared/* 2380*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16656_gshared/* 2381*/,
	(methodPointerType)&KeyCollection__ctor_m16657_gshared/* 2382*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m16658_gshared/* 2383*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m16659_gshared/* 2384*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m16660_gshared/* 2385*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m16661_gshared/* 2386*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m16662_gshared/* 2387*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m16663_gshared/* 2388*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m16664_gshared/* 2389*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m16665_gshared/* 2390*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m16666_gshared/* 2391*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m16667_gshared/* 2392*/,
	(methodPointerType)&KeyCollection_CopyTo_m16668_gshared/* 2393*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m16669_gshared/* 2394*/,
	(methodPointerType)&KeyCollection_get_Count_m16670_gshared/* 2395*/,
	(methodPointerType)&Enumerator__ctor_m16671_gshared/* 2396*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16672_gshared/* 2397*/,
	(methodPointerType)&Enumerator_Dispose_m16673_gshared/* 2398*/,
	(methodPointerType)&Enumerator_MoveNext_m16674_gshared/* 2399*/,
	(methodPointerType)&Enumerator_get_Current_m16675_gshared/* 2400*/,
	(methodPointerType)&Enumerator__ctor_m16676_gshared/* 2401*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16677_gshared/* 2402*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16678_gshared/* 2403*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16679_gshared/* 2404*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16680_gshared/* 2405*/,
	(methodPointerType)&Enumerator_MoveNext_m16681_gshared/* 2406*/,
	(methodPointerType)&Enumerator_get_Current_m16682_gshared/* 2407*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m16683_gshared/* 2408*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m16684_gshared/* 2409*/,
	(methodPointerType)&Enumerator_VerifyState_m16685_gshared/* 2410*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m16686_gshared/* 2411*/,
	(methodPointerType)&Enumerator_Dispose_m16687_gshared/* 2412*/,
	(methodPointerType)&Transform_1__ctor_m16688_gshared/* 2413*/,
	(methodPointerType)&Transform_1_Invoke_m16689_gshared/* 2414*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16690_gshared/* 2415*/,
	(methodPointerType)&Transform_1_EndInvoke_m16691_gshared/* 2416*/,
	(methodPointerType)&ValueCollection__ctor_m16692_gshared/* 2417*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m16693_gshared/* 2418*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m16694_gshared/* 2419*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m16695_gshared/* 2420*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m16696_gshared/* 2421*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m16697_gshared/* 2422*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m16698_gshared/* 2423*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m16699_gshared/* 2424*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m16700_gshared/* 2425*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m16701_gshared/* 2426*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m16702_gshared/* 2427*/,
	(methodPointerType)&ValueCollection_CopyTo_m16703_gshared/* 2428*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m16704_gshared/* 2429*/,
	(methodPointerType)&ValueCollection_get_Count_m16705_gshared/* 2430*/,
	(methodPointerType)&Enumerator__ctor_m16706_gshared/* 2431*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m16707_gshared/* 2432*/,
	(methodPointerType)&Enumerator_Dispose_m16708_gshared/* 2433*/,
	(methodPointerType)&Enumerator_MoveNext_m16709_gshared/* 2434*/,
	(methodPointerType)&Enumerator_get_Current_m16710_gshared/* 2435*/,
	(methodPointerType)&Transform_1__ctor_m16711_gshared/* 2436*/,
	(methodPointerType)&Transform_1_Invoke_m16712_gshared/* 2437*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16713_gshared/* 2438*/,
	(methodPointerType)&Transform_1_EndInvoke_m16714_gshared/* 2439*/,
	(methodPointerType)&Transform_1__ctor_m16715_gshared/* 2440*/,
	(methodPointerType)&Transform_1_Invoke_m16716_gshared/* 2441*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16717_gshared/* 2442*/,
	(methodPointerType)&Transform_1_EndInvoke_m16718_gshared/* 2443*/,
	(methodPointerType)&Transform_1__ctor_m16719_gshared/* 2444*/,
	(methodPointerType)&Transform_1_Invoke_m16720_gshared/* 2445*/,
	(methodPointerType)&Transform_1_BeginInvoke_m16721_gshared/* 2446*/,
	(methodPointerType)&Transform_1_EndInvoke_m16722_gshared/* 2447*/,
	(methodPointerType)&ShimEnumerator__ctor_m16723_gshared/* 2448*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m16724_gshared/* 2449*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m16725_gshared/* 2450*/,
	(methodPointerType)&ShimEnumerator_get_Key_m16726_gshared/* 2451*/,
	(methodPointerType)&ShimEnumerator_get_Value_m16727_gshared/* 2452*/,
	(methodPointerType)&ShimEnumerator_get_Current_m16728_gshared/* 2453*/,
	(methodPointerType)&EqualityComparer_1__ctor_m16729_gshared/* 2454*/,
	(methodPointerType)&EqualityComparer_1__cctor_m16730_gshared/* 2455*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16731_gshared/* 2456*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16732_gshared/* 2457*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m16733_gshared/* 2458*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m16734_gshared/* 2459*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m16735_gshared/* 2460*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m16736_gshared/* 2461*/,
	(methodPointerType)&DefaultComparer__ctor_m16737_gshared/* 2462*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m16738_gshared/* 2463*/,
	(methodPointerType)&DefaultComparer_Equals_m16739_gshared/* 2464*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m16914_gshared/* 2465*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16915_gshared/* 2466*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m16916_gshared/* 2467*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m16917_gshared/* 2468*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m16918_gshared/* 2469*/,
	(methodPointerType)&KeyValuePair_2__ctor_m16919_gshared/* 2470*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m16920_gshared/* 2471*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m16921_gshared/* 2472*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m16922_gshared/* 2473*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m16923_gshared/* 2474*/,
	(methodPointerType)&KeyValuePair_2_ToString_m16924_gshared/* 2475*/,
	(methodPointerType)&Dictionary_2__ctor_m17278_gshared/* 2476*/,
	(methodPointerType)&Dictionary_2__ctor_m17280_gshared/* 2477*/,
	(methodPointerType)&Dictionary_2__ctor_m17282_gshared/* 2478*/,
	(methodPointerType)&Dictionary_2__ctor_m17284_gshared/* 2479*/,
	(methodPointerType)&Dictionary_2__ctor_m17286_gshared/* 2480*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17288_gshared/* 2481*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17290_gshared/* 2482*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m17292_gshared/* 2483*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m17294_gshared/* 2484*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m17296_gshared/* 2485*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m17298_gshared/* 2486*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m17300_gshared/* 2487*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17302_gshared/* 2488*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17304_gshared/* 2489*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17306_gshared/* 2490*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17308_gshared/* 2491*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17310_gshared/* 2492*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17312_gshared/* 2493*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17314_gshared/* 2494*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m17316_gshared/* 2495*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17318_gshared/* 2496*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17320_gshared/* 2497*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17322_gshared/* 2498*/,
	(methodPointerType)&Dictionary_2_get_Count_m17324_gshared/* 2499*/,
	(methodPointerType)&Dictionary_2_get_Item_m17326_gshared/* 2500*/,
	(methodPointerType)&Dictionary_2_set_Item_m17328_gshared/* 2501*/,
	(methodPointerType)&Dictionary_2_Init_m17330_gshared/* 2502*/,
	(methodPointerType)&Dictionary_2_InitArrays_m17332_gshared/* 2503*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m17334_gshared/* 2504*/,
	(methodPointerType)&Dictionary_2_make_pair_m17336_gshared/* 2505*/,
	(methodPointerType)&Dictionary_2_pick_key_m17338_gshared/* 2506*/,
	(methodPointerType)&Dictionary_2_pick_value_m17340_gshared/* 2507*/,
	(methodPointerType)&Dictionary_2_CopyTo_m17342_gshared/* 2508*/,
	(methodPointerType)&Dictionary_2_Resize_m17344_gshared/* 2509*/,
	(methodPointerType)&Dictionary_2_Add_m17346_gshared/* 2510*/,
	(methodPointerType)&Dictionary_2_Clear_m17348_gshared/* 2511*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m17350_gshared/* 2512*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m17352_gshared/* 2513*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m17354_gshared/* 2514*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m17356_gshared/* 2515*/,
	(methodPointerType)&Dictionary_2_Remove_m17358_gshared/* 2516*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m17360_gshared/* 2517*/,
	(methodPointerType)&Dictionary_2_get_Keys_m17362_gshared/* 2518*/,
	(methodPointerType)&Dictionary_2_get_Values_m17364_gshared/* 2519*/,
	(methodPointerType)&Dictionary_2_ToTKey_m17366_gshared/* 2520*/,
	(methodPointerType)&Dictionary_2_ToTValue_m17368_gshared/* 2521*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m17370_gshared/* 2522*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m17372_gshared/* 2523*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m17374_gshared/* 2524*/,
	(methodPointerType)&KeyCollection__ctor_m17375_gshared/* 2525*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m17376_gshared/* 2526*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m17377_gshared/* 2527*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m17378_gshared/* 2528*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m17379_gshared/* 2529*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m17380_gshared/* 2530*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m17381_gshared/* 2531*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m17382_gshared/* 2532*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m17383_gshared/* 2533*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m17384_gshared/* 2534*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m17385_gshared/* 2535*/,
	(methodPointerType)&KeyCollection_CopyTo_m17386_gshared/* 2536*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m17387_gshared/* 2537*/,
	(methodPointerType)&KeyCollection_get_Count_m17388_gshared/* 2538*/,
	(methodPointerType)&Enumerator__ctor_m17389_gshared/* 2539*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17390_gshared/* 2540*/,
	(methodPointerType)&Enumerator_Dispose_m17391_gshared/* 2541*/,
	(methodPointerType)&Enumerator_MoveNext_m17392_gshared/* 2542*/,
	(methodPointerType)&Enumerator_get_Current_m17393_gshared/* 2543*/,
	(methodPointerType)&Enumerator__ctor_m17394_gshared/* 2544*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17395_gshared/* 2545*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17396_gshared/* 2546*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17397_gshared/* 2547*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17398_gshared/* 2548*/,
	(methodPointerType)&Enumerator_MoveNext_m17399_gshared/* 2549*/,
	(methodPointerType)&Enumerator_get_Current_m17400_gshared/* 2550*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m17401_gshared/* 2551*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m17402_gshared/* 2552*/,
	(methodPointerType)&Enumerator_VerifyState_m17403_gshared/* 2553*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m17404_gshared/* 2554*/,
	(methodPointerType)&Enumerator_Dispose_m17405_gshared/* 2555*/,
	(methodPointerType)&Transform_1__ctor_m17406_gshared/* 2556*/,
	(methodPointerType)&Transform_1_Invoke_m17407_gshared/* 2557*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17408_gshared/* 2558*/,
	(methodPointerType)&Transform_1_EndInvoke_m17409_gshared/* 2559*/,
	(methodPointerType)&ValueCollection__ctor_m17410_gshared/* 2560*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17411_gshared/* 2561*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17412_gshared/* 2562*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17413_gshared/* 2563*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17414_gshared/* 2564*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17415_gshared/* 2565*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m17416_gshared/* 2566*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17417_gshared/* 2567*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17418_gshared/* 2568*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17419_gshared/* 2569*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m17420_gshared/* 2570*/,
	(methodPointerType)&ValueCollection_CopyTo_m17421_gshared/* 2571*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m17422_gshared/* 2572*/,
	(methodPointerType)&ValueCollection_get_Count_m17423_gshared/* 2573*/,
	(methodPointerType)&Enumerator__ctor_m17424_gshared/* 2574*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m17425_gshared/* 2575*/,
	(methodPointerType)&Enumerator_Dispose_m17426_gshared/* 2576*/,
	(methodPointerType)&Enumerator_MoveNext_m17427_gshared/* 2577*/,
	(methodPointerType)&Enumerator_get_Current_m17428_gshared/* 2578*/,
	(methodPointerType)&Transform_1__ctor_m17429_gshared/* 2579*/,
	(methodPointerType)&Transform_1_Invoke_m17430_gshared/* 2580*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17431_gshared/* 2581*/,
	(methodPointerType)&Transform_1_EndInvoke_m17432_gshared/* 2582*/,
	(methodPointerType)&Transform_1__ctor_m17433_gshared/* 2583*/,
	(methodPointerType)&Transform_1_Invoke_m17434_gshared/* 2584*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17435_gshared/* 2585*/,
	(methodPointerType)&Transform_1_EndInvoke_m17436_gshared/* 2586*/,
	(methodPointerType)&Transform_1__ctor_m17437_gshared/* 2587*/,
	(methodPointerType)&Transform_1_Invoke_m17438_gshared/* 2588*/,
	(methodPointerType)&Transform_1_BeginInvoke_m17439_gshared/* 2589*/,
	(methodPointerType)&Transform_1_EndInvoke_m17440_gshared/* 2590*/,
	(methodPointerType)&ShimEnumerator__ctor_m17441_gshared/* 2591*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m17442_gshared/* 2592*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m17443_gshared/* 2593*/,
	(methodPointerType)&ShimEnumerator_get_Key_m17444_gshared/* 2594*/,
	(methodPointerType)&ShimEnumerator_get_Value_m17445_gshared/* 2595*/,
	(methodPointerType)&ShimEnumerator_get_Current_m17446_gshared/* 2596*/,
	(methodPointerType)&EqualityComparer_1__ctor_m17447_gshared/* 2597*/,
	(methodPointerType)&EqualityComparer_1__cctor_m17448_gshared/* 2598*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m17449_gshared/* 2599*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m17450_gshared/* 2600*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m17451_gshared/* 2601*/,
	(methodPointerType)&DefaultComparer__ctor_m17452_gshared/* 2602*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m17453_gshared/* 2603*/,
	(methodPointerType)&DefaultComparer_Equals_m17454_gshared/* 2604*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17644_gshared/* 2605*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17645_gshared/* 2606*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17646_gshared/* 2607*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17647_gshared/* 2608*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17648_gshared/* 2609*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m17649_gshared/* 2610*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17650_gshared/* 2611*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m17651_gshared/* 2612*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m17652_gshared/* 2613*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m17653_gshared/* 2614*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m17777_gshared/* 2615*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m17778_gshared/* 2616*/,
	(methodPointerType)&InvokableCall_1__ctor_m17779_gshared/* 2617*/,
	(methodPointerType)&InvokableCall_1__ctor_m17780_gshared/* 2618*/,
	(methodPointerType)&InvokableCall_1_Invoke_m17781_gshared/* 2619*/,
	(methodPointerType)&InvokableCall_1_Find_m17782_gshared/* 2620*/,
	(methodPointerType)&UnityAction_1__ctor_m17783_gshared/* 2621*/,
	(methodPointerType)&UnityAction_1_Invoke_m17784_gshared/* 2622*/,
	(methodPointerType)&UnityAction_1_BeginInvoke_m17785_gshared/* 2623*/,
	(methodPointerType)&UnityAction_1_EndInvoke_m17786_gshared/* 2624*/,
	(methodPointerType)&CachedInvokableCall_1_Invoke_m17794_gshared/* 2625*/,
	(methodPointerType)&Dictionary_2__ctor_m17991_gshared/* 2626*/,
	(methodPointerType)&Dictionary_2__ctor_m17994_gshared/* 2627*/,
	(methodPointerType)&Dictionary_2__ctor_m17996_gshared/* 2628*/,
	(methodPointerType)&Dictionary_2__ctor_m17998_gshared/* 2629*/,
	(methodPointerType)&Dictionary_2__ctor_m18000_gshared/* 2630*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18002_gshared/* 2631*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18004_gshared/* 2632*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m18006_gshared/* 2633*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m18008_gshared/* 2634*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m18010_gshared/* 2635*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m18012_gshared/* 2636*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m18014_gshared/* 2637*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18016_gshared/* 2638*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18018_gshared/* 2639*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18020_gshared/* 2640*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18022_gshared/* 2641*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18024_gshared/* 2642*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18026_gshared/* 2643*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18028_gshared/* 2644*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m18030_gshared/* 2645*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18032_gshared/* 2646*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18034_gshared/* 2647*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18036_gshared/* 2648*/,
	(methodPointerType)&Dictionary_2_get_Count_m18038_gshared/* 2649*/,
	(methodPointerType)&Dictionary_2_get_Item_m18040_gshared/* 2650*/,
	(methodPointerType)&Dictionary_2_set_Item_m18042_gshared/* 2651*/,
	(methodPointerType)&Dictionary_2_Init_m18044_gshared/* 2652*/,
	(methodPointerType)&Dictionary_2_InitArrays_m18046_gshared/* 2653*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m18048_gshared/* 2654*/,
	(methodPointerType)&Dictionary_2_make_pair_m18050_gshared/* 2655*/,
	(methodPointerType)&Dictionary_2_pick_key_m18052_gshared/* 2656*/,
	(methodPointerType)&Dictionary_2_pick_value_m18054_gshared/* 2657*/,
	(methodPointerType)&Dictionary_2_CopyTo_m18056_gshared/* 2658*/,
	(methodPointerType)&Dictionary_2_Resize_m18058_gshared/* 2659*/,
	(methodPointerType)&Dictionary_2_Add_m18060_gshared/* 2660*/,
	(methodPointerType)&Dictionary_2_Clear_m18062_gshared/* 2661*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m18064_gshared/* 2662*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m18066_gshared/* 2663*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m18068_gshared/* 2664*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m18070_gshared/* 2665*/,
	(methodPointerType)&Dictionary_2_Remove_m18072_gshared/* 2666*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m18074_gshared/* 2667*/,
	(methodPointerType)&Dictionary_2_get_Keys_m18076_gshared/* 2668*/,
	(methodPointerType)&Dictionary_2_get_Values_m18078_gshared/* 2669*/,
	(methodPointerType)&Dictionary_2_ToTKey_m18080_gshared/* 2670*/,
	(methodPointerType)&Dictionary_2_ToTValue_m18082_gshared/* 2671*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m18084_gshared/* 2672*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m18086_gshared/* 2673*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m18088_gshared/* 2674*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18089_gshared/* 2675*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18090_gshared/* 2676*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18091_gshared/* 2677*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18092_gshared/* 2678*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18093_gshared/* 2679*/,
	(methodPointerType)&KeyValuePair_2__ctor_m18094_gshared/* 2680*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m18095_gshared/* 2681*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m18096_gshared/* 2682*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m18097_gshared/* 2683*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m18098_gshared/* 2684*/,
	(methodPointerType)&KeyValuePair_2_ToString_m18099_gshared/* 2685*/,
	(methodPointerType)&KeyCollection__ctor_m18100_gshared/* 2686*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18101_gshared/* 2687*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18102_gshared/* 2688*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18103_gshared/* 2689*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18104_gshared/* 2690*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18105_gshared/* 2691*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m18106_gshared/* 2692*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18107_gshared/* 2693*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18108_gshared/* 2694*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18109_gshared/* 2695*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m18110_gshared/* 2696*/,
	(methodPointerType)&KeyCollection_CopyTo_m18111_gshared/* 2697*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m18112_gshared/* 2698*/,
	(methodPointerType)&KeyCollection_get_Count_m18113_gshared/* 2699*/,
	(methodPointerType)&Enumerator__ctor_m18114_gshared/* 2700*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18115_gshared/* 2701*/,
	(methodPointerType)&Enumerator_Dispose_m18116_gshared/* 2702*/,
	(methodPointerType)&Enumerator_MoveNext_m18117_gshared/* 2703*/,
	(methodPointerType)&Enumerator_get_Current_m18118_gshared/* 2704*/,
	(methodPointerType)&Enumerator__ctor_m18119_gshared/* 2705*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18120_gshared/* 2706*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121_gshared/* 2707*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122_gshared/* 2708*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123_gshared/* 2709*/,
	(methodPointerType)&Enumerator_MoveNext_m18124_gshared/* 2710*/,
	(methodPointerType)&Enumerator_get_Current_m18125_gshared/* 2711*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m18126_gshared/* 2712*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m18127_gshared/* 2713*/,
	(methodPointerType)&Enumerator_VerifyState_m18128_gshared/* 2714*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m18129_gshared/* 2715*/,
	(methodPointerType)&Enumerator_Dispose_m18130_gshared/* 2716*/,
	(methodPointerType)&Transform_1__ctor_m18131_gshared/* 2717*/,
	(methodPointerType)&Transform_1_Invoke_m18132_gshared/* 2718*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18133_gshared/* 2719*/,
	(methodPointerType)&Transform_1_EndInvoke_m18134_gshared/* 2720*/,
	(methodPointerType)&ValueCollection__ctor_m18135_gshared/* 2721*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18136_gshared/* 2722*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18137_gshared/* 2723*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18138_gshared/* 2724*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18139_gshared/* 2725*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18140_gshared/* 2726*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m18141_gshared/* 2727*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18142_gshared/* 2728*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18143_gshared/* 2729*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18144_gshared/* 2730*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m18145_gshared/* 2731*/,
	(methodPointerType)&ValueCollection_CopyTo_m18146_gshared/* 2732*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m18147_gshared/* 2733*/,
	(methodPointerType)&ValueCollection_get_Count_m18148_gshared/* 2734*/,
	(methodPointerType)&Enumerator__ctor_m18149_gshared/* 2735*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18150_gshared/* 2736*/,
	(methodPointerType)&Enumerator_Dispose_m18151_gshared/* 2737*/,
	(methodPointerType)&Enumerator_MoveNext_m18152_gshared/* 2738*/,
	(methodPointerType)&Enumerator_get_Current_m18153_gshared/* 2739*/,
	(methodPointerType)&Transform_1__ctor_m18154_gshared/* 2740*/,
	(methodPointerType)&Transform_1_Invoke_m18155_gshared/* 2741*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18156_gshared/* 2742*/,
	(methodPointerType)&Transform_1_EndInvoke_m18157_gshared/* 2743*/,
	(methodPointerType)&Transform_1__ctor_m18158_gshared/* 2744*/,
	(methodPointerType)&Transform_1_Invoke_m18159_gshared/* 2745*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18160_gshared/* 2746*/,
	(methodPointerType)&Transform_1_EndInvoke_m18161_gshared/* 2747*/,
	(methodPointerType)&Transform_1__ctor_m18162_gshared/* 2748*/,
	(methodPointerType)&Transform_1_Invoke_m18163_gshared/* 2749*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18164_gshared/* 2750*/,
	(methodPointerType)&Transform_1_EndInvoke_m18165_gshared/* 2751*/,
	(methodPointerType)&ShimEnumerator__ctor_m18166_gshared/* 2752*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m18167_gshared/* 2753*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m18168_gshared/* 2754*/,
	(methodPointerType)&ShimEnumerator_get_Key_m18169_gshared/* 2755*/,
	(methodPointerType)&ShimEnumerator_get_Value_m18170_gshared/* 2756*/,
	(methodPointerType)&ShimEnumerator_get_Current_m18171_gshared/* 2757*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18172_gshared/* 2758*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18173_gshared/* 2759*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18174_gshared/* 2760*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18175_gshared/* 2761*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18176_gshared/* 2762*/,
	(methodPointerType)&GenericEqualityComparer_1__ctor_m18177_gshared/* 2763*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18178_gshared/* 2764*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18179_gshared/* 2765*/,
	(methodPointerType)&DefaultComparer__ctor_m18180_gshared/* 2766*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18181_gshared/* 2767*/,
	(methodPointerType)&DefaultComparer_Equals_m18182_gshared/* 2768*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18238_gshared/* 2769*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18239_gshared/* 2770*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18240_gshared/* 2771*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18241_gshared/* 2772*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18242_gshared/* 2773*/,
	(methodPointerType)&Dictionary_2__ctor_m18253_gshared/* 2774*/,
	(methodPointerType)&Dictionary_2__ctor_m18254_gshared/* 2775*/,
	(methodPointerType)&Dictionary_2__ctor_m18255_gshared/* 2776*/,
	(methodPointerType)&Dictionary_2__ctor_m18256_gshared/* 2777*/,
	(methodPointerType)&Dictionary_2__ctor_m18257_gshared/* 2778*/,
	(methodPointerType)&Dictionary_2__ctor_m18258_gshared/* 2779*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18259_gshared/* 2780*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18260_gshared/* 2781*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_get_Item_m18261_gshared/* 2782*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_set_Item_m18262_gshared/* 2783*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Add_m18263_gshared/* 2784*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Contains_m18264_gshared/* 2785*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_Remove_m18265_gshared/* 2786*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18266_gshared/* 2787*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18267_gshared/* 2788*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18268_gshared/* 2789*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18269_gshared/* 2790*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18270_gshared/* 2791*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18271_gshared/* 2792*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18272_gshared/* 2793*/,
	(methodPointerType)&Dictionary_2_System_Collections_ICollection_CopyTo_m18273_gshared/* 2794*/,
	(methodPointerType)&Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18274_gshared/* 2795*/,
	(methodPointerType)&Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18275_gshared/* 2796*/,
	(methodPointerType)&Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18276_gshared/* 2797*/,
	(methodPointerType)&Dictionary_2_get_Count_m18277_gshared/* 2798*/,
	(methodPointerType)&Dictionary_2_get_Item_m18278_gshared/* 2799*/,
	(methodPointerType)&Dictionary_2_set_Item_m18279_gshared/* 2800*/,
	(methodPointerType)&Dictionary_2_Init_m18280_gshared/* 2801*/,
	(methodPointerType)&Dictionary_2_InitArrays_m18281_gshared/* 2802*/,
	(methodPointerType)&Dictionary_2_CopyToCheck_m18282_gshared/* 2803*/,
	(methodPointerType)&Dictionary_2_make_pair_m18283_gshared/* 2804*/,
	(methodPointerType)&Dictionary_2_pick_key_m18284_gshared/* 2805*/,
	(methodPointerType)&Dictionary_2_pick_value_m18285_gshared/* 2806*/,
	(methodPointerType)&Dictionary_2_CopyTo_m18286_gshared/* 2807*/,
	(methodPointerType)&Dictionary_2_Resize_m18287_gshared/* 2808*/,
	(methodPointerType)&Dictionary_2_Add_m18288_gshared/* 2809*/,
	(methodPointerType)&Dictionary_2_Clear_m18289_gshared/* 2810*/,
	(methodPointerType)&Dictionary_2_ContainsKey_m18290_gshared/* 2811*/,
	(methodPointerType)&Dictionary_2_ContainsValue_m18291_gshared/* 2812*/,
	(methodPointerType)&Dictionary_2_GetObjectData_m18292_gshared/* 2813*/,
	(methodPointerType)&Dictionary_2_OnDeserialization_m18293_gshared/* 2814*/,
	(methodPointerType)&Dictionary_2_Remove_m18294_gshared/* 2815*/,
	(methodPointerType)&Dictionary_2_TryGetValue_m18295_gshared/* 2816*/,
	(methodPointerType)&Dictionary_2_get_Keys_m18296_gshared/* 2817*/,
	(methodPointerType)&Dictionary_2_get_Values_m18297_gshared/* 2818*/,
	(methodPointerType)&Dictionary_2_ToTKey_m18298_gshared/* 2819*/,
	(methodPointerType)&Dictionary_2_ToTValue_m18299_gshared/* 2820*/,
	(methodPointerType)&Dictionary_2_ContainsKeyValuePair_m18300_gshared/* 2821*/,
	(methodPointerType)&Dictionary_2_GetEnumerator_m18301_gshared/* 2822*/,
	(methodPointerType)&Dictionary_2_U3CCopyToU3Em__0_m18302_gshared/* 2823*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18303_gshared/* 2824*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18304_gshared/* 2825*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18305_gshared/* 2826*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18306_gshared/* 2827*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18307_gshared/* 2828*/,
	(methodPointerType)&KeyValuePair_2__ctor_m18308_gshared/* 2829*/,
	(methodPointerType)&KeyValuePair_2_get_Key_m18309_gshared/* 2830*/,
	(methodPointerType)&KeyValuePair_2_set_Key_m18310_gshared/* 2831*/,
	(methodPointerType)&KeyValuePair_2_get_Value_m18311_gshared/* 2832*/,
	(methodPointerType)&KeyValuePair_2_set_Value_m18312_gshared/* 2833*/,
	(methodPointerType)&KeyValuePair_2_ToString_m18313_gshared/* 2834*/,
	(methodPointerType)&KeyCollection__ctor_m18314_gshared/* 2835*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m18315_gshared/* 2836*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m18316_gshared/* 2837*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m18317_gshared/* 2838*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m18318_gshared/* 2839*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m18319_gshared/* 2840*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_CopyTo_m18320_gshared/* 2841*/,
	(methodPointerType)&KeyCollection_System_Collections_IEnumerable_GetEnumerator_m18321_gshared/* 2842*/,
	(methodPointerType)&KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m18322_gshared/* 2843*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_IsSynchronized_m18323_gshared/* 2844*/,
	(methodPointerType)&KeyCollection_System_Collections_ICollection_get_SyncRoot_m18324_gshared/* 2845*/,
	(methodPointerType)&KeyCollection_CopyTo_m18325_gshared/* 2846*/,
	(methodPointerType)&KeyCollection_GetEnumerator_m18326_gshared/* 2847*/,
	(methodPointerType)&KeyCollection_get_Count_m18327_gshared/* 2848*/,
	(methodPointerType)&Enumerator__ctor_m18328_gshared/* 2849*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18329_gshared/* 2850*/,
	(methodPointerType)&Enumerator_Dispose_m18330_gshared/* 2851*/,
	(methodPointerType)&Enumerator_MoveNext_m18331_gshared/* 2852*/,
	(methodPointerType)&Enumerator_get_Current_m18332_gshared/* 2853*/,
	(methodPointerType)&Enumerator__ctor_m18333_gshared/* 2854*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18334_gshared/* 2855*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18335_gshared/* 2856*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18336_gshared/* 2857*/,
	(methodPointerType)&Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18337_gshared/* 2858*/,
	(methodPointerType)&Enumerator_MoveNext_m18338_gshared/* 2859*/,
	(methodPointerType)&Enumerator_get_Current_m18339_gshared/* 2860*/,
	(methodPointerType)&Enumerator_get_CurrentKey_m18340_gshared/* 2861*/,
	(methodPointerType)&Enumerator_get_CurrentValue_m18341_gshared/* 2862*/,
	(methodPointerType)&Enumerator_VerifyState_m18342_gshared/* 2863*/,
	(methodPointerType)&Enumerator_VerifyCurrent_m18343_gshared/* 2864*/,
	(methodPointerType)&Enumerator_Dispose_m18344_gshared/* 2865*/,
	(methodPointerType)&Transform_1__ctor_m18345_gshared/* 2866*/,
	(methodPointerType)&Transform_1_Invoke_m18346_gshared/* 2867*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18347_gshared/* 2868*/,
	(methodPointerType)&Transform_1_EndInvoke_m18348_gshared/* 2869*/,
	(methodPointerType)&ValueCollection__ctor_m18349_gshared/* 2870*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m18350_gshared/* 2871*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m18351_gshared/* 2872*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m18352_gshared/* 2873*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m18353_gshared/* 2874*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m18354_gshared/* 2875*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_CopyTo_m18355_gshared/* 2876*/,
	(methodPointerType)&ValueCollection_System_Collections_IEnumerable_GetEnumerator_m18356_gshared/* 2877*/,
	(methodPointerType)&ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m18357_gshared/* 2878*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_IsSynchronized_m18358_gshared/* 2879*/,
	(methodPointerType)&ValueCollection_System_Collections_ICollection_get_SyncRoot_m18359_gshared/* 2880*/,
	(methodPointerType)&ValueCollection_CopyTo_m18360_gshared/* 2881*/,
	(methodPointerType)&ValueCollection_GetEnumerator_m18361_gshared/* 2882*/,
	(methodPointerType)&ValueCollection_get_Count_m18362_gshared/* 2883*/,
	(methodPointerType)&Enumerator__ctor_m18363_gshared/* 2884*/,
	(methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m18364_gshared/* 2885*/,
	(methodPointerType)&Enumerator_Dispose_m18365_gshared/* 2886*/,
	(methodPointerType)&Enumerator_MoveNext_m18366_gshared/* 2887*/,
	(methodPointerType)&Enumerator_get_Current_m18367_gshared/* 2888*/,
	(methodPointerType)&Transform_1__ctor_m18368_gshared/* 2889*/,
	(methodPointerType)&Transform_1_Invoke_m18369_gshared/* 2890*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18370_gshared/* 2891*/,
	(methodPointerType)&Transform_1_EndInvoke_m18371_gshared/* 2892*/,
	(methodPointerType)&Transform_1__ctor_m18372_gshared/* 2893*/,
	(methodPointerType)&Transform_1_Invoke_m18373_gshared/* 2894*/,
	(methodPointerType)&Transform_1_BeginInvoke_m18374_gshared/* 2895*/,
	(methodPointerType)&Transform_1_EndInvoke_m18375_gshared/* 2896*/,
	(methodPointerType)&ShimEnumerator__ctor_m18376_gshared/* 2897*/,
	(methodPointerType)&ShimEnumerator_MoveNext_m18377_gshared/* 2898*/,
	(methodPointerType)&ShimEnumerator_get_Entry_m18378_gshared/* 2899*/,
	(methodPointerType)&ShimEnumerator_get_Key_m18379_gshared/* 2900*/,
	(methodPointerType)&ShimEnumerator_get_Value_m18380_gshared/* 2901*/,
	(methodPointerType)&ShimEnumerator_get_Current_m18381_gshared/* 2902*/,
	(methodPointerType)&Comparer_1__ctor_m18382_gshared/* 2903*/,
	(methodPointerType)&Comparer_1__cctor_m18383_gshared/* 2904*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m18384_gshared/* 2905*/,
	(methodPointerType)&Comparer_1_get_Default_m18385_gshared/* 2906*/,
	(methodPointerType)&GenericComparer_1__ctor_m18386_gshared/* 2907*/,
	(methodPointerType)&GenericComparer_1_Compare_m18387_gshared/* 2908*/,
	(methodPointerType)&DefaultComparer__ctor_m18388_gshared/* 2909*/,
	(methodPointerType)&DefaultComparer_Compare_m18389_gshared/* 2910*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18390_gshared/* 2911*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18391_gshared/* 2912*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18392_gshared/* 2913*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18393_gshared/* 2914*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18394_gshared/* 2915*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18395_gshared/* 2916*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18396_gshared/* 2917*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18397_gshared/* 2918*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18398_gshared/* 2919*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18399_gshared/* 2920*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18400_gshared/* 2921*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18401_gshared/* 2922*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18402_gshared/* 2923*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18403_gshared/* 2924*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18404_gshared/* 2925*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18425_gshared/* 2926*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18426_gshared/* 2927*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18427_gshared/* 2928*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18428_gshared/* 2929*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18429_gshared/* 2930*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18430_gshared/* 2931*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18431_gshared/* 2932*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18432_gshared/* 2933*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18433_gshared/* 2934*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18434_gshared/* 2935*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18465_gshared/* 2936*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18466_gshared/* 2937*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18467_gshared/* 2938*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18468_gshared/* 2939*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18469_gshared/* 2940*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18495_gshared/* 2941*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18496_gshared/* 2942*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18497_gshared/* 2943*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18498_gshared/* 2944*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18499_gshared/* 2945*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18500_gshared/* 2946*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18501_gshared/* 2947*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18502_gshared/* 2948*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18503_gshared/* 2949*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18504_gshared/* 2950*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18573_gshared/* 2951*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18574_gshared/* 2952*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18575_gshared/* 2953*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18576_gshared/* 2954*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18577_gshared/* 2955*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18578_gshared/* 2956*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18579_gshared/* 2957*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18580_gshared/* 2958*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18581_gshared/* 2959*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18582_gshared/* 2960*/,
	(methodPointerType)&InternalEnumerator_1__ctor_m18583_gshared/* 2961*/,
	(methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18584_gshared/* 2962*/,
	(methodPointerType)&InternalEnumerator_1_Dispose_m18585_gshared/* 2963*/,
	(methodPointerType)&InternalEnumerator_1_MoveNext_m18586_gshared/* 2964*/,
	(methodPointerType)&InternalEnumerator_1_get_Current_m18587_gshared/* 2965*/,
	(methodPointerType)&GenericComparer_1_Compare_m18690_gshared/* 2966*/,
	(methodPointerType)&Comparer_1__ctor_m18691_gshared/* 2967*/,
	(methodPointerType)&Comparer_1__cctor_m18692_gshared/* 2968*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m18693_gshared/* 2969*/,
	(methodPointerType)&Comparer_1_get_Default_m18694_gshared/* 2970*/,
	(methodPointerType)&DefaultComparer__ctor_m18695_gshared/* 2971*/,
	(methodPointerType)&DefaultComparer_Compare_m18696_gshared/* 2972*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18697_gshared/* 2973*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18698_gshared/* 2974*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18699_gshared/* 2975*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18700_gshared/* 2976*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18701_gshared/* 2977*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18702_gshared/* 2978*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18703_gshared/* 2979*/,
	(methodPointerType)&DefaultComparer__ctor_m18704_gshared/* 2980*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18705_gshared/* 2981*/,
	(methodPointerType)&DefaultComparer_Equals_m18706_gshared/* 2982*/,
	(methodPointerType)&GenericComparer_1_Compare_m18707_gshared/* 2983*/,
	(methodPointerType)&Comparer_1__ctor_m18708_gshared/* 2984*/,
	(methodPointerType)&Comparer_1__cctor_m18709_gshared/* 2985*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m18710_gshared/* 2986*/,
	(methodPointerType)&Comparer_1_get_Default_m18711_gshared/* 2987*/,
	(methodPointerType)&DefaultComparer__ctor_m18712_gshared/* 2988*/,
	(methodPointerType)&DefaultComparer_Compare_m18713_gshared/* 2989*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18714_gshared/* 2990*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18715_gshared/* 2991*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18716_gshared/* 2992*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18717_gshared/* 2993*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18718_gshared/* 2994*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18719_gshared/* 2995*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18720_gshared/* 2996*/,
	(methodPointerType)&DefaultComparer__ctor_m18721_gshared/* 2997*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18722_gshared/* 2998*/,
	(methodPointerType)&DefaultComparer_Equals_m18723_gshared/* 2999*/,
	(methodPointerType)&Nullable_1_Equals_m18724_gshared/* 3000*/,
	(methodPointerType)&Nullable_1_Equals_m18725_gshared/* 3001*/,
	(methodPointerType)&Nullable_1_GetHashCode_m18726_gshared/* 3002*/,
	(methodPointerType)&Nullable_1_ToString_m18727_gshared/* 3003*/,
	(methodPointerType)&GenericComparer_1_Compare_m18728_gshared/* 3004*/,
	(methodPointerType)&Comparer_1__ctor_m18729_gshared/* 3005*/,
	(methodPointerType)&Comparer_1__cctor_m18730_gshared/* 3006*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m18731_gshared/* 3007*/,
	(methodPointerType)&Comparer_1_get_Default_m18732_gshared/* 3008*/,
	(methodPointerType)&DefaultComparer__ctor_m18733_gshared/* 3009*/,
	(methodPointerType)&DefaultComparer_Compare_m18734_gshared/* 3010*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18735_gshared/* 3011*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18736_gshared/* 3012*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18737_gshared/* 3013*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18738_gshared/* 3014*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18739_gshared/* 3015*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18740_gshared/* 3016*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18741_gshared/* 3017*/,
	(methodPointerType)&DefaultComparer__ctor_m18742_gshared/* 3018*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18743_gshared/* 3019*/,
	(methodPointerType)&DefaultComparer_Equals_m18744_gshared/* 3020*/,
	(methodPointerType)&GenericComparer_1_Compare_m18745_gshared/* 3021*/,
	(methodPointerType)&Comparer_1__ctor_m18746_gshared/* 3022*/,
	(methodPointerType)&Comparer_1__cctor_m18747_gshared/* 3023*/,
	(methodPointerType)&Comparer_1_System_Collections_IComparer_Compare_m18748_gshared/* 3024*/,
	(methodPointerType)&Comparer_1_get_Default_m18749_gshared/* 3025*/,
	(methodPointerType)&DefaultComparer__ctor_m18750_gshared/* 3026*/,
	(methodPointerType)&DefaultComparer_Compare_m18751_gshared/* 3027*/,
	(methodPointerType)&GenericEqualityComparer_1_GetHashCode_m18752_gshared/* 3028*/,
	(methodPointerType)&GenericEqualityComparer_1_Equals_m18753_gshared/* 3029*/,
	(methodPointerType)&EqualityComparer_1__ctor_m18754_gshared/* 3030*/,
	(methodPointerType)&EqualityComparer_1__cctor_m18755_gshared/* 3031*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m18756_gshared/* 3032*/,
	(methodPointerType)&EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m18757_gshared/* 3033*/,
	(methodPointerType)&EqualityComparer_1_get_Default_m18758_gshared/* 3034*/,
	(methodPointerType)&DefaultComparer__ctor_m18759_gshared/* 3035*/,
	(methodPointerType)&DefaultComparer_GetHashCode_m18760_gshared/* 3036*/,
	(methodPointerType)&DefaultComparer_Equals_m18761_gshared/* 3037*/,
};
const InvokerMethod g_Il2CppInvokerPointers[455] = 
{
	NULL/* 0*/,
	RuntimeInvoker_Object_t/* 1*/,
	RuntimeInvoker_Void_t71/* 2*/,
	RuntimeInvoker_Void_t71_Object_t/* 3*/,
	RuntimeInvoker_Void_t71_Single_t85/* 4*/,
	RuntimeInvoker_Object_t_Single_t85/* 5*/,
	RuntimeInvoker_Void_t71_Int32_t54/* 6*/,
	RuntimeInvoker_Boolean_t72/* 7*/,
	RuntimeInvoker_Object_t_Object_t_Object_t/* 8*/,
	RuntimeInvoker_Object_t_Object_t/* 9*/,
	RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t/* 10*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* 11*/,
	RuntimeInvoker_Boolean_t72_Object_t/* 12*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t/* 13*/,
	RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* 14*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* 15*/,
	RuntimeInvoker_Boolean_t72_ObjectU26_t863_Object_t/* 16*/,
	RuntimeInvoker_Void_t71_ObjectU26_t863_Object_t/* 17*/,
	RuntimeInvoker_Int32_t54/* 18*/,
	RuntimeInvoker_Object_t_Int32_t54/* 19*/,
	RuntimeInvoker_Void_t71_Int32_t54_Object_t/* 20*/,
	RuntimeInvoker_Void_t71_Object_t_Int32_t54/* 21*/,
	RuntimeInvoker_Int32_t54_Object_t/* 22*/,
	RuntimeInvoker_Void_t71_SByte_t73_Object_t/* 23*/,
	RuntimeInvoker_Boolean_t72_Object_t_ObjectU26_t863/* 24*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2593/* 25*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593/* 26*/,
	RuntimeInvoker_Boolean_t72_Object_t_Object_t/* 27*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* 28*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 29*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t/* 30*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* 31*/,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54/* 32*/,
	RuntimeInvoker_Enumerator_t2462/* 33*/,
	RuntimeInvoker_Enumerator_t2486/* 34*/,
	RuntimeInvoker_Void_t71_Int32_t54_ObjectU26_t863/* 35*/,
	RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* 36*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54/* 37*/,
	RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t/* 38*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Int32_t54_Object_t/* 39*/,
	RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t/* 40*/,
	RuntimeInvoker_Int32_t54_Object_t_Object_t_Object_t/* 41*/,
	RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54/* 42*/,
	RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Int32_t54_Int32_t54/* 43*/,
	RuntimeInvoker_Int32_t54_Object_t_Object_t/* 44*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Object_t/* 45*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t/* 46*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Object_t_Object_t/* 47*/,
	RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54/* 48*/,
	RuntimeInvoker_Int32_t54_Object_t_Object_t_Int32_t54_Int32_t54/* 49*/,
	RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* 50*/,
	RuntimeInvoker_KeyValuePair_2_t2593_Object_t_Object_t/* 51*/,
	RuntimeInvoker_Enumerator_t2597/* 52*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t_Object_t/* 53*/,
	RuntimeInvoker_DictionaryEntry_t1147/* 54*/,
	RuntimeInvoker_KeyValuePair_2_t2593/* 55*/,
	RuntimeInvoker_Enumerator_t2596/* 56*/,
	RuntimeInvoker_Enumerator_t2600/* 57*/,
	RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Object_t/* 58*/,
	RuntimeInvoker_Enumerator_t2429/* 59*/,
	RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* 60*/,
	RuntimeInvoker_Enumerator_t2544/* 61*/,
	RuntimeInvoker_Enumerator_t2541/* 62*/,
	RuntimeInvoker_KeyValuePair_2_t2536/* 63*/,
	RuntimeInvoker_Void_t71_Color_t163/* 64*/,
	RuntimeInvoker_Void_t71_ColorTween_t162/* 65*/,
	RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32_t54/* 66*/,
	RuntimeInvoker_Boolean_t72_ByteU26_t1360_SByte_t73/* 67*/,
	RuntimeInvoker_Boolean_t72_SingleU26_t831_Single_t85/* 68*/,
	RuntimeInvoker_Boolean_t72_UInt16U26_t2046_Int16_t448/* 69*/,
	RuntimeInvoker_Void_t71_Vector2_t53/* 70*/,
	RuntimeInvoker_Boolean_t72_NavigationU26_t3380_Navigation_t232/* 71*/,
	RuntimeInvoker_Boolean_t72_ColorBlockU26_t3381_ColorBlock_t175/* 72*/,
	RuntimeInvoker_Boolean_t72_SpriteStateU26_t3382_SpriteState_t252/* 73*/,
	RuntimeInvoker_Void_t71_SByte_t73/* 74*/,
	RuntimeInvoker_Void_t71_Int32U26_t449_Int32_t54/* 75*/,
	RuntimeInvoker_Void_t71_Vector2U26_t825_Vector2_t53/* 76*/,
	RuntimeInvoker_Void_t71_SingleU26_t831_Single_t85/* 77*/,
	RuntimeInvoker_Void_t71_ByteU26_t1360_SByte_t73/* 78*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Single_t85/* 79*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54/* 80*/,
	RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73/* 81*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54/* 82*/,
	RuntimeInvoker_Void_t71_TimeSpan_t1051/* 83*/,
	RuntimeInvoker_TimeSpan_t1051/* 84*/,
	RuntimeInvoker_Int32_t54_Int32_t54/* 85*/,
	RuntimeInvoker_Boolean_t72_Int32_t54/* 86*/,
	RuntimeInvoker_Double_t752_Int32_t54/* 87*/,
	RuntimeInvoker_Void_t71_Double_t752/* 88*/,
	RuntimeInvoker_Boolean_t72_Double_t752/* 89*/,
	RuntimeInvoker_Int32_t54_Double_t752/* 90*/,
	RuntimeInvoker_Void_t71_Int32_t54_Double_t752/* 91*/,
	RuntimeInvoker_UInt16_t371_Int32_t54/* 92*/,
	RuntimeInvoker_Void_t71_Int16_t448/* 93*/,
	RuntimeInvoker_Boolean_t72_Int16_t448/* 94*/,
	RuntimeInvoker_Int32_t54_Int16_t448/* 95*/,
	RuntimeInvoker_Void_t71_Int32_t54_Int16_t448/* 96*/,
	RuntimeInvoker_RaycastResult_t137_Int32_t54/* 97*/,
	RuntimeInvoker_Void_t71_RaycastResult_t137/* 98*/,
	RuntimeInvoker_Boolean_t72_RaycastResult_t137/* 99*/,
	RuntimeInvoker_Int32_t54_RaycastResult_t137/* 100*/,
	RuntimeInvoker_Void_t71_Int32_t54_RaycastResult_t137/* 101*/,
	RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54/* 102*/,
	RuntimeInvoker_Void_t71_RaycastResultU5BU5DU26_t3383_Int32_t54_Int32_t54/* 103*/,
	RuntimeInvoker_Int32_t54_Object_t_RaycastResult_t137_Int32_t54_Int32_t54/* 104*/,
	RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137_Object_t/* 105*/,
	RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54/* 106*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2536/* 107*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2536/* 108*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2536/* 109*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2536/* 110*/,
	RuntimeInvoker_Link_t1459_Int32_t54/* 111*/,
	RuntimeInvoker_Void_t71_Link_t1459/* 112*/,
	RuntimeInvoker_Boolean_t72_Link_t1459/* 113*/,
	RuntimeInvoker_Int32_t54_Link_t1459/* 114*/,
	RuntimeInvoker_Void_t71_Int32_t54_Link_t1459/* 115*/,
	RuntimeInvoker_DictionaryEntry_t1147_Int32_t54/* 116*/,
	RuntimeInvoker_Void_t71_DictionaryEntry_t1147/* 117*/,
	RuntimeInvoker_Boolean_t72_DictionaryEntry_t1147/* 118*/,
	RuntimeInvoker_Int32_t54_DictionaryEntry_t1147/* 119*/,
	RuntimeInvoker_Void_t71_Int32_t54_DictionaryEntry_t1147/* 120*/,
	RuntimeInvoker_RaycastHit2D_t349_Int32_t54/* 121*/,
	RuntimeInvoker_Void_t71_RaycastHit2D_t349/* 122*/,
	RuntimeInvoker_Boolean_t72_RaycastHit2D_t349/* 123*/,
	RuntimeInvoker_Int32_t54_RaycastHit2D_t349/* 124*/,
	RuntimeInvoker_Void_t71_Int32_t54_RaycastHit2D_t349/* 125*/,
	RuntimeInvoker_RaycastHit_t323_Int32_t54/* 126*/,
	RuntimeInvoker_Void_t71_RaycastHit_t323/* 127*/,
	RuntimeInvoker_Boolean_t72_RaycastHit_t323/* 128*/,
	RuntimeInvoker_Int32_t54_RaycastHit_t323/* 129*/,
	RuntimeInvoker_Void_t71_Int32_t54_RaycastHit_t323/* 130*/,
	RuntimeInvoker_KeyValuePair_2_t2567_Int32_t54/* 131*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2567/* 132*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2567/* 133*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2567/* 134*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2567/* 135*/,
	RuntimeInvoker_KeyValuePair_2_t2593_Int32_t54/* 136*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2593/* 137*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2593/* 138*/,
	RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54/* 139*/,
	RuntimeInvoker_Void_t71_UIVertexU5BU5DU26_t3384_Int32_t54_Int32_t54/* 140*/,
	RuntimeInvoker_Int32_t54_Object_t_UIVertex_t225_Int32_t54_Int32_t54/* 141*/,
	RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225_Object_t/* 142*/,
	RuntimeInvoker_Vector2_t53_Int32_t54/* 143*/,
	RuntimeInvoker_Boolean_t72_Vector2_t53/* 144*/,
	RuntimeInvoker_Int32_t54_Vector2_t53/* 145*/,
	RuntimeInvoker_Void_t71_Int32_t54_Vector2_t53/* 146*/,
	RuntimeInvoker_UILineInfo_t375_Int32_t54/* 147*/,
	RuntimeInvoker_Void_t71_UILineInfo_t375/* 148*/,
	RuntimeInvoker_Boolean_t72_UILineInfo_t375/* 149*/,
	RuntimeInvoker_Int32_t54_UILineInfo_t375/* 150*/,
	RuntimeInvoker_Void_t71_Int32_t54_UILineInfo_t375/* 151*/,
	RuntimeInvoker_UICharInfo_t377_Int32_t54/* 152*/,
	RuntimeInvoker_Void_t71_UICharInfo_t377/* 153*/,
	RuntimeInvoker_Boolean_t72_UICharInfo_t377/* 154*/,
	RuntimeInvoker_Int32_t54_UICharInfo_t377/* 155*/,
	RuntimeInvoker_Void_t71_Int32_t54_UICharInfo_t377/* 156*/,
	RuntimeInvoker_Vector3_t138_Int32_t54/* 157*/,
	RuntimeInvoker_Void_t71_Vector3_t138/* 158*/,
	RuntimeInvoker_Boolean_t72_Vector3_t138/* 159*/,
	RuntimeInvoker_Int32_t54_Vector3_t138/* 160*/,
	RuntimeInvoker_Void_t71_Int32_t54_Vector3_t138/* 161*/,
	RuntimeInvoker_GcAchievementData_t650_Int32_t54/* 162*/,
	RuntimeInvoker_Void_t71_GcAchievementData_t650/* 163*/,
	RuntimeInvoker_Boolean_t72_GcAchievementData_t650/* 164*/,
	RuntimeInvoker_Int32_t54_GcAchievementData_t650/* 165*/,
	RuntimeInvoker_Void_t71_Int32_t54_GcAchievementData_t650/* 166*/,
	RuntimeInvoker_GcScoreData_t651_Int32_t54/* 167*/,
	RuntimeInvoker_Void_t71_GcScoreData_t651/* 168*/,
	RuntimeInvoker_Boolean_t72_GcScoreData_t651/* 169*/,
	RuntimeInvoker_Int32_t54_GcScoreData_t651/* 170*/,
	RuntimeInvoker_Void_t71_Int32_t54_GcScoreData_t651/* 171*/,
	RuntimeInvoker_Byte_t367_Int32_t54/* 172*/,
	RuntimeInvoker_Boolean_t72_SByte_t73/* 173*/,
	RuntimeInvoker_Int32_t54_SByte_t73/* 174*/,
	RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* 175*/,
	RuntimeInvoker_IntPtr_t_Int32_t54/* 176*/,
	RuntimeInvoker_Void_t71_IntPtr_t/* 177*/,
	RuntimeInvoker_Boolean_t72_IntPtr_t/* 178*/,
	RuntimeInvoker_Int32_t54_IntPtr_t/* 179*/,
	RuntimeInvoker_Void_t71_Int32_t54_IntPtr_t/* 180*/,
	RuntimeInvoker_Single_t85_Int32_t54/* 181*/,
	RuntimeInvoker_Boolean_t72_Single_t85/* 182*/,
	RuntimeInvoker_Int32_t54_Single_t85/* 183*/,
	RuntimeInvoker_Void_t71_Int32_t54_Single_t85/* 184*/,
	RuntimeInvoker_Keyframe_t580_Int32_t54/* 185*/,
	RuntimeInvoker_Void_t71_Keyframe_t580/* 186*/,
	RuntimeInvoker_Boolean_t72_Keyframe_t580/* 187*/,
	RuntimeInvoker_Int32_t54_Keyframe_t580/* 188*/,
	RuntimeInvoker_Void_t71_Int32_t54_Keyframe_t580/* 189*/,
	RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54/* 190*/,
	RuntimeInvoker_Void_t71_UICharInfoU5BU5DU26_t3385_Int32_t54_Int32_t54/* 191*/,
	RuntimeInvoker_Int32_t54_Object_t_UICharInfo_t377_Int32_t54_Int32_t54/* 192*/,
	RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377_Object_t/* 193*/,
	RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54/* 194*/,
	RuntimeInvoker_Void_t71_UILineInfoU5BU5DU26_t3386_Int32_t54_Int32_t54/* 195*/,
	RuntimeInvoker_Int32_t54_Object_t_UILineInfo_t375_Int32_t54_Int32_t54/* 196*/,
	RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375_Object_t/* 197*/,
	RuntimeInvoker_KeyValuePair_2_t2780_Int32_t54/* 198*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2780/* 199*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2780/* 200*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2780/* 201*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2780/* 202*/,
	RuntimeInvoker_Int64_t753_Int32_t54/* 203*/,
	RuntimeInvoker_Void_t71_Int64_t753/* 204*/,
	RuntimeInvoker_Boolean_t72_Int64_t753/* 205*/,
	RuntimeInvoker_Int32_t54_Int64_t753/* 206*/,
	RuntimeInvoker_Void_t71_Int32_t54_Int64_t753/* 207*/,
	RuntimeInvoker_KeyValuePair_2_t2818_Int32_t54/* 208*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2818/* 209*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2818/* 210*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2818/* 211*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2818/* 212*/,
	RuntimeInvoker_UInt64_t756_Int32_t54/* 213*/,
	RuntimeInvoker_KeyValuePair_2_t2839_Int32_t54/* 214*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2839/* 215*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2839/* 216*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2839/* 217*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2839/* 218*/,
	RuntimeInvoker_ParameterModifier_t1584_Int32_t54/* 219*/,
	RuntimeInvoker_Void_t71_ParameterModifier_t1584/* 220*/,
	RuntimeInvoker_Boolean_t72_ParameterModifier_t1584/* 221*/,
	RuntimeInvoker_Int32_t54_ParameterModifier_t1584/* 222*/,
	RuntimeInvoker_Void_t71_Int32_t54_ParameterModifier_t1584/* 223*/,
	RuntimeInvoker_HitInfo_t665_Int32_t54/* 224*/,
	RuntimeInvoker_Void_t71_HitInfo_t665/* 225*/,
	RuntimeInvoker_Boolean_t72_HitInfo_t665/* 226*/,
	RuntimeInvoker_Int32_t54_HitInfo_t665/* 227*/,
	RuntimeInvoker_Void_t71_Int32_t54_HitInfo_t665/* 228*/,
	RuntimeInvoker_KeyValuePair_2_t2933_Int32_t54/* 229*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2933/* 230*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2933/* 231*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2933/* 232*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2933/* 233*/,
	RuntimeInvoker_X509ChainStatus_t1045_Int32_t54/* 234*/,
	RuntimeInvoker_Void_t71_X509ChainStatus_t1045/* 235*/,
	RuntimeInvoker_Boolean_t72_X509ChainStatus_t1045/* 236*/,
	RuntimeInvoker_Int32_t54_X509ChainStatus_t1045/* 237*/,
	RuntimeInvoker_Void_t71_Int32_t54_X509ChainStatus_t1045/* 238*/,
	RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54/* 239*/,
	RuntimeInvoker_Void_t71_KeyValuePair_2_t2956/* 240*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2956/* 241*/,
	RuntimeInvoker_Int32_t54_KeyValuePair_2_t2956/* 242*/,
	RuntimeInvoker_Void_t71_Int32_t54_KeyValuePair_2_t2956/* 243*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54_Int32_t54_Object_t/* 244*/,
	RuntimeInvoker_Mark_t1099_Int32_t54/* 245*/,
	RuntimeInvoker_Void_t71_Mark_t1099/* 246*/,
	RuntimeInvoker_Boolean_t72_Mark_t1099/* 247*/,
	RuntimeInvoker_Int32_t54_Mark_t1099/* 248*/,
	RuntimeInvoker_Void_t71_Int32_t54_Mark_t1099/* 249*/,
	RuntimeInvoker_UriScheme_t1135_Int32_t54/* 250*/,
	RuntimeInvoker_Void_t71_UriScheme_t1135/* 251*/,
	RuntimeInvoker_Boolean_t72_UriScheme_t1135/* 252*/,
	RuntimeInvoker_Int32_t54_UriScheme_t1135/* 253*/,
	RuntimeInvoker_Void_t71_Int32_t54_UriScheme_t1135/* 254*/,
	RuntimeInvoker_UInt32_t744_Int32_t54/* 255*/,
	RuntimeInvoker_Int16_t448_Int32_t54/* 256*/,
	RuntimeInvoker_SByte_t73_Int32_t54/* 257*/,
	RuntimeInvoker_TableRange_t1392_Int32_t54/* 258*/,
	RuntimeInvoker_Void_t71_TableRange_t1392/* 259*/,
	RuntimeInvoker_Boolean_t72_TableRange_t1392/* 260*/,
	RuntimeInvoker_Int32_t54_TableRange_t1392/* 261*/,
	RuntimeInvoker_Void_t71_Int32_t54_TableRange_t1392/* 262*/,
	RuntimeInvoker_Slot_t1469_Int32_t54/* 263*/,
	RuntimeInvoker_Void_t71_Slot_t1469/* 264*/,
	RuntimeInvoker_Boolean_t72_Slot_t1469/* 265*/,
	RuntimeInvoker_Int32_t54_Slot_t1469/* 266*/,
	RuntimeInvoker_Void_t71_Int32_t54_Slot_t1469/* 267*/,
	RuntimeInvoker_Slot_t1476_Int32_t54/* 268*/,
	RuntimeInvoker_Void_t71_Slot_t1476/* 269*/,
	RuntimeInvoker_Boolean_t72_Slot_t1476/* 270*/,
	RuntimeInvoker_Int32_t54_Slot_t1476/* 271*/,
	RuntimeInvoker_Void_t71_Int32_t54_Slot_t1476/* 272*/,
	RuntimeInvoker_DateTime_t508_Int32_t54/* 273*/,
	RuntimeInvoker_Void_t71_DateTime_t508/* 274*/,
	RuntimeInvoker_Boolean_t72_DateTime_t508/* 275*/,
	RuntimeInvoker_Int32_t54_DateTime_t508/* 276*/,
	RuntimeInvoker_Void_t71_Int32_t54_DateTime_t508/* 277*/,
	RuntimeInvoker_Decimal_t755_Int32_t54/* 278*/,
	RuntimeInvoker_Void_t71_Decimal_t755/* 279*/,
	RuntimeInvoker_Boolean_t72_Decimal_t755/* 280*/,
	RuntimeInvoker_Int32_t54_Decimal_t755/* 281*/,
	RuntimeInvoker_Void_t71_Int32_t54_Decimal_t755/* 282*/,
	RuntimeInvoker_TimeSpan_t1051_Int32_t54/* 283*/,
	RuntimeInvoker_Boolean_t72_TimeSpan_t1051/* 284*/,
	RuntimeInvoker_Int32_t54_TimeSpan_t1051/* 285*/,
	RuntimeInvoker_Void_t71_Int32_t54_TimeSpan_t1051/* 286*/,
	RuntimeInvoker_Double_t752/* 287*/,
	RuntimeInvoker_UInt16_t371/* 288*/,
	RuntimeInvoker_Byte_t367_Object_t/* 289*/,
	RuntimeInvoker_Int32_t54_RaycastResult_t137_RaycastResult_t137/* 290*/,
	RuntimeInvoker_Object_t_RaycastResult_t137_RaycastResult_t137_Object_t_Object_t/* 291*/,
	RuntimeInvoker_RaycastResult_t137_Object_t/* 292*/,
	RuntimeInvoker_Enumerator_t2495/* 293*/,
	RuntimeInvoker_RaycastResult_t137/* 294*/,
	RuntimeInvoker_Boolean_t72_RaycastResult_t137_RaycastResult_t137/* 295*/,
	RuntimeInvoker_Object_t_RaycastResult_t137_Object_t_Object_t/* 296*/,
	RuntimeInvoker_KeyValuePair_2_t2536_Int32_t54_Object_t/* 297*/,
	RuntimeInvoker_Int32_t54_Int32_t54_Object_t/* 298*/,
	RuntimeInvoker_Object_t_Int32_t54_Object_t/* 299*/,
	RuntimeInvoker_Boolean_t72_Int32_t54_ObjectU26_t863/* 300*/,
	RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Object_t/* 301*/,
	RuntimeInvoker_Link_t1459/* 302*/,
	RuntimeInvoker_Enumerator_t2540/* 303*/,
	RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t/* 304*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t/* 305*/,
	RuntimeInvoker_KeyValuePair_2_t2536_Object_t/* 306*/,
	RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54/* 307*/,
	RuntimeInvoker_RaycastHit2D_t349/* 308*/,
	RuntimeInvoker_Int32_t54_RaycastHit_t323_RaycastHit_t323/* 309*/,
	RuntimeInvoker_Object_t_RaycastHit_t323_RaycastHit_t323_Object_t_Object_t/* 310*/,
	RuntimeInvoker_RaycastHit_t323/* 311*/,
	RuntimeInvoker_Object_t_Color_t163_Object_t_Object_t/* 312*/,
	RuntimeInvoker_KeyValuePair_2_t2567_Object_t_Int32_t54/* 313*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t54/* 314*/,
	RuntimeInvoker_Int32_t54_Object_t_Int32_t54/* 315*/,
	RuntimeInvoker_Boolean_t72_Object_t_Int32U26_t449/* 316*/,
	RuntimeInvoker_Enumerator_t2571/* 317*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int32_t54/* 318*/,
	RuntimeInvoker_KeyValuePair_2_t2567/* 319*/,
	RuntimeInvoker_Enumerator_t2570/* 320*/,
	RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t/* 321*/,
	RuntimeInvoker_Enumerator_t2574/* 322*/,
	RuntimeInvoker_KeyValuePair_2_t2567_Object_t/* 323*/,
	RuntimeInvoker_KeyValuePair_2_t2593_Object_t/* 324*/,
	RuntimeInvoker_Void_t71_UIVertex_t225/* 325*/,
	RuntimeInvoker_Boolean_t72_UIVertex_t225/* 326*/,
	RuntimeInvoker_UIVertex_t225_Object_t/* 327*/,
	RuntimeInvoker_Enumerator_t2611/* 328*/,
	RuntimeInvoker_Int32_t54_UIVertex_t225/* 329*/,
	RuntimeInvoker_Void_t71_Int32_t54_UIVertex_t225/* 330*/,
	RuntimeInvoker_UIVertex_t225_Int32_t54/* 331*/,
	RuntimeInvoker_UIVertex_t225/* 332*/,
	RuntimeInvoker_Boolean_t72_UIVertex_t225_UIVertex_t225/* 333*/,
	RuntimeInvoker_Object_t_UIVertex_t225_Object_t_Object_t/* 334*/,
	RuntimeInvoker_Int32_t54_UIVertex_t225_UIVertex_t225/* 335*/,
	RuntimeInvoker_Object_t_UIVertex_t225_UIVertex_t225_Object_t_Object_t/* 336*/,
	RuntimeInvoker_Object_t_ColorTween_t162/* 337*/,
	RuntimeInvoker_Vector2_t53/* 338*/,
	RuntimeInvoker_UILineInfo_t375/* 339*/,
	RuntimeInvoker_UICharInfo_t377/* 340*/,
	RuntimeInvoker_Object_t_Single_t85_Object_t_Object_t/* 341*/,
	RuntimeInvoker_Vector3_t138/* 342*/,
	RuntimeInvoker_Object_t_Vector2_t53_Object_t_Object_t/* 343*/,
	RuntimeInvoker_Object_t_SByte_t73_Object_t_Object_t/* 344*/,
	RuntimeInvoker_Single_t85_Object_t/* 345*/,
	RuntimeInvoker_GcAchievementData_t650/* 346*/,
	RuntimeInvoker_GcScoreData_t651/* 347*/,
	RuntimeInvoker_Byte_t367/* 348*/,
	RuntimeInvoker_IntPtr_t/* 349*/,
	RuntimeInvoker_Single_t85/* 350*/,
	RuntimeInvoker_Keyframe_t580/* 351*/,
	RuntimeInvoker_UICharInfo_t377_Object_t/* 352*/,
	RuntimeInvoker_Enumerator_t2759/* 353*/,
	RuntimeInvoker_Boolean_t72_UICharInfo_t377_UICharInfo_t377/* 354*/,
	RuntimeInvoker_Object_t_UICharInfo_t377_Object_t_Object_t/* 355*/,
	RuntimeInvoker_Int32_t54_UICharInfo_t377_UICharInfo_t377/* 356*/,
	RuntimeInvoker_Object_t_UICharInfo_t377_UICharInfo_t377_Object_t_Object_t/* 357*/,
	RuntimeInvoker_UILineInfo_t375_Object_t/* 358*/,
	RuntimeInvoker_Enumerator_t2768/* 359*/,
	RuntimeInvoker_Boolean_t72_UILineInfo_t375_UILineInfo_t375/* 360*/,
	RuntimeInvoker_Object_t_UILineInfo_t375_Object_t_Object_t/* 361*/,
	RuntimeInvoker_Int32_t54_UILineInfo_t375_UILineInfo_t375/* 362*/,
	RuntimeInvoker_Object_t_UILineInfo_t375_UILineInfo_t375_Object_t_Object_t/* 363*/,
	RuntimeInvoker_Int64_t753_Object_t/* 364*/,
	RuntimeInvoker_Void_t71_Object_t_Int64_t753/* 365*/,
	RuntimeInvoker_KeyValuePair_2_t2780_Object_t_Int64_t753/* 366*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t753/* 367*/,
	RuntimeInvoker_Int64_t753_Object_t_Int64_t753/* 368*/,
	RuntimeInvoker_Boolean_t72_Object_t_Int64U26_t2029/* 369*/,
	RuntimeInvoker_Enumerator_t2785/* 370*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t_Int64_t753/* 371*/,
	RuntimeInvoker_KeyValuePair_2_t2780/* 372*/,
	RuntimeInvoker_Int64_t753/* 373*/,
	RuntimeInvoker_Enumerator_t2784/* 374*/,
	RuntimeInvoker_Object_t_Object_t_Int64_t753_Object_t_Object_t/* 375*/,
	RuntimeInvoker_Enumerator_t2788/* 376*/,
	RuntimeInvoker_KeyValuePair_2_t2780_Object_t/* 377*/,
	RuntimeInvoker_Boolean_t72_Int64_t753_Int64_t753/* 378*/,
	RuntimeInvoker_Object_t_Int64_t753/* 379*/,
	RuntimeInvoker_Void_t71_Int64_t753_Object_t/* 380*/,
	RuntimeInvoker_KeyValuePair_2_t2818_Int64_t753_Object_t/* 381*/,
	RuntimeInvoker_UInt64_t756_Int64_t753_Object_t/* 382*/,
	RuntimeInvoker_Object_t_Int64_t753_Object_t/* 383*/,
	RuntimeInvoker_Boolean_t72_Int64_t753_ObjectU26_t863/* 384*/,
	RuntimeInvoker_UInt64_t756_Object_t/* 385*/,
	RuntimeInvoker_Enumerator_t2823/* 386*/,
	RuntimeInvoker_DictionaryEntry_t1147_Int64_t753_Object_t/* 387*/,
	RuntimeInvoker_KeyValuePair_2_t2818/* 388*/,
	RuntimeInvoker_UInt64_t756/* 389*/,
	RuntimeInvoker_Enumerator_t2822/* 390*/,
	RuntimeInvoker_Object_t_Int64_t753_Object_t_Object_t_Object_t/* 391*/,
	RuntimeInvoker_Enumerator_t2826/* 392*/,
	RuntimeInvoker_KeyValuePair_2_t2818_Object_t/* 393*/,
	RuntimeInvoker_KeyValuePair_2_t2839/* 394*/,
	RuntimeInvoker_Void_t71_Object_t_KeyValuePair_2_t2593/* 395*/,
	RuntimeInvoker_KeyValuePair_2_t2839_Object_t_KeyValuePair_2_t2593/* 396*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593/* 397*/,
	RuntimeInvoker_KeyValuePair_2_t2593_Object_t_KeyValuePair_2_t2593/* 398*/,
	RuntimeInvoker_Boolean_t72_Object_t_KeyValuePair_2U26_t3387/* 399*/,
	RuntimeInvoker_Enumerator_t2868/* 400*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t_KeyValuePair_2_t2593/* 401*/,
	RuntimeInvoker_Enumerator_t2867/* 402*/,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2593_Object_t_Object_t/* 403*/,
	RuntimeInvoker_Enumerator_t2871/* 404*/,
	RuntimeInvoker_KeyValuePair_2_t2839_Object_t/* 405*/,
	RuntimeInvoker_Boolean_t72_KeyValuePair_2_t2593_KeyValuePair_2_t2593/* 406*/,
	RuntimeInvoker_ParameterModifier_t1584/* 407*/,
	RuntimeInvoker_HitInfo_t665/* 408*/,
	RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t/* 409*/,
	RuntimeInvoker_Void_t71_Object_t_SByte_t73/* 410*/,
	RuntimeInvoker_KeyValuePair_2_t2933_Object_t_SByte_t73/* 411*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t73/* 412*/,
	RuntimeInvoker_Byte_t367_Object_t_SByte_t73/* 413*/,
	RuntimeInvoker_Boolean_t72_Object_t_ByteU26_t1360/* 414*/,
	RuntimeInvoker_Enumerator_t2937/* 415*/,
	RuntimeInvoker_DictionaryEntry_t1147_Object_t_SByte_t73/* 416*/,
	RuntimeInvoker_KeyValuePair_2_t2933/* 417*/,
	RuntimeInvoker_Enumerator_t2936/* 418*/,
	RuntimeInvoker_Object_t_Object_t_SByte_t73_Object_t_Object_t/* 419*/,
	RuntimeInvoker_Enumerator_t2940/* 420*/,
	RuntimeInvoker_KeyValuePair_2_t2933_Object_t/* 421*/,
	RuntimeInvoker_Boolean_t72_SByte_t73_SByte_t73/* 422*/,
	RuntimeInvoker_X509ChainStatus_t1045/* 423*/,
	RuntimeInvoker_KeyValuePair_2_t2956_Int32_t54_Int32_t54/* 424*/,
	RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54/* 425*/,
	RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449/* 426*/,
	RuntimeInvoker_Enumerator_t2960/* 427*/,
	RuntimeInvoker_DictionaryEntry_t1147_Int32_t54_Int32_t54/* 428*/,
	RuntimeInvoker_KeyValuePair_2_t2956/* 429*/,
	RuntimeInvoker_Enumerator_t2959/* 430*/,
	RuntimeInvoker_Object_t_Int32_t54_Int32_t54_Object_t_Object_t/* 431*/,
	RuntimeInvoker_Enumerator_t2963/* 432*/,
	RuntimeInvoker_KeyValuePair_2_t2956_Object_t/* 433*/,
	RuntimeInvoker_Mark_t1099/* 434*/,
	RuntimeInvoker_UriScheme_t1135/* 435*/,
	RuntimeInvoker_UInt32_t744/* 436*/,
	RuntimeInvoker_Int16_t448/* 437*/,
	RuntimeInvoker_SByte_t73/* 438*/,
	RuntimeInvoker_TableRange_t1392/* 439*/,
	RuntimeInvoker_Slot_t1469/* 440*/,
	RuntimeInvoker_Slot_t1476/* 441*/,
	RuntimeInvoker_DateTime_t508/* 442*/,
	RuntimeInvoker_Decimal_t755/* 443*/,
	RuntimeInvoker_Int32_t54_DateTime_t508_DateTime_t508/* 444*/,
	RuntimeInvoker_Boolean_t72_DateTime_t508_DateTime_t508/* 445*/,
	RuntimeInvoker_Int32_t54_DateTimeOffset_t768_DateTimeOffset_t768/* 446*/,
	RuntimeInvoker_Int32_t54_DateTimeOffset_t768/* 447*/,
	RuntimeInvoker_Boolean_t72_DateTimeOffset_t768_DateTimeOffset_t768/* 448*/,
	RuntimeInvoker_Boolean_t72_Nullable_1_t1924/* 449*/,
	RuntimeInvoker_Int32_t54_Guid_t769_Guid_t769/* 450*/,
	RuntimeInvoker_Int32_t54_Guid_t769/* 451*/,
	RuntimeInvoker_Boolean_t72_Guid_t769_Guid_t769/* 452*/,
	RuntimeInvoker_Int32_t54_TimeSpan_t1051_TimeSpan_t1051/* 453*/,
	RuntimeInvoker_Boolean_t72_TimeSpan_t1051_TimeSpan_t1051/* 454*/,
};
const Il2CppCodeRegistration g_CodeRegistration = 
{
	3038,
	g_Il2CppMethodPointers,
	455,
	g_Il2CppInvokerPointers,
};
extern const Il2CppMetadataRegistration g_MetadataRegistration;
static void s_Il2CppCodegenRegistration()
{
	il2cpp_codegen_register (&g_CodeRegistration, &g_MetadataRegistration);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_Il2CppCodegenRegistrationVariable (&s_Il2CppCodegenRegistration, NULL);
