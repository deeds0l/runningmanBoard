﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t142;
// System.Object
struct Object_t;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t3062;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.GameObject>
struct ICollection_1_t3063;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t3064;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GameObject>
struct ReadOnlyCollection_1_t2526;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2524;
// System.Predicate`1<UnityEngine.GameObject>
struct Predicate_1_t2527;
// System.Comparison`1<UnityEngine.GameObject>
struct Comparison_1_t2529;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m1500(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor(System.Int32)
#define List_1__ctor_m12609(__this, ___capacity, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.cctor()
#define List_1__cctor_m12610(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12611(__this, method) (( Object_t* (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m12612(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12613(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m12614(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m12615(__this, ___item, method) (( bool (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m12616(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m12617(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m12618(__this, ___item, method) (( void (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12619(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12620(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m12621(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m12622(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m12623(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m12624(__this, ___index, method) (( Object_t * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m12625(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(T)
#define List_1_Add_m12626(__this, ___item, method) (( void (*) (List_1_t142 *, GameObject_t5 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m12627(__this, ___newCount, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m12628(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m12629(__this, ___enumerable, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m12630(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.GameObject>::AsReadOnly()
#define List_1_AsReadOnly_m12631(__this, method) (( ReadOnlyCollection_1_t2526 * (*) (List_1_t142 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Clear()
#define List_1_Clear_m12632(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Contains(T)
#define List_1_Contains_m12633(__this, ___item, method) (( bool (*) (List_1_t142 *, GameObject_t5 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m12634(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, GameObjectU5BU5D_t2524*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.GameObject>::Find(System.Predicate`1<T>)
#define List_1_Find_m12635(__this, ___match, method) (( GameObject_t5 * (*) (List_1_t142 *, Predicate_1_t2527 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m12636(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2527 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m12637(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t142 *, int32_t, int32_t, Predicate_1_t2527 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
#define List_1_GetEnumerator_m12638(__this, method) (( Enumerator_t2528  (*) (List_1_t142 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::IndexOf(T)
#define List_1_IndexOf_m12639(__this, ___item, method) (( int32_t (*) (List_1_t142 *, GameObject_t5 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m12640(__this, ___start, ___delta, method) (( void (*) (List_1_t142 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m12641(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Insert(System.Int32,T)
#define List_1_Insert_m12642(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, GameObject_t5 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m12643(__this, ___collection, method) (( void (*) (List_1_t142 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(T)
#define List_1_Remove_m12644(__this, ___item, method) (( bool (*) (List_1_t142 *, GameObject_t5 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m12645(__this, ___match, method) (( int32_t (*) (List_1_t142 *, Predicate_1_t2527 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m12646(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Reverse()
#define List_1_Reverse_m12647(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Sort()
#define List_1_Sort_m12648(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m12649(__this, ___comparison, method) (( void (*) (List_1_t142 *, Comparison_1_t2529 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.GameObject>::ToArray()
#define List_1_ToArray_m12650(__this, method) (( GameObjectU5BU5D_t2524* (*) (List_1_t142 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::TrimExcess()
#define List_1_TrimExcess_m12651(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Capacity()
#define List_1_get_Capacity_m12652(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m12653(__this, ___value, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
#define List_1_get_Count_m12654(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m12655(__this, ___index, method) (( GameObject_t5 * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::set_Item(System.Int32,T)
#define List_1_set_Item_m12656(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, GameObject_t5 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
