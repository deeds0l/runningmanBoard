﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1782;

// System.Void System.Text.EncoderFallbackBuffer::.ctor()
extern "C" void EncoderFallbackBuffer__ctor_m9335 (EncoderFallbackBuffer_t1782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderFallbackBuffer::get_Remaining()
// System.Boolean System.Text.EncoderFallbackBuffer::Fallback(System.Char,System.Int32)
// System.Boolean System.Text.EncoderFallbackBuffer::Fallback(System.Char,System.Char,System.Int32)
// System.Char System.Text.EncoderFallbackBuffer::GetNextChar()
