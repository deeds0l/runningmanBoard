﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t1405;
struct Escape_t1405_marshaled;

void Escape_t1405_marshal(const Escape_t1405& unmarshaled, Escape_t1405_marshaled& marshaled);
void Escape_t1405_marshal_back(const Escape_t1405_marshaled& marshaled, Escape_t1405& unmarshaled);
void Escape_t1405_marshal_cleanup(Escape_t1405_marshaled& marshaled);
