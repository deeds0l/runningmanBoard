﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// Metadata Definition System.Reflection.AssemblyDescriptionAttribute
extern TypeInfo AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo;
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyDescriptionAttribute_t397_AssemblyDescriptionAttribute__ctor_m1979_ParameterInfos[] = 
{
	{"description", 0, 134221525, 0, &String_t_0_0_0},
};
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyDescriptionAttribute__ctor_m1979_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyDescriptionAttribute__ctor_m1979/* method */
	, &AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyDescriptionAttribute_t397_AssemblyDescriptionAttribute__ctor_m1979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyDescriptionAttribute_t397_MethodInfos[] =
{
	&AssemblyDescriptionAttribute__ctor_m1979_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m3659_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m3530_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference AssemblyDescriptionAttribute_t397_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyDescriptionAttribute_t397_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t832_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyDescriptionAttribute_t397_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyDescriptionAttribute_t397_0_0_0;
extern const Il2CppType AssemblyDescriptionAttribute_t397_1_0_0;
extern const Il2CppType Attribute_t539_0_0_0;
struct AssemblyDescriptionAttribute_t397;
const Il2CppTypeDefinitionMetadata AssemblyDescriptionAttribute_t397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyDescriptionAttribute_t397_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyDescriptionAttribute_t397_VTable/* vtableMethods */
	, AssemblyDescriptionAttribute_t397_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1125/* fieldStart */

};
TypeInfo AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyDescriptionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyDescriptionAttribute_t397_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 390/* custom_attributes_cache */
	, &AssemblyDescriptionAttribute_t397_0_0_0/* byval_arg */
	, &AssemblyDescriptionAttribute_t397_1_0_0/* this_arg */
	, &AssemblyDescriptionAttribute_t397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyDescriptionAttribute_t397)/* instance_size */
	, sizeof (AssemblyDescriptionAttribute_t397)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// Metadata Definition System.Reflection.AssemblyFileVersionAttribute
extern TypeInfo AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo;
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyFileVersionAttribute_t404_AssemblyFileVersionAttribute__ctor_m1986_ParameterInfos[] = 
{
	{"version", 0, 134221526, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyFileVersionAttribute__ctor_m1986_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyFileVersionAttribute__ctor_m1986/* method */
	, &AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyFileVersionAttribute_t404_AssemblyFileVersionAttribute__ctor_m1986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyFileVersionAttribute_t404_MethodInfos[] =
{
	&AssemblyFileVersionAttribute__ctor_m1986_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyFileVersionAttribute_t404_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyFileVersionAttribute_t404_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyFileVersionAttribute_t404_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyFileVersionAttribute_t404_0_0_0;
extern const Il2CppType AssemblyFileVersionAttribute_t404_1_0_0;
struct AssemblyFileVersionAttribute_t404;
const Il2CppTypeDefinitionMetadata AssemblyFileVersionAttribute_t404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyFileVersionAttribute_t404_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyFileVersionAttribute_t404_VTable/* vtableMethods */
	, AssemblyFileVersionAttribute_t404_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1126/* fieldStart */

};
TypeInfo AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyFileVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyFileVersionAttribute_t404_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 391/* custom_attributes_cache */
	, &AssemblyFileVersionAttribute_t404_0_0_0/* byval_arg */
	, &AssemblyFileVersionAttribute_t404_1_0_0/* this_arg */
	, &AssemblyFileVersionAttribute_t404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyFileVersionAttribute_t404)/* instance_size */
	, sizeof (AssemblyFileVersionAttribute_t404)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// Metadata Definition System.Reflection.AssemblyInformationalVersionAttribute
extern TypeInfo AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo;
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyInformationalVersionAttribute_t928_AssemblyInformationalVersionAttribute__ctor_m3740_ParameterInfos[] = 
{
	{"informationalVersion", 0, 134221527, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
extern const MethodInfo AssemblyInformationalVersionAttribute__ctor_m3740_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyInformationalVersionAttribute__ctor_m3740/* method */
	, &AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyInformationalVersionAttribute_t928_AssemblyInformationalVersionAttribute__ctor_m3740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyInformationalVersionAttribute_t928_MethodInfos[] =
{
	&AssemblyInformationalVersionAttribute__ctor_m3740_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyInformationalVersionAttribute_t928_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyInformationalVersionAttribute_t928_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyInformationalVersionAttribute_t928_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyInformationalVersionAttribute_t928_0_0_0;
extern const Il2CppType AssemblyInformationalVersionAttribute_t928_1_0_0;
struct AssemblyInformationalVersionAttribute_t928;
const Il2CppTypeDefinitionMetadata AssemblyInformationalVersionAttribute_t928_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyInformationalVersionAttribute_t928_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyInformationalVersionAttribute_t928_VTable/* vtableMethods */
	, AssemblyInformationalVersionAttribute_t928_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1127/* fieldStart */

};
TypeInfo AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyInformationalVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyInformationalVersionAttribute_t928_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 392/* custom_attributes_cache */
	, &AssemblyInformationalVersionAttribute_t928_0_0_0/* byval_arg */
	, &AssemblyInformationalVersionAttribute_t928_1_0_0/* this_arg */
	, &AssemblyInformationalVersionAttribute_t928_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyInformationalVersionAttribute_t928)/* instance_size */
	, sizeof (AssemblyInformationalVersionAttribute_t928)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// Metadata Definition System.Reflection.AssemblyKeyFileAttribute
extern TypeInfo AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo;
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyKeyFileAttribute_t934_AssemblyKeyFileAttribute__ctor_m3746_ParameterInfos[] = 
{
	{"keyFile", 0, 134221528, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern const MethodInfo AssemblyKeyFileAttribute__ctor_m3746_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyKeyFileAttribute__ctor_m3746/* method */
	, &AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyKeyFileAttribute_t934_AssemblyKeyFileAttribute__ctor_m3746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyKeyFileAttribute_t934_MethodInfos[] =
{
	&AssemblyKeyFileAttribute__ctor_m3746_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyKeyFileAttribute_t934_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyKeyFileAttribute_t934_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyKeyFileAttribute_t934_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyKeyFileAttribute_t934_0_0_0;
extern const Il2CppType AssemblyKeyFileAttribute_t934_1_0_0;
struct AssemblyKeyFileAttribute_t934;
const Il2CppTypeDefinitionMetadata AssemblyKeyFileAttribute_t934_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyKeyFileAttribute_t934_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyKeyFileAttribute_t934_VTable/* vtableMethods */
	, AssemblyKeyFileAttribute_t934_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1128/* fieldStart */

};
TypeInfo AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyKeyFileAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyKeyFileAttribute_t934_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 393/* custom_attributes_cache */
	, &AssemblyKeyFileAttribute_t934_0_0_0/* byval_arg */
	, &AssemblyKeyFileAttribute_t934_1_0_0/* this_arg */
	, &AssemblyKeyFileAttribute_t934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyKeyFileAttribute_t934)/* instance_size */
	, sizeof (AssemblyKeyFileAttribute_t934)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
// Metadata Definition System.Reflection.AssemblyName
extern TypeInfo AssemblyName_t1561_il2cpp_TypeInfo;
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyNameMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::.ctor()
extern const MethodInfo AssemblyName__ctor_m8208_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyName__ctor_m8208/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName__ctor_m8209_ParameterInfos[] = 
{
	{"si", 0, 134221529, 0, &SerializationInfo_t725_0_0_0},
	{"sc", 1, 134221530, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo AssemblyName__ctor_m8209_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyName__ctor_m8209/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, AssemblyName_t1561_AssemblyName__ctor_m8209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::get_Name()
extern const MethodInfo AssemblyName_get_Name_m8210_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&AssemblyName_get_Name_m8210/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AssemblyNameFlags_t1562_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1562 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.AssemblyNameFlags System.Reflection.AssemblyName::get_Flags()
extern const MethodInfo AssemblyName_get_Flags_m8211_MethodInfo = 
{
	"get_Flags"/* name */
	, (methodPointerType)&AssemblyName_get_Flags_m8211/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1562_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1562/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::get_FullName()
extern const MethodInfo AssemblyName_get_FullName_m8212_MethodInfo = 
{
	"get_FullName"/* name */
	, (methodPointerType)&AssemblyName_get_FullName_m8212/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Reflection.AssemblyName::get_Version()
extern const MethodInfo AssemblyName_get_Version_m8213_MethodInfo = 
{
	"get_Version"/* name */
	, (methodPointerType)&AssemblyName_get_Version_m8213/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1006_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName_set_Version_m8214_ParameterInfos[] = 
{
	{"value", 0, 134221531, 0, &Version_t1006_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::set_Version(System.Version)
extern const MethodInfo AssemblyName_set_Version_m8214_MethodInfo = 
{
	"set_Version"/* name */
	, (methodPointerType)&AssemblyName_set_Version_m8214/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyName_t1561_AssemblyName_set_Version_m8214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.AssemblyName::ToString()
extern const MethodInfo AssemblyName_ToString_m8215_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AssemblyName_ToString_m8215/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.AssemblyName::get_IsPublicKeyValid()
extern const MethodInfo AssemblyName_get_IsPublicKeyValid_m8216_MethodInfo = 
{
	"get_IsPublicKeyValid"/* name */
	, (methodPointerType)&AssemblyName_get_IsPublicKeyValid_m8216/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Reflection.AssemblyName::InternalGetPublicKeyToken()
extern const MethodInfo AssemblyName_InternalGetPublicKeyToken_m8217_MethodInfo = 
{
	"InternalGetPublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_InternalGetPublicKeyToken_m8217/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Byte[] System.Reflection.AssemblyName::ComputePublicKeyToken()
extern const MethodInfo AssemblyName_ComputePublicKeyToken_m8218_MethodInfo = 
{
	"ComputePublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_ComputePublicKeyToken_m8218/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t546_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName_SetPublicKey_m8219_ParameterInfos[] = 
{
	{"publicKey", 0, 134221532, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::SetPublicKey(System.Byte[])
extern const MethodInfo AssemblyName_SetPublicKey_m8219_MethodInfo = 
{
	"SetPublicKey"/* name */
	, (methodPointerType)&AssemblyName_SetPublicKey_m8219/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyName_t1561_AssemblyName_SetPublicKey_m8219_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName_SetPublicKeyToken_m8220_ParameterInfos[] = 
{
	{"publicKeyToken", 0, 134221533, 0, &ByteU5BU5D_t546_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::SetPublicKeyToken(System.Byte[])
extern const MethodInfo AssemblyName_SetPublicKeyToken_m8220_MethodInfo = 
{
	"SetPublicKeyToken"/* name */
	, (methodPointerType)&AssemblyName_SetPublicKeyToken_m8220/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyName_t1561_AssemblyName_SetPublicKeyToken_m8220_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName_GetObjectData_m8221_ParameterInfos[] = 
{
	{"info", 0, 134221534, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221535, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo AssemblyName_GetObjectData_m8221_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&AssemblyName_GetObjectData_m8221/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, AssemblyName_t1561_AssemblyName_GetObjectData_m8221_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyName_t1561_AssemblyName_OnDeserialization_m8222_ParameterInfos[] = 
{
	{"sender", 0, 134221536, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyName::OnDeserialization(System.Object)
extern const MethodInfo AssemblyName_OnDeserialization_m8222_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&AssemblyName_OnDeserialization_m8222/* method */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyName_t1561_AssemblyName_OnDeserialization_m8222_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyName_t1561_MethodInfos[] =
{
	&AssemblyName__ctor_m8208_MethodInfo,
	&AssemblyName__ctor_m8209_MethodInfo,
	&AssemblyName_get_Name_m8210_MethodInfo,
	&AssemblyName_get_Flags_m8211_MethodInfo,
	&AssemblyName_get_FullName_m8212_MethodInfo,
	&AssemblyName_get_Version_m8213_MethodInfo,
	&AssemblyName_set_Version_m8214_MethodInfo,
	&AssemblyName_ToString_m8215_MethodInfo,
	&AssemblyName_get_IsPublicKeyValid_m8216_MethodInfo,
	&AssemblyName_InternalGetPublicKeyToken_m8217_MethodInfo,
	&AssemblyName_ComputePublicKeyToken_m8218_MethodInfo,
	&AssemblyName_SetPublicKey_m8219_MethodInfo,
	&AssemblyName_SetPublicKeyToken_m8220_MethodInfo,
	&AssemblyName_GetObjectData_m8221_MethodInfo,
	&AssemblyName_OnDeserialization_m8222_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyName_get_Name_m8210_MethodInfo;
static const PropertyInfo AssemblyName_t1561____Name_PropertyInfo = 
{
	&AssemblyName_t1561_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &AssemblyName_get_Name_m8210_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_Flags_m8211_MethodInfo;
static const PropertyInfo AssemblyName_t1561____Flags_PropertyInfo = 
{
	&AssemblyName_t1561_il2cpp_TypeInfo/* parent */
	, "Flags"/* name */
	, &AssemblyName_get_Flags_m8211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_FullName_m8212_MethodInfo;
static const PropertyInfo AssemblyName_t1561____FullName_PropertyInfo = 
{
	&AssemblyName_t1561_il2cpp_TypeInfo/* parent */
	, "FullName"/* name */
	, &AssemblyName_get_FullName_m8212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_Version_m8213_MethodInfo;
extern const MethodInfo AssemblyName_set_Version_m8214_MethodInfo;
static const PropertyInfo AssemblyName_t1561____Version_PropertyInfo = 
{
	&AssemblyName_t1561_il2cpp_TypeInfo/* parent */
	, "Version"/* name */
	, &AssemblyName_get_Version_m8213_MethodInfo/* get */
	, &AssemblyName_set_Version_m8214_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AssemblyName_get_IsPublicKeyValid_m8216_MethodInfo;
static const PropertyInfo AssemblyName_t1561____IsPublicKeyValid_PropertyInfo = 
{
	&AssemblyName_t1561_il2cpp_TypeInfo/* parent */
	, "IsPublicKeyValid"/* name */
	, &AssemblyName_get_IsPublicKeyValid_m8216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AssemblyName_t1561_PropertyInfos[] =
{
	&AssemblyName_t1561____Name_PropertyInfo,
	&AssemblyName_t1561____Flags_PropertyInfo,
	&AssemblyName_t1561____FullName_PropertyInfo,
	&AssemblyName_t1561____Version_PropertyInfo,
	&AssemblyName_t1561____IsPublicKeyValid_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo AssemblyName_ToString_m8215_MethodInfo;
extern const MethodInfo AssemblyName_GetObjectData_m8221_MethodInfo;
extern const MethodInfo AssemblyName_OnDeserialization_m8222_MethodInfo;
static const Il2CppMethodReference AssemblyName_t1561_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&AssemblyName_ToString_m8215_MethodInfo,
	&AssemblyName_GetObjectData_m8221_MethodInfo,
	&AssemblyName_OnDeserialization_m8222_MethodInfo,
};
static bool AssemblyName_t1561_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
extern const Il2CppType ISerializable_t428_0_0_0;
extern const Il2CppType _AssemblyName_t1998_0_0_0;
extern const Il2CppType IDeserializationCallback_t1206_0_0_0;
static const Il2CppType* AssemblyName_t1561_InterfacesTypeInfos[] = 
{
	&ICloneable_t427_0_0_0,
	&ISerializable_t428_0_0_0,
	&_AssemblyName_t1998_0_0_0,
	&IDeserializationCallback_t1206_0_0_0,
};
static Il2CppInterfaceOffsetPair AssemblyName_t1561_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
	{ &_AssemblyName_t1998_0_0_0, 5},
	{ &IDeserializationCallback_t1206_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyName_t1561_0_0_0;
extern const Il2CppType AssemblyName_t1561_1_0_0;
struct AssemblyName_t1561;
const Il2CppTypeDefinitionMetadata AssemblyName_t1561_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AssemblyName_t1561_InterfacesTypeInfos/* implementedInterfaces */
	, AssemblyName_t1561_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AssemblyName_t1561_VTable/* vtableMethods */
	, AssemblyName_t1561_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1129/* fieldStart */

};
TypeInfo AssemblyName_t1561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyName"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyName_t1561_MethodInfos/* methods */
	, AssemblyName_t1561_PropertyInfos/* properties */
	, NULL/* events */
	, &AssemblyName_t1561_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 394/* custom_attributes_cache */
	, &AssemblyName_t1561_0_0_0/* byval_arg */
	, &AssemblyName_t1561_1_0_0/* this_arg */
	, &AssemblyName_t1561_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyName_t1561)/* instance_size */
	, sizeof (AssemblyName_t1561)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 5/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
// Metadata Definition System.Reflection.AssemblyNameFlags
extern TypeInfo AssemblyNameFlags_t1562_il2cpp_TypeInfo;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlagsMethodDeclarations.h"
static const MethodInfo* AssemblyNameFlags_t1562_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference AssemblyNameFlags_t1562_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool AssemblyNameFlags_t1562_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyNameFlags_t1562_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyNameFlags_t1562_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AssemblyNameFlags_t1562_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyNameFlags_t1562_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, AssemblyNameFlags_t1562_VTable/* vtableMethods */
	, AssemblyNameFlags_t1562_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1144/* fieldStart */

};
TypeInfo AssemblyNameFlags_t1562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyNameFlags"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyNameFlags_t1562_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 395/* custom_attributes_cache */
	, &AssemblyNameFlags_t1562_0_0_0/* byval_arg */
	, &AssemblyNameFlags_t1562_1_0_0/* this_arg */
	, &AssemblyNameFlags_t1562_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyNameFlags_t1562)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyNameFlags_t1562)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// Metadata Definition System.Reflection.AssemblyProductAttribute
extern TypeInfo AssemblyProductAttribute_t400_il2cpp_TypeInfo;
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyProductAttribute_t400_AssemblyProductAttribute__ctor_m1982_ParameterInfos[] = 
{
	{"product", 0, 134221537, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
extern const MethodInfo AssemblyProductAttribute__ctor_m1982_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyProductAttribute__ctor_m1982/* method */
	, &AssemblyProductAttribute_t400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyProductAttribute_t400_AssemblyProductAttribute__ctor_m1982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyProductAttribute_t400_MethodInfos[] =
{
	&AssemblyProductAttribute__ctor_m1982_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyProductAttribute_t400_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyProductAttribute_t400_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyProductAttribute_t400_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyProductAttribute_t400_0_0_0;
extern const Il2CppType AssemblyProductAttribute_t400_1_0_0;
struct AssemblyProductAttribute_t400;
const Il2CppTypeDefinitionMetadata AssemblyProductAttribute_t400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyProductAttribute_t400_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyProductAttribute_t400_VTable/* vtableMethods */
	, AssemblyProductAttribute_t400_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */

};
TypeInfo AssemblyProductAttribute_t400_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyProductAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyProductAttribute_t400_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyProductAttribute_t400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 396/* custom_attributes_cache */
	, &AssemblyProductAttribute_t400_0_0_0/* byval_arg */
	, &AssemblyProductAttribute_t400_1_0_0/* this_arg */
	, &AssemblyProductAttribute_t400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyProductAttribute_t400)/* instance_size */
	, sizeof (AssemblyProductAttribute_t400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// Metadata Definition System.Reflection.AssemblyTitleAttribute
extern TypeInfo AssemblyTitleAttribute_t403_il2cpp_TypeInfo;
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyTitleAttribute_t403_AssemblyTitleAttribute__ctor_m1985_ParameterInfos[] = 
{
	{"title", 0, 134221538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
extern const MethodInfo AssemblyTitleAttribute__ctor_m1985_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyTitleAttribute__ctor_m1985/* method */
	, &AssemblyTitleAttribute_t403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyTitleAttribute_t403_AssemblyTitleAttribute__ctor_m1985_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyTitleAttribute_t403_MethodInfos[] =
{
	&AssemblyTitleAttribute__ctor_m1985_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyTitleAttribute_t403_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyTitleAttribute_t403_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyTitleAttribute_t403_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTitleAttribute_t403_0_0_0;
extern const Il2CppType AssemblyTitleAttribute_t403_1_0_0;
struct AssemblyTitleAttribute_t403;
const Il2CppTypeDefinitionMetadata AssemblyTitleAttribute_t403_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTitleAttribute_t403_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyTitleAttribute_t403_VTable/* vtableMethods */
	, AssemblyTitleAttribute_t403_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1151/* fieldStart */

};
TypeInfo AssemblyTitleAttribute_t403_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTitleAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyTitleAttribute_t403_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyTitleAttribute_t403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 397/* custom_attributes_cache */
	, &AssemblyTitleAttribute_t403_0_0_0/* byval_arg */
	, &AssemblyTitleAttribute_t403_1_0_0/* this_arg */
	, &AssemblyTitleAttribute_t403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTitleAttribute_t403)/* instance_size */
	, sizeof (AssemblyTitleAttribute_t403)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// Metadata Definition System.Reflection.AssemblyTrademarkAttribute
extern TypeInfo AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo;
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AssemblyTrademarkAttribute_t405_AssemblyTrademarkAttribute__ctor_m1987_ParameterInfos[] = 
{
	{"trademark", 0, 134221539, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
extern const MethodInfo AssemblyTrademarkAttribute__ctor_m1987_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyTrademarkAttribute__ctor_m1987/* method */
	, &AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AssemblyTrademarkAttribute_t405_AssemblyTrademarkAttribute__ctor_m1987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyTrademarkAttribute_t405_MethodInfos[] =
{
	&AssemblyTrademarkAttribute__ctor_m1987_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyTrademarkAttribute_t405_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AssemblyTrademarkAttribute_t405_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyTrademarkAttribute_t405_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTrademarkAttribute_t405_0_0_0;
extern const Il2CppType AssemblyTrademarkAttribute_t405_1_0_0;
struct AssemblyTrademarkAttribute_t405;
const Il2CppTypeDefinitionMetadata AssemblyTrademarkAttribute_t405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTrademarkAttribute_t405_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, AssemblyTrademarkAttribute_t405_VTable/* vtableMethods */
	, AssemblyTrademarkAttribute_t405_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1152/* fieldStart */

};
TypeInfo AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTrademarkAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, AssemblyTrademarkAttribute_t405_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyTrademarkAttribute_t405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 398/* custom_attributes_cache */
	, &AssemblyTrademarkAttribute_t405_0_0_0/* byval_arg */
	, &AssemblyTrademarkAttribute_t405_1_0_0/* this_arg */
	, &AssemblyTrademarkAttribute_t405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTrademarkAttribute_t405)/* instance_size */
	, sizeof (AssemblyTrademarkAttribute_t405)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_Default.h"
// Metadata Definition System.Reflection.Binder/Default
extern TypeInfo Default_t1563_il2cpp_TypeInfo;
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_DefaultMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::.ctor()
extern const MethodInfo Default__ctor_m8223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Default__ctor_m8223/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo Default_t1563_Default_BindToMethod_m8224_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221567, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221568, 0, &MethodBaseU5BU5D_t1917_0_0_0},
	{"args", 2, 134221569, 0, &ObjectU5BU5D_t29_1_0_0},
	{"modifiers", 3, 134221570, 0, &ParameterModifierU5BU5D_t782_0_0_0},
	{"culture", 4, 134221571, 0, &CultureInfo_t750_0_0_0},
	{"names", 5, 134221572, 0, &StringU5BU5D_t45_0_0_0},
	{"state", 6, 134221573, 0, &Object_t_1_0_2},
};
extern const Il2CppType MethodBase_t47_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_ObjectU5BU5DU26_t2023_Object_t_Object_t_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::BindToMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Object[]&,System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[],System.Object&)
extern const MethodInfo Default_BindToMethod_m8224_MethodInfo = 
{
	"BindToMethod"/* name */
	, (methodPointerType)&Default_BindToMethod_m8224/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_ObjectU5BU5DU26_t2023_Object_t_Object_t_Object_t_ObjectU26_t863/* invoker_method */
	, Default_t1563_Default_BindToMethod_m8224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
static const ParameterInfo Default_t1563_Default_ReorderParameters_m8225_ParameterInfos[] = 
{
	{"names", 0, 134221574, 0, &StringU5BU5D_t45_0_0_0},
	{"args", 1, 134221575, 0, &ObjectU5BU5D_t29_1_0_0},
	{"selected", 2, 134221576, 0, &MethodBase_t47_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::ReorderParameters(System.String[],System.Object[]&,System.Reflection.MethodBase)
extern const MethodInfo Default_ReorderParameters_m8225_MethodInfo = 
{
	"ReorderParameters"/* name */
	, (methodPointerType)&Default_ReorderParameters_m8225/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_ObjectU5BU5DU26_t2023_Object_t/* invoker_method */
	, Default_t1563_Default_ReorderParameters_m8225_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t1563_Default_IsArrayAssignable_m8226_ParameterInfos[] = 
{
	{"object_type", 0, 134221577, 0, &Type_t_0_0_0},
	{"target_type", 1, 134221578, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::IsArrayAssignable(System.Type,System.Type)
extern const MethodInfo Default_IsArrayAssignable_m8226_MethodInfo = 
{
	"IsArrayAssignable"/* name */
	, (methodPointerType)&Default_IsArrayAssignable_m8226/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_IsArrayAssignable_m8226_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo Default_t1563_Default_ChangeType_m8227_ParameterInfos[] = 
{
	{"value", 0, 134221579, 0, &Object_t_0_0_0},
	{"type", 1, 134221580, 0, &Type_t_0_0_0},
	{"culture", 2, 134221581, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.Binder/Default::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo)
extern const MethodInfo Default_ChangeType_m8227_MethodInfo = 
{
	"ChangeType"/* name */
	, (methodPointerType)&Default_ChangeType_m8227/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_ChangeType_m8227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Default_t1563_Default_ReorderArgumentArray_m8228_ParameterInfos[] = 
{
	{"args", 0, 134221582, 0, &ObjectU5BU5D_t29_1_0_0},
	{"state", 1, 134221583, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder/Default::ReorderArgumentArray(System.Object[]&,System.Object)
extern const MethodInfo Default_ReorderArgumentArray_m8228_MethodInfo = 
{
	"ReorderArgumentArray"/* name */
	, (methodPointerType)&Default_ReorderArgumentArray_m8228/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t/* invoker_method */
	, Default_t1563_Default_ReorderArgumentArray_m8228_ParameterInfos/* parameters */
	, 400/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t1563_Default_check_type_m8229_ParameterInfos[] = 
{
	{"from", 0, 134221584, 0, &Type_t_0_0_0},
	{"to", 1, 134221585, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::check_type(System.Type,System.Type)
extern const MethodInfo Default_check_type_m8229_MethodInfo = 
{
	"check_type"/* name */
	, (methodPointerType)&Default_check_type_m8229/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_check_type_m8229_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t774_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t774_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Default_t1563_Default_check_arguments_m8230_ParameterInfos[] = 
{
	{"types", 0, 134221586, 0, &TypeU5BU5D_t628_0_0_0},
	{"args", 1, 134221587, 0, &ParameterInfoU5BU5D_t774_0_0_0},
	{"allowByRefMatch", 2, 134221588, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder/Default::check_arguments(System.Type[],System.Reflection.ParameterInfo[],System.Boolean)
extern const MethodInfo Default_check_arguments_m8230_MethodInfo = 
{
	"check_arguments"/* name */
	, (methodPointerType)&Default_check_arguments_m8230/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t_SByte_t73/* invoker_method */
	, Default_t1563_Default_check_arguments_m8230_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo Default_t1563_Default_SelectMethod_m8231_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221589, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221590, 0, &MethodBaseU5BU5D_t1917_0_0_0},
	{"types", 2, 134221591, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 3, 134221592, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Default_SelectMethod_m8231_MethodInfo = 
{
	"SelectMethod"/* name */
	, (methodPointerType)&Default_SelectMethod_m8231/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_SelectMethod_m8231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Default_t1563_Default_SelectMethod_m8232_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221593, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221594, 0, &MethodBaseU5BU5D_t1917_0_0_0},
	{"types", 2, 134221595, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 3, 134221596, 0, &ParameterModifierU5BU5D_t782_0_0_0},
	{"allowByRefMatch", 4, 134221597, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[],System.Boolean)
extern const MethodInfo Default_SelectMethod_m8232_MethodInfo = 
{
	"SelectMethod"/* name */
	, (methodPointerType)&Default_SelectMethod_m8232/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_SByte_t73/* invoker_method */
	, Default_t1563_Default_SelectMethod_m8232_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t47_0_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo Default_t1563_Default_GetBetterMethod_m8233_ParameterInfos[] = 
{
	{"m1", 0, 134221598, 0, &MethodBase_t47_0_0_0},
	{"m2", 1, 134221599, 0, &MethodBase_t47_0_0_0},
	{"types", 2, 134221600, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder/Default::GetBetterMethod(System.Reflection.MethodBase,System.Reflection.MethodBase,System.Type[])
extern const MethodInfo Default_GetBetterMethod_m8233_MethodInfo = 
{
	"GetBetterMethod"/* name */
	, (methodPointerType)&Default_GetBetterMethod_m8233/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_GetBetterMethod_m8233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t1563_Default_CompareCloserType_m8234_ParameterInfos[] = 
{
	{"t1", 0, 134221601, 0, &Type_t_0_0_0},
	{"t2", 1, 134221602, 0, &Type_t_0_0_0},
};
extern const Il2CppType Int32_t54_0_0_0;
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::CompareCloserType(System.Type,System.Type)
extern const MethodInfo Default_CompareCloserType_m8234_MethodInfo = 
{
	"CompareCloserType"/* name */
	, (methodPointerType)&Default_CompareCloserType_m8234/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_CompareCloserType_m8234_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t777_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t777_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo Default_t1563_Default_SelectProperty_m8235_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221603, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221604, 0, &PropertyInfoU5BU5D_t777_0_0_0},
	{"returnType", 2, 134221605, 0, &Type_t_0_0_0},
	{"indexes", 3, 134221606, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 4, 134221607, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.Reflection.Binder/Default::SelectProperty(System.Reflection.BindingFlags,System.Reflection.PropertyInfo[],System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Default_SelectProperty_m8235_MethodInfo = 
{
	"SelectProperty"/* name */
	, (methodPointerType)&Default_SelectProperty_m8235/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_SelectProperty_m8235_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t774_0_0_0;
static const ParameterInfo Default_t1563_Default_check_arguments_with_score_m8236_ParameterInfos[] = 
{
	{"types", 0, 134221608, 0, &TypeU5BU5D_t628_0_0_0},
	{"args", 1, 134221609, 0, &ParameterInfoU5BU5D_t774_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::check_arguments_with_score(System.Type[],System.Reflection.ParameterInfo[])
extern const MethodInfo Default_check_arguments_with_score_m8236_MethodInfo = 
{
	"check_arguments_with_score"/* name */
	, (methodPointerType)&Default_check_arguments_with_score_m8236/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_check_arguments_with_score_m8236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Default_t1563_Default_check_type_with_score_m8237_ParameterInfos[] = 
{
	{"from", 0, 134221610, 0, &Type_t_0_0_0},
	{"to", 1, 134221611, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder/Default::check_type_with_score(System.Type,System.Type)
extern const MethodInfo Default_check_type_with_score_m8237_MethodInfo = 
{
	"check_type_with_score"/* name */
	, (methodPointerType)&Default_check_type_with_score_m8237/* method */
	, &Default_t1563_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Object_t/* invoker_method */
	, Default_t1563_Default_check_type_with_score_m8237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Default_t1563_MethodInfos[] =
{
	&Default__ctor_m8223_MethodInfo,
	&Default_BindToMethod_m8224_MethodInfo,
	&Default_ReorderParameters_m8225_MethodInfo,
	&Default_IsArrayAssignable_m8226_MethodInfo,
	&Default_ChangeType_m8227_MethodInfo,
	&Default_ReorderArgumentArray_m8228_MethodInfo,
	&Default_check_type_m8229_MethodInfo,
	&Default_check_arguments_m8230_MethodInfo,
	&Default_SelectMethod_m8231_MethodInfo,
	&Default_SelectMethod_m8232_MethodInfo,
	&Default_GetBetterMethod_m8233_MethodInfo,
	&Default_CompareCloserType_m8234_MethodInfo,
	&Default_SelectProperty_m8235_MethodInfo,
	&Default_check_arguments_with_score_m8236_MethodInfo,
	&Default_check_type_with_score_m8237_MethodInfo,
	NULL
};
extern const MethodInfo Default_BindToMethod_m8224_MethodInfo;
extern const MethodInfo Default_ChangeType_m8227_MethodInfo;
extern const MethodInfo Default_ReorderArgumentArray_m8228_MethodInfo;
extern const MethodInfo Default_SelectMethod_m8231_MethodInfo;
extern const MethodInfo Default_SelectProperty_m8235_MethodInfo;
static const Il2CppMethodReference Default_t1563_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Default_BindToMethod_m8224_MethodInfo,
	&Default_ChangeType_m8227_MethodInfo,
	&Default_ReorderArgumentArray_m8228_MethodInfo,
	&Default_SelectMethod_m8231_MethodInfo,
	&Default_SelectProperty_m8235_MethodInfo,
};
static bool Default_t1563_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Default_t1563_0_0_0;
extern const Il2CppType Default_t1563_1_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern TypeInfo Binder_t781_il2cpp_TypeInfo;
struct Default_t1563;
const Il2CppTypeDefinitionMetadata Default_t1563_DefinitionMetadata = 
{
	&Binder_t781_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Binder_t781_0_0_0/* parent */
	, Default_t1563_VTable/* vtableMethods */
	, Default_t1563_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Default_t1563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Default"/* name */
	, ""/* namespaze */
	, Default_t1563_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Default_t1563_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Default_t1563_0_0_0/* byval_arg */
	, &Default_t1563_1_0_0/* this_arg */
	, &Default_t1563_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Default_t1563)/* instance_size */
	, sizeof (Default_t1563)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// Metadata Definition System.Reflection.Binder
// System.Reflection.Binder
#include "mscorlib_System_Reflection_BinderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::.ctor()
extern const MethodInfo Binder__ctor_m8238_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Binder__ctor_m8238/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::.cctor()
extern const MethodInfo Binder__cctor_m8239_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Binder__cctor_m8239/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo Binder_t781_Binder_BindToMethod_m10916_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221540, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221541, 0, &MethodBaseU5BU5D_t1917_0_0_0},
	{"args", 2, 134221542, 0, &ObjectU5BU5D_t29_1_0_0},
	{"modifiers", 3, 134221543, 0, &ParameterModifierU5BU5D_t782_0_0_0},
	{"culture", 4, 134221544, 0, &CultureInfo_t750_0_0_0},
	{"names", 5, 134221545, 0, &StringU5BU5D_t45_0_0_0},
	{"state", 6, 134221546, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_ObjectU5BU5DU26_t2023_Object_t_Object_t_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::BindToMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Object[]&,System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[],System.Object&)
extern const MethodInfo Binder_BindToMethod_m10916_MethodInfo = 
{
	"BindToMethod"/* name */
	, NULL/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_ObjectU5BU5DU26_t2023_Object_t_Object_t_Object_t_ObjectU26_t863/* invoker_method */
	, Binder_t781_Binder_BindToMethod_m10916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo Binder_t781_Binder_ChangeType_m10917_ParameterInfos[] = 
{
	{"value", 0, 134221547, 0, &Object_t_0_0_0},
	{"type", 1, 134221548, 0, &Type_t_0_0_0},
	{"culture", 2, 134221549, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.Binder::ChangeType(System.Object,System.Type,System.Globalization.CultureInfo)
extern const MethodInfo Binder_ChangeType_m10917_MethodInfo = 
{
	"ChangeType"/* name */
	, NULL/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t781_Binder_ChangeType_m10917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_1_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Binder_t781_Binder_ReorderArgumentArray_m10918_ParameterInfos[] = 
{
	{"args", 0, 134221550, 0, &ObjectU5BU5D_t29_1_0_0},
	{"state", 1, 134221551, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Binder::ReorderArgumentArray(System.Object[]&,System.Object)
extern const MethodInfo Binder_ReorderArgumentArray_m10918_MethodInfo = 
{
	"ReorderArgumentArray"/* name */
	, NULL/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_ObjectU5BU5DU26_t2023_Object_t/* invoker_method */
	, Binder_t781_Binder_ReorderArgumentArray_m10918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo Binder_t781_Binder_SelectMethod_m10919_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221552, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221553, 0, &MethodBaseU5BU5D_t1917_0_0_0},
	{"types", 2, 134221554, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 3, 134221555, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::SelectMethod(System.Reflection.BindingFlags,System.Reflection.MethodBase[],System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Binder_SelectMethod_m10919_MethodInfo = 
{
	"SelectMethod"/* name */
	, NULL/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t781_Binder_SelectMethod_m10919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType PropertyInfoU5BU5D_t777_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t782_0_0_0;
static const ParameterInfo Binder_t781_Binder_SelectProperty_m10920_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221556, 0, &BindingFlags_t1564_0_0_0},
	{"match", 1, 134221557, 0, &PropertyInfoU5BU5D_t777_0_0_0},
	{"returnType", 2, 134221558, 0, &Type_t_0_0_0},
	{"indexes", 3, 134221559, 0, &TypeU5BU5D_t628_0_0_0},
	{"modifiers", 4, 134221560, 0, &ParameterModifierU5BU5D_t782_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.Reflection.Binder::SelectProperty(System.Reflection.BindingFlags,System.Reflection.PropertyInfo[],System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo Binder_SelectProperty_m10920_MethodInfo = 
{
	"SelectProperty"/* name */
	, NULL/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t781_Binder_SelectProperty_m10920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Binder System.Reflection.Binder::get_DefaultBinder()
extern const MethodInfo Binder_get_DefaultBinder_m8240_MethodInfo = 
{
	"get_DefaultBinder"/* name */
	, (methodPointerType)&Binder_get_DefaultBinder_m8240/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Binder_t781_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2195/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ParameterInfoU5BU5D_t774_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo Binder_t781_Binder_ConvertArgs_m8241_ParameterInfos[] = 
{
	{"binder", 0, 134221561, 0, &Binder_t781_0_0_0},
	{"args", 1, 134221562, 0, &ObjectU5BU5D_t29_0_0_0},
	{"pinfo", 2, 134221563, 0, &ParameterInfoU5BU5D_t774_0_0_0},
	{"culture", 3, 134221564, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Binder::ConvertArgs(System.Reflection.Binder,System.Object[],System.Reflection.ParameterInfo[],System.Globalization.CultureInfo)
extern const MethodInfo Binder_ConvertArgs_m8241_MethodInfo = 
{
	"ConvertArgs"/* name */
	, (methodPointerType)&Binder_ConvertArgs_m8241/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Binder_t781_Binder_ConvertArgs_m8241_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Binder_t781_Binder_GetDerivedLevel_m8242_ParameterInfos[] = 
{
	{"type", 0, 134221565, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.Binder::GetDerivedLevel(System.Type)
extern const MethodInfo Binder_GetDerivedLevel_m8242_MethodInfo = 
{
	"GetDerivedLevel"/* name */
	, (methodPointerType)&Binder_GetDerivedLevel_m8242/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Binder_t781_Binder_GetDerivedLevel_m8242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBaseU5BU5D_t1917_0_0_0;
static const ParameterInfo Binder_t781_Binder_FindMostDerivedMatch_m8243_ParameterInfos[] = 
{
	{"match", 0, 134221566, 0, &MethodBaseU5BU5D_t1917_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.Binder::FindMostDerivedMatch(System.Reflection.MethodBase[])
extern const MethodInfo Binder_FindMostDerivedMatch_m8243_MethodInfo = 
{
	"FindMostDerivedMatch"/* name */
	, (methodPointerType)&Binder_FindMostDerivedMatch_m8243/* method */
	, &Binder_t781_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Binder_t781_Binder_FindMostDerivedMatch_m8243_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Binder_t781_MethodInfos[] =
{
	&Binder__ctor_m8238_MethodInfo,
	&Binder__cctor_m8239_MethodInfo,
	&Binder_BindToMethod_m10916_MethodInfo,
	&Binder_ChangeType_m10917_MethodInfo,
	&Binder_ReorderArgumentArray_m10918_MethodInfo,
	&Binder_SelectMethod_m10919_MethodInfo,
	&Binder_SelectProperty_m10920_MethodInfo,
	&Binder_get_DefaultBinder_m8240_MethodInfo,
	&Binder_ConvertArgs_m8241_MethodInfo,
	&Binder_GetDerivedLevel_m8242_MethodInfo,
	&Binder_FindMostDerivedMatch_m8243_MethodInfo,
	NULL
};
extern const MethodInfo Binder_get_DefaultBinder_m8240_MethodInfo;
static const PropertyInfo Binder_t781____DefaultBinder_PropertyInfo = 
{
	&Binder_t781_il2cpp_TypeInfo/* parent */
	, "DefaultBinder"/* name */
	, &Binder_get_DefaultBinder_m8240_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Binder_t781_PropertyInfos[] =
{
	&Binder_t781____DefaultBinder_PropertyInfo,
	NULL
};
static const Il2CppType* Binder_t781_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Default_t1563_0_0_0,
};
static const Il2CppMethodReference Binder_t781_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool Binder_t781_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Binder_t781_1_0_0;
struct Binder_t781;
const Il2CppTypeDefinitionMetadata Binder_t781_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Binder_t781_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Binder_t781_VTable/* vtableMethods */
	, Binder_t781_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */

};
TypeInfo Binder_t781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Binder"/* name */
	, "System.Reflection"/* namespaze */
	, Binder_t781_MethodInfos/* methods */
	, Binder_t781_PropertyInfos/* properties */
	, NULL/* events */
	, &Binder_t781_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 399/* custom_attributes_cache */
	, &Binder_t781_0_0_0/* byval_arg */
	, &Binder_t781_1_0_0/* this_arg */
	, &Binder_t781_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Binder_t781)/* instance_size */
	, sizeof (Binder_t781)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Binder_t781_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// Metadata Definition System.Reflection.BindingFlags
extern TypeInfo BindingFlags_t1564_il2cpp_TypeInfo;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlagsMethodDeclarations.h"
static const MethodInfo* BindingFlags_t1564_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BindingFlags_t1564_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool BindingFlags_t1564_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BindingFlags_t1564_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BindingFlags_t1564_1_0_0;
const Il2CppTypeDefinitionMetadata BindingFlags_t1564_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BindingFlags_t1564_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, BindingFlags_t1564_VTable/* vtableMethods */
	, BindingFlags_t1564_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1154/* fieldStart */

};
TypeInfo BindingFlags_t1564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BindingFlags"/* name */
	, "System.Reflection"/* namespaze */
	, BindingFlags_t1564_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 401/* custom_attributes_cache */
	, &BindingFlags_t1564_0_0_0/* byval_arg */
	, &BindingFlags_t1564_1_0_0/* this_arg */
	, &BindingFlags_t1564_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BindingFlags_t1564)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BindingFlags_t1564)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
// Metadata Definition System.Reflection.CallingConventions
extern TypeInfo CallingConventions_t1565_il2cpp_TypeInfo;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventionsMethodDeclarations.h"
static const MethodInfo* CallingConventions_t1565_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CallingConventions_t1565_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool CallingConventions_t1565_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CallingConventions_t1565_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConventions_t1565_0_0_0;
extern const Il2CppType CallingConventions_t1565_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConventions_t1565_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConventions_t1565_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, CallingConventions_t1565_VTable/* vtableMethods */
	, CallingConventions_t1565_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1175/* fieldStart */

};
TypeInfo CallingConventions_t1565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConventions"/* name */
	, "System.Reflection"/* namespaze */
	, CallingConventions_t1565_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 402/* custom_attributes_cache */
	, &CallingConventions_t1565_0_0_0/* byval_arg */
	, &CallingConventions_t1565_1_0_0/* this_arg */
	, &CallingConventions_t1565_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConventions_t1565)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConventions_t1565)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
// Metadata Definition System.Reflection.ConstructorInfo
extern TypeInfo ConstructorInfo_t632_il2cpp_TypeInfo;
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ConstructorInfo::.ctor()
extern const MethodInfo ConstructorInfo__ctor_m8244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructorInfo__ctor_m8244/* method */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ConstructorInfo::.cctor()
extern const MethodInfo ConstructorInfo__cctor_m8245_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructorInfo__cctor_m8245/* method */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberTypes_t1570_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.ConstructorInfo::get_MemberType()
extern const MethodInfo ConstructorInfo_get_MemberType_m8246_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&ConstructorInfo_get_MemberType_m8246/* method */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo ConstructorInfo_t632_ConstructorInfo_Invoke_m3491_ParameterInfos[] = 
{
	{"parameters", 0, 134221612, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern const MethodInfo ConstructorInfo_Invoke_m3491_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConstructorInfo_Invoke_m3491/* method */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorInfo_t632_ConstructorInfo_Invoke_m3491_ParameterInfos/* parameters */
	, 406/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo ConstructorInfo_t632_ConstructorInfo_Invoke_m10921_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221613, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 1, 134221614, 0, &Binder_t781_0_0_0},
	{"parameters", 2, 134221615, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 3, 134221616, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo ConstructorInfo_Invoke_m10921_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, ConstructorInfo_t632_ConstructorInfo_Invoke_m10921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructorInfo_t632_MethodInfos[] =
{
	&ConstructorInfo__ctor_m8244_MethodInfo,
	&ConstructorInfo__cctor_m8245_MethodInfo,
	&ConstructorInfo_get_MemberType_m8246_MethodInfo,
	&ConstructorInfo_Invoke_m3491_MethodInfo,
	&ConstructorInfo_Invoke_m10921_MethodInfo,
	NULL
};
extern const MethodInfo ConstructorInfo_get_MemberType_m8246_MethodInfo;
static const PropertyInfo ConstructorInfo_t632____MemberType_PropertyInfo = 
{
	&ConstructorInfo_t632_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &ConstructorInfo_get_MemberType_m8246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 407/* custom_attributes_cache */

};
static const PropertyInfo* ConstructorInfo_t632_PropertyInfos[] =
{
	&ConstructorInfo_t632____MemberType_PropertyInfo,
	NULL
};
extern const MethodInfo MemberInfo_GetCustomAttributes_m10589_MethodInfo;
extern const MethodInfo MemberInfo_IsDefined_m10587_MethodInfo;
extern const MethodInfo MemberInfo_get_Module_m6696_MethodInfo;
extern const MethodInfo MethodBase_Invoke_m8278_MethodInfo;
extern const MethodInfo MethodBase_get_CallingConvention_m8279_MethodInfo;
extern const MethodInfo MethodBase_get_IsPublic_m8280_MethodInfo;
extern const MethodInfo MethodBase_get_IsStatic_m8281_MethodInfo;
extern const MethodInfo MethodBase_get_IsVirtual_m8282_MethodInfo;
extern const MethodInfo MethodBase_GetGenericArguments_m8283_MethodInfo;
extern const MethodInfo MethodBase_get_ContainsGenericParameters_m8284_MethodInfo;
extern const MethodInfo MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo;
extern const MethodInfo MethodBase_get_IsGenericMethod_m8286_MethodInfo;
static const Il2CppMethodReference ConstructorInfo_t632_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	&ConstructorInfo_get_MemberType_m8246_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m8278_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m8279_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodBase_GetGenericArguments_m8283_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m8284_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo,
	&MethodBase_get_IsGenericMethod_m8286_MethodInfo,
	NULL,
};
static bool ConstructorInfo_t632_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _ConstructorInfo_t1999_0_0_0;
static const Il2CppType* ConstructorInfo_t632_InterfacesTypeInfos[] = 
{
	&_ConstructorInfo_t1999_0_0_0,
};
extern const Il2CppType _MethodBase_t2002_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t1925_0_0_0;
extern const Il2CppType _MemberInfo_t1962_0_0_0;
static Il2CppInterfaceOffsetPair ConstructorInfo_t632_InterfacesOffsets[] = 
{
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_ConstructorInfo_t1999_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructorInfo_t632_0_0_0;
extern const Il2CppType ConstructorInfo_t632_1_0_0;
struct ConstructorInfo_t632;
const Il2CppTypeDefinitionMetadata ConstructorInfo_t632_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructorInfo_t632_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructorInfo_t632_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t47_0_0_0/* parent */
	, ConstructorInfo_t632_VTable/* vtableMethods */
	, ConstructorInfo_t632_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1181/* fieldStart */

};
TypeInfo ConstructorInfo_t632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorInfo"/* name */
	, "System.Reflection"/* namespaze */
	, ConstructorInfo_t632_MethodInfos/* methods */
	, ConstructorInfo_t632_PropertyInfos/* properties */
	, NULL/* events */
	, &ConstructorInfo_t632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 403/* custom_attributes_cache */
	, &ConstructorInfo_t632_0_0_0/* byval_arg */
	, &ConstructorInfo_t632_1_0_0/* this_arg */
	, &ConstructorInfo_t632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorInfo_t632)/* instance_size */
	, sizeof (ConstructorInfo_t632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructorInfo_t632_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
// Metadata Definition System.Reflection.EventAttributes
extern TypeInfo EventAttributes_t1566_il2cpp_TypeInfo;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributesMethodDeclarations.h"
static const MethodInfo* EventAttributes_t1566_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference EventAttributes_t1566_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool EventAttributes_t1566_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventAttributes_t1566_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventAttributes_t1566_0_0_0;
extern const Il2CppType EventAttributes_t1566_1_0_0;
const Il2CppTypeDefinitionMetadata EventAttributes_t1566_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventAttributes_t1566_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, EventAttributes_t1566_VTable/* vtableMethods */
	, EventAttributes_t1566_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1183/* fieldStart */

};
TypeInfo EventAttributes_t1566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, EventAttributes_t1566_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 408/* custom_attributes_cache */
	, &EventAttributes_t1566_0_0_0/* byval_arg */
	, &EventAttributes_t1566_1_0_0/* this_arg */
	, &EventAttributes_t1566_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventAttributes_t1566)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventAttributes_t1566)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapter.h"
// Metadata Definition System.Reflection.EventInfo/AddEventAdapter
extern TypeInfo AddEventAdapter_t1567_il2cpp_TypeInfo;
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AddEventAdapter_t1567_AddEventAdapter__ctor_m8247_ParameterInfos[] = 
{
	{"object", 0, 134221618, 0, &Object_t_0_0_0},
	{"method", 1, 134221619, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AddEventAdapter__ctor_m8247_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddEventAdapter__ctor_m8247/* method */
	, &AddEventAdapter_t1567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, AddEventAdapter_t1567_AddEventAdapter__ctor_m8247_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Delegate_t361_0_0_0;
extern const Il2CppType Delegate_t361_0_0_0;
static const ParameterInfo AddEventAdapter_t1567_AddEventAdapter_Invoke_m8248_ParameterInfos[] = 
{
	{"_this", 0, 134221620, 0, &Object_t_0_0_0},
	{"dele", 1, 134221621, 0, &Delegate_t361_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::Invoke(System.Object,System.Delegate)
extern const MethodInfo AddEventAdapter_Invoke_m8248_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AddEventAdapter_Invoke_m8248/* method */
	, &AddEventAdapter_t1567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, AddEventAdapter_t1567_AddEventAdapter_Invoke_m8248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Delegate_t361_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AddEventAdapter_t1567_AddEventAdapter_BeginInvoke_m8249_ParameterInfos[] = 
{
	{"_this", 0, 134221622, 0, &Object_t_0_0_0},
	{"dele", 1, 134221623, 0, &Delegate_t361_0_0_0},
	{"callback", 2, 134221624, 0, &AsyncCallback_t214_0_0_0},
	{"object", 3, 134221625, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.EventInfo/AddEventAdapter::BeginInvoke(System.Object,System.Delegate,System.AsyncCallback,System.Object)
extern const MethodInfo AddEventAdapter_BeginInvoke_m8249_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AddEventAdapter_BeginInvoke_m8249/* method */
	, &AddEventAdapter_t1567_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AddEventAdapter_t1567_AddEventAdapter_BeginInvoke_m8249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo AddEventAdapter_t1567_AddEventAdapter_EndInvoke_m8250_ParameterInfos[] = 
{
	{"result", 0, 134221626, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo/AddEventAdapter::EndInvoke(System.IAsyncResult)
extern const MethodInfo AddEventAdapter_EndInvoke_m8250_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AddEventAdapter_EndInvoke_m8250/* method */
	, &AddEventAdapter_t1567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AddEventAdapter_t1567_AddEventAdapter_EndInvoke_m8250_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddEventAdapter_t1567_MethodInfos[] =
{
	&AddEventAdapter__ctor_m8247_MethodInfo,
	&AddEventAdapter_Invoke_m8248_MethodInfo,
	&AddEventAdapter_BeginInvoke_m8249_MethodInfo,
	&AddEventAdapter_EndInvoke_m8250_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo AddEventAdapter_Invoke_m8248_MethodInfo;
extern const MethodInfo AddEventAdapter_BeginInvoke_m8249_MethodInfo;
extern const MethodInfo AddEventAdapter_EndInvoke_m8250_MethodInfo;
static const Il2CppMethodReference AddEventAdapter_t1567_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&AddEventAdapter_Invoke_m8248_MethodInfo,
	&AddEventAdapter_BeginInvoke_m8249_MethodInfo,
	&AddEventAdapter_EndInvoke_m8250_MethodInfo,
};
static bool AddEventAdapter_t1567_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddEventAdapter_t1567_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AddEventAdapter_t1567_0_0_0;
extern const Il2CppType AddEventAdapter_t1567_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
extern TypeInfo EventInfo_t_il2cpp_TypeInfo;
extern const Il2CppType EventInfo_t_0_0_0;
struct AddEventAdapter_t1567;
const Il2CppTypeDefinitionMetadata AddEventAdapter_t1567_DefinitionMetadata = 
{
	&EventInfo_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddEventAdapter_t1567_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, AddEventAdapter_t1567_VTable/* vtableMethods */
	, AddEventAdapter_t1567_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AddEventAdapter_t1567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddEventAdapter"/* name */
	, ""/* namespaze */
	, AddEventAdapter_t1567_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddEventAdapter_t1567_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddEventAdapter_t1567_0_0_0/* byval_arg */
	, &AddEventAdapter_t1567_1_0_0/* this_arg */
	, &AddEventAdapter_t1567_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AddEventAdapter_t1567/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddEventAdapter_t1567)/* instance_size */
	, sizeof (AddEventAdapter_t1567)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
// Metadata Definition System.Reflection.EventInfo
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.EventInfo::.ctor()
extern const MethodInfo EventInfo__ctor_m8251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventInfo__ctor_m8251/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_EventAttributes_t1566 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventAttributes System.Reflection.EventInfo::get_Attributes()
extern const MethodInfo EventInfo_get_Attributes_m10922_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t1566_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t1566/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.EventInfo::get_EventHandlerType()
extern const MethodInfo EventInfo_get_EventHandlerType_m8252_MethodInfo = 
{
	"get_EventHandlerType"/* name */
	, (methodPointerType)&EventInfo_get_EventHandlerType_m8252/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.EventInfo::get_MemberType()
extern const MethodInfo EventInfo_get_MemberType_m8253_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&EventInfo_get_MemberType_m8253/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo EventInfo_t_EventInfo_GetAddMethod_m10923_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221617, 0, &Boolean_t72_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.EventInfo::GetAddMethod(System.Boolean)
extern const MethodInfo EventInfo_GetAddMethod_m10923_MethodInfo = 
{
	"GetAddMethod"/* name */
	, NULL/* method */
	, &EventInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, EventInfo_t_EventInfo_GetAddMethod_m10923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventInfo_t_MethodInfos[] =
{
	&EventInfo__ctor_m8251_MethodInfo,
	&EventInfo_get_Attributes_m10922_MethodInfo,
	&EventInfo_get_EventHandlerType_m8252_MethodInfo,
	&EventInfo_get_MemberType_m8253_MethodInfo,
	&EventInfo_GetAddMethod_m10923_MethodInfo,
	NULL
};
extern const MethodInfo EventInfo_get_Attributes_m10922_MethodInfo;
static const PropertyInfo EventInfo_t____Attributes_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &EventInfo_get_Attributes_m10922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo EventInfo_get_EventHandlerType_m8252_MethodInfo;
static const PropertyInfo EventInfo_t____EventHandlerType_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "EventHandlerType"/* name */
	, &EventInfo_get_EventHandlerType_m8252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo EventInfo_get_MemberType_m8253_MethodInfo;
static const PropertyInfo EventInfo_t____MemberType_PropertyInfo = 
{
	&EventInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &EventInfo_get_MemberType_m8253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* EventInfo_t_PropertyInfos[] =
{
	&EventInfo_t____Attributes_PropertyInfo,
	&EventInfo_t____EventHandlerType_PropertyInfo,
	&EventInfo_t____MemberType_PropertyInfo,
	NULL
};
static const Il2CppType* EventInfo_t_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AddEventAdapter_t1567_0_0_0,
};
static const Il2CppMethodReference EventInfo_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	&EventInfo_get_MemberType_m8253_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&EventInfo_get_EventHandlerType_m8252_MethodInfo,
	NULL,
};
static bool EventInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _EventInfo_t2000_0_0_0;
static const Il2CppType* EventInfo_t_InterfacesTypeInfos[] = 
{
	&_EventInfo_t2000_0_0_0,
};
static Il2CppInterfaceOffsetPair EventInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_EventInfo_t2000_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventInfo_t_1_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
struct EventInfo_t;
const Il2CppTypeDefinitionMetadata EventInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EventInfo_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EventInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, EventInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, EventInfo_t_VTable/* vtableMethods */
	, EventInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1188/* fieldStart */

};
TypeInfo EventInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, EventInfo_t_MethodInfos/* methods */
	, EventInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &EventInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 409/* custom_attributes_cache */
	, &EventInfo_t_0_0_0/* byval_arg */
	, &EventInfo_t_1_0_0/* this_arg */
	, &EventInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventInfo_t)/* instance_size */
	, sizeof (EventInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 17/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
// Metadata Definition System.Reflection.FieldAttributes
extern TypeInfo FieldAttributes_t1568_il2cpp_TypeInfo;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributesMethodDeclarations.h"
static const MethodInfo* FieldAttributes_t1568_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FieldAttributes_t1568_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool FieldAttributes_t1568_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FieldAttributes_t1568_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAttributes_t1568_0_0_0;
extern const Il2CppType FieldAttributes_t1568_1_0_0;
const Il2CppTypeDefinitionMetadata FieldAttributes_t1568_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAttributes_t1568_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, FieldAttributes_t1568_VTable/* vtableMethods */
	, FieldAttributes_t1568_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1189/* fieldStart */

};
TypeInfo FieldAttributes_t1568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, FieldAttributes_t1568_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 410/* custom_attributes_cache */
	, &FieldAttributes_t1568_0_0_0/* byval_arg */
	, &FieldAttributes_t1568_1_0_0/* this_arg */
	, &FieldAttributes_t1568_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAttributes_t1568)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FieldAttributes_t1568)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
// Metadata Definition System.Reflection.FieldInfo
extern TypeInfo FieldInfo_t_il2cpp_TypeInfo;
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::.ctor()
extern const MethodInfo FieldInfo__ctor_m8254_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldInfo__ctor_m8254/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FieldAttributes_t1568 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldAttributes System.Reflection.FieldInfo::get_Attributes()
extern const MethodInfo FieldInfo_get_Attributes_m10924_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t1568_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t1568/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeFieldHandle_t1372_0_0_0;
extern void* RuntimeInvoker_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeFieldHandle System.Reflection.FieldInfo::get_FieldHandle()
extern const MethodInfo FieldInfo_get_FieldHandle_m10925_MethodInfo = 
{
	"get_FieldHandle"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeFieldHandle_t1372_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeFieldHandle_t1372/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.FieldInfo::get_FieldType()
extern const MethodInfo FieldInfo_get_FieldType_m10926_MethodInfo = 
{
	"get_FieldType"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_GetValue_m10927_ParameterInfos[] = 
{
	{"obj", 0, 134221627, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.FieldInfo::GetValue(System.Object)
extern const MethodInfo FieldInfo_GetValue_m10927_MethodInfo = 
{
	"GetValue"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_GetValue_m10927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.FieldInfo::get_MemberType()
extern const MethodInfo FieldInfo_get_MemberType_m8255_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&FieldInfo_get_MemberType_m8255/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsLiteral()
extern const MethodInfo FieldInfo_get_IsLiteral_m8256_MethodInfo = 
{
	"get_IsLiteral"/* name */
	, (methodPointerType)&FieldInfo_get_IsLiteral_m8256/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
extern const MethodInfo FieldInfo_get_IsStatic_m8257_MethodInfo = 
{
	"get_IsStatic"/* name */
	, (methodPointerType)&FieldInfo_get_IsStatic_m8257/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsInitOnly()
extern const MethodInfo FieldInfo_get_IsInitOnly_m8258_MethodInfo = 
{
	"get_IsInitOnly"/* name */
	, (methodPointerType)&FieldInfo_get_IsInitOnly_m8258/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsPublic()
extern const MethodInfo FieldInfo_get_IsPublic_m8259_MethodInfo = 
{
	"get_IsPublic"/* name */
	, (methodPointerType)&FieldInfo_get_IsPublic_m8259/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized()
extern const MethodInfo FieldInfo_get_IsNotSerialized_m8260_MethodInfo = 
{
	"get_IsNotSerialized"/* name */
	, (methodPointerType)&FieldInfo_get_IsNotSerialized_m8260/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_SetValue_m10928_ParameterInfos[] = 
{
	{"obj", 0, 134221628, 0, &Object_t_0_0_0},
	{"value", 1, 134221629, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221630, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 3, 134221631, 0, &Binder_t781_0_0_0},
	{"culture", 4, 134221632, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern const MethodInfo FieldInfo_SetValue_m10928_MethodInfo = 
{
	"SetValue"/* name */
	, NULL/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_SetValue_m10928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_SetValue_m8261_ParameterInfos[] = 
{
	{"obj", 0, 134221633, 0, &Object_t_0_0_0},
	{"value", 1, 134221634, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern const MethodInfo FieldInfo_SetValue_m8261_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&FieldInfo_SetValue_m8261/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, FieldInfo_t_FieldInfo_SetValue_m8261_ParameterInfos/* parameters */
	, 412/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_internal_from_handle_type_m8262_ParameterInfos[] = 
{
	{"field_handle", 0, 134221635, 0, &IntPtr_t_0_0_0},
	{"type_handle", 1, 134221636, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType FieldInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.Reflection.FieldInfo::internal_from_handle_type(System.IntPtr,System.IntPtr)
extern const MethodInfo FieldInfo_internal_from_handle_type_m8262_MethodInfo = 
{
	"internal_from_handle_type"/* name */
	, (methodPointerType)&FieldInfo_internal_from_handle_type_m8262/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, FieldInfo_t_FieldInfo_internal_from_handle_type_m8262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeFieldHandle_t1372_0_0_0;
static const ParameterInfo FieldInfo_t_FieldInfo_GetFieldFromHandle_m8263_ParameterInfos[] = 
{
	{"handle", 0, 134221637, 0, &RuntimeFieldHandle_t1372_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.Reflection.FieldInfo::GetFieldFromHandle(System.RuntimeFieldHandle)
extern const MethodInfo FieldInfo_GetFieldFromHandle_m8263_MethodInfo = 
{
	"GetFieldFromHandle"/* name */
	, (methodPointerType)&FieldInfo_GetFieldFromHandle_m8263/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeFieldHandle_t1372/* invoker_method */
	, FieldInfo_t_FieldInfo_GetFieldFromHandle_m8263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.FieldInfo::GetFieldOffset()
extern const MethodInfo FieldInfo_GetFieldOffset_m8264_MethodInfo = 
{
	"GetFieldOffset"/* name */
	, (methodPointerType)&FieldInfo_GetFieldOffset_m8264/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnmanagedMarshal_t1543_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::GetUnmanagedMarshal()
extern const MethodInfo FieldInfo_GetUnmanagedMarshal_m8265_MethodInfo = 
{
	"GetUnmanagedMarshal"/* name */
	, (methodPointerType)&FieldInfo_GetUnmanagedMarshal_m8265/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedMarshal_t1543_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::get_UMarshal()
extern const MethodInfo FieldInfo_get_UMarshal_m8266_MethodInfo = 
{
	"get_UMarshal"/* name */
	, (methodPointerType)&FieldInfo_get_UMarshal_m8266/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &UnmanagedMarshal_t1543_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2499/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.FieldInfo::GetPseudoCustomAttributes()
extern const MethodInfo FieldInfo_GetPseudoCustomAttributes_m8267_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&FieldInfo_GetPseudoCustomAttributes_m8267/* method */
	, &FieldInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FieldInfo_t_MethodInfos[] =
{
	&FieldInfo__ctor_m8254_MethodInfo,
	&FieldInfo_get_Attributes_m10924_MethodInfo,
	&FieldInfo_get_FieldHandle_m10925_MethodInfo,
	&FieldInfo_get_FieldType_m10926_MethodInfo,
	&FieldInfo_GetValue_m10927_MethodInfo,
	&FieldInfo_get_MemberType_m8255_MethodInfo,
	&FieldInfo_get_IsLiteral_m8256_MethodInfo,
	&FieldInfo_get_IsStatic_m8257_MethodInfo,
	&FieldInfo_get_IsInitOnly_m8258_MethodInfo,
	&FieldInfo_get_IsPublic_m8259_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m8260_MethodInfo,
	&FieldInfo_SetValue_m10928_MethodInfo,
	&FieldInfo_SetValue_m8261_MethodInfo,
	&FieldInfo_internal_from_handle_type_m8262_MethodInfo,
	&FieldInfo_GetFieldFromHandle_m8263_MethodInfo,
	&FieldInfo_GetFieldOffset_m8264_MethodInfo,
	&FieldInfo_GetUnmanagedMarshal_m8265_MethodInfo,
	&FieldInfo_get_UMarshal_m8266_MethodInfo,
	&FieldInfo_GetPseudoCustomAttributes_m8267_MethodInfo,
	NULL
};
extern const MethodInfo FieldInfo_get_Attributes_m10924_MethodInfo;
static const PropertyInfo FieldInfo_t____Attributes_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &FieldInfo_get_Attributes_m10924_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_FieldHandle_m10925_MethodInfo;
static const PropertyInfo FieldInfo_t____FieldHandle_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "FieldHandle"/* name */
	, &FieldInfo_get_FieldHandle_m10925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_FieldType_m10926_MethodInfo;
static const PropertyInfo FieldInfo_t____FieldType_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "FieldType"/* name */
	, &FieldInfo_get_FieldType_m10926_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_MemberType_m8255_MethodInfo;
static const PropertyInfo FieldInfo_t____MemberType_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &FieldInfo_get_MemberType_m8255_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsLiteral_m8256_MethodInfo;
static const PropertyInfo FieldInfo_t____IsLiteral_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsLiteral"/* name */
	, &FieldInfo_get_IsLiteral_m8256_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsStatic_m8257_MethodInfo;
static const PropertyInfo FieldInfo_t____IsStatic_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsStatic"/* name */
	, &FieldInfo_get_IsStatic_m8257_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsInitOnly_m8258_MethodInfo;
static const PropertyInfo FieldInfo_t____IsInitOnly_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsInitOnly"/* name */
	, &FieldInfo_get_IsInitOnly_m8258_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsPublic_m8259_MethodInfo;
static const PropertyInfo FieldInfo_t____IsPublic_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsPublic"/* name */
	, &FieldInfo_get_IsPublic_m8259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_IsNotSerialized_m8260_MethodInfo;
static const PropertyInfo FieldInfo_t____IsNotSerialized_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "IsNotSerialized"/* name */
	, &FieldInfo_get_IsNotSerialized_m8260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo FieldInfo_get_UMarshal_m8266_MethodInfo;
static const PropertyInfo FieldInfo_t____UMarshal_PropertyInfo = 
{
	&FieldInfo_t_il2cpp_TypeInfo/* parent */
	, "UMarshal"/* name */
	, &FieldInfo_get_UMarshal_m8266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* FieldInfo_t_PropertyInfos[] =
{
	&FieldInfo_t____Attributes_PropertyInfo,
	&FieldInfo_t____FieldHandle_PropertyInfo,
	&FieldInfo_t____FieldType_PropertyInfo,
	&FieldInfo_t____MemberType_PropertyInfo,
	&FieldInfo_t____IsLiteral_PropertyInfo,
	&FieldInfo_t____IsStatic_PropertyInfo,
	&FieldInfo_t____IsInitOnly_PropertyInfo,
	&FieldInfo_t____IsPublic_PropertyInfo,
	&FieldInfo_t____IsNotSerialized_PropertyInfo,
	&FieldInfo_t____UMarshal_PropertyInfo,
	NULL
};
extern const MethodInfo FieldInfo_SetValue_m8261_MethodInfo;
extern const MethodInfo FieldInfo_GetFieldOffset_m8264_MethodInfo;
static const Il2CppMethodReference FieldInfo_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	&FieldInfo_get_MemberType_m8255_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&FieldInfo_get_IsLiteral_m8256_MethodInfo,
	&FieldInfo_get_IsStatic_m8257_MethodInfo,
	&FieldInfo_get_IsInitOnly_m8258_MethodInfo,
	&FieldInfo_get_IsPublic_m8259_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m8260_MethodInfo,
	NULL,
	&FieldInfo_SetValue_m8261_MethodInfo,
	&FieldInfo_GetFieldOffset_m8264_MethodInfo,
	&FieldInfo_get_UMarshal_m8266_MethodInfo,
};
static bool FieldInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _FieldInfo_t2001_0_0_0;
static const Il2CppType* FieldInfo_t_InterfacesTypeInfos[] = 
{
	&_FieldInfo_t2001_0_0_0,
};
static Il2CppInterfaceOffsetPair FieldInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_FieldInfo_t2001_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldInfo_t_1_0_0;
struct FieldInfo_t;
const Il2CppTypeDefinitionMetadata FieldInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FieldInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, FieldInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, FieldInfo_t_VTable/* vtableMethods */
	, FieldInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FieldInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldInfo"/* name */
	, "System.Reflection"/* namespaze */
	, FieldInfo_t_MethodInfos/* methods */
	, FieldInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &FieldInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 411/* custom_attributes_cache */
	, &FieldInfo_t_0_0_0/* byval_arg */
	, &FieldInfo_t_1_0_0/* this_arg */
	, &FieldInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldInfo_t)/* instance_size */
	, sizeof (FieldInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 10/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolder.h"
// Metadata Definition System.Reflection.MemberInfoSerializationHolder
extern TypeInfo MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo;
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder__ctor_m8268_ParameterInfos[] = 
{
	{"info", 0, 134221638, 0, &SerializationInfo_t725_0_0_0},
	{"ctx", 1, 134221639, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder__ctor_m8268_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder__ctor_m8268/* method */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder__ctor_m8268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberTypes_t1570_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_Serialize_m8269_ParameterInfos[] = 
{
	{"info", 0, 134221640, 0, &SerializationInfo_t725_0_0_0},
	{"name", 1, 134221641, 0, &String_t_0_0_0},
	{"klass", 2, 134221642, 0, &Type_t_0_0_0},
	{"signature", 3, 134221643, 0, &String_t_0_0_0},
	{"type", 4, 134221644, 0, &MemberTypes_t1570_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes)
extern const MethodInfo MemberInfoSerializationHolder_Serialize_m8269_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_Serialize_m8269/* method */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54/* invoker_method */
	, MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_Serialize_m8269_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberTypes_t1570_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_Serialize_m8270_ParameterInfos[] = 
{
	{"info", 0, 134221645, 0, &SerializationInfo_t725_0_0_0},
	{"name", 1, 134221646, 0, &String_t_0_0_0},
	{"klass", 2, 134221647, 0, &Type_t_0_0_0},
	{"signature", 3, 134221648, 0, &String_t_0_0_0},
	{"type", 4, 134221649, 0, &MemberTypes_t1570_0_0_0},
	{"genericArguments", 5, 134221650, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::Serialize(System.Runtime.Serialization.SerializationInfo,System.String,System.Type,System.String,System.Reflection.MemberTypes,System.Type[])
extern const MethodInfo MemberInfoSerializationHolder_Serialize_m8270_MethodInfo = 
{
	"Serialize"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_Serialize_m8270/* method */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t/* invoker_method */
	, MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_Serialize_m8270_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_GetObjectData_m8271_ParameterInfos[] = 
{
	{"info", 0, 134221651, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221652, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberInfoSerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder_GetObjectData_m8271_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_GetObjectData_m8271/* method */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_GetObjectData_m8271_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_GetRealObject_m8272_ParameterInfos[] = 
{
	{"context", 0, 134221653, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MemberInfoSerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberInfoSerializationHolder_GetRealObject_m8272_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&MemberInfoSerializationHolder_GetRealObject_m8272/* method */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t726/* invoker_method */
	, MemberInfoSerializationHolder_t1569_MemberInfoSerializationHolder_GetRealObject_m8272_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberInfoSerializationHolder_t1569_MethodInfos[] =
{
	&MemberInfoSerializationHolder__ctor_m8268_MethodInfo,
	&MemberInfoSerializationHolder_Serialize_m8269_MethodInfo,
	&MemberInfoSerializationHolder_Serialize_m8270_MethodInfo,
	&MemberInfoSerializationHolder_GetObjectData_m8271_MethodInfo,
	&MemberInfoSerializationHolder_GetRealObject_m8272_MethodInfo,
	NULL
};
extern const MethodInfo MemberInfoSerializationHolder_GetObjectData_m8271_MethodInfo;
extern const MethodInfo MemberInfoSerializationHolder_GetRealObject_m8272_MethodInfo;
static const Il2CppMethodReference MemberInfoSerializationHolder_t1569_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfoSerializationHolder_GetObjectData_m8271_MethodInfo,
	&MemberInfoSerializationHolder_GetRealObject_m8272_MethodInfo,
};
static bool MemberInfoSerializationHolder_t1569_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t1941_0_0_0;
static const Il2CppType* MemberInfoSerializationHolder_t1569_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&IObjectReference_t1941_0_0_0,
};
static Il2CppInterfaceOffsetPair MemberInfoSerializationHolder_t1569_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IObjectReference_t1941_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberInfoSerializationHolder_t1569_0_0_0;
extern const Il2CppType MemberInfoSerializationHolder_t1569_1_0_0;
struct MemberInfoSerializationHolder_t1569;
const Il2CppTypeDefinitionMetadata MemberInfoSerializationHolder_t1569_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MemberInfoSerializationHolder_t1569_InterfacesTypeInfos/* implementedInterfaces */
	, MemberInfoSerializationHolder_t1569_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MemberInfoSerializationHolder_t1569_VTable/* vtableMethods */
	, MemberInfoSerializationHolder_t1569_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1209/* fieldStart */

};
TypeInfo MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberInfoSerializationHolder"/* name */
	, "System.Reflection"/* namespaze */
	, MemberInfoSerializationHolder_t1569_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberInfoSerializationHolder_t1569_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MemberInfoSerializationHolder_t1569_0_0_0/* byval_arg */
	, &MemberInfoSerializationHolder_t1569_1_0_0/* this_arg */
	, &MemberInfoSerializationHolder_t1569_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberInfoSerializationHolder_t1569)/* instance_size */
	, sizeof (MemberInfoSerializationHolder_t1569)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
// Metadata Definition System.Reflection.MemberTypes
extern TypeInfo MemberTypes_t1570_il2cpp_TypeInfo;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypesMethodDeclarations.h"
static const MethodInfo* MemberTypes_t1570_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MemberTypes_t1570_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool MemberTypes_t1570_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberTypes_t1570_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberTypes_t1570_1_0_0;
const Il2CppTypeDefinitionMetadata MemberTypes_t1570_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberTypes_t1570_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, MemberTypes_t1570_VTable/* vtableMethods */
	, MemberTypes_t1570_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1214/* fieldStart */

};
TypeInfo MemberTypes_t1570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberTypes"/* name */
	, "System.Reflection"/* namespaze */
	, MemberTypes_t1570_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 413/* custom_attributes_cache */
	, &MemberTypes_t1570_0_0_0/* byval_arg */
	, &MemberTypes_t1570_1_0_0/* this_arg */
	, &MemberTypes_t1570_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberTypes_t1570)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MemberTypes_t1570)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
// Metadata Definition System.Reflection.MethodAttributes
extern TypeInfo MethodAttributes_t1571_il2cpp_TypeInfo;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributesMethodDeclarations.h"
static const MethodInfo* MethodAttributes_t1571_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodAttributes_t1571_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool MethodAttributes_t1571_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodAttributes_t1571_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAttributes_t1571_0_0_0;
extern const Il2CppType MethodAttributes_t1571_1_0_0;
const Il2CppTypeDefinitionMetadata MethodAttributes_t1571_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAttributes_t1571_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, MethodAttributes_t1571_VTable/* vtableMethods */
	, MethodAttributes_t1571_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1224/* fieldStart */

};
TypeInfo MethodAttributes_t1571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, MethodAttributes_t1571_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 414/* custom_attributes_cache */
	, &MethodAttributes_t1571_0_0_0/* byval_arg */
	, &MethodAttributes_t1571_1_0_0/* this_arg */
	, &MethodAttributes_t1571_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAttributes_t1571)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodAttributes_t1571)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// Metadata Definition System.Reflection.MethodBase
extern TypeInfo MethodBase_t47_il2cpp_TypeInfo;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MethodBase::.ctor()
extern const MethodInfo MethodBase__ctor_m8273_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodBase__ctor_m8273/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeMethodHandle_t1871_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t1871_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_GetMethodFromHandleNoGenericCheck_m8274_ParameterInfos[] = 
{
	{"handle", 0, 134221654, 0, &RuntimeMethodHandle_t1871_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleNoGenericCheck(System.RuntimeMethodHandle)
extern const MethodInfo MethodBase_GetMethodFromHandleNoGenericCheck_m8274_MethodInfo = 
{
	"GetMethodFromHandleNoGenericCheck"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandleNoGenericCheck_m8274/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeMethodHandle_t1871/* invoker_method */
	, MethodBase_t47_MethodBase_GetMethodFromHandleNoGenericCheck_m8274_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_GetMethodFromIntPtr_m8275_ParameterInfos[] = 
{
	{"handle", 0, 134221655, 0, &IntPtr_t_0_0_0},
	{"declaringType", 1, 134221656, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromIntPtr(System.IntPtr,System.IntPtr)
extern const MethodInfo MethodBase_GetMethodFromIntPtr_m8275_MethodInfo = 
{
	"GetMethodFromIntPtr"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromIntPtr_m8275/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, MethodBase_t47_MethodBase_GetMethodFromIntPtr_m8275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeMethodHandle_t1871_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_GetMethodFromHandle_m8276_ParameterInfos[] = 
{
	{"handle", 0, 134221657, 0, &RuntimeMethodHandle_t1871_0_0_0},
};
extern void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandle(System.RuntimeMethodHandle)
extern const MethodInfo MethodBase_GetMethodFromHandle_m8276_MethodInfo = 
{
	"GetMethodFromHandle"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandle_m8276/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RuntimeMethodHandle_t1871/* invoker_method */
	, MethodBase_t47_MethodBase_GetMethodFromHandle_m8276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_GetMethodFromHandleInternalType_m8277_ParameterInfos[] = 
{
	{"method_handle", 0, 134221658, 0, &IntPtr_t_0_0_0},
	{"type_handle", 1, 134221659, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleInternalType(System.IntPtr,System.IntPtr)
extern const MethodInfo MethodBase_GetMethodFromHandleInternalType_m8277_MethodInfo = 
{
	"GetMethodFromHandleInternalType"/* name */
	, (methodPointerType)&MethodBase_GetMethodFromHandleInternalType_m8277/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t/* invoker_method */
	, MethodBase_t47_MethodBase_GetMethodFromHandleInternalType_m8277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters()
extern const MethodInfo MethodBase_GetParameters_m10929_MethodInfo = 
{
	"GetParameters"/* name */
	, NULL/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_Invoke_m8278_ParameterInfos[] = 
{
	{"obj", 0, 134221660, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221661, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern const MethodInfo MethodBase_Invoke_m8278_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MethodBase_Invoke_m8278/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodBase_t47_MethodBase_Invoke_m8278_ParameterInfos/* parameters */
	, 416/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MethodBase_t47_MethodBase_Invoke_m10930_ParameterInfos[] = 
{
	{"obj", 0, 134221662, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221663, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134221664, 0, &Binder_t781_0_0_0},
	{"parameters", 3, 134221665, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 4, 134221666, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MethodBase_Invoke_m10930_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MethodBase_t47_MethodBase_Invoke_m10930_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MethodBase::get_MethodHandle()
extern const MethodInfo MethodBase_get_MethodHandle_m10931_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, NULL/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t1871_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t1871/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t1571 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MethodBase::get_Attributes()
extern const MethodInfo MethodBase_get_Attributes_m10932_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1571_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1571/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t1565 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention()
extern const MethodInfo MethodBase_get_CallingConvention_m8279_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MethodBase_get_CallingConvention_m8279/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1565_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1565/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsPublic()
extern const MethodInfo MethodBase_get_IsPublic_m8280_MethodInfo = 
{
	"get_IsPublic"/* name */
	, (methodPointerType)&MethodBase_get_IsPublic_m8280/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsStatic()
extern const MethodInfo MethodBase_get_IsStatic_m8281_MethodInfo = 
{
	"get_IsStatic"/* name */
	, (methodPointerType)&MethodBase_get_IsStatic_m8281/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsVirtual()
extern const MethodInfo MethodBase_get_IsVirtual_m8282_MethodInfo = 
{
	"get_IsVirtual"/* name */
	, (methodPointerType)&MethodBase_get_IsVirtual_m8282/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MethodBase::GetGenericArguments()
extern const MethodInfo MethodBase_GetGenericArguments_m8283_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MethodBase_GetGenericArguments_m8283/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 417/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_ContainsGenericParameters()
extern const MethodInfo MethodBase_get_ContainsGenericParameters_m8284_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MethodBase_get_ContainsGenericParameters_m8284/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethodDefinition()
extern const MethodInfo MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MethodBase_get_IsGenericMethodDefinition_m8285/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethod()
extern const MethodInfo MethodBase_get_IsGenericMethod_m8286_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MethodBase_get_IsGenericMethod_m8286/* method */
	, &MethodBase_t47_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodBase_t47_MethodInfos[] =
{
	&MethodBase__ctor_m8273_MethodInfo,
	&MethodBase_GetMethodFromHandleNoGenericCheck_m8274_MethodInfo,
	&MethodBase_GetMethodFromIntPtr_m8275_MethodInfo,
	&MethodBase_GetMethodFromHandle_m8276_MethodInfo,
	&MethodBase_GetMethodFromHandleInternalType_m8277_MethodInfo,
	&MethodBase_GetParameters_m10929_MethodInfo,
	&MethodBase_Invoke_m8278_MethodInfo,
	&MethodBase_Invoke_m10930_MethodInfo,
	&MethodBase_get_MethodHandle_m10931_MethodInfo,
	&MethodBase_get_Attributes_m10932_MethodInfo,
	&MethodBase_get_CallingConvention_m8279_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodBase_GetGenericArguments_m8283_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m8284_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo,
	&MethodBase_get_IsGenericMethod_m8286_MethodInfo,
	NULL
};
extern const MethodInfo MethodBase_get_MethodHandle_m10931_MethodInfo;
static const PropertyInfo MethodBase_t47____MethodHandle_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MethodBase_get_MethodHandle_m10931_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodBase_get_Attributes_m10932_MethodInfo;
static const PropertyInfo MethodBase_t47____Attributes_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MethodBase_get_Attributes_m10932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____CallingConvention_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MethodBase_get_CallingConvention_m8279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____IsPublic_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "IsPublic"/* name */
	, &MethodBase_get_IsPublic_m8280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____IsStatic_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "IsStatic"/* name */
	, &MethodBase_get_IsStatic_m8281_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____IsVirtual_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "IsVirtual"/* name */
	, &MethodBase_get_IsVirtual_m8282_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____ContainsGenericParameters_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MethodBase_get_ContainsGenericParameters_m8284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____IsGenericMethodDefinition_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodBase_t47____IsGenericMethod_PropertyInfo = 
{
	&MethodBase_t47_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MethodBase_get_IsGenericMethod_m8286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodBase_t47_PropertyInfos[] =
{
	&MethodBase_t47____MethodHandle_PropertyInfo,
	&MethodBase_t47____Attributes_PropertyInfo,
	&MethodBase_t47____CallingConvention_PropertyInfo,
	&MethodBase_t47____IsPublic_PropertyInfo,
	&MethodBase_t47____IsStatic_PropertyInfo,
	&MethodBase_t47____IsVirtual_PropertyInfo,
	&MethodBase_t47____ContainsGenericParameters_PropertyInfo,
	&MethodBase_t47____IsGenericMethodDefinition_PropertyInfo,
	&MethodBase_t47____IsGenericMethod_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MethodBase_t47_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m8278_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m8279_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodBase_GetGenericArguments_m8283_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m8284_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo,
	&MethodBase_get_IsGenericMethod_m8286_MethodInfo,
};
static bool MethodBase_t47_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodBase_t47_InterfacesTypeInfos[] = 
{
	&_MethodBase_t2002_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodBase_t47_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_MethodBase_t2002_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodBase_t47_1_0_0;
struct MethodBase_t47;
const Il2CppTypeDefinitionMetadata MethodBase_t47_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodBase_t47_InterfacesTypeInfos/* implementedInterfaces */
	, MethodBase_t47_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, MethodBase_t47_VTable/* vtableMethods */
	, MethodBase_t47_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodBase_t47_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodBase"/* name */
	, "System.Reflection"/* namespaze */
	, MethodBase_t47_MethodInfos/* methods */
	, MethodBase_t47_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodBase_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 415/* custom_attributes_cache */
	, &MethodBase_t47_0_0_0/* byval_arg */
	, &MethodBase_t47_1_0_0/* this_arg */
	, &MethodBase_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodBase_t47)/* instance_size */
	, sizeof (MethodBase_t47)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 9/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
// Metadata Definition System.Reflection.MethodImplAttributes
extern TypeInfo MethodImplAttributes_t1572_il2cpp_TypeInfo;
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributesMethodDeclarations.h"
static const MethodInfo* MethodImplAttributes_t1572_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodImplAttributes_t1572_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool MethodImplAttributes_t1572_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodImplAttributes_t1572_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodImplAttributes_t1572_0_0_0;
extern const Il2CppType MethodImplAttributes_t1572_1_0_0;
const Il2CppTypeDefinitionMetadata MethodImplAttributes_t1572_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodImplAttributes_t1572_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, MethodImplAttributes_t1572_VTable/* vtableMethods */
	, MethodImplAttributes_t1572_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1249/* fieldStart */

};
TypeInfo MethodImplAttributes_t1572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodImplAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, MethodImplAttributes_t1572_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 418/* custom_attributes_cache */
	, &MethodImplAttributes_t1572_0_0_0/* byval_arg */
	, &MethodImplAttributes_t1572_1_0_0/* this_arg */
	, &MethodImplAttributes_t1572_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodImplAttributes_t1572)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodImplAttributes_t1572)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Metadata Definition System.Reflection.MethodInfo
extern TypeInfo MethodInfo_t_il2cpp_TypeInfo;
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MethodInfo::.ctor()
extern const MethodInfo MethodInfo__ctor_m8287_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodInfo__ctor_m8287/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MethodInfo::GetBaseDefinition()
extern const MethodInfo MethodInfo_GetBaseDefinition_m10933_MethodInfo = 
{
	"GetBaseDefinition"/* name */
	, NULL/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.MethodInfo::get_MemberType()
extern const MethodInfo MethodInfo_get_MemberType_m8288_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&MethodInfo_get_MemberType_m8288/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MethodInfo::get_ReturnType()
extern const MethodInfo MethodInfo_get_ReturnType_m8289_MethodInfo = 
{
	"get_ReturnType"/* name */
	, (methodPointerType)&MethodInfo_get_ReturnType_m8289/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo MethodInfo_t_MethodInfo_MakeGenericMethod_m8290_ParameterInfos[] = 
{
	{"typeArguments", 0, 134221667, 420, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[])
extern const MethodInfo MethodInfo_MakeGenericMethod_m8290_MethodInfo = 
{
	"MakeGenericMethod"/* name */
	, (methodPointerType)&MethodInfo_MakeGenericMethod_m8290/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodInfo_t_MethodInfo_MakeGenericMethod_m8290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MethodInfo::GetGenericArguments()
extern const MethodInfo MethodInfo_GetGenericArguments_m8291_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MethodInfo_GetGenericArguments_m8291/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 421/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethod()
extern const MethodInfo MethodInfo_get_IsGenericMethod_m8292_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MethodInfo_get_IsGenericMethod_m8292/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethodDefinition()
extern const MethodInfo MethodInfo_get_IsGenericMethodDefinition_m8293_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MethodInfo_get_IsGenericMethodDefinition_m8293/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MethodInfo::get_ContainsGenericParameters()
extern const MethodInfo MethodInfo_get_ContainsGenericParameters_m8294_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MethodInfo_get_ContainsGenericParameters_m8294/* method */
	, &MethodInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodInfo_t_MethodInfos[] =
{
	&MethodInfo__ctor_m8287_MethodInfo,
	&MethodInfo_GetBaseDefinition_m10933_MethodInfo,
	&MethodInfo_get_MemberType_m8288_MethodInfo,
	&MethodInfo_get_ReturnType_m8289_MethodInfo,
	&MethodInfo_MakeGenericMethod_m8290_MethodInfo,
	&MethodInfo_GetGenericArguments_m8291_MethodInfo,
	&MethodInfo_get_IsGenericMethod_m8292_MethodInfo,
	&MethodInfo_get_IsGenericMethodDefinition_m8293_MethodInfo,
	&MethodInfo_get_ContainsGenericParameters_m8294_MethodInfo,
	NULL
};
extern const MethodInfo MethodInfo_get_MemberType_m8288_MethodInfo;
static const PropertyInfo MethodInfo_t____MemberType_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &MethodInfo_get_MemberType_m8288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_ReturnType_m8289_MethodInfo;
static const PropertyInfo MethodInfo_t____ReturnType_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "ReturnType"/* name */
	, &MethodInfo_get_ReturnType_m8289_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_IsGenericMethod_m8292_MethodInfo;
static const PropertyInfo MethodInfo_t____IsGenericMethod_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MethodInfo_get_IsGenericMethod_m8292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_IsGenericMethodDefinition_m8293_MethodInfo;
static const PropertyInfo MethodInfo_t____IsGenericMethodDefinition_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MethodInfo_get_IsGenericMethodDefinition_m8293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodInfo_get_ContainsGenericParameters_m8294_MethodInfo;
static const PropertyInfo MethodInfo_t____ContainsGenericParameters_PropertyInfo = 
{
	&MethodInfo_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MethodInfo_get_ContainsGenericParameters_m8294_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodInfo_t_PropertyInfos[] =
{
	&MethodInfo_t____MemberType_PropertyInfo,
	&MethodInfo_t____ReturnType_PropertyInfo,
	&MethodInfo_t____IsGenericMethod_PropertyInfo,
	&MethodInfo_t____IsGenericMethodDefinition_PropertyInfo,
	&MethodInfo_t____ContainsGenericParameters_PropertyInfo,
	NULL
};
extern const MethodInfo MethodInfo_GetGenericArguments_m8291_MethodInfo;
extern const MethodInfo MethodInfo_MakeGenericMethod_m8290_MethodInfo;
static const Il2CppMethodReference MethodInfo_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	&MethodInfo_get_MemberType_m8288_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	&MethodBase_Invoke_m8278_MethodInfo,
	NULL,
	NULL,
	NULL,
	&MethodBase_get_CallingConvention_m8279_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodInfo_GetGenericArguments_m8291_MethodInfo,
	&MethodInfo_get_ContainsGenericParameters_m8294_MethodInfo,
	&MethodInfo_get_IsGenericMethodDefinition_m8293_MethodInfo,
	&MethodInfo_get_IsGenericMethod_m8292_MethodInfo,
	NULL,
	&MethodInfo_get_ReturnType_m8289_MethodInfo,
	&MethodInfo_MakeGenericMethod_m8290_MethodInfo,
};
static bool MethodInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _MethodInfo_t2003_0_0_0;
static const Il2CppType* MethodInfo_t_InterfacesTypeInfos[] = 
{
	&_MethodInfo_t2003_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodInfo_t_InterfacesOffsets[] = 
{
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_MethodInfo_t2003_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodInfo_t_1_0_0;
struct MethodInfo_t;
const Il2CppTypeDefinitionMetadata MethodInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, MethodInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t47_0_0_0/* parent */
	, MethodInfo_t_VTable/* vtableMethods */
	, MethodInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MethodInfo_t_MethodInfos/* methods */
	, MethodInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 419/* custom_attributes_cache */
	, &MethodInfo_t_0_0_0/* byval_arg */
	, &MethodInfo_t_1_0_0/* this_arg */
	, &MethodInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodInfo_t)/* instance_size */
	, sizeof (MethodInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 30/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Missing
#include "mscorlib_System_Reflection_Missing.h"
// Metadata Definition System.Reflection.Missing
extern TypeInfo Missing_t1573_il2cpp_TypeInfo;
// System.Reflection.Missing
#include "mscorlib_System_Reflection_MissingMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::.ctor()
extern const MethodInfo Missing__ctor_m8295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Missing__ctor_m8295/* method */
	, &Missing_t1573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::.cctor()
extern const MethodInfo Missing__cctor_m8296_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Missing__cctor_m8296/* method */
	, &Missing_t1573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo Missing_t1573_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_ParameterInfos[] = 
{
	{"info", 0, 134221668, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221669, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Missing::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297/* method */
	, &Missing_t1573_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, Missing_t1573_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_ParameterInfos/* parameters */
	, 423/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Missing_t1573_MethodInfos[] =
{
	&Missing__ctor_m8295_MethodInfo,
	&Missing__cctor_m8296_MethodInfo,
	&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_MethodInfo,
	NULL
};
extern const MethodInfo Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_MethodInfo;
static const Il2CppMethodReference Missing_t1573_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8297_MethodInfo,
};
static bool Missing_t1573_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Missing_t1573_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair Missing_t1573_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Missing_t1573_0_0_0;
extern const Il2CppType Missing_t1573_1_0_0;
struct Missing_t1573;
const Il2CppTypeDefinitionMetadata Missing_t1573_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Missing_t1573_InterfacesTypeInfos/* implementedInterfaces */
	, Missing_t1573_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Missing_t1573_VTable/* vtableMethods */
	, Missing_t1573_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1264/* fieldStart */

};
TypeInfo Missing_t1573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Missing"/* name */
	, "System.Reflection"/* namespaze */
	, Missing_t1573_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Missing_t1573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 422/* custom_attributes_cache */
	, &Missing_t1573_0_0_0/* byval_arg */
	, &Missing_t1573_1_0_0/* this_arg */
	, &Missing_t1573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Missing_t1573)/* instance_size */
	, sizeof (Missing_t1573)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Missing_t1573_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// Metadata Definition System.Reflection.Module
extern TypeInfo Module_t1549_il2cpp_TypeInfo;
// System.Reflection.Module
#include "mscorlib_System_Reflection_ModuleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::.ctor()
extern const MethodInfo Module__ctor_m8298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Module__ctor_m8298/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::.cctor()
extern const MethodInfo Module__cctor_m8299_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Module__cctor_m8299/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t1164_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.Reflection.Module::get_Assembly()
extern const MethodInfo Module_get_Assembly_m8300_MethodInfo = 
{
	"get_Assembly"/* name */
	, (methodPointerType)&Module_get_Assembly_m8300/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t1164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.Module::get_ScopeName()
extern const MethodInfo Module_get_ScopeName_m8301_MethodInfo = 
{
	"get_ScopeName"/* name */
	, (methodPointerType)&Module_get_ScopeName_m8301/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Module_t1549_Module_GetCustomAttributes_m8302_ParameterInfos[] = 
{
	{"attributeType", 0, 134221670, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221671, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.Module::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo Module_GetCustomAttributes_m8302_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&Module_GetCustomAttributes_m8302/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, Module_t1549_Module_GetCustomAttributes_m8302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo Module_t1549_Module_GetObjectData_m8303_ParameterInfos[] = 
{
	{"info", 0, 134221672, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221673, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Module::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Module_GetObjectData_m8303_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&Module_GetObjectData_m8303/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, Module_t1549_Module_GetObjectData_m8303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Module_t1549_Module_IsDefined_m8304_ParameterInfos[] = 
{
	{"attributeType", 0, 134221674, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221675, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::IsDefined(System.Type,System.Boolean)
extern const MethodInfo Module_IsDefined_m8304_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&Module_IsDefined_m8304/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, Module_t1549_Module_IsDefined_m8304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::IsResource()
extern const MethodInfo Module_IsResource_m8305_MethodInfo = 
{
	"IsResource"/* name */
	, (methodPointerType)&Module_IsResource_m8305/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.Module::ToString()
extern const MethodInfo Module_ToString_m8306_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Module_ToString_m8306/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Module_t1549_Module_filter_by_type_name_m8307_ParameterInfos[] = 
{
	{"m", 0, 134221676, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134221677, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::filter_by_type_name(System.Type,System.Object)
extern const MethodInfo Module_filter_by_type_name_m8307_MethodInfo = 
{
	"filter_by_type_name"/* name */
	, (methodPointerType)&Module_filter_by_type_name_m8307/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Module_t1549_Module_filter_by_type_name_m8307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Module_t1549_Module_filter_by_type_name_ignore_case_m8308_ParameterInfos[] = 
{
	{"m", 0, 134221678, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134221679, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.Module::filter_by_type_name_ignore_case(System.Type,System.Object)
extern const MethodInfo Module_filter_by_type_name_ignore_case_m8308_MethodInfo = 
{
	"filter_by_type_name_ignore_case"/* name */
	, (methodPointerType)&Module_filter_by_type_name_ignore_case_m8308/* method */
	, &Module_t1549_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Module_t1549_Module_filter_by_type_name_ignore_case_m8308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Module_t1549_MethodInfos[] =
{
	&Module__ctor_m8298_MethodInfo,
	&Module__cctor_m8299_MethodInfo,
	&Module_get_Assembly_m8300_MethodInfo,
	&Module_get_ScopeName_m8301_MethodInfo,
	&Module_GetCustomAttributes_m8302_MethodInfo,
	&Module_GetObjectData_m8303_MethodInfo,
	&Module_IsDefined_m8304_MethodInfo,
	&Module_IsResource_m8305_MethodInfo,
	&Module_ToString_m8306_MethodInfo,
	&Module_filter_by_type_name_m8307_MethodInfo,
	&Module_filter_by_type_name_ignore_case_m8308_MethodInfo,
	NULL
};
extern const MethodInfo Module_get_Assembly_m8300_MethodInfo;
static const PropertyInfo Module_t1549____Assembly_PropertyInfo = 
{
	&Module_t1549_il2cpp_TypeInfo/* parent */
	, "Assembly"/* name */
	, &Module_get_Assembly_m8300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Module_get_ScopeName_m8301_MethodInfo;
static const PropertyInfo Module_t1549____ScopeName_PropertyInfo = 
{
	&Module_t1549_il2cpp_TypeInfo/* parent */
	, "ScopeName"/* name */
	, &Module_get_ScopeName_m8301_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Module_t1549_PropertyInfos[] =
{
	&Module_t1549____Assembly_PropertyInfo,
	&Module_t1549____ScopeName_PropertyInfo,
	NULL
};
extern const MethodInfo Module_ToString_m8306_MethodInfo;
extern const MethodInfo Module_GetObjectData_m8303_MethodInfo;
extern const MethodInfo Module_GetCustomAttributes_m8302_MethodInfo;
extern const MethodInfo Module_IsDefined_m8304_MethodInfo;
static const Il2CppMethodReference Module_t1549_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Module_ToString_m8306_MethodInfo,
	&Module_GetObjectData_m8303_MethodInfo,
	&Module_GetCustomAttributes_m8302_MethodInfo,
	&Module_IsDefined_m8304_MethodInfo,
	&Module_GetCustomAttributes_m8302_MethodInfo,
	&Module_GetObjectData_m8303_MethodInfo,
	&Module_IsDefined_m8304_MethodInfo,
};
static bool Module_t1549_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Module_t2004_0_0_0;
static const Il2CppType* Module_t1549_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&ICustomAttributeProvider_t1925_0_0_0,
	&_Module_t2004_0_0_0,
};
static Il2CppInterfaceOffsetPair Module_t1549_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &ICustomAttributeProvider_t1925_0_0_0, 5},
	{ &_Module_t2004_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Module_t1549_0_0_0;
extern const Il2CppType Module_t1549_1_0_0;
struct Module_t1549;
const Il2CppTypeDefinitionMetadata Module_t1549_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Module_t1549_InterfacesTypeInfos/* implementedInterfaces */
	, Module_t1549_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Module_t1549_VTable/* vtableMethods */
	, Module_t1549_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1265/* fieldStart */

};
TypeInfo Module_t1549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Module"/* name */
	, "System.Reflection"/* namespaze */
	, Module_t1549_MethodInfos/* methods */
	, Module_t1549_PropertyInfos/* properties */
	, NULL/* events */
	, &Module_t1549_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 424/* custom_attributes_cache */
	, &Module_t1549_0_0_0/* byval_arg */
	, &Module_t1549_1_0_0/* this_arg */
	, &Module_t1549_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Module_t1549)/* instance_size */
	, sizeof (Module_t1549)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Module_t1549_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
// Metadata Definition System.Reflection.MonoEventInfo
extern TypeInfo MonoEventInfo_t1576_il2cpp_TypeInfo;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfoMethodDeclarations.h"
extern const Il2CppType MonoEvent_t_0_0_0;
extern const Il2CppType MonoEvent_t_0_0_0;
extern const Il2CppType MonoEventInfo_t1576_1_0_2;
extern const Il2CppType MonoEventInfo_t1576_1_0_0;
static const ParameterInfo MonoEventInfo_t1576_MonoEventInfo_get_event_info_m8309_ParameterInfos[] = 
{
	{"ev", 0, 134221680, 0, &MonoEvent_t_0_0_0},
	{"info", 1, 134221681, 0, &MonoEventInfo_t1576_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_MonoEventInfoU26_t2373 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)
extern const MethodInfo MonoEventInfo_get_event_info_m8309_MethodInfo = 
{
	"get_event_info"/* name */
	, (methodPointerType)&MonoEventInfo_get_event_info_m8309/* method */
	, &MonoEventInfo_t1576_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_MonoEventInfoU26_t2373/* invoker_method */
	, MonoEventInfo_t1576_MonoEventInfo_get_event_info_m8309_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoEvent_t_0_0_0;
static const ParameterInfo MonoEventInfo_t1576_MonoEventInfo_GetEventInfo_m8310_ParameterInfos[] = 
{
	{"ev", 0, 134221682, 0, &MonoEvent_t_0_0_0},
};
extern const Il2CppType MonoEventInfo_t1576_0_0_0;
extern void* RuntimeInvoker_MonoEventInfo_t1576_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoEventInfo System.Reflection.MonoEventInfo::GetEventInfo(System.Reflection.MonoEvent)
extern const MethodInfo MonoEventInfo_GetEventInfo_m8310_MethodInfo = 
{
	"GetEventInfo"/* name */
	, (methodPointerType)&MonoEventInfo_GetEventInfo_m8310/* method */
	, &MonoEventInfo_t1576_il2cpp_TypeInfo/* declaring_type */
	, &MonoEventInfo_t1576_0_0_0/* return_type */
	, RuntimeInvoker_MonoEventInfo_t1576_Object_t/* invoker_method */
	, MonoEventInfo_t1576_MonoEventInfo_GetEventInfo_m8310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoEventInfo_t1576_MethodInfos[] =
{
	&MonoEventInfo_get_event_info_m8309_MethodInfo,
	&MonoEventInfo_GetEventInfo_m8310_MethodInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
static const Il2CppMethodReference MonoEventInfo_t1576_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool MonoEventInfo_t1576_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueType_t439_0_0_0;
const Il2CppTypeDefinitionMetadata MonoEventInfo_t1576_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, MonoEventInfo_t1576_VTable/* vtableMethods */
	, MonoEventInfo_t1576_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1275/* fieldStart */

};
TypeInfo MonoEventInfo_t1576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoEventInfo_t1576_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoEventInfo_t1576_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEventInfo_t1576_0_0_0/* byval_arg */
	, &MonoEventInfo_t1576_1_0_0/* this_arg */
	, &MonoEventInfo_t1576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEventInfo_t1576)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoEventInfo_t1576)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEvent.h"
// Metadata Definition System.Reflection.MonoEvent
extern TypeInfo MonoEvent_t_il2cpp_TypeInfo;
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEvent::.ctor()
extern const MethodInfo MonoEvent__ctor_m8311_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoEvent__ctor_m8311/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_EventAttributes_t1566 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventAttributes System.Reflection.MonoEvent::get_Attributes()
extern const MethodInfo MonoEvent_get_Attributes_m8312_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoEvent_get_Attributes_m8312/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &EventAttributes_t1566_0_0_0/* return_type */
	, RuntimeInvoker_EventAttributes_t1566/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetAddMethod_m8313_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221683, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoEvent::GetAddMethod(System.Boolean)
extern const MethodInfo MonoEvent_GetAddMethod_m8313_MethodInfo = 
{
	"GetAddMethod"/* name */
	, (methodPointerType)&MonoEvent_GetAddMethod_m8313/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoEvent_t_MonoEvent_GetAddMethod_m8313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoEvent::get_DeclaringType()
extern const MethodInfo MonoEvent_get_DeclaringType_m8314_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoEvent_get_DeclaringType_m8314/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoEvent::get_ReflectedType()
extern const MethodInfo MonoEvent_get_ReflectedType_m8315_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoEvent_get_ReflectedType_m8315/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoEvent::get_Name()
extern const MethodInfo MonoEvent_get_Name_m8316_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoEvent_get_Name_m8316/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoEvent::ToString()
extern const MethodInfo MonoEvent_ToString_m8317_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoEvent_ToString_m8317/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_IsDefined_m8318_ParameterInfos[] = 
{
	{"attributeType", 0, 134221684, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221685, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoEvent::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoEvent_IsDefined_m8318_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoEvent_IsDefined_m8318/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoEvent_t_MonoEvent_IsDefined_m8318_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetCustomAttributes_m8319_ParameterInfos[] = 
{
	{"inherit", 0, 134221686, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoEvent_GetCustomAttributes_m8319_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoEvent_GetCustomAttributes_m8319/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoEvent_t_MonoEvent_GetCustomAttributes_m8319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetCustomAttributes_m8320_ParameterInfos[] = 
{
	{"attributeType", 0, 134221687, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221688, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoEvent::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoEvent_GetCustomAttributes_m8320_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoEvent_GetCustomAttributes_m8320/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoEvent_t_MonoEvent_GetCustomAttributes_m8320_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoEvent_t_MonoEvent_GetObjectData_m8321_ParameterInfos[] = 
{
	{"info", 0, 134221689, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221690, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoEvent::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoEvent_GetObjectData_m8321_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoEvent_GetObjectData_m8321/* method */
	, &MonoEvent_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoEvent_t_MonoEvent_GetObjectData_m8321_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoEvent_t_MethodInfos[] =
{
	&MonoEvent__ctor_m8311_MethodInfo,
	&MonoEvent_get_Attributes_m8312_MethodInfo,
	&MonoEvent_GetAddMethod_m8313_MethodInfo,
	&MonoEvent_get_DeclaringType_m8314_MethodInfo,
	&MonoEvent_get_ReflectedType_m8315_MethodInfo,
	&MonoEvent_get_Name_m8316_MethodInfo,
	&MonoEvent_ToString_m8317_MethodInfo,
	&MonoEvent_IsDefined_m8318_MethodInfo,
	&MonoEvent_GetCustomAttributes_m8319_MethodInfo,
	&MonoEvent_GetCustomAttributes_m8320_MethodInfo,
	&MonoEvent_GetObjectData_m8321_MethodInfo,
	NULL
};
extern const MethodInfo MonoEvent_get_Attributes_m8312_MethodInfo;
static const PropertyInfo MonoEvent_t____Attributes_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoEvent_get_Attributes_m8312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_DeclaringType_m8314_MethodInfo;
static const PropertyInfo MonoEvent_t____DeclaringType_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoEvent_get_DeclaringType_m8314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_ReflectedType_m8315_MethodInfo;
static const PropertyInfo MonoEvent_t____ReflectedType_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoEvent_get_ReflectedType_m8315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoEvent_get_Name_m8316_MethodInfo;
static const PropertyInfo MonoEvent_t____Name_PropertyInfo = 
{
	&MonoEvent_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoEvent_get_Name_m8316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoEvent_t_PropertyInfos[] =
{
	&MonoEvent_t____Attributes_PropertyInfo,
	&MonoEvent_t____DeclaringType_PropertyInfo,
	&MonoEvent_t____ReflectedType_PropertyInfo,
	&MonoEvent_t____Name_PropertyInfo,
	NULL
};
extern const MethodInfo MonoEvent_ToString_m8317_MethodInfo;
extern const MethodInfo MonoEvent_GetCustomAttributes_m8320_MethodInfo;
extern const MethodInfo MonoEvent_IsDefined_m8318_MethodInfo;
extern const MethodInfo MonoEvent_GetCustomAttributes_m8319_MethodInfo;
extern const MethodInfo MonoEvent_GetAddMethod_m8313_MethodInfo;
extern const MethodInfo MonoEvent_GetObjectData_m8321_MethodInfo;
static const Il2CppMethodReference MonoEvent_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoEvent_ToString_m8317_MethodInfo,
	&MonoEvent_GetCustomAttributes_m8320_MethodInfo,
	&MonoEvent_IsDefined_m8318_MethodInfo,
	&MonoEvent_get_DeclaringType_m8314_MethodInfo,
	&EventInfo_get_MemberType_m8253_MethodInfo,
	&MonoEvent_get_Name_m8316_MethodInfo,
	&MonoEvent_get_ReflectedType_m8315_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoEvent_IsDefined_m8318_MethodInfo,
	&MonoEvent_GetCustomAttributes_m8319_MethodInfo,
	&MonoEvent_GetCustomAttributes_m8320_MethodInfo,
	&MonoEvent_get_Attributes_m8312_MethodInfo,
	&EventInfo_get_EventHandlerType_m8252_MethodInfo,
	&MonoEvent_GetAddMethod_m8313_MethodInfo,
	&MonoEvent_GetObjectData_m8321_MethodInfo,
};
static bool MonoEvent_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoEvent_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoEvent_t_InterfacesOffsets[] = 
{
	{ &_EventInfo_t2000_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 17},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEvent_t_1_0_0;
struct MonoEvent_t;
const Il2CppTypeDefinitionMetadata MonoEvent_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoEvent_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoEvent_t_InterfacesOffsets/* interfaceOffsets */
	, &EventInfo_t_0_0_0/* parent */
	, MonoEvent_t_VTable/* vtableMethods */
	, MonoEvent_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1283/* fieldStart */

};
TypeInfo MonoEvent_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEvent"/* name */
	, "System.Reflection"/* namespaze */
	, MonoEvent_t_MethodInfos/* methods */
	, MonoEvent_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoEvent_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEvent_t_0_0_0/* byval_arg */
	, &MonoEvent_t_1_0_0/* this_arg */
	, &MonoEvent_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEvent_t)/* instance_size */
	, sizeof (MonoEvent_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoField.h"
// Metadata Definition System.Reflection.MonoField
extern TypeInfo MonoField_t_il2cpp_TypeInfo;
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoFieldMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::.ctor()
extern const MethodInfo MonoField__ctor_m8322_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoField__ctor_m8322/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FieldAttributes_t1568 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldAttributes System.Reflection.MonoField::get_Attributes()
extern const MethodInfo MonoField_get_Attributes_m8323_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoField_get_Attributes_m8323/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldAttributes_t1568_0_0_0/* return_type */
	, RuntimeInvoker_FieldAttributes_t1568/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeFieldHandle_t1372 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeFieldHandle System.Reflection.MonoField::get_FieldHandle()
extern const MethodInfo MonoField_get_FieldHandle_m8324_MethodInfo = 
{
	"get_FieldHandle"/* name */
	, (methodPointerType)&MonoField_get_FieldHandle_m8324/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeFieldHandle_t1372_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeFieldHandle_t1372/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_FieldType()
extern const MethodInfo MonoField_get_FieldType_m8325_MethodInfo = 
{
	"get_FieldType"/* name */
	, (methodPointerType)&MonoField_get_FieldType_m8325/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetParentType_m8326_ParameterInfos[] = 
{
	{"declaring", 0, 134221691, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::GetParentType(System.Boolean)
extern const MethodInfo MonoField_GetParentType_m8326_MethodInfo = 
{
	"GetParentType"/* name */
	, (methodPointerType)&MonoField_GetParentType_m8326/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoField_t_MonoField_GetParentType_m8326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_ReflectedType()
extern const MethodInfo MonoField_get_ReflectedType_m8327_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoField_get_ReflectedType_m8327/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoField::get_DeclaringType()
extern const MethodInfo MonoField_get_DeclaringType_m8328_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoField_get_DeclaringType_m8328/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoField::get_Name()
extern const MethodInfo MonoField_get_Name_m8329_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoField_get_Name_m8329/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoField_t_MonoField_IsDefined_m8330_ParameterInfos[] = 
{
	{"attributeType", 0, 134221692, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221693, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoField::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoField_IsDefined_m8330_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoField_IsDefined_m8330/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoField_t_MonoField_IsDefined_m8330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetCustomAttributes_m8331_ParameterInfos[] = 
{
	{"inherit", 0, 134221694, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoField_GetCustomAttributes_m8331_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoField_GetCustomAttributes_m8331/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoField_t_MonoField_GetCustomAttributes_m8331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetCustomAttributes_m8332_ParameterInfos[] = 
{
	{"attributeType", 0, 134221695, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221696, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoField::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoField_GetCustomAttributes_m8332_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoField_GetCustomAttributes_m8332/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoField_t_MonoField_GetCustomAttributes_m8332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.MonoField::GetFieldOffset()
extern const MethodInfo MonoField_GetFieldOffset_m8333_MethodInfo = 
{
	"GetFieldOffset"/* name */
	, (methodPointerType)&MonoField_GetFieldOffset_m8333/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 4096/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetValueInternal_m8334_ParameterInfos[] = 
{
	{"obj", 0, 134221697, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoField::GetValueInternal(System.Object)
extern const MethodInfo MonoField_GetValueInternal_m8334_MethodInfo = 
{
	"GetValueInternal"/* name */
	, (methodPointerType)&MonoField_GetValueInternal_m8334/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_GetValueInternal_m8334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetValue_m8335_ParameterInfos[] = 
{
	{"obj", 0, 134221698, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoField::GetValue(System.Object)
extern const MethodInfo MonoField_GetValue_m8335_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoField_GetValue_m8335/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_GetValue_m8335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoField::ToString()
extern const MethodInfo MonoField_ToString_m8336_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoField_ToString_m8336/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoField_t_MonoField_SetValueInternal_m8337_ParameterInfos[] = 
{
	{"fi", 0, 134221699, 0, &FieldInfo_t_0_0_0},
	{"obj", 1, 134221700, 0, &Object_t_0_0_0},
	{"value", 2, 134221701, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::SetValueInternal(System.Reflection.FieldInfo,System.Object,System.Object)
extern const MethodInfo MonoField_SetValueInternal_m8337_MethodInfo = 
{
	"SetValueInternal"/* name */
	, (methodPointerType)&MonoField_SetValueInternal_m8337/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_SetValueInternal_m8337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoField_t_MonoField_SetValue_m8338_ParameterInfos[] = 
{
	{"obj", 0, 134221702, 0, &Object_t_0_0_0},
	{"val", 1, 134221703, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221704, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 3, 134221705, 0, &Binder_t781_0_0_0},
	{"culture", 4, 134221706, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern const MethodInfo MonoField_SetValue_m8338_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&MonoField_SetValue_m8338/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, MonoField_t_MonoField_SetValue_m8338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoField_t_MonoField_GetObjectData_m8339_ParameterInfos[] = 
{
	{"info", 0, 134221707, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221708, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoField_GetObjectData_m8339_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoField_GetObjectData_m8339/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoField_t_MonoField_GetObjectData_m8339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoField::CheckGeneric()
extern const MethodInfo MonoField_CheckGeneric_m8340_MethodInfo = 
{
	"CheckGeneric"/* name */
	, (methodPointerType)&MonoField_CheckGeneric_m8340/* method */
	, &MonoField_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoField_t_MethodInfos[] =
{
	&MonoField__ctor_m8322_MethodInfo,
	&MonoField_get_Attributes_m8323_MethodInfo,
	&MonoField_get_FieldHandle_m8324_MethodInfo,
	&MonoField_get_FieldType_m8325_MethodInfo,
	&MonoField_GetParentType_m8326_MethodInfo,
	&MonoField_get_ReflectedType_m8327_MethodInfo,
	&MonoField_get_DeclaringType_m8328_MethodInfo,
	&MonoField_get_Name_m8329_MethodInfo,
	&MonoField_IsDefined_m8330_MethodInfo,
	&MonoField_GetCustomAttributes_m8331_MethodInfo,
	&MonoField_GetCustomAttributes_m8332_MethodInfo,
	&MonoField_GetFieldOffset_m8333_MethodInfo,
	&MonoField_GetValueInternal_m8334_MethodInfo,
	&MonoField_GetValue_m8335_MethodInfo,
	&MonoField_ToString_m8336_MethodInfo,
	&MonoField_SetValueInternal_m8337_MethodInfo,
	&MonoField_SetValue_m8338_MethodInfo,
	&MonoField_GetObjectData_m8339_MethodInfo,
	&MonoField_CheckGeneric_m8340_MethodInfo,
	NULL
};
extern const MethodInfo MonoField_get_Attributes_m8323_MethodInfo;
static const PropertyInfo MonoField_t____Attributes_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoField_get_Attributes_m8323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_FieldHandle_m8324_MethodInfo;
static const PropertyInfo MonoField_t____FieldHandle_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "FieldHandle"/* name */
	, &MonoField_get_FieldHandle_m8324_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_FieldType_m8325_MethodInfo;
static const PropertyInfo MonoField_t____FieldType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "FieldType"/* name */
	, &MonoField_get_FieldType_m8325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_ReflectedType_m8327_MethodInfo;
static const PropertyInfo MonoField_t____ReflectedType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoField_get_ReflectedType_m8327_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_DeclaringType_m8328_MethodInfo;
static const PropertyInfo MonoField_t____DeclaringType_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoField_get_DeclaringType_m8328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoField_get_Name_m8329_MethodInfo;
static const PropertyInfo MonoField_t____Name_PropertyInfo = 
{
	&MonoField_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoField_get_Name_m8329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoField_t_PropertyInfos[] =
{
	&MonoField_t____Attributes_PropertyInfo,
	&MonoField_t____FieldHandle_PropertyInfo,
	&MonoField_t____FieldType_PropertyInfo,
	&MonoField_t____ReflectedType_PropertyInfo,
	&MonoField_t____DeclaringType_PropertyInfo,
	&MonoField_t____Name_PropertyInfo,
	NULL
};
extern const MethodInfo MonoField_ToString_m8336_MethodInfo;
extern const MethodInfo MonoField_GetCustomAttributes_m8332_MethodInfo;
extern const MethodInfo MonoField_IsDefined_m8330_MethodInfo;
extern const MethodInfo MonoField_GetCustomAttributes_m8331_MethodInfo;
extern const MethodInfo MonoField_GetValue_m8335_MethodInfo;
extern const MethodInfo MonoField_SetValue_m8338_MethodInfo;
extern const MethodInfo MonoField_GetFieldOffset_m8333_MethodInfo;
extern const MethodInfo MonoField_GetObjectData_m8339_MethodInfo;
static const Il2CppMethodReference MonoField_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoField_ToString_m8336_MethodInfo,
	&MonoField_GetCustomAttributes_m8332_MethodInfo,
	&MonoField_IsDefined_m8330_MethodInfo,
	&MonoField_get_DeclaringType_m8328_MethodInfo,
	&FieldInfo_get_MemberType_m8255_MethodInfo,
	&MonoField_get_Name_m8329_MethodInfo,
	&MonoField_get_ReflectedType_m8327_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoField_IsDefined_m8330_MethodInfo,
	&MonoField_GetCustomAttributes_m8331_MethodInfo,
	&MonoField_GetCustomAttributes_m8332_MethodInfo,
	&MonoField_get_Attributes_m8323_MethodInfo,
	&MonoField_get_FieldHandle_m8324_MethodInfo,
	&MonoField_get_FieldType_m8325_MethodInfo,
	&MonoField_GetValue_m8335_MethodInfo,
	&FieldInfo_get_IsLiteral_m8256_MethodInfo,
	&FieldInfo_get_IsStatic_m8257_MethodInfo,
	&FieldInfo_get_IsInitOnly_m8258_MethodInfo,
	&FieldInfo_get_IsPublic_m8259_MethodInfo,
	&FieldInfo_get_IsNotSerialized_m8260_MethodInfo,
	&MonoField_SetValue_m8338_MethodInfo,
	&FieldInfo_SetValue_m8261_MethodInfo,
	&MonoField_GetFieldOffset_m8333_MethodInfo,
	&FieldInfo_get_UMarshal_m8266_MethodInfo,
	&MonoField_GetObjectData_m8339_MethodInfo,
};
static bool MonoField_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoField_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoField_t_InterfacesOffsets[] = 
{
	{ &_FieldInfo_t2001_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoField_t_0_0_0;
extern const Il2CppType MonoField_t_1_0_0;
struct MonoField_t;
const Il2CppTypeDefinitionMetadata MonoField_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoField_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoField_t_InterfacesOffsets/* interfaceOffsets */
	, &FieldInfo_t_0_0_0/* parent */
	, MonoField_t_VTable/* vtableMethods */
	, MonoField_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1285/* fieldStart */

};
TypeInfo MonoField_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoField"/* name */
	, "System.Reflection"/* namespaze */
	, MonoField_t_MethodInfos/* methods */
	, MonoField_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoField_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoField_t_0_0_0/* byval_arg */
	, &MonoField_t_1_0_0/* this_arg */
	, &MonoField_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoField_t)/* instance_size */
	, sizeof (MonoField_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethod.h"
// Metadata Definition System.Reflection.MonoGenericMethod
extern TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoGenericMethod::.ctor()
extern const MethodInfo MonoGenericMethod__ctor_m8341_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoGenericMethod__ctor_m8341/* method */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoGenericMethod::get_ReflectedType()
extern const MethodInfo MonoGenericMethod_get_ReflectedType_m8342_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoGenericMethod_get_ReflectedType_m8342/* method */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoGenericMethod_t_MethodInfos[] =
{
	&MonoGenericMethod__ctor_m8341_MethodInfo,
	&MonoGenericMethod_get_ReflectedType_m8342_MethodInfo,
	NULL
};
extern const MethodInfo MonoGenericMethod_get_ReflectedType_m8342_MethodInfo;
static const PropertyInfo MonoGenericMethod_t____ReflectedType_PropertyInfo = 
{
	&MonoGenericMethod_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoGenericMethod_get_ReflectedType_m8342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoGenericMethod_t_PropertyInfos[] =
{
	&MonoGenericMethod_t____ReflectedType_PropertyInfo,
	NULL
};
extern const MethodInfo MonoMethod_ToString_m8373_MethodInfo;
extern const MethodInfo MonoMethod_GetCustomAttributes_m8369_MethodInfo;
extern const MethodInfo MonoMethod_IsDefined_m8367_MethodInfo;
extern const MethodInfo MonoMethod_get_DeclaringType_m8365_MethodInfo;
extern const MethodInfo MonoMethod_get_Name_m8366_MethodInfo;
extern const MethodInfo MonoMethod_GetCustomAttributes_m8368_MethodInfo;
extern const MethodInfo MonoMethod_GetParameters_m8358_MethodInfo;
extern const MethodInfo MonoMethod_Invoke_m8360_MethodInfo;
extern const MethodInfo MonoMethod_get_MethodHandle_m8361_MethodInfo;
extern const MethodInfo MonoMethod_get_Attributes_m8362_MethodInfo;
extern const MethodInfo MonoMethod_get_CallingConvention_m8363_MethodInfo;
extern const MethodInfo MonoMethod_GetGenericArguments_m8377_MethodInfo;
extern const MethodInfo MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo;
extern const MethodInfo MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo;
extern const MethodInfo MonoMethod_get_IsGenericMethod_m8379_MethodInfo;
extern const MethodInfo MonoMethod_GetBaseDefinition_m8356_MethodInfo;
extern const MethodInfo MonoMethod_get_ReturnType_m8357_MethodInfo;
extern const MethodInfo MonoMethod_MakeGenericMethod_m8375_MethodInfo;
extern const MethodInfo MonoMethod_GetObjectData_m8374_MethodInfo;
static const Il2CppMethodReference MonoGenericMethod_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoMethod_ToString_m8373_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8369_MethodInfo,
	&MonoMethod_IsDefined_m8367_MethodInfo,
	&MonoMethod_get_DeclaringType_m8365_MethodInfo,
	&MethodInfo_get_MemberType_m8288_MethodInfo,
	&MonoMethod_get_Name_m8366_MethodInfo,
	&MonoGenericMethod_get_ReflectedType_m8342_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoMethod_IsDefined_m8367_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8368_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8369_MethodInfo,
	&MonoMethod_GetParameters_m8358_MethodInfo,
	&MethodBase_Invoke_m8278_MethodInfo,
	&MonoMethod_Invoke_m8360_MethodInfo,
	&MonoMethod_get_MethodHandle_m8361_MethodInfo,
	&MonoMethod_get_Attributes_m8362_MethodInfo,
	&MonoMethod_get_CallingConvention_m8363_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MonoMethod_GetGenericArguments_m8377_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m8379_MethodInfo,
	&MonoMethod_GetBaseDefinition_m8356_MethodInfo,
	&MonoMethod_get_ReturnType_m8357_MethodInfo,
	&MonoMethod_MakeGenericMethod_m8375_MethodInfo,
	&MonoMethod_GetObjectData_m8374_MethodInfo,
};
static bool MonoGenericMethod_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoGenericMethod_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 30},
	{ &_MethodInfo_t2003_0_0_0, 27},
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericMethod_t_0_0_0;
extern const Il2CppType MonoGenericMethod_t_1_0_0;
extern const Il2CppType MonoMethod_t_0_0_0;
struct MonoGenericMethod_t;
const Il2CppTypeDefinitionMetadata MonoGenericMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MonoMethod_t_0_0_0/* parent */
	, MonoGenericMethod_t_VTable/* vtableMethods */
	, MonoGenericMethod_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoGenericMethod_t_MethodInfos/* methods */
	, MonoGenericMethod_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericMethod_t_0_0_0/* byval_arg */
	, &MonoGenericMethod_t_1_0_0/* this_arg */
	, &MonoGenericMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericMethod_t)/* instance_size */
	, sizeof (MonoGenericMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethod.h"
// Metadata Definition System.Reflection.MonoGenericCMethod
extern TypeInfo MonoGenericCMethod_t1577_il2cpp_TypeInfo;
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoGenericCMethod::.ctor()
extern const MethodInfo MonoGenericCMethod__ctor_m8343_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoGenericCMethod__ctor_m8343/* method */
	, &MonoGenericCMethod_t1577_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoGenericCMethod::get_ReflectedType()
extern const MethodInfo MonoGenericCMethod_get_ReflectedType_m8344_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoGenericCMethod_get_ReflectedType_m8344/* method */
	, &MonoGenericCMethod_t1577_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoGenericCMethod_t1577_MethodInfos[] =
{
	&MonoGenericCMethod__ctor_m8343_MethodInfo,
	&MonoGenericCMethod_get_ReflectedType_m8344_MethodInfo,
	NULL
};
extern const MethodInfo MonoGenericCMethod_get_ReflectedType_m8344_MethodInfo;
static const PropertyInfo MonoGenericCMethod_t1577____ReflectedType_PropertyInfo = 
{
	&MonoGenericCMethod_t1577_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoGenericCMethod_get_ReflectedType_m8344_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoGenericCMethod_t1577_PropertyInfos[] =
{
	&MonoGenericCMethod_t1577____ReflectedType_PropertyInfo,
	NULL
};
extern const MethodInfo MonoCMethod_ToString_m8395_MethodInfo;
extern const MethodInfo MonoCMethod_GetCustomAttributes_m8394_MethodInfo;
extern const MethodInfo MonoCMethod_IsDefined_m8392_MethodInfo;
extern const MethodInfo MonoCMethod_get_DeclaringType_m8390_MethodInfo;
extern const MethodInfo MonoCMethod_get_Name_m8391_MethodInfo;
extern const MethodInfo MonoCMethod_GetCustomAttributes_m8393_MethodInfo;
extern const MethodInfo MonoCMethod_GetParameters_m8382_MethodInfo;
extern const MethodInfo MonoCMethod_Invoke_m8384_MethodInfo;
extern const MethodInfo MonoCMethod_get_MethodHandle_m8386_MethodInfo;
extern const MethodInfo MonoCMethod_get_Attributes_m8387_MethodInfo;
extern const MethodInfo MonoCMethod_get_CallingConvention_m8388_MethodInfo;
extern const MethodInfo MonoCMethod_Invoke_m8385_MethodInfo;
extern const MethodInfo MonoCMethod_GetObjectData_m8396_MethodInfo;
static const Il2CppMethodReference MonoGenericCMethod_t1577_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoCMethod_ToString_m8395_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8394_MethodInfo,
	&MonoCMethod_IsDefined_m8392_MethodInfo,
	&MonoCMethod_get_DeclaringType_m8390_MethodInfo,
	&ConstructorInfo_get_MemberType_m8246_MethodInfo,
	&MonoCMethod_get_Name_m8391_MethodInfo,
	&MonoGenericCMethod_get_ReflectedType_m8344_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoCMethod_IsDefined_m8392_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8393_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8394_MethodInfo,
	&MonoCMethod_GetParameters_m8382_MethodInfo,
	&MethodBase_Invoke_m8278_MethodInfo,
	&MonoCMethod_Invoke_m8384_MethodInfo,
	&MonoCMethod_get_MethodHandle_m8386_MethodInfo,
	&MonoCMethod_get_Attributes_m8387_MethodInfo,
	&MonoCMethod_get_CallingConvention_m8388_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodBase_GetGenericArguments_m8283_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m8284_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo,
	&MethodBase_get_IsGenericMethod_m8286_MethodInfo,
	&MonoCMethod_Invoke_m8385_MethodInfo,
	&MonoCMethod_GetObjectData_m8396_MethodInfo,
};
static bool MonoGenericCMethod_t1577_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoGenericCMethod_t1577_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 28},
	{ &_ConstructorInfo_t1999_0_0_0, 27},
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericCMethod_t1577_0_0_0;
extern const Il2CppType MonoGenericCMethod_t1577_1_0_0;
extern const Il2CppType MonoCMethod_t1578_0_0_0;
struct MonoGenericCMethod_t1577;
const Il2CppTypeDefinitionMetadata MonoGenericCMethod_t1577_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericCMethod_t1577_InterfacesOffsets/* interfaceOffsets */
	, &MonoCMethod_t1578_0_0_0/* parent */
	, MonoGenericCMethod_t1577_VTable/* vtableMethods */
	, MonoGenericCMethod_t1577_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoGenericCMethod_t1577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoGenericCMethod_t1577_MethodInfos/* methods */
	, MonoGenericCMethod_t1577_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoGenericCMethod_t1577_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericCMethod_t1577_0_0_0/* byval_arg */
	, &MonoGenericCMethod_t1577_1_0_0/* this_arg */
	, &MonoGenericCMethod_t1577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericCMethod_t1577)/* instance_size */
	, sizeof (MonoGenericCMethod_t1577)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
// Metadata Definition System.Reflection.MonoMethodInfo
extern TypeInfo MonoMethodInfo_t1579_il2cpp_TypeInfo;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfoMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MonoMethodInfo_t1579_1_0_2;
extern const Il2CppType MonoMethodInfo_t1579_1_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_get_method_info_m8345_ParameterInfos[] = 
{
	{"handle", 0, 134221709, 0, &IntPtr_t_0_0_0},
	{"info", 1, 134221710, 0, &MonoMethodInfo_t1579_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t_MonoMethodInfoU26_t2374 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethodInfo::get_method_info(System.IntPtr,System.Reflection.MonoMethodInfo&)
extern const MethodInfo MonoMethodInfo_get_method_info_m8345_MethodInfo = 
{
	"get_method_info"/* name */
	, (methodPointerType)&MonoMethodInfo_get_method_info_m8345/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t_MonoMethodInfoU26_t2374/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_get_method_info_m8345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetMethodInfo_m8346_ParameterInfos[] = 
{
	{"handle", 0, 134221711, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType MonoMethodInfo_t1579_0_0_0;
extern void* RuntimeInvoker_MonoMethodInfo_t1579_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoMethodInfo System.Reflection.MonoMethodInfo::GetMethodInfo(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetMethodInfo_m8346_MethodInfo = 
{
	"GetMethodInfo"/* name */
	, (methodPointerType)&MonoMethodInfo_GetMethodInfo_m8346/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodInfo_t1579_0_0_0/* return_type */
	, RuntimeInvoker_MonoMethodInfo_t1579_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetMethodInfo_m8346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetDeclaringType_m8347_ParameterInfos[] = 
{
	{"handle", 0, 134221712, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethodInfo::GetDeclaringType(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetDeclaringType_m8347_MethodInfo = 
{
	"GetDeclaringType"/* name */
	, (methodPointerType)&MonoMethodInfo_GetDeclaringType_m8347/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetDeclaringType_m8347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetReturnType_m8348_ParameterInfos[] = 
{
	{"handle", 0, 134221713, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethodInfo::GetReturnType(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetReturnType_m8348_MethodInfo = 
{
	"GetReturnType"/* name */
	, (methodPointerType)&MonoMethodInfo_GetReturnType_m8348/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetReturnType_m8348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetAttributes_m8349_ParameterInfos[] = 
{
	{"handle", 0, 134221714, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_MethodAttributes_t1571_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoMethodInfo::GetAttributes(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetAttributes_m8349_MethodInfo = 
{
	"GetAttributes"/* name */
	, (methodPointerType)&MonoMethodInfo_GetAttributes_m8349/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1571_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1571_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetAttributes_m8349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetCallingConvention_m8350_ParameterInfos[] = 
{
	{"handle", 0, 134221715, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_CallingConventions_t1565_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoMethodInfo::GetCallingConvention(System.IntPtr)
extern const MethodInfo MonoMethodInfo_GetCallingConvention_m8350_MethodInfo = 
{
	"GetCallingConvention"/* name */
	, (methodPointerType)&MonoMethodInfo_GetCallingConvention_m8350/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1565_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1565_IntPtr_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetCallingConvention_m8350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_get_parameter_info_m8351_ParameterInfos[] = 
{
	{"handle", 0, 134221716, 0, &IntPtr_t_0_0_0},
	{"member", 1, 134221717, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::get_parameter_info(System.IntPtr,System.Reflection.MemberInfo)
extern const MethodInfo MonoMethodInfo_get_parameter_info_m8351_MethodInfo = 
{
	"get_parameter_info"/* name */
	, (methodPointerType)&MonoMethodInfo_get_parameter_info_m8351/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_Object_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_get_parameter_info_m8351_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo MonoMethodInfo_t1579_MonoMethodInfo_GetParametersInfo_m8352_ParameterInfos[] = 
{
	{"handle", 0, 134221718, 0, &IntPtr_t_0_0_0},
	{"member", 1, 134221719, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethodInfo::GetParametersInfo(System.IntPtr,System.Reflection.MemberInfo)
extern const MethodInfo MonoMethodInfo_GetParametersInfo_m8352_MethodInfo = 
{
	"GetParametersInfo"/* name */
	, (methodPointerType)&MonoMethodInfo_GetParametersInfo_m8352/* method */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t_Object_t/* invoker_method */
	, MonoMethodInfo_t1579_MonoMethodInfo_GetParametersInfo_m8352_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethodInfo_t1579_MethodInfos[] =
{
	&MonoMethodInfo_get_method_info_m8345_MethodInfo,
	&MonoMethodInfo_GetMethodInfo_m8346_MethodInfo,
	&MonoMethodInfo_GetDeclaringType_m8347_MethodInfo,
	&MonoMethodInfo_GetReturnType_m8348_MethodInfo,
	&MonoMethodInfo_GetAttributes_m8349_MethodInfo,
	&MonoMethodInfo_GetCallingConvention_m8350_MethodInfo,
	&MonoMethodInfo_get_parameter_info_m8351_MethodInfo,
	&MonoMethodInfo_GetParametersInfo_m8352_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoMethodInfo_t1579_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool MonoMethodInfo_t1579_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
const Il2CppTypeDefinitionMetadata MonoMethodInfo_t1579_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, MonoMethodInfo_t1579_VTable/* vtableMethods */
	, MonoMethodInfo_t1579_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1290/* fieldStart */

};
TypeInfo MonoMethodInfo_t1579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoMethodInfo_t1579_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoMethodInfo_t1579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodInfo_t1579_0_0_0/* byval_arg */
	, &MonoMethodInfo_t1579_1_0_0/* this_arg */
	, &MonoMethodInfo_t1579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodInfo_t1579)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoMethodInfo_t1579)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethod.h"
// Metadata Definition System.Reflection.MonoMethod
extern TypeInfo MonoMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethod::.ctor()
extern const MethodInfo MonoMethod__ctor_m8353_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoMethod__ctor_m8353/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t47_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_get_name_m8354_ParameterInfos[] = 
{
	{"method", 0, 134221720, 0, &MethodBase_t47_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::get_name(System.Reflection.MethodBase)
extern const MethodInfo MonoMethod_get_name_m8354_MethodInfo = 
{
	"get_name"/* name */
	, (methodPointerType)&MonoMethod_get_name_m8354/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_get_name_m8354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethod_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_get_base_definition_m8355_ParameterInfos[] = 
{
	{"method", 0, 134221721, 0, &MonoMethod_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoMethod System.Reflection.MonoMethod::get_base_definition(System.Reflection.MonoMethod)
extern const MethodInfo MonoMethod_get_base_definition_m8355_MethodInfo = 
{
	"get_base_definition"/* name */
	, (methodPointerType)&MonoMethod_get_base_definition_m8355/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethod_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_get_base_definition_m8355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::GetBaseDefinition()
extern const MethodInfo MonoMethod_GetBaseDefinition_m8356_MethodInfo = 
{
	"GetBaseDefinition"/* name */
	, (methodPointerType)&MonoMethod_GetBaseDefinition_m8356/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_ReturnType()
extern const MethodInfo MonoMethod_get_ReturnType_m8357_MethodInfo = 
{
	"get_ReturnType"/* name */
	, (methodPointerType)&MonoMethod_get_ReturnType_m8357/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoMethod::GetParameters()
extern const MethodInfo MonoMethod_GetParameters_m8358_MethodInfo = 
{
	"GetParameters"/* name */
	, (methodPointerType)&MonoMethod_GetParameters_m8358/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType Exception_t42_1_0_2;
extern const Il2CppType Exception_t42_1_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_InternalInvoke_m8359_ParameterInfos[] = 
{
	{"obj", 0, 134221722, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221723, 0, &ObjectU5BU5D_t29_0_0_0},
	{"exc", 2, 134221724, 0, &Exception_t42_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern const MethodInfo MonoMethod_InternalInvoke_m8359_MethodInfo = 
{
	"InternalInvoke"/* name */
	, (methodPointerType)&MonoMethod_InternalInvoke_m8359/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2024/* invoker_method */
	, MonoMethod_t_MonoMethod_InternalInvoke_m8359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_Invoke_m8360_ParameterInfos[] = 
{
	{"obj", 0, 134221725, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221726, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134221727, 0, &Binder_t781_0_0_0},
	{"parameters", 3, 134221728, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 4, 134221729, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoMethod_Invoke_m8360_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoMethod_Invoke_m8360/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_Invoke_m8360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MonoMethod::get_MethodHandle()
extern const MethodInfo MonoMethod_get_MethodHandle_m8361_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, (methodPointerType)&MonoMethod_get_MethodHandle_m8361/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t1871_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t1871/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t1571 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoMethod::get_Attributes()
extern const MethodInfo MonoMethod_get_Attributes_m8362_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoMethod_get_Attributes_m8362/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1571_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1571/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t1565 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoMethod::get_CallingConvention()
extern const MethodInfo MonoMethod_get_CallingConvention_m8363_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MonoMethod_get_CallingConvention_m8363/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1565_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1565/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_ReflectedType()
extern const MethodInfo MonoMethod_get_ReflectedType_m8364_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoMethod_get_ReflectedType_m8364/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoMethod::get_DeclaringType()
extern const MethodInfo MonoMethod_get_DeclaringType_m8365_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoMethod_get_DeclaringType_m8365/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::get_Name()
extern const MethodInfo MonoMethod_get_Name_m8366_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoMethod_get_Name_m8366/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_IsDefined_m8367_ParameterInfos[] = 
{
	{"attributeType", 0, 134221730, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221731, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoMethod_IsDefined_m8367_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoMethod_IsDefined_m8367/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoMethod_t_MonoMethod_IsDefined_m8367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetCustomAttributes_m8368_ParameterInfos[] = 
{
	{"inherit", 0, 134221732, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoMethod_GetCustomAttributes_m8368_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetCustomAttributes_m8368/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoMethod_t_MonoMethod_GetCustomAttributes_m8368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetCustomAttributes_m8369_ParameterInfos[] = 
{
	{"attributeType", 0, 134221733, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221734, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoMethod_GetCustomAttributes_m8369_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetCustomAttributes_m8369/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoMethod_t_MonoMethod_GetCustomAttributes_m8369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetDllImportAttribute_m8370_ParameterInfos[] = 
{
	{"mhandle", 0, 134221735, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType DllImportAttribute_t1374_0_0_0;
extern void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.InteropServices.DllImportAttribute System.Reflection.MonoMethod::GetDllImportAttribute(System.IntPtr)
extern const MethodInfo MonoMethod_GetDllImportAttribute_m8370_MethodInfo = 
{
	"GetDllImportAttribute"/* name */
	, (methodPointerType)&MonoMethod_GetDllImportAttribute_m8370/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &DllImportAttribute_t1374_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_IntPtr_t/* invoker_method */
	, MonoMethod_t_MonoMethod_GetDllImportAttribute_m8370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoMethod::GetPseudoCustomAttributes()
extern const MethodInfo MonoMethod_GetPseudoCustomAttributes_m8371_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&MonoMethod_GetPseudoCustomAttributes_m8371/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_ShouldPrintFullName_m8372_ParameterInfos[] = 
{
	{"type", 0, 134221736, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::ShouldPrintFullName(System.Type)
extern const MethodInfo MonoMethod_ShouldPrintFullName_m8372_MethodInfo = 
{
	"ShouldPrintFullName"/* name */
	, (methodPointerType)&MonoMethod_ShouldPrintFullName_m8372/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_ShouldPrintFullName_m8372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoMethod::ToString()
extern const MethodInfo MonoMethod_ToString_m8373_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoMethod_ToString_m8373/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_GetObjectData_m8374_ParameterInfos[] = 
{
	{"info", 0, 134221737, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221738, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoMethod_GetObjectData_m8374_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoMethod_GetObjectData_m8374/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoMethod_t_MonoMethod_GetObjectData_m8374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_MakeGenericMethod_m8375_ParameterInfos[] = 
{
	{"methodInstantiation", 0, 134221739, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod(System.Type[])
extern const MethodInfo MonoMethod_MakeGenericMethod_m8375_MethodInfo = 
{
	"MakeGenericMethod"/* name */
	, (methodPointerType)&MonoMethod_MakeGenericMethod_m8375/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_MakeGenericMethod_m8375_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo MonoMethod_t_MonoMethod_MakeGenericMethod_impl_m8376_ParameterInfos[] = 
{
	{"types", 0, 134221740, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoMethod::MakeGenericMethod_impl(System.Type[])
extern const MethodInfo MonoMethod_MakeGenericMethod_impl_m8376_MethodInfo = 
{
	"MakeGenericMethod_impl"/* name */
	, (methodPointerType)&MonoMethod_MakeGenericMethod_impl_m8376/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoMethod_t_MonoMethod_MakeGenericMethod_impl_m8376_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoMethod::GetGenericArguments()
extern const MethodInfo MonoMethod_GetGenericArguments_m8377_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MonoMethod_GetGenericArguments_m8377/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethodDefinition()
extern const MethodInfo MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo = 
{
	"get_IsGenericMethodDefinition"/* name */
	, (methodPointerType)&MonoMethod_get_IsGenericMethodDefinition_m8378/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_IsGenericMethod()
extern const MethodInfo MonoMethod_get_IsGenericMethod_m8379_MethodInfo = 
{
	"get_IsGenericMethod"/* name */
	, (methodPointerType)&MonoMethod_get_IsGenericMethod_m8379/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoMethod::get_ContainsGenericParameters()
extern const MethodInfo MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MonoMethod_get_ContainsGenericParameters_m8380/* method */
	, &MonoMethod_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethod_t_MethodInfos[] =
{
	&MonoMethod__ctor_m8353_MethodInfo,
	&MonoMethod_get_name_m8354_MethodInfo,
	&MonoMethod_get_base_definition_m8355_MethodInfo,
	&MonoMethod_GetBaseDefinition_m8356_MethodInfo,
	&MonoMethod_get_ReturnType_m8357_MethodInfo,
	&MonoMethod_GetParameters_m8358_MethodInfo,
	&MonoMethod_InternalInvoke_m8359_MethodInfo,
	&MonoMethod_Invoke_m8360_MethodInfo,
	&MonoMethod_get_MethodHandle_m8361_MethodInfo,
	&MonoMethod_get_Attributes_m8362_MethodInfo,
	&MonoMethod_get_CallingConvention_m8363_MethodInfo,
	&MonoMethod_get_ReflectedType_m8364_MethodInfo,
	&MonoMethod_get_DeclaringType_m8365_MethodInfo,
	&MonoMethod_get_Name_m8366_MethodInfo,
	&MonoMethod_IsDefined_m8367_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8368_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8369_MethodInfo,
	&MonoMethod_GetDllImportAttribute_m8370_MethodInfo,
	&MonoMethod_GetPseudoCustomAttributes_m8371_MethodInfo,
	&MonoMethod_ShouldPrintFullName_m8372_MethodInfo,
	&MonoMethod_ToString_m8373_MethodInfo,
	&MonoMethod_GetObjectData_m8374_MethodInfo,
	&MonoMethod_MakeGenericMethod_m8375_MethodInfo,
	&MonoMethod_MakeGenericMethod_impl_m8376_MethodInfo,
	&MonoMethod_GetGenericArguments_m8377_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m8379_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo,
	NULL
};
static const PropertyInfo MonoMethod_t____ReturnType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ReturnType"/* name */
	, &MonoMethod_get_ReturnType_m8357_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____MethodHandle_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MonoMethod_get_MethodHandle_m8361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____Attributes_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoMethod_get_Attributes_m8362_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____CallingConvention_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MonoMethod_get_CallingConvention_m8363_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethod_get_ReflectedType_m8364_MethodInfo;
static const PropertyInfo MonoMethod_t____ReflectedType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoMethod_get_ReflectedType_m8364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____DeclaringType_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoMethod_get_DeclaringType_m8365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____Name_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoMethod_get_Name_m8366_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____IsGenericMethodDefinition_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethodDefinition"/* name */
	, &MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____IsGenericMethod_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "IsGenericMethod"/* name */
	, &MonoMethod_get_IsGenericMethod_m8379_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoMethod_t____ContainsGenericParameters_PropertyInfo = 
{
	&MonoMethod_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoMethod_t_PropertyInfos[] =
{
	&MonoMethod_t____ReturnType_PropertyInfo,
	&MonoMethod_t____MethodHandle_PropertyInfo,
	&MonoMethod_t____Attributes_PropertyInfo,
	&MonoMethod_t____CallingConvention_PropertyInfo,
	&MonoMethod_t____ReflectedType_PropertyInfo,
	&MonoMethod_t____DeclaringType_PropertyInfo,
	&MonoMethod_t____Name_PropertyInfo,
	&MonoMethod_t____IsGenericMethodDefinition_PropertyInfo,
	&MonoMethod_t____IsGenericMethod_PropertyInfo,
	&MonoMethod_t____ContainsGenericParameters_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoMethod_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoMethod_ToString_m8373_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8369_MethodInfo,
	&MonoMethod_IsDefined_m8367_MethodInfo,
	&MonoMethod_get_DeclaringType_m8365_MethodInfo,
	&MethodInfo_get_MemberType_m8288_MethodInfo,
	&MonoMethod_get_Name_m8366_MethodInfo,
	&MonoMethod_get_ReflectedType_m8364_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoMethod_IsDefined_m8367_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8368_MethodInfo,
	&MonoMethod_GetCustomAttributes_m8369_MethodInfo,
	&MonoMethod_GetParameters_m8358_MethodInfo,
	&MethodBase_Invoke_m8278_MethodInfo,
	&MonoMethod_Invoke_m8360_MethodInfo,
	&MonoMethod_get_MethodHandle_m8361_MethodInfo,
	&MonoMethod_get_Attributes_m8362_MethodInfo,
	&MonoMethod_get_CallingConvention_m8363_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MonoMethod_GetGenericArguments_m8377_MethodInfo,
	&MonoMethod_get_ContainsGenericParameters_m8380_MethodInfo,
	&MonoMethod_get_IsGenericMethodDefinition_m8378_MethodInfo,
	&MonoMethod_get_IsGenericMethod_m8379_MethodInfo,
	&MonoMethod_GetBaseDefinition_m8356_MethodInfo,
	&MonoMethod_get_ReturnType_m8357_MethodInfo,
	&MonoMethod_MakeGenericMethod_m8375_MethodInfo,
	&MonoMethod_GetObjectData_m8374_MethodInfo,
};
static bool MonoMethod_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethod_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethod_t_InterfacesOffsets[] = 
{
	{ &_MethodInfo_t2003_0_0_0, 27},
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 30},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethod_t_1_0_0;
struct MonoMethod_t;
const Il2CppTypeDefinitionMetadata MonoMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethod_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodInfo_t_0_0_0/* parent */
	, MonoMethod_t_VTable/* vtableMethods */
	, MonoMethod_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1295/* fieldStart */

};
TypeInfo MonoMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoMethod_t_MethodInfos/* methods */
	, MonoMethod_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethod_t_0_0_0/* byval_arg */
	, &MonoMethod_t_1_0_0/* this_arg */
	, &MonoMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethod_t)/* instance_size */
	, sizeof (MonoMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 10/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethod.h"
// Metadata Definition System.Reflection.MonoCMethod
extern TypeInfo MonoCMethod_t1578_il2cpp_TypeInfo;
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethodMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoCMethod::.ctor()
extern const MethodInfo MonoCMethod__ctor_m8381_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoCMethod__ctor_m8381/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoCMethod::GetParameters()
extern const MethodInfo MonoCMethod_GetParameters_m8382_MethodInfo = 
{
	"GetParameters"/* name */
	, (methodPointerType)&MonoCMethod_GetParameters_m8382/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType Exception_t42_1_0_2;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_InternalInvoke_m8383_ParameterInfos[] = 
{
	{"obj", 0, 134221741, 0, &Object_t_0_0_0},
	{"parameters", 1, 134221742, 0, &ObjectU5BU5D_t29_0_0_0},
	{"exc", 2, 134221743, 0, &Exception_t42_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2024 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)
extern const MethodInfo MonoCMethod_InternalInvoke_m8383_MethodInfo = 
{
	"InternalInvoke"/* name */
	, (methodPointerType)&MonoCMethod_InternalInvoke_m8383/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2024/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_InternalInvoke_m8383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_Invoke_m8384_ParameterInfos[] = 
{
	{"obj", 0, 134221744, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221745, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134221746, 0, &Binder_t781_0_0_0},
	{"parameters", 3, 134221747, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 4, 134221748, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoCMethod_Invoke_m8384_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoCMethod_Invoke_m8384/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_Invoke_m8384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_Invoke_m8385_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221749, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 1, 134221750, 0, &Binder_t781_0_0_0},
	{"parameters", 2, 134221751, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 3, 134221752, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoCMethod::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoCMethod_Invoke_m8385_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MonoCMethod_Invoke_m8385/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_Invoke_m8385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_RuntimeMethodHandle_t1871 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeMethodHandle System.Reflection.MonoCMethod::get_MethodHandle()
extern const MethodInfo MonoCMethod_get_MethodHandle_m8386_MethodInfo = 
{
	"get_MethodHandle"/* name */
	, (methodPointerType)&MonoCMethod_get_MethodHandle_m8386/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeMethodHandle_t1871_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeMethodHandle_t1871/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MethodAttributes_t1571 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodAttributes System.Reflection.MonoCMethod::get_Attributes()
extern const MethodInfo MonoCMethod_get_Attributes_m8387_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoCMethod_get_Attributes_m8387/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &MethodAttributes_t1571_0_0_0/* return_type */
	, RuntimeInvoker_MethodAttributes_t1571/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_CallingConventions_t1565 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.CallingConventions System.Reflection.MonoCMethod::get_CallingConvention()
extern const MethodInfo MonoCMethod_get_CallingConvention_m8388_MethodInfo = 
{
	"get_CallingConvention"/* name */
	, (methodPointerType)&MonoCMethod_get_CallingConvention_m8388/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &CallingConventions_t1565_0_0_0/* return_type */
	, RuntimeInvoker_CallingConventions_t1565/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoCMethod::get_ReflectedType()
extern const MethodInfo MonoCMethod_get_ReflectedType_m8389_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoCMethod_get_ReflectedType_m8389/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoCMethod::get_DeclaringType()
extern const MethodInfo MonoCMethod_get_DeclaringType_m8390_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoCMethod_get_DeclaringType_m8390/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoCMethod::get_Name()
extern const MethodInfo MonoCMethod_get_Name_m8391_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoCMethod_get_Name_m8391/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_IsDefined_m8392_ParameterInfos[] = 
{
	{"attributeType", 0, 134221753, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221754, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoCMethod::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoCMethod_IsDefined_m8392_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoCMethod_IsDefined_m8392/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_IsDefined_m8392_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_GetCustomAttributes_m8393_ParameterInfos[] = 
{
	{"inherit", 0, 134221755, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoCMethod_GetCustomAttributes_m8393_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCMethod_GetCustomAttributes_m8393/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_GetCustomAttributes_m8393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_GetCustomAttributes_m8394_ParameterInfos[] = 
{
	{"attributeType", 0, 134221756, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221757, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoCMethod::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoCMethod_GetCustomAttributes_m8394_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCMethod_GetCustomAttributes_m8394/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_GetCustomAttributes_m8394_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoCMethod::ToString()
extern const MethodInfo MonoCMethod_ToString_m8395_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoCMethod_ToString_m8395/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoCMethod_t1578_MonoCMethod_GetObjectData_m8396_ParameterInfos[] = 
{
	{"info", 0, 134221758, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221759, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoCMethod::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoCMethod_GetObjectData_m8396_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoCMethod_GetObjectData_m8396/* method */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoCMethod_t1578_MonoCMethod_GetObjectData_m8396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoCMethod_t1578_MethodInfos[] =
{
	&MonoCMethod__ctor_m8381_MethodInfo,
	&MonoCMethod_GetParameters_m8382_MethodInfo,
	&MonoCMethod_InternalInvoke_m8383_MethodInfo,
	&MonoCMethod_Invoke_m8384_MethodInfo,
	&MonoCMethod_Invoke_m8385_MethodInfo,
	&MonoCMethod_get_MethodHandle_m8386_MethodInfo,
	&MonoCMethod_get_Attributes_m8387_MethodInfo,
	&MonoCMethod_get_CallingConvention_m8388_MethodInfo,
	&MonoCMethod_get_ReflectedType_m8389_MethodInfo,
	&MonoCMethod_get_DeclaringType_m8390_MethodInfo,
	&MonoCMethod_get_Name_m8391_MethodInfo,
	&MonoCMethod_IsDefined_m8392_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8393_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8394_MethodInfo,
	&MonoCMethod_ToString_m8395_MethodInfo,
	&MonoCMethod_GetObjectData_m8396_MethodInfo,
	NULL
};
static const PropertyInfo MonoCMethod_t1578____MethodHandle_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "MethodHandle"/* name */
	, &MonoCMethod_get_MethodHandle_m8386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t1578____Attributes_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoCMethod_get_Attributes_m8387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t1578____CallingConvention_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "CallingConvention"/* name */
	, &MonoCMethod_get_CallingConvention_m8388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoCMethod_get_ReflectedType_m8389_MethodInfo;
static const PropertyInfo MonoCMethod_t1578____ReflectedType_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoCMethod_get_ReflectedType_m8389_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t1578____DeclaringType_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoCMethod_get_DeclaringType_m8390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MonoCMethod_t1578____Name_PropertyInfo = 
{
	&MonoCMethod_t1578_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoCMethod_get_Name_m8391_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoCMethod_t1578_PropertyInfos[] =
{
	&MonoCMethod_t1578____MethodHandle_PropertyInfo,
	&MonoCMethod_t1578____Attributes_PropertyInfo,
	&MonoCMethod_t1578____CallingConvention_PropertyInfo,
	&MonoCMethod_t1578____ReflectedType_PropertyInfo,
	&MonoCMethod_t1578____DeclaringType_PropertyInfo,
	&MonoCMethod_t1578____Name_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoCMethod_t1578_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoCMethod_ToString_m8395_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8394_MethodInfo,
	&MonoCMethod_IsDefined_m8392_MethodInfo,
	&MonoCMethod_get_DeclaringType_m8390_MethodInfo,
	&ConstructorInfo_get_MemberType_m8246_MethodInfo,
	&MonoCMethod_get_Name_m8391_MethodInfo,
	&MonoCMethod_get_ReflectedType_m8389_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoCMethod_IsDefined_m8392_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8393_MethodInfo,
	&MonoCMethod_GetCustomAttributes_m8394_MethodInfo,
	&MonoCMethod_GetParameters_m8382_MethodInfo,
	&MethodBase_Invoke_m8278_MethodInfo,
	&MonoCMethod_Invoke_m8384_MethodInfo,
	&MonoCMethod_get_MethodHandle_m8386_MethodInfo,
	&MonoCMethod_get_Attributes_m8387_MethodInfo,
	&MonoCMethod_get_CallingConvention_m8388_MethodInfo,
	&MethodBase_get_IsPublic_m8280_MethodInfo,
	&MethodBase_get_IsStatic_m8281_MethodInfo,
	&MethodBase_get_IsVirtual_m8282_MethodInfo,
	&MethodBase_GetGenericArguments_m8283_MethodInfo,
	&MethodBase_get_ContainsGenericParameters_m8284_MethodInfo,
	&MethodBase_get_IsGenericMethodDefinition_m8285_MethodInfo,
	&MethodBase_get_IsGenericMethod_m8286_MethodInfo,
	&MonoCMethod_Invoke_m8385_MethodInfo,
	&MonoCMethod_GetObjectData_m8396_MethodInfo,
};
static bool MonoCMethod_t1578_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoCMethod_t1578_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoCMethod_t1578_InterfacesOffsets[] = 
{
	{ &_ConstructorInfo_t1999_0_0_0, 27},
	{ &_MethodBase_t2002_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCMethod_t1578_1_0_0;
struct MonoCMethod_t1578;
const Il2CppTypeDefinitionMetadata MonoCMethod_t1578_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoCMethod_t1578_InterfacesTypeInfos/* implementedInterfaces */
	, MonoCMethod_t1578_InterfacesOffsets/* interfaceOffsets */
	, &ConstructorInfo_t632_0_0_0/* parent */
	, MonoCMethod_t1578_VTable/* vtableMethods */
	, MonoCMethod_t1578_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1298/* fieldStart */

};
TypeInfo MonoCMethod_t1578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, MonoCMethod_t1578_MethodInfos/* methods */
	, MonoCMethod_t1578_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoCMethod_t1578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCMethod_t1578_0_0_0/* byval_arg */
	, &MonoCMethod_t1578_1_0_0/* this_arg */
	, &MonoCMethod_t1578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCMethod_t1578)/* instance_size */
	, sizeof (MonoCMethod_t1578)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 6/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
// Metadata Definition System.Reflection.MonoPropertyInfo
extern TypeInfo MonoPropertyInfo_t1580_il2cpp_TypeInfo;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfoMethodDeclarations.h"
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType MonoPropertyInfo_t1580_1_0_0;
extern const Il2CppType MonoPropertyInfo_t1580_1_0_0;
extern const Il2CppType PInfo_t1581_0_0_0;
extern const Il2CppType PInfo_t1581_0_0_0;
static const ParameterInfo MonoPropertyInfo_t1580_MonoPropertyInfo_get_property_info_m8397_ParameterInfos[] = 
{
	{"prop", 0, 134221760, 0, &MonoProperty_t_0_0_0},
	{"info", 1, 134221761, 0, &MonoPropertyInfo_t1580_1_0_0},
	{"req_info", 2, 134221762, 0, &PInfo_t1581_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_MonoPropertyInfoU26_t2375_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoPropertyInfo::get_property_info(System.Reflection.MonoProperty,System.Reflection.MonoPropertyInfo&,System.Reflection.PInfo)
extern const MethodInfo MonoPropertyInfo_get_property_info_m8397_MethodInfo = 
{
	"get_property_info"/* name */
	, (methodPointerType)&MonoPropertyInfo_get_property_info_m8397/* method */
	, &MonoPropertyInfo_t1580_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_MonoPropertyInfoU26_t2375_Int32_t54/* invoker_method */
	, MonoPropertyInfo_t1580_MonoPropertyInfo_get_property_info_m8397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoPropertyInfo_t1580_MonoPropertyInfo_GetTypeModifiers_m8398_ParameterInfos[] = 
{
	{"prop", 0, 134221763, 0, &MonoProperty_t_0_0_0},
	{"optional", 1, 134221764, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoPropertyInfo::GetTypeModifiers(System.Reflection.MonoProperty,System.Boolean)
extern const MethodInfo MonoPropertyInfo_GetTypeModifiers_m8398_MethodInfo = 
{
	"GetTypeModifiers"/* name */
	, (methodPointerType)&MonoPropertyInfo_GetTypeModifiers_m8398/* method */
	, &MonoPropertyInfo_t1580_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoPropertyInfo_t1580_MonoPropertyInfo_GetTypeModifiers_m8398_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoPropertyInfo_t1580_MethodInfos[] =
{
	&MonoPropertyInfo_get_property_info_m8397_MethodInfo,
	&MonoPropertyInfo_GetTypeModifiers_m8398_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoPropertyInfo_t1580_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool MonoPropertyInfo_t1580_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoPropertyInfo_t1580_0_0_0;
const Il2CppTypeDefinitionMetadata MonoPropertyInfo_t1580_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, MonoPropertyInfo_t1580_VTable/* vtableMethods */
	, MonoPropertyInfo_t1580_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1301/* fieldStart */

};
TypeInfo MonoPropertyInfo_t1580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoPropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, MonoPropertyInfo_t1580_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoPropertyInfo_t1580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoPropertyInfo_t1580_0_0_0/* byval_arg */
	, &MonoPropertyInfo_t1580_1_0_0/* this_arg */
	, &MonoPropertyInfo_t1580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoPropertyInfo_t1580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoPropertyInfo_t1580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfo.h"
// Metadata Definition System.Reflection.PInfo
extern TypeInfo PInfo_t1581_il2cpp_TypeInfo;
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfoMethodDeclarations.h"
static const MethodInfo* PInfo_t1581_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PInfo_t1581_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool PInfo_t1581_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PInfo_t1581_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PInfo_t1581_1_0_0;
const Il2CppTypeDefinitionMetadata PInfo_t1581_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PInfo_t1581_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, PInfo_t1581_VTable/* vtableMethods */
	, PInfo_t1581_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1306/* fieldStart */

};
TypeInfo PInfo_t1581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PInfo"/* name */
	, "System.Reflection"/* namespaze */
	, PInfo_t1581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 425/* custom_attributes_cache */
	, &PInfo_t1581_0_0_0/* byval_arg */
	, &PInfo_t1581_1_0_0/* this_arg */
	, &PInfo_t1581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PInfo_t1581)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PInfo_t1581)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapter.h"
// Metadata Definition System.Reflection.MonoProperty/GetterAdapter
extern TypeInfo GetterAdapter_t1582_il2cpp_TypeInfo;
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo GetterAdapter_t1582_GetterAdapter__ctor_m8399_ParameterInfos[] = 
{
	{"object", 0, 134221794, 0, &Object_t_0_0_0},
	{"method", 1, 134221795, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty/GetterAdapter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo GetterAdapter__ctor_m8399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GetterAdapter__ctor_m8399/* method */
	, &GetterAdapter_t1582_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, GetterAdapter_t1582_GetterAdapter__ctor_m8399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetterAdapter_t1582_GetterAdapter_Invoke_m8400_ParameterInfos[] = 
{
	{"_this", 0, 134221796, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty/GetterAdapter::Invoke(System.Object)
extern const MethodInfo GetterAdapter_Invoke_m8400_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&GetterAdapter_Invoke_m8400/* method */
	, &GetterAdapter_t1582_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t1582_GetterAdapter_Invoke_m8400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetterAdapter_t1582_GetterAdapter_BeginInvoke_m8401_ParameterInfos[] = 
{
	{"_this", 0, 134221797, 0, &Object_t_0_0_0},
	{"callback", 1, 134221798, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134221799, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MonoProperty/GetterAdapter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo GetterAdapter_BeginInvoke_m8401_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&GetterAdapter_BeginInvoke_m8401/* method */
	, &GetterAdapter_t1582_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t1582_GetterAdapter_BeginInvoke_m8401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo GetterAdapter_t1582_GetterAdapter_EndInvoke_m8402_ParameterInfos[] = 
{
	{"result", 0, 134221800, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty/GetterAdapter::EndInvoke(System.IAsyncResult)
extern const MethodInfo GetterAdapter_EndInvoke_m8402_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&GetterAdapter_EndInvoke_m8402/* method */
	, &GetterAdapter_t1582_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetterAdapter_t1582_GetterAdapter_EndInvoke_m8402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GetterAdapter_t1582_MethodInfos[] =
{
	&GetterAdapter__ctor_m8399_MethodInfo,
	&GetterAdapter_Invoke_m8400_MethodInfo,
	&GetterAdapter_BeginInvoke_m8401_MethodInfo,
	&GetterAdapter_EndInvoke_m8402_MethodInfo,
	NULL
};
extern const MethodInfo GetterAdapter_Invoke_m8400_MethodInfo;
extern const MethodInfo GetterAdapter_BeginInvoke_m8401_MethodInfo;
extern const MethodInfo GetterAdapter_EndInvoke_m8402_MethodInfo;
static const Il2CppMethodReference GetterAdapter_t1582_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&GetterAdapter_Invoke_m8400_MethodInfo,
	&GetterAdapter_BeginInvoke_m8401_MethodInfo,
	&GetterAdapter_EndInvoke_m8402_MethodInfo,
};
static bool GetterAdapter_t1582_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair GetterAdapter_t1582_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GetterAdapter_t1582_0_0_0;
extern const Il2CppType GetterAdapter_t1582_1_0_0;
extern TypeInfo MonoProperty_t_il2cpp_TypeInfo;
struct GetterAdapter_t1582;
const Il2CppTypeDefinitionMetadata GetterAdapter_t1582_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetterAdapter_t1582_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, GetterAdapter_t1582_VTable/* vtableMethods */
	, GetterAdapter_t1582_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GetterAdapter_t1582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetterAdapter"/* name */
	, ""/* namespaze */
	, GetterAdapter_t1582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GetterAdapter_t1582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetterAdapter_t1582_0_0_0/* byval_arg */
	, &GetterAdapter_t1582_1_0_0/* this_arg */
	, &GetterAdapter_t1582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetterAdapter_t1582/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetterAdapter_t1582)/* instance_size */
	, sizeof (GetterAdapter_t1582)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/Getter`2
extern TypeInfo Getter_2_t1930_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Getter_2_t1930_Il2CppGenericContainer;
extern TypeInfo Getter_2_t1930_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Getter_2_t1930_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Getter_2_t1930_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Getter_2_t1930_gp_R_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Getter_2_t1930_gp_R_1_il2cpp_TypeInfo_GenericParamFull = { &Getter_2_t1930_Il2CppGenericContainer, NULL, "R", 1, 0 };
static const Il2CppGenericParameter* Getter_2_t1930_Il2CppGenericParametersArray[2] = 
{
	&Getter_2_t1930_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Getter_2_t1930_gp_R_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Getter_2_t1930_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Getter_2_t1930_il2cpp_TypeInfo, 2, 0, Getter_2_t1930_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Getter_2_t1930_Getter_2__ctor_m10936_ParameterInfos[] = 
{
	{"object", 0, 134221801, 0, &Object_t_0_0_0},
	{"method", 1, 134221802, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Reflection.MonoProperty/Getter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Getter_2__ctor_m10936_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Getter_2_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t1930_Getter_2__ctor_m10936_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t1930_gp_0_0_0_0;
extern const Il2CppType Getter_2_t1930_gp_0_0_0_0;
static const ParameterInfo Getter_2_t1930_Getter_2_Invoke_m10937_ParameterInfos[] = 
{
	{"_this", 0, 134221803, 0, &Getter_2_t1930_gp_0_0_0_0},
};
extern const Il2CppType Getter_2_t1930_gp_1_0_0_0;
// R System.Reflection.MonoProperty/Getter`2::Invoke(T)
extern const MethodInfo Getter_2_Invoke_m10937_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Getter_2_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Getter_2_t1930_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t1930_Getter_2_Invoke_m10937_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t1930_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Getter_2_t1930_Getter_2_BeginInvoke_m10938_ParameterInfos[] = 
{
	{"_this", 0, 134221804, 0, &Getter_2_t1930_gp_0_0_0_0},
	{"callback", 1, 134221805, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134221806, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Getter_2_BeginInvoke_m10938_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Getter_2_t1930_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t1930_Getter_2_BeginInvoke_m10938_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo Getter_2_t1930_Getter_2_EndInvoke_m10939_ParameterInfos[] = 
{
	{"result", 0, 134221807, 0, &IAsyncResult_t213_0_0_0},
};
// R System.Reflection.MonoProperty/Getter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Getter_2_EndInvoke_m10939_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Getter_2_t1930_il2cpp_TypeInfo/* declaring_type */
	, &Getter_2_t1930_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Getter_2_t1930_Getter_2_EndInvoke_m10939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Getter_2_t1930_MethodInfos[] =
{
	&Getter_2__ctor_m10936_MethodInfo,
	&Getter_2_Invoke_m10937_MethodInfo,
	&Getter_2_BeginInvoke_m10938_MethodInfo,
	&Getter_2_EndInvoke_m10939_MethodInfo,
	NULL
};
extern const MethodInfo Getter_2_Invoke_m10937_MethodInfo;
extern const MethodInfo Getter_2_BeginInvoke_m10938_MethodInfo;
extern const MethodInfo Getter_2_EndInvoke_m10939_MethodInfo;
static const Il2CppMethodReference Getter_2_t1930_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&Getter_2_Invoke_m10937_MethodInfo,
	&Getter_2_BeginInvoke_m10938_MethodInfo,
	&Getter_2_EndInvoke_m10939_MethodInfo,
};
static bool Getter_2_t1930_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Getter_2_t1930_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Getter_2_t1930_0_0_0;
extern const Il2CppType Getter_2_t1930_1_0_0;
struct Getter_2_t1930;
const Il2CppTypeDefinitionMetadata Getter_2_t1930_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Getter_2_t1930_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, Getter_2_t1930_VTable/* vtableMethods */
	, Getter_2_t1930_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Getter_2_t1930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Getter`2"/* name */
	, ""/* namespaze */
	, Getter_2_t1930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Getter_2_t1930_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Getter_2_t1930_0_0_0/* byval_arg */
	, &Getter_2_t1930_1_0_0/* this_arg */
	, &Getter_2_t1930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Getter_2_t1930_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/StaticGetter`1
extern TypeInfo StaticGetter_1_t1929_il2cpp_TypeInfo;
extern const Il2CppGenericContainer StaticGetter_1_t1929_Il2CppGenericContainer;
extern TypeInfo StaticGetter_1_t1929_gp_R_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter StaticGetter_1_t1929_gp_R_0_il2cpp_TypeInfo_GenericParamFull = { &StaticGetter_1_t1929_Il2CppGenericContainer, NULL, "R", 0, 0 };
static const Il2CppGenericParameter* StaticGetter_1_t1929_Il2CppGenericParametersArray[1] = 
{
	&StaticGetter_1_t1929_gp_R_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer StaticGetter_1_t1929_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&StaticGetter_1_t1929_il2cpp_TypeInfo, 1, 0, StaticGetter_1_t1929_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo StaticGetter_1_t1929_StaticGetter_1__ctor_m10940_ParameterInfos[] = 
{
	{"object", 0, 134221808, 0, &Object_t_0_0_0},
	{"method", 1, 134221809, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Reflection.MonoProperty/StaticGetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo StaticGetter_1__ctor_m10940_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &StaticGetter_1_t1929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t1929_StaticGetter_1__ctor_m10940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StaticGetter_1_t1929_gp_0_0_0_0;
// R System.Reflection.MonoProperty/StaticGetter`1::Invoke()
extern const MethodInfo StaticGetter_1_Invoke_m10941_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t1929_il2cpp_TypeInfo/* declaring_type */
	, &StaticGetter_1_t1929_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StaticGetter_1_t1929_StaticGetter_1_BeginInvoke_m10942_ParameterInfos[] = 
{
	{"callback", 0, 134221810, 0, &AsyncCallback_t214_0_0_0},
	{"object", 1, 134221811, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo StaticGetter_1_BeginInvoke_m10942_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t1929_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t1929_StaticGetter_1_BeginInvoke_m10942_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo StaticGetter_1_t1929_StaticGetter_1_EndInvoke_m10943_ParameterInfos[] = 
{
	{"result", 0, 134221812, 0, &IAsyncResult_t213_0_0_0},
};
// R System.Reflection.MonoProperty/StaticGetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo StaticGetter_1_EndInvoke_m10943_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &StaticGetter_1_t1929_il2cpp_TypeInfo/* declaring_type */
	, &StaticGetter_1_t1929_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, StaticGetter_1_t1929_StaticGetter_1_EndInvoke_m10943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StaticGetter_1_t1929_MethodInfos[] =
{
	&StaticGetter_1__ctor_m10940_MethodInfo,
	&StaticGetter_1_Invoke_m10941_MethodInfo,
	&StaticGetter_1_BeginInvoke_m10942_MethodInfo,
	&StaticGetter_1_EndInvoke_m10943_MethodInfo,
	NULL
};
extern const MethodInfo StaticGetter_1_Invoke_m10941_MethodInfo;
extern const MethodInfo StaticGetter_1_BeginInvoke_m10942_MethodInfo;
extern const MethodInfo StaticGetter_1_EndInvoke_m10943_MethodInfo;
static const Il2CppMethodReference StaticGetter_1_t1929_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&StaticGetter_1_Invoke_m10941_MethodInfo,
	&StaticGetter_1_BeginInvoke_m10942_MethodInfo,
	&StaticGetter_1_EndInvoke_m10943_MethodInfo,
};
static bool StaticGetter_1_t1929_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StaticGetter_1_t1929_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StaticGetter_1_t1929_0_0_0;
extern const Il2CppType StaticGetter_1_t1929_1_0_0;
struct StaticGetter_1_t1929;
const Il2CppTypeDefinitionMetadata StaticGetter_1_t1929_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StaticGetter_1_t1929_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, StaticGetter_1_t1929_VTable/* vtableMethods */
	, StaticGetter_1_t1929_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StaticGetter_1_t1929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StaticGetter`1"/* name */
	, ""/* namespaze */
	, StaticGetter_1_t1929_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StaticGetter_1_t1929_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StaticGetter_1_t1929_0_0_0/* byval_arg */
	, &StaticGetter_1_t1929_1_0_0/* this_arg */
	, &StaticGetter_1_t1929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &StaticGetter_1_t1929_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// Metadata Definition System.Reflection.MonoProperty
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoPropertyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::.ctor()
extern const MethodInfo MonoProperty__ctor_m8403_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoProperty__ctor_m8403/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PInfo_t1581_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_CachePropertyInfo_m8404_ParameterInfos[] = 
{
	{"flags", 0, 134221765, 0, &PInfo_t1581_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::CachePropertyInfo(System.Reflection.PInfo)
extern const MethodInfo MonoProperty_CachePropertyInfo_m8404_MethodInfo = 
{
	"CachePropertyInfo"/* name */
	, (methodPointerType)&MonoProperty_CachePropertyInfo_m8404/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, MonoProperty_t_MonoProperty_CachePropertyInfo_m8404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyAttributes_t1587_0_0_0;
extern void* RuntimeInvoker_PropertyAttributes_t1587 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyAttributes System.Reflection.MonoProperty::get_Attributes()
extern const MethodInfo MonoProperty_get_Attributes_m8405_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&MonoProperty_get_Attributes_m8405/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t1587_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t1587/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::get_CanRead()
extern const MethodInfo MonoProperty_get_CanRead_m8406_MethodInfo = 
{
	"get_CanRead"/* name */
	, (methodPointerType)&MonoProperty_get_CanRead_m8406/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::get_CanWrite()
extern const MethodInfo MonoProperty_get_CanWrite_m8407_MethodInfo = 
{
	"get_CanWrite"/* name */
	, (methodPointerType)&MonoProperty_get_CanWrite_m8407/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_PropertyType()
extern const MethodInfo MonoProperty_get_PropertyType_m8408_MethodInfo = 
{
	"get_PropertyType"/* name */
	, (methodPointerType)&MonoProperty_get_PropertyType_m8408/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_ReflectedType()
extern const MethodInfo MonoProperty_get_ReflectedType_m8409_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoProperty_get_ReflectedType_m8409/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.MonoProperty::get_DeclaringType()
extern const MethodInfo MonoProperty_get_DeclaringType_m8410_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoProperty_get_DeclaringType_m8410/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoProperty::get_Name()
extern const MethodInfo MonoProperty_get_Name_m8411_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoProperty_get_Name_m8411/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetAccessors_m8412_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221766, 0, &Boolean_t72_0_0_0},
};
extern const Il2CppType MethodInfoU5BU5D_t1575_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.Reflection.MonoProperty::GetAccessors(System.Boolean)
extern const MethodInfo MonoProperty_GetAccessors_m8412_MethodInfo = 
{
	"GetAccessors"/* name */
	, (methodPointerType)&MonoProperty_GetAccessors_m8412/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t1575_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_GetAccessors_m8412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetGetMethod_m8413_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221767, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetGetMethod(System.Boolean)
extern const MethodInfo MonoProperty_GetGetMethod_m8413_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&MonoProperty_GetGetMethod_m8413/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_GetGetMethod_m8413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.MonoProperty::GetIndexParameters()
extern const MethodInfo MonoProperty_GetIndexParameters_m8414_MethodInfo = 
{
	"GetIndexParameters"/* name */
	, (methodPointerType)&MonoProperty_GetIndexParameters_m8414/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetSetMethod_m8415_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221768, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.MonoProperty::GetSetMethod(System.Boolean)
extern const MethodInfo MonoProperty_GetSetMethod_m8415_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&MonoProperty_GetSetMethod_m8415/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_GetSetMethod_m8415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_IsDefined_m8416_ParameterInfos[] = 
{
	{"attributeType", 0, 134221769, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221770, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MonoProperty::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoProperty_IsDefined_m8416_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoProperty_IsDefined_m8416/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_IsDefined_m8416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetCustomAttributes_m8417_ParameterInfos[] = 
{
	{"inherit", 0, 134221771, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoProperty_GetCustomAttributes_m8417_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoProperty_GetCustomAttributes_m8417/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_GetCustomAttributes_m8417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetCustomAttributes_m8418_ParameterInfos[] = 
{
	{"attributeType", 0, 134221772, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221773, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.MonoProperty::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoProperty_GetCustomAttributes_m8418_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoProperty_GetCustomAttributes_m8418/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, MonoProperty_t_MonoProperty_GetCustomAttributes_m8418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Getter_2_t2379_0_0_0;
extern const Il2CppType Getter_2_t2379_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetterAdapterFrame_m10934_ParameterInfos[] = 
{
	{"getter", 0, 134221774, 0, &Getter_2_t2379_0_0_0},
	{"obj", 1, 134221775, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericContainer;
extern TypeInfo MonoProperty_GetterAdapterFrame_m10934_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_GetterAdapterFrame_m10934_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo MonoProperty_GetterAdapterFrame_m10934_gp_R_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_GetterAdapterFrame_m10934_gp_R_1_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericContainer, NULL, "R", 1, 0 };
static const Il2CppGenericParameter* MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericParametersArray[2] = 
{
	&MonoProperty_GetterAdapterFrame_m10934_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&MonoProperty_GetterAdapterFrame_m10934_gp_R_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo MonoProperty_GetterAdapterFrame_m10934_MethodInfo;
extern const Il2CppGenericContainer MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MonoProperty_GetterAdapterFrame_m10934_MethodInfo, 2, 1, MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericParametersArray };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m10934_gp_0_0_0_0;
extern const Il2CppGenericMethod Getter_2_Invoke_m11262_GenericMethod;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m10934_gp_1_0_0_0;
static Il2CppRGCTXDefinition MonoProperty_GetterAdapterFrame_m10934_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_GetterAdapterFrame_m10934_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Getter_2_Invoke_m11262_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_GetterAdapterFrame_m10934_gp_1_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern const MethodInfo MonoProperty_GetterAdapterFrame_m10934_MethodInfo = 
{
	"GetterAdapterFrame"/* name */
	, NULL/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MonoProperty_t_MonoProperty_GetterAdapterFrame_m10934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 3285/* token */
	, MonoProperty_GetterAdapterFrame_m10934_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &MonoProperty_GetterAdapterFrame_m10934_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType StaticGetter_1_t2382_0_0_0;
extern const Il2CppType StaticGetter_1_t2382_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_StaticGetterAdapterFrame_m10935_ParameterInfos[] = 
{
	{"getter", 0, 134221776, 0, &StaticGetter_1_t2382_0_0_0},
	{"obj", 1, 134221777, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericContainer;
extern TypeInfo MonoProperty_StaticGetterAdapterFrame_m10935_gp_R_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter MonoProperty_StaticGetterAdapterFrame_m10935_gp_R_0_il2cpp_TypeInfo_GenericParamFull = { &MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericContainer, NULL, "R", 0, 0 };
static const Il2CppGenericParameter* MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericParametersArray[1] = 
{
	&MonoProperty_StaticGetterAdapterFrame_m10935_gp_R_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo MonoProperty_StaticGetterAdapterFrame_m10935_MethodInfo;
extern const Il2CppGenericContainer MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&MonoProperty_StaticGetterAdapterFrame_m10935_MethodInfo, 1, 1, MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod StaticGetter_1_Invoke_m11263_GenericMethod;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m10935_gp_0_0_0_0;
static Il2CppRGCTXDefinition MonoProperty_StaticGetterAdapterFrame_m10935_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &StaticGetter_1_Invoke_m11263_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&MonoProperty_StaticGetterAdapterFrame_m10935_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern const MethodInfo MonoProperty_StaticGetterAdapterFrame_m10935_MethodInfo = 
{
	"StaticGetterAdapterFrame"/* name */
	, NULL/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, MonoProperty_t_MonoProperty_StaticGetterAdapterFrame_m10935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 3286/* token */
	, MonoProperty_StaticGetterAdapterFrame_m10935_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &MonoProperty_StaticGetterAdapterFrame_m10935_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_CreateGetterDelegate_m8419_ParameterInfos[] = 
{
	{"method", 0, 134221778, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MonoProperty/GetterAdapter System.Reflection.MonoProperty::CreateGetterDelegate(System.Reflection.MethodInfo)
extern const MethodInfo MonoProperty_CreateGetterDelegate_m8419_MethodInfo = 
{
	"CreateGetterDelegate"/* name */
	, (methodPointerType)&MonoProperty_CreateGetterDelegate_m8419/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &GetterAdapter_t1582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_CreateGetterDelegate_m8419_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetValue_m8420_ParameterInfos[] = 
{
	{"obj", 0, 134221779, 0, &Object_t_0_0_0},
	{"index", 1, 134221780, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Object[])
extern const MethodInfo MonoProperty_GetValue_m8420_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoProperty_GetValue_m8420/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_GetValue_m8420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetValue_m8421_ParameterInfos[] = 
{
	{"obj", 0, 134221781, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221782, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134221783, 0, &Binder_t781_0_0_0},
	{"index", 3, 134221784, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 4, 134221785, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.MonoProperty::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoProperty_GetValue_m8421_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&MonoProperty_GetValue_m8421/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_GetValue_m8421_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_SetValue_m8422_ParameterInfos[] = 
{
	{"obj", 0, 134221786, 0, &Object_t_0_0_0},
	{"value", 1, 134221787, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221788, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 3, 134221789, 0, &Binder_t781_0_0_0},
	{"index", 4, 134221790, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 5, 134221791, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo MonoProperty_SetValue_m8422_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&MonoProperty_SetValue_m8422/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, MonoProperty_t_MonoProperty_SetValue_m8422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.MonoProperty::ToString()
extern const MethodInfo MonoProperty_ToString_m8423_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoProperty_ToString_m8423/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoProperty::GetOptionalCustomModifiers()
extern const MethodInfo MonoProperty_GetOptionalCustomModifiers_m8424_MethodInfo = 
{
	"GetOptionalCustomModifiers"/* name */
	, (methodPointerType)&MonoProperty_GetOptionalCustomModifiers_m8424/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.MonoProperty::GetRequiredCustomModifiers()
extern const MethodInfo MonoProperty_GetRequiredCustomModifiers_m8425_MethodInfo = 
{
	"GetRequiredCustomModifiers"/* name */
	, (methodPointerType)&MonoProperty_GetRequiredCustomModifiers_m8425/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MonoProperty_t_MonoProperty_GetObjectData_m8426_ParameterInfos[] = 
{
	{"info", 0, 134221792, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221793, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MonoProperty::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoProperty_GetObjectData_m8426_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoProperty_GetObjectData_m8426/* method */
	, &MonoProperty_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MonoProperty_t_MonoProperty_GetObjectData_m8426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoProperty_t_MethodInfos[] =
{
	&MonoProperty__ctor_m8403_MethodInfo,
	&MonoProperty_CachePropertyInfo_m8404_MethodInfo,
	&MonoProperty_get_Attributes_m8405_MethodInfo,
	&MonoProperty_get_CanRead_m8406_MethodInfo,
	&MonoProperty_get_CanWrite_m8407_MethodInfo,
	&MonoProperty_get_PropertyType_m8408_MethodInfo,
	&MonoProperty_get_ReflectedType_m8409_MethodInfo,
	&MonoProperty_get_DeclaringType_m8410_MethodInfo,
	&MonoProperty_get_Name_m8411_MethodInfo,
	&MonoProperty_GetAccessors_m8412_MethodInfo,
	&MonoProperty_GetGetMethod_m8413_MethodInfo,
	&MonoProperty_GetIndexParameters_m8414_MethodInfo,
	&MonoProperty_GetSetMethod_m8415_MethodInfo,
	&MonoProperty_IsDefined_m8416_MethodInfo,
	&MonoProperty_GetCustomAttributes_m8417_MethodInfo,
	&MonoProperty_GetCustomAttributes_m8418_MethodInfo,
	&MonoProperty_GetterAdapterFrame_m10934_MethodInfo,
	&MonoProperty_StaticGetterAdapterFrame_m10935_MethodInfo,
	&MonoProperty_CreateGetterDelegate_m8419_MethodInfo,
	&MonoProperty_GetValue_m8420_MethodInfo,
	&MonoProperty_GetValue_m8421_MethodInfo,
	&MonoProperty_SetValue_m8422_MethodInfo,
	&MonoProperty_ToString_m8423_MethodInfo,
	&MonoProperty_GetOptionalCustomModifiers_m8424_MethodInfo,
	&MonoProperty_GetRequiredCustomModifiers_m8425_MethodInfo,
	&MonoProperty_GetObjectData_m8426_MethodInfo,
	NULL
};
extern const MethodInfo MonoProperty_get_Attributes_m8405_MethodInfo;
static const PropertyInfo MonoProperty_t____Attributes_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &MonoProperty_get_Attributes_m8405_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_CanRead_m8406_MethodInfo;
static const PropertyInfo MonoProperty_t____CanRead_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "CanRead"/* name */
	, &MonoProperty_get_CanRead_m8406_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_CanWrite_m8407_MethodInfo;
static const PropertyInfo MonoProperty_t____CanWrite_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "CanWrite"/* name */
	, &MonoProperty_get_CanWrite_m8407_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_PropertyType_m8408_MethodInfo;
static const PropertyInfo MonoProperty_t____PropertyType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "PropertyType"/* name */
	, &MonoProperty_get_PropertyType_m8408_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_ReflectedType_m8409_MethodInfo;
static const PropertyInfo MonoProperty_t____ReflectedType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoProperty_get_ReflectedType_m8409_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_DeclaringType_m8410_MethodInfo;
static const PropertyInfo MonoProperty_t____DeclaringType_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoProperty_get_DeclaringType_m8410_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoProperty_get_Name_m8411_MethodInfo;
static const PropertyInfo MonoProperty_t____Name_PropertyInfo = 
{
	&MonoProperty_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoProperty_get_Name_m8411_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoProperty_t_PropertyInfos[] =
{
	&MonoProperty_t____Attributes_PropertyInfo,
	&MonoProperty_t____CanRead_PropertyInfo,
	&MonoProperty_t____CanWrite_PropertyInfo,
	&MonoProperty_t____PropertyType_PropertyInfo,
	&MonoProperty_t____ReflectedType_PropertyInfo,
	&MonoProperty_t____DeclaringType_PropertyInfo,
	&MonoProperty_t____Name_PropertyInfo,
	NULL
};
static const Il2CppType* MonoProperty_t_il2cpp_TypeInfo__nestedTypes[3] =
{
	&GetterAdapter_t1582_0_0_0,
	&Getter_2_t1930_0_0_0,
	&StaticGetter_1_t1929_0_0_0,
};
extern const MethodInfo MonoProperty_ToString_m8423_MethodInfo;
extern const MethodInfo MonoProperty_GetCustomAttributes_m8418_MethodInfo;
extern const MethodInfo MonoProperty_IsDefined_m8416_MethodInfo;
extern const MethodInfo PropertyInfo_get_MemberType_m8446_MethodInfo;
extern const MethodInfo MonoProperty_GetCustomAttributes_m8417_MethodInfo;
extern const MethodInfo MonoProperty_GetAccessors_m8412_MethodInfo;
extern const MethodInfo MonoProperty_GetGetMethod_m8413_MethodInfo;
extern const MethodInfo MonoProperty_GetIndexParameters_m8414_MethodInfo;
extern const MethodInfo MonoProperty_GetSetMethod_m8415_MethodInfo;
extern const MethodInfo MonoProperty_GetValue_m8420_MethodInfo;
extern const MethodInfo MonoProperty_GetValue_m8421_MethodInfo;
extern const MethodInfo PropertyInfo_SetValue_m8448_MethodInfo;
extern const MethodInfo MonoProperty_SetValue_m8422_MethodInfo;
extern const MethodInfo MonoProperty_GetOptionalCustomModifiers_m8424_MethodInfo;
extern const MethodInfo MonoProperty_GetRequiredCustomModifiers_m8425_MethodInfo;
extern const MethodInfo MonoProperty_GetObjectData_m8426_MethodInfo;
static const Il2CppMethodReference MonoProperty_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&MonoProperty_ToString_m8423_MethodInfo,
	&MonoProperty_GetCustomAttributes_m8418_MethodInfo,
	&MonoProperty_IsDefined_m8416_MethodInfo,
	&MonoProperty_get_DeclaringType_m8410_MethodInfo,
	&PropertyInfo_get_MemberType_m8446_MethodInfo,
	&MonoProperty_get_Name_m8411_MethodInfo,
	&MonoProperty_get_ReflectedType_m8409_MethodInfo,
	&MemberInfo_get_Module_m6696_MethodInfo,
	&MonoProperty_IsDefined_m8416_MethodInfo,
	&MonoProperty_GetCustomAttributes_m8417_MethodInfo,
	&MonoProperty_GetCustomAttributes_m8418_MethodInfo,
	&MonoProperty_get_Attributes_m8405_MethodInfo,
	&MonoProperty_get_CanRead_m8406_MethodInfo,
	&MonoProperty_get_CanWrite_m8407_MethodInfo,
	&MonoProperty_get_PropertyType_m8408_MethodInfo,
	&MonoProperty_GetAccessors_m8412_MethodInfo,
	&MonoProperty_GetGetMethod_m8413_MethodInfo,
	&MonoProperty_GetIndexParameters_m8414_MethodInfo,
	&MonoProperty_GetSetMethod_m8415_MethodInfo,
	&MonoProperty_GetValue_m8420_MethodInfo,
	&MonoProperty_GetValue_m8421_MethodInfo,
	&PropertyInfo_SetValue_m8448_MethodInfo,
	&MonoProperty_SetValue_m8422_MethodInfo,
	&MonoProperty_GetOptionalCustomModifiers_m8424_MethodInfo,
	&MonoProperty_GetRequiredCustomModifiers_m8425_MethodInfo,
	&MonoProperty_GetObjectData_m8426_MethodInfo,
};
static bool MonoProperty_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoProperty_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
extern const Il2CppType _PropertyInfo_t2006_0_0_0;
static Il2CppInterfaceOffsetPair MonoProperty_t_InterfacesOffsets[] = 
{
	{ &_PropertyInfo_t2006_0_0_0, 14},
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &ISerializable_t428_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoProperty_t_1_0_0;
struct MonoProperty_t;
const Il2CppTypeDefinitionMetadata MonoProperty_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoProperty_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MonoProperty_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoProperty_t_InterfacesOffsets/* interfaceOffsets */
	, &PropertyInfo_t_0_0_0/* parent */
	, MonoProperty_t_VTable/* vtableMethods */
	, MonoProperty_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1313/* fieldStart */

};
TypeInfo MonoProperty_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoProperty"/* name */
	, "System.Reflection"/* namespaze */
	, MonoProperty_t_MethodInfos/* methods */
	, MonoProperty_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoProperty_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoProperty_t_0_0_0/* byval_arg */
	, &MonoProperty_t_1_0_0/* this_arg */
	, &MonoProperty_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoProperty_t)/* instance_size */
	, sizeof (MonoProperty_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 7/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
// Metadata Definition System.Reflection.ParameterAttributes
extern TypeInfo ParameterAttributes_t1583_il2cpp_TypeInfo;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributesMethodDeclarations.h"
static const MethodInfo* ParameterAttributes_t1583_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ParameterAttributes_t1583_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ParameterAttributes_t1583_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ParameterAttributes_t1583_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterAttributes_t1583_0_0_0;
extern const Il2CppType ParameterAttributes_t1583_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterAttributes_t1583_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ParameterAttributes_t1583_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ParameterAttributes_t1583_VTable/* vtableMethods */
	, ParameterAttributes_t1583_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */

};
TypeInfo ParameterAttributes_t1583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterAttributes_t1583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 426/* custom_attributes_cache */
	, &ParameterAttributes_t1583_0_0_0/* byval_arg */
	, &ParameterAttributes_t1583_1_0_0/* this_arg */
	, &ParameterAttributes_t1583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterAttributes_t1583)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterAttributes_t1583)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
// Metadata Definition System.Reflection.ParameterInfo
extern TypeInfo ParameterInfo_t775_il2cpp_TypeInfo;
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor()
extern const MethodInfo ParameterInfo__ctor_m8427_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m8427/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ParameterBuilder_t1550_0_0_0;
extern const Il2CppType ParameterBuilder_t1550_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ParameterInfo_t775_ParameterInfo__ctor_m8428_ParameterInfos[] = 
{
	{"pb", 0, 134221813, 0, &ParameterBuilder_t1550_0_0_0},
	{"type", 1, 134221814, 0, &Type_t_0_0_0},
	{"member", 2, 134221815, 0, &MemberInfo_t_0_0_0},
	{"position", 3, 134221816, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.Emit.ParameterBuilder,System.Type,System.Reflection.MemberInfo,System.Int32)
extern const MethodInfo ParameterInfo__ctor_m8428_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m8428/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54/* invoker_method */
	, ParameterInfo_t775_ParameterInfo__ctor_m8428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ParameterInfo_t775_0_0_0;
extern const Il2CppType ParameterInfo_t775_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
static const ParameterInfo ParameterInfo_t775_ParameterInfo__ctor_m8429_ParameterInfos[] = 
{
	{"pinfo", 0, 134221817, 0, &ParameterInfo_t775_0_0_0},
	{"member", 1, 134221818, 0, &MemberInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.ParameterInfo,System.Reflection.MemberInfo)
extern const MethodInfo ParameterInfo__ctor_m8429_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ParameterInfo__ctor_m8429/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ParameterInfo_t775_ParameterInfo__ctor_m8429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.ParameterInfo::ToString()
extern const MethodInfo ParameterInfo_ToString_m8430_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ParameterInfo_ToString_m8430/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.ParameterInfo::get_ParameterType()
extern const MethodInfo ParameterInfo_get_ParameterType_m8431_MethodInfo = 
{
	"get_ParameterType"/* name */
	, (methodPointerType)&ParameterInfo_get_ParameterType_m8431/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ParameterAttributes_t1583 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes()
extern const MethodInfo ParameterInfo_get_Attributes_m8432_MethodInfo = 
{
	"get_Attributes"/* name */
	, (methodPointerType)&ParameterInfo_get_Attributes_m8432/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &ParameterAttributes_t1583_0_0_0/* return_type */
	, RuntimeInvoker_ParameterAttributes_t1583/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsIn()
extern const MethodInfo ParameterInfo_get_IsIn_m8433_MethodInfo = 
{
	"get_IsIn"/* name */
	, (methodPointerType)&ParameterInfo_get_IsIn_m8433/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsOptional()
extern const MethodInfo ParameterInfo_get_IsOptional_m8434_MethodInfo = 
{
	"get_IsOptional"/* name */
	, (methodPointerType)&ParameterInfo_get_IsOptional_m8434/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsOut()
extern const MethodInfo ParameterInfo_get_IsOut_m8435_MethodInfo = 
{
	"get_IsOut"/* name */
	, (methodPointerType)&ParameterInfo_get_IsOut_m8435/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::get_IsRetval()
extern const MethodInfo ParameterInfo_get_IsRetval_m8436_MethodInfo = 
{
	"get_IsRetval"/* name */
	, (methodPointerType)&ParameterInfo_get_IsRetval_m8436/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberInfo System.Reflection.ParameterInfo::get_Member()
extern const MethodInfo ParameterInfo_get_Member_m8437_MethodInfo = 
{
	"get_Member"/* name */
	, (methodPointerType)&ParameterInfo_get_Member_m8437/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &MemberInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Reflection.ParameterInfo::get_Name()
extern const MethodInfo ParameterInfo_get_Name_m8438_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ParameterInfo_get_Name_m8438/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Reflection.ParameterInfo::get_Position()
extern const MethodInfo ParameterInfo_get_Position_m8439_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&ParameterInfo_get_Position_m8439/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ParameterInfo_t775_ParameterInfo_GetCustomAttributes_m8440_ParameterInfos[] = 
{
	{"attributeType", 0, 134221819, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221820, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.ParameterInfo::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo ParameterInfo_GetCustomAttributes_m8440_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&ParameterInfo_GetCustomAttributes_m8440/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, ParameterInfo_t775_ParameterInfo_GetCustomAttributes_m8440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ParameterInfo_t775_ParameterInfo_IsDefined_m8441_ParameterInfos[] = 
{
	{"attributeType", 0, 134221821, 0, &Type_t_0_0_0},
	{"inherit", 1, 134221822, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.ParameterInfo::IsDefined(System.Type,System.Boolean)
extern const MethodInfo ParameterInfo_IsDefined_m8441_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&ParameterInfo_IsDefined_m8441/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_SByte_t73/* invoker_method */
	, ParameterInfo_t775_ParameterInfo_IsDefined_m8441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Reflection.ParameterInfo::GetPseudoCustomAttributes()
extern const MethodInfo ParameterInfo_GetPseudoCustomAttributes_m8442_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&ParameterInfo_GetPseudoCustomAttributes_m8442/* method */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ParameterInfo_t775_MethodInfos[] =
{
	&ParameterInfo__ctor_m8427_MethodInfo,
	&ParameterInfo__ctor_m8428_MethodInfo,
	&ParameterInfo__ctor_m8429_MethodInfo,
	&ParameterInfo_ToString_m8430_MethodInfo,
	&ParameterInfo_get_ParameterType_m8431_MethodInfo,
	&ParameterInfo_get_Attributes_m8432_MethodInfo,
	&ParameterInfo_get_IsIn_m8433_MethodInfo,
	&ParameterInfo_get_IsOptional_m8434_MethodInfo,
	&ParameterInfo_get_IsOut_m8435_MethodInfo,
	&ParameterInfo_get_IsRetval_m8436_MethodInfo,
	&ParameterInfo_get_Member_m8437_MethodInfo,
	&ParameterInfo_get_Name_m8438_MethodInfo,
	&ParameterInfo_get_Position_m8439_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m8440_MethodInfo,
	&ParameterInfo_IsDefined_m8441_MethodInfo,
	&ParameterInfo_GetPseudoCustomAttributes_m8442_MethodInfo,
	NULL
};
extern const MethodInfo ParameterInfo_get_ParameterType_m8431_MethodInfo;
static const PropertyInfo ParameterInfo_t775____ParameterType_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "ParameterType"/* name */
	, &ParameterInfo_get_ParameterType_m8431_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Attributes_m8432_MethodInfo;
static const PropertyInfo ParameterInfo_t775____Attributes_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &ParameterInfo_get_Attributes_m8432_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsIn_m8433_MethodInfo;
static const PropertyInfo ParameterInfo_t775____IsIn_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "IsIn"/* name */
	, &ParameterInfo_get_IsIn_m8433_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsOptional_m8434_MethodInfo;
static const PropertyInfo ParameterInfo_t775____IsOptional_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "IsOptional"/* name */
	, &ParameterInfo_get_IsOptional_m8434_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsOut_m8435_MethodInfo;
static const PropertyInfo ParameterInfo_t775____IsOut_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "IsOut"/* name */
	, &ParameterInfo_get_IsOut_m8435_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_IsRetval_m8436_MethodInfo;
static const PropertyInfo ParameterInfo_t775____IsRetval_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "IsRetval"/* name */
	, &ParameterInfo_get_IsRetval_m8436_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Member_m8437_MethodInfo;
static const PropertyInfo ParameterInfo_t775____Member_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "Member"/* name */
	, &ParameterInfo_get_Member_m8437_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Name_m8438_MethodInfo;
static const PropertyInfo ParameterInfo_t775____Name_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ParameterInfo_get_Name_m8438_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ParameterInfo_get_Position_m8439_MethodInfo;
static const PropertyInfo ParameterInfo_t775____Position_PropertyInfo = 
{
	&ParameterInfo_t775_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &ParameterInfo_get_Position_m8439_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ParameterInfo_t775_PropertyInfos[] =
{
	&ParameterInfo_t775____ParameterType_PropertyInfo,
	&ParameterInfo_t775____Attributes_PropertyInfo,
	&ParameterInfo_t775____IsIn_PropertyInfo,
	&ParameterInfo_t775____IsOptional_PropertyInfo,
	&ParameterInfo_t775____IsOut_PropertyInfo,
	&ParameterInfo_t775____IsRetval_PropertyInfo,
	&ParameterInfo_t775____Member_PropertyInfo,
	&ParameterInfo_t775____Name_PropertyInfo,
	&ParameterInfo_t775____Position_PropertyInfo,
	NULL
};
extern const MethodInfo ParameterInfo_ToString_m8430_MethodInfo;
extern const MethodInfo ParameterInfo_GetCustomAttributes_m8440_MethodInfo;
extern const MethodInfo ParameterInfo_IsDefined_m8441_MethodInfo;
static const Il2CppMethodReference ParameterInfo_t775_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&ParameterInfo_ToString_m8430_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m8440_MethodInfo,
	&ParameterInfo_IsDefined_m8441_MethodInfo,
	&ParameterInfo_get_ParameterType_m8431_MethodInfo,
	&ParameterInfo_get_Attributes_m8432_MethodInfo,
	&ParameterInfo_get_Member_m8437_MethodInfo,
	&ParameterInfo_get_Name_m8438_MethodInfo,
	&ParameterInfo_get_Position_m8439_MethodInfo,
	&ParameterInfo_GetCustomAttributes_m8440_MethodInfo,
	&ParameterInfo_IsDefined_m8441_MethodInfo,
};
static bool ParameterInfo_t775_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _ParameterInfo_t2005_0_0_0;
static const Il2CppType* ParameterInfo_t775_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t1925_0_0_0,
	&_ParameterInfo_t2005_0_0_0,
};
static Il2CppInterfaceOffsetPair ParameterInfo_t775_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_ParameterInfo_t2005_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterInfo_t775_1_0_0;
struct ParameterInfo_t775;
const Il2CppTypeDefinitionMetadata ParameterInfo_t775_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ParameterInfo_t775_InterfacesTypeInfos/* implementedInterfaces */
	, ParameterInfo_t775_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ParameterInfo_t775_VTable/* vtableMethods */
	, ParameterInfo_t775_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1330/* fieldStart */

};
TypeInfo ParameterInfo_t775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterInfo"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterInfo_t775_MethodInfos/* methods */
	, ParameterInfo_t775_PropertyInfos/* properties */
	, NULL/* events */
	, &ParameterInfo_t775_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 427/* custom_attributes_cache */
	, &ParameterInfo_t775_0_0_0/* byval_arg */
	, &ParameterInfo_t775_1_0_0/* this_arg */
	, &ParameterInfo_t775_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterInfo_t775)/* instance_size */
	, sizeof (ParameterInfo_t775)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 9/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
// Metadata Definition System.Reflection.ParameterModifier
extern TypeInfo ParameterModifier_t1584_il2cpp_TypeInfo;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifierMethodDeclarations.h"
static const MethodInfo* ParameterModifier_t1584_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ParameterModifier_t1584_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool ParameterModifier_t1584_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterModifier_t1584_0_0_0;
extern const Il2CppType ParameterModifier_t1584_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterModifier_t1584_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, ParameterModifier_t1584_VTable/* vtableMethods */
	, ParameterModifier_t1584_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1337/* fieldStart */

};
TypeInfo ParameterModifier_t1584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterModifier"/* name */
	, "System.Reflection"/* namespaze */
	, ParameterModifier_t1584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ParameterModifier_t1584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 428/* custom_attributes_cache */
	, &ParameterModifier_t1584_0_0_0/* byval_arg */
	, &ParameterModifier_t1584_1_0_0/* this_arg */
	, &ParameterModifier_t1584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ParameterModifier_t1584_marshal/* marshal_to_native_func */
	, (methodPointerType)ParameterModifier_t1584_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ParameterModifier_t1584_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ParameterModifier_t1584)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterModifier_t1584)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ParameterModifier_t1584_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_Pointer.h"
// Metadata Definition System.Reflection.Pointer
extern TypeInfo Pointer_t1585_il2cpp_TypeInfo;
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_PointerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Pointer::.ctor()
extern const MethodInfo Pointer__ctor_m8443_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Pointer__ctor_m8443/* method */
	, &Pointer_t1585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo Pointer_t1585_Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_ParameterInfos[] = 
{
	{"info", 0, 134221823, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221824, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444/* method */
	, &Pointer_t1585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, Pointer_t1585_Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Pointer_t1585_MethodInfos[] =
{
	&Pointer__ctor_m8443_MethodInfo,
	&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_MethodInfo,
	NULL
};
extern const MethodInfo Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_MethodInfo;
static const Il2CppMethodReference Pointer_t1585_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8444_MethodInfo,
};
static bool Pointer_t1585_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Pointer_t1585_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair Pointer_t1585_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Pointer_t1585_0_0_0;
extern const Il2CppType Pointer_t1585_1_0_0;
struct Pointer_t1585;
const Il2CppTypeDefinitionMetadata Pointer_t1585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Pointer_t1585_InterfacesTypeInfos/* implementedInterfaces */
	, Pointer_t1585_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Pointer_t1585_VTable/* vtableMethods */
	, Pointer_t1585_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1338/* fieldStart */

};
TypeInfo Pointer_t1585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Pointer"/* name */
	, "System.Reflection"/* namespaze */
	, Pointer_t1585_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Pointer_t1585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 429/* custom_attributes_cache */
	, &Pointer_t1585_0_0_0/* byval_arg */
	, &Pointer_t1585_1_0_0/* this_arg */
	, &Pointer_t1585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Pointer_t1585)/* instance_size */
	, sizeof (Pointer_t1585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"
// Metadata Definition System.Reflection.ProcessorArchitecture
extern TypeInfo ProcessorArchitecture_t1586_il2cpp_TypeInfo;
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitectureMethodDeclarations.h"
static const MethodInfo* ProcessorArchitecture_t1586_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ProcessorArchitecture_t1586_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ProcessorArchitecture_t1586_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ProcessorArchitecture_t1586_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProcessorArchitecture_t1586_0_0_0;
extern const Il2CppType ProcessorArchitecture_t1586_1_0_0;
const Il2CppTypeDefinitionMetadata ProcessorArchitecture_t1586_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ProcessorArchitecture_t1586_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ProcessorArchitecture_t1586_VTable/* vtableMethods */
	, ProcessorArchitecture_t1586_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1340/* fieldStart */

};
TypeInfo ProcessorArchitecture_t1586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProcessorArchitecture"/* name */
	, "System.Reflection"/* namespaze */
	, ProcessorArchitecture_t1586_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 430/* custom_attributes_cache */
	, &ProcessorArchitecture_t1586_0_0_0/* byval_arg */
	, &ProcessorArchitecture_t1586_1_0_0/* this_arg */
	, &ProcessorArchitecture_t1586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProcessorArchitecture_t1586)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ProcessorArchitecture_t1586)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
// Metadata Definition System.Reflection.PropertyAttributes
extern TypeInfo PropertyAttributes_t1587_il2cpp_TypeInfo;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributesMethodDeclarations.h"
static const MethodInfo* PropertyAttributes_t1587_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PropertyAttributes_t1587_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool PropertyAttributes_t1587_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttributes_t1587_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyAttributes_t1587_1_0_0;
const Il2CppTypeDefinitionMetadata PropertyAttributes_t1587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttributes_t1587_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, PropertyAttributes_t1587_VTable/* vtableMethods */
	, PropertyAttributes_t1587_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1346/* fieldStart */

};
TypeInfo PropertyAttributes_t1587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, PropertyAttributes_t1587_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 431/* custom_attributes_cache */
	, &PropertyAttributes_t1587_0_0_0/* byval_arg */
	, &PropertyAttributes_t1587_1_0_0/* this_arg */
	, &PropertyAttributes_t1587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttributes_t1587)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PropertyAttributes_t1587)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
// Metadata Definition System.Reflection.PropertyInfo
extern TypeInfo PropertyInfo_t_il2cpp_TypeInfo;
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::.ctor()
extern const MethodInfo PropertyInfo__ctor_m8445_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyInfo__ctor_m8445/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PropertyAttributes_t1587 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyAttributes System.Reflection.PropertyInfo::get_Attributes()
extern const MethodInfo PropertyInfo_get_Attributes_m10944_MethodInfo = 
{
	"get_Attributes"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyAttributes_t1587_0_0_0/* return_type */
	, RuntimeInvoker_PropertyAttributes_t1587/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.PropertyInfo::get_CanRead()
extern const MethodInfo PropertyInfo_get_CanRead_m10945_MethodInfo = 
{
	"get_CanRead"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.PropertyInfo::get_CanWrite()
extern const MethodInfo PropertyInfo_get_CanWrite_m10946_MethodInfo = 
{
	"get_CanWrite"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_MemberTypes_t1570 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.Reflection.PropertyInfo::get_MemberType()
extern const MethodInfo PropertyInfo_get_MemberType_m8446_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&PropertyInfo_get_MemberType_m8446/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t1570_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t1570/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Reflection.PropertyInfo::get_PropertyType()
extern const MethodInfo PropertyInfo_get_PropertyType_m10947_MethodInfo = 
{
	"get_PropertyType"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetAccessors_m10948_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221825, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.Reflection.PropertyInfo::GetAccessors(System.Boolean)
extern const MethodInfo PropertyInfo_GetAccessors_m10948_MethodInfo = 
{
	"GetAccessors"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t1575_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetAccessors_m10948_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetGetMethod_m10949_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221826, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean)
extern const MethodInfo PropertyInfo_GetGetMethod_m10949_MethodInfo = 
{
	"GetGetMethod"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetGetMethod_m10949_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters()
extern const MethodInfo PropertyInfo_GetIndexParameters_m10950_MethodInfo = 
{
	"GetIndexParameters"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &ParameterInfoU5BU5D_t774_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetSetMethod_m10951_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221827, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean)
extern const MethodInfo PropertyInfo_GetSetMethod_m10951_MethodInfo = 
{
	"GetSetMethod"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetSetMethod_m10951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetValue_m8447_ParameterInfos[] = 
{
	{"obj", 0, 134221828, 0, &Object_t_0_0_0},
	{"index", 1, 134221829, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[])
extern const MethodInfo PropertyInfo_GetValue_m8447_MethodInfo = 
{
	"GetValue"/* name */
	, (methodPointerType)&PropertyInfo_GetValue_m8447/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetValue_m8447_ParameterInfos/* parameters */
	, 433/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_GetValue_m10952_ParameterInfos[] = 
{
	{"obj", 0, 134221830, 0, &Object_t_0_0_0},
	{"invokeAttr", 1, 134221831, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 2, 134221832, 0, &Binder_t781_0_0_0},
	{"index", 3, 134221833, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 4, 134221834, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo PropertyInfo_GetValue_m10952_MethodInfo = 
{
	"GetValue"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_GetValue_m10952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_SetValue_m8448_ParameterInfos[] = 
{
	{"obj", 0, 134221835, 0, &Object_t_0_0_0},
	{"value", 1, 134221836, 0, &Object_t_0_0_0},
	{"index", 2, 134221837, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[])
extern const MethodInfo PropertyInfo_SetValue_m8448_MethodInfo = 
{
	"SetValue"/* name */
	, (methodPointerType)&PropertyInfo_SetValue_m8448/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_SetValue_m8448_ParameterInfos/* parameters */
	, 434/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType BindingFlags_t1564_0_0_0;
extern const Il2CppType Binder_t781_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType CultureInfo_t750_0_0_0;
static const ParameterInfo PropertyInfo_t_PropertyInfo_SetValue_m10953_ParameterInfos[] = 
{
	{"obj", 0, 134221838, 0, &Object_t_0_0_0},
	{"value", 1, 134221839, 0, &Object_t_0_0_0},
	{"invokeAttr", 2, 134221840, 0, &BindingFlags_t1564_0_0_0},
	{"binder", 3, 134221841, 0, &Binder_t781_0_0_0},
	{"index", 4, 134221842, 0, &ObjectU5BU5D_t29_0_0_0},
	{"culture", 5, 134221843, 0, &CultureInfo_t750_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern const MethodInfo PropertyInfo_SetValue_m10953_MethodInfo = 
{
	"SetValue"/* name */
	, NULL/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t_Object_t/* invoker_method */
	, PropertyInfo_t_PropertyInfo_SetValue_m10953_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.PropertyInfo::GetOptionalCustomModifiers()
extern const MethodInfo PropertyInfo_GetOptionalCustomModifiers_m8449_MethodInfo = 
{
	"GetOptionalCustomModifiers"/* name */
	, (methodPointerType)&PropertyInfo_GetOptionalCustomModifiers_m8449/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Reflection.PropertyInfo::GetRequiredCustomModifiers()
extern const MethodInfo PropertyInfo_GetRequiredCustomModifiers_m8450_MethodInfo = 
{
	"GetRequiredCustomModifiers"/* name */
	, (methodPointerType)&PropertyInfo_GetRequiredCustomModifiers_m8450/* method */
	, &PropertyInfo_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyInfo_t_MethodInfos[] =
{
	&PropertyInfo__ctor_m8445_MethodInfo,
	&PropertyInfo_get_Attributes_m10944_MethodInfo,
	&PropertyInfo_get_CanRead_m10945_MethodInfo,
	&PropertyInfo_get_CanWrite_m10946_MethodInfo,
	&PropertyInfo_get_MemberType_m8446_MethodInfo,
	&PropertyInfo_get_PropertyType_m10947_MethodInfo,
	&PropertyInfo_GetAccessors_m10948_MethodInfo,
	&PropertyInfo_GetGetMethod_m10949_MethodInfo,
	&PropertyInfo_GetIndexParameters_m10950_MethodInfo,
	&PropertyInfo_GetSetMethod_m10951_MethodInfo,
	&PropertyInfo_GetValue_m8447_MethodInfo,
	&PropertyInfo_GetValue_m10952_MethodInfo,
	&PropertyInfo_SetValue_m8448_MethodInfo,
	&PropertyInfo_SetValue_m10953_MethodInfo,
	&PropertyInfo_GetOptionalCustomModifiers_m8449_MethodInfo,
	&PropertyInfo_GetRequiredCustomModifiers_m8450_MethodInfo,
	NULL
};
extern const MethodInfo PropertyInfo_get_Attributes_m10944_MethodInfo;
static const PropertyInfo PropertyInfo_t____Attributes_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "Attributes"/* name */
	, &PropertyInfo_get_Attributes_m10944_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_CanRead_m10945_MethodInfo;
static const PropertyInfo PropertyInfo_t____CanRead_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "CanRead"/* name */
	, &PropertyInfo_get_CanRead_m10945_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_CanWrite_m10946_MethodInfo;
static const PropertyInfo PropertyInfo_t____CanWrite_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "CanWrite"/* name */
	, &PropertyInfo_get_CanWrite_m10946_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo PropertyInfo_t____MemberType_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &PropertyInfo_get_MemberType_m8446_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PropertyInfo_get_PropertyType_m10947_MethodInfo;
static const PropertyInfo PropertyInfo_t____PropertyType_PropertyInfo = 
{
	&PropertyInfo_t_il2cpp_TypeInfo/* parent */
	, "PropertyType"/* name */
	, &PropertyInfo_get_PropertyType_m10947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PropertyInfo_t_PropertyInfos[] =
{
	&PropertyInfo_t____Attributes_PropertyInfo,
	&PropertyInfo_t____CanRead_PropertyInfo,
	&PropertyInfo_t____CanWrite_PropertyInfo,
	&PropertyInfo_t____MemberType_PropertyInfo,
	&PropertyInfo_t____PropertyType_PropertyInfo,
	NULL
};
extern const MethodInfo PropertyInfo_GetValue_m8447_MethodInfo;
extern const MethodInfo PropertyInfo_GetOptionalCustomModifiers_m8449_MethodInfo;
extern const MethodInfo PropertyInfo_GetRequiredCustomModifiers_m8450_MethodInfo;
static const Il2CppMethodReference PropertyInfo_t_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MemberInfo_GetCustomAttributes_m10589_MethodInfo,
	&MemberInfo_IsDefined_m10587_MethodInfo,
	NULL,
	&PropertyInfo_get_MemberType_m8446_MethodInfo,
	NULL,
	NULL,
	&MemberInfo_get_Module_m6696_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&PropertyInfo_GetValue_m8447_MethodInfo,
	NULL,
	&PropertyInfo_SetValue_m8448_MethodInfo,
	NULL,
	&PropertyInfo_GetOptionalCustomModifiers_m8449_MethodInfo,
	&PropertyInfo_GetRequiredCustomModifiers_m8450_MethodInfo,
};
static bool PropertyInfo_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PropertyInfo_t_InterfacesTypeInfos[] = 
{
	&_PropertyInfo_t2006_0_0_0,
};
static Il2CppInterfaceOffsetPair PropertyInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t1925_0_0_0, 4},
	{ &_MemberInfo_t1962_0_0_0, 6},
	{ &_PropertyInfo_t2006_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyInfo_t_1_0_0;
struct PropertyInfo_t;
const Il2CppTypeDefinitionMetadata PropertyInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PropertyInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, PropertyInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, PropertyInfo_t_VTable/* vtableMethods */
	, PropertyInfo_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, PropertyInfo_t_MethodInfos/* methods */
	, PropertyInfo_t_PropertyInfos/* properties */
	, NULL/* events */
	, &PropertyInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 432/* custom_attributes_cache */
	, &PropertyInfo_t_0_0_0/* byval_arg */
	, &PropertyInfo_t_1_0_0/* this_arg */
	, &PropertyInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyInfo_t)/* instance_size */
	, sizeof (PropertyInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPair.h"
// Metadata Definition System.Reflection.StrongNameKeyPair
extern TypeInfo StrongNameKeyPair_t1560_il2cpp_TypeInfo;
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPairMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo StrongNameKeyPair_t1560_StrongNameKeyPair__ctor_m8451_ParameterInfos[] = 
{
	{"info", 0, 134221844, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221845, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo StrongNameKeyPair__ctor_m8451_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StrongNameKeyPair__ctor_m8451/* method */
	, &StrongNameKeyPair_t1560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, StrongNameKeyPair_t1560_StrongNameKeyPair__ctor_m8451_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo StrongNameKeyPair_t1560_StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_ParameterInfos[] = 
{
	{"info", 0, 134221846, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221847, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452/* method */
	, &StrongNameKeyPair_t1560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, StrongNameKeyPair_t1560_StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StrongNameKeyPair_t1560_StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_ParameterInfos[] = 
{
	{"sender", 0, 134221848, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453/* method */
	, &StrongNameKeyPair_t1560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, StrongNameKeyPair_t1560_StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StrongNameKeyPair_t1560_MethodInfos[] =
{
	&StrongNameKeyPair__ctor_m8451_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_MethodInfo,
	NULL
};
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_MethodInfo;
extern const MethodInfo StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_MethodInfo;
static const Il2CppMethodReference StrongNameKeyPair_t1560_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8452_MethodInfo,
	&StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8453_MethodInfo,
};
static bool StrongNameKeyPair_t1560_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* StrongNameKeyPair_t1560_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&IDeserializationCallback_t1206_0_0_0,
};
static Il2CppInterfaceOffsetPair StrongNameKeyPair_t1560_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IDeserializationCallback_t1206_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongNameKeyPair_t1560_0_0_0;
extern const Il2CppType StrongNameKeyPair_t1560_1_0_0;
struct StrongNameKeyPair_t1560;
const Il2CppTypeDefinitionMetadata StrongNameKeyPair_t1560_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StrongNameKeyPair_t1560_InterfacesTypeInfos/* implementedInterfaces */
	, StrongNameKeyPair_t1560_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongNameKeyPair_t1560_VTable/* vtableMethods */
	, StrongNameKeyPair_t1560_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1355/* fieldStart */

};
TypeInfo StrongNameKeyPair_t1560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongNameKeyPair"/* name */
	, "System.Reflection"/* namespaze */
	, StrongNameKeyPair_t1560_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StrongNameKeyPair_t1560_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 435/* custom_attributes_cache */
	, &StrongNameKeyPair_t1560_0_0_0/* byval_arg */
	, &StrongNameKeyPair_t1560_1_0_0/* this_arg */
	, &StrongNameKeyPair_t1560_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongNameKeyPair_t1560)/* instance_size */
	, sizeof (StrongNameKeyPair_t1560)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetException.h"
// Metadata Definition System.Reflection.TargetException
extern TypeInfo TargetException_t1588_il2cpp_TypeInfo;
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor()
extern const MethodInfo TargetException__ctor_m8454_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m8454/* method */
	, &TargetException_t1588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TargetException_t1588_TargetException__ctor_m8455_ParameterInfos[] = 
{
	{"message", 0, 134221849, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor(System.String)
extern const MethodInfo TargetException__ctor_m8455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m8455/* method */
	, &TargetException_t1588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TargetException_t1588_TargetException__ctor_m8455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TargetException_t1588_TargetException__ctor_m8456_ParameterInfos[] = 
{
	{"info", 0, 134221850, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221851, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetException__ctor_m8456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetException__ctor_m8456/* method */
	, &TargetException_t1588_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TargetException_t1588_TargetException__ctor_m8456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetException_t1588_MethodInfos[] =
{
	&TargetException__ctor_m8454_MethodInfo,
	&TargetException__ctor_m8455_MethodInfo,
	&TargetException__ctor_m8456_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m3652_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m3653_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m3654_MethodInfo;
extern const MethodInfo Exception_get_Message_m3655_MethodInfo;
extern const MethodInfo Exception_get_Source_m3656_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m3657_MethodInfo;
extern const MethodInfo Exception_GetType_m3658_MethodInfo;
static const Il2CppMethodReference TargetException_t1588_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool TargetException_t1588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t824_0_0_0;
static Il2CppInterfaceOffsetPair TargetException_t1588_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetException_t1588_0_0_0;
extern const Il2CppType TargetException_t1588_1_0_0;
extern const Il2CppType Exception_t42_0_0_0;
struct TargetException_t1588;
const Il2CppTypeDefinitionMetadata TargetException_t1588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetException_t1588_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t42_0_0_0/* parent */
	, TargetException_t1588_VTable/* vtableMethods */
	, TargetException_t1588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetException_t1588_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetException_t1588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetException_t1588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 436/* custom_attributes_cache */
	, &TargetException_t1588_0_0_0/* byval_arg */
	, &TargetException_t1588_1_0_0/* this_arg */
	, &TargetException_t1588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetException_t1588)/* instance_size */
	, sizeof (TargetException_t1588)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationException.h"
// Metadata Definition System.Reflection.TargetInvocationException
extern TypeInfo TargetInvocationException_t1589_il2cpp_TypeInfo;
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationExceptionMethodDeclarations.h"
extern const Il2CppType Exception_t42_0_0_0;
static const ParameterInfo TargetInvocationException_t1589_TargetInvocationException__ctor_m8457_ParameterInfos[] = 
{
	{"inner", 0, 134221852, 0, &Exception_t42_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Exception)
extern const MethodInfo TargetInvocationException__ctor_m8457_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetInvocationException__ctor_m8457/* method */
	, &TargetInvocationException_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TargetInvocationException_t1589_TargetInvocationException__ctor_m8457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TargetInvocationException_t1589_TargetInvocationException__ctor_m8458_ParameterInfos[] = 
{
	{"info", 0, 134221853, 0, &SerializationInfo_t725_0_0_0},
	{"sc", 1, 134221854, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetInvocationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetInvocationException__ctor_m8458_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetInvocationException__ctor_m8458/* method */
	, &TargetInvocationException_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TargetInvocationException_t1589_TargetInvocationException__ctor_m8458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetInvocationException_t1589_MethodInfos[] =
{
	&TargetInvocationException__ctor_m8457_MethodInfo,
	&TargetInvocationException__ctor_m8458_MethodInfo,
	NULL
};
static const Il2CppMethodReference TargetInvocationException_t1589_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool TargetInvocationException_t1589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TargetInvocationException_t1589_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetInvocationException_t1589_0_0_0;
extern const Il2CppType TargetInvocationException_t1589_1_0_0;
struct TargetInvocationException_t1589;
const Il2CppTypeDefinitionMetadata TargetInvocationException_t1589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetInvocationException_t1589_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t42_0_0_0/* parent */
	, TargetInvocationException_t1589_VTable/* vtableMethods */
	, TargetInvocationException_t1589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetInvocationException_t1589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetInvocationException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetInvocationException_t1589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetInvocationException_t1589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 437/* custom_attributes_cache */
	, &TargetInvocationException_t1589_0_0_0/* byval_arg */
	, &TargetInvocationException_t1589_1_0_0/* this_arg */
	, &TargetInvocationException_t1589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetInvocationException_t1589)/* instance_size */
	, sizeof (TargetInvocationException_t1589)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountException.h"
// Metadata Definition System.Reflection.TargetParameterCountException
extern TypeInfo TargetParameterCountException_t1590_il2cpp_TypeInfo;
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor()
extern const MethodInfo TargetParameterCountException__ctor_m8459_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m8459/* method */
	, &TargetParameterCountException_t1590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TargetParameterCountException_t1590_TargetParameterCountException__ctor_m8460_ParameterInfos[] = 
{
	{"message", 0, 134221855, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.String)
extern const MethodInfo TargetParameterCountException__ctor_m8460_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m8460/* method */
	, &TargetParameterCountException_t1590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TargetParameterCountException_t1590_TargetParameterCountException__ctor_m8460_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo TargetParameterCountException_t1590_TargetParameterCountException__ctor_m8461_ParameterInfos[] = 
{
	{"info", 0, 134221856, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221857, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TargetParameterCountException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TargetParameterCountException__ctor_m8461_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TargetParameterCountException__ctor_m8461/* method */
	, &TargetParameterCountException_t1590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, TargetParameterCountException_t1590_TargetParameterCountException__ctor_m8461_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TargetParameterCountException_t1590_MethodInfos[] =
{
	&TargetParameterCountException__ctor_m8459_MethodInfo,
	&TargetParameterCountException__ctor_m8460_MethodInfo,
	&TargetParameterCountException__ctor_m8461_MethodInfo,
	NULL
};
static const Il2CppMethodReference TargetParameterCountException_t1590_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool TargetParameterCountException_t1590_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TargetParameterCountException_t1590_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetParameterCountException_t1590_0_0_0;
extern const Il2CppType TargetParameterCountException_t1590_1_0_0;
struct TargetParameterCountException_t1590;
const Il2CppTypeDefinitionMetadata TargetParameterCountException_t1590_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetParameterCountException_t1590_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t42_0_0_0/* parent */
	, TargetParameterCountException_t1590_VTable/* vtableMethods */
	, TargetParameterCountException_t1590_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TargetParameterCountException_t1590_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetParameterCountException"/* name */
	, "System.Reflection"/* namespaze */
	, TargetParameterCountException_t1590_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TargetParameterCountException_t1590_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 438/* custom_attributes_cache */
	, &TargetParameterCountException_t1590_0_0_0/* byval_arg */
	, &TargetParameterCountException_t1590_1_0_0/* this_arg */
	, &TargetParameterCountException_t1590_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetParameterCountException_t1590)/* instance_size */
	, sizeof (TargetParameterCountException_t1590)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// Metadata Definition System.Reflection.TypeAttributes
extern TypeInfo TypeAttributes_t1591_il2cpp_TypeInfo;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributesMethodDeclarations.h"
static const MethodInfo* TypeAttributes_t1591_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeAttributes_t1591_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TypeAttributes_t1591_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeAttributes_t1591_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeAttributes_t1591_0_0_0;
extern const Il2CppType TypeAttributes_t1591_1_0_0;
const Il2CppTypeDefinitionMetadata TypeAttributes_t1591_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeAttributes_t1591_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TypeAttributes_t1591_VTable/* vtableMethods */
	, TypeAttributes_t1591_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1359/* fieldStart */

};
TypeInfo TypeAttributes_t1591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, TypeAttributes_t1591_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 439/* custom_attributes_cache */
	, &TypeAttributes_t1591_0_0_0/* byval_arg */
	, &TypeAttributes_t1591_1_0_0/* this_arg */
	, &TypeAttributes_t1591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeAttributes_t1591)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeAttributes_t1591)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 32/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// Metadata Definition System.Resources.NeutralResourcesLanguageAttribute
extern TypeInfo NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo;
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NeutralResourcesLanguageAttribute_t931_NeutralResourcesLanguageAttribute__ctor_m3743_ParameterInfos[] = 
{
	{"cultureName", 0, 134221858, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Resources.NeutralResourcesLanguageAttribute::.ctor(System.String)
extern const MethodInfo NeutralResourcesLanguageAttribute__ctor_m3743_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NeutralResourcesLanguageAttribute__ctor_m3743/* method */
	, &NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, NeutralResourcesLanguageAttribute_t931_NeutralResourcesLanguageAttribute__ctor_m3743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NeutralResourcesLanguageAttribute_t931_MethodInfos[] =
{
	&NeutralResourcesLanguageAttribute__ctor_m3743_MethodInfo,
	NULL
};
static const Il2CppMethodReference NeutralResourcesLanguageAttribute_t931_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool NeutralResourcesLanguageAttribute_t931_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NeutralResourcesLanguageAttribute_t931_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NeutralResourcesLanguageAttribute_t931_0_0_0;
extern const Il2CppType NeutralResourcesLanguageAttribute_t931_1_0_0;
struct NeutralResourcesLanguageAttribute_t931;
const Il2CppTypeDefinitionMetadata NeutralResourcesLanguageAttribute_t931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NeutralResourcesLanguageAttribute_t931_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, NeutralResourcesLanguageAttribute_t931_VTable/* vtableMethods */
	, NeutralResourcesLanguageAttribute_t931_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1391/* fieldStart */

};
TypeInfo NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NeutralResourcesLanguageAttribute"/* name */
	, "System.Resources"/* namespaze */
	, NeutralResourcesLanguageAttribute_t931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 440/* custom_attributes_cache */
	, &NeutralResourcesLanguageAttribute_t931_0_0_0/* byval_arg */
	, &NeutralResourcesLanguageAttribute_t931_1_0_0/* this_arg */
	, &NeutralResourcesLanguageAttribute_t931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NeutralResourcesLanguageAttribute_t931)/* instance_size */
	, sizeof (NeutralResourcesLanguageAttribute_t931)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// Metadata Definition System.Resources.SatelliteContractVersionAttribute
extern TypeInfo SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo;
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SatelliteContractVersionAttribute_t929_SatelliteContractVersionAttribute__ctor_m3741_ParameterInfos[] = 
{
	{"version", 0, 134221859, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern const MethodInfo SatelliteContractVersionAttribute__ctor_m3741_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SatelliteContractVersionAttribute__ctor_m3741/* method */
	, &SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SatelliteContractVersionAttribute_t929_SatelliteContractVersionAttribute__ctor_m3741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SatelliteContractVersionAttribute_t929_MethodInfos[] =
{
	&SatelliteContractVersionAttribute__ctor_m3741_MethodInfo,
	NULL
};
static const Il2CppMethodReference SatelliteContractVersionAttribute_t929_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool SatelliteContractVersionAttribute_t929_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SatelliteContractVersionAttribute_t929_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SatelliteContractVersionAttribute_t929_0_0_0;
extern const Il2CppType SatelliteContractVersionAttribute_t929_1_0_0;
struct SatelliteContractVersionAttribute_t929;
const Il2CppTypeDefinitionMetadata SatelliteContractVersionAttribute_t929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SatelliteContractVersionAttribute_t929_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, SatelliteContractVersionAttribute_t929_VTable/* vtableMethods */
	, SatelliteContractVersionAttribute_t929_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1392/* fieldStart */

};
TypeInfo SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SatelliteContractVersionAttribute"/* name */
	, "System.Resources"/* namespaze */
	, SatelliteContractVersionAttribute_t929_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 441/* custom_attributes_cache */
	, &SatelliteContractVersionAttribute_t929_0_0_0/* byval_arg */
	, &SatelliteContractVersionAttribute_t929_1_0_0/* this_arg */
	, &SatelliteContractVersionAttribute_t929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SatelliteContractVersionAttribute_t929)/* instance_size */
	, sizeof (SatelliteContractVersionAttribute_t929)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxations
extern TypeInfo CompilationRelaxations_t1592_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0MethodDeclarations.h"
static const MethodInfo* CompilationRelaxations_t1592_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CompilationRelaxations_t1592_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool CompilationRelaxations_t1592_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CompilationRelaxations_t1592_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxations_t1592_0_0_0;
extern const Il2CppType CompilationRelaxations_t1592_1_0_0;
const Il2CppTypeDefinitionMetadata CompilationRelaxations_t1592_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxations_t1592_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, CompilationRelaxations_t1592_VTable/* vtableMethods */
	, CompilationRelaxations_t1592_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1393/* fieldStart */

};
TypeInfo CompilationRelaxations_t1592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxations"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, CompilationRelaxations_t1592_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 442/* custom_attributes_cache */
	, &CompilationRelaxations_t1592_0_0_0/* byval_arg */
	, &CompilationRelaxations_t1592_1_0_0/* this_arg */
	, &CompilationRelaxations_t1592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxations_t1592)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CompilationRelaxations_t1592)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxationsAttribute
extern TypeInfo CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern const Il2CppType CompilationRelaxations_t1592_0_0_0;
static const ParameterInfo CompilationRelaxationsAttribute_t933_CompilationRelaxationsAttribute__ctor_m3745_ParameterInfos[] = 
{
	{"relaxations", 0, 134221860, 0, &CompilationRelaxations_t1592_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Runtime.CompilerServices.CompilationRelaxations)
extern const MethodInfo CompilationRelaxationsAttribute__ctor_m3745_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompilationRelaxationsAttribute__ctor_m3745/* method */
	, &CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CompilationRelaxationsAttribute_t933_CompilationRelaxationsAttribute__ctor_m3745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CompilationRelaxationsAttribute_t933_MethodInfos[] =
{
	&CompilationRelaxationsAttribute__ctor_m3745_MethodInfo,
	NULL
};
static const Il2CppMethodReference CompilationRelaxationsAttribute_t933_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CompilationRelaxationsAttribute_t933_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CompilationRelaxationsAttribute_t933_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxationsAttribute_t933_0_0_0;
extern const Il2CppType CompilationRelaxationsAttribute_t933_1_0_0;
struct CompilationRelaxationsAttribute_t933;
const Il2CppTypeDefinitionMetadata CompilationRelaxationsAttribute_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxationsAttribute_t933_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, CompilationRelaxationsAttribute_t933_VTable/* vtableMethods */
	, CompilationRelaxationsAttribute_t933_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1395/* fieldStart */

};
TypeInfo CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxationsAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, CompilationRelaxationsAttribute_t933_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 443/* custom_attributes_cache */
	, &CompilationRelaxationsAttribute_t933_0_0_0/* byval_arg */
	, &CompilationRelaxationsAttribute_t933_1_0_0/* this_arg */
	, &CompilationRelaxationsAttribute_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxationsAttribute_t933)/* instance_size */
	, sizeof (CompilationRelaxationsAttribute_t933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// Metadata Definition System.Runtime.CompilerServices.DefaultDependencyAttribute
extern TypeInfo DefaultDependencyAttribute_t1593_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
extern const Il2CppType LoadHint_t1595_0_0_0;
extern const Il2CppType LoadHint_t1595_0_0_0;
static const ParameterInfo DefaultDependencyAttribute_t1593_DefaultDependencyAttribute__ctor_m8462_ParameterInfos[] = 
{
	{"loadHintArgument", 0, 134221861, 0, &LoadHint_t1595_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.DefaultDependencyAttribute::.ctor(System.Runtime.CompilerServices.LoadHint)
extern const MethodInfo DefaultDependencyAttribute__ctor_m8462_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultDependencyAttribute__ctor_m8462/* method */
	, &DefaultDependencyAttribute_t1593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, DefaultDependencyAttribute_t1593_DefaultDependencyAttribute__ctor_m8462_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultDependencyAttribute_t1593_MethodInfos[] =
{
	&DefaultDependencyAttribute__ctor_m8462_MethodInfo,
	NULL
};
static const Il2CppMethodReference DefaultDependencyAttribute_t1593_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool DefaultDependencyAttribute_t1593_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultDependencyAttribute_t1593_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultDependencyAttribute_t1593_0_0_0;
extern const Il2CppType DefaultDependencyAttribute_t1593_1_0_0;
struct DefaultDependencyAttribute_t1593;
const Il2CppTypeDefinitionMetadata DefaultDependencyAttribute_t1593_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultDependencyAttribute_t1593_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, DefaultDependencyAttribute_t1593_VTable/* vtableMethods */
	, DefaultDependencyAttribute_t1593_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1396/* fieldStart */

};
TypeInfo DefaultDependencyAttribute_t1593_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultDependencyAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, DefaultDependencyAttribute_t1593_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultDependencyAttribute_t1593_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 444/* custom_attributes_cache */
	, &DefaultDependencyAttribute_t1593_0_0_0/* byval_arg */
	, &DefaultDependencyAttribute_t1593_1_0_0/* this_arg */
	, &DefaultDependencyAttribute_t1593_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultDependencyAttribute_t1593)/* instance_size */
	, sizeof (DefaultDependencyAttribute_t1593)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile.h"
// Metadata Definition System.Runtime.CompilerServices.IsVolatile
extern TypeInfo IsVolatile_t1594_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatileMethodDeclarations.h"
static const MethodInfo* IsVolatile_t1594_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference IsVolatile_t1594_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool IsVolatile_t1594_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IsVolatile_t1594_0_0_0;
extern const Il2CppType IsVolatile_t1594_1_0_0;
struct IsVolatile_t1594;
const Il2CppTypeDefinitionMetadata IsVolatile_t1594_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IsVolatile_t1594_VTable/* vtableMethods */
	, IsVolatile_t1594_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IsVolatile_t1594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IsVolatile"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, IsVolatile_t1594_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IsVolatile_t1594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 445/* custom_attributes_cache */
	, &IsVolatile_t1594_0_0_0/* byval_arg */
	, &IsVolatile_t1594_1_0_0/* this_arg */
	, &IsVolatile_t1594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IsVolatile_t1594)/* instance_size */
	, sizeof (IsVolatile_t1594)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"
// Metadata Definition System.Runtime.CompilerServices.LoadHint
extern TypeInfo LoadHint_t1595_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHintMethodDeclarations.h"
static const MethodInfo* LoadHint_t1595_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LoadHint_t1595_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool LoadHint_t1595_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LoadHint_t1595_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoadHint_t1595_1_0_0;
const Il2CppTypeDefinitionMetadata LoadHint_t1595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoadHint_t1595_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, LoadHint_t1595_VTable/* vtableMethods */
	, LoadHint_t1595_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1397/* fieldStart */

};
TypeInfo LoadHint_t1595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoadHint"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, LoadHint_t1595_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoadHint_t1595_0_0_0/* byval_arg */
	, &LoadHint_t1595_1_0_0/* this_arg */
	, &LoadHint_t1595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoadHint_t1595)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoadHint_t1595)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// Metadata Definition System.Runtime.CompilerServices.StringFreezingAttribute
extern TypeInfo StringFreezingAttribute_t1596_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
extern const MethodInfo StringFreezingAttribute__ctor_m8463_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringFreezingAttribute__ctor_m8463/* method */
	, &StringFreezingAttribute_t1596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringFreezingAttribute_t1596_MethodInfos[] =
{
	&StringFreezingAttribute__ctor_m8463_MethodInfo,
	NULL
};
static const Il2CppMethodReference StringFreezingAttribute_t1596_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool StringFreezingAttribute_t1596_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringFreezingAttribute_t1596_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringFreezingAttribute_t1596_0_0_0;
extern const Il2CppType StringFreezingAttribute_t1596_1_0_0;
struct StringFreezingAttribute_t1596;
const Il2CppTypeDefinitionMetadata StringFreezingAttribute_t1596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringFreezingAttribute_t1596_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, StringFreezingAttribute_t1596_VTable/* vtableMethods */
	, StringFreezingAttribute_t1596_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StringFreezingAttribute_t1596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringFreezingAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, StringFreezingAttribute_t1596_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringFreezingAttribute_t1596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 446/* custom_attributes_cache */
	, &StringFreezingAttribute_t1596_0_0_0/* byval_arg */
	, &StringFreezingAttribute_t1596_1_0_0/* this_arg */
	, &StringFreezingAttribute_t1596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringFreezingAttribute_t1596)/* instance_size */
	, sizeof (StringFreezingAttribute_t1596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Cer
extern TypeInfo Cer_t1597_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_CerMethodDeclarations.h"
static const MethodInfo* Cer_t1597_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Cer_t1597_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Cer_t1597_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Cer_t1597_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Cer_t1597_0_0_0;
extern const Il2CppType Cer_t1597_1_0_0;
const Il2CppTypeDefinitionMetadata Cer_t1597_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Cer_t1597_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Cer_t1597_VTable/* vtableMethods */
	, Cer_t1597_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1401/* fieldStart */

};
TypeInfo Cer_t1597_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Cer"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, Cer_t1597_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Cer_t1597_0_0_0/* byval_arg */
	, &Cer_t1597_1_0_0/* this_arg */
	, &Cer_t1597_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Cer_t1597)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Cer_t1597)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Consistency
extern TypeInfo Consistency_t1598_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_ConsistencyMethodDeclarations.h"
static const MethodInfo* Consistency_t1598_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Consistency_t1598_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Consistency_t1598_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Consistency_t1598_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Consistency_t1598_0_0_0;
extern const Il2CppType Consistency_t1598_1_0_0;
const Il2CppTypeDefinitionMetadata Consistency_t1598_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Consistency_t1598_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Consistency_t1598_VTable/* vtableMethods */
	, Consistency_t1598_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1405/* fieldStart */

};
TypeInfo Consistency_t1598_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Consistency"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, Consistency_t1598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Consistency_t1598_0_0_0/* byval_arg */
	, &Consistency_t1598_1_0_0/* this_arg */
	, &Consistency_t1598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Consistency_t1598)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Consistency_t1598)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
// Metadata Definition System.Runtime.ConstrainedExecution.CriticalFinalizerObject
extern TypeInfo CriticalFinalizerObject_t1599_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinalizMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
extern const MethodInfo CriticalFinalizerObject__ctor_m8464_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CriticalFinalizerObject__ctor_m8464/* method */
	, &CriticalFinalizerObject_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 448/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
extern const MethodInfo CriticalFinalizerObject_Finalize_m8465_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&CriticalFinalizerObject_Finalize_m8465/* method */
	, &CriticalFinalizerObject_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 449/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CriticalFinalizerObject_t1599_MethodInfos[] =
{
	&CriticalFinalizerObject__ctor_m8464_MethodInfo,
	&CriticalFinalizerObject_Finalize_m8465_MethodInfo,
	NULL
};
extern const MethodInfo CriticalFinalizerObject_Finalize_m8465_MethodInfo;
static const Il2CppMethodReference CriticalFinalizerObject_t1599_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&CriticalFinalizerObject_Finalize_m8465_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CriticalFinalizerObject_t1599_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CriticalFinalizerObject_t1599_0_0_0;
extern const Il2CppType CriticalFinalizerObject_t1599_1_0_0;
struct CriticalFinalizerObject_t1599;
const Il2CppTypeDefinitionMetadata CriticalFinalizerObject_t1599_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CriticalFinalizerObject_t1599_VTable/* vtableMethods */
	, CriticalFinalizerObject_t1599_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CriticalFinalizerObject_t1599_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CriticalFinalizerObject"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, CriticalFinalizerObject_t1599_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CriticalFinalizerObject_t1599_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 447/* custom_attributes_cache */
	, &CriticalFinalizerObject_t1599_0_0_0/* byval_arg */
	, &CriticalFinalizerObject_t1599_1_0_0/* this_arg */
	, &CriticalFinalizerObject_t1599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CriticalFinalizerObject_t1599)/* instance_size */
	, sizeof (CriticalFinalizerObject_t1599)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// Metadata Definition System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
extern TypeInfo ReliabilityContractAttribute_t1600_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern const Il2CppType Consistency_t1598_0_0_0;
extern const Il2CppType Cer_t1597_0_0_0;
static const ParameterInfo ReliabilityContractAttribute_t1600_ReliabilityContractAttribute__ctor_m8466_ParameterInfos[] = 
{
	{"consistencyGuarantee", 0, 134221862, 0, &Consistency_t1598_0_0_0},
	{"cer", 1, 134221863, 0, &Cer_t1597_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.ConstrainedExecution.ReliabilityContractAttribute::.ctor(System.Runtime.ConstrainedExecution.Consistency,System.Runtime.ConstrainedExecution.Cer)
extern const MethodInfo ReliabilityContractAttribute__ctor_m8466_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReliabilityContractAttribute__ctor_m8466/* method */
	, &ReliabilityContractAttribute_t1600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, ReliabilityContractAttribute_t1600_ReliabilityContractAttribute__ctor_m8466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReliabilityContractAttribute_t1600_MethodInfos[] =
{
	&ReliabilityContractAttribute__ctor_m8466_MethodInfo,
	NULL
};
static const Il2CppMethodReference ReliabilityContractAttribute_t1600_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ReliabilityContractAttribute_t1600_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReliabilityContractAttribute_t1600_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReliabilityContractAttribute_t1600_0_0_0;
extern const Il2CppType ReliabilityContractAttribute_t1600_1_0_0;
struct ReliabilityContractAttribute_t1600;
const Il2CppTypeDefinitionMetadata ReliabilityContractAttribute_t1600_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReliabilityContractAttribute_t1600_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ReliabilityContractAttribute_t1600_VTable/* vtableMethods */
	, ReliabilityContractAttribute_t1600_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1410/* fieldStart */

};
TypeInfo ReliabilityContractAttribute_t1600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReliabilityContractAttribute"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, ReliabilityContractAttribute_t1600_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReliabilityContractAttribute_t1600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 450/* custom_attributes_cache */
	, &ReliabilityContractAttribute_t1600_0_0_0/* byval_arg */
	, &ReliabilityContractAttribute_t1600_1_0_0/* this_arg */
	, &ReliabilityContractAttribute_t1600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReliabilityContractAttribute_t1600)/* instance_size */
	, sizeof (ReliabilityContractAttribute_t1600)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArguments.h"
// Metadata Definition System.Runtime.Hosting.ActivationArguments
extern TypeInfo ActivationArguments_t1601_il2cpp_TypeInfo;
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArgumentsMethodDeclarations.h"
static const MethodInfo* ActivationArguments_t1601_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ActivationArguments_t1601_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ActivationArguments_t1601_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationArguments_t1601_0_0_0;
extern const Il2CppType ActivationArguments_t1601_1_0_0;
struct ActivationArguments_t1601;
const Il2CppTypeDefinitionMetadata ActivationArguments_t1601_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationArguments_t1601_VTable/* vtableMethods */
	, ActivationArguments_t1601_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ActivationArguments_t1601_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationArguments"/* name */
	, "System.Runtime.Hosting"/* namespaze */
	, ActivationArguments_t1601_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ActivationArguments_t1601_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 451/* custom_attributes_cache */
	, &ActivationArguments_t1601_0_0_0/* byval_arg */
	, &ActivationArguments_t1601_1_0_0/* this_arg */
	, &ActivationArguments_t1601_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationArguments_t1601)/* instance_size */
	, sizeof (ActivationArguments_t1601)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
// Metadata Definition System.Runtime.InteropServices.CallingConvention
extern TypeInfo CallingConvention_t1602_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConventionMethodDeclarations.h"
static const MethodInfo* CallingConvention_t1602_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CallingConvention_t1602_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool CallingConvention_t1602_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CallingConvention_t1602_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConvention_t1602_0_0_0;
extern const Il2CppType CallingConvention_t1602_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConvention_t1602_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConvention_t1602_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, CallingConvention_t1602_VTable/* vtableMethods */
	, CallingConvention_t1602_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1412/* fieldStart */

};
TypeInfo CallingConvention_t1602_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConvention"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, CallingConvention_t1602_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 452/* custom_attributes_cache */
	, &CallingConvention_t1602_0_0_0/* byval_arg */
	, &CallingConvention_t1602_1_0_0/* this_arg */
	, &CallingConvention_t1602_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConvention_t1602)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConvention_t1602)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
// Metadata Definition System.Runtime.InteropServices.CharSet
extern TypeInfo CharSet_t1603_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSetMethodDeclarations.h"
static const MethodInfo* CharSet_t1603_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CharSet_t1603_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool CharSet_t1603_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CharSet_t1603_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CharSet_t1603_0_0_0;
extern const Il2CppType CharSet_t1603_1_0_0;
const Il2CppTypeDefinitionMetadata CharSet_t1603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CharSet_t1603_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, CharSet_t1603_VTable/* vtableMethods */
	, CharSet_t1603_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1418/* fieldStart */

};
TypeInfo CharSet_t1603_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharSet"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, CharSet_t1603_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 453/* custom_attributes_cache */
	, &CharSet_t1603_0_0_0/* byval_arg */
	, &CharSet_t1603_1_0_0/* this_arg */
	, &CharSet_t1603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharSet_t1603)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharSet_t1603)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceAttribute
extern TypeInfo ClassInterfaceAttribute_t1604_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern const Il2CppType ClassInterfaceType_t1605_0_0_0;
extern const Il2CppType ClassInterfaceType_t1605_0_0_0;
static const ParameterInfo ClassInterfaceAttribute_t1604_ClassInterfaceAttribute__ctor_m8467_ParameterInfos[] = 
{
	{"classInterfaceType", 0, 134221864, 0, &ClassInterfaceType_t1605_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern const MethodInfo ClassInterfaceAttribute__ctor_m8467_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClassInterfaceAttribute__ctor_m8467/* method */
	, &ClassInterfaceAttribute_t1604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ClassInterfaceAttribute_t1604_ClassInterfaceAttribute__ctor_m8467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClassInterfaceAttribute_t1604_MethodInfos[] =
{
	&ClassInterfaceAttribute__ctor_m8467_MethodInfo,
	NULL
};
static const Il2CppMethodReference ClassInterfaceAttribute_t1604_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ClassInterfaceAttribute_t1604_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ClassInterfaceAttribute_t1604_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceAttribute_t1604_0_0_0;
extern const Il2CppType ClassInterfaceAttribute_t1604_1_0_0;
struct ClassInterfaceAttribute_t1604;
const Il2CppTypeDefinitionMetadata ClassInterfaceAttribute_t1604_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceAttribute_t1604_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ClassInterfaceAttribute_t1604_VTable/* vtableMethods */
	, ClassInterfaceAttribute_t1604_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1423/* fieldStart */

};
TypeInfo ClassInterfaceAttribute_t1604_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ClassInterfaceAttribute_t1604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ClassInterfaceAttribute_t1604_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 454/* custom_attributes_cache */
	, &ClassInterfaceAttribute_t1604_0_0_0/* byval_arg */
	, &ClassInterfaceAttribute_t1604_1_0_0/* this_arg */
	, &ClassInterfaceAttribute_t1604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceAttribute_t1604)/* instance_size */
	, sizeof (ClassInterfaceAttribute_t1604)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceType
extern TypeInfo ClassInterfaceType_t1605_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceTypeMethodDeclarations.h"
static const MethodInfo* ClassInterfaceType_t1605_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ClassInterfaceType_t1605_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ClassInterfaceType_t1605_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ClassInterfaceType_t1605_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceType_t1605_1_0_0;
const Il2CppTypeDefinitionMetadata ClassInterfaceType_t1605_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceType_t1605_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ClassInterfaceType_t1605_VTable/* vtableMethods */
	, ClassInterfaceType_t1605_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1424/* fieldStart */

};
TypeInfo ClassInterfaceType_t1605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ClassInterfaceType_t1605_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 455/* custom_attributes_cache */
	, &ClassInterfaceType_t1605_0_0_0/* byval_arg */
	, &ClassInterfaceType_t1605_1_0_0/* this_arg */
	, &ClassInterfaceType_t1605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceType_t1605)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ClassInterfaceType_t1605)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// Metadata Definition System.Runtime.InteropServices.ComDefaultInterfaceAttribute
extern TypeInfo ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ComDefaultInterfaceAttribute_t1606_ComDefaultInterfaceAttribute__ctor_m8468_ParameterInfos[] = 
{
	{"defaultInterface", 0, 134221865, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.ComDefaultInterfaceAttribute::.ctor(System.Type)
extern const MethodInfo ComDefaultInterfaceAttribute__ctor_m8468_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ComDefaultInterfaceAttribute__ctor_m8468/* method */
	, &ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ComDefaultInterfaceAttribute_t1606_ComDefaultInterfaceAttribute__ctor_m8468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComDefaultInterfaceAttribute_t1606_MethodInfos[] =
{
	&ComDefaultInterfaceAttribute__ctor_m8468_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComDefaultInterfaceAttribute_t1606_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ComDefaultInterfaceAttribute_t1606_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ComDefaultInterfaceAttribute_t1606_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComDefaultInterfaceAttribute_t1606_0_0_0;
extern const Il2CppType ComDefaultInterfaceAttribute_t1606_1_0_0;
struct ComDefaultInterfaceAttribute_t1606;
const Il2CppTypeDefinitionMetadata ComDefaultInterfaceAttribute_t1606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComDefaultInterfaceAttribute_t1606_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ComDefaultInterfaceAttribute_t1606_VTable/* vtableMethods */
	, ComDefaultInterfaceAttribute_t1606_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1428/* fieldStart */

};
TypeInfo ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComDefaultInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ComDefaultInterfaceAttribute_t1606_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComDefaultInterfaceAttribute_t1606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 456/* custom_attributes_cache */
	, &ComDefaultInterfaceAttribute_t1606_0_0_0/* byval_arg */
	, &ComDefaultInterfaceAttribute_t1606_1_0_0/* this_arg */
	, &ComDefaultInterfaceAttribute_t1606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComDefaultInterfaceAttribute_t1606)/* instance_size */
	, sizeof (ComDefaultInterfaceAttribute_t1606)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ComInterfaceType
extern TypeInfo ComInterfaceType_t1607_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceTypeMethodDeclarations.h"
static const MethodInfo* ComInterfaceType_t1607_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ComInterfaceType_t1607_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ComInterfaceType_t1607_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ComInterfaceType_t1607_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComInterfaceType_t1607_0_0_0;
extern const Il2CppType ComInterfaceType_t1607_1_0_0;
const Il2CppTypeDefinitionMetadata ComInterfaceType_t1607_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComInterfaceType_t1607_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ComInterfaceType_t1607_VTable/* vtableMethods */
	, ComInterfaceType_t1607_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1429/* fieldStart */

};
TypeInfo ComInterfaceType_t1607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, ComInterfaceType_t1607_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 457/* custom_attributes_cache */
	, &ComInterfaceType_t1607_0_0_0/* byval_arg */
	, &ComInterfaceType_t1607_1_0_0/* this_arg */
	, &ComInterfaceType_t1607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComInterfaceType_t1607)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ComInterfaceType_t1607)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// Metadata Definition System.Runtime.InteropServices.DispIdAttribute
extern TypeInfo DispIdAttribute_t1608_il2cpp_TypeInfo;
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo DispIdAttribute_t1608_DispIdAttribute__ctor_m8469_ParameterInfos[] = 
{
	{"dispId", 0, 134221866, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.DispIdAttribute::.ctor(System.Int32)
extern const MethodInfo DispIdAttribute__ctor_m8469_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DispIdAttribute__ctor_m8469/* method */
	, &DispIdAttribute_t1608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, DispIdAttribute_t1608_DispIdAttribute__ctor_m8469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DispIdAttribute_t1608_MethodInfos[] =
{
	&DispIdAttribute__ctor_m8469_MethodInfo,
	NULL
};
static const Il2CppMethodReference DispIdAttribute_t1608_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool DispIdAttribute_t1608_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DispIdAttribute_t1608_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DispIdAttribute_t1608_0_0_0;
extern const Il2CppType DispIdAttribute_t1608_1_0_0;
struct DispIdAttribute_t1608;
const Il2CppTypeDefinitionMetadata DispIdAttribute_t1608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DispIdAttribute_t1608_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, DispIdAttribute_t1608_VTable/* vtableMethods */
	, DispIdAttribute_t1608_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1433/* fieldStart */

};
TypeInfo DispIdAttribute_t1608_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DispIdAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, DispIdAttribute_t1608_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DispIdAttribute_t1608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 458/* custom_attributes_cache */
	, &DispIdAttribute_t1608_0_0_0/* byval_arg */
	, &DispIdAttribute_t1608_1_0_0/* this_arg */
	, &DispIdAttribute_t1608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DispIdAttribute_t1608)/* instance_size */
	, sizeof (DispIdAttribute_t1608)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// Metadata Definition System.Runtime.InteropServices.GCHandle
extern TypeInfo GCHandle_t1609_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t1610_0_0_0;
extern const Il2CppType GCHandleType_t1610_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle__ctor_m8470_ParameterInfos[] = 
{
	{"value", 0, 134221867, 0, &Object_t_0_0_0},
	{"type", 1, 134221868, 0, &GCHandleType_t1610_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle__ctor_m8470_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GCHandle__ctor_m8470/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, GCHandle_t1609_GCHandle__ctor_m8470_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern const MethodInfo GCHandle_get_IsAllocated_m8471_MethodInfo = 
{
	"get_IsAllocated"/* name */
	, (methodPointerType)&GCHandle_get_IsAllocated_m8471/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern const MethodInfo GCHandle_get_Target_m8472_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&GCHandle_get_Target_m8472/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType GCHandleType_t1610_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle_Alloc_m8473_ParameterInfos[] = 
{
	{"value", 0, 134221869, 0, &Object_t_0_0_0},
	{"type", 1, 134221870, 0, &GCHandleType_t1610_0_0_0},
};
extern const Il2CppType GCHandle_t1609_0_0_0;
extern void* RuntimeInvoker_GCHandle_t1609_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_Alloc_m8473_MethodInfo = 
{
	"Alloc"/* name */
	, (methodPointerType)&GCHandle_Alloc_m8473/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &GCHandle_t1609_0_0_0/* return_type */
	, RuntimeInvoker_GCHandle_t1609_Object_t_Int32_t54/* invoker_method */
	, GCHandle_t1609_GCHandle_Alloc_m8473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern const MethodInfo GCHandle_Free_m8474_MethodInfo = 
{
	"Free"/* name */
	, (methodPointerType)&GCHandle_Free_m8474/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle_GetTarget_m8475_ParameterInfos[] = 
{
	{"handle", 0, 134221871, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)
extern const MethodInfo GCHandle_GetTarget_m8475_MethodInfo = 
{
	"GetTarget"/* name */
	, (methodPointerType)&GCHandle_GetTarget_m8475/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, GCHandle_t1609_GCHandle_GetTarget_m8475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType GCHandleType_t1610_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle_GetTargetHandle_m8476_ParameterInfos[] = 
{
	{"obj", 0, 134221872, 0, &Object_t_0_0_0},
	{"handle", 1, 134221873, 0, &Int32_t54_0_0_0},
	{"type", 2, 134221874, 0, &GCHandleType_t1610_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)
extern const MethodInfo GCHandle_GetTargetHandle_m8476_MethodInfo = 
{
	"GetTargetHandle"/* name */
	, (methodPointerType)&GCHandle_GetTargetHandle_m8476/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, GCHandle_t1609_GCHandle_GetTargetHandle_m8476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle_FreeHandle_m8477_ParameterInfos[] = 
{
	{"handle", 0, 134221875, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)
extern const MethodInfo GCHandle_FreeHandle_m8477_MethodInfo = 
{
	"FreeHandle"/* name */
	, (methodPointerType)&GCHandle_FreeHandle_m8477/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, GCHandle_t1609_GCHandle_FreeHandle_m8477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GCHandle_t1609_GCHandle_Equals_m8478_ParameterInfos[] = 
{
	{"o", 0, 134221876, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.GCHandle::Equals(System.Object)
extern const MethodInfo GCHandle_Equals_m8478_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&GCHandle_Equals_m8478/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, GCHandle_t1609_GCHandle_Equals_m8478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.InteropServices.GCHandle::GetHashCode()
extern const MethodInfo GCHandle_GetHashCode_m8479_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&GCHandle_GetHashCode_m8479/* method */
	, &GCHandle_t1609_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GCHandle_t1609_MethodInfos[] =
{
	&GCHandle__ctor_m8470_MethodInfo,
	&GCHandle_get_IsAllocated_m8471_MethodInfo,
	&GCHandle_get_Target_m8472_MethodInfo,
	&GCHandle_Alloc_m8473_MethodInfo,
	&GCHandle_Free_m8474_MethodInfo,
	&GCHandle_GetTarget_m8475_MethodInfo,
	&GCHandle_GetTargetHandle_m8476_MethodInfo,
	&GCHandle_FreeHandle_m8477_MethodInfo,
	&GCHandle_Equals_m8478_MethodInfo,
	&GCHandle_GetHashCode_m8479_MethodInfo,
	NULL
};
extern const MethodInfo GCHandle_get_IsAllocated_m8471_MethodInfo;
static const PropertyInfo GCHandle_t1609____IsAllocated_PropertyInfo = 
{
	&GCHandle_t1609_il2cpp_TypeInfo/* parent */
	, "IsAllocated"/* name */
	, &GCHandle_get_IsAllocated_m8471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GCHandle_get_Target_m8472_MethodInfo;
static const PropertyInfo GCHandle_t1609____Target_PropertyInfo = 
{
	&GCHandle_t1609_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &GCHandle_get_Target_m8472_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GCHandle_t1609_PropertyInfos[] =
{
	&GCHandle_t1609____IsAllocated_PropertyInfo,
	&GCHandle_t1609____Target_PropertyInfo,
	NULL
};
extern const MethodInfo GCHandle_Equals_m8478_MethodInfo;
extern const MethodInfo GCHandle_GetHashCode_m8479_MethodInfo;
static const Il2CppMethodReference GCHandle_t1609_VTable[] =
{
	&GCHandle_Equals_m8478_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&GCHandle_GetHashCode_m8479_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool GCHandle_t1609_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandle_t1609_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandle_t1609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, GCHandle_t1609_VTable/* vtableMethods */
	, GCHandle_t1609_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1434/* fieldStart */

};
TypeInfo GCHandle_t1609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandle_t1609_MethodInfos/* methods */
	, GCHandle_t1609_PropertyInfos/* properties */
	, NULL/* events */
	, &GCHandle_t1609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 459/* custom_attributes_cache */
	, &GCHandle_t1609_0_0_0/* byval_arg */
	, &GCHandle_t1609_1_0_0/* this_arg */
	, &GCHandle_t1609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandle_t1609)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandle_t1609)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GCHandle_t1609 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// Metadata Definition System.Runtime.InteropServices.GCHandleType
extern TypeInfo GCHandleType_t1610_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleTypeMethodDeclarations.h"
static const MethodInfo* GCHandleType_t1610_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference GCHandleType_t1610_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool GCHandleType_t1610_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair GCHandleType_t1610_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandleType_t1610_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandleType_t1610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GCHandleType_t1610_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, GCHandleType_t1610_VTable/* vtableMethods */
	, GCHandleType_t1610_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1435/* fieldStart */

};
TypeInfo GCHandleType_t1610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandleType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, GCHandleType_t1610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 460/* custom_attributes_cache */
	, &GCHandleType_t1610_0_0_0/* byval_arg */
	, &GCHandleType_t1610_1_0_0/* this_arg */
	, &GCHandleType_t1610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandleType_t1610)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandleType_t1610)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute
extern TypeInfo InterfaceTypeAttribute_t1611_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
extern const Il2CppType ComInterfaceType_t1607_0_0_0;
static const ParameterInfo InterfaceTypeAttribute_t1611_InterfaceTypeAttribute__ctor_m8480_ParameterInfos[] = 
{
	{"interfaceType", 0, 134221877, 0, &ComInterfaceType_t1607_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern const MethodInfo InterfaceTypeAttribute__ctor_m8480_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterfaceTypeAttribute__ctor_m8480/* method */
	, &InterfaceTypeAttribute_t1611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, InterfaceTypeAttribute_t1611_InterfaceTypeAttribute__ctor_m8480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterfaceTypeAttribute_t1611_MethodInfos[] =
{
	&InterfaceTypeAttribute__ctor_m8480_MethodInfo,
	NULL
};
static const Il2CppMethodReference InterfaceTypeAttribute_t1611_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool InterfaceTypeAttribute_t1611_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InterfaceTypeAttribute_t1611_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InterfaceTypeAttribute_t1611_0_0_0;
extern const Il2CppType InterfaceTypeAttribute_t1611_1_0_0;
struct InterfaceTypeAttribute_t1611;
const Il2CppTypeDefinitionMetadata InterfaceTypeAttribute_t1611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InterfaceTypeAttribute_t1611_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, InterfaceTypeAttribute_t1611_VTable/* vtableMethods */
	, InterfaceTypeAttribute_t1611_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1440/* fieldStart */

};
TypeInfo InterfaceTypeAttribute_t1611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterfaceTypeAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, InterfaceTypeAttribute_t1611_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InterfaceTypeAttribute_t1611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 461/* custom_attributes_cache */
	, &InterfaceTypeAttribute_t1611_0_0_0/* byval_arg */
	, &InterfaceTypeAttribute_t1611_1_0_0/* this_arg */
	, &InterfaceTypeAttribute_t1611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterfaceTypeAttribute_t1611)/* instance_size */
	, sizeof (InterfaceTypeAttribute_t1611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
// Metadata Definition System.Runtime.InteropServices.Marshal
extern TypeInfo Marshal_t1612_il2cpp_TypeInfo;
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern const MethodInfo Marshal__cctor_m8481_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Marshal__cctor_m8481/* method */
	, &Marshal_t1612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Marshal_t1612_Marshal_copy_from_unmanaged_m8482_ParameterInfos[] = 
{
	{"source", 0, 134221878, 0, &IntPtr_t_0_0_0},
	{"startIndex", 1, 134221879, 0, &Int32_t54_0_0_0},
	{"destination", 2, 134221880, 0, &Array_t_0_0_0},
	{"length", 3, 134221881, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern const MethodInfo Marshal_copy_from_unmanaged_m8482_MethodInfo = 
{
	"copy_from_unmanaged"/* name */
	, (methodPointerType)&Marshal_copy_from_unmanaged_m8482/* method */
	, &Marshal_t1612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t_Int32_t54_Object_t_Int32_t54/* invoker_method */
	, Marshal_t1612_Marshal_copy_from_unmanaged_m8482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType CharU5BU5D_t222_0_0_0;
extern const Il2CppType CharU5BU5D_t222_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Marshal_t1612_Marshal_Copy_m8483_ParameterInfos[] = 
{
	{"source", 0, 134221882, 0, &IntPtr_t_0_0_0},
	{"destination", 1, 134221883, 0, &CharU5BU5D_t222_0_0_0},
	{"startIndex", 2, 134221884, 0, &Int32_t54_0_0_0},
	{"length", 3, 134221885, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern const MethodInfo Marshal_Copy_m8483_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&Marshal_Copy_m8483/* method */
	, &Marshal_t1612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, Marshal_t1612_Marshal_Copy_m8483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Marshal_t1612_MethodInfos[] =
{
	&Marshal__cctor_m8481_MethodInfo,
	&Marshal_copy_from_unmanaged_m8482_MethodInfo,
	&Marshal_Copy_m8483_MethodInfo,
	NULL
};
static const Il2CppMethodReference Marshal_t1612_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Marshal_t1612_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Marshal_t1612_0_0_0;
extern const Il2CppType Marshal_t1612_1_0_0;
struct Marshal_t1612;
const Il2CppTypeDefinitionMetadata Marshal_t1612_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Marshal_t1612_VTable/* vtableMethods */
	, Marshal_t1612_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1441/* fieldStart */

};
TypeInfo Marshal_t1612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Marshal"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, Marshal_t1612_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Marshal_t1612_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 462/* custom_attributes_cache */
	, &Marshal_t1612_0_0_0/* byval_arg */
	, &Marshal_t1612_1_0_0/* this_arg */
	, &Marshal_t1612_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Marshal_t1612)/* instance_size */
	, sizeof (Marshal_t1612)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Marshal_t1612_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 262529/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
// Metadata Definition System.Runtime.InteropServices.MarshalDirectiveException
extern TypeInfo MarshalDirectiveException_t1613_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern const MethodInfo MarshalDirectiveException__ctor_m8484_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m8484/* method */
	, &MarshalDirectiveException_t1613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MarshalDirectiveException_t1613_MarshalDirectiveException__ctor_m8485_ParameterInfos[] = 
{
	{"info", 0, 134221886, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221887, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MarshalDirectiveException__ctor_m8485_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarshalDirectiveException__ctor_m8485/* method */
	, &MarshalDirectiveException_t1613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MarshalDirectiveException_t1613_MarshalDirectiveException__ctor_m8485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MarshalDirectiveException_t1613_MethodInfos[] =
{
	&MarshalDirectiveException__ctor_m8484_MethodInfo,
	&MarshalDirectiveException__ctor_m8485_MethodInfo,
	NULL
};
static const Il2CppMethodReference MarshalDirectiveException_t1613_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool MarshalDirectiveException_t1613_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MarshalDirectiveException_t1613_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalDirectiveException_t1613_0_0_0;
extern const Il2CppType MarshalDirectiveException_t1613_1_0_0;
extern const Il2CppType SystemException_t1181_0_0_0;
struct MarshalDirectiveException_t1613;
const Il2CppTypeDefinitionMetadata MarshalDirectiveException_t1613_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalDirectiveException_t1613_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, MarshalDirectiveException_t1613_VTable/* vtableMethods */
	, MarshalDirectiveException_t1613_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1443/* fieldStart */

};
TypeInfo MarshalDirectiveException_t1613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalDirectiveException"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, MarshalDirectiveException_t1613_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MarshalDirectiveException_t1613_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 463/* custom_attributes_cache */
	, &MarshalDirectiveException_t1613_0_0_0/* byval_arg */
	, &MarshalDirectiveException_t1613_1_0_0/* this_arg */
	, &MarshalDirectiveException_t1613_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalDirectiveException_t1613)/* instance_size */
	, sizeof (MarshalDirectiveException_t1613)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute
extern TypeInfo PreserveSigAttribute_t1614_il2cpp_TypeInfo;
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern const MethodInfo PreserveSigAttribute__ctor_m8486_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PreserveSigAttribute__ctor_m8486/* method */
	, &PreserveSigAttribute_t1614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PreserveSigAttribute_t1614_MethodInfos[] =
{
	&PreserveSigAttribute__ctor_m8486_MethodInfo,
	NULL
};
static const Il2CppMethodReference PreserveSigAttribute_t1614_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool PreserveSigAttribute_t1614_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PreserveSigAttribute_t1614_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreserveSigAttribute_t1614_0_0_0;
extern const Il2CppType PreserveSigAttribute_t1614_1_0_0;
struct PreserveSigAttribute_t1614;
const Il2CppTypeDefinitionMetadata PreserveSigAttribute_t1614_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PreserveSigAttribute_t1614_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, PreserveSigAttribute_t1614_VTable/* vtableMethods */
	, PreserveSigAttribute_t1614_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PreserveSigAttribute_t1614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreserveSigAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, PreserveSigAttribute_t1614_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PreserveSigAttribute_t1614_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 464/* custom_attributes_cache */
	, &PreserveSigAttribute_t1614_0_0_0/* byval_arg */
	, &PreserveSigAttribute_t1614_1_0_0/* this_arg */
	, &PreserveSigAttribute_t1614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PreserveSigAttribute_t1614)/* instance_size */
	, sizeof (PreserveSigAttribute_t1614)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
// Metadata Definition System.Runtime.InteropServices.SafeHandle
extern TypeInfo SafeHandle_t1390_il2cpp_TypeInfo;
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo SafeHandle_t1390_SafeHandle__ctor_m8487_ParameterInfos[] = 
{
	{"invalidHandleValue", 0, 134221888, 0, &IntPtr_t_0_0_0},
	{"ownsHandle", 1, 134221889, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::.ctor(System.IntPtr,System.Boolean)
extern const MethodInfo SafeHandle__ctor_m8487_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SafeHandle__ctor_m8487/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t_SByte_t73/* invoker_method */
	, SafeHandle_t1390_SafeHandle__ctor_m8487_ParameterInfos/* parameters */
	, 465/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Close()
extern const MethodInfo SafeHandle_Close_m8488_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&SafeHandle_Close_m8488/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 466/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_1_0_0;
extern const Il2CppType Boolean_t72_1_0_0;
static const ParameterInfo SafeHandle_t1390_SafeHandle_DangerousAddRef_m8489_ParameterInfos[] = 
{
	{"success", 0, 134221890, 0, &Boolean_t72_1_0_0},
};
extern void* RuntimeInvoker_Void_t71_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
extern const MethodInfo SafeHandle_DangerousAddRef_m8489_MethodInfo = 
{
	"DangerousAddRef"/* name */
	, (methodPointerType)&SafeHandle_DangerousAddRef_m8489/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_BooleanU26_t441/* invoker_method */
	, SafeHandle_t1390_SafeHandle_DangerousAddRef_m8489_ParameterInfos/* parameters */
	, 467/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern const MethodInfo SafeHandle_DangerousGetHandle_m8490_MethodInfo = 
{
	"DangerousGetHandle"/* name */
	, (methodPointerType)&SafeHandle_DangerousGetHandle_m8490/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 468/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
extern const MethodInfo SafeHandle_DangerousRelease_m8491_MethodInfo = 
{
	"DangerousRelease"/* name */
	, (methodPointerType)&SafeHandle_DangerousRelease_m8491/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 469/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern const MethodInfo SafeHandle_Dispose_m8492_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m8492/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 470/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo SafeHandle_t1390_SafeHandle_Dispose_m8493_ParameterInfos[] = 
{
	{"disposing", 0, 134221891, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose(System.Boolean)
extern const MethodInfo SafeHandle_Dispose_m8493_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&SafeHandle_Dispose_m8493/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, SafeHandle_t1390_SafeHandle_Dispose_m8493_ParameterInfos/* parameters */
	, 471/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle()
extern const MethodInfo SafeHandle_ReleaseHandle_m10954_MethodInfo = 
{
	"ReleaseHandle"/* name */
	, NULL/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 472/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SafeHandle_t1390_SafeHandle_SetHandle_m8494_ParameterInfos[] = 
{
	{"handle", 0, 134221892, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandle(System.IntPtr)
extern const MethodInfo SafeHandle_SetHandle_m8494_MethodInfo = 
{
	"SetHandle"/* name */
	, (methodPointerType)&SafeHandle_SetHandle_m8494/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_IntPtr_t/* invoker_method */
	, SafeHandle_t1390_SafeHandle_SetHandle_m8494_ParameterInfos/* parameters */
	, 473/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid()
extern const MethodInfo SafeHandle_get_IsInvalid_m10955_MethodInfo = 
{
	"get_IsInvalid"/* name */
	, NULL/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 474/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.SafeHandle::Finalize()
extern const MethodInfo SafeHandle_Finalize_m8495_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&SafeHandle_Finalize_m8495/* method */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SafeHandle_t1390_MethodInfos[] =
{
	&SafeHandle__ctor_m8487_MethodInfo,
	&SafeHandle_Close_m8488_MethodInfo,
	&SafeHandle_DangerousAddRef_m8489_MethodInfo,
	&SafeHandle_DangerousGetHandle_m8490_MethodInfo,
	&SafeHandle_DangerousRelease_m8491_MethodInfo,
	&SafeHandle_Dispose_m8492_MethodInfo,
	&SafeHandle_Dispose_m8493_MethodInfo,
	&SafeHandle_ReleaseHandle_m10954_MethodInfo,
	&SafeHandle_SetHandle_m8494_MethodInfo,
	&SafeHandle_get_IsInvalid_m10955_MethodInfo,
	&SafeHandle_Finalize_m8495_MethodInfo,
	NULL
};
extern const MethodInfo SafeHandle_get_IsInvalid_m10955_MethodInfo;
static const PropertyInfo SafeHandle_t1390____IsInvalid_PropertyInfo = 
{
	&SafeHandle_t1390_il2cpp_TypeInfo/* parent */
	, "IsInvalid"/* name */
	, &SafeHandle_get_IsInvalid_m10955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SafeHandle_t1390_PropertyInfos[] =
{
	&SafeHandle_t1390____IsInvalid_PropertyInfo,
	NULL
};
extern const MethodInfo SafeHandle_Finalize_m8495_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m8492_MethodInfo;
extern const MethodInfo SafeHandle_Dispose_m8493_MethodInfo;
static const Il2CppMethodReference SafeHandle_t1390_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&SafeHandle_Finalize_m8495_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&SafeHandle_Dispose_m8492_MethodInfo,
	&SafeHandle_Dispose_m8493_MethodInfo,
	NULL,
	NULL,
};
static bool SafeHandle_t1390_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t43_0_0_0;
static const Il2CppType* SafeHandle_t1390_InterfacesTypeInfos[] = 
{
	&IDisposable_t43_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandle_t1390_InterfacesOffsets[] = 
{
	{ &IDisposable_t43_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandle_t1390_0_0_0;
extern const Il2CppType SafeHandle_t1390_1_0_0;
struct SafeHandle_t1390;
const Il2CppTypeDefinitionMetadata SafeHandle_t1390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandle_t1390_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandle_t1390_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t1599_0_0_0/* parent */
	, SafeHandle_t1390_VTable/* vtableMethods */
	, SafeHandle_t1390_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1444/* fieldStart */

};
TypeInfo SafeHandle_t1390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, SafeHandle_t1390_MethodInfos/* methods */
	, SafeHandle_t1390_PropertyInfos/* properties */
	, NULL/* events */
	, &SafeHandle_t1390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandle_t1390_0_0_0/* byval_arg */
	, &SafeHandle_t1390_1_0_0/* this_arg */
	, &SafeHandle_t1390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandle_t1390)/* instance_size */
	, sizeof (SafeHandle_t1390)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute
extern TypeInfo TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeLibImportClassAttribute_t1615_TypeLibImportClassAttribute__ctor_m8496_ParameterInfos[] = 
{
	{"importClass", 0, 134221893, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern const MethodInfo TypeLibImportClassAttribute__ctor_m8496_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibImportClassAttribute__ctor_m8496/* method */
	, &TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TypeLibImportClassAttribute_t1615_TypeLibImportClassAttribute__ctor_m8496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibImportClassAttribute_t1615_MethodInfos[] =
{
	&TypeLibImportClassAttribute__ctor_m8496_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibImportClassAttribute_t1615_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TypeLibImportClassAttribute_t1615_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibImportClassAttribute_t1615_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibImportClassAttribute_t1615_0_0_0;
extern const Il2CppType TypeLibImportClassAttribute_t1615_1_0_0;
struct TypeLibImportClassAttribute_t1615;
const Il2CppTypeDefinitionMetadata TypeLibImportClassAttribute_t1615_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibImportClassAttribute_t1615_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, TypeLibImportClassAttribute_t1615_VTable/* vtableMethods */
	, TypeLibImportClassAttribute_t1615_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1448/* fieldStart */

};
TypeInfo TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibImportClassAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibImportClassAttribute_t1615_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibImportClassAttribute_t1615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 475/* custom_attributes_cache */
	, &TypeLibImportClassAttribute_t1615_0_0_0/* byval_arg */
	, &TypeLibImportClassAttribute_t1615_1_0_0/* this_arg */
	, &TypeLibImportClassAttribute_t1615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibImportClassAttribute_t1615)/* instance_size */
	, sizeof (TypeLibImportClassAttribute_t1615)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute
extern TypeInfo TypeLibVersionAttribute_t1616_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo TypeLibVersionAttribute_t1616_TypeLibVersionAttribute__ctor_m8497_ParameterInfos[] = 
{
	{"major", 0, 134221894, 0, &Int32_t54_0_0_0},
	{"minor", 1, 134221895, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TypeLibVersionAttribute__ctor_m8497_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLibVersionAttribute__ctor_m8497/* method */
	, &TypeLibVersionAttribute_t1616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, TypeLibVersionAttribute_t1616_TypeLibVersionAttribute__ctor_m8497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLibVersionAttribute_t1616_MethodInfos[] =
{
	&TypeLibVersionAttribute__ctor_m8497_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeLibVersionAttribute_t1616_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TypeLibVersionAttribute_t1616_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLibVersionAttribute_t1616_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibVersionAttribute_t1616_0_0_0;
extern const Il2CppType TypeLibVersionAttribute_t1616_1_0_0;
struct TypeLibVersionAttribute_t1616;
const Il2CppTypeDefinitionMetadata TypeLibVersionAttribute_t1616_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibVersionAttribute_t1616_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, TypeLibVersionAttribute_t1616_VTable/* vtableMethods */
	, TypeLibVersionAttribute_t1616_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1449/* fieldStart */

};
TypeInfo TypeLibVersionAttribute_t1616_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibVersionAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, TypeLibVersionAttribute_t1616_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeLibVersionAttribute_t1616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 476/* custom_attributes_cache */
	, &TypeLibVersionAttribute_t1616_0_0_0/* byval_arg */
	, &TypeLibVersionAttribute_t1616_1_0_0/* this_arg */
	, &TypeLibVersionAttribute_t1616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibVersionAttribute_t1616)/* instance_size */
	, sizeof (TypeLibVersionAttribute_t1616)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
// Metadata Definition System.Runtime.InteropServices.UnmanagedType
extern TypeInfo UnmanagedType_t1617_il2cpp_TypeInfo;
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTypeMethodDeclarations.h"
static const MethodInfo* UnmanagedType_t1617_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnmanagedType_t1617_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UnmanagedType_t1617_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnmanagedType_t1617_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnmanagedType_t1617_0_0_0;
extern const Il2CppType UnmanagedType_t1617_1_0_0;
const Il2CppTypeDefinitionMetadata UnmanagedType_t1617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnmanagedType_t1617_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UnmanagedType_t1617_VTable/* vtableMethods */
	, UnmanagedType_t1617_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1451/* fieldStart */

};
TypeInfo UnmanagedType_t1617_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnmanagedType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, UnmanagedType_t1617_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 477/* custom_attributes_cache */
	, &UnmanagedType_t1617_0_0_0/* byval_arg */
	, &UnmanagedType_t1617_1_0_0/* this_arg */
	, &UnmanagedType_t1617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnmanagedType_t1617)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnmanagedType_t1617)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 36/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Activator
extern TypeInfo _Activator_t2007_il2cpp_TypeInfo;
static const MethodInfo* _Activator_t2007_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Activator_t2007_0_0_0;
extern const Il2CppType _Activator_t2007_1_0_0;
struct _Activator_t2007;
const Il2CppTypeDefinitionMetadata _Activator_t2007_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Activator_t2007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Activator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Activator_t2007_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Activator_t2007_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 478/* custom_attributes_cache */
	, &_Activator_t2007_0_0_0/* byval_arg */
	, &_Activator_t2007_1_0_0/* this_arg */
	, &_Activator_t2007_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Assembly
extern TypeInfo _Assembly_t1997_il2cpp_TypeInfo;
static const MethodInfo* _Assembly_t1997_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Assembly_t1997_0_0_0;
extern const Il2CppType _Assembly_t1997_1_0_0;
struct _Assembly_t1997;
const Il2CppTypeDefinitionMetadata _Assembly_t1997_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Assembly_t1997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Assembly"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Assembly_t1997_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Assembly_t1997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 479/* custom_attributes_cache */
	, &_Assembly_t1997_0_0_0/* byval_arg */
	, &_Assembly_t1997_1_0_0/* this_arg */
	, &_Assembly_t1997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyBuilder
extern TypeInfo _AssemblyBuilder_t1988_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyBuilder_t1988_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyBuilder_t1988_0_0_0;
extern const Il2CppType _AssemblyBuilder_t1988_1_0_0;
struct _AssemblyBuilder_t1988;
const Il2CppTypeDefinitionMetadata _AssemblyBuilder_t1988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyBuilder_t1988_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyBuilder_t1988_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyBuilder_t1988_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 480/* custom_attributes_cache */
	, &_AssemblyBuilder_t1988_0_0_0/* byval_arg */
	, &_AssemblyBuilder_t1988_1_0_0/* this_arg */
	, &_AssemblyBuilder_t1988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyName
extern TypeInfo _AssemblyName_t1998_il2cpp_TypeInfo;
static const MethodInfo* _AssemblyName_t1998_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyName_t1998_1_0_0;
struct _AssemblyName_t1998;
const Il2CppTypeDefinitionMetadata _AssemblyName_t1998_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _AssemblyName_t1998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyName"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _AssemblyName_t1998_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_AssemblyName_t1998_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 481/* custom_attributes_cache */
	, &_AssemblyName_t1998_0_0_0/* byval_arg */
	, &_AssemblyName_t1998_1_0_0/* this_arg */
	, &_AssemblyName_t1998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder
extern TypeInfo _ConstructorBuilder_t1989_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorBuilder_t1989_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorBuilder_t1989_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1989_1_0_0;
struct _ConstructorBuilder_t1989;
const Il2CppTypeDefinitionMetadata _ConstructorBuilder_t1989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorBuilder_t1989_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorBuilder_t1989_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorBuilder_t1989_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 482/* custom_attributes_cache */
	, &_ConstructorBuilder_t1989_0_0_0/* byval_arg */
	, &_ConstructorBuilder_t1989_1_0_0/* this_arg */
	, &_ConstructorBuilder_t1989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorInfo
extern TypeInfo _ConstructorInfo_t1999_il2cpp_TypeInfo;
static const MethodInfo* _ConstructorInfo_t1999_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorInfo_t1999_1_0_0;
struct _ConstructorInfo_t1999;
const Il2CppTypeDefinitionMetadata _ConstructorInfo_t1999_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ConstructorInfo_t1999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ConstructorInfo_t1999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ConstructorInfo_t1999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 483/* custom_attributes_cache */
	, &_ConstructorInfo_t1999_0_0_0/* byval_arg */
	, &_ConstructorInfo_t1999_1_0_0/* this_arg */
	, &_ConstructorInfo_t1999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EnumBuilder
extern TypeInfo _EnumBuilder_t1990_il2cpp_TypeInfo;
static const MethodInfo* _EnumBuilder_t1990_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EnumBuilder_t1990_0_0_0;
extern const Il2CppType _EnumBuilder_t1990_1_0_0;
struct _EnumBuilder_t1990;
const Il2CppTypeDefinitionMetadata _EnumBuilder_t1990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EnumBuilder_t1990_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EnumBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EnumBuilder_t1990_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EnumBuilder_t1990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 484/* custom_attributes_cache */
	, &_EnumBuilder_t1990_0_0_0/* byval_arg */
	, &_EnumBuilder_t1990_1_0_0/* this_arg */
	, &_EnumBuilder_t1990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EventInfo
extern TypeInfo _EventInfo_t2000_il2cpp_TypeInfo;
static const MethodInfo* _EventInfo_t2000_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EventInfo_t2000_1_0_0;
struct _EventInfo_t2000;
const Il2CppTypeDefinitionMetadata _EventInfo_t2000_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _EventInfo_t2000_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EventInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _EventInfo_t2000_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_EventInfo_t2000_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 485/* custom_attributes_cache */
	, &_EventInfo_t2000_0_0_0/* byval_arg */
	, &_EventInfo_t2000_1_0_0/* this_arg */
	, &_EventInfo_t2000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldBuilder
extern TypeInfo _FieldBuilder_t1991_il2cpp_TypeInfo;
static const MethodInfo* _FieldBuilder_t1991_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldBuilder_t1991_0_0_0;
extern const Il2CppType _FieldBuilder_t1991_1_0_0;
struct _FieldBuilder_t1991;
const Il2CppTypeDefinitionMetadata _FieldBuilder_t1991_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldBuilder_t1991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldBuilder_t1991_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldBuilder_t1991_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 486/* custom_attributes_cache */
	, &_FieldBuilder_t1991_0_0_0/* byval_arg */
	, &_FieldBuilder_t1991_1_0_0/* this_arg */
	, &_FieldBuilder_t1991_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldInfo
extern TypeInfo _FieldInfo_t2001_il2cpp_TypeInfo;
static const MethodInfo* _FieldInfo_t2001_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldInfo_t2001_1_0_0;
struct _FieldInfo_t2001;
const Il2CppTypeDefinitionMetadata _FieldInfo_t2001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _FieldInfo_t2001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _FieldInfo_t2001_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_FieldInfo_t2001_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 487/* custom_attributes_cache */
	, &_FieldInfo_t2001_0_0_0/* byval_arg */
	, &_FieldInfo_t2001_1_0_0/* this_arg */
	, &_FieldInfo_t2001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBase
extern TypeInfo _MethodBase_t2002_il2cpp_TypeInfo;
static const MethodInfo* _MethodBase_t2002_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBase_t2002_1_0_0;
struct _MethodBase_t2002;
const Il2CppTypeDefinitionMetadata _MethodBase_t2002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBase_t2002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBase"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBase_t2002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBase_t2002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 488/* custom_attributes_cache */
	, &_MethodBase_t2002_0_0_0/* byval_arg */
	, &_MethodBase_t2002_1_0_0/* this_arg */
	, &_MethodBase_t2002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBuilder
extern TypeInfo _MethodBuilder_t1992_il2cpp_TypeInfo;
static const MethodInfo* _MethodBuilder_t1992_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBuilder_t1992_0_0_0;
extern const Il2CppType _MethodBuilder_t1992_1_0_0;
struct _MethodBuilder_t1992;
const Il2CppTypeDefinitionMetadata _MethodBuilder_t1992_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodBuilder_t1992_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodBuilder_t1992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodBuilder_t1992_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 489/* custom_attributes_cache */
	, &_MethodBuilder_t1992_0_0_0/* byval_arg */
	, &_MethodBuilder_t1992_1_0_0/* this_arg */
	, &_MethodBuilder_t1992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodInfo
extern TypeInfo _MethodInfo_t2003_il2cpp_TypeInfo;
static const MethodInfo* _MethodInfo_t2003_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodInfo_t2003_1_0_0;
struct _MethodInfo_t2003;
const Il2CppTypeDefinitionMetadata _MethodInfo_t2003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _MethodInfo_t2003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _MethodInfo_t2003_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_MethodInfo_t2003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 490/* custom_attributes_cache */
	, &_MethodInfo_t2003_0_0_0/* byval_arg */
	, &_MethodInfo_t2003_1_0_0/* this_arg */
	, &_MethodInfo_t2003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Module
extern TypeInfo _Module_t2004_il2cpp_TypeInfo;
static const MethodInfo* _Module_t2004_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Module_t2004_1_0_0;
struct _Module_t2004;
const Il2CppTypeDefinitionMetadata _Module_t2004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Module_t2004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Module"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Module_t2004_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Module_t2004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 491/* custom_attributes_cache */
	, &_Module_t2004_0_0_0/* byval_arg */
	, &_Module_t2004_1_0_0/* this_arg */
	, &_Module_t2004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
extern TypeInfo _ModuleBuilder_t1993_il2cpp_TypeInfo;
static const MethodInfo* _ModuleBuilder_t1993_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ModuleBuilder_t1993_0_0_0;
extern const Il2CppType _ModuleBuilder_t1993_1_0_0;
struct _ModuleBuilder_t1993;
const Il2CppTypeDefinitionMetadata _ModuleBuilder_t1993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ModuleBuilder_t1993_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ModuleBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ModuleBuilder_t1993_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ModuleBuilder_t1993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 492/* custom_attributes_cache */
	, &_ModuleBuilder_t1993_0_0_0/* byval_arg */
	, &_ModuleBuilder_t1993_1_0_0/* this_arg */
	, &_ModuleBuilder_t1993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
extern TypeInfo _ParameterBuilder_t1994_il2cpp_TypeInfo;
static const MethodInfo* _ParameterBuilder_t1994_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterBuilder_t1994_0_0_0;
extern const Il2CppType _ParameterBuilder_t1994_1_0_0;
struct _ParameterBuilder_t1994;
const Il2CppTypeDefinitionMetadata _ParameterBuilder_t1994_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterBuilder_t1994_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterBuilder_t1994_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterBuilder_t1994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 493/* custom_attributes_cache */
	, &_ParameterBuilder_t1994_0_0_0/* byval_arg */
	, &_ParameterBuilder_t1994_1_0_0/* this_arg */
	, &_ParameterBuilder_t1994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterInfo
extern TypeInfo _ParameterInfo_t2005_il2cpp_TypeInfo;
static const MethodInfo* _ParameterInfo_t2005_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterInfo_t2005_1_0_0;
struct _ParameterInfo_t2005;
const Il2CppTypeDefinitionMetadata _ParameterInfo_t2005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _ParameterInfo_t2005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _ParameterInfo_t2005_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_ParameterInfo_t2005_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 494/* custom_attributes_cache */
	, &_ParameterInfo_t2005_0_0_0/* byval_arg */
	, &_ParameterInfo_t2005_1_0_0/* this_arg */
	, &_ParameterInfo_t2005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
