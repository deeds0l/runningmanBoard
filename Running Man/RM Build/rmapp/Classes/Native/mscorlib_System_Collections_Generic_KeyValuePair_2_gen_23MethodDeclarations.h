﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t2900;
// UnityEngine.Event
struct Event_t229;
struct Event_t229_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17708(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2900 *, Event_t229 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m13149_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m17709(__this, method) (( Event_t229 * (*) (KeyValuePair_2_t2900 *, const MethodInfo*))KeyValuePair_2_get_Key_m13150_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17710(__this, ___value, method) (( void (*) (KeyValuePair_2_t2900 *, Event_t229 *, const MethodInfo*))KeyValuePair_2_set_Key_m13151_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m17711(__this, method) (( int32_t (*) (KeyValuePair_2_t2900 *, const MethodInfo*))KeyValuePair_2_get_Value_m13152_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17712(__this, ___value, method) (( void (*) (KeyValuePair_2_t2900 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m13153_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m17713(__this, method) (( String_t* (*) (KeyValuePair_2_t2900 *, const MethodInfo*))KeyValuePair_2_ToString_m13154_gshared)(__this, method)
