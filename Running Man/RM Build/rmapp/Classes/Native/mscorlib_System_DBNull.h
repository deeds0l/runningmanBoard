﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t1827;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t1827  : public Object_t
{
};
struct DBNull_t1827_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t1827 * ___Value_0;
};
