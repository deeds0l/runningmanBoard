﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACMD5
struct HMACMD5_t1726;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void System.Security.Cryptography.HMACMD5::.ctor()
extern "C" void HMACMD5__ctor_m8975 (HMACMD5_t1726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACMD5::.ctor(System.Byte[])
extern "C" void HMACMD5__ctor_m8976 (HMACMD5_t1726 * __this, ByteU5BU5D_t546* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
