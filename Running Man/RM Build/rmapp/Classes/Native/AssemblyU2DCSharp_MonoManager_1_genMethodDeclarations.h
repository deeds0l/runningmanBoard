﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MonoManager`1<AudioManager>
struct MonoManager_1_t3;
// AudioManager
struct AudioManager_t2;
// UnityEngine.Transform
struct Transform_t30;

// System.Void MonoManager`1<AudioManager>::.ctor()
// MonoManager`1<System.Object>
#include "AssemblyU2DCSharp_MonoManager_1_gen_0MethodDeclarations.h"
#define MonoManager_1__ctor_m76(__this, method) (( void (*) (MonoManager_1_t3 *, const MethodInfo*))MonoManager_1__ctor_m11264_gshared)(__this, method)
// System.Void MonoManager`1<AudioManager>::.cctor()
#define MonoManager_1__cctor_m11265(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))MonoManager_1__cctor_m11266_gshared)(__this /* static, unused */, method)
// T MonoManager`1<AudioManager>::get_Instance()
#define MonoManager_1_get_Instance_m112(__this /* static, unused */, method) (( AudioManager_t2 * (*) (Object_t * /* static, unused */, const MethodInfo*))MonoManager_1_get_Instance_m11267_gshared)(__this /* static, unused */, method)
// System.Void MonoManager`1<AudioManager>::Create(UnityEngine.Transform)
#define MonoManager_1_Create_m116(__this /* static, unused */, ___p_parent, method) (( void (*) (Object_t * /* static, unused */, Transform_t30 *, const MethodInfo*))MonoManager_1_Create_m11268_gshared)(__this /* static, unused */, ___p_parent, method)
// System.Void MonoManager`1<AudioManager>::Awake()
#define MonoManager_1_Awake_m11269(__this, method) (( void (*) (MonoManager_1_t3 *, const MethodInfo*))MonoManager_1_Awake_m11270_gshared)(__this, method)
// System.Void MonoManager`1<AudioManager>::OnDestroy()
#define MonoManager_1_OnDestroy_m221(__this, method) (( void (*) (MonoManager_1_t3 *, const MethodInfo*))MonoManager_1_OnDestroy_m11271_gshared)(__this, method)
// System.Void MonoManager`1<AudioManager>::Destroy()
#define MonoManager_1_Destroy_m11272(__this, method) (( void (*) (MonoManager_1_t3 *, const MethodInfo*))MonoManager_1_Destroy_m11273_gshared)(__this, method)
