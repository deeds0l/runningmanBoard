﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t1844;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct  Environment_t1845  : public Object_t
{
};
struct Environment_t1845_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t1844 * ___os_0;
};
