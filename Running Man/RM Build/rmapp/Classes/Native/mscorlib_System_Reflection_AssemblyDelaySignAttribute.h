﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyDelaySignAttribute
struct  AssemblyDelaySignAttribute_t935  : public Attribute_t539
{
	// System.Boolean System.Reflection.AssemblyDelaySignAttribute::delay
	bool ___delay_0;
};
