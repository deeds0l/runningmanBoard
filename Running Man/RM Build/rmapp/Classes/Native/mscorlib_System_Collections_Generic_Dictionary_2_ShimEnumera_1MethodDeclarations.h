﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2602;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2592;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m13548_gshared (ShimEnumerator_t2602 * __this, Dictionary_2_t2592 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m13548(__this, ___host, method) (( void (*) (ShimEnumerator_t2602 *, Dictionary_2_t2592 *, const MethodInfo*))ShimEnumerator__ctor_m13548_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m13549_gshared (ShimEnumerator_t2602 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m13549(__this, method) (( bool (*) (ShimEnumerator_t2602 *, const MethodInfo*))ShimEnumerator_MoveNext_m13549_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1147  ShimEnumerator_get_Entry_m13550_gshared (ShimEnumerator_t2602 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m13550(__this, method) (( DictionaryEntry_t1147  (*) (ShimEnumerator_t2602 *, const MethodInfo*))ShimEnumerator_get_Entry_m13550_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m13551_gshared (ShimEnumerator_t2602 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m13551(__this, method) (( Object_t * (*) (ShimEnumerator_t2602 *, const MethodInfo*))ShimEnumerator_get_Key_m13551_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m13552_gshared (ShimEnumerator_t2602 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m13552(__this, method) (( Object_t * (*) (ShimEnumerator_t2602 *, const MethodInfo*))ShimEnumerator_get_Value_m13552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m13553_gshared (ShimEnumerator_t2602 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m13553(__this, method) (( Object_t * (*) (ShimEnumerator_t2602 *, const MethodInfo*))ShimEnumerator_get_Current_m13553_gshared)(__this, method)
