﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>
struct InternalEnumerator_1_t2884;
// System.Object
struct Object_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17520(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2884 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17521(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2884 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m17522(__this, method) (( void (*) (InternalEnumerator_1_t2884 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17523(__this, method) (( bool (*) (InternalEnumerator_1_t2884 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m17524(__this, method) (( FieldInfo_t * (*) (InternalEnumerator_1_t2884 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
