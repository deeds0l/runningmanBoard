﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.Camera
struct Camera_t156;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t665 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t5 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t156 * ___camera_1;
};
