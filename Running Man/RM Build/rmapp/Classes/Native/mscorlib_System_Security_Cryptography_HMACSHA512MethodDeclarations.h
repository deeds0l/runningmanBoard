﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA512
struct HMACSHA512_t1730;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void System.Security.Cryptography.HMACSHA512::.ctor()
extern "C" void HMACSHA512__ctor_m8987 (HMACSHA512_t1730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.ctor(System.Byte[])
extern "C" void HMACSHA512__ctor_m8988 (HMACSHA512_t1730 * __this, ByteU5BU5D_t546* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.cctor()
extern "C" void HMACSHA512__cctor_m8989 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m8990 (HMACSHA512_t1730 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
