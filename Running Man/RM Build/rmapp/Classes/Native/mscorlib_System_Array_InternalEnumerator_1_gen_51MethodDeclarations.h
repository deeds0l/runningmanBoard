﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t2895;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17649_gshared (InternalEnumerator_1_t2895 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17649(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2895 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17649_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17650_gshared (InternalEnumerator_1_t2895 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17650(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2895 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17650_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17651_gshared (InternalEnumerator_1_t2895 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17651(__this, method) (( void (*) (InternalEnumerator_1_t2895 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17651_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17652_gshared (InternalEnumerator_1_t2895 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17652(__this, method) (( bool (*) (InternalEnumerator_1_t2895 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17652_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t665  InternalEnumerator_1_get_Current_m17653_gshared (InternalEnumerator_1_t2895 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17653(__this, method) (( HitInfo_t665  (*) (InternalEnumerator_1_t2895 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17653_gshared)(__this, method)
