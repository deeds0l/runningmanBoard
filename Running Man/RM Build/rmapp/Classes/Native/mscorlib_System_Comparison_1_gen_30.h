﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t604;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  Comparison_1_t2804  : public MulticastDelegate_t216
{
};
