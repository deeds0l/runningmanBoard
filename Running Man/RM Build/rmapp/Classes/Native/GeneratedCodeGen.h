﻿#pragma once
struct Object_t;
typedef Object_t Il2CppCodeGenObject;
// System.Array
#include "mscorlib_System_Array.h"
typedef Array_t Il2CppCodeGenArray;
struct String_t;
typedef String_t Il2CppCodeGenString;
struct Type_t;
typedef Type_t Il2CppCodeGenType;
struct Exception_t42;
typedef Exception_t42 Il2CppCodeGenException;
struct Exception_t42;
typedef Exception_t42 Il2CppCodeGenException;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
typedef RuntimeTypeHandle_t1371 Il2CppCodeGenRuntimeTypeHandle;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
typedef RuntimeFieldHandle_t1372 Il2CppCodeGenRuntimeFieldHandle;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
typedef RuntimeArgumentHandle_t1381 Il2CppCodeGenRuntimeArgumentHandle;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
typedef RuntimeMethodHandle_t1871 Il2CppCodeGenRuntimeMethodHandle;
struct StringBuilder_t338;
typedef StringBuilder_t338 Il2CppCodeGenStringBuilder;
struct MulticastDelegate_t216;
typedef MulticastDelegate_t216 Il2CppCodeGenMulticastDelegate;
