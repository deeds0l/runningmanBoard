﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1223;
// Mono.Math.BigInteger
struct BigInteger_t1222;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m4983 (ModulusRing_t1223 * __this, BigInteger_t1222 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m4984 (ModulusRing_t1223 * __this, BigInteger_t1222 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1222 * ModulusRing_Multiply_m4985 (ModulusRing_t1223 * __this, BigInteger_t1222 * ___a, BigInteger_t1222 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1222 * ModulusRing_Difference_m4986 (ModulusRing_t1223 * __this, BigInteger_t1222 * ___a, BigInteger_t1222 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1222 * ModulusRing_Pow_m4987 (ModulusRing_t1223 * __this, BigInteger_t1222 * ___a, BigInteger_t1222 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t1222 * ModulusRing_Pow_m4988 (ModulusRing_t1223 * __this, uint32_t ___b, BigInteger_t1222 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
