﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<System.Byte>
struct UnityEvent_1_t2674;
// UnityEngine.Events.UnityAction`1<System.Byte>
struct UnityAction_1_t2675;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t682;

// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::.ctor()
extern "C" void UnityEvent_1__ctor_m14597_gshared (UnityEvent_1_t2674 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m14597(__this, method) (( void (*) (UnityEvent_1_t2674 *, const MethodInfo*))UnityEvent_1__ctor_m14597_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m14599_gshared (UnityEvent_1_t2674 * __this, UnityAction_1_t2675 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m14599(__this, ___call, method) (( void (*) (UnityEvent_1_t2674 *, UnityAction_1_t2675 *, const MethodInfo*))UnityEvent_1_AddListener_m14599_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m14601_gshared (UnityEvent_1_t2674 * __this, UnityAction_1_t2675 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m14601(__this, ___call, method) (( void (*) (UnityEvent_1_t2674 *, UnityAction_1_t2675 *, const MethodInfo*))UnityEvent_1_RemoveListener_m14601_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Byte>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m14602_gshared (UnityEvent_1_t2674 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m14602(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t2674 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m14602_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Byte>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t682 * UnityEvent_1_GetDelegate_m14603_gshared (UnityEvent_1_t2674 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m14603(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t682 * (*) (UnityEvent_1_t2674 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m14603_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Byte>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t682 * UnityEvent_1_GetDelegate_m14605_gshared (Object_t * __this /* static, unused */, UnityAction_1_t2675 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m14605(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t682 * (*) (Object_t * /* static, unused */, UnityAction_1_t2675 *, const MethodInfo*))UnityEvent_1_GetDelegate_m14605_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m14606_gshared (UnityEvent_1_t2674 * __this, uint8_t ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m14606(__this, ___arg0, method) (( void (*) (UnityEvent_1_t2674 *, uint8_t, const MethodInfo*))UnityEvent_1_Invoke_m14606_gshared)(__this, ___arg0, method)
