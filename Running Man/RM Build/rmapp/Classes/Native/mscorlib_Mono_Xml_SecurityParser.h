﻿#pragma once
#include <stdint.h>
// System.Security.SecurityElement
struct SecurityElement_t1451;
// System.Collections.Stack
struct Stack_t696;
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Mono.Xml.SecurityParser
struct  SecurityParser_t1452  : public SmallXmlParser_t1453
{
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t1451 * ___root_13;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t1451 * ___current_14;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t696 * ___stack_15;
};
