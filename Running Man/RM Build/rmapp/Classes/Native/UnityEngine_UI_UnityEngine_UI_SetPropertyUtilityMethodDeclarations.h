﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.SetPropertyUtility
struct SetPropertyUtility_t253;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern "C" bool SetPropertyUtility_SetColor_m1094 (Object_t * __this /* static, unused */, Color_t163 * ___currentValue, Color_t163  ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
