﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// Tools/<WaitForRealTime>c__Iterator5
struct  U3CWaitForRealTimeU3Ec__Iterator5_t26  : public Object_t
{
	// System.Single Tools/<WaitForRealTime>c__Iterator5::p_delay
	float ___p_delay_0;
	// System.Single Tools/<WaitForRealTime>c__Iterator5::<pauseEndTime>__0
	float ___U3CpauseEndTimeU3E__0_1;
	// System.Int32 Tools/<WaitForRealTime>c__Iterator5::$PC
	int32_t ___U24PC_2;
	// System.Object Tools/<WaitForRealTime>c__Iterator5::$current
	Object_t * ___U24current_3;
	// System.Single Tools/<WaitForRealTime>c__Iterator5::<$>p_delay
	float ___U3CU24U3Ep_delay_4;
};
