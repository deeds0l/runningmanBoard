﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t190;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t188;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3111;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.Graphic>
struct ICollection_1_t363;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Graphic>
struct IEnumerable_1_t3112;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Graphic>
struct ReadOnlyCollection_1_t2627;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t2626;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t2628;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t191;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m1651(__this, method) (( void (*) (List_1_t190 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::.ctor(System.Int32)
#define List_1__ctor_m13915(__this, ___capacity, method) (( void (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::.cctor()
#define List_1__cctor_m13916(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13917(__this, method) (( Object_t* (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13918(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t190 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13919(__this, method) (( Object_t * (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13920(__this, ___item, method) (( int32_t (*) (List_1_t190 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13921(__this, ___item, method) (( bool (*) (List_1_t190 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13922(__this, ___item, method) (( int32_t (*) (List_1_t190 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13923(__this, ___index, ___item, method) (( void (*) (List_1_t190 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13924(__this, ___item, method) (( void (*) (List_1_t190 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13925(__this, method) (( bool (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13926(__this, method) (( bool (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m13927(__this, method) (( Object_t * (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m13928(__this, method) (( bool (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m13929(__this, method) (( bool (*) (List_1_t190 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m13930(__this, ___index, method) (( Object_t * (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m13931(__this, ___index, ___value, method) (( void (*) (List_1_t190 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Add(T)
#define List_1_Add_m13932(__this, ___item, method) (( void (*) (List_1_t190 *, Graphic_t188 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m13933(__this, ___newCount, method) (( void (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m13934(__this, ___collection, method) (( void (*) (List_1_t190 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m13935(__this, ___enumerable, method) (( void (*) (List_1_t190 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m13936(__this, ___collection, method) (( void (*) (List_1_t190 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::AsReadOnly()
#define List_1_AsReadOnly_m13937(__this, method) (( ReadOnlyCollection_1_t2627 * (*) (List_1_t190 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Clear()
#define List_1_Clear_m13938(__this, method) (( void (*) (List_1_t190 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Contains(T)
#define List_1_Contains_m13939(__this, ___item, method) (( bool (*) (List_1_t190 *, Graphic_t188 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m13940(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t190 *, GraphicU5BU5D_t2626*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Find(System.Predicate`1<T>)
#define List_1_Find_m13941(__this, ___match, method) (( Graphic_t188 * (*) (List_1_t190 *, Predicate_1_t2628 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m13942(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2628 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m13943(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t190 *, int32_t, int32_t, Predicate_1_t2628 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define List_1_GetEnumerator_m13944(__this, method) (( Enumerator_t2629  (*) (List_1_t190 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define List_1_IndexOf_m13945(__this, ___item, method) (( int32_t (*) (List_1_t190 *, Graphic_t188 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13946(__this, ___start, ___delta, method) (( void (*) (List_1_t190 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m13947(__this, ___index, method) (( void (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define List_1_Insert_m13948(__this, ___index, ___item, method) (( void (*) (List_1_t190 *, int32_t, Graphic_t188 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m13949(__this, ___collection, method) (( void (*) (List_1_t190 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Remove(T)
#define List_1_Remove_m13950(__this, ___item, method) (( bool (*) (List_1_t190 *, Graphic_t188 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m13951(__this, ___match, method) (( int32_t (*) (List_1_t190 *, Predicate_1_t2628 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m13952(__this, ___index, method) (( void (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Reverse()
#define List_1_Reverse_m13953(__this, method) (( void (*) (List_1_t190 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Sort()
#define List_1_Sort_m13954(__this, method) (( void (*) (List_1_t190 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1671(__this, ___comparison, method) (( void (*) (List_1_t190 *, Comparison_1_t191 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::ToArray()
#define List_1_ToArray_m13955(__this, method) (( GraphicU5BU5D_t2626* (*) (List_1_t190 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::TrimExcess()
#define List_1_TrimExcess_m13956(__this, method) (( void (*) (List_1_t190 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::get_Capacity()
#define List_1_get_Capacity_m13957(__this, method) (( int32_t (*) (List_1_t190 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m13958(__this, ___value, method) (( void (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::get_Count()
#define List_1_get_Count_m13959(__this, method) (( int32_t (*) (List_1_t190 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define List_1_get_Item_m13960(__this, ___index, method) (( Graphic_t188 * (*) (List_1_t190 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define List_1_set_Item_m13961(__this, ___index, ___value, method) (( void (*) (List_1_t190 *, int32_t, Graphic_t188 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
