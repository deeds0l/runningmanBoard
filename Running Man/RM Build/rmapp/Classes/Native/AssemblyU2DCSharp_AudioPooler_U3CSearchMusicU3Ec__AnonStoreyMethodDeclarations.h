﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<SearchMusic>c__AnonStoreyB
struct U3CSearchMusicU3Ec__AnonStoreyB_t18;
// UnityEngine.AudioClip
struct AudioClip_t20;

// System.Void AudioPooler/<SearchMusic>c__AnonStoreyB::.ctor()
extern "C" void U3CSearchMusicU3Ec__AnonStoreyB__ctor_m30 (U3CSearchMusicU3Ec__AnonStoreyB_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<SearchMusic>c__AnonStoreyB::<>m__5(UnityEngine.AudioClip)
extern "C" bool U3CSearchMusicU3Ec__AnonStoreyB_U3CU3Em__5_m31 (U3CSearchMusicU3Ec__AnonStoreyB_t18 * __this, AudioClip_t20 * ___clip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
