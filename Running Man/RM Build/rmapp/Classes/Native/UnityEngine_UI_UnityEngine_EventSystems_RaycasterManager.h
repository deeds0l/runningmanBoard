﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t134;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t135  : public Object_t
{
};
struct RaycasterManager_t135_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t134 * ___s_Raycasters_0;
};
