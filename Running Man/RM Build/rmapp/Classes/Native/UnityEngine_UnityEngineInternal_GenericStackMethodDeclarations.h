﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.GenericStack
struct GenericStack_t506;

// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m3370 (GenericStack_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
