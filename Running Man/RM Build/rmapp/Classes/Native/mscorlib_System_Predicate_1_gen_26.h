﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t546;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Byte[]>
struct  Predicate_1_t2745  : public MulticastDelegate_t216
{
};
