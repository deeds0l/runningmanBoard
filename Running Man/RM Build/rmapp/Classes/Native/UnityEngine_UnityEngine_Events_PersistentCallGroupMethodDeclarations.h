﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t687;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t689;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t690;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m3341 (PersistentCallGroup_t687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m3342 (PersistentCallGroup_t687 * __this, InvokableCallList_t689 * ___invokableList, UnityEventBase_t690 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
