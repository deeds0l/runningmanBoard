﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>
struct InternalEnumerator_1_t2954;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.Group
struct Group_t1073;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18248(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2954 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18249(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2954 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::Dispose()
#define InternalEnumerator_1_Dispose_m18250(__this, method) (( void (*) (InternalEnumerator_1_t2954 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18251(__this, method) (( bool (*) (InternalEnumerator_1_t2954 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Group>::get_Current()
#define InternalEnumerator_1_get_Current_m18252(__this, method) (( Group_t1073 * (*) (InternalEnumerator_1_t2954 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
