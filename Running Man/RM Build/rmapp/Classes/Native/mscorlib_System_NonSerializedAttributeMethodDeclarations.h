﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.NonSerializedAttribute
struct NonSerializedAttribute_t1863;

// System.Void System.NonSerializedAttribute::.ctor()
extern "C" void NonSerializedAttribute__ctor_m10129 (NonSerializedAttribute_t1863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
