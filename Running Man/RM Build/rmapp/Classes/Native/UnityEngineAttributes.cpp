﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1081);
		RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 16;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t56 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t56 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m152(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t791 * tmp;
		tmp = (InternalsVisibleToAttribute_t791 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t791_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m3532(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t471_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m2164(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t471_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m2165(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m2169(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m2170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m2171(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_LayerToName_m2174(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m2175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_t158_LayerMask_GetMask_m2176_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void RuntimePlatform_t476_CustomAttributesCacheGenerator_NaCl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("NaCl export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void RuntimePlatform_t476_CustomAttributesCacheGenerator_FlashPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("FlashPlayer export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerX86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX86 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerX64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX64 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerARM(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerARM instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void SystemInfo_t477_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m2177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Coroutine_t228_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m2180(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m2183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_t480_ScriptableObject_Internal_CreateScriptableObject_m2183_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m2184(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m2186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m2191(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m2192(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m2193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m2194(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m2195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m2196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m2197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m2198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m2199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m2200(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m2201(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m2202(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m2203(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m2204(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m2205(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m2206(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2207(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2211(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m2255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m2256(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m2257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m2258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Renderer_t494_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m1573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Renderer_t494_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m1574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Screen_t495_CustomAttributesCacheGenerator_Screen_get_width_m1655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Screen_t495_CustomAttributesCacheGenerator_Screen_get_height_m1656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Screen_t495_CustomAttributesCacheGenerator_Screen_get_dpi_m1932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Texture_t233_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m2280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Texture_t233_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m2281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m2285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_t181_Texture2D_Internal_Create_m2285_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m1623(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m1719(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RenderTexture_t496_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m2286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RenderTexture_t496_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m2287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUILayer_t499_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m2291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Gradient_t502_CustomAttributesCacheGenerator_Gradient_Init_m2295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Gradient_t502_CustomAttributesCacheGenerator_Gradient_Cleanup_m2296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void GUI_t507_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void GUI_t507_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void GUI_t507_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m2304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUI_t507_CustomAttributesCacheGenerator_GUI_set_changed_m2307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUILayoutUtility_t514_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m2320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUILayoutUtility_t514_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m2322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m2353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m2354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m2356(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m2358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m2362(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISettings_t522_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISettings_t522_CustomAttributesCacheGenerator_m_TripleClickSelectsLine(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISettings_t522_CustomAttributesCacheGenerator_m_CursorColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISettings_t522_CustomAttributesCacheGenerator_m_CursorFlashSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISettings_t522_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t416_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t416 * tmp;
		tmp = (ExecuteInEditMode_t416 *)il2cpp_codegen_object_new (ExecuteInEditMode_t416_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2043(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_box(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_button(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_toggle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_label(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_textField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_textArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_window(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_ScrollView(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_CustomStyles(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUISkin_t505_CustomAttributesCacheGenerator_m_Settings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUIContent_t373_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUIContent_t373_CustomAttributesCacheGenerator_m_Image(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void GUIContent_t373_CustomAttributesCacheGenerator_m_Tooltip(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_Init_m2429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m2430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m2431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m2432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_Init_m2437(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_Cleanup_m2438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_left_m1949(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_left_m2439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_right_m2440(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_right_m2441(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_top_m1950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_top_m2442(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_bottom_m2443(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_bottom_m2444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m1942(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_vertical_m1944(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m2446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Init_m2451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m2452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_name_m2453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_name_m2454(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m2456(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m2459(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m2460(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m2461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m2462(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m2463(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m2464(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m2465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m2466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m2468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m2472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m2474(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m2476(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m2479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m1830(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m1831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("TouchScreenKeyboardType.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m1749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m1750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m1829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m1748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m1828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m1766(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m1762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_Init_m2483(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_Cleanup_m2485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_rawType_m1780(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_type_m2486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m2488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_modifiers_m1776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_character_m1778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_commandName_m2489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_get_keyCode_m1777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m2491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Event_t229_CustomAttributesCacheGenerator_Event_PopEvent_m1781(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void EventModifiers_t531_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Vector2_t53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Vector3_t138_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Color_t163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern TypeInfo* IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo_var;
void Color32_t335_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		IL2CPPStructAlignmentAttribute_t640 * tmp;
		tmp = (IL2CPPStructAlignmentAttribute_t640 *)il2cpp_codegen_object_new (IL2CPPStructAlignmentAttribute_t640_il2cpp_TypeInfo_var);
		IL2CPPStructAlignmentAttribute__ctor_m3194(tmp, NULL);
		tmp->___Align_0 = 4;
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Quaternion_t362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Quaternion_t362_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m2519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m2534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m2536(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m2538(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m2541(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m2553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m2556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m2557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m2574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m2577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m2580(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2584(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void Vector4_t327_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Mathf_t340_CustomAttributesCacheGenerator_Mathf_t340_Mathf_SmoothDamp_m1857_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Mathf_t340_CustomAttributesCacheGenerator_Mathf_t340_Mathf_SmoothDamp_m1857_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void DrivenTransformProperties_t533_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m2613(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m2614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m2615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m2616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m2617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m2618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m2619(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m2620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m2621(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m2622(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m2623(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void Resources_t537_CustomAttributesCacheGenerator_Resources_Load_m2629(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void SerializePrivateVariables_t538_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Shader_t540_CustomAttributesCacheGenerator_Shader_PropertyToID_m2631(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Material_t180_CustomAttributesCacheGenerator_Material_GetTexture_m2633(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Material_t180_CustomAttributesCacheGenerator_Material_SetFloat_m2635(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Material_t180_CustomAttributesCacheGenerator_Material_HasProperty_m2636(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Material_t180_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m2637(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void Material_t180_CustomAttributesCacheGenerator_Material_t180_Material_Internal_CreateWithMaterial_m2637_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2640(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2643(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2646(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_rect_m1691(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m1686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_texture_m1683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_textureRect_m1716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_border_m1684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m1702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m1701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetPadding_m1690(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m2656(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_DestroyWWW_m2660(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_InitWWW_m2661(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m2663(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_get_bytes_m2667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_get_error_m2668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void WWW_t543_CustomAttributesCacheGenerator_WWW_get_isDone_m2669(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void WWWForm_t547_CustomAttributesCacheGenerator_WWWForm_AddField_m2673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void WWWForm_t547_CustomAttributesCacheGenerator_WWWForm_t547_WWWForm_AddField_m2674_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void WWWTranscoder_t548_CustomAttributesCacheGenerator_WWWTranscoder_t548_WWWTranscoder_QPEncode_m2681_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void WWWTranscoder_t548_CustomAttributesCacheGenerator_WWWTranscoder_t548_WWWTranscoder_SevenBitClean_m2684_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void CacheIndex_t549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("this API is not for public use."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void UnityString_t550_CustomAttributesCacheGenerator_UnityString_t550_UnityString_Format_m2686_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AsyncOperation_t472_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m2688(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Application_t552_CustomAttributesCacheGenerator_Application_get_isPlaying_m1833(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Application_t552_CustomAttributesCacheGenerator_Application_get_isEditor_m1837(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Application_t552_CustomAttributesCacheGenerator_Application_get_platform_m1747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_get_enabled_m84(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_set_enabled_m104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1497(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m1563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m1562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_depth_m1457(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_cullingMask_m1578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_eventMask_m2700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m2701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_targetTexture_m2703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_clearFlags_m2704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m2705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m2706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_main_m1577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m2707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_GetAllCameras_m2708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m2713(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m2715(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var;
void CameraCallback_t553_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t793 * tmp;
		tmp = (EditorBrowsableAttribute_t793 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m3537(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Debug_t554_CustomAttributesCacheGenerator_Debug_Break_m120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Debug_t554_CustomAttributesCacheGenerator_Debug_Internal_Log_m2716(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void Debug_t554_CustomAttributesCacheGenerator_Debug_t554_Debug_Internal_Log_m2716_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Debug_t554_CustomAttributesCacheGenerator_Debug_Internal_LogException_m2717(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void Debug_t554_CustomAttributesCacheGenerator_Debug_t554_Debug_Internal_LogException_m2717_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m2743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m2744(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m2745(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m2746(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m2747(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_SetParamsImpl_m2748(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m2749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Display_t557_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m2750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m2751(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2753(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m2754(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetAxisRaw_m1549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetButtonDown_m1548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButton_m1556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m1517(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m1518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_get_mousePosition_m1519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m1521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_get_mousePresent_m1547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_GetTouch_m1555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_get_touchCount_m151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m1832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_get_compositionString_m1752(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Input_t55_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m2756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t406_il2cpp_TypeInfo_var;
void HideFlags_t560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t406 * tmp;
		tmp = (FlagsAttribute_t406 *)il2cpp_codegen_object_new (FlagsAttribute_t406_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m1988(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m2758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_Destroy_m2759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_t33_Object_Destroy_m2759_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_Destroy_m1835(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_t33_Object_DestroyImmediate_m2760_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_DestroyImmediate_m1836(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m2761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_get_name_m88(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_set_name_m146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_set_hideFlags_m1797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_ToString_m220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_Instantiate_m145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void Object_t33_CustomAttributesCacheGenerator_Object_FindObjectOfType_m2767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_get_transform_m87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_get_gameObject_m77(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_GetComponent_m1957(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m2769(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_GetComponent_m3539(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t794 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t794 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m3540(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Component_t49_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m2770(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponent_m2771(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m2772(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponent_m3544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t794 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t794 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m3540(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m2773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_transform_m147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_layer_m1799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_set_layer_m1800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_SetActive_m2774(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m1498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_SendMessage_m2775(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_SendMessage_m2775_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_SendMessage_m2775_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m2776(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_AddComponent_m2777(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1084);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t695 * tmp;
		tmp = (TypeInferenceRuleAttribute_t695 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t695_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m3367(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m2778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t646_il2cpp_TypeInfo_var;
void GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_Internal_CreateGameObject_m2778_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t646 * tmp;
		tmp = (WritableAttribute_t646 *)il2cpp_codegen_object_new (WritableAttribute_t646_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m3202(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m2782(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m2783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m2784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m2785(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m2786(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m2787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m2788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m2789(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_get_parentInternal_m2790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_set_parentInternal_m2791(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_SetParent_m2792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m2793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m2794(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m2795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_get_childCount_m1958(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m1798(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Transform_t30_CustomAttributesCacheGenerator_Transform_GetChild_m1956(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Time_t562_CustomAttributesCacheGenerator_Time_get_deltaTime_m2797(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Time_t562_CustomAttributesCacheGenerator_Time_get_unscaledTime_m1551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Time_t562_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m1591(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Time_t562_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Random_t563_CustomAttributesCacheGenerator_Random_RandomRangeInt_m2799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m2801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_t564_PlayerPrefs_GetString_m2801_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m2802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m2820(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m2821_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m2821_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m1658_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m1658_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m1580_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m1580_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m2822_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m2822_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Physics_t566_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m2823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m2826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_Raycast_m1659(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m1566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m2828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Collider2D_t350_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m2830(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AudioClip_t20_CustomAttributesCacheGenerator_AudioClip_get_length_m82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_get_clip_m107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_set_clip_m114(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_Play_m2846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t692_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_t9_AudioSource_Play_m2846_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t692_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t692 * tmp;
		tmp = (DefaultValueAttribute_t692 *)il2cpp_codegen_object_new (DefaultValueAttribute_t692_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3362(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_Play_m81(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t693 * tmp;
		tmp = (ExcludeFromDocsAttribute_t693 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t693_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m3366(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_set_loop_m78(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void AnimationEvent_t577_CustomAttributesCacheGenerator_AnimationEvent_t577____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void AnimationCurve_t581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_t581_AnimationCurve__ctor_m2872_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m2874(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_Init_m2876(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void AnimatorStateInfo_t578_CustomAttributesCacheGenerator_AnimatorStateInfo_t578____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Animator_t329_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m1896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Animator_t329_CustomAttributesCacheGenerator_Animator_StringToHash_m2895(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Animator_t329_CustomAttributesCacheGenerator_Animator_SetTriggerString_m2896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Animator_t329_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m2897(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void CharacterInfo_t587_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void CharacterInfo_t587_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void CharacterInfo_t587_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t410_il2cpp_TypeInfo_var;
void CharacterInfo_t587_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t410 * tmp;
		tmp = (ObsoleteAttribute_t410 *)il2cpp_codegen_object_new (ObsoleteAttribute_t410_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2008(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Font_t176_CustomAttributesCacheGenerator_Font_get_material_m1908(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Font_t176_CustomAttributesCacheGenerator_Font_HasCharacter_m1779(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Font_t176_CustomAttributesCacheGenerator_Font_get_dynamic_m1911(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Font_t176_CustomAttributesCacheGenerator_Font_get_fontSize_m1913(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var;
void FontTextureRebuildCallback_t588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t793 * tmp;
		tmp = (EditorBrowsableAttribute_t793 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m3537(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_Init_m2925(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m2926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m1795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m2930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m2931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m2932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m2933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m2934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m2935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m1772(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m2936(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m2937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m1818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_renderMode_m1652(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_renderMode_m128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m1929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m1668(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m1912(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m1933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m1687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m1934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m1638(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_pixelPerfect_m129(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m1654(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m1653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_sortingOrder_m2948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m1667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m1619(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void Canvas_t48_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m1907(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_alpha_m138(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m1886(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_interactable_m139(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m2950(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_blocksRaycasts_m140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m1637(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_ignoreParentGroups_m141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m2953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m1641(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m1968(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m1631(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m2954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m2955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m1624(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m1620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2957(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2959(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t639_il2cpp_TypeInfo_var;
void RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m1640(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t639_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t639 * tmp;
		tmp = (WrapperlessIcall_t639 *)il2cpp_codegen_object_new (WrapperlessIcall_t639_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m3193(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_Request_get_sourceId_m2964(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_Request_get_appId_m2965(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Request_t592_CustomAttributesCacheGenerator_Request_get_domain_m2966(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_Response_get_success_m2975(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_Response_set_success_m2976(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_Response_get_extendedInfo_m2977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Response_t594_CustomAttributesCacheGenerator_Response_set_extendedInfo_m2978(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m2983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m2984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m2985(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m2986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m2987(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m2988(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m2989(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m2990(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m2991(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m2994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m2995(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m2996(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m2997(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m2998(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m2999(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m3000(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m3001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m3002(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m3003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m3004(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m3005(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m3009(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m3010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m3011(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m3012(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m3015(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m3016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m3017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m3018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m3019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m3020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m3021(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m3022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m3023(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m3024(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m3025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m3026(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t601_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t601_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m3030(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t601_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m3031(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m3034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m3035(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m3036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m3037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m3040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m3041(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m3042(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m3043(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m3044(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m3045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m3046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m3047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m3050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m3051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m3052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m3053(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m3054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m3055(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m3059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m3060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_name_m3061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_name_m3062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m3063(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m3064(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m3065(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m3066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m3067(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m3068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m3069(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m3070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m3071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m3072(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m3073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchResponse_t608_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchResponse_t608_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m3077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void ListMatchResponse_t608_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m3078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
extern TypeInfo* DefaultValueAttribute_t796_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t609_il2cpp_TypeInfo_var;
void AppID_t609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t796_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		AppID_t609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(966);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t796 * tmp;
		tmp = (DefaultValueAttribute_t796 *)il2cpp_codegen_object_new (DefaultValueAttribute_t796_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3552(tmp, Box(AppID_t609_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
extern TypeInfo* DefaultValueAttribute_t796_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t610_il2cpp_TypeInfo_var;
void SourceID_t610_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t796_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		SourceID_t610_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(965);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t796 * tmp;
		tmp = (DefaultValueAttribute_t796 *)il2cpp_codegen_object_new (DefaultValueAttribute_t796_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3552(tmp, Box(SourceID_t610_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
extern TypeInfo* DefaultValueAttribute_t796_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t611_il2cpp_TypeInfo_var;
void NetworkID_t611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t796_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		NetworkID_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(970);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t796 * tmp;
		tmp = (DefaultValueAttribute_t796 *)il2cpp_codegen_object_new (DefaultValueAttribute_t796_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3552(tmp, Box(NetworkID_t611_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
extern TypeInfo* DefaultValueAttribute_t796_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t612_il2cpp_TypeInfo_var;
void NodeID_t612_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t796_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1090);
		NodeID_t612_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(971);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint16_t _tmp_0 = 0;
		DefaultValueAttribute_t796 * tmp;
		tmp = (DefaultValueAttribute_t796 *)il2cpp_codegen_object_new (DefaultValueAttribute_t796_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m3552(tmp, Box(NodeID_t612_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void NetworkMatch_t618_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m3553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3559(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3560(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m3562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var;
void JsonArray_t619_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t793 * tmp;
		tmp = (EditorBrowsableAttribute_t793 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m3537(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var;
void JsonObject_t621_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t793 * tmp;
		tmp = (EditorBrowsableAttribute_t793 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m3537(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
void SimpleJson_t624_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
extern TypeInfo* SuppressMessageAttribute_t800_il2cpp_TypeInfo_var;
void SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m3122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t800 * tmp;
		tmp = (SuppressMessageAttribute_t800 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t800_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m3564(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m3565(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t800_il2cpp_TypeInfo_var;
void SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_NextToken_m3134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t800 * tmp;
		tmp = (SuppressMessageAttribute_t800 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t800_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m3564(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Maintainability"), il2cpp_codegen_string_new_wrapper("CA1502:AvoidExcessiveComplexity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var;
void SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_t624____PocoJsonSerializerStrategy_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t793 * tmp;
		tmp = (EditorBrowsableAttribute_t793 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t793_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m3537(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t622_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t800_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t622_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m3566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t800 * tmp;
		tmp = (SuppressMessageAttribute_t800 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t800_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m3564(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m3565(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t800_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m3151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t800 * tmp;
		tmp = (SuppressMessageAttribute_t800 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t800_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m3564(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m3565(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t800_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3152(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t800 * tmp;
		tmp = (SuppressMessageAttribute_t800 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t800_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m3564(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m3565(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var;
void ReflectionUtils_t638_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t799 * tmp;
		tmp = (GeneratedCodeAttribute_t799 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t799_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m3563(tmp, il2cpp_codegen_string_new_wrapper("reflection-utils"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetConstructorInfo_m3177_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetContructor_m3182_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetConstructorByReflection_m3184_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t423_il2cpp_TypeInfo_var;
void ThreadSafeDictionary_2_t801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t423_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t423 * tmp;
		tmp = (DefaultMemberAttribute_t423 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t423_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2071(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void ConstructorDelegate_t631_CustomAttributesCacheGenerator_ConstructorDelegate_t631_ConstructorDelegate_Invoke_m3162_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void ConstructorDelegate_t631_CustomAttributesCacheGenerator_ConstructorDelegate_t631_ConstructorDelegate_BeginInvoke_m3163_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetConstructorByReflectionU3Ec__AnonStorey1_t633_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void IL2CPPStructAlignmentAttribute_t640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 8, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DisallowMultipleComponent_t417_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void RequireComponent_t61_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void WritableAttribute_t646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 2048, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void AssemblyIsEditorAssembly_t647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_Achievement_get_id_m3235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_Achievement_set_id_m3236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m3237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Achievement_t659_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m3238(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AchievementDescription_t660_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AchievementDescription_t660_CustomAttributesCacheGenerator_AchievementDescription_get_id_m3245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void AchievementDescription_t660_CustomAttributesCacheGenerator_AchievementDescription_set_id_m3246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_Score_get_leaderboardID_m3255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_Score_set_leaderboardID_m3256(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_Score_get_value_m3257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Score_t661_CustomAttributesCacheGenerator_Score_set_value_m3258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_id_m3266(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_id_m3267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m3268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m3269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_range_m3270(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_range_m3271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m3272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m3273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void PropertyAttribute_t672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void TooltipAttribute_t420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void SpaceAttribute_t418_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void RangeAttribute_t415_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void TextAreaAttribute_t421_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void SelectionBaseAttribute_t419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var;
void StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m3286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t794 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t794 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m3540(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var;
void StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m3289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t794 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t794 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m3540(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var;
void StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m3291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t794 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t794 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t794_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m3540(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void SharedBetweenAnimatorsAttribute_t675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void ArgumentCache_t681_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PersistentCall_t685_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PersistentCall_t685_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PersistentCall_t685_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PersistentCall_t685_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void PersistentCall_t685_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void PersistentCallGroup_t687_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var;
void UnityEventBase_t690_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t409 * tmp;
		tmp = (FormerlySerializedAsAttribute_t409 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t409_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2007(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void UnityEventBase_t690_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t408_il2cpp_TypeInfo_var;
void UserAuthorizationDialog_t691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t408_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t408 * tmp;
		tmp = (AddComponentMenu_t408 *)il2cpp_codegen_object_new (AddComponentMenu_t408_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2006(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 18432, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ExcludeFromDocsAttribute_t693_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void FormerlySerializedAsAttribute_t409_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 256, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m3592(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m3591(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void TypeInferenceRuleAttribute_t695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_Assembly_AttributeGenerators[658] = 
{
	NULL,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	AssetBundleCreateRequest_t471_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m2164,
	AssetBundleCreateRequest_t471_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m2165,
	AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m2169,
	AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m2170,
	AssetBundle_t473_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m2171,
	LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_LayerToName_m2174,
	LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m2175,
	LayerMask_t158_CustomAttributesCacheGenerator_LayerMask_t158_LayerMask_GetMask_m2176_Arg0_ParameterInfo,
	RuntimePlatform_t476_CustomAttributesCacheGenerator_NaCl,
	RuntimePlatform_t476_CustomAttributesCacheGenerator_FlashPlayer,
	RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerX86,
	RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerX64,
	RuntimePlatform_t476_CustomAttributesCacheGenerator_MetroPlayerARM,
	SystemInfo_t477_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m2177,
	Coroutine_t228_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m2180,
	ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m2183,
	ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_t480_ScriptableObject_Internal_CreateScriptableObject_m2183_Arg0_ParameterInfo,
	ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m2184,
	ScriptableObject_t480_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m2186,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m2191,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m2192,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m2193,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m2194,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m2195,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m2196,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m2197,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m2198,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m2199,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m2200,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m2201,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m2202,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m2203,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m2204,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m2205,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m2206,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2207,
	GameCenterPlatform_t490_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2211,
	GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m2255,
	GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m2256,
	GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m2257,
	GcLeaderboard_t492_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m2258,
	Renderer_t494_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m1573,
	Renderer_t494_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m1574,
	Screen_t495_CustomAttributesCacheGenerator_Screen_get_width_m1655,
	Screen_t495_CustomAttributesCacheGenerator_Screen_get_height_m1656,
	Screen_t495_CustomAttributesCacheGenerator_Screen_get_dpi_m1932,
	Texture_t233_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m2280,
	Texture_t233_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m2281,
	Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m2285,
	Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_t181_Texture2D_Internal_Create_m2285_Arg0_ParameterInfo,
	Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m1623,
	Texture2D_t181_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m1719,
	RenderTexture_t496_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m2286,
	RenderTexture_t496_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m2287,
	GUILayer_t499_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m2291,
	Gradient_t502_CustomAttributesCacheGenerator_Gradient_Init_m2295,
	Gradient_t502_CustomAttributesCacheGenerator_Gradient_Cleanup_m2296,
	GUI_t507_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField,
	GUI_t507_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField,
	GUI_t507_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m2304,
	GUI_t507_CustomAttributesCacheGenerator_GUI_set_changed_m2307,
	GUILayoutUtility_t514_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m2320,
	GUILayoutUtility_t514_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m2322,
	GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m2353,
	GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m2354,
	GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m2356,
	GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m2358,
	GUIUtility_t521_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m2362,
	GUISettings_t522_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord,
	GUISettings_t522_CustomAttributesCacheGenerator_m_TripleClickSelectsLine,
	GUISettings_t522_CustomAttributesCacheGenerator_m_CursorColor,
	GUISettings_t522_CustomAttributesCacheGenerator_m_CursorFlashSpeed,
	GUISettings_t522_CustomAttributesCacheGenerator_m_SelectionColor,
	GUISkin_t505_CustomAttributesCacheGenerator,
	GUISkin_t505_CustomAttributesCacheGenerator_m_Font,
	GUISkin_t505_CustomAttributesCacheGenerator_m_box,
	GUISkin_t505_CustomAttributesCacheGenerator_m_button,
	GUISkin_t505_CustomAttributesCacheGenerator_m_toggle,
	GUISkin_t505_CustomAttributesCacheGenerator_m_label,
	GUISkin_t505_CustomAttributesCacheGenerator_m_textField,
	GUISkin_t505_CustomAttributesCacheGenerator_m_textArea,
	GUISkin_t505_CustomAttributesCacheGenerator_m_window,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalSlider,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalSliderThumb,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalSlider,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalSliderThumb,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbar,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton,
	GUISkin_t505_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbar,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarThumb,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton,
	GUISkin_t505_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton,
	GUISkin_t505_CustomAttributesCacheGenerator_m_ScrollView,
	GUISkin_t505_CustomAttributesCacheGenerator_m_CustomStyles,
	GUISkin_t505_CustomAttributesCacheGenerator_m_Settings,
	GUIContent_t373_CustomAttributesCacheGenerator_m_Text,
	GUIContent_t373_CustomAttributesCacheGenerator_m_Image,
	GUIContent_t373_CustomAttributesCacheGenerator_m_Tooltip,
	GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_Init_m2429,
	GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m2430,
	GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m2431,
	GUIStyleState_t526_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m2432,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_Init_m2437,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_Cleanup_m2438,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_left_m1949,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_left_m2439,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_right_m2440,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_right_m2441,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_top_m1950,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_top_m2442,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_bottom_m2443,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_set_bottom_m2444,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m1942,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_get_vertical_m1944,
	RectOffset_t284_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m2446,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Init_m2451,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m2452,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_name_m2453,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_name_m2454,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m2456,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m2459,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m2460,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m2461,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m2462,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m2463,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m2464,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m2465,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m2466,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m2468,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m2472,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m2474,
	GUIStyle_t513_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m2476,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m2479,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2481,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m1830,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m1831,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg1_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg2_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg3_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg4_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg5_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_t221_TouchScreenKeyboard_Open_m2482_Arg6_ParameterInfo,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m1749,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m1750,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m1829,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m1748,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m1828,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m1766,
	TouchScreenKeyboard_t221_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m1762,
	Event_t229_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	Event_t229_CustomAttributesCacheGenerator_Event_Init_m2483,
	Event_t229_CustomAttributesCacheGenerator_Event_Cleanup_m2485,
	Event_t229_CustomAttributesCacheGenerator_Event_get_rawType_m1780,
	Event_t229_CustomAttributesCacheGenerator_Event_get_type_m2486,
	Event_t229_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m2488,
	Event_t229_CustomAttributesCacheGenerator_Event_get_modifiers_m1776,
	Event_t229_CustomAttributesCacheGenerator_Event_get_character_m1778,
	Event_t229_CustomAttributesCacheGenerator_Event_get_commandName_m2489,
	Event_t229_CustomAttributesCacheGenerator_Event_get_keyCode_m1777,
	Event_t229_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m2491,
	Event_t229_CustomAttributesCacheGenerator_Event_PopEvent_m1781,
	EventModifiers_t531_CustomAttributesCacheGenerator,
	Vector2_t53_CustomAttributesCacheGenerator,
	Vector3_t138_CustomAttributesCacheGenerator,
	Color_t163_CustomAttributesCacheGenerator,
	Color32_t335_CustomAttributesCacheGenerator,
	Quaternion_t362_CustomAttributesCacheGenerator,
	Quaternion_t362_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m2519,
	Matrix4x4_t383_CustomAttributesCacheGenerator,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m2534,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m2536,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m2538,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m2541,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m2553,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m2556,
	Matrix4x4_t383_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m2557,
	Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m2574,
	Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m2577,
	Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m2580,
	Bounds_t247_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2584,
	Vector4_t327_CustomAttributesCacheGenerator,
	Mathf_t340_CustomAttributesCacheGenerator_Mathf_t340_Mathf_SmoothDamp_m1857_Arg4_ParameterInfo,
	Mathf_t340_CustomAttributesCacheGenerator_Mathf_t340_Mathf_SmoothDamp_m1857_Arg5_ParameterInfo,
	DrivenTransformProperties_t533_CustomAttributesCacheGenerator,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m2613,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m2614,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m2615,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m2616,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m2617,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m2618,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m2619,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m2620,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m2621,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m2622,
	RectTransform_t183_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m2623,
	Resources_t537_CustomAttributesCacheGenerator_Resources_Load_m2629,
	SerializePrivateVariables_t538_CustomAttributesCacheGenerator,
	Shader_t540_CustomAttributesCacheGenerator_Shader_PropertyToID_m2631,
	Material_t180_CustomAttributesCacheGenerator_Material_GetTexture_m2633,
	Material_t180_CustomAttributesCacheGenerator_Material_SetFloat_m2635,
	Material_t180_CustomAttributesCacheGenerator_Material_HasProperty_m2636,
	Material_t180_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m2637,
	Material_t180_CustomAttributesCacheGenerator_Material_t180_Material_Internal_CreateWithMaterial_m2637_Arg0_ParameterInfo,
	SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator,
	SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2640,
	SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2643,
	SphericalHarmonicsL2_t541_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2646,
	Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_rect_m1691,
	Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m1686,
	Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_texture_m1683,
	Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_textureRect_m1716,
	Sprite_t201_CustomAttributesCacheGenerator_Sprite_get_border_m1684,
	DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m1702,
	DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m1701,
	DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_GetPadding_m1690,
	DataUtility_t542_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m2656,
	WWW_t543_CustomAttributesCacheGenerator_WWW_DestroyWWW_m2660,
	WWW_t543_CustomAttributesCacheGenerator_WWW_InitWWW_m2661,
	WWW_t543_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m2663,
	WWW_t543_CustomAttributesCacheGenerator_WWW_get_bytes_m2667,
	WWW_t543_CustomAttributesCacheGenerator_WWW_get_error_m2668,
	WWW_t543_CustomAttributesCacheGenerator_WWW_get_isDone_m2669,
	WWWForm_t547_CustomAttributesCacheGenerator_WWWForm_AddField_m2673,
	WWWForm_t547_CustomAttributesCacheGenerator_WWWForm_t547_WWWForm_AddField_m2674_Arg2_ParameterInfo,
	WWWTranscoder_t548_CustomAttributesCacheGenerator_WWWTranscoder_t548_WWWTranscoder_QPEncode_m2681_Arg1_ParameterInfo,
	WWWTranscoder_t548_CustomAttributesCacheGenerator_WWWTranscoder_t548_WWWTranscoder_SevenBitClean_m2684_Arg1_ParameterInfo,
	CacheIndex_t549_CustomAttributesCacheGenerator,
	UnityString_t550_CustomAttributesCacheGenerator_UnityString_t550_UnityString_Format_m2686_Arg1_ParameterInfo,
	AsyncOperation_t472_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m2688,
	Application_t552_CustomAttributesCacheGenerator_Application_get_isPlaying_m1833,
	Application_t552_CustomAttributesCacheGenerator_Application_get_isEditor_m1837,
	Application_t552_CustomAttributesCacheGenerator_Application_get_platform_m1747,
	Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_get_enabled_m84,
	Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_set_enabled_m104,
	Behaviour_t394_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m1497,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m1563,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m1562,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_depth_m1457,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_cullingMask_m1578,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_eventMask_m2700,
	Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m2701,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_targetTexture_m2703,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_clearFlags_m2704,
	Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m2705,
	Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m2706,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_main_m1577,
	Camera_t156_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m2707,
	Camera_t156_CustomAttributesCacheGenerator_Camera_GetAllCameras_m2708,
	Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m2713,
	Camera_t156_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m2715,
	CameraCallback_t553_CustomAttributesCacheGenerator,
	Debug_t554_CustomAttributesCacheGenerator_Debug_Break_m120,
	Debug_t554_CustomAttributesCacheGenerator_Debug_Internal_Log_m2716,
	Debug_t554_CustomAttributesCacheGenerator_Debug_t554_Debug_Internal_Log_m2716_Arg2_ParameterInfo,
	Debug_t554_CustomAttributesCacheGenerator_Debug_Internal_LogException_m2717,
	Debug_t554_CustomAttributesCacheGenerator_Debug_t554_Debug_Internal_LogException_m2717_Arg1_ParameterInfo,
	Display_t557_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m2743,
	Display_t557_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m2744,
	Display_t557_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m2745,
	Display_t557_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m2746,
	Display_t557_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m2747,
	Display_t557_CustomAttributesCacheGenerator_Display_SetParamsImpl_m2748,
	Display_t557_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m2749,
	Display_t557_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m2750,
	MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m2751,
	MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2753,
	MonoBehaviour_t22_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m2754,
	Input_t55_CustomAttributesCacheGenerator_Input_GetAxisRaw_m1549,
	Input_t55_CustomAttributesCacheGenerator_Input_GetButtonDown_m1548,
	Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButton_m1556,
	Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m1517,
	Input_t55_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m1518,
	Input_t55_CustomAttributesCacheGenerator_Input_get_mousePosition_m1519,
	Input_t55_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m1521,
	Input_t55_CustomAttributesCacheGenerator_Input_get_mousePresent_m1547,
	Input_t55_CustomAttributesCacheGenerator_Input_GetTouch_m1555,
	Input_t55_CustomAttributesCacheGenerator_Input_get_touchCount_m151,
	Input_t55_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m1832,
	Input_t55_CustomAttributesCacheGenerator_Input_get_compositionString_m1752,
	Input_t55_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m2756,
	HideFlags_t560_CustomAttributesCacheGenerator,
	Object_t33_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m2758,
	Object_t33_CustomAttributesCacheGenerator_Object_Destroy_m2759,
	Object_t33_CustomAttributesCacheGenerator_Object_t33_Object_Destroy_m2759_Arg1_ParameterInfo,
	Object_t33_CustomAttributesCacheGenerator_Object_Destroy_m1835,
	Object_t33_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2760,
	Object_t33_CustomAttributesCacheGenerator_Object_t33_Object_DestroyImmediate_m2760_Arg1_ParameterInfo,
	Object_t33_CustomAttributesCacheGenerator_Object_DestroyImmediate_m1836,
	Object_t33_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m2761,
	Object_t33_CustomAttributesCacheGenerator_Object_get_name_m88,
	Object_t33_CustomAttributesCacheGenerator_Object_set_name_m146,
	Object_t33_CustomAttributesCacheGenerator_Object_set_hideFlags_m1797,
	Object_t33_CustomAttributesCacheGenerator_Object_ToString_m220,
	Object_t33_CustomAttributesCacheGenerator_Object_Instantiate_m145,
	Object_t33_CustomAttributesCacheGenerator_Object_FindObjectOfType_m2767,
	Component_t49_CustomAttributesCacheGenerator_Component_get_transform_m87,
	Component_t49_CustomAttributesCacheGenerator_Component_get_gameObject_m77,
	Component_t49_CustomAttributesCacheGenerator_Component_GetComponent_m1957,
	Component_t49_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m2769,
	Component_t49_CustomAttributesCacheGenerator_Component_GetComponent_m3539,
	Component_t49_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m2770,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponent_m2771,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m2772,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponent_m3544,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m2773,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_transform_m147,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_layer_m1799,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_set_layer_m1800,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_SetActive_m2774,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m1498,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_SendMessage_m2775,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_SendMessage_m2775_Arg1_ParameterInfo,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_SendMessage_m2775_Arg2_ParameterInfo,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m2776,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_AddComponent_m2777,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m2778,
	GameObject_t5_CustomAttributesCacheGenerator_GameObject_t5_GameObject_Internal_CreateGameObject_m2778_Arg0_ParameterInfo,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m2782,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m2783,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m2784,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m2785,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m2786,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m2787,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m2788,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m2789,
	Transform_t30_CustomAttributesCacheGenerator_Transform_get_parentInternal_m2790,
	Transform_t30_CustomAttributesCacheGenerator_Transform_set_parentInternal_m2791,
	Transform_t30_CustomAttributesCacheGenerator_Transform_SetParent_m2792,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m2793,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m2794,
	Transform_t30_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m2795,
	Transform_t30_CustomAttributesCacheGenerator_Transform_get_childCount_m1958,
	Transform_t30_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m1798,
	Transform_t30_CustomAttributesCacheGenerator_Transform_GetChild_m1956,
	Time_t562_CustomAttributesCacheGenerator_Time_get_deltaTime_m2797,
	Time_t562_CustomAttributesCacheGenerator_Time_get_unscaledTime_m1551,
	Time_t562_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m1591,
	Time_t562_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m142,
	Random_t563_CustomAttributesCacheGenerator_Random_RandomRangeInt_m2799,
	PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m2801,
	PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_t564_PlayerPrefs_GetString_m2801_Arg1_ParameterInfo,
	PlayerPrefs_t564_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m2802,
	Physics_t566_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m2820,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m2821_Arg3_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m2821_Arg4_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m1658_Arg2_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_Raycast_m1658_Arg3_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m1580_Arg1_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m1580_Arg2_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m2822_Arg2_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_t566_Physics_RaycastAll_m2822_Arg3_ParameterInfo,
	Physics_t566_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m2823,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m2826,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_Raycast_m1659,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg2_ParameterInfo,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg3_ParameterInfo,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg4_ParameterInfo,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_t348_Physics2D_Raycast_m2827_Arg5_ParameterInfo,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m1566,
	Physics2D_t348_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m2828,
	Collider2D_t350_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m2830,
	AudioClip_t20_CustomAttributesCacheGenerator_AudioClip_get_length_m82,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_get_clip_m107,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_set_clip_m114,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_Play_m2846,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_t9_AudioSource_Play_m2846_Arg0_ParameterInfo,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_Play_m81,
	AudioSource_t9_CustomAttributesCacheGenerator_AudioSource_set_loop_m78,
	AnimationEvent_t577_CustomAttributesCacheGenerator_AnimationEvent_t577____data_PropertyInfo,
	AnimationCurve_t581_CustomAttributesCacheGenerator,
	AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_t581_AnimationCurve__ctor_m2872_Arg0_ParameterInfo,
	AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m2874,
	AnimationCurve_t581_CustomAttributesCacheGenerator_AnimationCurve_Init_m2876,
	AnimatorStateInfo_t578_CustomAttributesCacheGenerator_AnimatorStateInfo_t578____nameHash_PropertyInfo,
	Animator_t329_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m1896,
	Animator_t329_CustomAttributesCacheGenerator_Animator_StringToHash_m2895,
	Animator_t329_CustomAttributesCacheGenerator_Animator_SetTriggerString_m2896,
	Animator_t329_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m2897,
	CharacterInfo_t587_CustomAttributesCacheGenerator_uv,
	CharacterInfo_t587_CustomAttributesCacheGenerator_vert,
	CharacterInfo_t587_CustomAttributesCacheGenerator_width,
	CharacterInfo_t587_CustomAttributesCacheGenerator_flipped,
	Font_t176_CustomAttributesCacheGenerator_Font_get_material_m1908,
	Font_t176_CustomAttributesCacheGenerator_Font_HasCharacter_m1779,
	Font_t176_CustomAttributesCacheGenerator_Font_get_dynamic_m1911,
	Font_t176_CustomAttributesCacheGenerator_Font_get_fontSize_m1913,
	FontTextureRebuildCallback_t588_CustomAttributesCacheGenerator,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_Init_m2925,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m2926,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2929,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m1795,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m2930,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m2931,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m2932,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m2933,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m2934,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m2935,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m1772,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m2936,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m2937,
	TextGenerator_t226_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m1818,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_renderMode_m1652,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_renderMode_m128,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m1929,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m1668,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m1912,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m1933,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m1687,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m1934,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m1638,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_pixelPerfect_m129,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m1654,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m1653,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_set_sortingOrder_m2948,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m1667,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m1619,
	Canvas_t48_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m1907,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_alpha_m138,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m1886,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_interactable_m139,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m2950,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_blocksRaycasts_m140,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m1637,
	CanvasGroup_t52_CustomAttributesCacheGenerator_CanvasGroup_set_ignoreParentGroups_m141,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m2953,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m1641,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m1968,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m1631,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m2954,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m2955,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m1624,
	CanvasRenderer_t184_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m1620,
	RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2957,
	RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2959,
	RectTransformUtility_t360_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m1640,
	Request_t592_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField,
	Request_t592_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField,
	Request_t592_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField,
	Request_t592_CustomAttributesCacheGenerator_Request_get_sourceId_m2964,
	Request_t592_CustomAttributesCacheGenerator_Request_get_appId_m2965,
	Request_t592_CustomAttributesCacheGenerator_Request_get_domain_m2966,
	Response_t594_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField,
	Response_t594_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField,
	Response_t594_CustomAttributesCacheGenerator_Response_get_success_m2975,
	Response_t594_CustomAttributesCacheGenerator_Response_set_success_m2976,
	Response_t594_CustomAttributesCacheGenerator_Response_get_extendedInfo_m2977,
	Response_t594_CustomAttributesCacheGenerator_Response_set_extendedInfo_m2978,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m2983,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m2984,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m2985,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m2986,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m2987,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m2988,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m2989,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m2990,
	CreateMatchRequest_t597_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m2991,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m2994,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m2995,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m2996,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m2997,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m2998,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m2999,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m3000,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m3001,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m3002,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m3003,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m3004,
	CreateMatchResponse_t598_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m3005,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m3009,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m3010,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m3011,
	JoinMatchRequest_t599_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m3012,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m3015,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m3016,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m3017,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m3018,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m3019,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m3020,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m3021,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m3022,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m3023,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m3024,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m3025,
	JoinMatchResponse_t600_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m3026,
	DestroyMatchRequest_t601_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DestroyMatchRequest_t601_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m3030,
	DestroyMatchRequest_t601_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m3031,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m3034,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m3035,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m3036,
	DropConnectionRequest_t602_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m3037,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m3040,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m3041,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m3042,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m3043,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m3044,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m3045,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m3046,
	ListMatchRequest_t603_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m3047,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m3050,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m3051,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m3052,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m3053,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m3054,
	MatchDirectConnectInfo_t604_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m3055,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m3059,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m3060,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_name_m3061,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_name_m3062,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m3063,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m3064,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m3065,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m3066,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m3067,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m3068,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m3069,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m3070,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m3071,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m3072,
	MatchDesc_t606_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m3073,
	ListMatchResponse_t608_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField,
	ListMatchResponse_t608_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m3077,
	ListMatchResponse_t608_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m3078,
	AppID_t609_CustomAttributesCacheGenerator,
	SourceID_t610_CustomAttributesCacheGenerator,
	NetworkID_t611_CustomAttributesCacheGenerator,
	NodeID_t612_CustomAttributesCacheGenerator,
	NetworkMatch_t618_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m3553,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3559,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3560,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t798_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m3562,
	JsonArray_t619_CustomAttributesCacheGenerator,
	JsonObject_t621_CustomAttributesCacheGenerator,
	SimpleJson_t624_CustomAttributesCacheGenerator,
	SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m3122,
	SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_NextToken_m3134,
	SimpleJson_t624_CustomAttributesCacheGenerator_SimpleJson_t624____PocoJsonSerializerStrategy_PropertyInfo,
	IJsonSerializerStrategy_t622_CustomAttributesCacheGenerator,
	IJsonSerializerStrategy_t622_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m3566,
	PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator,
	PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m3151,
	PocoJsonSerializerStrategy_t623_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m3152,
	ReflectionUtils_t638_CustomAttributesCacheGenerator,
	ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetConstructorInfo_m3177_Arg1_ParameterInfo,
	ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetContructor_m3182_Arg1_ParameterInfo,
	ReflectionUtils_t638_CustomAttributesCacheGenerator_ReflectionUtils_t638_ReflectionUtils_GetConstructorByReflection_m3184_Arg1_ParameterInfo,
	ThreadSafeDictionary_2_t801_CustomAttributesCacheGenerator,
	ConstructorDelegate_t631_CustomAttributesCacheGenerator_ConstructorDelegate_t631_ConstructorDelegate_Invoke_m3162_Arg0_ParameterInfo,
	ConstructorDelegate_t631_CustomAttributesCacheGenerator_ConstructorDelegate_t631_ConstructorDelegate_BeginInvoke_m3163_Arg0_ParameterInfo,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_t633_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t634_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t635_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t636_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t637_CustomAttributesCacheGenerator,
	IL2CPPStructAlignmentAttribute_t640_CustomAttributesCacheGenerator,
	DisallowMultipleComponent_t417_CustomAttributesCacheGenerator,
	RequireComponent_t61_CustomAttributesCacheGenerator,
	WritableAttribute_t646_CustomAttributesCacheGenerator,
	AssemblyIsEditorAssembly_t647_CustomAttributesCacheGenerator,
	Achievement_t659_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t659_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t659_CustomAttributesCacheGenerator_Achievement_get_id_m3235,
	Achievement_t659_CustomAttributesCacheGenerator_Achievement_set_id_m3236,
	Achievement_t659_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m3237,
	Achievement_t659_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m3238,
	AchievementDescription_t660_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t660_CustomAttributesCacheGenerator_AchievementDescription_get_id_m3245,
	AchievementDescription_t660_CustomAttributesCacheGenerator_AchievementDescription_set_id_m3246,
	Score_t661_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t661_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t661_CustomAttributesCacheGenerator_Score_get_leaderboardID_m3255,
	Score_t661_CustomAttributesCacheGenerator_Score_set_leaderboardID_m3256,
	Score_t661_CustomAttributesCacheGenerator_Score_get_value_m3257,
	Score_t661_CustomAttributesCacheGenerator_Score_set_value_m3258,
	Leaderboard_t491_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t491_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t491_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t491_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_id_m3266,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_id_m3267,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m3268,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m3269,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_range_m3270,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_range_m3271,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m3272,
	Leaderboard_t491_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m3273,
	PropertyAttribute_t672_CustomAttributesCacheGenerator,
	TooltipAttribute_t420_CustomAttributesCacheGenerator,
	SpaceAttribute_t418_CustomAttributesCacheGenerator,
	RangeAttribute_t415_CustomAttributesCacheGenerator,
	TextAreaAttribute_t421_CustomAttributesCacheGenerator,
	SelectionBaseAttribute_t419_CustomAttributesCacheGenerator,
	StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m3286,
	StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m3289,
	StackTraceUtility_t674_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m3291,
	SharedBetweenAnimatorsAttribute_t675_CustomAttributesCacheGenerator,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t681_CustomAttributesCacheGenerator_m_BoolArgument,
	PersistentCall_t685_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t685_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t685_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t685_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t685_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t687_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t690_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t690_CustomAttributesCacheGenerator_m_TypeName,
	UserAuthorizationDialog_t691_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t692_CustomAttributesCacheGenerator,
	ExcludeFromDocsAttribute_t693_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t409_CustomAttributesCacheGenerator,
	TypeInferenceRuleAttribute_t695_CustomAttributesCacheGenerator,
};
