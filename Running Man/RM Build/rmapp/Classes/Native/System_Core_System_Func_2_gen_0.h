﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip
struct AudioClip_t20;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.AudioClip,System.Boolean>
struct  Func_2_t40  : public MulticastDelegate_t216
{
};
