﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>
struct ReadOnlyCollection_1_t2450;
// AudioPooler/objectSource
struct objectSource_t8;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<AudioPooler/objectSource>
struct IList_1_t2449;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// AudioPooler/objectSource[]
struct objectSourceU5BU5D_t2448;
// System.Collections.Generic.IEnumerator`1<AudioPooler/objectSource>
struct IEnumerator_1_t3035;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m11561(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m11356_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11562(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2450 *, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11357_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11563(__this, method) (( void (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11358_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11564(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11359_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11565(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11360_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11566(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11361_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11567(__this, ___index, method) (( objectSource_t8 * (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11362_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11568(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11363_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11569(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11364_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11570(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11365_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11571(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11366_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m11572(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m11367_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m11573(__this, method) (( void (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m11368_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m11574(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m11369_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11575(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11370_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m11576(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m11371_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m11577(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m11372_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11578(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11373_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11579(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11374_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11580(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11375_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11581(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11376_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11582(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11377_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m11583(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m11378_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m11584(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m11379_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::Contains(T)
#define ReadOnlyCollection_1_Contains_m11585(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m11380_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m11586(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, objectSourceU5BU5D_t2448*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m11381_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m11587(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m11382_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m11588(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, objectSource_t8 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m11383_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::get_Count()
#define ReadOnlyCollection_1_get_Count_m11589(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m11384_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AudioPooler/objectSource>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m11590(__this, ___index, method) (( objectSource_t8 * (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m11385_gshared)(__this, ___index, method)
