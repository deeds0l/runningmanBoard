﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct KeyValuePair_2_t2877;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17459(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, String_t*, KeyValuePair_2_t767 , const MethodInfo*))KeyValuePair_2__ctor_m16919_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m17460(__this, method) (( String_t* (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_get_Key_m16920_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17461(__this, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m16921_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m17462(__this, method) (( KeyValuePair_2_t767  (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_get_Value_m16922_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17463(__this, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, KeyValuePair_2_t767 , const MethodInfo*))KeyValuePair_2_set_Value_m16923_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m17464(__this, method) (( String_t* (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_ToString_m16924_gshared)(__this, method)
