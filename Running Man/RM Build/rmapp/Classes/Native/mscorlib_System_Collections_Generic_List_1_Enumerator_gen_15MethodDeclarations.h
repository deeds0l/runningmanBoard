﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
struct Enumerator_t2589;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t223;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t354;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m13465(__this, ___l, method) (( void (*) (Enumerator_t2589 *, List_1_t354 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13466(__this, method) (( Object_t * (*) (Enumerator_t2589 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::Dispose()
#define Enumerator_Dispose_m13467(__this, method) (( void (*) (Enumerator_t2589 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::VerifyState()
#define Enumerator_VerifyState_m13468(__this, method) (( void (*) (Enumerator_t2589 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::MoveNext()
#define Enumerator_MoveNext_m13469(__this, method) (( bool (*) (Enumerator_t2589 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::get_Current()
#define Enumerator_get_Current_m13470(__this, method) (( Text_t223 * (*) (Enumerator_t2589 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
