﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyValuePair_2_t2847;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t631;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17019(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2847 *, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m17020(__this, method) (( Type_t * (*) (KeyValuePair_2_t2847 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17021(__this, ___value, method) (( void (*) (KeyValuePair_2_t2847 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m17022(__this, method) (( ConstructorDelegate_t631 * (*) (KeyValuePair_2_t2847 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17023(__this, ___value, method) (( void (*) (KeyValuePair_2_t2847 *, ConstructorDelegate_t631 *, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToString()
#define KeyValuePair_2_ToString_m17024(__this, method) (( String_t* (*) (KeyValuePair_2_t2847 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
