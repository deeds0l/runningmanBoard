﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2614;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t224;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3104;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t385;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m13765_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13765(__this, method) (( void (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1__ctor_m13765_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13766_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13766(__this, method) (( bool (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13766_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13767_gshared (Collection_1_t2614 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13767(__this, ___array, ___index, method) (( void (*) (Collection_1_t2614 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13767_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13768_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13768(__this, method) (( Object_t * (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13768_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13769_gshared (Collection_1_t2614 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13769(__this, ___value, method) (( int32_t (*) (Collection_1_t2614 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13769_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13770_gshared (Collection_1_t2614 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13770(__this, ___value, method) (( bool (*) (Collection_1_t2614 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13770_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13771_gshared (Collection_1_t2614 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13771(__this, ___value, method) (( int32_t (*) (Collection_1_t2614 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13771_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13772_gshared (Collection_1_t2614 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13772(__this, ___index, ___value, method) (( void (*) (Collection_1_t2614 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13772_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13773_gshared (Collection_1_t2614 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13773(__this, ___value, method) (( void (*) (Collection_1_t2614 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13773_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13774_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13774(__this, method) (( bool (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13774_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13775_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13775(__this, method) (( Object_t * (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13775_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13776_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13776(__this, method) (( bool (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13776_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13777_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13777(__this, method) (( bool (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13777_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13778_gshared (Collection_1_t2614 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13778(__this, ___index, method) (( Object_t * (*) (Collection_1_t2614 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13778_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13779_gshared (Collection_1_t2614 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13779(__this, ___index, ___value, method) (( void (*) (Collection_1_t2614 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13779_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m13780_gshared (Collection_1_t2614 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_Add_m13780(__this, ___item, method) (( void (*) (Collection_1_t2614 *, UIVertex_t225 , const MethodInfo*))Collection_1_Add_m13780_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m13781_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13781(__this, method) (( void (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_Clear_m13781_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m13782_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13782(__this, method) (( void (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_ClearItems_m13782_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m13783_gshared (Collection_1_t2614 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13783(__this, ___item, method) (( bool (*) (Collection_1_t2614 *, UIVertex_t225 , const MethodInfo*))Collection_1_Contains_m13783_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13784_gshared (Collection_1_t2614 * __this, UIVertexU5BU5D_t224* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13784(__this, ___array, ___index, method) (( void (*) (Collection_1_t2614 *, UIVertexU5BU5D_t224*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13784_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13785_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13785(__this, method) (( Object_t* (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_GetEnumerator_m13785_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13786_gshared (Collection_1_t2614 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13786(__this, ___item, method) (( int32_t (*) (Collection_1_t2614 *, UIVertex_t225 , const MethodInfo*))Collection_1_IndexOf_m13786_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13787_gshared (Collection_1_t2614 * __this, int32_t ___index, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13787(__this, ___index, ___item, method) (( void (*) (Collection_1_t2614 *, int32_t, UIVertex_t225 , const MethodInfo*))Collection_1_Insert_m13787_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13788_gshared (Collection_1_t2614 * __this, int32_t ___index, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13788(__this, ___index, ___item, method) (( void (*) (Collection_1_t2614 *, int32_t, UIVertex_t225 , const MethodInfo*))Collection_1_InsertItem_m13788_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m13789_gshared (Collection_1_t2614 * __this, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13789(__this, ___item, method) (( bool (*) (Collection_1_t2614 *, UIVertex_t225 , const MethodInfo*))Collection_1_Remove_m13789_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13790_gshared (Collection_1_t2614 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13790(__this, ___index, method) (( void (*) (Collection_1_t2614 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13790_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13791_gshared (Collection_1_t2614 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13791(__this, ___index, method) (( void (*) (Collection_1_t2614 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13791_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13792_gshared (Collection_1_t2614 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13792(__this, method) (( int32_t (*) (Collection_1_t2614 *, const MethodInfo*))Collection_1_get_Count_m13792_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t225  Collection_1_get_Item_m13793_gshared (Collection_1_t2614 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13793(__this, ___index, method) (( UIVertex_t225  (*) (Collection_1_t2614 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13793_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13794_gshared (Collection_1_t2614 * __this, int32_t ___index, UIVertex_t225  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13794(__this, ___index, ___value, method) (( void (*) (Collection_1_t2614 *, int32_t, UIVertex_t225 , const MethodInfo*))Collection_1_set_Item_m13794_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13795_gshared (Collection_1_t2614 * __this, int32_t ___index, UIVertex_t225  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13795(__this, ___index, ___item, method) (( void (*) (Collection_1_t2614 *, int32_t, UIVertex_t225 , const MethodInfo*))Collection_1_SetItem_m13795_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13796_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13796(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13796_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t225  Collection_1_ConvertItem_m13797_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13797(__this /* static, unused */, ___item, method) (( UIVertex_t225  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13797_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13798_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13798(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13798_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13799_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13799(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13799_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13800_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13800(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13800_gshared)(__this /* static, unused */, ___list, method)
