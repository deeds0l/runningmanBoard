﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t391;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.RectTransform
struct  RectTransform_t183  : public Transform_t30
{
};
struct RectTransform_t183_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t391 * ___reapplyDrivenProperties_2;
};
