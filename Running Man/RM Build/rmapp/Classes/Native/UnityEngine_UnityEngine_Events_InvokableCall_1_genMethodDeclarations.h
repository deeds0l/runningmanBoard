﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t2517;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2484;
// System.Object[]
struct ObjectU5BU5D_t29;

// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m12464_gshared (InvokableCall_1_t2517 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m12464(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2517 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m12464_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m12465_gshared (InvokableCall_1_t2517 * __this, UnityAction_1_t2484 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m12465(__this, ___callback, method) (( void (*) (InvokableCall_1_t2517 *, UnityAction_1_t2484 *, const MethodInfo*))InvokableCall_1__ctor_m12465_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m12466_gshared (InvokableCall_1_t2517 * __this, ObjectU5BU5D_t29* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m12466(__this, ___args, method) (( void (*) (InvokableCall_1_t2517 *, ObjectU5BU5D_t29*, const MethodInfo*))InvokableCall_1_Invoke_m12466_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m12467_gshared (InvokableCall_1_t2517 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m12467(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2517 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m12467_gshared)(__this, ___targetObj, ___method, method)
