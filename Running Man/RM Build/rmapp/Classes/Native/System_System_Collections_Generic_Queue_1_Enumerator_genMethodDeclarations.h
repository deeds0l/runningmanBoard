﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1/Enumerator<System.Object>
struct Enumerator_t2462;
// System.Object
struct Object_t;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2461;

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m11727_gshared (Enumerator_t2462 * __this, Queue_1_t2461 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m11727(__this, ___q, method) (( void (*) (Enumerator_t2462 *, Queue_1_t2461 *, const MethodInfo*))Enumerator__ctor_m11727_gshared)(__this, ___q, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11728_gshared (Enumerator_t2462 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11728(__this, method) (( Object_t * (*) (Enumerator_t2462 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11728_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m11729_gshared (Enumerator_t2462 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11729(__this, method) (( void (*) (Enumerator_t2462 *, const MethodInfo*))Enumerator_Dispose_m11729_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11730_gshared (Enumerator_t2462 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11730(__this, method) (( bool (*) (Enumerator_t2462 *, const MethodInfo*))Enumerator_MoveNext_m11730_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m11731_gshared (Enumerator_t2462 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11731(__this, method) (( Object_t * (*) (Enumerator_t2462 *, const MethodInfo*))Enumerator_get_Current_m11731_gshared)(__this, method)
