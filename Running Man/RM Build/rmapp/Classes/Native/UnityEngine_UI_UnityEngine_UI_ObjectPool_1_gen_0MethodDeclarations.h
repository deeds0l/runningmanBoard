﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t182;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t187;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t227;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m1618(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t182 *, UnityAction_1_t187 *, UnityAction_1_t187 *, const MethodInfo*))ObjectPool_1__ctor_m11987_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m13659(__this, method) (( int32_t (*) (ObjectPool_1_t182 *, const MethodInfo*))ObjectPool_1_get_countAll_m11989_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m13660(__this, ___value, method) (( void (*) (ObjectPool_1_t182 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11991_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m13661(__this, method) (( int32_t (*) (ObjectPool_1_t182 *, const MethodInfo*))ObjectPool_1_get_countActive_m11993_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m13662(__this, method) (( int32_t (*) (ObjectPool_1_t182 *, const MethodInfo*))ObjectPool_1_get_countInactive_m11995_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m1625(__this, method) (( List_1_t227 * (*) (ObjectPool_1_t182 *, const MethodInfo*))ObjectPool_1_Get_m11997_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m1630(__this, ___element, method) (( void (*) (ObjectPool_1_t182 *, List_1_t227 *, const MethodInfo*))ObjectPool_1_Release_m11999_gshared)(__this, ___element, method)
