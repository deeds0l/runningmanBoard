﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t546;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.IO.FileStream/WriteDelegate
struct  WriteDelegate_t1518  : public MulticastDelegate_t216
{
};
