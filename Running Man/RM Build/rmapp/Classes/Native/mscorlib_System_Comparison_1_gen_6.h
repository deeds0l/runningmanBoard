﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.AudioSource>
struct  Comparison_1_t2460  : public MulticastDelegate_t216
{
};
