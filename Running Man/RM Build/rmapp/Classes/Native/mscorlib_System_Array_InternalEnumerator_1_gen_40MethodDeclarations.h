﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>
struct InternalEnumerator_1_t2819;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16641_gshared (InternalEnumerator_1_t2819 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16641(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2819 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16641_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16642_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16642(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16642_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16643_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16643(__this, method) (( void (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16643_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16644_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16644(__this, method) (( bool (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16644_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2818  InternalEnumerator_1_get_Current_m16645_gshared (InternalEnumerator_1_t2819 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16645(__this, method) (( KeyValuePair_2_t2818  (*) (InternalEnumerator_1_t2819 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16645_gshared)(__this, method)
