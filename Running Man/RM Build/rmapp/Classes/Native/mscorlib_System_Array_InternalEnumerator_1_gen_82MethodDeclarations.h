﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>
struct InternalEnumerator_1_t3000;
// System.Object
struct Object_t;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t1551;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18540(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3000 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18541(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3000 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m18542(__this, method) (( void (*) (InternalEnumerator_1_t3000 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18543(__this, method) (( bool (*) (InternalEnumerator_1_t3000 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.PropertyBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m18544(__this, method) (( PropertyBuilder_t1551 * (*) (InternalEnumerator_1_t3000 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
