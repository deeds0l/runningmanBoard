﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>
struct Enumerator_t2733;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t528;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m15412(__this, ___dictionary, method) (( void (*) (Enumerator_t2733 *, Dictionary_2_t528 *, const MethodInfo*))Enumerator__ctor_m13174_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15413(__this, method) (( Object_t * (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13175_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15414(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13176_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15415(__this, method) (( Object_t * (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13177_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15416(__this, method) (( Object_t * (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m15417(__this, method) (( bool (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_MoveNext_m13179_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m15418(__this, method) (( KeyValuePair_2_t2730  (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_get_Current_m13180_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15419(__this, method) (( String_t* (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_get_CurrentKey_m13181_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15420(__this, method) (( int32_t (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_get_CurrentValue_m13182_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m15421(__this, method) (( void (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_VerifyState_m13183_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15422(__this, method) (( void (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_VerifyCurrent_m13184_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m15423(__this, method) (( void (*) (Enumerator_t2733 *, const MethodInfo*))Enumerator_Dispose_m13185_gshared)(__this, method)
