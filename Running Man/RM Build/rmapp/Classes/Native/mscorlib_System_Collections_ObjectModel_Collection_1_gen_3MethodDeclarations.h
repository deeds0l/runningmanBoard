﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2770;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t709;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3184;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t376;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m16049_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16049(__this, method) (( void (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1__ctor_m16049_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16050_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16050(__this, method) (( bool (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16050_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16051_gshared (Collection_1_t2770 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16051(__this, ___array, ___index, method) (( void (*) (Collection_1_t2770 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16051_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16052_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16052(__this, method) (( Object_t * (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16052_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16053_gshared (Collection_1_t2770 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16053(__this, ___value, method) (( int32_t (*) (Collection_1_t2770 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16053_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16054_gshared (Collection_1_t2770 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16054(__this, ___value, method) (( bool (*) (Collection_1_t2770 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16054_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16055_gshared (Collection_1_t2770 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16055(__this, ___value, method) (( int32_t (*) (Collection_1_t2770 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16055_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16056_gshared (Collection_1_t2770 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16056(__this, ___index, ___value, method) (( void (*) (Collection_1_t2770 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16056_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16057_gshared (Collection_1_t2770 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16057(__this, ___value, method) (( void (*) (Collection_1_t2770 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16057_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16058_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16058(__this, method) (( bool (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16058_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16059_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16059(__this, method) (( Object_t * (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16059_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16060_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16060(__this, method) (( bool (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16060_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16061_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16061(__this, method) (( bool (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16061_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16062_gshared (Collection_1_t2770 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16062(__this, ___index, method) (( Object_t * (*) (Collection_1_t2770 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16062_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16063_gshared (Collection_1_t2770 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16063(__this, ___index, ___value, method) (( void (*) (Collection_1_t2770 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16063_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m16064_gshared (Collection_1_t2770 * __this, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_Add_m16064(__this, ___item, method) (( void (*) (Collection_1_t2770 *, UILineInfo_t375 , const MethodInfo*))Collection_1_Add_m16064_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m16065_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16065(__this, method) (( void (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_Clear_m16065_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m16066_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16066(__this, method) (( void (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_ClearItems_m16066_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m16067_gshared (Collection_1_t2770 * __this, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_Contains_m16067(__this, ___item, method) (( bool (*) (Collection_1_t2770 *, UILineInfo_t375 , const MethodInfo*))Collection_1_Contains_m16067_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16068_gshared (Collection_1_t2770 * __this, UILineInfoU5BU5D_t709* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16068(__this, ___array, ___index, method) (( void (*) (Collection_1_t2770 *, UILineInfoU5BU5D_t709*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16068_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16069_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16069(__this, method) (( Object_t* (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_GetEnumerator_m16069_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16070_gshared (Collection_1_t2770 * __this, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16070(__this, ___item, method) (( int32_t (*) (Collection_1_t2770 *, UILineInfo_t375 , const MethodInfo*))Collection_1_IndexOf_m16070_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16071_gshared (Collection_1_t2770 * __this, int32_t ___index, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_Insert_m16071(__this, ___index, ___item, method) (( void (*) (Collection_1_t2770 *, int32_t, UILineInfo_t375 , const MethodInfo*))Collection_1_Insert_m16071_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16072_gshared (Collection_1_t2770 * __this, int32_t ___index, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16072(__this, ___index, ___item, method) (( void (*) (Collection_1_t2770 *, int32_t, UILineInfo_t375 , const MethodInfo*))Collection_1_InsertItem_m16072_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m16073_gshared (Collection_1_t2770 * __this, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_Remove_m16073(__this, ___item, method) (( bool (*) (Collection_1_t2770 *, UILineInfo_t375 , const MethodInfo*))Collection_1_Remove_m16073_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16074_gshared (Collection_1_t2770 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16074(__this, ___index, method) (( void (*) (Collection_1_t2770 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16074_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16075_gshared (Collection_1_t2770 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16075(__this, ___index, method) (( void (*) (Collection_1_t2770 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16075_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16076_gshared (Collection_1_t2770 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16076(__this, method) (( int32_t (*) (Collection_1_t2770 *, const MethodInfo*))Collection_1_get_Count_m16076_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t375  Collection_1_get_Item_m16077_gshared (Collection_1_t2770 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16077(__this, ___index, method) (( UILineInfo_t375  (*) (Collection_1_t2770 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16077_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16078_gshared (Collection_1_t2770 * __this, int32_t ___index, UILineInfo_t375  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16078(__this, ___index, ___value, method) (( void (*) (Collection_1_t2770 *, int32_t, UILineInfo_t375 , const MethodInfo*))Collection_1_set_Item_m16078_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16079_gshared (Collection_1_t2770 * __this, int32_t ___index, UILineInfo_t375  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16079(__this, ___index, ___item, method) (( void (*) (Collection_1_t2770 *, int32_t, UILineInfo_t375 , const MethodInfo*))Collection_1_SetItem_m16079_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16080_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16080(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16080_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t375  Collection_1_ConvertItem_m16081_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16081(__this /* static, unused */, ___item, method) (( UILineInfo_t375  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16081_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16082_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16082(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16082_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16083_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16083(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16083_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16084_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16084(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16084_gshared)(__this /* static, unused */, ___list, method)
