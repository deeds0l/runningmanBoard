﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition System.Runtime.InteropServices._PropertyBuilder
extern TypeInfo _PropertyBuilder_t1995_il2cpp_TypeInfo;
static const MethodInfo* _PropertyBuilder_t1995_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyBuilder_t1995_0_0_0;
extern const Il2CppType _PropertyBuilder_t1995_1_0_0;
struct _PropertyBuilder_t1995;
const Il2CppTypeDefinitionMetadata _PropertyBuilder_t1995_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyBuilder_t1995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyBuilder_t1995_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyBuilder_t1995_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 495/* custom_attributes_cache */
	, &_PropertyBuilder_t1995_0_0_0/* byval_arg */
	, &_PropertyBuilder_t1995_1_0_0/* this_arg */
	, &_PropertyBuilder_t1995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyInfo
extern TypeInfo _PropertyInfo_t2006_il2cpp_TypeInfo;
static const MethodInfo* _PropertyInfo_t2006_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyInfo_t2006_0_0_0;
extern const Il2CppType _PropertyInfo_t2006_1_0_0;
struct _PropertyInfo_t2006;
const Il2CppTypeDefinitionMetadata _PropertyInfo_t2006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _PropertyInfo_t2006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _PropertyInfo_t2006_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_PropertyInfo_t2006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 496/* custom_attributes_cache */
	, &_PropertyInfo_t2006_0_0_0/* byval_arg */
	, &_PropertyInfo_t2006_1_0_0/* this_arg */
	, &_PropertyInfo_t2006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t2008_il2cpp_TypeInfo;
static const MethodInfo* _Thread_t2008_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Thread_t2008_0_0_0;
extern const Il2CppType _Thread_t2008_1_0_0;
struct _Thread_t2008;
const Il2CppTypeDefinitionMetadata _Thread_t2008_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _Thread_t2008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _Thread_t2008_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_Thread_t2008_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 497/* custom_attributes_cache */
	, &_Thread_t2008_0_0_0/* byval_arg */
	, &_Thread_t2008_1_0_0/* this_arg */
	, &_Thread_t2008_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t1996_il2cpp_TypeInfo;
static const MethodInfo* _TypeBuilder_t1996_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _TypeBuilder_t1996_0_0_0;
extern const Il2CppType _TypeBuilder_t1996_1_0_0;
struct _TypeBuilder_t1996;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t1996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo _TypeBuilder_t1996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, _TypeBuilder_t1996_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &_TypeBuilder_t1996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 498/* custom_attributes_cache */
	, &_TypeBuilder_t1996_0_0_0/* byval_arg */
	, &_TypeBuilder_t1996_1_0_0/* this_arg */
	, &_TypeBuilder_t1996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t1619_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
extern const Il2CppType IActivator_t1618_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::get_ConstructionActivator()
extern const MethodInfo ActivationServices_get_ConstructionActivator_m8498_MethodInfo = 
{
	"get_ConstructionActivator"/* name */
	, (methodPointerType)&ActivationServices_get_ConstructionActivator_m8498/* method */
	, &ActivationServices_t1619_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1618_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2193/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo ActivationServices_t1619_ActivationServices_CreateProxyFromAttributes_m8499_ParameterInfos[] = 
{
	{"type", 0, 134221896, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134221897, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern const Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyFromAttributes(System.Type,System.Object[])
extern const MethodInfo ActivationServices_CreateProxyFromAttributes_m8499_MethodInfo = 
{
	"CreateProxyFromAttributes"/* name */
	, (methodPointerType)&ActivationServices_CreateProxyFromAttributes_m8499/* method */
	, &ActivationServices_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1619_ActivationServices_CreateProxyFromAttributes_m8499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo ActivationServices_t1619_ActivationServices_CreateConstructionCall_m8500_ParameterInfos[] = 
{
	{"type", 0, 134221898, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134221899, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134221900, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern const Il2CppType ConstructionCall_t1643_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.ConstructionCall System.Runtime.Remoting.Activation.ActivationServices::CreateConstructionCall(System.Type,System.String,System.Object[])
extern const MethodInfo ActivationServices_CreateConstructionCall_m8500_MethodInfo = 
{
	"CreateConstructionCall"/* name */
	, (methodPointerType)&ActivationServices_CreateConstructionCall_m8500/* method */
	, &ActivationServices_t1619_il2cpp_TypeInfo/* declaring_type */
	, &ConstructionCall_t1643_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1619_ActivationServices_CreateConstructionCall_m8500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ActivationServices_t1619_ActivationServices_AllocateUninitializedClassInstance_m8501_ParameterInfos[] = 
{
	{"type", 0, 134221901, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)
extern const MethodInfo ActivationServices_AllocateUninitializedClassInstance_m8501_MethodInfo = 
{
	"AllocateUninitializedClassInstance"/* name */
	, (methodPointerType)&ActivationServices_AllocateUninitializedClassInstance_m8501/* method */
	, &ActivationServices_t1619_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ActivationServices_t1619_ActivationServices_AllocateUninitializedClassInstance_m8501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivationServices_t1619_MethodInfos[] =
{
	&ActivationServices_get_ConstructionActivator_m8498_MethodInfo,
	&ActivationServices_CreateProxyFromAttributes_m8499_MethodInfo,
	&ActivationServices_CreateConstructionCall_m8500_MethodInfo,
	&ActivationServices_AllocateUninitializedClassInstance_m8501_MethodInfo,
	NULL
};
extern const MethodInfo ActivationServices_get_ConstructionActivator_m8498_MethodInfo;
static const PropertyInfo ActivationServices_t1619____ConstructionActivator_PropertyInfo = 
{
	&ActivationServices_t1619_il2cpp_TypeInfo/* parent */
	, "ConstructionActivator"/* name */
	, &ActivationServices_get_ConstructionActivator_m8498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivationServices_t1619_PropertyInfos[] =
{
	&ActivationServices_t1619____ConstructionActivator_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference ActivationServices_t1619_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ActivationServices_t1619_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationServices_t1619_0_0_0;
extern const Il2CppType ActivationServices_t1619_1_0_0;
struct ActivationServices_t1619;
const Il2CppTypeDefinitionMetadata ActivationServices_t1619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t1619_VTable/* vtableMethods */
	, ActivationServices_t1619_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1487/* fieldStart */

};
TypeInfo ActivationServices_t1619_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ActivationServices_t1619_MethodInfos/* methods */
	, ActivationServices_t1619_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivationServices_t1619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t1619_0_0_0/* byval_arg */
	, &ActivationServices_t1619_1_0_0/* this_arg */
	, &ActivationServices_t1619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t1619)/* instance_size */
	, sizeof (ActivationServices_t1619)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t1619_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t1620_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IActivator_t1618_0_0_0;
static const ParameterInfo AppDomainLevelActivator_t1620_AppDomainLevelActivator__ctor_m8502_ParameterInfos[] = 
{
	{"activationUrl", 0, 134221902, 0, &String_t_0_0_0},
	{"next", 1, 134221903, 0, &IActivator_t1618_0_0_0},
};
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo AppDomainLevelActivator__ctor_m8502_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainLevelActivator__ctor_m8502/* method */
	, &AppDomainLevelActivator_t1620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, AppDomainLevelActivator_t1620_AppDomainLevelActivator__ctor_m8502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainLevelActivator_t1620_MethodInfos[] =
{
	&AppDomainLevelActivator__ctor_m8502_MethodInfo,
	NULL
};
static const Il2CppMethodReference AppDomainLevelActivator_t1620_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AppDomainLevelActivator_t1620_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* AppDomainLevelActivator_t1620_InterfacesTypeInfos[] = 
{
	&IActivator_t1618_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t1620_InterfacesOffsets[] = 
{
	{ &IActivator_t1618_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainLevelActivator_t1620_0_0_0;
extern const Il2CppType AppDomainLevelActivator_t1620_1_0_0;
struct AppDomainLevelActivator_t1620;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t1620_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t1620_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t1620_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t1620_VTable/* vtableMethods */
	, AppDomainLevelActivator_t1620_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1488/* fieldStart */

};
TypeInfo AppDomainLevelActivator_t1620_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, AppDomainLevelActivator_t1620_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainLevelActivator_t1620_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t1620_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t1620_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t1620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t1620)/* instance_size */
	, sizeof (AppDomainLevelActivator_t1620)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t1621_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern const MethodInfo ConstructionLevelActivator__ctor_m8503_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionLevelActivator__ctor_m8503/* method */
	, &ConstructionLevelActivator_t1621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionLevelActivator_t1621_MethodInfos[] =
{
	&ConstructionLevelActivator__ctor_m8503_MethodInfo,
	NULL
};
static const Il2CppMethodReference ConstructionLevelActivator_t1621_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ConstructionLevelActivator_t1621_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionLevelActivator_t1621_InterfacesTypeInfos[] = 
{
	&IActivator_t1618_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t1621_InterfacesOffsets[] = 
{
	{ &IActivator_t1618_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionLevelActivator_t1621_0_0_0;
extern const Il2CppType ConstructionLevelActivator_t1621_1_0_0;
struct ConstructionLevelActivator_t1621;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t1621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t1621_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t1621_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t1621_VTable/* vtableMethods */
	, ConstructionLevelActivator_t1621_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructionLevelActivator_t1621_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ConstructionLevelActivator_t1621_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionLevelActivator_t1621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t1621_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t1621_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t1621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t1621)/* instance_size */
	, sizeof (ConstructionLevelActivator_t1621)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t1622_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
extern const Il2CppType IActivator_t1618_0_0_0;
static const ParameterInfo ContextLevelActivator_t1622_ContextLevelActivator__ctor_m8504_ParameterInfos[] = 
{
	{"next", 0, 134221904, 0, &IActivator_t1618_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ContextLevelActivator__ctor_m8504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextLevelActivator__ctor_m8504/* method */
	, &ContextLevelActivator_t1622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ContextLevelActivator_t1622_ContextLevelActivator__ctor_m8504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextLevelActivator_t1622_MethodInfos[] =
{
	&ContextLevelActivator__ctor_m8504_MethodInfo,
	NULL
};
static const Il2CppMethodReference ContextLevelActivator_t1622_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ContextLevelActivator_t1622_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextLevelActivator_t1622_InterfacesTypeInfos[] = 
{
	&IActivator_t1618_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t1622_InterfacesOffsets[] = 
{
	{ &IActivator_t1618_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextLevelActivator_t1622_0_0_0;
extern const Il2CppType ContextLevelActivator_t1622_1_0_0;
struct ContextLevelActivator_t1622;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t1622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t1622_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t1622_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t1622_VTable/* vtableMethods */
	, ContextLevelActivator_t1622_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1490/* fieldStart */

};
TypeInfo ContextLevelActivator_t1622_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, ContextLevelActivator_t1622_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ContextLevelActivator_t1622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t1622_0_0_0/* byval_arg */
	, &ContextLevelActivator_t1622_1_0_0/* this_arg */
	, &ContextLevelActivator_t1622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t1622)/* instance_size */
	, sizeof (ContextLevelActivator_t1622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t1618_il2cpp_TypeInfo;
static const MethodInfo* IActivator_t1618_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IActivator_t1618_1_0_0;
struct IActivator_t1618;
const Il2CppTypeDefinitionMetadata IActivator_t1618_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IActivator_t1618_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IActivator_t1618_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IActivator_t1618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 499/* custom_attributes_cache */
	, &IActivator_t1618_0_0_0/* byval_arg */
	, &IActivator_t1618_1_0_0/* this_arg */
	, &IActivator_t1618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t1918_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType()
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m10956_MethodInfo = 
{
	"get_ActivationType"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationTypeName()
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m10957_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator()
extern const MethodInfo IConstructionCallMessage_get_Activator_m10958_MethodInfo = 
{
	"get_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1618_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t1618_0_0_0;
static const ParameterInfo IConstructionCallMessage_t1918_IConstructionCallMessage_set_Activator_m10959_ParameterInfos[] = 
{
	{"value", 0, 134221905, 0, &IActivator_t1618_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.IConstructionCallMessage::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo IConstructionCallMessage_set_Activator_m10959_MethodInfo = 
{
	"set_Activator"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IConstructionCallMessage_t1918_IConstructionCallMessage_set_Activator_m10959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Activation.IConstructionCallMessage::get_CallSiteActivationAttributes()
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m10960_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IList_t861_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties()
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m10961_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, NULL/* method */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* declaring_type */
	, &IList_t861_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IConstructionCallMessage_t1918_MethodInfos[] =
{
	&IConstructionCallMessage_get_ActivationType_m10956_MethodInfo,
	&IConstructionCallMessage_get_ActivationTypeName_m10957_MethodInfo,
	&IConstructionCallMessage_get_Activator_m10958_MethodInfo,
	&IConstructionCallMessage_set_Activator_m10959_MethodInfo,
	&IConstructionCallMessage_get_CallSiteActivationAttributes_m10960_MethodInfo,
	&IConstructionCallMessage_get_ContextProperties_m10961_MethodInfo,
	NULL
};
extern const MethodInfo IConstructionCallMessage_get_ActivationType_m10956_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t1918____ActivationType_PropertyInfo = 
{
	&IConstructionCallMessage_t1918_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &IConstructionCallMessage_get_ActivationType_m10956_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ActivationTypeName_m10957_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t1918____ActivationTypeName_PropertyInfo = 
{
	&IConstructionCallMessage_t1918_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &IConstructionCallMessage_get_ActivationTypeName_m10957_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_Activator_m10958_MethodInfo;
extern const MethodInfo IConstructionCallMessage_set_Activator_m10959_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t1918____Activator_PropertyInfo = 
{
	&IConstructionCallMessage_t1918_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &IConstructionCallMessage_get_Activator_m10958_MethodInfo/* get */
	, &IConstructionCallMessage_set_Activator_m10959_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_CallSiteActivationAttributes_m10960_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t1918____CallSiteActivationAttributes_PropertyInfo = 
{
	&IConstructionCallMessage_t1918_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &IConstructionCallMessage_get_CallSiteActivationAttributes_m10960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IConstructionCallMessage_get_ContextProperties_m10961_MethodInfo;
static const PropertyInfo IConstructionCallMessage_t1918____ContextProperties_PropertyInfo = 
{
	&IConstructionCallMessage_t1918_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &IConstructionCallMessage_get_ContextProperties_m10961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IConstructionCallMessage_t1918_PropertyInfos[] =
{
	&IConstructionCallMessage_t1918____ActivationType_PropertyInfo,
	&IConstructionCallMessage_t1918____ActivationTypeName_PropertyInfo,
	&IConstructionCallMessage_t1918____Activator_PropertyInfo,
	&IConstructionCallMessage_t1918____CallSiteActivationAttributes_PropertyInfo,
	&IConstructionCallMessage_t1918____ContextProperties_PropertyInfo,
	NULL
};
extern const Il2CppType IMessage_t1641_0_0_0;
extern const Il2CppType IMethodCallMessage_t1922_0_0_0;
extern const Il2CppType IMethodMessage_t1653_0_0_0;
static const Il2CppType* IConstructionCallMessage_t1918_InterfacesTypeInfos[] = 
{
	&IMessage_t1641_0_0_0,
	&IMethodCallMessage_t1922_0_0_0,
	&IMethodMessage_t1653_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_1_0_0;
struct IConstructionCallMessage_t1918;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t1918_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t1918_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IConstructionCallMessage_t1918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, IConstructionCallMessage_t1918_MethodInfos/* methods */
	, IConstructionCallMessage_t1918_PropertyInfos/* properties */
	, NULL/* events */
	, &IConstructionCallMessage_t1918_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 500/* custom_attributes_cache */
	, &IConstructionCallMessage_t1918_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t1918_1_0_0/* this_arg */
	, &IConstructionCallMessage_t1918_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t1623_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
static const MethodInfo* RemoteActivator_t1623_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RemoteActivator_t1623_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RemoteActivator_t1623_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemoteActivator_t1623_InterfacesTypeInfos[] = 
{
	&IActivator_t1618_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t1623_InterfacesOffsets[] = 
{
	{ &IActivator_t1618_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemoteActivator_t1623_0_0_0;
extern const Il2CppType RemoteActivator_t1623_1_0_0;
extern const Il2CppType MarshalByRefObject_t1022_0_0_0;
struct RemoteActivator_t1623;
const Il2CppTypeDefinitionMetadata RemoteActivator_t1623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t1623_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t1623_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1022_0_0_0/* parent */
	, RemoteActivator_t1623_VTable/* vtableMethods */
	, RemoteActivator_t1623_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteActivator_t1623_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, RemoteActivator_t1623_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteActivator_t1623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t1623_0_0_0/* byval_arg */
	, &RemoteActivator_t1623_1_0_0/* this_arg */
	, &RemoteActivator_t1623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t1623)/* instance_size */
	, sizeof (RemoteActivator_t1623)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t1624_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Activation.UrlAttribute::get_UrlValue()
extern const MethodInfo UrlAttribute_get_UrlValue_m8505_MethodInfo = 
{
	"get_UrlValue"/* name */
	, (methodPointerType)&UrlAttribute_get_UrlValue_m8505/* method */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UrlAttribute_t1624_UrlAttribute_Equals_m8506_ParameterInfos[] = 
{
	{"o", 0, 134221906, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::Equals(System.Object)
extern const MethodInfo UrlAttribute_Equals_m8506_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&UrlAttribute_Equals_m8506/* method */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, UrlAttribute_t1624_UrlAttribute_Equals_m8506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Activation.UrlAttribute::GetHashCode()
extern const MethodInfo UrlAttribute_GetHashCode_m8507_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&UrlAttribute_GetHashCode_m8507/* method */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo UrlAttribute_t1624_UrlAttribute_GetPropertiesForNewContext_m8508_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221907, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m8508_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&UrlAttribute_GetPropertiesForNewContext_m8508/* method */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UrlAttribute_t1624_UrlAttribute_GetPropertiesForNewContext_m8508_ParameterInfos/* parameters */
	, 502/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo UrlAttribute_t1624_UrlAttribute_IsContextOK_m8509_ParameterInfos[] = 
{
	{"ctx", 0, 134221908, 0, &Context_t1632_0_0_0},
	{"msg", 1, 134221909, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo UrlAttribute_IsContextOK_m8509_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&UrlAttribute_IsContextOK_m8509/* method */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, UrlAttribute_t1624_UrlAttribute_IsContextOK_m8509_ParameterInfos/* parameters */
	, 503/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UrlAttribute_t1624_MethodInfos[] =
{
	&UrlAttribute_get_UrlValue_m8505_MethodInfo,
	&UrlAttribute_Equals_m8506_MethodInfo,
	&UrlAttribute_GetHashCode_m8507_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m8508_MethodInfo,
	&UrlAttribute_IsContextOK_m8509_MethodInfo,
	NULL
};
extern const MethodInfo UrlAttribute_get_UrlValue_m8505_MethodInfo;
static const PropertyInfo UrlAttribute_t1624____UrlValue_PropertyInfo = 
{
	&UrlAttribute_t1624_il2cpp_TypeInfo/* parent */
	, "UrlValue"/* name */
	, &UrlAttribute_get_UrlValue_m8505_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UrlAttribute_t1624_PropertyInfos[] =
{
	&UrlAttribute_t1624____UrlValue_PropertyInfo,
	NULL
};
extern const MethodInfo UrlAttribute_Equals_m8506_MethodInfo;
extern const MethodInfo UrlAttribute_GetHashCode_m8507_MethodInfo;
extern const MethodInfo UrlAttribute_GetPropertiesForNewContext_m8508_MethodInfo;
extern const MethodInfo UrlAttribute_IsContextOK_m8509_MethodInfo;
extern const MethodInfo ContextAttribute_get_Name_m8533_MethodInfo;
static const Il2CppMethodReference UrlAttribute_t1624_VTable[] =
{
	&UrlAttribute_Equals_m8506_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&UrlAttribute_GetHashCode_m8507_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m8508_MethodInfo,
	&UrlAttribute_IsContextOK_m8509_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&UrlAttribute_GetPropertiesForNewContext_m8508_MethodInfo,
	&UrlAttribute_IsContextOK_m8509_MethodInfo,
};
static bool UrlAttribute_t1624_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IContextAttribute_t1931_0_0_0;
extern const Il2CppType IContextProperty_t1920_0_0_0;
extern const Il2CppType _Attribute_t832_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t1624_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t1931_0_0_0, 4},
	{ &IContextProperty_t1920_0_0_0, 6},
	{ &_Attribute_t832_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UrlAttribute_t1624_0_0_0;
extern const Il2CppType UrlAttribute_t1624_1_0_0;
extern const Il2CppType ContextAttribute_t1625_0_0_0;
struct UrlAttribute_t1624;
const Il2CppTypeDefinitionMetadata UrlAttribute_t1624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t1624_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1625_0_0_0/* parent */
	, UrlAttribute_t1624_VTable/* vtableMethods */
	, UrlAttribute_t1624_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1491/* fieldStart */

};
TypeInfo UrlAttribute_t1624_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, UrlAttribute_t1624_MethodInfos/* methods */
	, UrlAttribute_t1624_PropertyInfos/* properties */
	, NULL/* events */
	, &UrlAttribute_t1624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 501/* custom_attributes_cache */
	, &UrlAttribute_t1624_0_0_0/* byval_arg */
	, &UrlAttribute_t1624_1_0_0/* this_arg */
	, &UrlAttribute_t1624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t1624)/* instance_size */
	, sizeof (UrlAttribute_t1624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t1626_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern const MethodInfo ChannelInfo__ctor_m8510_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ChannelInfo__ctor_m8510/* method */
	, &ChannelInfo_t1626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern const MethodInfo ChannelInfo_get_ChannelData_m8511_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&ChannelInfo_get_ChannelData_m8511/* method */
	, &ChannelInfo_t1626_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelInfo_t1626_MethodInfos[] =
{
	&ChannelInfo__ctor_m8510_MethodInfo,
	&ChannelInfo_get_ChannelData_m8511_MethodInfo,
	NULL
};
extern const MethodInfo ChannelInfo_get_ChannelData_m8511_MethodInfo;
static const PropertyInfo ChannelInfo_t1626____ChannelData_PropertyInfo = 
{
	&ChannelInfo_t1626_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &ChannelInfo_get_ChannelData_m8511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ChannelInfo_t1626_PropertyInfos[] =
{
	&ChannelInfo_t1626____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ChannelInfo_t1626_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ChannelInfo_get_ChannelData_m8511_MethodInfo,
};
static bool ChannelInfo_t1626_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelInfo_t1672_0_0_0;
static const Il2CppType* ChannelInfo_t1626_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t1672_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t1626_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t1672_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelInfo_t1626_0_0_0;
extern const Il2CppType ChannelInfo_t1626_1_0_0;
struct ChannelInfo_t1626;
const Il2CppTypeDefinitionMetadata ChannelInfo_t1626_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t1626_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t1626_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t1626_VTable/* vtableMethods */
	, ChannelInfo_t1626_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1492/* fieldStart */

};
TypeInfo ChannelInfo_t1626_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ChannelInfo_t1626_MethodInfos/* methods */
	, ChannelInfo_t1626_PropertyInfos/* properties */
	, NULL/* events */
	, &ChannelInfo_t1626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t1626_0_0_0/* byval_arg */
	, &ChannelInfo_t1626_1_0_0/* this_arg */
	, &ChannelInfo_t1626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t1626)/* instance_size */
	, sizeof (ChannelInfo_t1626)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t1628_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
extern const MethodInfo ChannelServices__cctor_m8512_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ChannelServices__cctor_m8512/* method */
	, &ChannelServices_t1628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t1919_0_0_0;
extern const Il2CppType IChannel_t1919_0_0_0;
static const ParameterInfo ChannelServices_t1628_ChannelServices_RegisterChannel_m8513_ParameterInfos[] = 
{
	{"chnl", 0, 134221910, 0, &IChannel_t1919_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern const MethodInfo ChannelServices_RegisterChannel_m8513_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m8513/* method */
	, &ChannelServices_t1628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ChannelServices_t1628_ChannelServices_RegisterChannel_m8513_ParameterInfos/* parameters */
	, 505/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IChannel_t1919_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ChannelServices_t1628_ChannelServices_RegisterChannel_m8514_ParameterInfos[] = 
{
	{"chnl", 0, 134221911, 0, &IChannel_t1919_0_0_0},
	{"ensureSecurity", 1, 134221912, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
extern const MethodInfo ChannelServices_RegisterChannel_m8514_MethodInfo = 
{
	"RegisterChannel"/* name */
	, (methodPointerType)&ChannelServices_RegisterChannel_m8514/* method */
	, &ChannelServices_t1628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, ChannelServices_t1628_ChannelServices_RegisterChannel_m8514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
extern const MethodInfo ChannelServices_GetCurrentChannelInfo_m8515_MethodInfo = 
{
	"GetCurrentChannelInfo"/* name */
	, (methodPointerType)&ChannelServices_GetCurrentChannelInfo_m8515/* method */
	, &ChannelServices_t1628_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ChannelServices_t1628_MethodInfos[] =
{
	&ChannelServices__cctor_m8512_MethodInfo,
	&ChannelServices_RegisterChannel_m8513_MethodInfo,
	&ChannelServices_RegisterChannel_m8514_MethodInfo,
	&ChannelServices_GetCurrentChannelInfo_m8515_MethodInfo,
	NULL
};
static const Il2CppMethodReference ChannelServices_t1628_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ChannelServices_t1628_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelServices_t1628_0_0_0;
extern const Il2CppType ChannelServices_t1628_1_0_0;
struct ChannelServices_t1628;
const Il2CppTypeDefinitionMetadata ChannelServices_t1628_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t1628_VTable/* vtableMethods */
	, ChannelServices_t1628_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1493/* fieldStart */

};
TypeInfo ChannelServices_t1628_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ChannelServices_t1628_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ChannelServices_t1628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 504/* custom_attributes_cache */
	, &ChannelServices_t1628_0_0_0/* byval_arg */
	, &ChannelServices_t1628_1_0_0/* this_arg */
	, &ChannelServices_t1628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t1628)/* instance_size */
	, sizeof (ChannelServices_t1628)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t1628_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t1629_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo CrossAppDomainData_t1629_CrossAppDomainData__ctor_m8516_ParameterInfos[] = 
{
	{"domainId", 0, 134221913, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
extern const MethodInfo CrossAppDomainData__ctor_m8516_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainData__ctor_m8516/* method */
	, &CrossAppDomainData_t1629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CrossAppDomainData_t1629_CrossAppDomainData__ctor_m8516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainData_t1629_MethodInfos[] =
{
	&CrossAppDomainData__ctor_m8516_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainData_t1629_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CrossAppDomainData_t1629_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainData_t1629_0_0_0;
extern const Il2CppType CrossAppDomainData_t1629_1_0_0;
struct CrossAppDomainData_t1629;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t1629_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t1629_VTable/* vtableMethods */
	, CrossAppDomainData_t1629_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1498/* fieldStart */

};
TypeInfo CrossAppDomainData_t1629_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainData_t1629_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossAppDomainData_t1629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t1629_0_0_0/* byval_arg */
	, &CrossAppDomainData_t1629_1_0_0/* this_arg */
	, &CrossAppDomainData_t1629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t1629)/* instance_size */
	, sizeof (CrossAppDomainData_t1629)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t1630_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
extern const MethodInfo CrossAppDomainChannel__ctor_m8517_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__ctor_m8517/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
extern const MethodInfo CrossAppDomainChannel__cctor_m8518_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainChannel__cctor_m8518/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
extern const MethodInfo CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8519_MethodInfo = 
{
	"RegisterCrossAppDomainChannel"/* name */
	, (methodPointerType)&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8519/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo = 
{
	"get_ChannelName"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelName_m8520/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelPriority_m8521/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo = 
{
	"get_ChannelData"/* name */
	, (methodPointerType)&CrossAppDomainChannel_get_ChannelData_m8522/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CrossAppDomainChannel_t1630_CrossAppDomainChannel_StartListening_m8523_ParameterInfos[] = 
{
	{"data", 0, 134221914, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
extern const MethodInfo CrossAppDomainChannel_StartListening_m8523_MethodInfo = 
{
	"StartListening"/* name */
	, (methodPointerType)&CrossAppDomainChannel_StartListening_m8523/* method */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CrossAppDomainChannel_t1630_CrossAppDomainChannel_StartListening_m8523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainChannel_t1630_MethodInfos[] =
{
	&CrossAppDomainChannel__ctor_m8517_MethodInfo,
	&CrossAppDomainChannel__cctor_m8518_MethodInfo,
	&CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8519_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo,
	&CrossAppDomainChannel_StartListening_m8523_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1630____ChannelName_PropertyInfo = 
{
	&CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1630____ChannelPriority_PropertyInfo = 
{
	&CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo;
static const PropertyInfo CrossAppDomainChannel_t1630____ChannelData_PropertyInfo = 
{
	&CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainChannel_t1630_PropertyInfos[] =
{
	&CrossAppDomainChannel_t1630____ChannelName_PropertyInfo,
	&CrossAppDomainChannel_t1630____ChannelPriority_PropertyInfo,
	&CrossAppDomainChannel_t1630____ChannelData_PropertyInfo,
	NULL
};
extern const MethodInfo CrossAppDomainChannel_StartListening_m8523_MethodInfo;
static const Il2CppMethodReference CrossAppDomainChannel_t1630_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo,
	&CrossAppDomainChannel_StartListening_m8523_MethodInfo,
	&CrossAppDomainChannel_get_ChannelName_m8520_MethodInfo,
	&CrossAppDomainChannel_get_ChannelPriority_m8521_MethodInfo,
	&CrossAppDomainChannel_get_ChannelData_m8522_MethodInfo,
	&CrossAppDomainChannel_StartListening_m8523_MethodInfo,
};
static bool CrossAppDomainChannel_t1630_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IChannelReceiver_t1933_0_0_0;
extern const Il2CppType IChannelSender_t2009_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t1630_InterfacesTypeInfos[] = 
{
	&IChannel_t1919_0_0_0,
	&IChannelReceiver_t1933_0_0_0,
	&IChannelSender_t2009_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t1630_InterfacesOffsets[] = 
{
	{ &IChannel_t1919_0_0_0, 4},
	{ &IChannelReceiver_t1933_0_0_0, 6},
	{ &IChannelSender_t2009_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainChannel_t1630_0_0_0;
extern const Il2CppType CrossAppDomainChannel_t1630_1_0_0;
struct CrossAppDomainChannel_t1630;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t1630_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t1630_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t1630_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t1630_VTable/* vtableMethods */
	, CrossAppDomainChannel_t1630_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1501/* fieldStart */

};
TypeInfo CrossAppDomainChannel_t1630_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainChannel_t1630_MethodInfos/* methods */
	, CrossAppDomainChannel_t1630_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainChannel_t1630_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t1630_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t1630_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t1630_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t1630)/* instance_size */
	, sizeof (CrossAppDomainChannel_t1630)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t1630_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t1631_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.cctor()
extern const MethodInfo CrossAppDomainSink__cctor_m8524_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CrossAppDomainSink__cctor_m8524/* method */
	, &CrossAppDomainSink_t1631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainSink::get_TargetDomainId()
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m8525_MethodInfo = 
{
	"get_TargetDomainId"/* name */
	, (methodPointerType)&CrossAppDomainSink_get_TargetDomainId_m8525/* method */
	, &CrossAppDomainSink_t1631_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossAppDomainSink_t1631_MethodInfos[] =
{
	&CrossAppDomainSink__cctor_m8524_MethodInfo,
	&CrossAppDomainSink_get_TargetDomainId_m8525_MethodInfo,
	NULL
};
extern const MethodInfo CrossAppDomainSink_get_TargetDomainId_m8525_MethodInfo;
static const PropertyInfo CrossAppDomainSink_t1631____TargetDomainId_PropertyInfo = 
{
	&CrossAppDomainSink_t1631_il2cpp_TypeInfo/* parent */
	, "TargetDomainId"/* name */
	, &CrossAppDomainSink_get_TargetDomainId_m8525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CrossAppDomainSink_t1631_PropertyInfos[] =
{
	&CrossAppDomainSink_t1631____TargetDomainId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference CrossAppDomainSink_t1631_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CrossAppDomainSink_t1631_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType IMessageSink_t1218_0_0_0;
static const Il2CppType* CrossAppDomainSink_t1631_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1218_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t1631_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1218_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainSink_t1631_0_0_0;
extern const Il2CppType CrossAppDomainSink_t1631_1_0_0;
struct CrossAppDomainSink_t1631;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t1631_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t1631_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t1631_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t1631_VTable/* vtableMethods */
	, CrossAppDomainSink_t1631_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1502/* fieldStart */

};
TypeInfo CrossAppDomainSink_t1631_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, CrossAppDomainSink_t1631_MethodInfos/* methods */
	, CrossAppDomainSink_t1631_PropertyInfos/* properties */
	, NULL/* events */
	, &CrossAppDomainSink_t1631_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 506/* custom_attributes_cache */
	, &CrossAppDomainSink_t1631_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t1631_1_0_0/* this_arg */
	, &CrossAppDomainSink_t1631_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t1631)/* instance_size */
	, sizeof (CrossAppDomainSink_t1631)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t1631_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t1919_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName()
extern const MethodInfo IChannel_get_ChannelName_m10962_MethodInfo = 
{
	"get_ChannelName"/* name */
	, NULL/* method */
	, &IChannel_t1919_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority()
extern const MethodInfo IChannel_get_ChannelPriority_m10963_MethodInfo = 
{
	"get_ChannelPriority"/* name */
	, NULL/* method */
	, &IChannel_t1919_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannel_t1919_MethodInfos[] =
{
	&IChannel_get_ChannelName_m10962_MethodInfo,
	&IChannel_get_ChannelPriority_m10963_MethodInfo,
	NULL
};
extern const MethodInfo IChannel_get_ChannelName_m10962_MethodInfo;
static const PropertyInfo IChannel_t1919____ChannelName_PropertyInfo = 
{
	&IChannel_t1919_il2cpp_TypeInfo/* parent */
	, "ChannelName"/* name */
	, &IChannel_get_ChannelName_m10962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IChannel_get_ChannelPriority_m10963_MethodInfo;
static const PropertyInfo IChannel_t1919____ChannelPriority_PropertyInfo = 
{
	&IChannel_t1919_il2cpp_TypeInfo/* parent */
	, "ChannelPriority"/* name */
	, &IChannel_get_ChannelPriority_m10963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannel_t1919_PropertyInfos[] =
{
	&IChannel_t1919____ChannelName_PropertyInfo,
	&IChannel_t1919____ChannelPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannel_t1919_1_0_0;
struct IChannel_t1919;
const Il2CppTypeDefinitionMetadata IChannel_t1919_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannel_t1919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannel_t1919_MethodInfos/* methods */
	, IChannel_t1919_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannel_t1919_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 507/* custom_attributes_cache */
	, &IChannel_t1919_0_0_0/* byval_arg */
	, &IChannel_t1919_1_0_0/* this_arg */
	, &IChannel_t1919_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t1933_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Channels.IChannelReceiver::get_ChannelData()
extern const MethodInfo IChannelReceiver_get_ChannelData_m10964_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelReceiver_t1933_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IChannelReceiver_t1933_IChannelReceiver_StartListening_m10965_ParameterInfos[] = 
{
	{"data", 0, 134221915, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StartListening(System.Object)
extern const MethodInfo IChannelReceiver_StartListening_m10965_MethodInfo = 
{
	"StartListening"/* name */
	, NULL/* method */
	, &IChannelReceiver_t1933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IChannelReceiver_t1933_IChannelReceiver_StartListening_m10965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelReceiver_t1933_MethodInfos[] =
{
	&IChannelReceiver_get_ChannelData_m10964_MethodInfo,
	&IChannelReceiver_StartListening_m10965_MethodInfo,
	NULL
};
extern const MethodInfo IChannelReceiver_get_ChannelData_m10964_MethodInfo;
static const PropertyInfo IChannelReceiver_t1933____ChannelData_PropertyInfo = 
{
	&IChannelReceiver_t1933_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelReceiver_get_ChannelData_m10964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelReceiver_t1933_PropertyInfos[] =
{
	&IChannelReceiver_t1933____ChannelData_PropertyInfo,
	NULL
};
static const Il2CppType* IChannelReceiver_t1933_InterfacesTypeInfos[] = 
{
	&IChannel_t1919_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelReceiver_t1933_1_0_0;
struct IChannelReceiver_t1933;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t1933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t1933_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelReceiver_t1933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelReceiver_t1933_MethodInfos/* methods */
	, IChannelReceiver_t1933_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelReceiver_t1933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 508/* custom_attributes_cache */
	, &IChannelReceiver_t1933_0_0_0/* byval_arg */
	, &IChannelReceiver_t1933_1_0_0/* this_arg */
	, &IChannelReceiver_t1933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t2009_il2cpp_TypeInfo;
static const MethodInfo* IChannelSender_t2009_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IChannelSender_t2009_InterfacesTypeInfos[] = 
{
	&IChannel_t1919_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelSender_t2009_1_0_0;
struct IChannelSender_t2009;
const Il2CppTypeDefinitionMetadata IChannelSender_t2009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t2009_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelSender_t2009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, IChannelSender_t2009_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IChannelSender_t2009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 509/* custom_attributes_cache */
	, &IChannelSender_t2009_0_0_0/* byval_arg */
	, &IChannelSender_t2009_1_0_0/* this_arg */
	, &IChannelSender_t2009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t1932_il2cpp_TypeInfo;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ISecurableChannel_t1932_ISecurableChannel_set_IsSecured_m10966_ParameterInfos[] = 
{
	{"value", 0, 134221916, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Channels.ISecurableChannel::set_IsSecured(System.Boolean)
extern const MethodInfo ISecurableChannel_set_IsSecured_m10966_MethodInfo = 
{
	"set_IsSecured"/* name */
	, NULL/* method */
	, &ISecurableChannel_t1932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ISecurableChannel_t1932_ISecurableChannel_set_IsSecured_m10966_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISecurableChannel_t1932_MethodInfos[] =
{
	&ISecurableChannel_set_IsSecured_m10966_MethodInfo,
	NULL
};
extern const MethodInfo ISecurableChannel_set_IsSecured_m10966_MethodInfo;
static const PropertyInfo ISecurableChannel_t1932____IsSecured_PropertyInfo = 
{
	&ISecurableChannel_t1932_il2cpp_TypeInfo/* parent */
	, "IsSecured"/* name */
	, NULL/* get */
	, &ISecurableChannel_set_IsSecured_m10966_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ISecurableChannel_t1932_PropertyInfos[] =
{
	&ISecurableChannel_t1932____IsSecured_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurableChannel_t1932_0_0_0;
extern const Il2CppType ISecurableChannel_t1932_1_0_0;
struct ISecurableChannel_t1932;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t1932_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISecurableChannel_t1932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, ISecurableChannel_t1932_MethodInfos/* methods */
	, ISecurableChannel_t1932_PropertyInfos/* properties */
	, NULL/* events */
	, &ISecurableChannel_t1932_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t1932_0_0_0/* byval_arg */
	, &ISecurableChannel_t1932_1_0_0/* this_arg */
	, &ISecurableChannel_t1932_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t1632_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::.cctor()
extern const MethodInfo Context__cctor_m8526_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Context__cctor_m8526/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.Context::Finalize()
extern const MethodInfo Context_Finalize_m8527_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&Context_Finalize_m8527/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::get_DefaultContext()
extern const MethodInfo Context_get_DefaultContext_m8528_MethodInfo = 
{
	"get_DefaultContext"/* name */
	, (methodPointerType)&Context_get_DefaultContext_m8528/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Context_t1632_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_IsDefaultContext()
extern const MethodInfo Context_get_IsDefaultContext_m8529_MethodInfo = 
{
	"get_IsDefaultContext"/* name */
	, (methodPointerType)&Context_get_IsDefaultContext_m8529/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Context_t1632_Context_GetProperty_m8530_ParameterInfos[] = 
{
	{"name", 0, 134221917, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String)
extern const MethodInfo Context_GetProperty_m8530_MethodInfo = 
{
	"GetProperty"/* name */
	, (methodPointerType)&Context_GetProperty_m8530/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &IContextProperty_t1920_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Context_t1632_Context_GetProperty_m8530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.Context::ToString()
extern const MethodInfo Context_ToString_m8531_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Context_ToString_m8531/* method */
	, &Context_t1632_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Context_t1632_MethodInfos[] =
{
	&Context__cctor_m8526_MethodInfo,
	&Context_Finalize_m8527_MethodInfo,
	&Context_get_DefaultContext_m8528_MethodInfo,
	&Context_get_IsDefaultContext_m8529_MethodInfo,
	&Context_GetProperty_m8530_MethodInfo,
	&Context_ToString_m8531_MethodInfo,
	NULL
};
extern const MethodInfo Context_get_DefaultContext_m8528_MethodInfo;
static const PropertyInfo Context_t1632____DefaultContext_PropertyInfo = 
{
	&Context_t1632_il2cpp_TypeInfo/* parent */
	, "DefaultContext"/* name */
	, &Context_get_DefaultContext_m8528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Context_get_IsDefaultContext_m8529_MethodInfo;
static const PropertyInfo Context_t1632____IsDefaultContext_PropertyInfo = 
{
	&Context_t1632_il2cpp_TypeInfo/* parent */
	, "IsDefaultContext"/* name */
	, &Context_get_IsDefaultContext_m8529_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Context_t1632_PropertyInfos[] =
{
	&Context_t1632____DefaultContext_PropertyInfo,
	&Context_t1632____IsDefaultContext_PropertyInfo,
	NULL
};
extern const MethodInfo Context_Finalize_m8527_MethodInfo;
extern const MethodInfo Context_ToString_m8531_MethodInfo;
extern const MethodInfo Context_GetProperty_m8530_MethodInfo;
static const Il2CppMethodReference Context_t1632_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Context_Finalize_m8527_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Context_ToString_m8531_MethodInfo,
	&Context_GetProperty_m8530_MethodInfo,
};
static bool Context_t1632_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t1632_1_0_0;
struct Context_t1632;
const Il2CppTypeDefinitionMetadata Context_t1632_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t1632_VTable/* vtableMethods */
	, Context_t1632_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1505/* fieldStart */

};
TypeInfo Context_t1632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, Context_t1632_MethodInfos/* methods */
	, Context_t1632_PropertyInfos/* properties */
	, NULL/* events */
	, &Context_t1632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 510/* custom_attributes_cache */
	, &Context_t1632_0_0_0/* byval_arg */
	, &Context_t1632_1_0_0/* this_arg */
	, &Context_t1632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t1632)/* instance_size */
	, sizeof (Context_t1632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t1632_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t1625_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ContextAttribute_t1625_ContextAttribute__ctor_m8532_ParameterInfos[] = 
{
	{"name", 0, 134221918, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::.ctor(System.String)
extern const MethodInfo ContextAttribute__ctor_m8532_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContextAttribute__ctor_m8532/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ContextAttribute_t1625_ContextAttribute__ctor_m8532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.ContextAttribute::get_Name()
extern const MethodInfo ContextAttribute_get_Name_m8533_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&ContextAttribute_get_Name_m8533/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ContextAttribute_t1625_ContextAttribute_Equals_m8534_ParameterInfos[] = 
{
	{"o", 0, 134221919, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::Equals(System.Object)
extern const MethodInfo ContextAttribute_Equals_m8534_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&ContextAttribute_Equals_m8534/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, ContextAttribute_t1625_ContextAttribute_Equals_m8534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Contexts.ContextAttribute::GetHashCode()
extern const MethodInfo ContextAttribute_GetHashCode_m8535_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&ContextAttribute_GetHashCode_m8535/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo ContextAttribute_t1625_ContextAttribute_GetPropertiesForNewContext_m8536_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221920, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m8536_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ContextAttribute_GetPropertiesForNewContext_m8536/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ContextAttribute_t1625_ContextAttribute_GetPropertiesForNewContext_m8536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo ContextAttribute_t1625_ContextAttribute_IsContextOK_m8537_ParameterInfos[] = 
{
	{"ctx", 0, 134221921, 0, &Context_t1632_0_0_0},
	{"ctorMsg", 1, 134221922, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ContextAttribute_IsContextOK_m8537_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ContextAttribute_IsContextOK_m8537/* method */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, ContextAttribute_t1625_ContextAttribute_IsContextOK_m8537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContextAttribute_t1625_MethodInfos[] =
{
	&ContextAttribute__ctor_m8532_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&ContextAttribute_Equals_m8534_MethodInfo,
	&ContextAttribute_GetHashCode_m8535_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m8536_MethodInfo,
	&ContextAttribute_IsContextOK_m8537_MethodInfo,
	NULL
};
static const PropertyInfo ContextAttribute_t1625____Name_PropertyInfo = 
{
	&ContextAttribute_t1625_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &ContextAttribute_get_Name_m8533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContextAttribute_t1625_PropertyInfos[] =
{
	&ContextAttribute_t1625____Name_PropertyInfo,
	NULL
};
extern const MethodInfo ContextAttribute_Equals_m8534_MethodInfo;
extern const MethodInfo ContextAttribute_GetHashCode_m8535_MethodInfo;
extern const MethodInfo ContextAttribute_GetPropertiesForNewContext_m8536_MethodInfo;
extern const MethodInfo ContextAttribute_IsContextOK_m8537_MethodInfo;
static const Il2CppMethodReference ContextAttribute_t1625_VTable[] =
{
	&ContextAttribute_Equals_m8534_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ContextAttribute_GetHashCode_m8535_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m8536_MethodInfo,
	&ContextAttribute_IsContextOK_m8537_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&ContextAttribute_GetPropertiesForNewContext_m8536_MethodInfo,
	&ContextAttribute_IsContextOK_m8537_MethodInfo,
};
static bool ContextAttribute_t1625_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContextAttribute_t1625_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t1931_0_0_0,
	&IContextProperty_t1920_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t1625_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
	{ &IContextAttribute_t1931_0_0_0, 4},
	{ &IContextProperty_t1920_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextAttribute_t1625_1_0_0;
extern const Il2CppType Attribute_t539_0_0_0;
struct ContextAttribute_t1625;
const Il2CppTypeDefinitionMetadata ContextAttribute_t1625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t1625_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t1625_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ContextAttribute_t1625_VTable/* vtableMethods */
	, ContextAttribute_t1625_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1508/* fieldStart */

};
TypeInfo ContextAttribute_t1625_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, ContextAttribute_t1625_MethodInfos/* methods */
	, ContextAttribute_t1625_PropertyInfos/* properties */
	, NULL/* events */
	, &ContextAttribute_t1625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 511/* custom_attributes_cache */
	, &ContextAttribute_t1625_0_0_0/* byval_arg */
	, &ContextAttribute_t1625_1_0_0/* this_arg */
	, &ContextAttribute_t1625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t1625)/* instance_size */
	, sizeof (ContextAttribute_t1625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t1627_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern const MethodInfo CrossContextChannel__ctor_m8538_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CrossContextChannel__ctor_m8538/* method */
	, &CrossContextChannel_t1627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CrossContextChannel_t1627_MethodInfos[] =
{
	&CrossContextChannel__ctor_m8538_MethodInfo,
	NULL
};
static const Il2CppMethodReference CrossContextChannel_t1627_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CrossContextChannel_t1627_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CrossContextChannel_t1627_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1218_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t1627_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1218_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossContextChannel_t1627_0_0_0;
extern const Il2CppType CrossContextChannel_t1627_1_0_0;
struct CrossContextChannel_t1627;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t1627_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t1627_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t1627_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t1627_VTable/* vtableMethods */
	, CrossContextChannel_t1627_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CrossContextChannel_t1627_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, CrossContextChannel_t1627_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CrossContextChannel_t1627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t1627_0_0_0/* byval_arg */
	, &CrossContextChannel_t1627_1_0_0/* this_arg */
	, &CrossContextChannel_t1627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t1627)/* instance_size */
	, sizeof (CrossContextChannel_t1627)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t1931_il2cpp_TypeInfo;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo IContextAttribute_t1931_IContextAttribute_GetPropertiesForNewContext_m10967_ParameterInfos[] = 
{
	{"msg", 0, 134221923, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.IContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_GetPropertiesForNewContext_m10967_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, NULL/* method */
	, &IContextAttribute_t1931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IContextAttribute_t1931_IContextAttribute_GetPropertiesForNewContext_m10967_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo IContextAttribute_t1931_IContextAttribute_IsContextOK_m10968_ParameterInfos[] = 
{
	{"ctx", 0, 134221924, 0, &Context_t1632_0_0_0},
	{"msg", 1, 134221925, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo IContextAttribute_IsContextOK_m10968_MethodInfo = 
{
	"IsContextOK"/* name */
	, NULL/* method */
	, &IContextAttribute_t1931_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, IContextAttribute_t1931_IContextAttribute_IsContextOK_m10968_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextAttribute_t1931_MethodInfos[] =
{
	&IContextAttribute_GetPropertiesForNewContext_m10967_MethodInfo,
	&IContextAttribute_IsContextOK_m10968_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextAttribute_t1931_1_0_0;
struct IContextAttribute_t1931;
const Il2CppTypeDefinitionMetadata IContextAttribute_t1931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextAttribute_t1931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextAttribute_t1931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContextAttribute_t1931_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 512/* custom_attributes_cache */
	, &IContextAttribute_t1931_0_0_0/* byval_arg */
	, &IContextAttribute_t1931_1_0_0/* this_arg */
	, &IContextAttribute_t1931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t1920_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name()
extern const MethodInfo IContextProperty_get_Name_m10969_MethodInfo = 
{
	"get_Name"/* name */
	, NULL/* method */
	, &IContextProperty_t1920_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IContextProperty_t1920_MethodInfos[] =
{
	&IContextProperty_get_Name_m10969_MethodInfo,
	NULL
};
extern const MethodInfo IContextProperty_get_Name_m10969_MethodInfo;
static const PropertyInfo IContextProperty_t1920____Name_PropertyInfo = 
{
	&IContextProperty_t1920_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &IContextProperty_get_Name_m10969_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IContextProperty_t1920_PropertyInfos[] =
{
	&IContextProperty_t1920____Name_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextProperty_t1920_1_0_0;
struct IContextProperty_t1920;
const Il2CppTypeDefinitionMetadata IContextProperty_t1920_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContextProperty_t1920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContextProperty_t1920_MethodInfos/* methods */
	, IContextProperty_t1920_PropertyInfos/* properties */
	, NULL/* events */
	, &IContextProperty_t1920_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 513/* custom_attributes_cache */
	, &IContextProperty_t1920_0_0_0/* byval_arg */
	, &IContextProperty_t1920_1_0_0/* this_arg */
	, &IContextProperty_t1920_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t2010_il2cpp_TypeInfo;
static const MethodInfo* IContributeClientContextSink_t2010_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeClientContextSink_t2010_0_0_0;
extern const Il2CppType IContributeClientContextSink_t2010_1_0_0;
struct IContributeClientContextSink_t2010;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t2010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeClientContextSink_t2010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeClientContextSink_t2010_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeClientContextSink_t2010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 514/* custom_attributes_cache */
	, &IContributeClientContextSink_t2010_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t2010_1_0_0/* this_arg */
	, &IContributeClientContextSink_t2010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t2011_il2cpp_TypeInfo;
static const MethodInfo* IContributeServerContextSink_t2011_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeServerContextSink_t2011_0_0_0;
extern const Il2CppType IContributeServerContextSink_t2011_1_0_0;
struct IContributeServerContextSink_t2011;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t2011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IContributeServerContextSink_t2011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, IContributeServerContextSink_t2011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IContributeServerContextSink_t2011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 515/* custom_attributes_cache */
	, &IContributeServerContextSink_t2011_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t2011_1_0_0/* this_arg */
	, &IContributeServerContextSink_t2011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t1635_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor()
extern const MethodInfo SynchronizationAttribute__ctor_m8539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m8539/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1635_SynchronizationAttribute__ctor_m8540_ParameterInfos[] = 
{
	{"flag", 0, 134221926, 0, &Int32_t54_0_0_0},
	{"reEntrant", 1, 134221927, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::.ctor(System.Int32,System.Boolean)
extern const MethodInfo SynchronizationAttribute__ctor_m8540_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SynchronizationAttribute__ctor_m8540/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73/* invoker_method */
	, SynchronizationAttribute_t1635_SynchronizationAttribute__ctor_m8540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1635_SynchronizationAttribute_set_Locked_m8541_ParameterInfos[] = 
{
	{"value", 0, 134221928, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::set_Locked(System.Boolean)
extern const MethodInfo SynchronizationAttribute_set_Locked_m8541_MethodInfo = 
{
	"set_Locked"/* name */
	, (methodPointerType)&SynchronizationAttribute_set_Locked_m8541/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, SynchronizationAttribute_t1635_SynchronizationAttribute_set_Locked_m8541_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ReleaseLock()
extern const MethodInfo SynchronizationAttribute_ReleaseLock_m8542_MethodInfo = 
{
	"ReleaseLock"/* name */
	, (methodPointerType)&SynchronizationAttribute_ReleaseLock_m8542/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1635_SynchronizationAttribute_GetPropertiesForNewContext_m8543_ParameterInfos[] = 
{
	{"ctorMsg", 0, 134221929, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m8543_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_GetPropertiesForNewContext_m8543/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, SynchronizationAttribute_t1635_SynchronizationAttribute_GetPropertiesForNewContext_m8543_ParameterInfos/* parameters */
	, 517/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo SynchronizationAttribute_t1635_SynchronizationAttribute_IsContextOK_m8544_ParameterInfos[] = 
{
	{"ctx", 0, 134221930, 0, &Context_t1632_0_0_0},
	{"msg", 1, 134221931, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Contexts.SynchronizationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo SynchronizationAttribute_IsContextOK_m8544_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&SynchronizationAttribute_IsContextOK_m8544/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, SynchronizationAttribute_t1635_SynchronizationAttribute_IsContextOK_m8544_ParameterInfos/* parameters */
	, 518/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::ExitContext()
extern const MethodInfo SynchronizationAttribute_ExitContext_m8545_MethodInfo = 
{
	"ExitContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_ExitContext_m8545/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Contexts.SynchronizationAttribute::EnterContext()
extern const MethodInfo SynchronizationAttribute_EnterContext_m8546_MethodInfo = 
{
	"EnterContext"/* name */
	, (methodPointerType)&SynchronizationAttribute_EnterContext_m8546/* method */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SynchronizationAttribute_t1635_MethodInfos[] =
{
	&SynchronizationAttribute__ctor_m8539_MethodInfo,
	&SynchronizationAttribute__ctor_m8540_MethodInfo,
	&SynchronizationAttribute_set_Locked_m8541_MethodInfo,
	&SynchronizationAttribute_ReleaseLock_m8542_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m8543_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m8544_MethodInfo,
	&SynchronizationAttribute_ExitContext_m8545_MethodInfo,
	&SynchronizationAttribute_EnterContext_m8546_MethodInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_set_Locked_m8541_MethodInfo;
static const PropertyInfo SynchronizationAttribute_t1635____Locked_PropertyInfo = 
{
	&SynchronizationAttribute_t1635_il2cpp_TypeInfo/* parent */
	, "Locked"/* name */
	, NULL/* get */
	, &SynchronizationAttribute_set_Locked_m8541_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SynchronizationAttribute_t1635_PropertyInfos[] =
{
	&SynchronizationAttribute_t1635____Locked_PropertyInfo,
	NULL
};
extern const MethodInfo SynchronizationAttribute_GetPropertiesForNewContext_m8543_MethodInfo;
extern const MethodInfo SynchronizationAttribute_IsContextOK_m8544_MethodInfo;
static const Il2CppMethodReference SynchronizationAttribute_t1635_VTable[] =
{
	&ContextAttribute_Equals_m8534_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ContextAttribute_GetHashCode_m8535_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m8543_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m8544_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&ContextAttribute_get_Name_m8533_MethodInfo,
	&SynchronizationAttribute_GetPropertiesForNewContext_m8543_MethodInfo,
	&SynchronizationAttribute_IsContextOK_m8544_MethodInfo,
	&SynchronizationAttribute_set_Locked_m8541_MethodInfo,
};
static bool SynchronizationAttribute_t1635_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* SynchronizationAttribute_t1635_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t2010_0_0_0,
	&IContributeServerContextSink_t2011_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t1635_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t1931_0_0_0, 4},
	{ &IContextProperty_t1920_0_0_0, 6},
	{ &_Attribute_t832_0_0_0, 4},
	{ &IContributeClientContextSink_t2010_0_0_0, 10},
	{ &IContributeServerContextSink_t2011_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationAttribute_t1635_0_0_0;
extern const Il2CppType SynchronizationAttribute_t1635_1_0_0;
struct SynchronizationAttribute_t1635;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t1635_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t1635_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t1635_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1625_0_0_0/* parent */
	, SynchronizationAttribute_t1635_VTable/* vtableMethods */
	, SynchronizationAttribute_t1635_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1509/* fieldStart */

};
TypeInfo SynchronizationAttribute_t1635_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, SynchronizationAttribute_t1635_MethodInfos/* methods */
	, SynchronizationAttribute_t1635_PropertyInfos/* properties */
	, NULL/* events */
	, &SynchronizationAttribute_t1635_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 516/* custom_attributes_cache */
	, &SynchronizationAttribute_t1635_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t1635_1_0_0/* this_arg */
	, &SynchronizationAttribute_t1635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t1635)/* instance_size */
	, sizeof (SynchronizationAttribute_t1635)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t1636_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static const MethodInfo* ArgInfoType_t1636_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference ArgInfoType_t1636_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ArgInfoType_t1636_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair ArgInfoType_t1636_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfoType_t1636_0_0_0;
extern const Il2CppType ArgInfoType_t1636_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t367_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t1636_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t1636_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ArgInfoType_t1636_VTable/* vtableMethods */
	, ArgInfoType_t1636_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1514/* fieldStart */

};
TypeInfo ArgInfoType_t1636_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfoType_t1636_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t1636_0_0_0/* byval_arg */
	, &ArgInfoType_t1636_1_0_0/* this_arg */
	, &ArgInfoType_t1636_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t1636)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t1636)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t1637_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
extern const Il2CppType MethodBase_t47_0_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
extern const Il2CppType ArgInfoType_t1636_0_0_0;
static const ParameterInfo ArgInfo_t1637_ArgInfo__ctor_m8547_ParameterInfos[] = 
{
	{"method", 0, 134221932, 0, &MethodBase_t47_0_0_0},
	{"type", 1, 134221933, 0, &ArgInfoType_t1636_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Byte_t367 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ArgInfo::.ctor(System.Reflection.MethodBase,System.Runtime.Remoting.Messaging.ArgInfoType)
extern const MethodInfo ArgInfo__ctor_m8547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgInfo__ctor_m8547/* method */
	, &ArgInfo_t1637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Byte_t367/* invoker_method */
	, ArgInfo_t1637_ArgInfo__ctor_m8547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo ArgInfo_t1637_ArgInfo_GetInOutArgs_m8548_ParameterInfos[] = 
{
	{"args", 0, 134221934, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ArgInfo::GetInOutArgs(System.Object[])
extern const MethodInfo ArgInfo_GetInOutArgs_m8548_MethodInfo = 
{
	"GetInOutArgs"/* name */
	, (methodPointerType)&ArgInfo_GetInOutArgs_m8548/* method */
	, &ArgInfo_t1637_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ArgInfo_t1637_ArgInfo_GetInOutArgs_m8548_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgInfo_t1637_MethodInfos[] =
{
	&ArgInfo__ctor_m8547_MethodInfo,
	&ArgInfo_GetInOutArgs_m8548_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArgInfo_t1637_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ArgInfo_t1637_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfo_t1637_0_0_0;
extern const Il2CppType ArgInfo_t1637_1_0_0;
struct ArgInfo_t1637;
const Il2CppTypeDefinitionMetadata ArgInfo_t1637_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t1637_VTable/* vtableMethods */
	, ArgInfo_t1637_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1517/* fieldStart */

};
TypeInfo ArgInfo_t1637_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ArgInfo_t1637_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArgInfo_t1637_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t1637_0_0_0/* byval_arg */
	, &ArgInfo_t1637_1_0_0/* this_arg */
	, &ArgInfo_t1637_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t1637)/* instance_size */
	, sizeof (ArgInfo_t1637)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t1642_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::.ctor()
extern const MethodInfo AsyncResult__ctor_m8549_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AsyncResult__ctor_m8549/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncState()
extern const MethodInfo AsyncResult_get_AsyncState_m8550_MethodInfo = 
{
	"get_AsyncState"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncState_m8550/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WaitHandle_t1340_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Threading.WaitHandle System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncWaitHandle()
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo = 
{
	"get_AsyncWaitHandle"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncWaitHandle_m8551/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &WaitHandle_t1340_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_CompletedSynchronously()
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m8552_MethodInfo = 
{
	"get_CompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_get_CompletedSynchronously_m8552/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_IsCompleted()
extern const MethodInfo AsyncResult_get_IsCompleted_m8553_MethodInfo = 
{
	"get_IsCompleted"/* name */
	, (methodPointerType)&AsyncResult_get_IsCompleted_m8553/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.AsyncResult::get_EndInvokeCalled()
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m8554_MethodInfo = 
{
	"get_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_get_EndInvokeCalled_m8554/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_set_EndInvokeCalled_m8555_ParameterInfos[] = 
{
	{"value", 0, 134221935, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_EndInvokeCalled(System.Boolean)
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m8555_MethodInfo = 
{
	"set_EndInvokeCalled"/* name */
	, (methodPointerType)&AsyncResult_set_EndInvokeCalled_m8555/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, AsyncResult_t1642_AsyncResult_set_EndInvokeCalled_m8555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.AsyncResult::get_AsyncDelegate()
extern const MethodInfo AsyncResult_get_AsyncDelegate_m8556_MethodInfo = 
{
	"get_AsyncDelegate"/* name */
	, (methodPointerType)&AsyncResult_get_AsyncDelegate_m8556/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.AsyncResult::get_NextSink()
extern const MethodInfo AsyncResult_get_NextSink_m8557_MethodInfo = 
{
	"get_NextSink"/* name */
	, (methodPointerType)&AsyncResult_get_NextSink_m8557/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t1641_0_0_0;
extern const Il2CppType IMessageSink_t1218_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_AsyncProcessMessage_m8558_ParameterInfos[] = 
{
	{"msg", 0, 134221936, 0, &IMessage_t1641_0_0_0},
	{"replySink", 1, 134221937, 0, &IMessageSink_t1218_0_0_0},
};
extern const Il2CppType IMessageCtrl_t1640_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.AsyncResult::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo AsyncResult_AsyncProcessMessage_m8558_MethodInfo = 
{
	"AsyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_AsyncProcessMessage_m8558/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &IMessageCtrl_t1640_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, AsyncResult_t1642_AsyncResult_AsyncProcessMessage_m8558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::GetReplyMessage()
extern const MethodInfo AsyncResult_GetReplyMessage_m8559_MethodInfo = 
{
	"GetReplyMessage"/* name */
	, (methodPointerType)&AsyncResult_GetReplyMessage_m8559/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1641_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageCtrl_t1640_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_SetMessageCtrl_m8560_ParameterInfos[] = 
{
	{"mc", 0, 134221938, 0, &IMessageCtrl_t1640_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetMessageCtrl(System.Runtime.Remoting.Messaging.IMessageCtrl)
extern const MethodInfo AsyncResult_SetMessageCtrl_m8560_MethodInfo = 
{
	"SetMessageCtrl"/* name */
	, (methodPointerType)&AsyncResult_SetMessageCtrl_m8560/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AsyncResult_t1642_AsyncResult_SetMessageCtrl_m8560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_SetCompletedSynchronously_m8561_ParameterInfos[] = 
{
	{"completed", 0, 134221939, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::SetCompletedSynchronously(System.Boolean)
extern const MethodInfo AsyncResult_SetCompletedSynchronously_m8561_MethodInfo = 
{
	"SetCompletedSynchronously"/* name */
	, (methodPointerType)&AsyncResult_SetCompletedSynchronously_m8561/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, AsyncResult_t1642_AsyncResult_SetCompletedSynchronously_m8561_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::EndInvoke()
extern const MethodInfo AsyncResult_EndInvoke_m8562_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AsyncResult_EndInvoke_m8562/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1641_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessage_t1641_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_SyncProcessMessage_m8563_ParameterInfos[] = 
{
	{"msg", 0, 134221940, 0, &IMessage_t1641_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.AsyncResult::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern const MethodInfo AsyncResult_SyncProcessMessage_m8563_MethodInfo = 
{
	"SyncProcessMessage"/* name */
	, (methodPointerType)&AsyncResult_SyncProcessMessage_m8563/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &IMessage_t1641_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AsyncResult_t1642_AsyncResult_SyncProcessMessage_m8563_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t1639_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.MonoMethodMessage System.Runtime.Remoting.Messaging.AsyncResult::get_CallMessage()
extern const MethodInfo AsyncResult_get_CallMessage_m8564_MethodInfo = 
{
	"get_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_get_CallMessage_m8564/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &MonoMethodMessage_t1639_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MonoMethodMessage_t1639_0_0_0;
static const ParameterInfo AsyncResult_t1642_AsyncResult_set_CallMessage_m8565_ParameterInfos[] = 
{
	{"value", 0, 134221941, 0, &MonoMethodMessage_t1639_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.AsyncResult::set_CallMessage(System.Runtime.Remoting.Messaging.MonoMethodMessage)
extern const MethodInfo AsyncResult_set_CallMessage_m8565_MethodInfo = 
{
	"set_CallMessage"/* name */
	, (methodPointerType)&AsyncResult_set_CallMessage_m8565/* method */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, AsyncResult_t1642_AsyncResult_set_CallMessage_m8565_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AsyncResult_t1642_MethodInfos[] =
{
	&AsyncResult__ctor_m8549_MethodInfo,
	&AsyncResult_get_AsyncState_m8550_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m8552_MethodInfo,
	&AsyncResult_get_IsCompleted_m8553_MethodInfo,
	&AsyncResult_get_EndInvokeCalled_m8554_MethodInfo,
	&AsyncResult_set_EndInvokeCalled_m8555_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m8556_MethodInfo,
	&AsyncResult_get_NextSink_m8557_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m8558_MethodInfo,
	&AsyncResult_GetReplyMessage_m8559_MethodInfo,
	&AsyncResult_SetMessageCtrl_m8560_MethodInfo,
	&AsyncResult_SetCompletedSynchronously_m8561_MethodInfo,
	&AsyncResult_EndInvoke_m8562_MethodInfo,
	&AsyncResult_SyncProcessMessage_m8563_MethodInfo,
	&AsyncResult_get_CallMessage_m8564_MethodInfo,
	&AsyncResult_set_CallMessage_m8565_MethodInfo,
	NULL
};
extern const MethodInfo AsyncResult_get_AsyncState_m8550_MethodInfo;
static const PropertyInfo AsyncResult_t1642____AsyncState_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "AsyncState"/* name */
	, &AsyncResult_get_AsyncState_m8550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo;
static const PropertyInfo AsyncResult_t1642____AsyncWaitHandle_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "AsyncWaitHandle"/* name */
	, &AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CompletedSynchronously_m8552_MethodInfo;
static const PropertyInfo AsyncResult_t1642____CompletedSynchronously_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "CompletedSynchronously"/* name */
	, &AsyncResult_get_CompletedSynchronously_m8552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_IsCompleted_m8553_MethodInfo;
static const PropertyInfo AsyncResult_t1642____IsCompleted_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "IsCompleted"/* name */
	, &AsyncResult_get_IsCompleted_m8553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_EndInvokeCalled_m8554_MethodInfo;
extern const MethodInfo AsyncResult_set_EndInvokeCalled_m8555_MethodInfo;
static const PropertyInfo AsyncResult_t1642____EndInvokeCalled_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "EndInvokeCalled"/* name */
	, &AsyncResult_get_EndInvokeCalled_m8554_MethodInfo/* get */
	, &AsyncResult_set_EndInvokeCalled_m8555_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_AsyncDelegate_m8556_MethodInfo;
static const PropertyInfo AsyncResult_t1642____AsyncDelegate_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "AsyncDelegate"/* name */
	, &AsyncResult_get_AsyncDelegate_m8556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_NextSink_m8557_MethodInfo;
static const PropertyInfo AsyncResult_t1642____NextSink_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "NextSink"/* name */
	, &AsyncResult_get_NextSink_m8557_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AsyncResult_get_CallMessage_m8564_MethodInfo;
extern const MethodInfo AsyncResult_set_CallMessage_m8565_MethodInfo;
static const PropertyInfo AsyncResult_t1642____CallMessage_PropertyInfo = 
{
	&AsyncResult_t1642_il2cpp_TypeInfo/* parent */
	, "CallMessage"/* name */
	, &AsyncResult_get_CallMessage_m8564_MethodInfo/* get */
	, &AsyncResult_set_CallMessage_m8565_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AsyncResult_t1642_PropertyInfos[] =
{
	&AsyncResult_t1642____AsyncState_PropertyInfo,
	&AsyncResult_t1642____AsyncWaitHandle_PropertyInfo,
	&AsyncResult_t1642____CompletedSynchronously_PropertyInfo,
	&AsyncResult_t1642____IsCompleted_PropertyInfo,
	&AsyncResult_t1642____EndInvokeCalled_PropertyInfo,
	&AsyncResult_t1642____AsyncDelegate_PropertyInfo,
	&AsyncResult_t1642____NextSink_PropertyInfo,
	&AsyncResult_t1642____CallMessage_PropertyInfo,
	NULL
};
extern const MethodInfo AsyncResult_AsyncProcessMessage_m8558_MethodInfo;
extern const MethodInfo AsyncResult_GetReplyMessage_m8559_MethodInfo;
extern const MethodInfo AsyncResult_SetMessageCtrl_m8560_MethodInfo;
extern const MethodInfo AsyncResult_SyncProcessMessage_m8563_MethodInfo;
static const Il2CppMethodReference AsyncResult_t1642_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&AsyncResult_get_AsyncState_m8550_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo,
	&AsyncResult_get_IsCompleted_m8553_MethodInfo,
	&AsyncResult_get_AsyncState_m8550_MethodInfo,
	&AsyncResult_get_AsyncWaitHandle_m8551_MethodInfo,
	&AsyncResult_get_CompletedSynchronously_m8552_MethodInfo,
	&AsyncResult_get_IsCompleted_m8553_MethodInfo,
	&AsyncResult_get_AsyncDelegate_m8556_MethodInfo,
	&AsyncResult_get_NextSink_m8557_MethodInfo,
	&AsyncResult_AsyncProcessMessage_m8558_MethodInfo,
	&AsyncResult_GetReplyMessage_m8559_MethodInfo,
	&AsyncResult_SetMessageCtrl_m8560_MethodInfo,
	&AsyncResult_SyncProcessMessage_m8563_MethodInfo,
};
static bool AsyncResult_t1642_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const Il2CppType* AsyncResult_t1642_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t213_0_0_0,
	&IMessageSink_t1218_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t1642_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t213_0_0_0, 4},
	{ &IMessageSink_t1218_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncResult_t1642_0_0_0;
extern const Il2CppType AsyncResult_t1642_1_0_0;
struct AsyncResult_t1642;
const Il2CppTypeDefinitionMetadata AsyncResult_t1642_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t1642_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t1642_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t1642_VTable/* vtableMethods */
	, AsyncResult_t1642_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1520/* fieldStart */

};
TypeInfo AsyncResult_t1642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, AsyncResult_t1642_MethodInfos/* methods */
	, AsyncResult_t1642_PropertyInfos/* properties */
	, NULL/* events */
	, &AsyncResult_t1642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 519/* custom_attributes_cache */
	, &AsyncResult_t1642_0_0_0/* byval_arg */
	, &AsyncResult_t1642_1_0_0/* this_arg */
	, &AsyncResult_t1642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t1642)/* instance_size */
	, sizeof (AsyncResult_t1642)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t1643_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall__ctor_m8566_ParameterInfos[] = 
{
	{"type", 0, 134221942, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Type)
extern const MethodInfo ConstructionCall__ctor_m8566_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m8566/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall__ctor_m8566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall__ctor_m8567_ParameterInfos[] = 
{
	{"info", 0, 134221943, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221944, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall__ctor_m8567_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCall__ctor_m8567/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall__ctor_m8567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitDictionary()
extern const MethodInfo ConstructionCall_InitDictionary_m8568_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&ConstructionCall_InitDictionary_m8568/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall_set_IsContextOk_m8569_ParameterInfos[] = 
{
	{"value", 0, 134221945, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_IsContextOk(System.Boolean)
extern const MethodInfo ConstructionCall_set_IsContextOk_m8569_MethodInfo = 
{
	"set_IsContextOk"/* name */
	, (methodPointerType)&ConstructionCall_set_IsContextOk_m8569/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall_set_IsContextOk_m8569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationType()
extern const MethodInfo ConstructionCall_get_ActivationType_m8570_MethodInfo = 
{
	"get_ActivationType"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationType_m8570/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ConstructionCall::get_ActivationTypeName()
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m8571_MethodInfo = 
{
	"get_ActivationTypeName"/* name */
	, (methodPointerType)&ConstructionCall_get_ActivationTypeName_m8571/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Messaging.ConstructionCall::get_Activator()
extern const MethodInfo ConstructionCall_get_Activator_m8572_MethodInfo = 
{
	"get_Activator"/* name */
	, (methodPointerType)&ConstructionCall_get_Activator_m8572/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &IActivator_t1618_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IActivator_t1618_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall_set_Activator_m8573_ParameterInfos[] = 
{
	{"value", 0, 134221946, 0, &IActivator_t1618_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator)
extern const MethodInfo ConstructionCall_set_Activator_m8573_MethodInfo = 
{
	"set_Activator"/* name */
	, (methodPointerType)&ConstructionCall_set_Activator_m8573/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall_set_Activator_m8573_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ConstructionCall::get_CallSiteActivationAttributes()
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m8574_MethodInfo = 
{
	"get_CallSiteActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_get_CallSiteActivationAttributes_m8574/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall_SetActivationAttributes_m8575_ParameterInfos[] = 
{
	{"attributes", 0, 134221947, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::SetActivationAttributes(System.Object[])
extern const MethodInfo ConstructionCall_SetActivationAttributes_m8575_MethodInfo = 
{
	"SetActivationAttributes"/* name */
	, (methodPointerType)&ConstructionCall_SetActivationAttributes_m8575/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall_SetActivationAttributes_m8575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IList System.Runtime.Remoting.Messaging.ConstructionCall::get_ContextProperties()
extern const MethodInfo ConstructionCall_get_ContextProperties_m8576_MethodInfo = 
{
	"get_ContextProperties"/* name */
	, (methodPointerType)&ConstructionCall_get_ContextProperties_m8576/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &IList_t861_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall_InitMethodProperty_m8577_ParameterInfos[] = 
{
	{"key", 0, 134221948, 0, &String_t_0_0_0},
	{"value", 1, 134221949, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCall_InitMethodProperty_m8577_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&ConstructionCall_InitMethodProperty_m8577/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall_InitMethodProperty_m8577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ConstructionCall_t1643_ConstructionCall_GetObjectData_m8578_ParameterInfos[] = 
{
	{"info", 0, 134221950, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221951, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ConstructionCall_GetObjectData_m8578_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ConstructionCall_GetObjectData_m8578/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ConstructionCall_t1643_ConstructionCall_GetObjectData_m8578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1081_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ConstructionCall::get_Properties()
extern const MethodInfo ConstructionCall_get_Properties_m8579_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ConstructionCall_get_Properties_m8579/* method */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCall_t1643_MethodInfos[] =
{
	&ConstructionCall__ctor_m8566_MethodInfo,
	&ConstructionCall__ctor_m8567_MethodInfo,
	&ConstructionCall_InitDictionary_m8568_MethodInfo,
	&ConstructionCall_set_IsContextOk_m8569_MethodInfo,
	&ConstructionCall_get_ActivationType_m8570_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m8571_MethodInfo,
	&ConstructionCall_get_Activator_m8572_MethodInfo,
	&ConstructionCall_set_Activator_m8573_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m8574_MethodInfo,
	&ConstructionCall_SetActivationAttributes_m8575_MethodInfo,
	&ConstructionCall_get_ContextProperties_m8576_MethodInfo,
	&ConstructionCall_InitMethodProperty_m8577_MethodInfo,
	&ConstructionCall_GetObjectData_m8578_MethodInfo,
	&ConstructionCall_get_Properties_m8579_MethodInfo,
	NULL
};
extern const MethodInfo ConstructionCall_set_IsContextOk_m8569_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____IsContextOk_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "IsContextOk"/* name */
	, NULL/* get */
	, &ConstructionCall_set_IsContextOk_m8569_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationType_m8570_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____ActivationType_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "ActivationType"/* name */
	, &ConstructionCall_get_ActivationType_m8570_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ActivationTypeName_m8571_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____ActivationTypeName_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "ActivationTypeName"/* name */
	, &ConstructionCall_get_ActivationTypeName_m8571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Activator_m8572_MethodInfo;
extern const MethodInfo ConstructionCall_set_Activator_m8573_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____Activator_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "Activator"/* name */
	, &ConstructionCall_get_Activator_m8572_MethodInfo/* get */
	, &ConstructionCall_set_Activator_m8573_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_CallSiteActivationAttributes_m8574_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____CallSiteActivationAttributes_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "CallSiteActivationAttributes"/* name */
	, &ConstructionCall_get_CallSiteActivationAttributes_m8574_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_ContextProperties_m8576_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____ContextProperties_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "ContextProperties"/* name */
	, &ConstructionCall_get_ContextProperties_m8576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ConstructionCall_get_Properties_m8579_MethodInfo;
static const PropertyInfo ConstructionCall_t1643____Properties_PropertyInfo = 
{
	&ConstructionCall_t1643_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ConstructionCall_get_Properties_m8579_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ConstructionCall_t1643_PropertyInfos[] =
{
	&ConstructionCall_t1643____IsContextOk_PropertyInfo,
	&ConstructionCall_t1643____ActivationType_PropertyInfo,
	&ConstructionCall_t1643____ActivationTypeName_PropertyInfo,
	&ConstructionCall_t1643____Activator_PropertyInfo,
	&ConstructionCall_t1643____CallSiteActivationAttributes_PropertyInfo,
	&ConstructionCall_t1643____ContextProperties_PropertyInfo,
	&ConstructionCall_t1643____Properties_PropertyInfo,
	NULL
};
extern const MethodInfo ConstructionCall_GetObjectData_m8578_MethodInfo;
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo;
extern const MethodInfo MethodCall_get_Args_m8600_MethodInfo;
extern const MethodInfo MethodCall_get_LogicalCallContext_m8601_MethodInfo;
extern const MethodInfo MethodCall_get_MethodBase_m8602_MethodInfo;
extern const MethodInfo MethodCall_get_MethodName_m8603_MethodInfo;
extern const MethodInfo MethodCall_get_MethodSignature_m8604_MethodInfo;
extern const MethodInfo MethodCall_get_TypeName_m8607_MethodInfo;
extern const MethodInfo MethodCall_get_Uri_m8608_MethodInfo;
extern const MethodInfo ConstructionCall_InitMethodProperty_m8577_MethodInfo;
extern const MethodInfo ConstructionCall_InitDictionary_m8568_MethodInfo;
extern const MethodInfo MethodCall_set_Uri_m8609_MethodInfo;
extern const MethodInfo MethodCall_Init_m8610_MethodInfo;
static const Il2CppMethodReference ConstructionCall_t1643_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ConstructionCall_GetObjectData_m8578_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo,
	&MethodCall_get_Args_m8600_MethodInfo,
	&MethodCall_get_LogicalCallContext_m8601_MethodInfo,
	&MethodCall_get_MethodBase_m8602_MethodInfo,
	&MethodCall_get_MethodName_m8603_MethodInfo,
	&MethodCall_get_MethodSignature_m8604_MethodInfo,
	&MethodCall_get_TypeName_m8607_MethodInfo,
	&MethodCall_get_Uri_m8608_MethodInfo,
	&ConstructionCall_InitMethodProperty_m8577_MethodInfo,
	&ConstructionCall_GetObjectData_m8578_MethodInfo,
	&ConstructionCall_get_Properties_m8579_MethodInfo,
	&ConstructionCall_InitDictionary_m8568_MethodInfo,
	&MethodCall_set_Uri_m8609_MethodInfo,
	&MethodCall_Init_m8610_MethodInfo,
	&ConstructionCall_get_ActivationType_m8570_MethodInfo,
	&ConstructionCall_get_ActivationTypeName_m8571_MethodInfo,
	&ConstructionCall_get_Activator_m8572_MethodInfo,
	&ConstructionCall_set_Activator_m8573_MethodInfo,
	&ConstructionCall_get_CallSiteActivationAttributes_m8574_MethodInfo,
	&ConstructionCall_get_ContextProperties_m8576_MethodInfo,
};
static bool ConstructionCall_t1643_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ConstructionCall_t1643_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t1918_0_0_0,
	&IMessage_t1641_0_0_0,
	&IMethodCallMessage_t1922_0_0_0,
	&IMethodMessage_t1653_0_0_0,
};
extern const Il2CppType ISerializable_t428_0_0_0;
extern const Il2CppType IInternalMessage_t1934_0_0_0;
extern const Il2CppType ISerializationRootObject_t2013_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t1643_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IInternalMessage_t1934_0_0_0, 5},
	{ &IMessage_t1641_0_0_0, 6},
	{ &IMethodCallMessage_t1922_0_0_0, 6},
	{ &IMethodMessage_t1653_0_0_0, 6},
	{ &ISerializationRootObject_t2013_0_0_0, 13},
	{ &IConstructionCallMessage_t1918_0_0_0, 19},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCall_t1643_1_0_0;
extern const Il2CppType MethodCall_t1644_0_0_0;
struct ConstructionCall_t1643;
const Il2CppTypeDefinitionMetadata ConstructionCall_t1643_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t1643_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t1643_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t1644_0_0_0/* parent */
	, ConstructionCall_t1643_VTable/* vtableMethods */
	, ConstructionCall_t1643_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1535/* fieldStart */

};
TypeInfo ConstructionCall_t1643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCall_t1643_MethodInfos/* methods */
	, ConstructionCall_t1643_PropertyInfos/* properties */
	, NULL/* events */
	, &ConstructionCall_t1643_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 520/* custom_attributes_cache */
	, &ConstructionCall_t1643_0_0_0/* byval_arg */
	, &ConstructionCall_t1643_1_0_0/* this_arg */
	, &ConstructionCall_t1643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t1643)/* instance_size */
	, sizeof (ConstructionCall_t1643)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t1643_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t1645_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1645_ConstructionCallDictionary__ctor_m8580_ParameterInfos[] = 
{
	{"message", 0, 134221952, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.ctor(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ConstructionCallDictionary__ctor_m8580_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__ctor_m8580/* method */
	, &ConstructionCallDictionary_t1645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1645_ConstructionCallDictionary__ctor_m8580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::.cctor()
extern const MethodInfo ConstructionCallDictionary__cctor_m8581_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ConstructionCallDictionary__cctor_m8581/* method */
	, &ConstructionCallDictionary_t1645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1645_ConstructionCallDictionary_GetMethodProperty_m8582_ParameterInfos[] = 
{
	{"key", 0, 134221953, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ConstructionCallDictionary::GetMethodProperty(System.String)
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m8582_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_GetMethodProperty_m8582/* method */
	, &ConstructionCallDictionary_t1645_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1645_ConstructionCallDictionary_GetMethodProperty_m8582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructionCallDictionary_t1645_ConstructionCallDictionary_SetMethodProperty_m8583_ParameterInfos[] = 
{
	{"key", 0, 134221954, 0, &String_t_0_0_0},
	{"value", 1, 134221955, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ConstructionCallDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m8583_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&ConstructionCallDictionary_SetMethodProperty_m8583/* method */
	, &ConstructionCallDictionary_t1645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ConstructionCallDictionary_t1645_ConstructionCallDictionary_SetMethodProperty_m8583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructionCallDictionary_t1645_MethodInfos[] =
{
	&ConstructionCallDictionary__ctor_m8580_MethodInfo,
	&ConstructionCallDictionary__cctor_m8581_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m8582_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m8583_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo;
extern const MethodInfo MethodDictionary_get_Count_m8637_MethodInfo;
extern const MethodInfo MethodDictionary_get_IsSynchronized_m8638_MethodInfo;
extern const MethodInfo MethodDictionary_get_SyncRoot_m8639_MethodInfo;
extern const MethodInfo MethodDictionary_CopyTo_m8640_MethodInfo;
extern const MethodInfo MethodDictionary_get_Item_m8629_MethodInfo;
extern const MethodInfo MethodDictionary_set_Item_m8630_MethodInfo;
extern const MethodInfo MethodDictionary_Add_m8634_MethodInfo;
extern const MethodInfo MethodDictionary_Contains_m8635_MethodInfo;
extern const MethodInfo MethodDictionary_GetEnumerator_m8641_MethodInfo;
extern const MethodInfo MethodDictionary_Remove_m8636_MethodInfo;
extern const MethodInfo MethodDictionary_AllocInternalProperties_m8626_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_GetMethodProperty_m8582_MethodInfo;
extern const MethodInfo ConstructionCallDictionary_SetMethodProperty_m8583_MethodInfo;
extern const MethodInfo MethodDictionary_get_Values_m8633_MethodInfo;
static const Il2CppMethodReference ConstructionCallDictionary_t1645_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo,
	&MethodDictionary_get_Count_m8637_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m8638_MethodInfo,
	&MethodDictionary_get_SyncRoot_m8639_MethodInfo,
	&MethodDictionary_CopyTo_m8640_MethodInfo,
	&MethodDictionary_get_Item_m8629_MethodInfo,
	&MethodDictionary_set_Item_m8630_MethodInfo,
	&MethodDictionary_Add_m8634_MethodInfo,
	&MethodDictionary_Contains_m8635_MethodInfo,
	&MethodDictionary_GetEnumerator_m8641_MethodInfo,
	&MethodDictionary_Remove_m8636_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m8626_MethodInfo,
	&ConstructionCallDictionary_GetMethodProperty_m8582_MethodInfo,
	&ConstructionCallDictionary_SetMethodProperty_m8583_MethodInfo,
	&MethodDictionary_get_Values_m8633_MethodInfo,
};
static bool ConstructionCallDictionary_t1645_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t464_0_0_0;
extern const Il2CppType ICollection_t860_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t1645_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICollection_t860_0_0_0, 5},
	{ &IDictionary_t1081_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCallDictionary_t1645_0_0_0;
extern const Il2CppType ConstructionCallDictionary_t1645_1_0_0;
extern const Il2CppType MethodDictionary_t1646_0_0_0;
struct ConstructionCallDictionary_t1645;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t1645_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t1645_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1646_0_0_0/* parent */
	, ConstructionCallDictionary_t1645_VTable/* vtableMethods */
	, ConstructionCallDictionary_t1645_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1542/* fieldStart */

};
TypeInfo ConstructionCallDictionary_t1645_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ConstructionCallDictionary_t1645_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructionCallDictionary_t1645_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t1645_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t1645_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t1645_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t1645)/* instance_size */
	, sizeof (ConstructionCallDictionary_t1645)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t1645_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t1647_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.ctor()
extern const MethodInfo EnvoyTerminatorSink__ctor_m8584_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__ctor_m8584/* method */
	, &EnvoyTerminatorSink_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::.cctor()
extern const MethodInfo EnvoyTerminatorSink__cctor_m8585_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&EnvoyTerminatorSink__cctor_m8585/* method */
	, &EnvoyTerminatorSink_t1647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyTerminatorSink_t1647_MethodInfos[] =
{
	&EnvoyTerminatorSink__ctor_m8584_MethodInfo,
	&EnvoyTerminatorSink__cctor_m8585_MethodInfo,
	NULL
};
static const Il2CppMethodReference EnvoyTerminatorSink_t1647_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool EnvoyTerminatorSink_t1647_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* EnvoyTerminatorSink_t1647_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1218_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t1647_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1218_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyTerminatorSink_t1647_0_0_0;
extern const Il2CppType EnvoyTerminatorSink_t1647_1_0_0;
struct EnvoyTerminatorSink_t1647;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t1647_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t1647_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t1647_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t1647_VTable/* vtableMethods */
	, EnvoyTerminatorSink_t1647_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1545/* fieldStart */

};
TypeInfo EnvoyTerminatorSink_t1647_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, EnvoyTerminatorSink_t1647_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EnvoyTerminatorSink_t1647_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t1647_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t1647_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t1647_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t1647)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t1647)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t1647_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t1648_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Header_t1648_Header__ctor_m8586_ParameterInfos[] = 
{
	{"_Name", 0, 134221956, 0, &String_t_0_0_0},
	{"_Value", 1, 134221957, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object)
extern const MethodInfo Header__ctor_m8586_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m8586/* method */
	, &Header_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Header_t1648_Header__ctor_m8586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Header_t1648_Header__ctor_m8587_ParameterInfos[] = 
{
	{"_Name", 0, 134221958, 0, &String_t_0_0_0},
	{"_Value", 1, 134221959, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134221960, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean)
extern const MethodInfo Header__ctor_m8587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m8587/* method */
	, &Header_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73/* invoker_method */
	, Header_t1648_Header__ctor_m8587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Header_t1648_Header__ctor_m8588_ParameterInfos[] = 
{
	{"_Name", 0, 134221961, 0, &String_t_0_0_0},
	{"_Value", 1, 134221962, 0, &Object_t_0_0_0},
	{"_MustUnderstand", 2, 134221963, 0, &Boolean_t72_0_0_0},
	{"_HeaderNamespace", 3, 134221964, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.Header::.ctor(System.String,System.Object,System.Boolean,System.String)
extern const MethodInfo Header__ctor_m8588_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Header__ctor_m8588/* method */
	, &Header_t1648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_Object_t/* invoker_method */
	, Header_t1648_Header__ctor_m8588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Header_t1648_MethodInfos[] =
{
	&Header__ctor_m8586_MethodInfo,
	&Header__ctor_m8587_MethodInfo,
	&Header__ctor_m8588_MethodInfo,
	NULL
};
static const Il2CppMethodReference Header_t1648_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Header_t1648_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Header_t1648_0_0_0;
extern const Il2CppType Header_t1648_1_0_0;
struct Header_t1648;
const Il2CppTypeDefinitionMetadata Header_t1648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t1648_VTable/* vtableMethods */
	, Header_t1648_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1546/* fieldStart */

};
TypeInfo Header_t1648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, Header_t1648_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Header_t1648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 524/* custom_attributes_cache */
	, &Header_t1648_0_0_0/* byval_arg */
	, &Header_t1648_1_0_0/* this_arg */
	, &Header_t1648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t1648)/* instance_size */
	, sizeof (Header_t1648)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t1934_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IInternalMessage_t1934_IInternalMessage_set_Uri_m10970_ParameterInfos[] = 
{
	{"value", 0, 134221965, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.IInternalMessage::set_Uri(System.String)
extern const MethodInfo IInternalMessage_set_Uri_m10970_MethodInfo = 
{
	"set_Uri"/* name */
	, NULL/* method */
	, &IInternalMessage_t1934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IInternalMessage_t1934_IInternalMessage_set_Uri_m10970_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IInternalMessage_t1934_MethodInfos[] =
{
	&IInternalMessage_set_Uri_m10970_MethodInfo,
	NULL
};
extern const MethodInfo IInternalMessage_set_Uri_m10970_MethodInfo;
static const PropertyInfo IInternalMessage_t1934____Uri_PropertyInfo = 
{
	&IInternalMessage_t1934_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, NULL/* get */
	, &IInternalMessage_set_Uri_m10970_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IInternalMessage_t1934_PropertyInfos[] =
{
	&IInternalMessage_t1934____Uri_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IInternalMessage_t1934_1_0_0;
struct IInternalMessage_t1934;
const Il2CppTypeDefinitionMetadata IInternalMessage_t1934_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IInternalMessage_t1934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IInternalMessage_t1934_MethodInfos/* methods */
	, IInternalMessage_t1934_PropertyInfos/* properties */
	, NULL/* events */
	, &IInternalMessage_t1934_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t1934_0_0_0/* byval_arg */
	, &IInternalMessage_t1934_1_0_0/* this_arg */
	, &IInternalMessage_t1934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t1641_il2cpp_TypeInfo;
static const MethodInfo* IMessage_t1641_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessage_t1641_1_0_0;
struct IMessage_t1641;
const Il2CppTypeDefinitionMetadata IMessage_t1641_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessage_t1641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessage_t1641_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessage_t1641_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 525/* custom_attributes_cache */
	, &IMessage_t1641_0_0_0/* byval_arg */
	, &IMessage_t1641_1_0_0/* this_arg */
	, &IMessage_t1641_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t1640_il2cpp_TypeInfo;
static const MethodInfo* IMessageCtrl_t1640_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageCtrl_t1640_1_0_0;
struct IMessageCtrl_t1640;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t1640_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageCtrl_t1640_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageCtrl_t1640_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageCtrl_t1640_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 526/* custom_attributes_cache */
	, &IMessageCtrl_t1640_0_0_0/* byval_arg */
	, &IMessageCtrl_t1640_1_0_0/* this_arg */
	, &IMessageCtrl_t1640_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t1218_il2cpp_TypeInfo;
static const MethodInfo* IMessageSink_t1218_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageSink_t1218_1_0_0;
struct IMessageSink_t1218;
const Il2CppTypeDefinitionMetadata IMessageSink_t1218_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMessageSink_t1218_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMessageSink_t1218_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMessageSink_t1218_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 527/* custom_attributes_cache */
	, &IMessageSink_t1218_0_0_0/* byval_arg */
	, &IMessageSink_t1218_1_0_0/* this_arg */
	, &IMessageSink_t1218_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t1922_il2cpp_TypeInfo;
static const MethodInfo* IMethodCallMessage_t1922_MethodInfos[] =
{
	NULL
};
static const Il2CppType* IMethodCallMessage_t1922_InterfacesTypeInfos[] = 
{
	&IMessage_t1641_0_0_0,
	&IMethodMessage_t1653_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodCallMessage_t1922_1_0_0;
struct IMethodCallMessage_t1922;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t1922_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t1922_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodCallMessage_t1922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodCallMessage_t1922_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMethodCallMessage_t1922_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 528/* custom_attributes_cache */
	, &IMethodCallMessage_t1922_0_0_0/* byval_arg */
	, &IMethodCallMessage_t1922_1_0_0/* this_arg */
	, &IMethodCallMessage_t1922_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t1653_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args()
extern const MethodInfo IMethodMessage_get_Args_m10971_MethodInfo = 
{
	"get_Args"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogicalCallContext_t1650_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.IMethodMessage::get_LogicalCallContext()
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m10972_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1650_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase()
extern const MethodInfo IMethodMessage_get_MethodBase_m10973_MethodInfo = 
{
	"get_MethodBase"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodName()
extern const MethodInfo IMethodMessage_get_MethodName_m10974_MethodInfo = 
{
	"get_MethodName"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodSignature()
extern const MethodInfo IMethodMessage_get_MethodSignature_m10975_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_TypeName()
extern const MethodInfo IMethodMessage_get_TypeName_m10976_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri()
extern const MethodInfo IMethodMessage_get_Uri_m10977_MethodInfo = 
{
	"get_Uri"/* name */
	, NULL/* method */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodMessage_t1653_MethodInfos[] =
{
	&IMethodMessage_get_Args_m10971_MethodInfo,
	&IMethodMessage_get_LogicalCallContext_m10972_MethodInfo,
	&IMethodMessage_get_MethodBase_m10973_MethodInfo,
	&IMethodMessage_get_MethodName_m10974_MethodInfo,
	&IMethodMessage_get_MethodSignature_m10975_MethodInfo,
	&IMethodMessage_get_TypeName_m10976_MethodInfo,
	&IMethodMessage_get_Uri_m10977_MethodInfo,
	NULL
};
extern const MethodInfo IMethodMessage_get_Args_m10971_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____Args_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &IMethodMessage_get_Args_m10971_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_LogicalCallContext_m10972_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____LogicalCallContext_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &IMethodMessage_get_LogicalCallContext_m10972_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodBase_m10973_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____MethodBase_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &IMethodMessage_get_MethodBase_m10973_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodName_m10974_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____MethodName_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &IMethodMessage_get_MethodName_m10974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_MethodSignature_m10975_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____MethodSignature_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &IMethodMessage_get_MethodSignature_m10975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_TypeName_m10976_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____TypeName_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IMethodMessage_get_TypeName_m10976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodMessage_get_Uri_m10977_MethodInfo;
static const PropertyInfo IMethodMessage_t1653____Uri_PropertyInfo = 
{
	&IMethodMessage_t1653_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &IMethodMessage_get_Uri_m10977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodMessage_t1653_PropertyInfos[] =
{
	&IMethodMessage_t1653____Args_PropertyInfo,
	&IMethodMessage_t1653____LogicalCallContext_PropertyInfo,
	&IMethodMessage_t1653____MethodBase_PropertyInfo,
	&IMethodMessage_t1653____MethodName_PropertyInfo,
	&IMethodMessage_t1653____MethodSignature_PropertyInfo,
	&IMethodMessage_t1653____TypeName_PropertyInfo,
	&IMethodMessage_t1653____Uri_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodMessage_t1653_InterfacesTypeInfos[] = 
{
	&IMessage_t1641_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodMessage_t1653_1_0_0;
struct IMethodMessage_t1653;
const Il2CppTypeDefinitionMetadata IMethodMessage_t1653_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t1653_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodMessage_t1653_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodMessage_t1653_MethodInfos/* methods */
	, IMethodMessage_t1653_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodMessage_t1653_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 529/* custom_attributes_cache */
	, &IMethodMessage_t1653_0_0_0/* byval_arg */
	, &IMethodMessage_t1653_1_0_0/* this_arg */
	, &IMethodMessage_t1653_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t1921_il2cpp_TypeInfo;
extern const Il2CppType Exception_t42_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception()
extern const MethodInfo IMethodReturnMessage_get_Exception_m10978_MethodInfo = 
{
	"get_Exception"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t1921_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_OutArgs()
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m10979_MethodInfo = 
{
	"get_OutArgs"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t1921_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_ReturnValue()
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m10980_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, NULL/* method */
	, &IMethodReturnMessage_t1921_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMethodReturnMessage_t1921_MethodInfos[] =
{
	&IMethodReturnMessage_get_Exception_m10978_MethodInfo,
	&IMethodReturnMessage_get_OutArgs_m10979_MethodInfo,
	&IMethodReturnMessage_get_ReturnValue_m10980_MethodInfo,
	NULL
};
extern const MethodInfo IMethodReturnMessage_get_Exception_m10978_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t1921____Exception_PropertyInfo = 
{
	&IMethodReturnMessage_t1921_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &IMethodReturnMessage_get_Exception_m10978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_OutArgs_m10979_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t1921____OutArgs_PropertyInfo = 
{
	&IMethodReturnMessage_t1921_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &IMethodReturnMessage_get_OutArgs_m10979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMethodReturnMessage_get_ReturnValue_m10980_MethodInfo;
static const PropertyInfo IMethodReturnMessage_t1921____ReturnValue_PropertyInfo = 
{
	&IMethodReturnMessage_t1921_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &IMethodReturnMessage_get_ReturnValue_m10980_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMethodReturnMessage_t1921_PropertyInfos[] =
{
	&IMethodReturnMessage_t1921____Exception_PropertyInfo,
	&IMethodReturnMessage_t1921____OutArgs_PropertyInfo,
	&IMethodReturnMessage_t1921____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppType* IMethodReturnMessage_t1921_InterfacesTypeInfos[] = 
{
	&IMessage_t1641_0_0_0,
	&IMethodMessage_t1653_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodReturnMessage_t1921_0_0_0;
extern const Il2CppType IMethodReturnMessage_t1921_1_0_0;
struct IMethodReturnMessage_t1921;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t1921_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t1921_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMethodReturnMessage_t1921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IMethodReturnMessage_t1921_MethodInfos/* methods */
	, IMethodReturnMessage_t1921_PropertyInfos/* properties */
	, NULL/* events */
	, &IMethodReturnMessage_t1921_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 530/* custom_attributes_cache */
	, &IMethodReturnMessage_t1921_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t1921_1_0_0/* this_arg */
	, &IMethodReturnMessage_t1921_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t2012_il2cpp_TypeInfo;
static const MethodInfo* IRemotingFormatter_t2012_MethodInfos[] =
{
	NULL
};
extern const Il2CppType IFormatter_t2014_0_0_0;
static const Il2CppType* IRemotingFormatter_t2012_InterfacesTypeInfos[] = 
{
	&IFormatter_t2014_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingFormatter_t2012_0_0_0;
extern const Il2CppType IRemotingFormatter_t2012_1_0_0;
struct IRemotingFormatter_t2012;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t2012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t2012_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingFormatter_t2012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, IRemotingFormatter_t2012_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IRemotingFormatter_t2012_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 531/* custom_attributes_cache */
	, &IRemotingFormatter_t2012_0_0_0/* byval_arg */
	, &IRemotingFormatter_t2012_1_0_0/* this_arg */
	, &IRemotingFormatter_t2012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t2013_il2cpp_TypeInfo;
static const MethodInfo* ISerializationRootObject_t2013_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationRootObject_t2013_1_0_0;
struct ISerializationRootObject_t2013;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t2013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISerializationRootObject_t2013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ISerializationRootObject_t2013_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISerializationRootObject_t2013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t2013_0_0_0/* byval_arg */
	, &ISerializationRootObject_t2013_1_0_0/* this_arg */
	, &ISerializationRootObject_t2013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t1650_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor()
extern const MethodInfo LogicalCallContext__ctor_m8589_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m8589/* method */
	, &LogicalCallContext_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo LogicalCallContext_t1650_LogicalCallContext__ctor_m8590_ParameterInfos[] = 
{
	{"info", 0, 134221966, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221967, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext__ctor_m8590_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LogicalCallContext__ctor_m8590/* method */
	, &LogicalCallContext_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, LogicalCallContext_t1650_LogicalCallContext__ctor_m8590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo LogicalCallContext_t1650_LogicalCallContext_GetObjectData_m8591_ParameterInfos[] = 
{
	{"info", 0, 134221968, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221969, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo LogicalCallContext_GetObjectData_m8591_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LogicalCallContext_GetObjectData_m8591/* method */
	, &LogicalCallContext_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, LogicalCallContext_t1650_LogicalCallContext_GetObjectData_m8591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LogicalCallContext_t1650_LogicalCallContext_SetData_m8592_ParameterInfos[] = 
{
	{"name", 0, 134221970, 0, &String_t_0_0_0},
	{"data", 1, 134221971, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.LogicalCallContext::SetData(System.String,System.Object)
extern const MethodInfo LogicalCallContext_SetData_m8592_MethodInfo = 
{
	"SetData"/* name */
	, (methodPointerType)&LogicalCallContext_SetData_m8592/* method */
	, &LogicalCallContext_t1650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, LogicalCallContext_t1650_LogicalCallContext_SetData_m8592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LogicalCallContext_t1650_MethodInfos[] =
{
	&LogicalCallContext__ctor_m8589_MethodInfo,
	&LogicalCallContext__ctor_m8590_MethodInfo,
	&LogicalCallContext_GetObjectData_m8591_MethodInfo,
	&LogicalCallContext_SetData_m8592_MethodInfo,
	NULL
};
extern const MethodInfo LogicalCallContext_GetObjectData_m8591_MethodInfo;
static const Il2CppMethodReference LogicalCallContext_t1650_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&LogicalCallContext_GetObjectData_m8591_MethodInfo,
};
static bool LogicalCallContext_t1650_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
static const Il2CppType* LogicalCallContext_t1650_InterfacesTypeInfos[] = 
{
	&ICloneable_t427_0_0_0,
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t1650_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LogicalCallContext_t1650_1_0_0;
struct LogicalCallContext_t1650;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t1650_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t1650_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t1650_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t1650_VTable/* vtableMethods */
	, LogicalCallContext_t1650_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1550/* fieldStart */

};
TypeInfo LogicalCallContext_t1650_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, LogicalCallContext_t1650_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LogicalCallContext_t1650_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 532/* custom_attributes_cache */
	, &LogicalCallContext_t1650_0_0_0/* byval_arg */
	, &LogicalCallContext_t1650_1_0_0/* this_arg */
	, &LogicalCallContext_t1650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t1650)/* instance_size */
	, sizeof (LogicalCallContext_t1650)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t1649_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern const MethodInfo CallContextRemotingData__ctor_m8593_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CallContextRemotingData__ctor_m8593/* method */
	, &CallContextRemotingData_t1649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CallContextRemotingData_t1649_MethodInfos[] =
{
	&CallContextRemotingData__ctor_m8593_MethodInfo,
	NULL
};
static const Il2CppMethodReference CallContextRemotingData_t1649_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CallContextRemotingData_t1649_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static const Il2CppType* CallContextRemotingData_t1649_InterfacesTypeInfos[] = 
{
	&ICloneable_t427_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t1649_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallContextRemotingData_t1649_0_0_0;
extern const Il2CppType CallContextRemotingData_t1649_1_0_0;
struct CallContextRemotingData_t1649;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t1649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t1649_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t1649_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t1649_VTable/* vtableMethods */
	, CallContextRemotingData_t1649_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CallContextRemotingData_t1649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, CallContextRemotingData_t1649_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CallContextRemotingData_t1649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t1649_0_0_0/* byval_arg */
	, &CallContextRemotingData_t1649_1_0_0/* this_arg */
	, &CallContextRemotingData_t1649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t1649)/* instance_size */
	, sizeof (CallContextRemotingData_t1649)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t1644_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
extern const Il2CppType HeaderU5BU5D_t1886_0_0_0;
extern const Il2CppType HeaderU5BU5D_t1886_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall__ctor_m8594_ParameterInfos[] = 
{
	{"h1", 0, 134221972, 0, &HeaderU5BU5D_t1886_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo MethodCall__ctor_m8594_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m8594/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall__ctor_m8594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall__ctor_m8595_ParameterInfos[] = 
{
	{"info", 0, 134221973, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221974, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall__ctor_m8595_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m8595/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MethodCall_t1644_MethodCall__ctor_m8595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::.ctor()
extern const MethodInfo MethodCall__ctor_m8596_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCall__ctor_m8596/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_ParameterInfos[] = 
{
	{"value", 0, 134221975, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_InitMethodProperty_m8598_ParameterInfos[] = 
{
	{"key", 0, 134221976, 0, &String_t_0_0_0},
	{"value", 1, 134221977, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitMethodProperty(System.String,System.Object)
extern const MethodInfo MethodCall_InitMethodProperty_m8598_MethodInfo = 
{
	"InitMethodProperty"/* name */
	, (methodPointerType)&MethodCall_InitMethodProperty_m8598/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall_InitMethodProperty_m8598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_GetObjectData_m8599_ParameterInfos[] = 
{
	{"info", 0, 134221978, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134221979, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodCall_GetObjectData_m8599_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MethodCall_GetObjectData_m8599/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, MethodCall_t1644_MethodCall_GetObjectData_m8599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MethodCall::get_Args()
extern const MethodInfo MethodCall_get_Args_m8600_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MethodCall_get_Args_m8600/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodCall::get_LogicalCallContext()
extern const MethodInfo MethodCall_get_LogicalCallContext_m8601_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MethodCall_get_LogicalCallContext_m8601/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1650_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodCall::get_MethodBase()
extern const MethodInfo MethodCall_get_MethodBase_m8602_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MethodCall_get_MethodBase_m8602/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_MethodName()
extern const MethodInfo MethodCall_get_MethodName_m8603_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MethodCall_get_MethodName_m8603/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodCall::get_MethodSignature()
extern const MethodInfo MethodCall_get_MethodSignature_m8604_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MethodCall_get_MethodSignature_m8604/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCall::get_Properties()
extern const MethodInfo MethodCall_get_Properties_m8605_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&MethodCall_get_Properties_m8605/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::InitDictionary()
extern const MethodInfo MethodCall_InitDictionary_m8606_MethodInfo = 
{
	"InitDictionary"/* name */
	, (methodPointerType)&MethodCall_InitDictionary_m8606/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_TypeName()
extern const MethodInfo MethodCall_get_TypeName_m8607_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MethodCall_get_TypeName_m8607/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::get_Uri()
extern const MethodInfo MethodCall_get_Uri_m8608_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MethodCall_get_Uri_m8608/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_set_Uri_m8609_ParameterInfos[] = 
{
	{"value", 0, 134221980, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::set_Uri(System.String)
extern const MethodInfo MethodCall_set_Uri_m8609_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MethodCall_set_Uri_m8609/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall_set_Uri_m8609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::Init()
extern const MethodInfo MethodCall_Init_m8610_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&MethodCall_Init_m8610/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCall::ResolveMethod()
extern const MethodInfo MethodCall_ResolveMethod_m8611_MethodInfo = 
{
	"ResolveMethod"/* name */
	, (methodPointerType)&MethodCall_ResolveMethod_m8611/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_CastTo_m8612_ParameterInfos[] = 
{
	{"clientType", 0, 134221981, 0, &String_t_0_0_0},
	{"serverType", 1, 134221982, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Messaging.MethodCall::CastTo(System.String,System.Type)
extern const MethodInfo MethodCall_CastTo_m8612_MethodInfo = 
{
	"CastTo"/* name */
	, (methodPointerType)&MethodCall_CastTo_m8612/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall_CastTo_m8612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodCall_t1644_MethodCall_GetTypeNameFromAssemblyQualifiedName_m8613_ParameterInfos[] = 
{
	{"aqname", 0, 134221983, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MethodCall::GetTypeNameFromAssemblyQualifiedName(System.String)
extern const MethodInfo MethodCall_GetTypeNameFromAssemblyQualifiedName_m8613_MethodInfo = 
{
	"GetTypeNameFromAssemblyQualifiedName"/* name */
	, (methodPointerType)&MethodCall_GetTypeNameFromAssemblyQualifiedName_m8613/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodCall_t1644_MethodCall_GetTypeNameFromAssemblyQualifiedName_m8613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.Runtime.Remoting.Messaging.MethodCall::get_GenericArguments()
extern const MethodInfo MethodCall_get_GenericArguments_m8614_MethodInfo = 
{
	"get_GenericArguments"/* name */
	, (methodPointerType)&MethodCall_get_GenericArguments_m8614/* method */
	, &MethodCall_t1644_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t628_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCall_t1644_MethodInfos[] =
{
	&MethodCall__ctor_m8594_MethodInfo,
	&MethodCall__ctor_m8595_MethodInfo,
	&MethodCall__ctor_m8596_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo,
	&MethodCall_InitMethodProperty_m8598_MethodInfo,
	&MethodCall_GetObjectData_m8599_MethodInfo,
	&MethodCall_get_Args_m8600_MethodInfo,
	&MethodCall_get_LogicalCallContext_m8601_MethodInfo,
	&MethodCall_get_MethodBase_m8602_MethodInfo,
	&MethodCall_get_MethodName_m8603_MethodInfo,
	&MethodCall_get_MethodSignature_m8604_MethodInfo,
	&MethodCall_get_Properties_m8605_MethodInfo,
	&MethodCall_InitDictionary_m8606_MethodInfo,
	&MethodCall_get_TypeName_m8607_MethodInfo,
	&MethodCall_get_Uri_m8608_MethodInfo,
	&MethodCall_set_Uri_m8609_MethodInfo,
	&MethodCall_Init_m8610_MethodInfo,
	&MethodCall_ResolveMethod_m8611_MethodInfo,
	&MethodCall_CastTo_m8612_MethodInfo,
	&MethodCall_GetTypeNameFromAssemblyQualifiedName_m8613_MethodInfo,
	&MethodCall_get_GenericArguments_m8614_MethodInfo,
	NULL
};
static const PropertyInfo MethodCall_t1644____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____Args_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MethodCall_get_Args_m8600_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____LogicalCallContext_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MethodCall_get_LogicalCallContext_m8601_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____MethodBase_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MethodCall_get_MethodBase_m8602_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____MethodName_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MethodCall_get_MethodName_m8603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____MethodSignature_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MethodCall_get_MethodSignature_m8604_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_Properties_m8605_MethodInfo;
static const PropertyInfo MethodCall_t1644____Properties_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &MethodCall_get_Properties_m8605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____TypeName_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MethodCall_get_TypeName_m8607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodCall_t1644____Uri_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MethodCall_get_Uri_m8608_MethodInfo/* get */
	, &MethodCall_set_Uri_m8609_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MethodCall_get_GenericArguments_m8614_MethodInfo;
static const PropertyInfo MethodCall_t1644____GenericArguments_PropertyInfo = 
{
	&MethodCall_t1644_il2cpp_TypeInfo/* parent */
	, "GenericArguments"/* name */
	, &MethodCall_get_GenericArguments_m8614_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodCall_t1644_PropertyInfos[] =
{
	&MethodCall_t1644____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&MethodCall_t1644____Args_PropertyInfo,
	&MethodCall_t1644____LogicalCallContext_PropertyInfo,
	&MethodCall_t1644____MethodBase_PropertyInfo,
	&MethodCall_t1644____MethodName_PropertyInfo,
	&MethodCall_t1644____MethodSignature_PropertyInfo,
	&MethodCall_t1644____Properties_PropertyInfo,
	&MethodCall_t1644____TypeName_PropertyInfo,
	&MethodCall_t1644____Uri_PropertyInfo,
	&MethodCall_t1644____GenericArguments_PropertyInfo,
	NULL
};
extern const MethodInfo MethodCall_GetObjectData_m8599_MethodInfo;
extern const MethodInfo MethodCall_InitMethodProperty_m8598_MethodInfo;
extern const MethodInfo MethodCall_InitDictionary_m8606_MethodInfo;
static const Il2CppMethodReference MethodCall_t1644_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MethodCall_GetObjectData_m8599_MethodInfo,
	&MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8597_MethodInfo,
	&MethodCall_get_Args_m8600_MethodInfo,
	&MethodCall_get_LogicalCallContext_m8601_MethodInfo,
	&MethodCall_get_MethodBase_m8602_MethodInfo,
	&MethodCall_get_MethodName_m8603_MethodInfo,
	&MethodCall_get_MethodSignature_m8604_MethodInfo,
	&MethodCall_get_TypeName_m8607_MethodInfo,
	&MethodCall_get_Uri_m8608_MethodInfo,
	&MethodCall_InitMethodProperty_m8598_MethodInfo,
	&MethodCall_GetObjectData_m8599_MethodInfo,
	&MethodCall_get_Properties_m8605_MethodInfo,
	&MethodCall_InitDictionary_m8606_MethodInfo,
	&MethodCall_set_Uri_m8609_MethodInfo,
	&MethodCall_Init_m8610_MethodInfo,
};
static bool MethodCall_t1644_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodCall_t1644_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&IInternalMessage_t1934_0_0_0,
	&IMessage_t1641_0_0_0,
	&IMethodCallMessage_t1922_0_0_0,
	&IMethodMessage_t1653_0_0_0,
	&ISerializationRootObject_t2013_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t1644_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IInternalMessage_t1934_0_0_0, 5},
	{ &IMessage_t1641_0_0_0, 6},
	{ &IMethodCallMessage_t1922_0_0_0, 6},
	{ &IMethodMessage_t1653_0_0_0, 6},
	{ &ISerializationRootObject_t2013_0_0_0, 13},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCall_t1644_1_0_0;
struct MethodCall_t1644;
const Il2CppTypeDefinitionMetadata MethodCall_t1644_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t1644_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t1644_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t1644_VTable/* vtableMethods */
	, MethodCall_t1644_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1552/* fieldStart */

};
TypeInfo MethodCall_t1644_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCall_t1644_MethodInfos/* methods */
	, MethodCall_t1644_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodCall_t1644_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 533/* custom_attributes_cache */
	, &MethodCall_t1644_0_0_0/* byval_arg */
	, &MethodCall_t1644_1_0_0/* this_arg */
	, &MethodCall_t1644_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t1644)/* instance_size */
	, sizeof (MethodCall_t1644)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t1644_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t1651_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t1653_0_0_0;
static const ParameterInfo MethodCallDictionary_t1651_MethodCallDictionary__ctor_m8615_ParameterInfos[] = 
{
	{"message", 0, 134221984, 0, &IMethodMessage_t1653_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodCallDictionary__ctor_m8615_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodCallDictionary__ctor_m8615/* method */
	, &MethodCallDictionary_t1651_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodCallDictionary_t1651_MethodCallDictionary__ctor_m8615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
extern const MethodInfo MethodCallDictionary__cctor_m8616_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodCallDictionary__cctor_m8616/* method */
	, &MethodCallDictionary_t1651_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodCallDictionary_t1651_MethodInfos[] =
{
	&MethodCallDictionary__ctor_m8615_MethodInfo,
	&MethodCallDictionary__cctor_m8616_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_GetMethodProperty_m8631_MethodInfo;
extern const MethodInfo MethodDictionary_SetMethodProperty_m8632_MethodInfo;
static const Il2CppMethodReference MethodCallDictionary_t1651_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo,
	&MethodDictionary_get_Count_m8637_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m8638_MethodInfo,
	&MethodDictionary_get_SyncRoot_m8639_MethodInfo,
	&MethodDictionary_CopyTo_m8640_MethodInfo,
	&MethodDictionary_get_Item_m8629_MethodInfo,
	&MethodDictionary_set_Item_m8630_MethodInfo,
	&MethodDictionary_Add_m8634_MethodInfo,
	&MethodDictionary_Contains_m8635_MethodInfo,
	&MethodDictionary_GetEnumerator_m8641_MethodInfo,
	&MethodDictionary_Remove_m8636_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m8626_MethodInfo,
	&MethodDictionary_GetMethodProperty_m8631_MethodInfo,
	&MethodDictionary_SetMethodProperty_m8632_MethodInfo,
	&MethodDictionary_get_Values_m8633_MethodInfo,
};
static bool MethodCallDictionary_t1651_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t1651_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICollection_t860_0_0_0, 5},
	{ &IDictionary_t1081_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCallDictionary_t1651_0_0_0;
extern const Il2CppType MethodCallDictionary_t1651_1_0_0;
struct MethodCallDictionary_t1651;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t1651_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t1651_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1646_0_0_0/* parent */
	, MethodCallDictionary_t1651_VTable/* vtableMethods */
	, MethodCallDictionary_t1651_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1563/* fieldStart */

};
TypeInfo MethodCallDictionary_t1651_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodCallDictionary_t1651_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodCallDictionary_t1651_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t1651_0_0_0/* byval_arg */
	, &MethodCallDictionary_t1651_1_0_0/* this_arg */
	, &MethodCallDictionary_t1651_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t1651)/* instance_size */
	, sizeof (MethodCallDictionary_t1651)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t1651_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t1652_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
extern const Il2CppType MethodDictionary_t1646_0_0_0;
static const ParameterInfo DictionaryEnumerator_t1652_DictionaryEnumerator__ctor_m8617_ParameterInfos[] = 
{
	{"methodDictionary", 0, 134222000, 0, &MethodDictionary_t1646_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::.ctor(System.Runtime.Remoting.Messaging.MethodDictionary)
extern const MethodInfo DictionaryEnumerator__ctor_m8617_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DictionaryEnumerator__ctor_m8617/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, DictionaryEnumerator_t1652_DictionaryEnumerator__ctor_m8617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Current()
extern const MethodInfo DictionaryEnumerator_get_Current_m8618_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Current_m8618/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::MoveNext()
extern const MethodInfo DictionaryEnumerator_MoveNext_m8619_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&DictionaryEnumerator_MoveNext_m8619/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DictionaryEntry_t1147_0_0_0;
extern void* RuntimeInvoker_DictionaryEntry_t1147 (const MethodInfo* method, void* obj, void** args);
// System.Collections.DictionaryEntry System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Entry()
extern const MethodInfo DictionaryEnumerator_get_Entry_m8620_MethodInfo = 
{
	"get_Entry"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Entry_m8620/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &DictionaryEntry_t1147_0_0_0/* return_type */
	, RuntimeInvoker_DictionaryEntry_t1147/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Key()
extern const MethodInfo DictionaryEnumerator_get_Key_m8621_MethodInfo = 
{
	"get_Key"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Key_m8621/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator::get_Value()
extern const MethodInfo DictionaryEnumerator_get_Value_m8622_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DictionaryEnumerator_get_Value_m8622/* method */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DictionaryEnumerator_t1652_MethodInfos[] =
{
	&DictionaryEnumerator__ctor_m8617_MethodInfo,
	&DictionaryEnumerator_get_Current_m8618_MethodInfo,
	&DictionaryEnumerator_MoveNext_m8619_MethodInfo,
	&DictionaryEnumerator_get_Entry_m8620_MethodInfo,
	&DictionaryEnumerator_get_Key_m8621_MethodInfo,
	&DictionaryEnumerator_get_Value_m8622_MethodInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_get_Current_m8618_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1652____Current_PropertyInfo = 
{
	&DictionaryEnumerator_t1652_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &DictionaryEnumerator_get_Current_m8618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Entry_m8620_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1652____Entry_PropertyInfo = 
{
	&DictionaryEnumerator_t1652_il2cpp_TypeInfo/* parent */
	, "Entry"/* name */
	, &DictionaryEnumerator_get_Entry_m8620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Key_m8621_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1652____Key_PropertyInfo = 
{
	&DictionaryEnumerator_t1652_il2cpp_TypeInfo/* parent */
	, "Key"/* name */
	, &DictionaryEnumerator_get_Key_m8621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo DictionaryEnumerator_get_Value_m8622_MethodInfo;
static const PropertyInfo DictionaryEnumerator_t1652____Value_PropertyInfo = 
{
	&DictionaryEnumerator_t1652_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DictionaryEnumerator_get_Value_m8622_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DictionaryEnumerator_t1652_PropertyInfos[] =
{
	&DictionaryEnumerator_t1652____Current_PropertyInfo,
	&DictionaryEnumerator_t1652____Entry_PropertyInfo,
	&DictionaryEnumerator_t1652____Key_PropertyInfo,
	&DictionaryEnumerator_t1652____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DictionaryEnumerator_MoveNext_m8619_MethodInfo;
static const Il2CppMethodReference DictionaryEnumerator_t1652_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&DictionaryEnumerator_get_Current_m8618_MethodInfo,
	&DictionaryEnumerator_MoveNext_m8619_MethodInfo,
	&DictionaryEnumerator_get_Entry_m8620_MethodInfo,
	&DictionaryEnumerator_get_Key_m8621_MethodInfo,
	&DictionaryEnumerator_get_Value_m8622_MethodInfo,
};
static bool DictionaryEnumerator_t1652_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t28_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t1146_0_0_0;
static const Il2CppType* DictionaryEnumerator_t1652_InterfacesTypeInfos[] = 
{
	&IEnumerator_t28_0_0_0,
	&IDictionaryEnumerator_t1146_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t1652_InterfacesOffsets[] = 
{
	{ &IEnumerator_t28_0_0_0, 4},
	{ &IDictionaryEnumerator_t1146_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEnumerator_t1652_0_0_0;
extern const Il2CppType DictionaryEnumerator_t1652_1_0_0;
extern TypeInfo MethodDictionary_t1646_il2cpp_TypeInfo;
struct DictionaryEnumerator_t1652;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t1652_DefinitionMetadata = 
{
	&MethodDictionary_t1646_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t1652_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t1652_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t1652_VTable/* vtableMethods */
	, DictionaryEnumerator_t1652_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1564/* fieldStart */

};
TypeInfo DictionaryEnumerator_t1652_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, DictionaryEnumerator_t1652_MethodInfos/* methods */
	, DictionaryEnumerator_t1652_PropertyInfos/* properties */
	, NULL/* events */
	, &DictionaryEnumerator_t1652_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t1652_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t1652_1_0_0/* this_arg */
	, &DictionaryEnumerator_t1652_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t1652)/* instance_size */
	, sizeof (DictionaryEnumerator_t1652)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern const Il2CppType IMethodMessage_t1653_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary__ctor_m8623_ParameterInfos[] = 
{
	{"message", 0, 134221985, 0, &IMethodMessage_t1653_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo MethodDictionary__ctor_m8623_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodDictionary__ctor_m8623/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary__ctor_m8623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern const Il2CppType StringU5BU5D_t45_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_set_MethodKeys_m8625_ParameterInfos[] = 
{
	{"value", 0, 134221986, 0, &StringU5BU5D_t45_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_MethodKeys(System.String[])
extern const MethodInfo MethodDictionary_set_MethodKeys_m8625_MethodInfo = 
{
	"set_MethodKeys"/* name */
	, (methodPointerType)&MethodDictionary_set_MethodKeys_m8625/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_set_MethodKeys_m8625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::AllocInternalProperties()
extern const MethodInfo MethodDictionary_AllocInternalProperties_m8626_MethodInfo = 
{
	"AllocInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_AllocInternalProperties_m8626/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodDictionary::GetInternalProperties()
extern const MethodInfo MethodDictionary_GetInternalProperties_m8627_MethodInfo = 
{
	"GetInternalProperties"/* name */
	, (methodPointerType)&MethodDictionary_GetInternalProperties_m8627/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_IsOverridenKey_m8628_ParameterInfos[] = 
{
	{"key", 0, 134221987, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::IsOverridenKey(System.String)
extern const MethodInfo MethodDictionary_IsOverridenKey_m8628_MethodInfo = 
{
	"IsOverridenKey"/* name */
	, (methodPointerType)&MethodDictionary_IsOverridenKey_m8628/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_IsOverridenKey_m8628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_get_Item_m8629_ParameterInfos[] = 
{
	{"key", 0, 134221988, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_Item(System.Object)
extern const MethodInfo MethodDictionary_get_Item_m8629_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&MethodDictionary_get_Item_m8629/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_get_Item_m8629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_set_Item_m8630_ParameterInfos[] = 
{
	{"key", 0, 134221989, 0, &Object_t_0_0_0},
	{"value", 1, 134221990, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::set_Item(System.Object,System.Object)
extern const MethodInfo MethodDictionary_set_Item_m8630_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&MethodDictionary_set_Item_m8630/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_set_Item_m8630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_GetMethodProperty_m8631_ParameterInfos[] = 
{
	{"key", 0, 134221991, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::GetMethodProperty(System.String)
extern const MethodInfo MethodDictionary_GetMethodProperty_m8631_MethodInfo = 
{
	"GetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_GetMethodProperty_m8631/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_GetMethodProperty_m8631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_SetMethodProperty_m8632_ParameterInfos[] = 
{
	{"key", 0, 134221992, 0, &String_t_0_0_0},
	{"value", 1, 134221993, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::SetMethodProperty(System.String,System.Object)
extern const MethodInfo MethodDictionary_SetMethodProperty_m8632_MethodInfo = 
{
	"SetMethodProperty"/* name */
	, (methodPointerType)&MethodDictionary_SetMethodProperty_m8632/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_SetMethodProperty_m8632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.ICollection System.Runtime.Remoting.Messaging.MethodDictionary::get_Values()
extern const MethodInfo MethodDictionary_get_Values_m8633_MethodInfo = 
{
	"get_Values"/* name */
	, (methodPointerType)&MethodDictionary_get_Values_m8633/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_t860_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_Add_m8634_ParameterInfos[] = 
{
	{"key", 0, 134221994, 0, &Object_t_0_0_0},
	{"value", 1, 134221995, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Add(System.Object,System.Object)
extern const MethodInfo MethodDictionary_Add_m8634_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&MethodDictionary_Add_m8634/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_Add_m8634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_Contains_m8635_ParameterInfos[] = 
{
	{"key", 0, 134221996, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::Contains(System.Object)
extern const MethodInfo MethodDictionary_Contains_m8635_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&MethodDictionary_Contains_m8635/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_Contains_m8635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_Remove_m8636_ParameterInfos[] = 
{
	{"key", 0, 134221997, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::Remove(System.Object)
extern const MethodInfo MethodDictionary_Remove_m8636_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&MethodDictionary_Remove_m8636/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_Remove_m8636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MethodDictionary::get_Count()
extern const MethodInfo MethodDictionary_get_Count_m8637_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&MethodDictionary_get_Count_m8637/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Messaging.MethodDictionary::get_IsSynchronized()
extern const MethodInfo MethodDictionary_get_IsSynchronized_m8638_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&MethodDictionary_get_IsSynchronized_m8638/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MethodDictionary::get_SyncRoot()
extern const MethodInfo MethodDictionary_get_SyncRoot_m8639_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&MethodDictionary_get_SyncRoot_m8639/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo MethodDictionary_t1646_MethodDictionary_CopyTo_m8640_ParameterInfos[] = 
{
	{"array", 0, 134221998, 0, &Array_t_0_0_0},
	{"index", 1, 134221999, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodDictionary::CopyTo(System.Array,System.Int32)
extern const MethodInfo MethodDictionary_CopyTo_m8640_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&MethodDictionary_CopyTo_m8640/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, MethodDictionary_t1646_MethodDictionary_CopyTo_m8640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Messaging.MethodDictionary::GetEnumerator()
extern const MethodInfo MethodDictionary_GetEnumerator_m8641_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&MethodDictionary_GetEnumerator_m8641/* method */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* declaring_type */
	, &IDictionaryEnumerator_t1146_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodDictionary_t1646_MethodInfos[] =
{
	&MethodDictionary__ctor_m8623_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo,
	&MethodDictionary_set_MethodKeys_m8625_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m8626_MethodInfo,
	&MethodDictionary_GetInternalProperties_m8627_MethodInfo,
	&MethodDictionary_IsOverridenKey_m8628_MethodInfo,
	&MethodDictionary_get_Item_m8629_MethodInfo,
	&MethodDictionary_set_Item_m8630_MethodInfo,
	&MethodDictionary_GetMethodProperty_m8631_MethodInfo,
	&MethodDictionary_SetMethodProperty_m8632_MethodInfo,
	&MethodDictionary_get_Values_m8633_MethodInfo,
	&MethodDictionary_Add_m8634_MethodInfo,
	&MethodDictionary_Contains_m8635_MethodInfo,
	&MethodDictionary_Remove_m8636_MethodInfo,
	&MethodDictionary_get_Count_m8637_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m8638_MethodInfo,
	&MethodDictionary_get_SyncRoot_m8639_MethodInfo,
	&MethodDictionary_CopyTo_m8640_MethodInfo,
	&MethodDictionary_GetEnumerator_m8641_MethodInfo,
	NULL
};
extern const MethodInfo MethodDictionary_set_MethodKeys_m8625_MethodInfo;
static const PropertyInfo MethodDictionary_t1646____MethodKeys_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "MethodKeys"/* name */
	, NULL/* get */
	, &MethodDictionary_set_MethodKeys_m8625_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1646____Item_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &MethodDictionary_get_Item_m8629_MethodInfo/* get */
	, &MethodDictionary_set_Item_m8630_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1646____Values_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &MethodDictionary_get_Values_m8633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1646____Count_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &MethodDictionary_get_Count_m8637_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1646____IsSynchronized_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &MethodDictionary_get_IsSynchronized_m8638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo MethodDictionary_t1646____SyncRoot_PropertyInfo = 
{
	&MethodDictionary_t1646_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &MethodDictionary_get_SyncRoot_m8639_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MethodDictionary_t1646_PropertyInfos[] =
{
	&MethodDictionary_t1646____MethodKeys_PropertyInfo,
	&MethodDictionary_t1646____Item_PropertyInfo,
	&MethodDictionary_t1646____Values_PropertyInfo,
	&MethodDictionary_t1646____Count_PropertyInfo,
	&MethodDictionary_t1646____IsSynchronized_PropertyInfo,
	&MethodDictionary_t1646____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* MethodDictionary_t1646_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t1652_0_0_0,
};
static const Il2CppMethodReference MethodDictionary_t1646_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo,
	&MethodDictionary_get_Count_m8637_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m8638_MethodInfo,
	&MethodDictionary_get_SyncRoot_m8639_MethodInfo,
	&MethodDictionary_CopyTo_m8640_MethodInfo,
	&MethodDictionary_get_Item_m8629_MethodInfo,
	&MethodDictionary_set_Item_m8630_MethodInfo,
	&MethodDictionary_Add_m8634_MethodInfo,
	&MethodDictionary_Contains_m8635_MethodInfo,
	&MethodDictionary_GetEnumerator_m8641_MethodInfo,
	&MethodDictionary_Remove_m8636_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m8626_MethodInfo,
	&MethodDictionary_GetMethodProperty_m8631_MethodInfo,
	&MethodDictionary_SetMethodProperty_m8632_MethodInfo,
	&MethodDictionary_get_Values_m8633_MethodInfo,
};
static bool MethodDictionary_t1646_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MethodDictionary_t1646_InterfacesTypeInfos[] = 
{
	&IEnumerable_t464_0_0_0,
	&ICollection_t860_0_0_0,
	&IDictionary_t1081_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t1646_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICollection_t860_0_0_0, 5},
	{ &IDictionary_t1081_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodDictionary_t1646_1_0_0;
struct MethodDictionary_t1646;
const Il2CppTypeDefinitionMetadata MethodDictionary_t1646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t1646_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t1646_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t1646_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t1646_VTable/* vtableMethods */
	, MethodDictionary_t1646_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1567/* fieldStart */

};
TypeInfo MethodDictionary_t1646_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodDictionary_t1646_MethodInfos/* methods */
	, MethodDictionary_t1646_PropertyInfos/* properties */
	, NULL/* events */
	, &MethodDictionary_t1646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 535/* custom_attributes_cache */
	, &MethodDictionary_t1646_0_0_0/* byval_arg */
	, &MethodDictionary_t1646_1_0_0/* this_arg */
	, &MethodDictionary_t1646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t1646)/* instance_size */
	, sizeof (MethodDictionary_t1646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t1646_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t1654_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
extern const Il2CppType IMethodReturnMessage_t1921_0_0_0;
static const ParameterInfo MethodReturnDictionary_t1654_MethodReturnDictionary__ctor_m8642_ParameterInfos[] = 
{
	{"message", 0, 134222001, 0, &IMethodReturnMessage_t1921_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern const MethodInfo MethodReturnDictionary__ctor_m8642_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__ctor_m8642/* method */
	, &MethodReturnDictionary_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MethodReturnDictionary_t1654_MethodReturnDictionary__ctor_m8642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern const MethodInfo MethodReturnDictionary__cctor_m8643_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MethodReturnDictionary__cctor_m8643/* method */
	, &MethodReturnDictionary_t1654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodReturnDictionary_t1654_MethodInfos[] =
{
	&MethodReturnDictionary__ctor_m8642_MethodInfo,
	&MethodReturnDictionary__cctor_m8643_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodReturnDictionary_t1654_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8624_MethodInfo,
	&MethodDictionary_get_Count_m8637_MethodInfo,
	&MethodDictionary_get_IsSynchronized_m8638_MethodInfo,
	&MethodDictionary_get_SyncRoot_m8639_MethodInfo,
	&MethodDictionary_CopyTo_m8640_MethodInfo,
	&MethodDictionary_get_Item_m8629_MethodInfo,
	&MethodDictionary_set_Item_m8630_MethodInfo,
	&MethodDictionary_Add_m8634_MethodInfo,
	&MethodDictionary_Contains_m8635_MethodInfo,
	&MethodDictionary_GetEnumerator_m8641_MethodInfo,
	&MethodDictionary_Remove_m8636_MethodInfo,
	&MethodDictionary_AllocInternalProperties_m8626_MethodInfo,
	&MethodDictionary_GetMethodProperty_m8631_MethodInfo,
	&MethodDictionary_SetMethodProperty_m8632_MethodInfo,
	&MethodDictionary_get_Values_m8633_MethodInfo,
};
static bool MethodReturnDictionary_t1654_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t1654_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICollection_t860_0_0_0, 5},
	{ &IDictionary_t1081_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodReturnDictionary_t1654_0_0_0;
extern const Il2CppType MethodReturnDictionary_t1654_1_0_0;
struct MethodReturnDictionary_t1654;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t1654_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t1654_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1646_0_0_0/* parent */
	, MethodReturnDictionary_t1654_VTable/* vtableMethods */
	, MethodReturnDictionary_t1654_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1573/* fieldStart */

};
TypeInfo MethodReturnDictionary_t1654_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MethodReturnDictionary_t1654_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodReturnDictionary_t1654_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t1654_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t1654_1_0_0/* this_arg */
	, &MethodReturnDictionary_t1654_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t1654)/* instance_size */
	, sizeof (MethodReturnDictionary_t1654)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t1654_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t1639_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Args()
extern const MethodInfo MonoMethodMessage_get_Args_m8644_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Args_m8644/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::get_LogicalCallContext()
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m8645_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&MonoMethodMessage_get_LogicalCallContext_m8645/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1650_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodBase()
extern const MethodInfo MonoMethodMessage_get_MethodBase_m8646_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodBase_m8646/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodName()
extern const MethodInfo MonoMethodMessage_get_MethodName_m8647_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodName_m8647/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_MethodSignature()
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m8648_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&MonoMethodMessage_get_MethodSignature_m8648/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_TypeName()
extern const MethodInfo MonoMethodMessage_get_TypeName_m8649_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&MonoMethodMessage_get_TypeName_m8649/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Uri()
extern const MethodInfo MonoMethodMessage_get_Uri_m8650_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Uri_m8650/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MonoMethodMessage_t1639_MonoMethodMessage_set_Uri_m8651_ParameterInfos[] = 
{
	{"value", 0, 134222002, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.MonoMethodMessage::set_Uri(System.String)
extern const MethodInfo MonoMethodMessage_set_Uri_m8651_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&MonoMethodMessage_set_Uri_m8651/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MonoMethodMessage_t1639_MonoMethodMessage_set_Uri_m8651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::get_Exception()
extern const MethodInfo MonoMethodMessage_get_Exception_m8652_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&MonoMethodMessage_get_Exception_m8652/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgCount()
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m8653_MethodInfo = 
{
	"get_OutArgCount"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgCount_m8653/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::get_OutArgs()
extern const MethodInfo MonoMethodMessage_get_OutArgs_m8654_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&MonoMethodMessage_get_OutArgs_m8654/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::get_ReturnValue()
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m8655_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&MonoMethodMessage_get_ReturnValue_m8655/* method */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoMethodMessage_t1639_MethodInfos[] =
{
	&MonoMethodMessage_get_Args_m8644_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m8645_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m8646_MethodInfo,
	&MonoMethodMessage_get_MethodName_m8647_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m8648_MethodInfo,
	&MonoMethodMessage_get_TypeName_m8649_MethodInfo,
	&MonoMethodMessage_get_Uri_m8650_MethodInfo,
	&MonoMethodMessage_set_Uri_m8651_MethodInfo,
	&MonoMethodMessage_get_Exception_m8652_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m8653_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m8654_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m8655_MethodInfo,
	NULL
};
extern const MethodInfo MonoMethodMessage_get_Args_m8644_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____Args_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &MonoMethodMessage_get_Args_m8644_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_LogicalCallContext_m8645_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____LogicalCallContext_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &MonoMethodMessage_get_LogicalCallContext_m8645_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodBase_m8646_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____MethodBase_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &MonoMethodMessage_get_MethodBase_m8646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodName_m8647_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____MethodName_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &MonoMethodMessage_get_MethodName_m8647_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_MethodSignature_m8648_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____MethodSignature_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &MonoMethodMessage_get_MethodSignature_m8648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_TypeName_m8649_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____TypeName_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &MonoMethodMessage_get_TypeName_m8649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Uri_m8650_MethodInfo;
extern const MethodInfo MonoMethodMessage_set_Uri_m8651_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____Uri_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &MonoMethodMessage_get_Uri_m8650_MethodInfo/* get */
	, &MonoMethodMessage_set_Uri_m8651_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_Exception_m8652_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____Exception_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &MonoMethodMessage_get_Exception_m8652_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgCount_m8653_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____OutArgCount_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "OutArgCount"/* name */
	, &MonoMethodMessage_get_OutArgCount_m8653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_OutArgs_m8654_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____OutArgs_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &MonoMethodMessage_get_OutArgs_m8654_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoMethodMessage_get_ReturnValue_m8655_MethodInfo;
static const PropertyInfo MonoMethodMessage_t1639____ReturnValue_PropertyInfo = 
{
	&MonoMethodMessage_t1639_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &MonoMethodMessage_get_ReturnValue_m8655_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoMethodMessage_t1639_PropertyInfos[] =
{
	&MonoMethodMessage_t1639____Args_PropertyInfo,
	&MonoMethodMessage_t1639____LogicalCallContext_PropertyInfo,
	&MonoMethodMessage_t1639____MethodBase_PropertyInfo,
	&MonoMethodMessage_t1639____MethodName_PropertyInfo,
	&MonoMethodMessage_t1639____MethodSignature_PropertyInfo,
	&MonoMethodMessage_t1639____TypeName_PropertyInfo,
	&MonoMethodMessage_t1639____Uri_PropertyInfo,
	&MonoMethodMessage_t1639____Exception_PropertyInfo,
	&MonoMethodMessage_t1639____OutArgCount_PropertyInfo,
	&MonoMethodMessage_t1639____OutArgs_PropertyInfo,
	&MonoMethodMessage_t1639____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MonoMethodMessage_t1639_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MonoMethodMessage_set_Uri_m8651_MethodInfo,
	&MonoMethodMessage_get_Args_m8644_MethodInfo,
	&MonoMethodMessage_get_LogicalCallContext_m8645_MethodInfo,
	&MonoMethodMessage_get_MethodBase_m8646_MethodInfo,
	&MonoMethodMessage_get_MethodName_m8647_MethodInfo,
	&MonoMethodMessage_get_MethodSignature_m8648_MethodInfo,
	&MonoMethodMessage_get_TypeName_m8649_MethodInfo,
	&MonoMethodMessage_get_Uri_m8650_MethodInfo,
	&MonoMethodMessage_get_Exception_m8652_MethodInfo,
	&MonoMethodMessage_get_OutArgs_m8654_MethodInfo,
	&MonoMethodMessage_get_ReturnValue_m8655_MethodInfo,
	&MonoMethodMessage_get_OutArgCount_m8653_MethodInfo,
};
static bool MonoMethodMessage_t1639_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoMethodMessage_t1639_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t1934_0_0_0,
	&IMessage_t1641_0_0_0,
	&IMethodCallMessage_t1922_0_0_0,
	&IMethodMessage_t1653_0_0_0,
	&IMethodReturnMessage_t1921_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t1639_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t1934_0_0_0, 4},
	{ &IMessage_t1641_0_0_0, 5},
	{ &IMethodCallMessage_t1922_0_0_0, 5},
	{ &IMethodMessage_t1653_0_0_0, 5},
	{ &IMethodReturnMessage_t1921_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodMessage_t1639_1_0_0;
struct MonoMethodMessage_t1639;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t1639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t1639_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t1639_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t1639_VTable/* vtableMethods */
	, MonoMethodMessage_t1639_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1575/* fieldStart */

};
TypeInfo MonoMethodMessage_t1639_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, MonoMethodMessage_t1639_MethodInfos/* methods */
	, MonoMethodMessage_t1639_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoMethodMessage_t1639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t1639_0_0_0/* byval_arg */
	, &MonoMethodMessage_t1639_1_0_0/* this_arg */
	, &MonoMethodMessage_t1639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t1639)/* instance_size */
	, sizeof (MonoMethodMessage_t1639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 11/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t1655_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogate::.ctor()
extern const MethodInfo RemotingSurrogate__ctor_m8656_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogate__ctor_m8656/* method */
	, &RemotingSurrogate_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType ISurrogateSelector_t1657_0_0_0;
extern const Il2CppType ISurrogateSelector_t1657_0_0_0;
static const ParameterInfo RemotingSurrogate_t1655_RemotingSurrogate_SetObjectData_m8657_ParameterInfos[] = 
{
	{"obj", 0, 134222003, 0, &Object_t_0_0_0},
	{"si", 1, 134222004, 0, &SerializationInfo_t725_0_0_0},
	{"sc", 2, 134222005, 0, &StreamingContext_t726_0_0_0},
	{"selector", 3, 134222006, 0, &ISurrogateSelector_t1657_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t726_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.RemotingSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo RemotingSurrogate_SetObjectData_m8657_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&RemotingSurrogate_SetObjectData_m8657/* method */
	, &RemotingSurrogate_t1655_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t726_Object_t/* invoker_method */
	, RemotingSurrogate_t1655_RemotingSurrogate_SetObjectData_m8657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogate_t1655_MethodInfos[] =
{
	&RemotingSurrogate__ctor_m8656_MethodInfo,
	&RemotingSurrogate_SetObjectData_m8657_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogate_SetObjectData_m8657_MethodInfo;
static const Il2CppMethodReference RemotingSurrogate_t1655_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&RemotingSurrogate_SetObjectData_m8657_MethodInfo,
	&RemotingSurrogate_SetObjectData_m8657_MethodInfo,
};
static bool RemotingSurrogate_t1655_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationSurrogate_t1708_0_0_0;
static const Il2CppType* RemotingSurrogate_t1655_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1708_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t1655_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1708_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogate_t1655_0_0_0;
extern const Il2CppType RemotingSurrogate_t1655_1_0_0;
struct RemotingSurrogate_t1655;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t1655_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t1655_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t1655_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t1655_VTable/* vtableMethods */
	, RemotingSurrogate_t1655_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingSurrogate_t1655_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogate_t1655_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogate_t1655_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t1655_0_0_0/* byval_arg */
	, &RemotingSurrogate_t1655_1_0_0/* this_arg */
	, &RemotingSurrogate_t1655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t1655)/* instance_size */
	, sizeof (RemotingSurrogate_t1655)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t1656_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::.ctor()
extern const MethodInfo ObjRefSurrogate__ctor_m8658_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRefSurrogate__ctor_m8658/* method */
	, &ObjRefSurrogate_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType ISurrogateSelector_t1657_0_0_0;
static const ParameterInfo ObjRefSurrogate_t1656_ObjRefSurrogate_SetObjectData_m8659_ParameterInfos[] = 
{
	{"obj", 0, 134222007, 0, &Object_t_0_0_0},
	{"si", 1, 134222008, 0, &SerializationInfo_t725_0_0_0},
	{"sc", 2, 134222009, 0, &StreamingContext_t726_0_0_0},
	{"selector", 3, 134222010, 0, &ISurrogateSelector_t1657_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t726_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ObjRefSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
extern const MethodInfo ObjRefSurrogate_SetObjectData_m8659_MethodInfo = 
{
	"SetObjectData"/* name */
	, (methodPointerType)&ObjRefSurrogate_SetObjectData_m8659/* method */
	, &ObjRefSurrogate_t1656_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t726_Object_t/* invoker_method */
	, ObjRefSurrogate_t1656_ObjRefSurrogate_SetObjectData_m8659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRefSurrogate_t1656_MethodInfos[] =
{
	&ObjRefSurrogate__ctor_m8658_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m8659_MethodInfo,
	NULL
};
extern const MethodInfo ObjRefSurrogate_SetObjectData_m8659_MethodInfo;
static const Il2CppMethodReference ObjRefSurrogate_t1656_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m8659_MethodInfo,
	&ObjRefSurrogate_SetObjectData_m8659_MethodInfo,
};
static bool ObjRefSurrogate_t1656_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ObjRefSurrogate_t1656_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1708_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t1656_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1708_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRefSurrogate_t1656_0_0_0;
extern const Il2CppType ObjRefSurrogate_t1656_1_0_0;
struct ObjRefSurrogate_t1656;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t1656_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t1656_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t1656_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t1656_VTable/* vtableMethods */
	, ObjRefSurrogate_t1656_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ObjRefSurrogate_t1656_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ObjRefSurrogate_t1656_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ObjRefSurrogate_t1656_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t1656_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t1656_1_0_0/* this_arg */
	, &ObjRefSurrogate_t1656_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t1656)/* instance_size */
	, sizeof (ObjRefSurrogate_t1656)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t1658_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.ctor()
extern const MethodInfo RemotingSurrogateSelector__ctor_m8660_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__ctor_m8660/* method */
	, &RemotingSurrogateSelector_t1658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::.cctor()
extern const MethodInfo RemotingSurrogateSelector__cctor_m8661_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingSurrogateSelector__cctor_m8661/* method */
	, &RemotingSurrogateSelector_t1658_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType ISurrogateSelector_t1657_1_0_2;
extern const Il2CppType ISurrogateSelector_t1657_1_0_0;
static const ParameterInfo RemotingSurrogateSelector_t1658_RemotingSurrogateSelector_GetSurrogate_m8662_ParameterInfos[] = 
{
	{"type", 0, 134222011, 0, &Type_t_0_0_0},
	{"context", 1, 134222012, 0, &StreamingContext_t726_0_0_0},
	{"ssout", 2, 134222013, 0, &ISurrogateSelector_t1657_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t726_ISurrogateSelectorU26_t2384 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISerializationSurrogate System.Runtime.Remoting.Messaging.RemotingSurrogateSelector::GetSurrogate(System.Type,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector&)
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m8662_MethodInfo = 
{
	"GetSurrogate"/* name */
	, (methodPointerType)&RemotingSurrogateSelector_GetSurrogate_m8662/* method */
	, &RemotingSurrogateSelector_t1658_il2cpp_TypeInfo/* declaring_type */
	, &ISerializationSurrogate_t1708_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_StreamingContext_t726_ISurrogateSelectorU26_t2384/* invoker_method */
	, RemotingSurrogateSelector_t1658_RemotingSurrogateSelector_GetSurrogate_m8662_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingSurrogateSelector_t1658_MethodInfos[] =
{
	&RemotingSurrogateSelector__ctor_m8660_MethodInfo,
	&RemotingSurrogateSelector__cctor_m8661_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m8662_MethodInfo,
	NULL
};
extern const MethodInfo RemotingSurrogateSelector_GetSurrogate_m8662_MethodInfo;
static const Il2CppMethodReference RemotingSurrogateSelector_t1658_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m8662_MethodInfo,
	&RemotingSurrogateSelector_GetSurrogate_m8662_MethodInfo,
};
static bool RemotingSurrogateSelector_t1658_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RemotingSurrogateSelector_t1658_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t1657_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t1658_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t1657_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogateSelector_t1658_0_0_0;
extern const Il2CppType RemotingSurrogateSelector_t1658_1_0_0;
struct RemotingSurrogateSelector_t1658;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t1658_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t1658_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t1658_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t1658_VTable/* vtableMethods */
	, RemotingSurrogateSelector_t1658_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1583/* fieldStart */

};
TypeInfo RemotingSurrogateSelector_t1658_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, RemotingSurrogateSelector_t1658_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingSurrogateSelector_t1658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 538/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t1658_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t1658_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t1658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t1658)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t1658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t1658_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t1659_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType LogicalCallContext_t1650_0_0_0;
extern const Il2CppType IMethodCallMessage_t1922_0_0_0;
static const ParameterInfo ReturnMessage_t1659_ReturnMessage__ctor_m8663_ParameterInfos[] = 
{
	{"ret", 0, 134222014, 0, &Object_t_0_0_0},
	{"outArgs", 1, 134222015, 0, &ObjectU5BU5D_t29_0_0_0},
	{"outArgsCount", 2, 134222016, 0, &Int32_t54_0_0_0},
	{"callCtx", 3, 134222017, 0, &LogicalCallContext_t1650_0_0_0},
	{"mcm", 4, 134222018, 0, &IMethodCallMessage_t1922_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Object,System.Object[],System.Int32,System.Runtime.Remoting.Messaging.LogicalCallContext,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m8663_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m8663/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t1659_ReturnMessage__ctor_m8663_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Exception_t42_0_0_0;
extern const Il2CppType IMethodCallMessage_t1922_0_0_0;
static const ParameterInfo ReturnMessage_t1659_ReturnMessage__ctor_m8664_ParameterInfos[] = 
{
	{"e", 0, 134222019, 0, &Exception_t42_0_0_0},
	{"mcm", 1, 134222020, 0, &IMethodCallMessage_t1922_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::.ctor(System.Exception,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern const MethodInfo ReturnMessage__ctor_m8664_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReturnMessage__ctor_m8664/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ReturnMessage_t1659_ReturnMessage__ctor_m8664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t1659_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_ParameterInfos[] = 
{
	{"value", 0, 134222021, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_MethodInfo = 
{
	"System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ReturnMessage_t1659_ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_Args()
extern const MethodInfo ReturnMessage_get_Args_m8666_MethodInfo = 
{
	"get_Args"/* name */
	, (methodPointerType)&ReturnMessage_get_Args_m8666/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.ReturnMessage::get_LogicalCallContext()
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m8667_MethodInfo = 
{
	"get_LogicalCallContext"/* name */
	, (methodPointerType)&ReturnMessage_get_LogicalCallContext_m8667/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &LogicalCallContext_t1650_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodBase()
extern const MethodInfo ReturnMessage_get_MethodBase_m8668_MethodInfo = 
{
	"get_MethodBase"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodBase_m8668/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodName()
extern const MethodInfo ReturnMessage_get_MethodName_m8669_MethodInfo = 
{
	"get_MethodName"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodName_m8669/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_MethodSignature()
extern const MethodInfo ReturnMessage_get_MethodSignature_m8670_MethodInfo = 
{
	"get_MethodSignature"/* name */
	, (methodPointerType)&ReturnMessage_get_MethodSignature_m8670/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ReturnMessage::get_Properties()
extern const MethodInfo ReturnMessage_get_Properties_m8671_MethodInfo = 
{
	"get_Properties"/* name */
	, (methodPointerType)&ReturnMessage_get_Properties_m8671/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_TypeName()
extern const MethodInfo ReturnMessage_get_TypeName_m8672_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&ReturnMessage_get_TypeName_m8672/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Messaging.ReturnMessage::get_Uri()
extern const MethodInfo ReturnMessage_get_Uri_m8673_MethodInfo = 
{
	"get_Uri"/* name */
	, (methodPointerType)&ReturnMessage_get_Uri_m8673/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReturnMessage_t1659_ReturnMessage_set_Uri_m8674_ParameterInfos[] = 
{
	{"value", 0, 134222022, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.ReturnMessage::set_Uri(System.String)
extern const MethodInfo ReturnMessage_set_Uri_m8674_MethodInfo = 
{
	"set_Uri"/* name */
	, (methodPointerType)&ReturnMessage_set_Uri_m8674/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ReturnMessage_t1659_ReturnMessage_set_Uri_m8674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Exception System.Runtime.Remoting.Messaging.ReturnMessage::get_Exception()
extern const MethodInfo ReturnMessage_get_Exception_m8675_MethodInfo = 
{
	"get_Exception"/* name */
	, (methodPointerType)&ReturnMessage_get_Exception_m8675/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Exception_t42_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.Messaging.ReturnMessage::get_OutArgs()
extern const MethodInfo ReturnMessage_get_OutArgs_m8676_MethodInfo = 
{
	"get_OutArgs"/* name */
	, (methodPointerType)&ReturnMessage_get_OutArgs_m8676/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.ReturnMessage::get_ReturnValue()
extern const MethodInfo ReturnMessage_get_ReturnValue_m8677_MethodInfo = 
{
	"get_ReturnValue"/* name */
	, (methodPointerType)&ReturnMessage_get_ReturnValue_m8677/* method */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReturnMessage_t1659_MethodInfos[] =
{
	&ReturnMessage__ctor_m8663_MethodInfo,
	&ReturnMessage__ctor_m8664_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_MethodInfo,
	&ReturnMessage_get_Args_m8666_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m8667_MethodInfo,
	&ReturnMessage_get_MethodBase_m8668_MethodInfo,
	&ReturnMessage_get_MethodName_m8669_MethodInfo,
	&ReturnMessage_get_MethodSignature_m8670_MethodInfo,
	&ReturnMessage_get_Properties_m8671_MethodInfo,
	&ReturnMessage_get_TypeName_m8672_MethodInfo,
	&ReturnMessage_get_Uri_m8673_MethodInfo,
	&ReturnMessage_set_Uri_m8674_MethodInfo,
	&ReturnMessage_get_Exception_m8675_MethodInfo,
	&ReturnMessage_get_OutArgs_m8676_MethodInfo,
	&ReturnMessage_get_ReturnValue_m8677_MethodInfo,
	NULL
};
extern const MethodInfo ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "System.Runtime.Remoting.Messaging.IInternalMessage.Uri"/* name */
	, NULL/* get */
	, &ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Args_m8666_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____Args_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "Args"/* name */
	, &ReturnMessage_get_Args_m8666_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_LogicalCallContext_m8667_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____LogicalCallContext_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "LogicalCallContext"/* name */
	, &ReturnMessage_get_LogicalCallContext_m8667_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodBase_m8668_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____MethodBase_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "MethodBase"/* name */
	, &ReturnMessage_get_MethodBase_m8668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodName_m8669_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____MethodName_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "MethodName"/* name */
	, &ReturnMessage_get_MethodName_m8669_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_MethodSignature_m8670_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____MethodSignature_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "MethodSignature"/* name */
	, &ReturnMessage_get_MethodSignature_m8670_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Properties_m8671_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____Properties_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "Properties"/* name */
	, &ReturnMessage_get_Properties_m8671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_TypeName_m8672_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____TypeName_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &ReturnMessage_get_TypeName_m8672_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Uri_m8673_MethodInfo;
extern const MethodInfo ReturnMessage_set_Uri_m8674_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____Uri_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "Uri"/* name */
	, &ReturnMessage_get_Uri_m8673_MethodInfo/* get */
	, &ReturnMessage_set_Uri_m8674_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_Exception_m8675_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____Exception_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "Exception"/* name */
	, &ReturnMessage_get_Exception_m8675_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_OutArgs_m8676_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____OutArgs_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "OutArgs"/* name */
	, &ReturnMessage_get_OutArgs_m8676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ReturnMessage_get_ReturnValue_m8677_MethodInfo;
static const PropertyInfo ReturnMessage_t1659____ReturnValue_PropertyInfo = 
{
	&ReturnMessage_t1659_il2cpp_TypeInfo/* parent */
	, "ReturnValue"/* name */
	, &ReturnMessage_get_ReturnValue_m8677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReturnMessage_t1659_PropertyInfos[] =
{
	&ReturnMessage_t1659____System_Runtime_Remoting_Messaging_IInternalMessage_Uri_PropertyInfo,
	&ReturnMessage_t1659____Args_PropertyInfo,
	&ReturnMessage_t1659____LogicalCallContext_PropertyInfo,
	&ReturnMessage_t1659____MethodBase_PropertyInfo,
	&ReturnMessage_t1659____MethodName_PropertyInfo,
	&ReturnMessage_t1659____MethodSignature_PropertyInfo,
	&ReturnMessage_t1659____Properties_PropertyInfo,
	&ReturnMessage_t1659____TypeName_PropertyInfo,
	&ReturnMessage_t1659____Uri_PropertyInfo,
	&ReturnMessage_t1659____Exception_PropertyInfo,
	&ReturnMessage_t1659____OutArgs_PropertyInfo,
	&ReturnMessage_t1659____ReturnValue_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ReturnMessage_t1659_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8665_MethodInfo,
	&ReturnMessage_get_Args_m8666_MethodInfo,
	&ReturnMessage_get_LogicalCallContext_m8667_MethodInfo,
	&ReturnMessage_get_MethodBase_m8668_MethodInfo,
	&ReturnMessage_get_MethodName_m8669_MethodInfo,
	&ReturnMessage_get_MethodSignature_m8670_MethodInfo,
	&ReturnMessage_get_TypeName_m8672_MethodInfo,
	&ReturnMessage_get_Uri_m8673_MethodInfo,
	&ReturnMessage_get_Exception_m8675_MethodInfo,
	&ReturnMessage_get_OutArgs_m8676_MethodInfo,
	&ReturnMessage_get_ReturnValue_m8677_MethodInfo,
	&ReturnMessage_get_Properties_m8671_MethodInfo,
	&ReturnMessage_set_Uri_m8674_MethodInfo,
	&ReturnMessage_get_ReturnValue_m8677_MethodInfo,
};
static bool ReturnMessage_t1659_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ReturnMessage_t1659_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t1934_0_0_0,
	&IMessage_t1641_0_0_0,
	&IMethodMessage_t1653_0_0_0,
	&IMethodReturnMessage_t1921_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t1659_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t1934_0_0_0, 4},
	{ &IMessage_t1641_0_0_0, 5},
	{ &IMethodMessage_t1653_0_0_0, 5},
	{ &IMethodReturnMessage_t1921_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnMessage_t1659_0_0_0;
extern const Il2CppType ReturnMessage_t1659_1_0_0;
struct ReturnMessage_t1659;
const Il2CppTypeDefinitionMetadata ReturnMessage_t1659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t1659_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t1659_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t1659_VTable/* vtableMethods */
	, ReturnMessage_t1659_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1587/* fieldStart */

};
TypeInfo ReturnMessage_t1659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, ReturnMessage_t1659_MethodInfos/* methods */
	, ReturnMessage_t1659_PropertyInfos/* properties */
	, NULL/* events */
	, &ReturnMessage_t1659_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 539/* custom_attributes_cache */
	, &ReturnMessage_t1659_0_0_0/* byval_arg */
	, &ReturnMessage_t1659_1_0_0/* this_arg */
	, &ReturnMessage_t1659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t1659)/* instance_size */
	, sizeof (ReturnMessage_t1659)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t1660_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ProxyAttribute_t1660_ProxyAttribute_CreateInstance_m8678_ParameterInfos[] = 
{
	{"serverType", 0, 134222023, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.Proxies.ProxyAttribute::CreateInstance(System.Type)
extern const MethodInfo ProxyAttribute_CreateInstance_m8678_MethodInfo = 
{
	"CreateInstance"/* name */
	, (methodPointerType)&ProxyAttribute_CreateInstance_m8678/* method */
	, &ProxyAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1022_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1660_ProxyAttribute_CreateInstance_m8678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Context_t1632_0_0_0;
static const ParameterInfo ProxyAttribute_t1660_ProxyAttribute_CreateProxy_m8679_ParameterInfos[] = 
{
	{"objRef", 0, 134222024, 0, &ObjRef_t1669_0_0_0},
	{"serverType", 1, 134222025, 0, &Type_t_0_0_0},
	{"serverObject", 2, 134222026, 0, &Object_t_0_0_0},
	{"serverContext", 3, 134222027, 0, &Context_t1632_0_0_0},
};
extern const Il2CppType RealProxy_t1661_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.Proxies.ProxyAttribute::CreateProxy(System.Runtime.Remoting.ObjRef,System.Type,System.Object,System.Runtime.Remoting.Contexts.Context)
extern const MethodInfo ProxyAttribute_CreateProxy_m8679_MethodInfo = 
{
	"CreateProxy"/* name */
	, (methodPointerType)&ProxyAttribute_CreateProxy_m8679/* method */
	, &ProxyAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t1661_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1660_ProxyAttribute_CreateProxy_m8679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo ProxyAttribute_t1660_ProxyAttribute_GetPropertiesForNewContext_m8680_ParameterInfos[] = 
{
	{"msg", 0, 134222028, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.ProxyAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m8680_MethodInfo = 
{
	"GetPropertiesForNewContext"/* name */
	, (methodPointerType)&ProxyAttribute_GetPropertiesForNewContext_m8680/* method */
	, &ProxyAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ProxyAttribute_t1660_ProxyAttribute_GetPropertiesForNewContext_m8680_ParameterInfos/* parameters */
	, 541/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType IConstructionCallMessage_t1918_0_0_0;
static const ParameterInfo ProxyAttribute_t1660_ProxyAttribute_IsContextOK_m8681_ParameterInfos[] = 
{
	{"ctx", 0, 134222029, 0, &Context_t1632_0_0_0},
	{"msg", 1, 134222030, 0, &IConstructionCallMessage_t1918_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Proxies.ProxyAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const MethodInfo ProxyAttribute_IsContextOK_m8681_MethodInfo = 
{
	"IsContextOK"/* name */
	, (methodPointerType)&ProxyAttribute_IsContextOK_m8681/* method */
	, &ProxyAttribute_t1660_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, ProxyAttribute_t1660_ProxyAttribute_IsContextOK_m8681_ParameterInfos/* parameters */
	, 542/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ProxyAttribute_t1660_MethodInfos[] =
{
	&ProxyAttribute_CreateInstance_m8678_MethodInfo,
	&ProxyAttribute_CreateProxy_m8679_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m8680_MethodInfo,
	&ProxyAttribute_IsContextOK_m8681_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m3659_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m3530_MethodInfo;
extern const MethodInfo ProxyAttribute_GetPropertiesForNewContext_m8680_MethodInfo;
extern const MethodInfo ProxyAttribute_IsContextOK_m8681_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateInstance_m8678_MethodInfo;
extern const MethodInfo ProxyAttribute_CreateProxy_m8679_MethodInfo;
static const Il2CppMethodReference ProxyAttribute_t1660_VTable[] =
{
	&Attribute_Equals_m3659_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Attribute_GetHashCode_m3530_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ProxyAttribute_GetPropertiesForNewContext_m8680_MethodInfo,
	&ProxyAttribute_IsContextOK_m8681_MethodInfo,
	&ProxyAttribute_CreateInstance_m8678_MethodInfo,
	&ProxyAttribute_CreateProxy_m8679_MethodInfo,
};
static bool ProxyAttribute_t1660_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ProxyAttribute_t1660_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t1931_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t1660_InterfacesOffsets[] = 
{
	{ &_Attribute_t832_0_0_0, 4},
	{ &IContextAttribute_t1931_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProxyAttribute_t1660_0_0_0;
extern const Il2CppType ProxyAttribute_t1660_1_0_0;
struct ProxyAttribute_t1660;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t1660_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t1660_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t1660_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t539_0_0_0/* parent */
	, ProxyAttribute_t1660_VTable/* vtableMethods */
	, ProxyAttribute_t1660_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ProxyAttribute_t1660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, ProxyAttribute_t1660_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ProxyAttribute_t1660_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 540/* custom_attributes_cache */
	, &ProxyAttribute_t1660_0_0_0/* byval_arg */
	, &ProxyAttribute_t1660_1_0_0/* this_arg */
	, &ProxyAttribute_t1660_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t1660)/* instance_size */
	, sizeof (ProxyAttribute_t1660)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t1662_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static const MethodInfo* TransparentProxy_t1662_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TransparentProxy_t1662_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TransparentProxy_t1662_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TransparentProxy_t1662_0_0_0;
extern const Il2CppType TransparentProxy_t1662_1_0_0;
struct TransparentProxy_t1662;
const Il2CppTypeDefinitionMetadata TransparentProxy_t1662_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t1662_VTable/* vtableMethods */
	, TransparentProxy_t1662_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1600/* fieldStart */

};
TypeInfo TransparentProxy_t1662_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, TransparentProxy_t1662_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TransparentProxy_t1662_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t1662_0_0_0/* byval_arg */
	, &TransparentProxy_t1662_1_0_0/* this_arg */
	, &TransparentProxy_t1662_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t1662)/* instance_size */
	, sizeof (TransparentProxy_t1662)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t1661_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy__ctor_m8682_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222031, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type)
extern const MethodInfo RealProxy__ctor_m8682_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m8682/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RealProxy_t1661_RealProxy__ctor_m8682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t1671_0_0_0;
extern const Il2CppType ClientIdentity_t1671_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy__ctor_m8683_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222032, 0, &Type_t_0_0_0},
	{"identity", 1, 134222033, 0, &ClientIdentity_t1671_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RealProxy__ctor_m8683_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m8683/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, RealProxy_t1661_RealProxy__ctor_m8683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy__ctor_m8684_ParameterInfos[] = 
{
	{"classToProxy", 0, 134222034, 0, &Type_t_0_0_0},
	{"stub", 1, 134222035, 0, &IntPtr_t_0_0_0},
	{"stubData", 2, 134222036, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::.ctor(System.Type,System.IntPtr,System.Object)
extern const MethodInfo RealProxy__ctor_m8684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RealProxy__ctor_m8684/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t_Object_t/* invoker_method */
	, RealProxy_t1661_RealProxy__ctor_m8684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy_InternalGetProxyType_m8685_ParameterInfos[] = 
{
	{"transparentProxy", 0, 134222037, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::InternalGetProxyType(System.Object)
extern const MethodInfo RealProxy_InternalGetProxyType_m8685_MethodInfo = 
{
	"InternalGetProxyType"/* name */
	, (methodPointerType)&RealProxy_InternalGetProxyType_m8685/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t1661_RealProxy_InternalGetProxyType_m8685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.Proxies.RealProxy::GetProxiedType()
extern const MethodInfo RealProxy_GetProxiedType_m8686_MethodInfo = 
{
	"GetProxiedType"/* name */
	, (methodPointerType)&RealProxy_GetProxiedType_m8686/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy_InternalGetTransparentProxy_m8687_ParameterInfos[] = 
{
	{"className", 0, 134222038, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::InternalGetTransparentProxy(System.String)
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m8687_MethodInfo = 
{
	"InternalGetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_InternalGetTransparentProxy_m8687/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RealProxy_t1661_RealProxy_InternalGetTransparentProxy_m8687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 4096/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy()
extern const MethodInfo RealProxy_GetTransparentProxy_m8688_MethodInfo = 
{
	"GetTransparentProxy"/* name */
	, (methodPointerType)&RealProxy_GetTransparentProxy_m8688/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo RealProxy_t1661_RealProxy_SetTargetDomain_m8689_ParameterInfos[] = 
{
	{"domainId", 0, 134222039, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RealProxy::SetTargetDomain(System.Int32)
extern const MethodInfo RealProxy_SetTargetDomain_m8689_MethodInfo = 
{
	"SetTargetDomain"/* name */
	, (methodPointerType)&RealProxy_SetTargetDomain_m8689/* method */
	, &RealProxy_t1661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, RealProxy_t1661_RealProxy_SetTargetDomain_m8689_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RealProxy_t1661_MethodInfos[] =
{
	&RealProxy__ctor_m8682_MethodInfo,
	&RealProxy__ctor_m8683_MethodInfo,
	&RealProxy__ctor_m8684_MethodInfo,
	&RealProxy_InternalGetProxyType_m8685_MethodInfo,
	&RealProxy_GetProxiedType_m8686_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m8687_MethodInfo,
	&RealProxy_GetTransparentProxy_m8688_MethodInfo,
	&RealProxy_SetTargetDomain_m8689_MethodInfo,
	NULL
};
extern const MethodInfo RealProxy_InternalGetTransparentProxy_m8687_MethodInfo;
extern const MethodInfo RealProxy_GetTransparentProxy_m8688_MethodInfo;
static const Il2CppMethodReference RealProxy_t1661_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m8687_MethodInfo,
	&RealProxy_GetTransparentProxy_m8688_MethodInfo,
};
static bool RealProxy_t1661_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RealProxy_t1661_1_0_0;
struct RealProxy_t1661;
const Il2CppTypeDefinitionMetadata RealProxy_t1661_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t1661_VTable/* vtableMethods */
	, RealProxy_t1661_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1601/* fieldStart */

};
TypeInfo RealProxy_t1661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RealProxy_t1661_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RealProxy_t1661_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 543/* custom_attributes_cache */
	, &RealProxy_t1661_0_0_0/* byval_arg */
	, &RealProxy_t1661_1_0_0/* this_arg */
	, &RealProxy_t1661_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t1661)/* instance_size */
	, sizeof (RealProxy_t1661)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t1664_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ClientIdentity_t1671_0_0_0;
static const ParameterInfo RemotingProxy_t1664_RemotingProxy__ctor_m8690_ParameterInfos[] = 
{
	{"type", 0, 134222040, 0, &Type_t_0_0_0},
	{"identity", 1, 134222041, 0, &ClientIdentity_t1671_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
extern const MethodInfo RemotingProxy__ctor_m8690_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m8690/* method */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t1664_RemotingProxy__ctor_m8690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo RemotingProxy_t1664_RemotingProxy__ctor_m8691_ParameterInfos[] = 
{
	{"type", 0, 134222042, 0, &Type_t_0_0_0},
	{"activationUrl", 1, 134222043, 0, &String_t_0_0_0},
	{"activationAttributes", 2, 134222044, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
extern const MethodInfo RemotingProxy__ctor_m8691_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingProxy__ctor_m8691/* method */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingProxy_t1664_RemotingProxy__ctor_m8691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
extern const MethodInfo RemotingProxy__cctor_m8692_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingProxy__cctor_m8692/* method */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
extern const MethodInfo RemotingProxy_get_TypeName_m8693_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&RemotingProxy_get_TypeName_m8693/* method */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
extern const MethodInfo RemotingProxy_Finalize_m8694_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&RemotingProxy_Finalize_m8694/* method */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingProxy_t1664_MethodInfos[] =
{
	&RemotingProxy__ctor_m8690_MethodInfo,
	&RemotingProxy__ctor_m8691_MethodInfo,
	&RemotingProxy__cctor_m8692_MethodInfo,
	&RemotingProxy_get_TypeName_m8693_MethodInfo,
	&RemotingProxy_Finalize_m8694_MethodInfo,
	NULL
};
extern const MethodInfo RemotingProxy_get_TypeName_m8693_MethodInfo;
static const PropertyInfo RemotingProxy_t1664____TypeName_PropertyInfo = 
{
	&RemotingProxy_t1664_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &RemotingProxy_get_TypeName_m8693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingProxy_t1664_PropertyInfos[] =
{
	&RemotingProxy_t1664____TypeName_PropertyInfo,
	NULL
};
extern const MethodInfo RemotingProxy_Finalize_m8694_MethodInfo;
static const Il2CppMethodReference RemotingProxy_t1664_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&RemotingProxy_Finalize_m8694_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&RealProxy_InternalGetTransparentProxy_m8687_MethodInfo,
	&RealProxy_GetTransparentProxy_m8688_MethodInfo,
	&RemotingProxy_get_TypeName_m8693_MethodInfo,
};
static bool RemotingProxy_t1664_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IRemotingTypeInfo_t1673_0_0_0;
static const Il2CppType* RemotingProxy_t1664_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1673_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t1664_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1673_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingProxy_t1664_0_0_0;
extern const Il2CppType RemotingProxy_t1664_1_0_0;
struct RemotingProxy_t1664;
const Il2CppTypeDefinitionMetadata RemotingProxy_t1664_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t1664_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t1664_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t1661_0_0_0/* parent */
	, RemotingProxy_t1664_VTable/* vtableMethods */
	, RemotingProxy_t1664_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1606/* fieldStart */

};
TypeInfo RemotingProxy_t1664_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, RemotingProxy_t1664_MethodInfos/* methods */
	, RemotingProxy_t1664_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingProxy_t1664_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t1664_0_0_0/* byval_arg */
	, &RemotingProxy_t1664_1_0_0/* this_arg */
	, &RemotingProxy_t1664_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t1664)/* instance_size */
	, sizeof (RemotingProxy_t1664)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t1664_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t1936_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t1669_0_0_0;
static const ParameterInfo ITrackingHandler_t1936_ITrackingHandler_UnmarshaledObject_m10981_ParameterInfos[] = 
{
	{"obj", 0, 134222045, 0, &Object_t_0_0_0},
	{"or", 1, 134222046, 0, &ObjRef_t1669_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.ITrackingHandler::UnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ITrackingHandler_UnmarshaledObject_m10981_MethodInfo = 
{
	"UnmarshaledObject"/* name */
	, NULL/* method */
	, &ITrackingHandler_t1936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ITrackingHandler_t1936_ITrackingHandler_UnmarshaledObject_m10981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ITrackingHandler_t1936_MethodInfos[] =
{
	&ITrackingHandler_UnmarshaledObject_m10981_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ITrackingHandler_t1936_0_0_0;
extern const Il2CppType ITrackingHandler_t1936_1_0_0;
struct ITrackingHandler_t1936;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t1936_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ITrackingHandler_t1936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, ITrackingHandler_t1936_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ITrackingHandler_t1936_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 544/* custom_attributes_cache */
	, &ITrackingHandler_t1936_0_0_0/* byval_arg */
	, &ITrackingHandler_t1936_1_0_0/* this_arg */
	, &ITrackingHandler_t1936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t1665_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::.cctor()
extern const MethodInfo TrackingServices__cctor_m8695_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TrackingServices__cctor_m8695/* method */
	, &TrackingServices_t1665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjRef_t1669_0_0_0;
static const ParameterInfo TrackingServices_t1665_TrackingServices_NotifyUnmarshaledObject_m8696_ParameterInfos[] = 
{
	{"obj", 0, 134222047, 0, &Object_t_0_0_0},
	{"or", 1, 134222048, 0, &ObjRef_t1669_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Services.TrackingServices::NotifyUnmarshaledObject(System.Object,System.Runtime.Remoting.ObjRef)
extern const MethodInfo TrackingServices_NotifyUnmarshaledObject_m8696_MethodInfo = 
{
	"NotifyUnmarshaledObject"/* name */
	, (methodPointerType)&TrackingServices_NotifyUnmarshaledObject_m8696/* method */
	, &TrackingServices_t1665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, TrackingServices_t1665_TrackingServices_NotifyUnmarshaledObject_m8696_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackingServices_t1665_MethodInfos[] =
{
	&TrackingServices__cctor_m8695_MethodInfo,
	&TrackingServices_NotifyUnmarshaledObject_m8696_MethodInfo,
	NULL
};
static const Il2CppMethodReference TrackingServices_t1665_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TrackingServices_t1665_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TrackingServices_t1665_0_0_0;
extern const Il2CppType TrackingServices_t1665_1_0_0;
struct TrackingServices_t1665;
const Il2CppTypeDefinitionMetadata TrackingServices_t1665_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t1665_VTable/* vtableMethods */
	, TrackingServices_t1665_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1611/* fieldStart */

};
TypeInfo TrackingServices_t1665_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, TrackingServices_t1665_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackingServices_t1665_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 545/* custom_attributes_cache */
	, &TrackingServices_t1665_0_0_0/* byval_arg */
	, &TrackingServices_t1665_1_0_0/* this_arg */
	, &TrackingServices_t1665_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t1665)/* instance_size */
	, sizeof (TrackingServices_t1665)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t1665_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::get_ApplicationUrl()
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m8697_MethodInfo = 
{
	"get_ApplicationUrl"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ApplicationUrl_m8697/* method */
	, &ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IContextAttributeU5BU5D_t1923_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Contexts.IContextAttribute[] System.Runtime.Remoting.ActivatedClientTypeEntry::get_ContextAttributes()
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m8698_MethodInfo = 
{
	"get_ContextAttributes"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ContextAttributes_m8698/* method */
	, &ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* declaring_type */
	, &IContextAttributeU5BU5D_t1923_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ActivatedClientTypeEntry::get_ObjectType()
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m8699_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_get_ObjectType_m8699/* method */
	, &ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ActivatedClientTypeEntry::ToString()
extern const MethodInfo ActivatedClientTypeEntry_ToString_m8700_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&ActivatedClientTypeEntry_ToString_m8700/* method */
	, &ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ActivatedClientTypeEntry_t1666_MethodInfos[] =
{
	&ActivatedClientTypeEntry_get_ApplicationUrl_m8697_MethodInfo,
	&ActivatedClientTypeEntry_get_ContextAttributes_m8698_MethodInfo,
	&ActivatedClientTypeEntry_get_ObjectType_m8699_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m8700_MethodInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_get_ApplicationUrl_m8697_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t1666____ApplicationUrl_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* parent */
	, "ApplicationUrl"/* name */
	, &ActivatedClientTypeEntry_get_ApplicationUrl_m8697_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ContextAttributes_m8698_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t1666____ContextAttributes_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* parent */
	, "ContextAttributes"/* name */
	, &ActivatedClientTypeEntry_get_ContextAttributes_m8698_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ActivatedClientTypeEntry_get_ObjectType_m8699_MethodInfo;
static const PropertyInfo ActivatedClientTypeEntry_t1666____ObjectType_PropertyInfo = 
{
	&ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ActivatedClientTypeEntry_get_ObjectType_m8699_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ActivatedClientTypeEntry_t1666_PropertyInfos[] =
{
	&ActivatedClientTypeEntry_t1666____ApplicationUrl_PropertyInfo,
	&ActivatedClientTypeEntry_t1666____ContextAttributes_PropertyInfo,
	&ActivatedClientTypeEntry_t1666____ObjectType_PropertyInfo,
	NULL
};
extern const MethodInfo ActivatedClientTypeEntry_ToString_m8700_MethodInfo;
static const Il2CppMethodReference ActivatedClientTypeEntry_t1666_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&ActivatedClientTypeEntry_ToString_m8700_MethodInfo,
};
static bool ActivatedClientTypeEntry_t1666_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedClientTypeEntry_t1666_0_0_0;
extern const Il2CppType ActivatedClientTypeEntry_t1666_1_0_0;
extern const Il2CppType TypeEntry_t1667_0_0_0;
struct ActivatedClientTypeEntry_t1666;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t1666_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1667_0_0_0/* parent */
	, ActivatedClientTypeEntry_t1666_VTable/* vtableMethods */
	, ActivatedClientTypeEntry_t1666_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1612/* fieldStart */

};
TypeInfo ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ActivatedClientTypeEntry_t1666_MethodInfos/* methods */
	, ActivatedClientTypeEntry_t1666_PropertyInfos/* properties */
	, NULL/* events */
	, &ActivatedClientTypeEntry_t1666_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 546/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t1666_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t1666_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t1666_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t1666)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t1666)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t1668_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
extern const Il2CppType IMessageSink_t1218_0_0_0;
static const ParameterInfo EnvoyInfo_t1668_EnvoyInfo__ctor_m8701_ParameterInfos[] = 
{
	{"sinks", 0, 134222049, 0, &IMessageSink_t1218_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.EnvoyInfo::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo EnvoyInfo__ctor_m8701_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EnvoyInfo__ctor_m8701/* method */
	, &EnvoyInfo_t1668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, EnvoyInfo_t1668_EnvoyInfo__ctor_m8701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.EnvoyInfo::get_EnvoySinks()
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m8702_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, (methodPointerType)&EnvoyInfo_get_EnvoySinks_m8702/* method */
	, &EnvoyInfo_t1668_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EnvoyInfo_t1668_MethodInfos[] =
{
	&EnvoyInfo__ctor_m8701_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m8702_MethodInfo,
	NULL
};
extern const MethodInfo EnvoyInfo_get_EnvoySinks_m8702_MethodInfo;
static const PropertyInfo EnvoyInfo_t1668____EnvoySinks_PropertyInfo = 
{
	&EnvoyInfo_t1668_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &EnvoyInfo_get_EnvoySinks_m8702_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* EnvoyInfo_t1668_PropertyInfos[] =
{
	&EnvoyInfo_t1668____EnvoySinks_PropertyInfo,
	NULL
};
static const Il2CppMethodReference EnvoyInfo_t1668_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&EnvoyInfo_get_EnvoySinks_m8702_MethodInfo,
};
static bool EnvoyInfo_t1668_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnvoyInfo_t1674_0_0_0;
static const Il2CppType* EnvoyInfo_t1668_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t1674_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t1668_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t1674_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyInfo_t1668_0_0_0;
extern const Il2CppType EnvoyInfo_t1668_1_0_0;
struct EnvoyInfo_t1668;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t1668_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t1668_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t1668_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t1668_VTable/* vtableMethods */
	, EnvoyInfo_t1668_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1614/* fieldStart */

};
TypeInfo EnvoyInfo_t1668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, EnvoyInfo_t1668_MethodInfos/* methods */
	, EnvoyInfo_t1668_PropertyInfos/* properties */
	, NULL/* events */
	, &EnvoyInfo_t1668_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t1668_0_0_0/* byval_arg */
	, &EnvoyInfo_t1668_1_0_0/* this_arg */
	, &EnvoyInfo_t1668_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t1668)/* instance_size */
	, sizeof (EnvoyInfo_t1668)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t1672_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.Runtime.Remoting.IChannelInfo::get_ChannelData()
extern const MethodInfo IChannelInfo_get_ChannelData_m10982_MethodInfo = 
{
	"get_ChannelData"/* name */
	, NULL/* method */
	, &IChannelInfo_t1672_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IChannelInfo_t1672_MethodInfos[] =
{
	&IChannelInfo_get_ChannelData_m10982_MethodInfo,
	NULL
};
extern const MethodInfo IChannelInfo_get_ChannelData_m10982_MethodInfo;
static const PropertyInfo IChannelInfo_t1672____ChannelData_PropertyInfo = 
{
	&IChannelInfo_t1672_il2cpp_TypeInfo/* parent */
	, "ChannelData"/* name */
	, &IChannelInfo_get_ChannelData_m10982_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IChannelInfo_t1672_PropertyInfos[] =
{
	&IChannelInfo_t1672____ChannelData_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelInfo_t1672_1_0_0;
struct IChannelInfo_t1672;
const Il2CppTypeDefinitionMetadata IChannelInfo_t1672_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IChannelInfo_t1672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IChannelInfo_t1672_MethodInfos/* methods */
	, IChannelInfo_t1672_PropertyInfos/* properties */
	, NULL/* events */
	, &IChannelInfo_t1672_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 547/* custom_attributes_cache */
	, &IChannelInfo_t1672_0_0_0/* byval_arg */
	, &IChannelInfo_t1672_1_0_0/* this_arg */
	, &IChannelInfo_t1672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t1674_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.IEnvoyInfo::get_EnvoySinks()
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m10983_MethodInfo = 
{
	"get_EnvoySinks"/* name */
	, NULL/* method */
	, &IEnvoyInfo_t1674_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IEnvoyInfo_t1674_MethodInfos[] =
{
	&IEnvoyInfo_get_EnvoySinks_m10983_MethodInfo,
	NULL
};
extern const MethodInfo IEnvoyInfo_get_EnvoySinks_m10983_MethodInfo;
static const PropertyInfo IEnvoyInfo_t1674____EnvoySinks_PropertyInfo = 
{
	&IEnvoyInfo_t1674_il2cpp_TypeInfo/* parent */
	, "EnvoySinks"/* name */
	, &IEnvoyInfo_get_EnvoySinks_m10983_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IEnvoyInfo_t1674_PropertyInfos[] =
{
	&IEnvoyInfo_t1674____EnvoySinks_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnvoyInfo_t1674_1_0_0;
struct IEnvoyInfo_t1674;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t1674_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IEnvoyInfo_t1674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IEnvoyInfo_t1674_MethodInfos/* methods */
	, IEnvoyInfo_t1674_PropertyInfos/* properties */
	, NULL/* events */
	, &IEnvoyInfo_t1674_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 548/* custom_attributes_cache */
	, &IEnvoyInfo_t1674_0_0_0/* byval_arg */
	, &IEnvoyInfo_t1674_1_0_0/* this_arg */
	, &IEnvoyInfo_t1674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t1673_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.IRemotingTypeInfo::get_TypeName()
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m10984_MethodInfo = 
{
	"get_TypeName"/* name */
	, NULL/* method */
	, &IRemotingTypeInfo_t1673_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IRemotingTypeInfo_t1673_MethodInfos[] =
{
	&IRemotingTypeInfo_get_TypeName_m10984_MethodInfo,
	NULL
};
extern const MethodInfo IRemotingTypeInfo_get_TypeName_m10984_MethodInfo;
static const PropertyInfo IRemotingTypeInfo_t1673____TypeName_PropertyInfo = 
{
	&IRemotingTypeInfo_t1673_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &IRemotingTypeInfo_get_TypeName_m10984_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IRemotingTypeInfo_t1673_PropertyInfos[] =
{
	&IRemotingTypeInfo_t1673____TypeName_PropertyInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingTypeInfo_t1673_1_0_0;
struct IRemotingTypeInfo_t1673;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t1673_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IRemotingTypeInfo_t1673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, IRemotingTypeInfo_t1673_MethodInfos/* methods */
	, IRemotingTypeInfo_t1673_PropertyInfos/* properties */
	, NULL/* events */
	, &IRemotingTypeInfo_t1673_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 549/* custom_attributes_cache */
	, &IRemotingTypeInfo_t1673_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t1673_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t1673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t1663_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Identity_t1663_Identity__ctor_m8703_ParameterInfos[] = 
{
	{"objectUri", 0, 134222050, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::.ctor(System.String)
extern const MethodInfo Identity__ctor_m8703_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Identity__ctor_m8703/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Identity_t1663_Identity__ctor_m8703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo Identity_t1663_Identity_CreateObjRef_m10985_ParameterInfos[] = 
{
	{"requestedType", 0, 134222051, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.Identity::CreateObjRef(System.Type)
extern const MethodInfo Identity_CreateObjRef_m10985_MethodInfo = 
{
	"CreateObjRef"/* name */
	, NULL/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t1669_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Identity_t1663_Identity_CreateObjRef_m10985_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Identity::get_ChannelSink()
extern const MethodInfo Identity_get_ChannelSink_m8704_MethodInfo = 
{
	"get_ChannelSink"/* name */
	, (methodPointerType)&Identity_get_ChannelSink_m8704/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &IMessageSink_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMessageSink_t1218_0_0_0;
static const ParameterInfo Identity_t1663_Identity_set_ChannelSink_m8705_ParameterInfos[] = 
{
	{"value", 0, 134222052, 0, &IMessageSink_t1218_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_ChannelSink(System.Runtime.Remoting.Messaging.IMessageSink)
extern const MethodInfo Identity_set_ChannelSink_m8705_MethodInfo = 
{
	"set_ChannelSink"/* name */
	, (methodPointerType)&Identity_set_ChannelSink_m8705/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Identity_t1663_Identity_set_ChannelSink_m8705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.Identity::get_ObjectUri()
extern const MethodInfo Identity_get_ObjectUri_m8706_MethodInfo = 
{
	"get_ObjectUri"/* name */
	, (methodPointerType)&Identity_get_ObjectUri_m8706/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.Identity::get_Disposed()
extern const MethodInfo Identity_get_Disposed_m8707_MethodInfo = 
{
	"get_Disposed"/* name */
	, (methodPointerType)&Identity_get_Disposed_m8707/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Identity_t1663_Identity_set_Disposed_m8708_ParameterInfos[] = 
{
	{"value", 0, 134222053, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Identity::set_Disposed(System.Boolean)
extern const MethodInfo Identity_set_Disposed_m8708_MethodInfo = 
{
	"set_Disposed"/* name */
	, (methodPointerType)&Identity_set_Disposed_m8708/* method */
	, &Identity_t1663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Identity_t1663_Identity_set_Disposed_m8708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Identity_t1663_MethodInfos[] =
{
	&Identity__ctor_m8703_MethodInfo,
	&Identity_CreateObjRef_m10985_MethodInfo,
	&Identity_get_ChannelSink_m8704_MethodInfo,
	&Identity_set_ChannelSink_m8705_MethodInfo,
	&Identity_get_ObjectUri_m8706_MethodInfo,
	&Identity_get_Disposed_m8707_MethodInfo,
	&Identity_set_Disposed_m8708_MethodInfo,
	NULL
};
extern const MethodInfo Identity_get_ChannelSink_m8704_MethodInfo;
extern const MethodInfo Identity_set_ChannelSink_m8705_MethodInfo;
static const PropertyInfo Identity_t1663____ChannelSink_PropertyInfo = 
{
	&Identity_t1663_il2cpp_TypeInfo/* parent */
	, "ChannelSink"/* name */
	, &Identity_get_ChannelSink_m8704_MethodInfo/* get */
	, &Identity_set_ChannelSink_m8705_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_ObjectUri_m8706_MethodInfo;
static const PropertyInfo Identity_t1663____ObjectUri_PropertyInfo = 
{
	&Identity_t1663_il2cpp_TypeInfo/* parent */
	, "ObjectUri"/* name */
	, &Identity_get_ObjectUri_m8706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Identity_get_Disposed_m8707_MethodInfo;
extern const MethodInfo Identity_set_Disposed_m8708_MethodInfo;
static const PropertyInfo Identity_t1663____Disposed_PropertyInfo = 
{
	&Identity_t1663_il2cpp_TypeInfo/* parent */
	, "Disposed"/* name */
	, &Identity_get_Disposed_m8707_MethodInfo/* get */
	, &Identity_set_Disposed_m8708_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Identity_t1663_PropertyInfos[] =
{
	&Identity_t1663____ChannelSink_PropertyInfo,
	&Identity_t1663____ObjectUri_PropertyInfo,
	&Identity_t1663____Disposed_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Identity_t1663_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
};
static bool Identity_t1663_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Identity_t1663_0_0_0;
extern const Il2CppType Identity_t1663_1_0_0;
struct Identity_t1663;
const Il2CppTypeDefinitionMetadata Identity_t1663_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t1663_VTable/* vtableMethods */
	, Identity_t1663_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1615/* fieldStart */

};
TypeInfo Identity_t1663_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, Identity_t1663_MethodInfos/* methods */
	, Identity_t1663_PropertyInfos/* properties */
	, NULL/* events */
	, &Identity_t1663_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t1663_0_0_0/* byval_arg */
	, &Identity_t1663_1_0_0/* this_arg */
	, &Identity_t1663_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t1663)/* instance_size */
	, sizeof (Identity_t1663)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t1671_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ObjRef_t1669_0_0_0;
static const ParameterInfo ClientIdentity_t1671_ClientIdentity__ctor_m8709_ParameterInfos[] = 
{
	{"objectUri", 0, 134222054, 0, &String_t_0_0_0},
	{"objRef", 1, 134222055, 0, &ObjRef_t1669_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::.ctor(System.String,System.Runtime.Remoting.ObjRef)
extern const MethodInfo ClientIdentity__ctor_m8709_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ClientIdentity__ctor_m8709/* method */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t1671_ClientIdentity__ctor_m8709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientIdentity::get_ClientProxy()
extern const MethodInfo ClientIdentity_get_ClientProxy_m8710_MethodInfo = 
{
	"get_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_get_ClientProxy_m8710/* method */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1022_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MarshalByRefObject_t1022_0_0_0;
static const ParameterInfo ClientIdentity_t1671_ClientIdentity_set_ClientProxy_m8711_ParameterInfos[] = 
{
	{"value", 0, 134222056, 0, &MarshalByRefObject_t1022_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ClientIdentity::set_ClientProxy(System.MarshalByRefObject)
extern const MethodInfo ClientIdentity_set_ClientProxy_m8711_MethodInfo = 
{
	"set_ClientProxy"/* name */
	, (methodPointerType)&ClientIdentity_set_ClientProxy_m8711/* method */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ClientIdentity_t1671_ClientIdentity_set_ClientProxy_m8711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ClientIdentity_t1671_ClientIdentity_CreateObjRef_m8712_ParameterInfos[] = 
{
	{"requestedType", 0, 134222057, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ClientIdentity::CreateObjRef(System.Type)
extern const MethodInfo ClientIdentity_CreateObjRef_m8712_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ClientIdentity_CreateObjRef_m8712/* method */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t1669_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ClientIdentity_t1671_ClientIdentity_CreateObjRef_m8712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ClientIdentity::get_TargetUri()
extern const MethodInfo ClientIdentity_get_TargetUri_m8713_MethodInfo = 
{
	"get_TargetUri"/* name */
	, (methodPointerType)&ClientIdentity_get_TargetUri_m8713/* method */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientIdentity_t1671_MethodInfos[] =
{
	&ClientIdentity__ctor_m8709_MethodInfo,
	&ClientIdentity_get_ClientProxy_m8710_MethodInfo,
	&ClientIdentity_set_ClientProxy_m8711_MethodInfo,
	&ClientIdentity_CreateObjRef_m8712_MethodInfo,
	&ClientIdentity_get_TargetUri_m8713_MethodInfo,
	NULL
};
extern const MethodInfo ClientIdentity_get_ClientProxy_m8710_MethodInfo;
extern const MethodInfo ClientIdentity_set_ClientProxy_m8711_MethodInfo;
static const PropertyInfo ClientIdentity_t1671____ClientProxy_PropertyInfo = 
{
	&ClientIdentity_t1671_il2cpp_TypeInfo/* parent */
	, "ClientProxy"/* name */
	, &ClientIdentity_get_ClientProxy_m8710_MethodInfo/* get */
	, &ClientIdentity_set_ClientProxy_m8711_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ClientIdentity_get_TargetUri_m8713_MethodInfo;
static const PropertyInfo ClientIdentity_t1671____TargetUri_PropertyInfo = 
{
	&ClientIdentity_t1671_il2cpp_TypeInfo/* parent */
	, "TargetUri"/* name */
	, &ClientIdentity_get_TargetUri_m8713_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ClientIdentity_t1671_PropertyInfos[] =
{
	&ClientIdentity_t1671____ClientProxy_PropertyInfo,
	&ClientIdentity_t1671____TargetUri_PropertyInfo,
	NULL
};
extern const MethodInfo ClientIdentity_CreateObjRef_m8712_MethodInfo;
static const Il2CppMethodReference ClientIdentity_t1671_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ClientIdentity_CreateObjRef_m8712_MethodInfo,
};
static bool ClientIdentity_t1671_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientIdentity_t1671_1_0_0;
struct ClientIdentity_t1671;
const Il2CppTypeDefinitionMetadata ClientIdentity_t1671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1663_0_0_0/* parent */
	, ClientIdentity_t1671_VTable/* vtableMethods */
	, ClientIdentity_t1671_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1620/* fieldStart */

};
TypeInfo ClientIdentity_t1671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientIdentity_t1671_MethodInfos/* methods */
	, ClientIdentity_t1671_PropertyInfos/* properties */
	, NULL/* events */
	, &ClientIdentity_t1671_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t1671_0_0_0/* byval_arg */
	, &ClientIdentity_t1671_1_0_0/* this_arg */
	, &ClientIdentity_t1671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t1671)/* instance_size */
	, sizeof (ClientIdentity_t1671)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t1669_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor()
extern const MethodInfo ObjRef__ctor_m8714_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m8714/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef__ctor_m8715_ParameterInfos[] = 
{
	{"info", 0, 134222058, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134222059, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef__ctor_m8715_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjRef__ctor_m8715/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ObjRef_t1669_ObjRef__ctor_m8715_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::.cctor()
extern const MethodInfo ObjRef__cctor_m8716_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ObjRef__cctor_m8716/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.ObjRef::get_IsReferenceToWellKnow()
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m8717_MethodInfo = 
{
	"get_IsReferenceToWellKnow"/* name */
	, (methodPointerType)&ObjRef_get_IsReferenceToWellKnow_m8717/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IChannelInfo System.Runtime.Remoting.ObjRef::get_ChannelInfo()
extern const MethodInfo ObjRef_get_ChannelInfo_m8718_MethodInfo = 
{
	"get_ChannelInfo"/* name */
	, (methodPointerType)&ObjRef_get_ChannelInfo_m8718/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &IChannelInfo_t1672_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 552/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IEnvoyInfo System.Runtime.Remoting.ObjRef::get_EnvoyInfo()
extern const MethodInfo ObjRef_get_EnvoyInfo_m8719_MethodInfo = 
{
	"get_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_get_EnvoyInfo_m8719/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &IEnvoyInfo_t1674_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnvoyInfo_t1674_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef_set_EnvoyInfo_m8720_ParameterInfos[] = 
{
	{"value", 0, 134222060, 0, &IEnvoyInfo_t1674_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_EnvoyInfo(System.Runtime.Remoting.IEnvoyInfo)
extern const MethodInfo ObjRef_set_EnvoyInfo_m8720_MethodInfo = 
{
	"set_EnvoyInfo"/* name */
	, (methodPointerType)&ObjRef_set_EnvoyInfo_m8720/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjRef_t1669_ObjRef_set_EnvoyInfo_m8720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.IRemotingTypeInfo System.Runtime.Remoting.ObjRef::get_TypeInfo()
extern const MethodInfo ObjRef_get_TypeInfo_m8721_MethodInfo = 
{
	"get_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_get_TypeInfo_m8721/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &IRemotingTypeInfo_t1673_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IRemotingTypeInfo_t1673_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef_set_TypeInfo_m8722_ParameterInfos[] = 
{
	{"value", 0, 134222061, 0, &IRemotingTypeInfo_t1673_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_TypeInfo(System.Runtime.Remoting.IRemotingTypeInfo)
extern const MethodInfo ObjRef_set_TypeInfo_m8722_MethodInfo = 
{
	"set_TypeInfo"/* name */
	, (methodPointerType)&ObjRef_set_TypeInfo_m8722/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjRef_t1669_ObjRef_set_TypeInfo_m8722_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.ObjRef::get_URI()
extern const MethodInfo ObjRef_get_URI_m8723_MethodInfo = 
{
	"get_URI"/* name */
	, (methodPointerType)&ObjRef_get_URI_m8723/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef_set_URI_m8724_ParameterInfos[] = 
{
	{"value", 0, 134222062, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::set_URI(System.String)
extern const MethodInfo ObjRef_set_URI_m8724_MethodInfo = 
{
	"set_URI"/* name */
	, (methodPointerType)&ObjRef_set_URI_m8724/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjRef_t1669_ObjRef_set_URI_m8724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef_GetObjectData_m8725_ParameterInfos[] = 
{
	{"info", 0, 134222063, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134222064, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetObjectData_m8725_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjRef_GetObjectData_m8725/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, ObjRef_t1669_ObjRef_GetObjectData_m8725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo ObjRef_t1669_ObjRef_GetRealObject_m8726_ParameterInfos[] = 
{
	{"context", 0, 134222065, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.ObjRef::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjRef_GetRealObject_m8726_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&ObjRef_GetRealObject_m8726/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t726/* invoker_method */
	, ObjRef_t1669_ObjRef_GetRealObject_m8726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ObjRef::UpdateChannelInfo()
extern const MethodInfo ObjRef_UpdateChannelInfo_m8727_MethodInfo = 
{
	"UpdateChannelInfo"/* name */
	, (methodPointerType)&ObjRef_UpdateChannelInfo_m8727/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ObjRef::get_ServerType()
extern const MethodInfo ObjRef_get_ServerType_m8728_MethodInfo = 
{
	"get_ServerType"/* name */
	, (methodPointerType)&ObjRef_get_ServerType_m8728/* method */
	, &ObjRef_t1669_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjRef_t1669_MethodInfos[] =
{
	&ObjRef__ctor_m8714_MethodInfo,
	&ObjRef__ctor_m8715_MethodInfo,
	&ObjRef__cctor_m8716_MethodInfo,
	&ObjRef_get_IsReferenceToWellKnow_m8717_MethodInfo,
	&ObjRef_get_ChannelInfo_m8718_MethodInfo,
	&ObjRef_get_EnvoyInfo_m8719_MethodInfo,
	&ObjRef_set_EnvoyInfo_m8720_MethodInfo,
	&ObjRef_get_TypeInfo_m8721_MethodInfo,
	&ObjRef_set_TypeInfo_m8722_MethodInfo,
	&ObjRef_get_URI_m8723_MethodInfo,
	&ObjRef_set_URI_m8724_MethodInfo,
	&ObjRef_GetObjectData_m8725_MethodInfo,
	&ObjRef_GetRealObject_m8726_MethodInfo,
	&ObjRef_UpdateChannelInfo_m8727_MethodInfo,
	&ObjRef_get_ServerType_m8728_MethodInfo,
	NULL
};
extern const MethodInfo ObjRef_get_IsReferenceToWellKnow_m8717_MethodInfo;
static const PropertyInfo ObjRef_t1669____IsReferenceToWellKnow_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "IsReferenceToWellKnow"/* name */
	, &ObjRef_get_IsReferenceToWellKnow_m8717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ChannelInfo_m8718_MethodInfo;
static const PropertyInfo ObjRef_t1669____ChannelInfo_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "ChannelInfo"/* name */
	, &ObjRef_get_ChannelInfo_m8718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_EnvoyInfo_m8719_MethodInfo;
extern const MethodInfo ObjRef_set_EnvoyInfo_m8720_MethodInfo;
static const PropertyInfo ObjRef_t1669____EnvoyInfo_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "EnvoyInfo"/* name */
	, &ObjRef_get_EnvoyInfo_m8719_MethodInfo/* get */
	, &ObjRef_set_EnvoyInfo_m8720_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_TypeInfo_m8721_MethodInfo;
extern const MethodInfo ObjRef_set_TypeInfo_m8722_MethodInfo;
static const PropertyInfo ObjRef_t1669____TypeInfo_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "TypeInfo"/* name */
	, &ObjRef_get_TypeInfo_m8721_MethodInfo/* get */
	, &ObjRef_set_TypeInfo_m8722_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_URI_m8723_MethodInfo;
extern const MethodInfo ObjRef_set_URI_m8724_MethodInfo;
static const PropertyInfo ObjRef_t1669____URI_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "URI"/* name */
	, &ObjRef_get_URI_m8723_MethodInfo/* get */
	, &ObjRef_set_URI_m8724_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjRef_get_ServerType_m8728_MethodInfo;
static const PropertyInfo ObjRef_t1669____ServerType_PropertyInfo = 
{
	&ObjRef_t1669_il2cpp_TypeInfo/* parent */
	, "ServerType"/* name */
	, &ObjRef_get_ServerType_m8728_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjRef_t1669_PropertyInfos[] =
{
	&ObjRef_t1669____IsReferenceToWellKnow_PropertyInfo,
	&ObjRef_t1669____ChannelInfo_PropertyInfo,
	&ObjRef_t1669____EnvoyInfo_PropertyInfo,
	&ObjRef_t1669____TypeInfo_PropertyInfo,
	&ObjRef_t1669____URI_PropertyInfo,
	&ObjRef_t1669____ServerType_PropertyInfo,
	NULL
};
extern const MethodInfo ObjRef_GetObjectData_m8725_MethodInfo;
extern const MethodInfo ObjRef_GetRealObject_m8726_MethodInfo;
static const Il2CppMethodReference ObjRef_t1669_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ObjRef_GetObjectData_m8725_MethodInfo,
	&ObjRef_GetRealObject_m8726_MethodInfo,
	&ObjRef_get_ChannelInfo_m8718_MethodInfo,
	&ObjRef_get_EnvoyInfo_m8719_MethodInfo,
	&ObjRef_set_EnvoyInfo_m8720_MethodInfo,
	&ObjRef_get_TypeInfo_m8721_MethodInfo,
	&ObjRef_set_TypeInfo_m8722_MethodInfo,
	&ObjRef_get_URI_m8723_MethodInfo,
	&ObjRef_set_URI_m8724_MethodInfo,
	&ObjRef_GetObjectData_m8725_MethodInfo,
	&ObjRef_GetRealObject_m8726_MethodInfo,
};
static bool ObjRef_t1669_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t1941_0_0_0;
static const Il2CppType* ObjRef_t1669_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
	&IObjectReference_t1941_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t1669_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &IObjectReference_t1941_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRef_t1669_1_0_0;
struct ObjRef_t1669;
const Il2CppTypeDefinitionMetadata ObjRef_t1669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t1669_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t1669_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t1669_VTable/* vtableMethods */
	, ObjRef_t1669_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1621/* fieldStart */

};
TypeInfo ObjRef_t1669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ObjRef_t1669_MethodInfos/* methods */
	, ObjRef_t1669_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjRef_t1669_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 550/* custom_attributes_cache */
	, &ObjRef_t1669_0_0_0/* byval_arg */
	, &ObjRef_t1669_1_0_0/* this_arg */
	, &ObjRef_t1669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t1669)/* instance_size */
	, sizeof (ObjRef_t1669)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t1669_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t1675_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingConfiguration::.cctor()
extern const MethodInfo RemotingConfiguration__cctor_m8729_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingConfiguration__cctor_m8729/* method */
	, &RemotingConfiguration_t1675_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationName()
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m8730_MethodInfo = 
{
	"get_ApplicationName"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ApplicationName_m8730/* method */
	, &RemotingConfiguration_t1675_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ProcessId()
extern const MethodInfo RemotingConfiguration_get_ProcessId_m8731_MethodInfo = 
{
	"get_ProcessId"/* name */
	, (methodPointerType)&RemotingConfiguration_get_ProcessId_m8731/* method */
	, &RemotingConfiguration_t1675_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingConfiguration_t1675_RemotingConfiguration_IsRemotelyActivatedClientType_m8732_ParameterInfos[] = 
{
	{"svrType", 0, 134222066, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.Type)
extern const MethodInfo RemotingConfiguration_IsRemotelyActivatedClientType_m8732_MethodInfo = 
{
	"IsRemotelyActivatedClientType"/* name */
	, (methodPointerType)&RemotingConfiguration_IsRemotelyActivatedClientType_m8732/* method */
	, &RemotingConfiguration_t1675_il2cpp_TypeInfo/* declaring_type */
	, &ActivatedClientTypeEntry_t1666_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingConfiguration_t1675_RemotingConfiguration_IsRemotelyActivatedClientType_m8732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingConfiguration_t1675_MethodInfos[] =
{
	&RemotingConfiguration__cctor_m8729_MethodInfo,
	&RemotingConfiguration_get_ApplicationName_m8730_MethodInfo,
	&RemotingConfiguration_get_ProcessId_m8731_MethodInfo,
	&RemotingConfiguration_IsRemotelyActivatedClientType_m8732_MethodInfo,
	NULL
};
extern const MethodInfo RemotingConfiguration_get_ApplicationName_m8730_MethodInfo;
static const PropertyInfo RemotingConfiguration_t1675____ApplicationName_PropertyInfo = 
{
	&RemotingConfiguration_t1675_il2cpp_TypeInfo/* parent */
	, "ApplicationName"/* name */
	, &RemotingConfiguration_get_ApplicationName_m8730_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RemotingConfiguration_get_ProcessId_m8731_MethodInfo;
static const PropertyInfo RemotingConfiguration_t1675____ProcessId_PropertyInfo = 
{
	&RemotingConfiguration_t1675_il2cpp_TypeInfo/* parent */
	, "ProcessId"/* name */
	, &RemotingConfiguration_get_ProcessId_m8731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RemotingConfiguration_t1675_PropertyInfos[] =
{
	&RemotingConfiguration_t1675____ApplicationName_PropertyInfo,
	&RemotingConfiguration_t1675____ProcessId_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RemotingConfiguration_t1675_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RemotingConfiguration_t1675_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingConfiguration_t1675_0_0_0;
extern const Il2CppType RemotingConfiguration_t1675_1_0_0;
struct RemotingConfiguration_t1675;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t1675_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t1675_VTable/* vtableMethods */
	, RemotingConfiguration_t1675_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1630/* fieldStart */

};
TypeInfo RemotingConfiguration_t1675_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingConfiguration_t1675_MethodInfos/* methods */
	, RemotingConfiguration_t1675_PropertyInfos/* properties */
	, NULL/* events */
	, &RemotingConfiguration_t1675_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 553/* custom_attributes_cache */
	, &RemotingConfiguration_t1675_0_0_0/* byval_arg */
	, &RemotingConfiguration_t1675_1_0_0/* this_arg */
	, &RemotingConfiguration_t1675_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t1675)/* instance_size */
	, sizeof (RemotingConfiguration_t1675)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t1675_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t1676_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor()
extern const MethodInfo RemotingException__ctor_m8733_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m8733/* method */
	, &RemotingException_t1676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingException_t1676_RemotingException__ctor_m8734_ParameterInfos[] = 
{
	{"message", 0, 134222067, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.String)
extern const MethodInfo RemotingException__ctor_m8734_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m8734/* method */
	, &RemotingException_t1676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RemotingException_t1676_RemotingException__ctor_m8734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo RemotingException_t1676_RemotingException__ctor_m8735_ParameterInfos[] = 
{
	{"info", 0, 134222068, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134222069, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RemotingException__ctor_m8735_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemotingException__ctor_m8735/* method */
	, &RemotingException_t1676_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, RemotingException_t1676_RemotingException__ctor_m8735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingException_t1676_MethodInfos[] =
{
	&RemotingException__ctor_m8733_MethodInfo,
	&RemotingException__ctor_m8734_MethodInfo,
	&RemotingException__ctor_m8735_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m3652_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m3653_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m3654_MethodInfo;
extern const MethodInfo Exception_get_Message_m3655_MethodInfo;
extern const MethodInfo Exception_get_Source_m3656_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m3657_MethodInfo;
extern const MethodInfo Exception_GetType_m3658_MethodInfo;
static const Il2CppMethodReference RemotingException_t1676_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool RemotingException_t1676_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t824_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t1676_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingException_t1676_0_0_0;
extern const Il2CppType RemotingException_t1676_1_0_0;
extern const Il2CppType SystemException_t1181_0_0_0;
struct RemotingException_t1676;
const Il2CppTypeDefinitionMetadata RemotingException_t1676_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t1676_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1181_0_0_0/* parent */
	, RemotingException_t1676_VTable/* vtableMethods */
	, RemotingException_t1676_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemotingException_t1676_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingException_t1676_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingException_t1676_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 554/* custom_attributes_cache */
	, &RemotingException_t1676_0_0_0/* byval_arg */
	, &RemotingException_t1676_1_0_0/* this_arg */
	, &RemotingException_t1676_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t1676)/* instance_size */
	, sizeof (RemotingException_t1676)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t1678_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::.cctor()
extern const MethodInfo RemotingServices__cctor_m8736_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RemotingServices__cctor_m8736/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType MethodBase_t47_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetVirtualMethod_m8737_ParameterInfos[] = 
{
	{"type", 0, 134222070, 0, &Type_t_0_0_0},
	{"method", 1, 134222071, 0, &MethodBase_t47_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)
extern const MethodInfo RemotingServices_GetVirtualMethod_m8737_MethodInfo = 
{
	"GetVirtualMethod"/* name */
	, (methodPointerType)&RemotingServices_GetVirtualMethod_m8737/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetVirtualMethod_m8737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_IsTransparentProxy_m8738_ParameterInfos[] = 
{
	{"proxy", 0, 134222072, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)
extern const MethodInfo RemotingServices_IsTransparentProxy_m8738_MethodInfo = 
{
	"IsTransparentProxy"/* name */
	, (methodPointerType)&RemotingServices_IsTransparentProxy_m8738/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_IsTransparentProxy_m8738_ParameterInfos/* parameters */
	, 556/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetServerTypeForUri_m8739_ParameterInfos[] = 
{
	{"URI", 0, 134222073, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.RemotingServices::GetServerTypeForUri(System.String)
extern const MethodInfo RemotingServices_GetServerTypeForUri_m8739_MethodInfo = 
{
	"GetServerTypeForUri"/* name */
	, (methodPointerType)&RemotingServices_GetServerTypeForUri_m8739/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetServerTypeForUri_m8739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_Unmarshal_m8740_ParameterInfos[] = 
{
	{"objectRef", 0, 134222074, 0, &ObjRef_t1669_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef)
extern const MethodInfo RemotingServices_Unmarshal_m8740_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m8740/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_Unmarshal_m8740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_Unmarshal_m8741_ParameterInfos[] = 
{
	{"objectRef", 0, 134222075, 0, &ObjRef_t1669_0_0_0},
	{"fRefine", 1, 134222076, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef,System.Boolean)
extern const MethodInfo RemotingServices_Unmarshal_m8741_MethodInfo = 
{
	"Unmarshal"/* name */
	, (methodPointerType)&RemotingServices_Unmarshal_m8741/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, RemotingServices_t1678_RemotingServices_Unmarshal_m8741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetRealProxy_m8742_ParameterInfos[] = 
{
	{"proxy", 0, 134222077, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.RemotingServices::GetRealProxy(System.Object)
extern const MethodInfo RemotingServices_GetRealProxy_m8742_MethodInfo = 
{
	"GetRealProxy"/* name */
	, (methodPointerType)&RemotingServices_GetRealProxy_m8742/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &RealProxy_t1661_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetRealProxy_m8742_ParameterInfos/* parameters */
	, 557/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IMethodMessage_t1653_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetMethodBaseFromMethodMessage_m8743_ParameterInfos[] = 
{
	{"msg", 0, 134222078, 0, &IMethodMessage_t1653_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
extern const MethodInfo RemotingServices_GetMethodBaseFromMethodMessage_m8743_MethodInfo = 
{
	"GetMethodBaseFromMethodMessage"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromMethodMessage_m8743/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetMethodBaseFromMethodMessage_m8743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetMethodBaseFromName_m8744_ParameterInfos[] = 
{
	{"type", 0, 134222079, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222080, 0, &String_t_0_0_0},
	{"signature", 2, 134222081, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromName(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_GetMethodBaseFromName_m8744_MethodInfo = 
{
	"GetMethodBaseFromName"/* name */
	, (methodPointerType)&RemotingServices_GetMethodBaseFromName_m8744/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetMethodBaseFromName_m8744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t628_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_FindInterfaceMethod_m8745_ParameterInfos[] = 
{
	{"type", 0, 134222082, 0, &Type_t_0_0_0},
	{"methodName", 1, 134222083, 0, &String_t_0_0_0},
	{"signature", 2, 134222084, 0, &TypeU5BU5D_t628_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::FindInterfaceMethod(System.Type,System.String,System.Type[])
extern const MethodInfo RemotingServices_FindInterfaceMethod_m8745_MethodInfo = 
{
	"FindInterfaceMethod"/* name */
	, (methodPointerType)&RemotingServices_FindInterfaceMethod_m8745/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_FindInterfaceMethod_m8745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ActivatedClientTypeEntry_t1666_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_CreateClientProxy_m8746_ParameterInfos[] = 
{
	{"entry", 0, 134222085, 0, &ActivatedClientTypeEntry_t1666_0_0_0},
	{"activationAttributes", 1, 134222086, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.ActivatedClientTypeEntry,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxy_m8746_MethodInfo = 
{
	"CreateClientProxy"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxy_m8746/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_CreateClientProxy_m8746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t29_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_CreateClientProxyForContextBound_m8747_ParameterInfos[] = 
{
	{"type", 0, 134222087, 0, &Type_t_0_0_0},
	{"activationAttributes", 1, 134222088, 0, &ObjectU5BU5D_t29_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxyForContextBound(System.Type,System.Object[])
extern const MethodInfo RemotingServices_CreateClientProxyForContextBound_m8747_MethodInfo = 
{
	"CreateClientProxyForContextBound"/* name */
	, (methodPointerType)&RemotingServices_CreateClientProxyForContextBound_m8747/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_CreateClientProxyForContextBound_m8747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetIdentityForUri_m8748_ParameterInfos[] = 
{
	{"uri", 0, 134222089, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetIdentityForUri(System.String)
extern const MethodInfo RemotingServices_GetIdentityForUri_m8748_MethodInfo = 
{
	"GetIdentityForUri"/* name */
	, (methodPointerType)&RemotingServices_GetIdentityForUri_m8748/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Identity_t1663_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetIdentityForUri_m8748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_RemoveAppNameFromUri_m8749_ParameterInfos[] = 
{
	{"uri", 0, 134222090, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::RemoveAppNameFromUri(System.String)
extern const MethodInfo RemotingServices_RemoveAppNameFromUri_m8749_MethodInfo = 
{
	"RemoveAppNameFromUri"/* name */
	, (methodPointerType)&RemotingServices_RemoveAppNameFromUri_m8749/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_RemoveAppNameFromUri_m8749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetOrCreateClientIdentity_m8750_ParameterInfos[] = 
{
	{"objRef", 0, 134222091, 0, &ObjRef_t1669_0_0_0},
	{"proxyType", 1, 134222092, 0, &Type_t_0_0_0},
	{"clientProxy", 2, 134222093, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ClientIdentity System.Runtime.Remoting.RemotingServices::GetOrCreateClientIdentity(System.Runtime.Remoting.ObjRef,System.Type,System.Object&)
extern const MethodInfo RemotingServices_GetOrCreateClientIdentity_m8750_MethodInfo = 
{
	"GetOrCreateClientIdentity"/* name */
	, (methodPointerType)&RemotingServices_GetOrCreateClientIdentity_m8750/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &ClientIdentity_t1671_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t863/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetOrCreateClientIdentity_m8750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType WellKnownObjectMode_t1683_0_0_0;
extern const Il2CppType WellKnownObjectMode_t1683_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_CreateWellKnownServerIdentity_m8751_ParameterInfos[] = 
{
	{"objectType", 0, 134222094, 0, &Type_t_0_0_0},
	{"objectUri", 1, 134222095, 0, &String_t_0_0_0},
	{"mode", 2, 134222096, 0, &WellKnownObjectMode_t1683_0_0_0},
};
extern const Il2CppType ServerIdentity_t1384_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.RemotingServices::CreateWellKnownServerIdentity(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
extern const MethodInfo RemotingServices_CreateWellKnownServerIdentity_m8751_MethodInfo = 
{
	"CreateWellKnownServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_CreateWellKnownServerIdentity_m8751/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &ServerIdentity_t1384_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54/* invoker_method */
	, RemotingServices_t1678_RemotingServices_CreateWellKnownServerIdentity_m8751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ServerIdentity_t1384_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_RegisterServerIdentity_m8752_ParameterInfos[] = 
{
	{"identity", 0, 134222097, 0, &ServerIdentity_t1384_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterServerIdentity(System.Runtime.Remoting.ServerIdentity)
extern const MethodInfo RemotingServices_RegisterServerIdentity_m8752_MethodInfo = 
{
	"RegisterServerIdentity"/* name */
	, (methodPointerType)&RemotingServices_RegisterServerIdentity_m8752/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_RegisterServerIdentity_m8752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetProxyForRemoteObject_m8753_ParameterInfos[] = 
{
	{"objref", 0, 134222098, 0, &ObjRef_t1669_0_0_0},
	{"classToProxy", 1, 134222099, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetProxyForRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetProxyForRemoteObject_m8753_MethodInfo = 
{
	"GetProxyForRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetProxyForRemoteObject_m8753/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetProxyForRemoteObject_m8753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjRef_t1669_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetRemoteObject_m8754_ParameterInfos[] = 
{
	{"objRef", 0, 134222100, 0, &ObjRef_t1669_0_0_0},
	{"proxyType", 1, 134222101, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.RemotingServices::GetRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern const MethodInfo RemotingServices_GetRemoteObject_m8754_MethodInfo = 
{
	"GetRemoteObject"/* name */
	, (methodPointerType)&RemotingServices_GetRemoteObject_m8754/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetRemoteObject_m8754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::RegisterInternalChannels()
extern const MethodInfo RemotingServices_RegisterInternalChannels_m8755_MethodInfo = 
{
	"RegisterInternalChannels"/* name */
	, (methodPointerType)&RemotingServices_RegisterInternalChannels_m8755/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Identity_t1663_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_DisposeIdentity_m8756_ParameterInfos[] = 
{
	{"ident", 0, 134222102, 0, &Identity_t1663_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.RemotingServices::DisposeIdentity(System.Runtime.Remoting.Identity)
extern const MethodInfo RemotingServices_DisposeIdentity_m8756_MethodInfo = 
{
	"DisposeIdentity"/* name */
	, (methodPointerType)&RemotingServices_DisposeIdentity_m8756/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_DisposeIdentity_m8756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RemotingServices_t1678_RemotingServices_GetNormalizedUri_m8757_ParameterInfos[] = 
{
	{"uri", 0, 134222103, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.RemotingServices::GetNormalizedUri(System.String)
extern const MethodInfo RemotingServices_GetNormalizedUri_m8757_MethodInfo = 
{
	"GetNormalizedUri"/* name */
	, (methodPointerType)&RemotingServices_GetNormalizedUri_m8757/* method */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, RemotingServices_t1678_RemotingServices_GetNormalizedUri_m8757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemotingServices_t1678_MethodInfos[] =
{
	&RemotingServices__cctor_m8736_MethodInfo,
	&RemotingServices_GetVirtualMethod_m8737_MethodInfo,
	&RemotingServices_IsTransparentProxy_m8738_MethodInfo,
	&RemotingServices_GetServerTypeForUri_m8739_MethodInfo,
	&RemotingServices_Unmarshal_m8740_MethodInfo,
	&RemotingServices_Unmarshal_m8741_MethodInfo,
	&RemotingServices_GetRealProxy_m8742_MethodInfo,
	&RemotingServices_GetMethodBaseFromMethodMessage_m8743_MethodInfo,
	&RemotingServices_GetMethodBaseFromName_m8744_MethodInfo,
	&RemotingServices_FindInterfaceMethod_m8745_MethodInfo,
	&RemotingServices_CreateClientProxy_m8746_MethodInfo,
	&RemotingServices_CreateClientProxyForContextBound_m8747_MethodInfo,
	&RemotingServices_GetIdentityForUri_m8748_MethodInfo,
	&RemotingServices_RemoveAppNameFromUri_m8749_MethodInfo,
	&RemotingServices_GetOrCreateClientIdentity_m8750_MethodInfo,
	&RemotingServices_CreateWellKnownServerIdentity_m8751_MethodInfo,
	&RemotingServices_RegisterServerIdentity_m8752_MethodInfo,
	&RemotingServices_GetProxyForRemoteObject_m8753_MethodInfo,
	&RemotingServices_GetRemoteObject_m8754_MethodInfo,
	&RemotingServices_RegisterInternalChannels_m8755_MethodInfo,
	&RemotingServices_DisposeIdentity_m8756_MethodInfo,
	&RemotingServices_GetNormalizedUri_m8757_MethodInfo,
	NULL
};
static const Il2CppMethodReference RemotingServices_t1678_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RemotingServices_t1678_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingServices_t1678_0_0_0;
extern const Il2CppType RemotingServices_t1678_1_0_0;
struct RemotingServices_t1678;
const Il2CppTypeDefinitionMetadata RemotingServices_t1678_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t1678_VTable/* vtableMethods */
	, RemotingServices_t1678_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1642/* fieldStart */

};
TypeInfo RemotingServices_t1678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, RemotingServices_t1678_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemotingServices_t1678_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 555/* custom_attributes_cache */
	, &RemotingServices_t1678_0_0_0/* byval_arg */
	, &RemotingServices_t1678_1_0_0/* this_arg */
	, &RemotingServices_t1678_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t1678)/* instance_size */
	, sizeof (RemotingServices_t1678)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t1678_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t1384_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t1384_ServerIdentity__ctor_m8758_ParameterInfos[] = 
{
	{"objectUri", 0, 134222104, 0, &String_t_0_0_0},
	{"context", 1, 134222105, 0, &Context_t1632_0_0_0},
	{"objectType", 2, 134222106, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.ServerIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo ServerIdentity__ctor_m8758_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ServerIdentity__ctor_m8758/* method */
	, &ServerIdentity_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t1384_ServerIdentity__ctor_m8758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Remoting.ServerIdentity::get_ObjectType()
extern const MethodInfo ServerIdentity_get_ObjectType_m8759_MethodInfo = 
{
	"get_ObjectType"/* name */
	, (methodPointerType)&ServerIdentity_get_ObjectType_m8759/* method */
	, &ServerIdentity_t1384_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ServerIdentity_t1384_ServerIdentity_CreateObjRef_m8760_ParameterInfos[] = 
{
	{"requestedType", 0, 134222107, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.ServerIdentity::CreateObjRef(System.Type)
extern const MethodInfo ServerIdentity_CreateObjRef_m8760_MethodInfo = 
{
	"CreateObjRef"/* name */
	, (methodPointerType)&ServerIdentity_CreateObjRef_m8760/* method */
	, &ServerIdentity_t1384_il2cpp_TypeInfo/* declaring_type */
	, &ObjRef_t1669_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ServerIdentity_t1384_ServerIdentity_CreateObjRef_m8760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ServerIdentity_t1384_MethodInfos[] =
{
	&ServerIdentity__ctor_m8758_MethodInfo,
	&ServerIdentity_get_ObjectType_m8759_MethodInfo,
	&ServerIdentity_CreateObjRef_m8760_MethodInfo,
	NULL
};
extern const MethodInfo ServerIdentity_get_ObjectType_m8759_MethodInfo;
static const PropertyInfo ServerIdentity_t1384____ObjectType_PropertyInfo = 
{
	&ServerIdentity_t1384_il2cpp_TypeInfo/* parent */
	, "ObjectType"/* name */
	, &ServerIdentity_get_ObjectType_m8759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ServerIdentity_t1384_PropertyInfos[] =
{
	&ServerIdentity_t1384____ObjectType_PropertyInfo,
	NULL
};
extern const MethodInfo ServerIdentity_CreateObjRef_m8760_MethodInfo;
static const Il2CppMethodReference ServerIdentity_t1384_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ServerIdentity_CreateObjRef_m8760_MethodInfo,
};
static bool ServerIdentity_t1384_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ServerIdentity_t1384_1_0_0;
struct ServerIdentity_t1384;
const Il2CppTypeDefinitionMetadata ServerIdentity_t1384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1663_0_0_0/* parent */
	, ServerIdentity_t1384_VTable/* vtableMethods */
	, ServerIdentity_t1384_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1650/* fieldStart */

};
TypeInfo ServerIdentity_t1384_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ServerIdentity_t1384_MethodInfos/* methods */
	, ServerIdentity_t1384_PropertyInfos/* properties */
	, NULL/* events */
	, &ServerIdentity_t1384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t1384_0_0_0/* byval_arg */
	, &ServerIdentity_t1384_1_0_0/* this_arg */
	, &ServerIdentity_t1384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t1384)/* instance_size */
	, sizeof (ServerIdentity_t1384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t1679_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern const MethodInfo ClientActivatedIdentity_GetServerObject_m8761_MethodInfo = 
{
	"GetServerObject"/* name */
	, (methodPointerType)&ClientActivatedIdentity_GetServerObject_m8761/* method */
	, &ClientActivatedIdentity_t1679_il2cpp_TypeInfo/* declaring_type */
	, &MarshalByRefObject_t1022_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ClientActivatedIdentity_t1679_MethodInfos[] =
{
	&ClientActivatedIdentity_GetServerObject_m8761_MethodInfo,
	NULL
};
static const Il2CppMethodReference ClientActivatedIdentity_t1679_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ServerIdentity_CreateObjRef_m8760_MethodInfo,
};
static bool ClientActivatedIdentity_t1679_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientActivatedIdentity_t1679_0_0_0;
extern const Il2CppType ClientActivatedIdentity_t1679_1_0_0;
struct ClientActivatedIdentity_t1679;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t1679_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1384_0_0_0/* parent */
	, ClientActivatedIdentity_t1679_VTable/* vtableMethods */
	, ClientActivatedIdentity_t1679_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ClientActivatedIdentity_t1679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, ClientActivatedIdentity_t1679_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ClientActivatedIdentity_t1679_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t1679_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t1679_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t1679_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t1679)/* instance_size */
	, sizeof (ClientActivatedIdentity_t1679)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t1680_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingletonIdentity_t1680_SingletonIdentity__ctor_m8762_ParameterInfos[] = 
{
	{"objectUri", 0, 134222108, 0, &String_t_0_0_0},
	{"context", 1, 134222109, 0, &Context_t1632_0_0_0},
	{"objectType", 2, 134222110, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingletonIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingletonIdentity__ctor_m8762_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingletonIdentity__ctor_m8762/* method */
	, &SingletonIdentity_t1680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, SingletonIdentity_t1680_SingletonIdentity__ctor_m8762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingletonIdentity_t1680_MethodInfos[] =
{
	&SingletonIdentity__ctor_m8762_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingletonIdentity_t1680_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ServerIdentity_CreateObjRef_m8760_MethodInfo,
};
static bool SingletonIdentity_t1680_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingletonIdentity_t1680_0_0_0;
extern const Il2CppType SingletonIdentity_t1680_1_0_0;
struct SingletonIdentity_t1680;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t1680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1384_0_0_0/* parent */
	, SingletonIdentity_t1680_VTable/* vtableMethods */
	, SingletonIdentity_t1680_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingletonIdentity_t1680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingletonIdentity_t1680_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingletonIdentity_t1680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t1680_0_0_0/* byval_arg */
	, &SingletonIdentity_t1680_1_0_0/* this_arg */
	, &SingletonIdentity_t1680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t1680)/* instance_size */
	, sizeof (SingletonIdentity_t1680)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t1681_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Context_t1632_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo SingleCallIdentity_t1681_SingleCallIdentity__ctor_m8763_ParameterInfos[] = 
{
	{"objectUri", 0, 134222111, 0, &String_t_0_0_0},
	{"context", 1, 134222112, 0, &Context_t1632_0_0_0},
	{"objectType", 2, 134222113, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern const MethodInfo SingleCallIdentity__ctor_m8763_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SingleCallIdentity__ctor_m8763/* method */
	, &SingleCallIdentity_t1681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t/* invoker_method */
	, SingleCallIdentity_t1681_SingleCallIdentity__ctor_m8763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SingleCallIdentity_t1681_MethodInfos[] =
{
	&SingleCallIdentity__ctor_m8763_MethodInfo,
	NULL
};
static const Il2CppMethodReference SingleCallIdentity_t1681_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ServerIdentity_CreateObjRef_m8760_MethodInfo,
};
static bool SingleCallIdentity_t1681_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingleCallIdentity_t1681_0_0_0;
extern const Il2CppType SingleCallIdentity_t1681_1_0_0;
struct SingleCallIdentity_t1681;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t1681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1384_0_0_0/* parent */
	, SingleCallIdentity_t1681_VTable/* vtableMethods */
	, SingleCallIdentity_t1681_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SingleCallIdentity_t1681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, SingleCallIdentity_t1681_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SingleCallIdentity_t1681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t1681_0_0_0/* byval_arg */
	, &SingleCallIdentity_t1681_1_0_0/* this_arg */
	, &SingleCallIdentity_t1681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t1681)/* instance_size */
	, sizeof (SingleCallIdentity_t1681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t1667_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
extern const MethodInfo TypeEntry_get_AssemblyName_m8764_MethodInfo = 
{
	"get_AssemblyName"/* name */
	, (methodPointerType)&TypeEntry_get_AssemblyName_m8764/* method */
	, &TypeEntry_t1667_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
extern const MethodInfo TypeEntry_get_TypeName_m8765_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeEntry_get_TypeName_m8765/* method */
	, &TypeEntry_t1667_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeEntry_t1667_MethodInfos[] =
{
	&TypeEntry_get_AssemblyName_m8764_MethodInfo,
	&TypeEntry_get_TypeName_m8765_MethodInfo,
	NULL
};
extern const MethodInfo TypeEntry_get_AssemblyName_m8764_MethodInfo;
static const PropertyInfo TypeEntry_t1667____AssemblyName_PropertyInfo = 
{
	&TypeEntry_t1667_il2cpp_TypeInfo/* parent */
	, "AssemblyName"/* name */
	, &TypeEntry_get_AssemblyName_m8764_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TypeEntry_get_TypeName_m8765_MethodInfo;
static const PropertyInfo TypeEntry_t1667____TypeName_PropertyInfo = 
{
	&TypeEntry_t1667_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeEntry_get_TypeName_m8765_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeEntry_t1667_PropertyInfos[] =
{
	&TypeEntry_t1667____AssemblyName_PropertyInfo,
	&TypeEntry_t1667____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeEntry_t1667_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TypeEntry_t1667_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeEntry_t1667_1_0_0;
struct TypeEntry_t1667;
const Il2CppTypeDefinitionMetadata TypeEntry_t1667_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t1667_VTable/* vtableMethods */
	, TypeEntry_t1667_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1653/* fieldStart */

};
TypeInfo TypeEntry_t1667_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeEntry_t1667_MethodInfos/* methods */
	, TypeEntry_t1667_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeEntry_t1667_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 558/* custom_attributes_cache */
	, &TypeEntry_t1667_0_0_0/* byval_arg */
	, &TypeEntry_t1667_1_0_0/* this_arg */
	, &TypeEntry_t1667_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t1667)/* instance_size */
	, sizeof (TypeEntry_t1667)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t1682_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo TypeInfo_t1682_TypeInfo__ctor_m8766_ParameterInfos[] = 
{
	{"type", 0, 134222114, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.TypeInfo::.ctor(System.Type)
extern const MethodInfo TypeInfo__ctor_m8766_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInfo__ctor_m8766/* method */
	, &TypeInfo_t1682_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, TypeInfo_t1682_TypeInfo__ctor_m8766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Remoting.TypeInfo::get_TypeName()
extern const MethodInfo TypeInfo_get_TypeName_m8767_MethodInfo = 
{
	"get_TypeName"/* name */
	, (methodPointerType)&TypeInfo_get_TypeName_m8767/* method */
	, &TypeInfo_t1682_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInfo_t1682_MethodInfos[] =
{
	&TypeInfo__ctor_m8766_MethodInfo,
	&TypeInfo_get_TypeName_m8767_MethodInfo,
	NULL
};
extern const MethodInfo TypeInfo_get_TypeName_m8767_MethodInfo;
static const PropertyInfo TypeInfo_t1682____TypeName_PropertyInfo = 
{
	&TypeInfo_t1682_il2cpp_TypeInfo/* parent */
	, "TypeName"/* name */
	, &TypeInfo_get_TypeName_m8767_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeInfo_t1682_PropertyInfos[] =
{
	&TypeInfo_t1682____TypeName_PropertyInfo,
	NULL
};
static const Il2CppMethodReference TypeInfo_t1682_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&TypeInfo_get_TypeName_m8767_MethodInfo,
};
static bool TypeInfo_t1682_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* TypeInfo_t1682_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1673_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t1682_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1673_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t1682_0_0_0;
extern const Il2CppType TypeInfo_t1682_1_0_0;
struct TypeInfo_t1682;
const Il2CppTypeDefinitionMetadata TypeInfo_t1682_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t1682_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t1682_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t1682_VTable/* vtableMethods */
	, TypeInfo_t1682_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1655/* fieldStart */

};
TypeInfo TypeInfo_t1682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, TypeInfo_t1682_MethodInfos/* methods */
	, TypeInfo_t1682_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeInfo_t1682_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t1682_0_0_0/* byval_arg */
	, &TypeInfo_t1682_1_0_0/* this_arg */
	, &TypeInfo_t1682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t1682)/* instance_size */
	, sizeof (TypeInfo_t1682)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t1683_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static const MethodInfo* WellKnownObjectMode_t1683_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference WellKnownObjectMode_t1683_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool WellKnownObjectMode_t1683_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t1683_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownObjectMode_t1683_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t1683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t1683_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, WellKnownObjectMode_t1683_VTable/* vtableMethods */
	, WellKnownObjectMode_t1683_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1658/* fieldStart */

};
TypeInfo WellKnownObjectMode_t1683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, WellKnownObjectMode_t1683_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 559/* custom_attributes_cache */
	, &WellKnownObjectMode_t1683_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t1683_1_0_0/* this_arg */
	, &WellKnownObjectMode_t1683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t1683)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t1683)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t1684_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::.cctor()
extern const MethodInfo BinaryCommon__cctor_m8768_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&BinaryCommon__cctor_m8768/* method */
	, &BinaryCommon_t1684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo BinaryCommon_t1684_BinaryCommon_IsPrimitive_m8769_ParameterInfos[] = 
{
	{"type", 0, 134222115, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.BinaryCommon::IsPrimitive(System.Type)
extern const MethodInfo BinaryCommon_IsPrimitive_m8769_MethodInfo = 
{
	"IsPrimitive"/* name */
	, (methodPointerType)&BinaryCommon_IsPrimitive_m8769/* method */
	, &BinaryCommon_t1684_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, BinaryCommon_t1684_BinaryCommon_IsPrimitive_m8769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo BinaryCommon_t1684_BinaryCommon_GetTypeFromCode_m8770_ParameterInfos[] = 
{
	{"code", 0, 134222116, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.BinaryCommon::GetTypeFromCode(System.Int32)
extern const MethodInfo BinaryCommon_GetTypeFromCode_m8770_MethodInfo = 
{
	"GetTypeFromCode"/* name */
	, (methodPointerType)&BinaryCommon_GetTypeFromCode_m8770/* method */
	, &BinaryCommon_t1684_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, BinaryCommon_t1684_BinaryCommon_GetTypeFromCode_m8770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType ByteU5BU5D_t546_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo BinaryCommon_t1684_BinaryCommon_SwapBytes_m8771_ParameterInfos[] = 
{
	{"byteArray", 0, 134222117, 0, &ByteU5BU5D_t546_0_0_0},
	{"size", 1, 134222118, 0, &Int32_t54_0_0_0},
	{"dataSize", 2, 134222119, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryCommon::SwapBytes(System.Byte[],System.Int32,System.Int32)
extern const MethodInfo BinaryCommon_SwapBytes_m8771_MethodInfo = 
{
	"SwapBytes"/* name */
	, (methodPointerType)&BinaryCommon_SwapBytes_m8771/* method */
	, &BinaryCommon_t1684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, BinaryCommon_t1684_BinaryCommon_SwapBytes_m8771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryCommon_t1684_MethodInfos[] =
{
	&BinaryCommon__cctor_m8768_MethodInfo,
	&BinaryCommon_IsPrimitive_m8769_MethodInfo,
	&BinaryCommon_GetTypeFromCode_m8770_MethodInfo,
	&BinaryCommon_SwapBytes_m8771_MethodInfo,
	NULL
};
static const Il2CppMethodReference BinaryCommon_t1684_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool BinaryCommon_t1684_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryCommon_t1684_0_0_0;
extern const Il2CppType BinaryCommon_t1684_1_0_0;
struct BinaryCommon_t1684;
const Il2CppTypeDefinitionMetadata BinaryCommon_t1684_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t1684_VTable/* vtableMethods */
	, BinaryCommon_t1684_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1661/* fieldStart */

};
TypeInfo BinaryCommon_t1684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryCommon_t1684_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BinaryCommon_t1684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t1684_0_0_0/* byval_arg */
	, &BinaryCommon_t1684_1_0_0/* this_arg */
	, &BinaryCommon_t1684_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t1684)/* instance_size */
	, sizeof (BinaryCommon_t1684)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t1684_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t1685_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
static const MethodInfo* BinaryElement_t1685_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference BinaryElement_t1685_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool BinaryElement_t1685_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BinaryElement_t1685_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryElement_t1685_1_0_0;
const Il2CppTypeDefinitionMetadata BinaryElement_t1685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t1685_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, BinaryElement_t1685_VTable/* vtableMethods */
	, BinaryElement_t1685_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1665/* fieldStart */

};
TypeInfo BinaryElement_t1685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryElement_t1685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t1685_0_0_0/* byval_arg */
	, &BinaryElement_t1685_1_0_0/* this_arg */
	, &BinaryElement_t1685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t1685)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t1685)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t1686_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static const MethodInfo* TypeTag_t1686_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeTag_t1686_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TypeTag_t1686_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeTag_t1686_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeTag_t1686_0_0_0;
extern const Il2CppType TypeTag_t1686_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t1686_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t1686_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TypeTag_t1686_VTable/* vtableMethods */
	, TypeTag_t1686_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1689/* fieldStart */

};
TypeInfo TypeTag_t1686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, TypeTag_t1686_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t1686_0_0_0/* byval_arg */
	, &TypeTag_t1686_1_0_0/* this_arg */
	, &TypeTag_t1686_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t1686)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t1686)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t1687_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static const MethodInfo* MethodFlags_t1687_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference MethodFlags_t1687_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool MethodFlags_t1687_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodFlags_t1687_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodFlags_t1687_0_0_0;
extern const Il2CppType MethodFlags_t1687_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t1687_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t1687_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, MethodFlags_t1687_VTable/* vtableMethods */
	, MethodFlags_t1687_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1698/* fieldStart */

};
TypeInfo MethodFlags_t1687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MethodFlags_t1687_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t1687_0_0_0/* byval_arg */
	, &MethodFlags_t1687_1_0_0/* this_arg */
	, &MethodFlags_t1687_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t1687)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t1687)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t1688_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static const MethodInfo* ReturnTypeTag_t1688_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReturnTypeTag_t1688_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool ReturnTypeTag_t1688_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t1688_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnTypeTag_t1688_0_0_0;
extern const Il2CppType ReturnTypeTag_t1688_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t1688_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t1688_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, ReturnTypeTag_t1688_VTable/* vtableMethods */
	, ReturnTypeTag_t1688_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1709/* fieldStart */

};
TypeInfo ReturnTypeTag_t1688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ReturnTypeTag_t1688_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t1688_0_0_0/* byval_arg */
	, &ReturnTypeTag_t1688_1_0_0/* this_arg */
	, &ReturnTypeTag_t1688_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t1688)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t1688)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t1677_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern const MethodInfo BinaryFormatter__ctor_m8772_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m8772/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ISurrogateSelector_t1657_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo BinaryFormatter_t1677_BinaryFormatter__ctor_m8773_ParameterInfos[] = 
{
	{"selector", 0, 134222120, 0, &ISurrogateSelector_t1657_0_0_0},
	{"context", 1, 134222121, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo BinaryFormatter__ctor_m8773_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BinaryFormatter__ctor_m8773/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, BinaryFormatter_t1677_BinaryFormatter__ctor_m8773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_DefaultSurrogateSelector()
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m8774_MethodInfo = 
{
	"get_DefaultSurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_DefaultSurrogateSelector_m8774/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t1657_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 562/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FormatterAssemblyStyle_t1696_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t1696_0_0_0;
static const ParameterInfo BinaryFormatter_t1677_BinaryFormatter_set_AssemblyFormat_m8775_ParameterInfos[] = 
{
	{"value", 0, 134222122, 0, &FormatterAssemblyStyle_t1696_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_AssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m8775_MethodInfo = 
{
	"set_AssemblyFormat"/* name */
	, (methodPointerType)&BinaryFormatter_set_AssemblyFormat_m8775/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, BinaryFormatter_t1677_BinaryFormatter_set_AssemblyFormat_m8775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationBinder_t1689_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Binder()
extern const MethodInfo BinaryFormatter_get_Binder_m8776_MethodInfo = 
{
	"get_Binder"/* name */
	, (methodPointerType)&BinaryFormatter_get_Binder_m8776/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &SerializationBinder_t1689_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Context()
extern const MethodInfo BinaryFormatter_get_Context_m8777_MethodInfo = 
{
	"get_Context"/* name */
	, (methodPointerType)&BinaryFormatter_get_Context_m8777/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &StreamingContext_t726_0_0_0/* return_type */
	, RuntimeInvoker_StreamingContext_t726/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_SurrogateSelector()
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m8778_MethodInfo = 
{
	"get_SurrogateSelector"/* name */
	, (methodPointerType)&BinaryFormatter_get_SurrogateSelector_m8778/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &ISurrogateSelector_t1657_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TypeFilterLevel_t1698_0_0_0;
extern void* RuntimeInvoker_TypeFilterLevel_t1698 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_FilterLevel()
extern const MethodInfo BinaryFormatter_get_FilterLevel_m8779_MethodInfo = 
{
	"get_FilterLevel"/* name */
	, (methodPointerType)&BinaryFormatter_get_FilterLevel_m8779/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &TypeFilterLevel_t1698_0_0_0/* return_type */
	, RuntimeInvoker_TypeFilterLevel_t1698/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t1294_0_0_0;
extern const Il2CppType Stream_t1294_0_0_0;
static const ParameterInfo BinaryFormatter_t1677_BinaryFormatter_Deserialize_m8780_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222123, 0, &Stream_t1294_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern const MethodInfo BinaryFormatter_Deserialize_m8780_MethodInfo = 
{
	"Deserialize"/* name */
	, (methodPointerType)&BinaryFormatter_Deserialize_m8780/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t1677_BinaryFormatter_Deserialize_m8780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Stream_t1294_0_0_0;
extern const Il2CppType HeaderHandler_t1887_0_0_0;
extern const Il2CppType HeaderHandler_t1887_0_0_0;
static const ParameterInfo BinaryFormatter_t1677_BinaryFormatter_NoCheckDeserialize_m8781_ParameterInfos[] = 
{
	{"serializationStream", 0, 134222124, 0, &Stream_t1294_0_0_0},
	{"handler", 1, 134222125, 0, &HeaderHandler_t1887_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern const MethodInfo BinaryFormatter_NoCheckDeserialize_m8781_MethodInfo = 
{
	"NoCheckDeserialize"/* name */
	, (methodPointerType)&BinaryFormatter_NoCheckDeserialize_m8781/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, BinaryFormatter_t1677_BinaryFormatter_NoCheckDeserialize_m8781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_1_0_2;
extern const Il2CppType Boolean_t72_1_0_0;
static const ParameterInfo BinaryFormatter_t1677_BinaryFormatter_ReadBinaryHeader_m8782_ParameterInfos[] = 
{
	{"reader", 0, 134222126, 0, &BinaryReader_t1505_0_0_0},
	{"hasHeaders", 1, 134222127, 0, &Boolean_t72_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::ReadBinaryHeader(System.IO.BinaryReader,System.Boolean&)
extern const MethodInfo BinaryFormatter_ReadBinaryHeader_m8782_MethodInfo = 
{
	"ReadBinaryHeader"/* name */
	, (methodPointerType)&BinaryFormatter_ReadBinaryHeader_m8782/* method */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_BooleanU26_t441/* invoker_method */
	, BinaryFormatter_t1677_BinaryFormatter_ReadBinaryHeader_m8782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BinaryFormatter_t1677_MethodInfos[] =
{
	&BinaryFormatter__ctor_m8772_MethodInfo,
	&BinaryFormatter__ctor_m8773_MethodInfo,
	&BinaryFormatter_get_DefaultSurrogateSelector_m8774_MethodInfo,
	&BinaryFormatter_set_AssemblyFormat_m8775_MethodInfo,
	&BinaryFormatter_get_Binder_m8776_MethodInfo,
	&BinaryFormatter_get_Context_m8777_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m8778_MethodInfo,
	&BinaryFormatter_get_FilterLevel_m8779_MethodInfo,
	&BinaryFormatter_Deserialize_m8780_MethodInfo,
	&BinaryFormatter_NoCheckDeserialize_m8781_MethodInfo,
	&BinaryFormatter_ReadBinaryHeader_m8782_MethodInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_get_DefaultSurrogateSelector_m8774_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____DefaultSurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "DefaultSurrogateSelector"/* name */
	, &BinaryFormatter_get_DefaultSurrogateSelector_m8774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_set_AssemblyFormat_m8775_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____AssemblyFormat_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "AssemblyFormat"/* name */
	, NULL/* get */
	, &BinaryFormatter_set_AssemblyFormat_m8775_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Binder_m8776_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____Binder_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "Binder"/* name */
	, &BinaryFormatter_get_Binder_m8776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_Context_m8777_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____Context_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "Context"/* name */
	, &BinaryFormatter_get_Context_m8777_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_SurrogateSelector_m8778_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____SurrogateSelector_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "SurrogateSelector"/* name */
	, &BinaryFormatter_get_SurrogateSelector_m8778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo BinaryFormatter_get_FilterLevel_m8779_MethodInfo;
static const PropertyInfo BinaryFormatter_t1677____FilterLevel_PropertyInfo = 
{
	&BinaryFormatter_t1677_il2cpp_TypeInfo/* parent */
	, "FilterLevel"/* name */
	, &BinaryFormatter_get_FilterLevel_m8779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BinaryFormatter_t1677_PropertyInfos[] =
{
	&BinaryFormatter_t1677____DefaultSurrogateSelector_PropertyInfo,
	&BinaryFormatter_t1677____AssemblyFormat_PropertyInfo,
	&BinaryFormatter_t1677____Binder_PropertyInfo,
	&BinaryFormatter_t1677____Context_PropertyInfo,
	&BinaryFormatter_t1677____SurrogateSelector_PropertyInfo,
	&BinaryFormatter_t1677____FilterLevel_PropertyInfo,
	NULL
};
extern const MethodInfo BinaryFormatter_Deserialize_m8780_MethodInfo;
static const Il2CppMethodReference BinaryFormatter_t1677_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&BinaryFormatter_get_Binder_m8776_MethodInfo,
	&BinaryFormatter_get_Context_m8777_MethodInfo,
	&BinaryFormatter_get_SurrogateSelector_m8778_MethodInfo,
	&BinaryFormatter_Deserialize_m8780_MethodInfo,
};
static bool BinaryFormatter_t1677_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* BinaryFormatter_t1677_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t2012_0_0_0,
	&IFormatter_t2014_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t1677_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t2012_0_0_0, 4},
	{ &IFormatter_t2014_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryFormatter_t1677_0_0_0;
extern const Il2CppType BinaryFormatter_t1677_1_0_0;
struct BinaryFormatter_t1677;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t1677_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t1677_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t1677_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t1677_VTable/* vtableMethods */
	, BinaryFormatter_t1677_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1714/* fieldStart */

};
TypeInfo BinaryFormatter_t1677_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, BinaryFormatter_t1677_MethodInfos/* methods */
	, BinaryFormatter_t1677_PropertyInfos/* properties */
	, NULL/* events */
	, &BinaryFormatter_t1677_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 560/* custom_attributes_cache */
	, &BinaryFormatter_t1677_0_0_0/* byval_arg */
	, &BinaryFormatter_t1677_1_0_0/* this_arg */
	, &BinaryFormatter_t1677_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t1677)/* instance_size */
	, sizeof (BinaryFormatter_t1677)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t1677_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t1690_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType HeaderHandler_t1887_0_0_0;
extern const Il2CppType BinaryFormatter_t1677_0_0_0;
static const ParameterInfo MessageFormatter_t1690_MessageFormatter_ReadMethodCall_m8783_ParameterInfos[] = 
{
	{"elem", 0, 134222128, 0, &BinaryElement_t1685_0_0_0},
	{"reader", 1, 134222129, 0, &BinaryReader_t1505_0_0_0},
	{"hasHeaders", 2, 134222130, 0, &Boolean_t72_0_0_0},
	{"headerHandler", 3, 134222131, 0, &HeaderHandler_t1887_0_0_0},
	{"formatter", 4, 134222132, 0, &BinaryFormatter_t1677_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodCall(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodCall_m8783_MethodInfo = 
{
	"ReadMethodCall"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodCall_m8783/* method */
	, &MessageFormatter_t1690_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t1690_MessageFormatter_ReadMethodCall_m8783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType HeaderHandler_t1887_0_0_0;
extern const Il2CppType IMethodCallMessage_t1922_0_0_0;
extern const Il2CppType BinaryFormatter_t1677_0_0_0;
static const ParameterInfo MessageFormatter_t1690_MessageFormatter_ReadMethodResponse_m8784_ParameterInfos[] = 
{
	{"elem", 0, 134222133, 0, &BinaryElement_t1685_0_0_0},
	{"reader", 1, 134222134, 0, &BinaryReader_t1505_0_0_0},
	{"hasHeaders", 2, 134222135, 0, &Boolean_t72_0_0_0},
	{"headerHandler", 3, 134222136, 0, &HeaderHandler_t1887_0_0_0},
	{"methodCallMessage", 4, 134222137, 0, &IMethodCallMessage_t1922_0_0_0},
	{"formatter", 5, 134222138, 0, &BinaryFormatter_t1677_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.MessageFormatter::ReadMethodResponse(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo MessageFormatter_ReadMethodResponse_m8784_MethodInfo = 
{
	"ReadMethodResponse"/* name */
	, (methodPointerType)&MessageFormatter_ReadMethodResponse_m8784/* method */
	, &MessageFormatter_t1690_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Byte_t367_Object_t_SByte_t73_Object_t_Object_t_Object_t/* invoker_method */
	, MessageFormatter_t1690_MessageFormatter_ReadMethodResponse_m8784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MessageFormatter_t1690_MethodInfos[] =
{
	&MessageFormatter_ReadMethodCall_m8783_MethodInfo,
	&MessageFormatter_ReadMethodResponse_m8784_MethodInfo,
	NULL
};
static const Il2CppMethodReference MessageFormatter_t1690_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MessageFormatter_t1690_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MessageFormatter_t1690_0_0_0;
extern const Il2CppType MessageFormatter_t1690_1_0_0;
struct MessageFormatter_t1690;
const Il2CppTypeDefinitionMetadata MessageFormatter_t1690_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t1690_VTable/* vtableMethods */
	, MessageFormatter_t1690_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MessageFormatter_t1690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, MessageFormatter_t1690_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MessageFormatter_t1690_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t1690_0_0_0/* byval_arg */
	, &MessageFormatter_t1690_1_0_0/* this_arg */
	, &MessageFormatter_t1690_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t1690)/* instance_size */
	, sizeof (MessageFormatter_t1690)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t1692_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata::.ctor()
extern const MethodInfo TypeMetadata__ctor_m8785_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeMetadata__ctor_m8785/* method */
	, &TypeMetadata_t1692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeMetadata_t1692_MethodInfos[] =
{
	&TypeMetadata__ctor_m8785_MethodInfo,
	NULL
};
static const Il2CppMethodReference TypeMetadata_t1692_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool TypeMetadata_t1692_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t1692_0_0_0;
extern const Il2CppType TypeMetadata_t1692_1_0_0;
extern TypeInfo ObjectReader_t1695_il2cpp_TypeInfo;
extern const Il2CppType ObjectReader_t1695_0_0_0;
struct TypeMetadata_t1692;
const Il2CppTypeDefinitionMetadata TypeMetadata_t1692_DefinitionMetadata = 
{
	&ObjectReader_t1695_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t1692_VTable/* vtableMethods */
	, TypeMetadata_t1692_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1721/* fieldStart */

};
TypeInfo TypeMetadata_t1692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, TypeMetadata_t1692_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeMetadata_t1692_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t1692_0_0_0/* byval_arg */
	, &TypeMetadata_t1692_1_0_0/* this_arg */
	, &TypeMetadata_t1692_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t1692)/* instance_size */
	, sizeof (TypeMetadata_t1692)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t1693_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ArrayNullFiller_t1693_ArrayNullFiller__ctor_m8786_ParameterInfos[] = 
{
	{"count", 0, 134222233, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller::.ctor(System.Int32)
extern const MethodInfo ArrayNullFiller__ctor_m8786_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArrayNullFiller__ctor_m8786/* method */
	, &ArrayNullFiller_t1693_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ArrayNullFiller_t1693_ArrayNullFiller__ctor_m8786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArrayNullFiller_t1693_MethodInfos[] =
{
	&ArrayNullFiller__ctor_m8786_MethodInfo,
	NULL
};
static const Il2CppMethodReference ArrayNullFiller_t1693_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ArrayNullFiller_t1693_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayNullFiller_t1693_0_0_0;
extern const Il2CppType ArrayNullFiller_t1693_1_0_0;
struct ArrayNullFiller_t1693;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t1693_DefinitionMetadata = 
{
	&ObjectReader_t1695_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t1693_VTable/* vtableMethods */
	, ArrayNullFiller_t1693_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1727/* fieldStart */

};
TypeInfo ArrayNullFiller_t1693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, ArrayNullFiller_t1693_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ArrayNullFiller_t1693_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t1693_0_0_0/* byval_arg */
	, &ArrayNullFiller_t1693_1_0_0/* this_arg */
	, &ArrayNullFiller_t1693_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t1693)/* instance_size */
	, sizeof (ArrayNullFiller_t1693)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern const Il2CppType BinaryFormatter_t1677_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader__ctor_m8787_ParameterInfos[] = 
{
	{"formatter", 0, 134222139, 0, &BinaryFormatter_t1677_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::.ctor(System.Runtime.Serialization.Formatters.Binary.BinaryFormatter)
extern const MethodInfo ObjectReader__ctor_m8787_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectReader__ctor_m8787/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader__ctor_m8787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t1886_1_0_2;
extern const Il2CppType HeaderU5BU5D_t1886_1_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadObjectGraph_m8788_ParameterInfos[] = 
{
	{"reader", 0, 134222140, 0, &BinaryReader_t1505_0_0_0},
	{"readHeaders", 1, 134222141, 0, &Boolean_t72_0_0_0},
	{"result", 2, 134222142, 0, &Object_t_1_0_2},
	{"headers", 3, 134222143, 0, &HeaderU5BU5D_t1886_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m8788_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m8788/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadObjectGraph_m8788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType HeaderU5BU5D_t1886_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadObjectGraph_m8789_ParameterInfos[] = 
{
	{"elem", 0, 134222144, 0, &BinaryElement_t1685_0_0_0},
	{"reader", 1, 134222145, 0, &BinaryReader_t1505_0_0_0},
	{"readHeaders", 2, 134222146, 0, &Boolean_t72_0_0_0},
	{"result", 3, 134222147, 0, &Object_t_1_0_2},
	{"headers", 4, 134222148, 0, &HeaderU5BU5D_t1886_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Byte_t367_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectGraph(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Boolean,System.Object&,System.Runtime.Remoting.Messaging.Header[]&)
extern const MethodInfo ObjectReader_ReadObjectGraph_m8789_MethodInfo = 
{
	"ReadObjectGraph"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectGraph_m8789/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Byte_t367_Object_t_SByte_t73_ObjectU26_t863_HeaderU5BU5DU26_t2385/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadObjectGraph_m8789_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadNextObject_m8790_ParameterInfos[] = 
{
	{"element", 0, 134222149, 0, &BinaryElement_t1685_0_0_0},
	{"reader", 1, 134222150, 0, &BinaryReader_t1505_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Byte_t367_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m8790_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m8790/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Byte_t367_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadNextObject_m8790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadNextObject_m8791_ParameterInfos[] = 
{
	{"reader", 0, 134222151, 0, &BinaryReader_t1505_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadNextObject(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadNextObject_m8791_MethodInfo = 
{
	"ReadNextObject"/* name */
	, (methodPointerType)&ObjectReader_ReadNextObject_m8791/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadNextObject_m8791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::get_CurrentObject()
extern const MethodInfo ObjectReader_get_CurrentObject_m8792_MethodInfo = 
{
	"get_CurrentObject"/* name */
	, (methodPointerType)&ObjectReader_get_CurrentObject_m8792/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryElement_t1685_0_0_0;
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Int64_t753_1_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t725_1_0_2;
extern const Il2CppType SerializationInfo_t725_1_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadObject_m8793_ParameterInfos[] = 
{
	{"element", 0, 134222152, 0, &BinaryElement_t1685_0_0_0},
	{"reader", 1, 134222153, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 2, 134222154, 0, &Int64_t753_1_0_2},
	{"value", 3, 134222155, 0, &Object_t_1_0_2},
	{"info", 4, 134222156, 0, &SerializationInfo_t725_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Byte_t367_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObject(System.Runtime.Serialization.Formatters.Binary.BinaryElement,System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObject_m8793_MethodInfo = 
{
	"ReadObject"/* name */
	, (methodPointerType)&ObjectReader_ReadObject_m8793/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Byte_t367_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadObject_m8793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadAssembly_m8794_ParameterInfos[] = 
{
	{"reader", 0, 134222157, 0, &BinaryReader_t1505_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadAssembly(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadAssembly_m8794_MethodInfo = 
{
	"ReadAssembly"/* name */
	, (methodPointerType)&ObjectReader_ReadAssembly_m8794/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadAssembly_m8794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t725_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadObjectInstance_m8795_ParameterInfos[] = 
{
	{"reader", 0, 134222158, 0, &BinaryReader_t1505_0_0_0},
	{"isRuntimeObject", 1, 134222159, 0, &Boolean_t72_0_0_0},
	{"hasTypeInfo", 2, 134222160, 0, &Boolean_t72_0_0_0},
	{"objectId", 3, 134222161, 0, &Int64_t753_1_0_2},
	{"value", 4, 134222162, 0, &Object_t_1_0_2},
	{"info", 5, 134222163, 0, &SerializationInfo_t725_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectInstance(System.IO.BinaryReader,System.Boolean,System.Boolean,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectInstance_m8795_MethodInfo = 
{
	"ReadObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectInstance_m8795/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadObjectInstance_m8795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t725_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadRefTypeObjectInstance_m8796_ParameterInfos[] = 
{
	{"reader", 0, 134222164, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222165, 0, &Int64_t753_1_0_2},
	{"value", 2, 134222166, 0, &Object_t_1_0_2},
	{"info", 3, 134222167, 0, &SerializationInfo_t725_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadRefTypeObjectInstance(System.IO.BinaryReader,System.Int64&,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadRefTypeObjectInstance_m8796_MethodInfo = 
{
	"ReadRefTypeObjectInstance"/* name */
	, (methodPointerType)&ObjectReader_ReadRefTypeObjectInstance_m8796/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863_SerializationInfoU26_t2386/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadRefTypeObjectInstance_m8796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType TypeMetadata_t1692_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType SerializationInfo_t725_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadObjectContent_m8797_ParameterInfos[] = 
{
	{"reader", 0, 134222168, 0, &BinaryReader_t1505_0_0_0},
	{"metadata", 1, 134222169, 0, &TypeMetadata_t1692_0_0_0},
	{"objectId", 2, 134222170, 0, &Int64_t753_0_0_0},
	{"objectInstance", 3, 134222171, 0, &Object_t_1_0_2},
	{"info", 4, 134222172, 0, &SerializationInfo_t725_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_ObjectU26_t863_SerializationInfoU26_t2386 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadObjectContent(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata,System.Int64,System.Object&,System.Runtime.Serialization.SerializationInfo&)
extern const MethodInfo ObjectReader_ReadObjectContent_m8797_MethodInfo = 
{
	"ReadObjectContent"/* name */
	, (methodPointerType)&ObjectReader_ReadObjectContent_m8797/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_ObjectU26_t863_SerializationInfoU26_t2386/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadObjectContent_m8797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_RegisterObject_m8798_ParameterInfos[] = 
{
	{"objectId", 0, 134222173, 0, &Int64_t753_0_0_0},
	{"objectInstance", 1, 134222174, 0, &Object_t_0_0_0},
	{"info", 2, 134222175, 0, &SerializationInfo_t725_0_0_0},
	{"parentObjectId", 3, 134222176, 0, &Int64_t753_0_0_0},
	{"parentObjectMemeber", 4, 134222177, 0, &MemberInfo_t_0_0_0},
	{"indices", 5, 134222178, 0, &Int32U5BU5D_t1082_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753_Object_t_Object_t_Int64_t753_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RegisterObject(System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.Int64,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RegisterObject_m8798_MethodInfo = 
{
	"RegisterObject"/* name */
	, (methodPointerType)&ObjectReader_RegisterObject_m8798/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753_Object_t_Object_t_Int64_t753_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_RegisterObject_m8798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadStringIntance_m8799_ParameterInfos[] = 
{
	{"reader", 0, 134222179, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222180, 0, &Int64_t753_1_0_2},
	{"value", 2, 134222181, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadStringIntance(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadStringIntance_m8799_MethodInfo = 
{
	"ReadStringIntance"/* name */
	, (methodPointerType)&ObjectReader_ReadStringIntance_m8799/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadStringIntance_m8799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadGenericArray_m8800_ParameterInfos[] = 
{
	{"reader", 0, 134222182, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222183, 0, &Int64_t753_1_0_2},
	{"val", 2, 134222184, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadGenericArray(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadGenericArray_m8800_MethodInfo = 
{
	"ReadGenericArray"/* name */
	, (methodPointerType)&ObjectReader_ReadGenericArray_m8800/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadGenericArray_m8800_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadBoxedPrimitiveTypeValue_m8801_ParameterInfos[] = 
{
	{"reader", 0, 134222185, 0, &BinaryReader_t1505_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadBoxedPrimitiveTypeValue(System.IO.BinaryReader)
extern const MethodInfo ObjectReader_ReadBoxedPrimitiveTypeValue_m8801_MethodInfo = 
{
	"ReadBoxedPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadBoxedPrimitiveTypeValue_m8801/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadBoxedPrimitiveTypeValue_m8801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadArrayOfPrimitiveType_m8802_ParameterInfos[] = 
{
	{"reader", 0, 134222186, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222187, 0, &Int64_t753_1_0_2},
	{"val", 2, 134222188, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfPrimitiveType(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfPrimitiveType_m8802_MethodInfo = 
{
	"ReadArrayOfPrimitiveType"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfPrimitiveType_m8802/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadArrayOfPrimitiveType_m8802_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_BlockRead_m8803_ParameterInfos[] = 
{
	{"reader", 0, 134222189, 0, &BinaryReader_t1505_0_0_0},
	{"array", 1, 134222190, 0, &Array_t_0_0_0},
	{"dataSize", 2, 134222191, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::BlockRead(System.IO.BinaryReader,System.Array,System.Int32)
extern const MethodInfo ObjectReader_BlockRead_m8803_MethodInfo = 
{
	"BlockRead"/* name */
	, (methodPointerType)&ObjectReader_BlockRead_m8803/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54/* invoker_method */
	, ObjectReader_t1695_ObjectReader_BlockRead_m8803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadArrayOfObject_m8804_ParameterInfos[] = 
{
	{"reader", 0, 134222192, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222193, 0, &Int64_t753_1_0_2},
	{"array", 2, 134222194, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfObject(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfObject_m8804_MethodInfo = 
{
	"ReadArrayOfObject"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfObject_m8804/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadArrayOfObject_m8804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadArrayOfString_m8805_ParameterInfos[] = 
{
	{"reader", 0, 134222195, 0, &BinaryReader_t1505_0_0_0},
	{"objectId", 1, 134222196, 0, &Int64_t753_1_0_2},
	{"array", 2, 134222197, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadArrayOfString(System.IO.BinaryReader,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadArrayOfString_m8805_MethodInfo = 
{
	"ReadArrayOfString"/* name */
	, (methodPointerType)&ObjectReader_ReadArrayOfString_m8805/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadArrayOfString_m8805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int64_t753_1_0_2;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadSimpleArray_m8806_ParameterInfos[] = 
{
	{"reader", 0, 134222198, 0, &BinaryReader_t1505_0_0_0},
	{"elementType", 1, 134222199, 0, &Type_t_0_0_0},
	{"objectId", 2, 134222200, 0, &Int64_t753_1_0_2},
	{"val", 3, 134222201, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64U26_t2029_ObjectU26_t863 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadSimpleArray(System.IO.BinaryReader,System.Type,System.Int64&,System.Object&)
extern const MethodInfo ObjectReader_ReadSimpleArray_m8806_MethodInfo = 
{
	"ReadSimpleArray"/* name */
	, (methodPointerType)&ObjectReader_ReadSimpleArray_m8806/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int64U26_t2029_ObjectU26_t863/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadSimpleArray_m8806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadTypeMetadata_m8807_ParameterInfos[] = 
{
	{"reader", 0, 134222202, 0, &BinaryReader_t1505_0_0_0},
	{"isRuntimeObject", 1, 134222203, 0, &Boolean_t72_0_0_0},
	{"hasTypeInfo", 2, 134222204, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadTypeMetadata(System.IO.BinaryReader,System.Boolean,System.Boolean)
extern const MethodInfo ObjectReader_ReadTypeMetadata_m8807_MethodInfo = 
{
	"ReadTypeMetadata"/* name */
	, (methodPointerType)&ObjectReader_ReadTypeMetadata_m8807/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &TypeMetadata_t1692_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadTypeMetadata_m8807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadValue_m8808_ParameterInfos[] = 
{
	{"reader", 0, 134222205, 0, &BinaryReader_t1505_0_0_0},
	{"parentObject", 1, 134222206, 0, &Object_t_0_0_0},
	{"parentObjectId", 2, 134222207, 0, &Int64_t753_0_0_0},
	{"info", 3, 134222208, 0, &SerializationInfo_t725_0_0_0},
	{"valueType", 4, 134222209, 0, &Type_t_0_0_0},
	{"fieldName", 5, 134222210, 0, &String_t_0_0_0},
	{"memberInfo", 6, 134222211, 0, &MemberInfo_t_0_0_0},
	{"indices", 7, 134222212, 0, &Int32U5BU5D_t1082_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadValue(System.IO.BinaryReader,System.Object,System.Int64,System.Runtime.Serialization.SerializationInfo,System.Type,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_ReadValue_m8808_MethodInfo = 
{
	"ReadValue"/* name */
	, (methodPointerType)&ObjectReader_ReadValue_m8808/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadValue_m8808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_SetObjectValue_m8809_ParameterInfos[] = 
{
	{"parentObject", 0, 134222213, 0, &Object_t_0_0_0},
	{"fieldName", 1, 134222214, 0, &String_t_0_0_0},
	{"memberInfo", 2, 134222215, 0, &MemberInfo_t_0_0_0},
	{"info", 3, 134222216, 0, &SerializationInfo_t725_0_0_0},
	{"value", 4, 134222217, 0, &Object_t_0_0_0},
	{"valueType", 5, 134222218, 0, &Type_t_0_0_0},
	{"indices", 6, 134222219, 0, &Int32U5BU5D_t1082_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::SetObjectValue(System.Object,System.String,System.Reflection.MemberInfo,System.Runtime.Serialization.SerializationInfo,System.Object,System.Type,System.Int32[])
extern const MethodInfo ObjectReader_SetObjectValue_m8809_MethodInfo = 
{
	"SetObjectValue"/* name */
	, (methodPointerType)&ObjectReader_SetObjectValue_m8809/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_SetObjectValue_m8809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Int32U5BU5D_t1082_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_RecordFixup_m8810_ParameterInfos[] = 
{
	{"parentObjectId", 0, 134222220, 0, &Int64_t753_0_0_0},
	{"childObjectId", 1, 134222221, 0, &Int64_t753_0_0_0},
	{"parentObject", 2, 134222222, 0, &Object_t_0_0_0},
	{"info", 3, 134222223, 0, &SerializationInfo_t725_0_0_0},
	{"fieldName", 4, 134222224, 0, &String_t_0_0_0},
	{"memberInfo", 5, 134222225, 0, &MemberInfo_t_0_0_0},
	{"indices", 6, 134222226, 0, &Int32U5BU5D_t1082_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int64_t753_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.Formatters.Binary.ObjectReader::RecordFixup(System.Int64,System.Int64,System.Object,System.Runtime.Serialization.SerializationInfo,System.String,System.Reflection.MemberInfo,System.Int32[])
extern const MethodInfo ObjectReader_RecordFixup_m8810_MethodInfo = 
{
	"RecordFixup"/* name */
	, (methodPointerType)&ObjectReader_RecordFixup_m8810/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int64_t753_Int64_t753_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_RecordFixup_m8810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t753_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_GetDeserializationType_m8811_ParameterInfos[] = 
{
	{"assemblyId", 0, 134222227, 0, &Int64_t753_0_0_0},
	{"className", 1, 134222228, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::GetDeserializationType(System.Int64,System.String)
extern const MethodInfo ObjectReader_GetDeserializationType_m8811_MethodInfo = 
{
	"GetDeserializationType"/* name */
	, (methodPointerType)&ObjectReader_GetDeserializationType_m8811/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t753_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_GetDeserializationType_m8811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType TypeTag_t1686_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadType_m8812_ParameterInfos[] = 
{
	{"reader", 0, 134222229, 0, &BinaryReader_t1505_0_0_0},
	{"code", 1, 134222230, 0, &TypeTag_t1686_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Byte_t367 (const MethodInfo* method, void* obj, void** args);
// System.Type System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadType(System.IO.BinaryReader,System.Runtime.Serialization.Formatters.Binary.TypeTag)
extern const MethodInfo ObjectReader_ReadType_m8812_MethodInfo = 
{
	"ReadType"/* name */
	, (methodPointerType)&ObjectReader_ReadType_m8812/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Byte_t367/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadType_m8812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BinaryReader_t1505_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ObjectReader_t1695_ObjectReader_ReadPrimitiveTypeValue_m8813_ParameterInfos[] = 
{
	{"reader", 0, 134222231, 0, &BinaryReader_t1505_0_0_0},
	{"type", 1, 134222232, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.Formatters.Binary.ObjectReader::ReadPrimitiveTypeValue(System.IO.BinaryReader,System.Type)
extern const MethodInfo ObjectReader_ReadPrimitiveTypeValue_m8813_MethodInfo = 
{
	"ReadPrimitiveTypeValue"/* name */
	, (methodPointerType)&ObjectReader_ReadPrimitiveTypeValue_m8813/* method */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ObjectReader_t1695_ObjectReader_ReadPrimitiveTypeValue_m8813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectReader_t1695_MethodInfos[] =
{
	&ObjectReader__ctor_m8787_MethodInfo,
	&ObjectReader_ReadObjectGraph_m8788_MethodInfo,
	&ObjectReader_ReadObjectGraph_m8789_MethodInfo,
	&ObjectReader_ReadNextObject_m8790_MethodInfo,
	&ObjectReader_ReadNextObject_m8791_MethodInfo,
	&ObjectReader_get_CurrentObject_m8792_MethodInfo,
	&ObjectReader_ReadObject_m8793_MethodInfo,
	&ObjectReader_ReadAssembly_m8794_MethodInfo,
	&ObjectReader_ReadObjectInstance_m8795_MethodInfo,
	&ObjectReader_ReadRefTypeObjectInstance_m8796_MethodInfo,
	&ObjectReader_ReadObjectContent_m8797_MethodInfo,
	&ObjectReader_RegisterObject_m8798_MethodInfo,
	&ObjectReader_ReadStringIntance_m8799_MethodInfo,
	&ObjectReader_ReadGenericArray_m8800_MethodInfo,
	&ObjectReader_ReadBoxedPrimitiveTypeValue_m8801_MethodInfo,
	&ObjectReader_ReadArrayOfPrimitiveType_m8802_MethodInfo,
	&ObjectReader_BlockRead_m8803_MethodInfo,
	&ObjectReader_ReadArrayOfObject_m8804_MethodInfo,
	&ObjectReader_ReadArrayOfString_m8805_MethodInfo,
	&ObjectReader_ReadSimpleArray_m8806_MethodInfo,
	&ObjectReader_ReadTypeMetadata_m8807_MethodInfo,
	&ObjectReader_ReadValue_m8808_MethodInfo,
	&ObjectReader_SetObjectValue_m8809_MethodInfo,
	&ObjectReader_RecordFixup_m8810_MethodInfo,
	&ObjectReader_GetDeserializationType_m8811_MethodInfo,
	&ObjectReader_ReadType_m8812_MethodInfo,
	&ObjectReader_ReadPrimitiveTypeValue_m8813_MethodInfo,
	NULL
};
extern const MethodInfo ObjectReader_get_CurrentObject_m8792_MethodInfo;
static const PropertyInfo ObjectReader_t1695____CurrentObject_PropertyInfo = 
{
	&ObjectReader_t1695_il2cpp_TypeInfo/* parent */
	, "CurrentObject"/* name */
	, &ObjectReader_get_CurrentObject_m8792_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectReader_t1695_PropertyInfos[] =
{
	&ObjectReader_t1695____CurrentObject_PropertyInfo,
	NULL
};
static const Il2CppType* ObjectReader_t1695_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t1692_0_0_0,
	&ArrayNullFiller_t1693_0_0_0,
};
static const Il2CppMethodReference ObjectReader_t1695_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ObjectReader_t1695_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectReader_t1695_1_0_0;
struct ObjectReader_t1695;
const Il2CppTypeDefinitionMetadata ObjectReader_t1695_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t1695_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t1695_VTable/* vtableMethods */
	, ObjectReader_t1695_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1728/* fieldStart */

};
TypeInfo ObjectReader_t1695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, ObjectReader_t1695_MethodInfos/* methods */
	, ObjectReader_t1695_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectReader_t1695_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t1695_0_0_0/* byval_arg */
	, &ObjectReader_t1695_1_0_0/* this_arg */
	, &ObjectReader_t1695_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t1695)/* instance_size */
	, sizeof (ObjectReader_t1695)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t1696_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static const MethodInfo* FormatterAssemblyStyle_t1696_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterAssemblyStyle_t1696_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool FormatterAssemblyStyle_t1696_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t1696_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterAssemblyStyle_t1696_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t1696_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t1696_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, FormatterAssemblyStyle_t1696_VTable/* vtableMethods */
	, FormatterAssemblyStyle_t1696_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1740/* fieldStart */

};
TypeInfo FormatterAssemblyStyle_t1696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterAssemblyStyle_t1696_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 563/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t1696_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t1696_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t1696_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t1696)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t1696)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t1697_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static const MethodInfo* FormatterTypeStyle_t1697_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FormatterTypeStyle_t1697_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool FormatterTypeStyle_t1697_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t1697_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterTypeStyle_t1697_0_0_0;
extern const Il2CppType FormatterTypeStyle_t1697_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t1697_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t1697_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, FormatterTypeStyle_t1697_VTable/* vtableMethods */
	, FormatterTypeStyle_t1697_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1743/* fieldStart */

};
TypeInfo FormatterTypeStyle_t1697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, FormatterTypeStyle_t1697_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 564/* custom_attributes_cache */
	, &FormatterTypeStyle_t1697_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t1697_1_0_0/* this_arg */
	, &FormatterTypeStyle_t1697_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t1697)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t1697)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t1698_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static const MethodInfo* TypeFilterLevel_t1698_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeFilterLevel_t1698_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool TypeFilterLevel_t1698_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t1698_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilterLevel_t1698_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t1698_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t1698_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, TypeFilterLevel_t1698_VTable/* vtableMethods */
	, TypeFilterLevel_t1698_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1747/* fieldStart */

};
TypeInfo TypeFilterLevel_t1698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, TypeFilterLevel_t1698_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 565/* custom_attributes_cache */
	, &TypeFilterLevel_t1698_0_0_0/* byval_arg */
	, &TypeFilterLevel_t1698_1_0_0/* this_arg */
	, &TypeFilterLevel_t1698_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t1698)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t1698)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t1699_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.FormatterConverter::.ctor()
extern const MethodInfo FormatterConverter__ctor_m8814_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatterConverter__ctor_m8814/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_Convert_m8815_ParameterInfos[] = 
{
	{"value", 0, 134222234, 0, &Object_t_0_0_0},
	{"type", 1, 134222235, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo FormatterConverter_Convert_m8815_MethodInfo = 
{
	"Convert"/* name */
	, (methodPointerType)&FormatterConverter_Convert_m8815/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_Convert_m8815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_ToBoolean_m8816_ParameterInfos[] = 
{
	{"value", 0, 134222236, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.FormatterConverter::ToBoolean(System.Object)
extern const MethodInfo FormatterConverter_ToBoolean_m8816_MethodInfo = 
{
	"ToBoolean"/* name */
	, (methodPointerType)&FormatterConverter_ToBoolean_m8816/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_ToBoolean_m8816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_ToInt16_m8817_ParameterInfos[] = 
{
	{"value", 0, 134222237, 0, &Object_t_0_0_0},
};
extern const Il2CppType Int16_t448_0_0_0;
extern void* RuntimeInvoker_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.FormatterConverter::ToInt16(System.Object)
extern const MethodInfo FormatterConverter_ToInt16_m8817_MethodInfo = 
{
	"ToInt16"/* name */
	, (methodPointerType)&FormatterConverter_ToInt16_m8817/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t448_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t448_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_ToInt16_m8817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_ToInt32_m8818_ParameterInfos[] = 
{
	{"value", 0, 134222238, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.FormatterConverter::ToInt32(System.Object)
extern const MethodInfo FormatterConverter_ToInt32_m8818_MethodInfo = 
{
	"ToInt32"/* name */
	, (methodPointerType)&FormatterConverter_ToInt32_m8818/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_ToInt32_m8818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_ToInt64_m8819_ParameterInfos[] = 
{
	{"value", 0, 134222239, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.FormatterConverter::ToInt64(System.Object)
extern const MethodInfo FormatterConverter_ToInt64_m8819_MethodInfo = 
{
	"ToInt64"/* name */
	, (methodPointerType)&FormatterConverter_ToInt64_m8819/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_ToInt64_m8819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo FormatterConverter_t1699_FormatterConverter_ToString_m8820_ParameterInfos[] = 
{
	{"value", 0, 134222240, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.FormatterConverter::ToString(System.Object)
extern const MethodInfo FormatterConverter_ToString_m8820_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&FormatterConverter_ToString_m8820/* method */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterConverter_t1699_FormatterConverter_ToString_m8820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterConverter_t1699_MethodInfos[] =
{
	&FormatterConverter__ctor_m8814_MethodInfo,
	&FormatterConverter_Convert_m8815_MethodInfo,
	&FormatterConverter_ToBoolean_m8816_MethodInfo,
	&FormatterConverter_ToInt16_m8817_MethodInfo,
	&FormatterConverter_ToInt32_m8818_MethodInfo,
	&FormatterConverter_ToInt64_m8819_MethodInfo,
	&FormatterConverter_ToString_m8820_MethodInfo,
	NULL
};
extern const MethodInfo FormatterConverter_Convert_m8815_MethodInfo;
extern const MethodInfo FormatterConverter_ToBoolean_m8816_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt16_m8817_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt32_m8818_MethodInfo;
extern const MethodInfo FormatterConverter_ToInt64_m8819_MethodInfo;
extern const MethodInfo FormatterConverter_ToString_m8820_MethodInfo;
static const Il2CppMethodReference FormatterConverter_t1699_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&FormatterConverter_Convert_m8815_MethodInfo,
	&FormatterConverter_ToBoolean_m8816_MethodInfo,
	&FormatterConverter_ToInt16_m8817_MethodInfo,
	&FormatterConverter_ToInt32_m8818_MethodInfo,
	&FormatterConverter_ToInt64_m8819_MethodInfo,
	&FormatterConverter_ToString_m8820_MethodInfo,
};
static bool FormatterConverter_t1699_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormatterConverter_t1716_0_0_0;
static const Il2CppType* FormatterConverter_t1699_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t1716_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t1699_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t1716_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterConverter_t1699_0_0_0;
extern const Il2CppType FormatterConverter_t1699_1_0_0;
struct FormatterConverter_t1699;
const Il2CppTypeDefinitionMetadata FormatterConverter_t1699_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t1699_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t1699_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t1699_VTable/* vtableMethods */
	, FormatterConverter_t1699_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterConverter_t1699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterConverter_t1699_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterConverter_t1699_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 566/* custom_attributes_cache */
	, &FormatterConverter_t1699_0_0_0/* byval_arg */
	, &FormatterConverter_t1699_1_0_0/* this_arg */
	, &FormatterConverter_t1699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t1699)/* instance_size */
	, sizeof (FormatterConverter_t1699)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t1700_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t1700_FormatterServices_GetUninitializedObject_m8821_ParameterInfos[] = 
{
	{"type", 0, 134222241, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetUninitializedObject_m8821_MethodInfo = 
{
	"GetUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetUninitializedObject_m8821/* method */
	, &FormatterServices_t1700_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t1700_FormatterServices_GetUninitializedObject_m8821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo FormatterServices_t1700_FormatterServices_GetSafeUninitializedObject_m8822_ParameterInfos[] = 
{
	{"type", 0, 134222242, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
extern const MethodInfo FormatterServices_GetSafeUninitializedObject_m8822_MethodInfo = 
{
	"GetSafeUninitializedObject"/* name */
	, (methodPointerType)&FormatterServices_GetSafeUninitializedObject_m8822/* method */
	, &FormatterServices_t1700_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, FormatterServices_t1700_FormatterServices_GetSafeUninitializedObject_m8822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatterServices_t1700_MethodInfos[] =
{
	&FormatterServices_GetUninitializedObject_m8821_MethodInfo,
	&FormatterServices_GetSafeUninitializedObject_m8822_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatterServices_t1700_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool FormatterServices_t1700_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterServices_t1700_0_0_0;
extern const Il2CppType FormatterServices_t1700_1_0_0;
struct FormatterServices_t1700;
const Il2CppTypeDefinitionMetadata FormatterServices_t1700_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t1700_VTable/* vtableMethods */
	, FormatterServices_t1700_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FormatterServices_t1700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, FormatterServices_t1700_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatterServices_t1700_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 567/* custom_attributes_cache */
	, &FormatterServices_t1700_0_0_0/* byval_arg */
	, &FormatterServices_t1700_1_0_0/* this_arg */
	, &FormatterServices_t1700_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t1700)/* instance_size */
	, sizeof (FormatterServices_t1700)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t1206_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IDeserializationCallback_t1206_IDeserializationCallback_OnDeserialization_m10986_ParameterInfos[] = 
{
	{"sender", 0, 134222243, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object)
extern const MethodInfo IDeserializationCallback_OnDeserialization_m10986_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &IDeserializationCallback_t1206_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IDeserializationCallback_t1206_IDeserializationCallback_OnDeserialization_m10986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IDeserializationCallback_t1206_MethodInfos[] =
{
	&IDeserializationCallback_OnDeserialization_m10986_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDeserializationCallback_t1206_0_0_0;
extern const Il2CppType IDeserializationCallback_t1206_1_0_0;
struct IDeserializationCallback_t1206;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t1206_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IDeserializationCallback_t1206_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IDeserializationCallback_t1206_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IDeserializationCallback_t1206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 568/* custom_attributes_cache */
	, &IDeserializationCallback_t1206_0_0_0/* byval_arg */
	, &IDeserializationCallback_t1206_1_0_0/* this_arg */
	, &IDeserializationCallback_t1206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t2014_il2cpp_TypeInfo;
static const MethodInfo* IFormatter_t2014_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatter_t2014_1_0_0;
struct IFormatter_t2014;
const Il2CppTypeDefinitionMetadata IFormatter_t2014_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatter_t2014_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatter_t2014_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatter_t2014_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 569/* custom_attributes_cache */
	, &IFormatter_t2014_0_0_0/* byval_arg */
	, &IFormatter_t2014_1_0_0/* this_arg */
	, &IFormatter_t2014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t1716_il2cpp_TypeInfo;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_Convert_m10987_ParameterInfos[] = 
{
	{"value", 0, 134222244, 0, &Object_t_0_0_0},
	{"type", 1, 134222245, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IFormatterConverter::Convert(System.Object,System.Type)
extern const MethodInfo IFormatterConverter_Convert_m10987_MethodInfo = 
{
	"Convert"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_Convert_m10987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_ToBoolean_m10988_ParameterInfos[] = 
{
	{"value", 0, 134222246, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Runtime.Serialization.IFormatterConverter::ToBoolean(System.Object)
extern const MethodInfo IFormatterConverter_ToBoolean_m10988_MethodInfo = 
{
	"ToBoolean"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_ToBoolean_m10988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_ToInt16_m10989_ParameterInfos[] = 
{
	{"value", 0, 134222247, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int16_t448_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int16 System.Runtime.Serialization.IFormatterConverter::ToInt16(System.Object)
extern const MethodInfo IFormatterConverter_ToInt16_m10989_MethodInfo = 
{
	"ToInt16"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Int16_t448_0_0_0/* return_type */
	, RuntimeInvoker_Int16_t448_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_ToInt16_m10989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_ToInt32_m10990_ParameterInfos[] = 
{
	{"value", 0, 134222248, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Runtime.Serialization.IFormatterConverter::ToInt32(System.Object)
extern const MethodInfo IFormatterConverter_ToInt32_m10990_MethodInfo = 
{
	"ToInt32"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_ToInt32_m10990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_ToInt64_m10991_ParameterInfos[] = 
{
	{"value", 0, 134222249, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int64_t753_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Runtime.Serialization.IFormatterConverter::ToInt64(System.Object)
extern const MethodInfo IFormatterConverter_ToInt64_m10991_MethodInfo = 
{
	"ToInt64"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t753_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t753_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_ToInt64_m10991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo IFormatterConverter_t1716_IFormatterConverter_ToString_m10992_ParameterInfos[] = 
{
	{"value", 0, 134222250, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Runtime.Serialization.IFormatterConverter::ToString(System.Object)
extern const MethodInfo IFormatterConverter_ToString_m10992_MethodInfo = 
{
	"ToString"/* name */
	, NULL/* method */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatterConverter_t1716_IFormatterConverter_ToString_m10992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatterConverter_t1716_MethodInfos[] =
{
	&IFormatterConverter_Convert_m10987_MethodInfo,
	&IFormatterConverter_ToBoolean_m10988_MethodInfo,
	&IFormatterConverter_ToInt16_m10989_MethodInfo,
	&IFormatterConverter_ToInt32_m10990_MethodInfo,
	&IFormatterConverter_ToInt64_m10991_MethodInfo,
	&IFormatterConverter_ToString_m10992_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatterConverter_t1716_1_0_0;
struct IFormatterConverter_t1716;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t1716_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatterConverter_t1716_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IFormatterConverter_t1716_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatterConverter_t1716_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 570/* custom_attributes_cache */
	, &IFormatterConverter_t1716_0_0_0/* byval_arg */
	, &IFormatterConverter_t1716_1_0_0/* this_arg */
	, &IFormatterConverter_t1716_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t1941_il2cpp_TypeInfo;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo IObjectReference_t1941_IObjectReference_GetRealObject_m10993_ParameterInfos[] = 
{
	{"context", 0, 134222251, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Serialization.IObjectReference::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IObjectReference_GetRealObject_m10993_MethodInfo = 
{
	"GetRealObject"/* name */
	, NULL/* method */
	, &IObjectReference_t1941_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t726/* invoker_method */
	, IObjectReference_t1941_IObjectReference_GetRealObject_m10993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IObjectReference_t1941_MethodInfos[] =
{
	&IObjectReference_GetRealObject_m10993_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IObjectReference_t1941_1_0_0;
struct IObjectReference_t1941;
const Il2CppTypeDefinitionMetadata IObjectReference_t1941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IObjectReference_t1941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, IObjectReference_t1941_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IObjectReference_t1941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 571/* custom_attributes_cache */
	, &IObjectReference_t1941_0_0_0/* byval_arg */
	, &IObjectReference_t1941_1_0_0/* this_arg */
	, &IObjectReference_t1941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
