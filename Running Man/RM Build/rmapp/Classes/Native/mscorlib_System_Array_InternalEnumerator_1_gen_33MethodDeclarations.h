﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Camera>
struct InternalEnumerator_1_t2748;
// System.Object
struct Object_t;
// UnityEngine.Camera
struct Camera_t156;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15706(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2748 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15707(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2748 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::Dispose()
#define InternalEnumerator_1_Dispose_m15708(__this, method) (( void (*) (InternalEnumerator_1_t2748 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Camera>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15709(__this, method) (( bool (*) (InternalEnumerator_1_t2748 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Camera>::get_Current()
#define InternalEnumerator_1_get_Current_m15710(__this, method) (( Camera_t156 * (*) (InternalEnumerator_1_t2748 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
