﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallback
struct EncoderFallback_t1780;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1782;

// System.Void System.Text.EncoderFallback::.ctor()
extern "C" void EncoderFallback__ctor_m9330 (EncoderFallback_t1780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallback::.cctor()
extern "C" void EncoderFallback__cctor_m9331 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ExceptionFallback()
extern "C" EncoderFallback_t1780 * EncoderFallback_get_ExceptionFallback_m9332 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ReplacementFallback()
extern "C" EncoderFallback_t1780 * EncoderFallback_get_ReplacementFallback_m9333 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_StandardSafeFallback()
extern "C" EncoderFallback_t1780 * EncoderFallback_get_StandardSafeFallback_m9334 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderFallback::CreateFallbackBuffer()
