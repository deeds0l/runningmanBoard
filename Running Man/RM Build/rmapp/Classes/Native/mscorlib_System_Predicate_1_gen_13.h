﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.GameObject>
struct  Predicate_1_t2527  : public MulticastDelegate_t216
{
};
