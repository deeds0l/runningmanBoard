﻿#pragma once
#include <stdint.h>
// UnityEngine.Font
struct Font_t176;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Font>
struct  Action_1_t355  : public MulticastDelegate_t216
{
};
