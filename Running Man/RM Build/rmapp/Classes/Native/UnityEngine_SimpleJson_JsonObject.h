﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t620;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.JsonObject
struct  JsonObject_t621  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> SimpleJson.JsonObject::_members
	Dictionary_2_t620 * ____members_0;
};
