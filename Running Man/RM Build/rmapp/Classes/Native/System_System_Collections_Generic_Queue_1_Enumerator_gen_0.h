﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Queue`1<UnityEngine.AudioSource>
struct Queue_1_t7;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>
struct  Enumerator_t2463 
{
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::q
	Queue_1_t7 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.AudioSource>::ver
	int32_t ___ver_2;
};
