﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t2874;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2865;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17441_gshared (ShimEnumerator_t2874 * __this, Dictionary_2_t2865 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17441(__this, ___host, method) (( void (*) (ShimEnumerator_t2874 *, Dictionary_2_t2865 *, const MethodInfo*))ShimEnumerator__ctor_m17441_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17442_gshared (ShimEnumerator_t2874 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17442(__this, method) (( bool (*) (ShimEnumerator_t2874 *, const MethodInfo*))ShimEnumerator_MoveNext_m17442_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t1147  ShimEnumerator_get_Entry_m17443_gshared (ShimEnumerator_t2874 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17443(__this, method) (( DictionaryEntry_t1147  (*) (ShimEnumerator_t2874 *, const MethodInfo*))ShimEnumerator_get_Entry_m17443_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17444_gshared (ShimEnumerator_t2874 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17444(__this, method) (( Object_t * (*) (ShimEnumerator_t2874 *, const MethodInfo*))ShimEnumerator_get_Key_m17444_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17445_gshared (ShimEnumerator_t2874 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17445(__this, method) (( Object_t * (*) (ShimEnumerator_t2874 *, const MethodInfo*))ShimEnumerator_get_Value_m17445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17446_gshared (ShimEnumerator_t2874 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17446(__this, method) (( Object_t * (*) (ShimEnumerator_t2874 *, const MethodInfo*))ShimEnumerator_get_Current_m17446_gshared)(__this, method)
