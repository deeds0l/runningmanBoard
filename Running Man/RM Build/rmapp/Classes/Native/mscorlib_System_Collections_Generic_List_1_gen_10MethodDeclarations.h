﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t354;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t223;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Text>
struct IEnumerator_1_t3095;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.Text>
struct ICollection_1_t3098;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Text>
struct IEnumerable_1_t3099;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Text>
struct ReadOnlyCollection_1_t2587;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t2585;
// System.Predicate`1<UnityEngine.UI.Text>
struct Predicate_1_t2588;
// System.Comparison`1<UnityEngine.UI.Text>
struct Comparison_1_t2590;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m1606(__this, method) (( void (*) (List_1_t354 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.ctor(System.Int32)
#define List_1__ctor_m13558(__this, ___capacity, method) (( void (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::.cctor()
#define List_1__cctor_m13559(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13560(__this, method) (( Object_t* (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13561(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t354 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13562(__this, method) (( Object_t * (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13563(__this, ___item, method) (( int32_t (*) (List_1_t354 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13564(__this, ___item, method) (( bool (*) (List_1_t354 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13565(__this, ___item, method) (( int32_t (*) (List_1_t354 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13566(__this, ___index, ___item, method) (( void (*) (List_1_t354 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13567(__this, ___item, method) (( void (*) (List_1_t354 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13568(__this, method) (( bool (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13569(__this, method) (( bool (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m13570(__this, method) (( Object_t * (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m13571(__this, method) (( bool (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m13572(__this, method) (( bool (*) (List_1_t354 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m13573(__this, ___index, method) (( Object_t * (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m13574(__this, ___index, ___value, method) (( void (*) (List_1_t354 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Add(T)
#define List_1_Add_m13575(__this, ___item, method) (( void (*) (List_1_t354 *, Text_t223 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m13576(__this, ___newCount, method) (( void (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m13577(__this, ___collection, method) (( void (*) (List_1_t354 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m13578(__this, ___enumerable, method) (( void (*) (List_1_t354 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m13579(__this, ___collection, method) (( void (*) (List_1_t354 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::AsReadOnly()
#define List_1_AsReadOnly_m13580(__this, method) (( ReadOnlyCollection_1_t2587 * (*) (List_1_t354 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Clear()
#define List_1_Clear_m13581(__this, method) (( void (*) (List_1_t354 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Contains(T)
#define List_1_Contains_m13582(__this, ___item, method) (( bool (*) (List_1_t354 *, Text_t223 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m13583(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t354 *, TextU5BU5D_t2585*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::Find(System.Predicate`1<T>)
#define List_1_Find_m13584(__this, ___match, method) (( Text_t223 * (*) (List_1_t354 *, Predicate_1_t2588 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m13585(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2588 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m13586(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t354 *, int32_t, int32_t, Predicate_1_t2588 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Text>::GetEnumerator()
#define List_1_GetEnumerator_m13587(__this, method) (( Enumerator_t2589  (*) (List_1_t354 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::IndexOf(T)
#define List_1_IndexOf_m13588(__this, ___item, method) (( int32_t (*) (List_1_t354 *, Text_t223 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13589(__this, ___start, ___delta, method) (( void (*) (List_1_t354 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m13590(__this, ___index, method) (( void (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Insert(System.Int32,T)
#define List_1_Insert_m13591(__this, ___index, ___item, method) (( void (*) (List_1_t354 *, int32_t, Text_t223 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m13592(__this, ___collection, method) (( void (*) (List_1_t354 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Text>::Remove(T)
#define List_1_Remove_m13593(__this, ___item, method) (( bool (*) (List_1_t354 *, Text_t223 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m13594(__this, ___match, method) (( int32_t (*) (List_1_t354 *, Predicate_1_t2588 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m13595(__this, ___index, method) (( void (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Reverse()
#define List_1_Reverse_m13596(__this, method) (( void (*) (List_1_t354 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort()
#define List_1_Sort_m13597(__this, method) (( void (*) (List_1_t354 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m13598(__this, ___comparison, method) (( void (*) (List_1_t354 *, Comparison_1_t2590 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Text>::ToArray()
#define List_1_ToArray_m13599(__this, method) (( TextU5BU5D_t2585* (*) (List_1_t354 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::TrimExcess()
#define List_1_TrimExcess_m13600(__this, method) (( void (*) (List_1_t354 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Capacity()
#define List_1_get_Capacity_m13601(__this, method) (( int32_t (*) (List_1_t354 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m13602(__this, ___value, method) (( void (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Count()
#define List_1_get_Count_m13603(__this, method) (( int32_t (*) (List_1_t354 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Text>::get_Item(System.Int32)
#define List_1_get_Item_m13604(__this, ___index, method) (( Text_t223 * (*) (List_1_t354 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Text>::set_Item(System.Int32,T)
#define List_1_set_Item_m13605(__this, ___index, ___value, method) (( void (*) (List_1_t354 *, int32_t, Text_t223 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
