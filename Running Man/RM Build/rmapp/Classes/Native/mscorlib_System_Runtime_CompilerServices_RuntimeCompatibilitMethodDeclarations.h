﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_t56;

// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
extern "C" void RuntimeCompatibilityAttribute__ctor_m152 (RuntimeCompatibilityAttribute_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153 (RuntimeCompatibilityAttribute_t56 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
