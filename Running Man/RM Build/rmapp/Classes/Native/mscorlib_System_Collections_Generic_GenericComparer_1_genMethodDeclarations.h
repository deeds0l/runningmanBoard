﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t1944;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m10414_gshared (GenericComparer_1_t1944 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m10414(__this, method) (( void (*) (GenericComparer_1_t1944 *, const MethodInfo*))GenericComparer_1__ctor_m10414_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m18690_gshared (GenericComparer_1_t1944 * __this, DateTime_t508  ___x, DateTime_t508  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m18690(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1944 *, DateTime_t508 , DateTime_t508 , const MethodInfo*))GenericComparer_1_Compare_m18690_gshared)(__this, ___x, ___y, method)
