﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
extern TypeInfo AudioManager_t2_il2cpp_TypeInfo;
// AudioManager
#include "AssemblyU2DCSharp_AudioManager.h"
extern TypeInfo SOUNDTYPE_t4_il2cpp_TypeInfo;
// AudioPooler/SOUNDTYPE
#include "AssemblyU2DCSharp_AudioPooler_SOUNDTYPE.h"
extern TypeInfo objectSource_t8_il2cpp_TypeInfo;
// AudioPooler/objectSource
#include "AssemblyU2DCSharp_AudioPooler_objectSource.h"
extern TypeInfo U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo;
// AudioPooler/<DisableMusic>c__Iterator0
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableMusicU3Ec__Iterator0.h"
extern TypeInfo U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo;
// AudioPooler/<DisableSound>c__Iterator1
#include "AssemblyU2DCSharp_AudioPooler_U3CDisableSoundU3Ec__Iterator1.h"
extern TypeInfo U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo;
// AudioPooler/<GetAudioSource>c__AnonStorey6
#include "AssemblyU2DCSharp_AudioPooler_U3CGetAudioSourceU3Ec__AnonSto.h"
extern TypeInfo U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo;
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
#include "AssemblyU2DCSharp_AudioPooler_U3CReturnAudioSourceU3Ec__Anon.h"
extern TypeInfo U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo;
// AudioPooler/<ClearAudioSource>c__AnonStorey8
#include "AssemblyU2DCSharp_AudioPooler_U3CClearAudioSourceU3Ec__AnonS.h"
extern TypeInfo U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo;
// AudioPooler/<ClearMusic>c__AnonStorey9
#include "AssemblyU2DCSharp_AudioPooler_U3CClearMusicU3Ec__AnonStorey9.h"
extern TypeInfo U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo;
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
#include "AssemblyU2DCSharp_AudioPooler_U3CClearSoundEffectsU3Ec__Anon.h"
extern TypeInfo U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo;
// AudioPooler/<SearchMusic>c__AnonStoreyB
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchMusicU3Ec__AnonStorey.h"
extern TypeInfo U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo;
// AudioPooler/<SearchSound>c__AnonStoreyC
#include "AssemblyU2DCSharp_AudioPooler_U3CSearchSoundU3Ec__AnonStorey.h"
extern TypeInfo AudioPooler_t10_il2cpp_TypeInfo;
// AudioPooler
#include "AssemblyU2DCSharp_AudioPooler.h"
extern TypeInfo BaseCanvas_1_t60_il2cpp_TypeInfo;
extern TypeInfo ICanvasInteractable_t62_il2cpp_TypeInfo;
extern TypeInfo U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo;
extern TypeInfo U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo;
extern TypeInfo U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo;
extern TypeInfo BaseOverlay_1_t63_il2cpp_TypeInfo;
extern TypeInfo BoardController_t23_il2cpp_TypeInfo;
// BoardController
#include "AssemblyU2DCSharp_BoardController.h"
extern TypeInfo DebugUtil_t24_il2cpp_TypeInfo;
// DebugUtil
#include "AssemblyU2DCSharp_DebugUtil.h"
extern TypeInfo DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo;
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetup.h"
extern TypeInfo MonoManager_1_t69_il2cpp_TypeInfo;
extern TypeInfo NonMonoManager_1_t70_il2cpp_TypeInfo;
extern TypeInfo U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo;
// Tools/<WaitForRealTime>c__Iterator5
#include "AssemblyU2DCSharp_Tools_U3CWaitForRealTimeU3Ec__Iterator5.h"
extern TypeInfo Tools_t27_il2cpp_TypeInfo;
// Tools
#include "AssemblyU2DCSharp_Tools.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[28] = 
{
	&U3CModuleU3E_t0_il2cpp_TypeInfo,
	&AudioManager_t2_il2cpp_TypeInfo,
	&SOUNDTYPE_t4_il2cpp_TypeInfo,
	&objectSource_t8_il2cpp_TypeInfo,
	&U3CDisableMusicU3Ec__Iterator0_t11_il2cpp_TypeInfo,
	&U3CDisableSoundU3Ec__Iterator1_t12_il2cpp_TypeInfo,
	&U3CGetAudioSourceU3Ec__AnonStorey6_t13_il2cpp_TypeInfo,
	&U3CReturnAudioSourceU3Ec__AnonStorey7_t14_il2cpp_TypeInfo,
	&U3CClearAudioSourceU3Ec__AnonStorey8_t15_il2cpp_TypeInfo,
	&U3CClearMusicU3Ec__AnonStorey9_t16_il2cpp_TypeInfo,
	&U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_il2cpp_TypeInfo,
	&U3CSearchMusicU3Ec__AnonStoreyB_t18_il2cpp_TypeInfo,
	&U3CSearchSoundU3Ec__AnonStoreyC_t19_il2cpp_TypeInfo,
	&AudioPooler_t10_il2cpp_TypeInfo,
	&BaseCanvas_1_t60_il2cpp_TypeInfo,
	&ICanvasInteractable_t62_il2cpp_TypeInfo,
	&U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_il2cpp_TypeInfo,
	&U3CFadeInCoroutineU3Ec__Iterator3_t65_il2cpp_TypeInfo,
	&U3CFadeOutCoroutineU3Ec__Iterator4_t66_il2cpp_TypeInfo,
	&BaseOverlay_1_t63_il2cpp_TypeInfo,
	&BoardController_t23_il2cpp_TypeInfo,
	&DebugUtil_t24_il2cpp_TypeInfo,
	&DefiniteCasinoCanvasSetup_t25_il2cpp_TypeInfo,
	&MonoManager_1_t69_il2cpp_TypeInfo,
	&NonMonoManager_1_t70_il2cpp_TypeInfo,
	&U3CWaitForRealTimeU3Ec__Iterator5_t26_il2cpp_TypeInfo,
	&Tools_t27_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", NULL, NULL, NULL, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[57];
static const char* s_StringTable[39] = 
{
	"m_listClips",
	"value__",
	"BGM",
	"SFX",
	"m_audioContainer",
	"m_spawnedMusicSource",
	"m_musicSourceQueue",
	"m_spawnedSoundSource",
	"m_soundSourceQueue",
	"p_audioSource",
	"p_bisLooping",
	"$PC",
	"$current",
	"<$>p_audioSource",
	"<$>p_bisLooping",
	"<>f__this",
	"p_audioContainer",
	"p_currentAudioSource",
	"key",
	"instance",
	"m_currentAudioSource",
	"m_currentAudioClip",
	"newObjectSource",
	"m_audioPreloadCount",
	"m_bstoppedMusic",
	"m_bstoppedSound",
	"m_objectSourceList",
	"s_instance",
	"m_canvas",
	"m_canvasGroup",
	"p_delay",
	"<$>p_delay",
	"<t>__0",
	"duration",
	"<$>duration",
	"m_coroutine",
	"DUTIL_UNITY",
	"ENABLE_DEBUG",
	"<pauseEndTime>__0",
};
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
static const Il2CppFieldDefinition s_FieldTable[68] = 
{
	{ 0, 49, offsetof(AudioManager_t2, ___m_listClips_3), 0 } ,
	{ 1, 51, offsetof(SOUNDTYPE_t4, ___value___1) + sizeof(Object_t), 0 } ,
	{ 2, 52, 0, 0 } ,
	{ 3, 52, 0, 0 } ,
	{ 4, 59, offsetof(objectSource_t8, ___m_audioContainer_0), 0 } ,
	{ 5, 60, offsetof(objectSource_t8, ___m_spawnedMusicSource_1), 0 } ,
	{ 6, 61, offsetof(objectSource_t8, ___m_musicSourceQueue_2), 0 } ,
	{ 7, 60, offsetof(objectSource_t8, ___m_spawnedSoundSource_3), 0 } ,
	{ 8, 61, offsetof(objectSource_t8, ___m_soundSourceQueue_4), 0 } ,
	{ 9, 63, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___p_audioSource_0), 0 } ,
	{ 10, 64, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___p_bisLooping_1), 0 } ,
	{ 11, 65, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___U24PC_2), 0 } ,
	{ 12, 66, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___U24current_3), 0 } ,
	{ 13, 63, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___U3CU24U3Ep_audioSource_4), 0 } ,
	{ 14, 64, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___U3CU24U3Ep_bisLooping_5), 0 } ,
	{ 15, 67, offsetof(U3CDisableMusicU3Ec__Iterator0_t11, ___U3CU3Ef__this_6), 0 } ,
	{ 9, 63, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___p_audioSource_0), 0 } ,
	{ 10, 64, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___p_bisLooping_1), 0 } ,
	{ 11, 65, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___U24PC_2), 0 } ,
	{ 12, 66, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___U24current_3), 0 } ,
	{ 13, 63, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___U3CU24U3Ep_audioSource_4), 0 } ,
	{ 14, 64, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___U3CU24U3Ep_bisLooping_5), 0 } ,
	{ 15, 67, offsetof(U3CDisableSoundU3Ec__Iterator1_t12, ___U3CU3Ef__this_6), 0 } ,
	{ 16, 72, offsetof(U3CGetAudioSourceU3Ec__AnonStorey6_t13, ___p_audioContainer_0), 0 } ,
	{ 17, 63, offsetof(U3CReturnAudioSourceU3Ec__AnonStorey7_t14, ___p_currentAudioSource_0), 0 } ,
	{ 16, 72, offsetof(U3CClearAudioSourceU3Ec__AnonStorey8_t15, ___p_audioContainer_0), 0 } ,
	{ 16, 72, offsetof(U3CClearMusicU3Ec__AnonStorey9_t16, ___p_audioContainer_0), 0 } ,
	{ 16, 72, offsetof(U3CClearSoundEffectsU3Ec__AnonStoreyA_t17, ___p_audioContainer_0), 0 } ,
	{ 18, 78, offsetof(U3CSearchMusicU3Ec__AnonStoreyB_t18, ___key_0), 0 } ,
	{ 18, 78, offsetof(U3CSearchSoundU3Ec__AnonStoreyC_t19, ___key_0), 0 } ,
	{ 19, 81, offsetof(AudioPooler_t10_StaticFields, ___instance_2), 0 } ,
	{ 20, 82, offsetof(AudioPooler_t10, ___m_currentAudioSource_3), 0 } ,
	{ 21, 83, offsetof(AudioPooler_t10, ___m_currentAudioClip_4), 0 } ,
	{ 22, 84, offsetof(AudioPooler_t10, ___newObjectSource_5), 0 } ,
	{ 23, 85, offsetof(AudioPooler_t10, ___m_audioPreloadCount_6), 2 } ,
	{ 24, 86, offsetof(AudioPooler_t10, ___m_bstoppedMusic_7), 0 } ,
	{ 25, 86, offsetof(AudioPooler_t10, ___m_bstoppedSound_8), 0 } ,
	{ 26, 87, offsetof(AudioPooler_t10, ___m_objectSourceList_9), 0 } ,
	{ 27, 92, 0, 0 } ,
	{ 28, 93, 0, 0 } ,
	{ 29, 94, 0, 0 } ,
	{ 30, 101, 0, 0 } ,
	{ 11, 65, 0, 0 } ,
	{ 12, 66, 0, 0 } ,
	{ 31, 101, 0, 0 } ,
	{ 15, 102, 0, 0 } ,
	{ 32, 101, 0, 0 } ,
	{ 33, 101, 0, 0 } ,
	{ 11, 65, 0, 0 } ,
	{ 12, 66, 0, 0 } ,
	{ 34, 101, 0, 0 } ,
	{ 15, 107, 0, 0 } ,
	{ 32, 101, 0, 0 } ,
	{ 33, 101, 0, 0 } ,
	{ 11, 65, 0, 0 } ,
	{ 12, 66, 0, 0 } ,
	{ 34, 101, 0, 0 } ,
	{ 15, 111, 0, 0 } ,
	{ 35, 116, 0, 0 } ,
	{ 36, 126, 0, 0 } ,
	{ 37, 127, offsetof(DebugUtil_t24_StaticFields, ___ENABLE_DEBUG_1), 0 } ,
	{ 27, 132, 0, 0 } ,
	{ 27, 137, 0, 0 } ,
	{ 30, 101, offsetof(U3CWaitForRealTimeU3Ec__Iterator5_t26, ___p_delay_0), 0 } ,
	{ 38, 101, offsetof(U3CWaitForRealTimeU3Ec__Iterator5_t26, ___U3CpauseEndTimeU3E__0_1), 0 } ,
	{ 11, 65, offsetof(U3CWaitForRealTimeU3Ec__Iterator5_t26, ___U24PC_2), 0 } ,
	{ 12, 66, offsetof(U3CWaitForRealTimeU3Ec__Iterator5_t26, ___U24current_3), 0 } ,
	{ 31, 101, offsetof(U3CWaitForRealTimeU3Ec__Iterator5_t26, ___U3CU24U3Ep_delay_4), 0 } ,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
static const Il2CppFieldDefaultValue s_DefaultValues[3] = 
{
	{ 2, 34, 0 },
	{ 3, 34, 4 },
	{ 59, 5, 8 },
};
static const uint8_t s_DefaultValueDataTable[22] = 
{
	0x00,
	0x00,
	0x00,
	0x00,
	0x01,
	0x00,
	0x00,
	0x00,
	0x0A,
	0x00,
	0x00,
	0x00,
	0x3C,
	0x55,
	0x6E,
	0x69,
	0x74,
	0x79,
	0x3E,
	0x20,
	0x3A,
	0x20,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	27,
	NULL,
	s_StringTable,
	39,
	s_FieldTable,
	68,
	s_DefaultValues,
	3,
	s_DefaultValueDataTable,
	22,
	57,
	NULL,
	g_AssemblyU2DCSharp_Assembly_AttributeGenerators,
};
