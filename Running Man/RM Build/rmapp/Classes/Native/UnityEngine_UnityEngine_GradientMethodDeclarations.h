﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t502;
struct Gradient_t502_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m2294 (Gradient_t502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m2295 (Gradient_t502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m2296 (Gradient_t502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m2297 (Gradient_t502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t502_marshal(const Gradient_t502& unmarshaled, Gradient_t502_marshaled& marshaled);
void Gradient_t502_marshal_back(const Gradient_t502_marshaled& marshaled, Gradient_t502& unmarshaled);
void Gradient_t502_marshal_cleanup(Gradient_t502_marshaled& marshaled);
