﻿#pragma once
#include <stdint.h>
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t245;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.RectTransformUtility
struct  RectTransformUtility_t360  : public Object_t
{
};
struct RectTransformUtility_t360_StaticFields{
	// UnityEngine.Vector3[] UnityEngine.RectTransformUtility::s_Corners
	Vector3U5BU5D_t245* ___s_Corners_0;
};
