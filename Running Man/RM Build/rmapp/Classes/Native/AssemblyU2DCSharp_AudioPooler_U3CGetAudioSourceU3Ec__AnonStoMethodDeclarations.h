﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<GetAudioSource>c__AnonStorey6
struct U3CGetAudioSourceU3Ec__AnonStorey6_t13;
// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/<GetAudioSource>c__AnonStorey6::.ctor()
extern "C" void U3CGetAudioSourceU3Ec__AnonStorey6__ctor_m20 (U3CGetAudioSourceU3Ec__AnonStorey6_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<GetAudioSource>c__AnonStorey6::<>m__0(AudioPooler/objectSource)
extern "C" bool U3CGetAudioSourceU3Ec__AnonStorey6_U3CU3Em__0_m21 (U3CGetAudioSourceU3Ec__AnonStorey6_t13 * __this, objectSource_t8 * ___audio, const MethodInfo* method) IL2CPP_METHOD_ATTR;
