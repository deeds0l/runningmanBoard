﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
struct Enumerator_t2570;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2563;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13169_gshared (Enumerator_t2570 * __this, Dictionary_2_t2563 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m13169(__this, ___host, method) (( void (*) (Enumerator_t2570 *, Dictionary_2_t2563 *, const MethodInfo*))Enumerator__ctor_m13169_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13170_gshared (Enumerator_t2570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13170(__this, method) (( Object_t * (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13170_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m13171_gshared (Enumerator_t2570 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13171(__this, method) (( void (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_Dispose_m13171_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13172_gshared (Enumerator_t2570 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13172(__this, method) (( bool (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_MoveNext_m13172_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13173_gshared (Enumerator_t2570 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13173(__this, method) (( Object_t * (*) (Enumerator_t2570 *, const MethodInfo*))Enumerator_get_Current_m13173_gshared)(__this, method)
