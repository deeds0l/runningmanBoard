﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t108;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct  Predicate_1_t2513  : public MulticastDelegate_t216
{
};
