﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>
struct Enumerator_t746;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t620;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"
#define Enumerator__ctor_m16897(__this, ___dictionary, method) (( void (*) (Enumerator_t746 *, Dictionary_2_t620 *, const MethodInfo*))Enumerator__ctor_m13505_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16898(__this, method) (( Object_t * (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16899(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16900(__this, method) (( Object_t * (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16901(__this, method) (( Object_t * (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::MoveNext()
#define Enumerator_MoveNext_m16902(__this, method) (( bool (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_MoveNext_m13510_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_Current()
#define Enumerator_get_Current_m16903(__this, method) (( KeyValuePair_2_t718  (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_get_Current_m13511_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16904(__this, method) (( String_t* (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_get_CurrentKey_m13512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16905(__this, method) (( Object_t * (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_get_CurrentValue_m13513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyState()
#define Enumerator_VerifyState_m16906(__this, method) (( void (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_VerifyState_m13514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16907(__this, method) (( void (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_VerifyCurrent_m13515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::Dispose()
#define Enumerator_Dispose_m16908(__this, method) (( void (*) (Enumerator_t746 *, const MethodInfo*))Enumerator_Dispose_m13516_gshared)(__this, method)
