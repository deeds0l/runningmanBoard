﻿#pragma once
#include <stdint.h>
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t641;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t642;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t643;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t644  : public Object_t
{
};
struct AttributeHelperEngine_t644_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t641* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t642* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t643* ____requireComponentArray_2;
};
