﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
struct Enumerator_t2475;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t101;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t100;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m11874(__this, ___l, method) (( void (*) (Enumerator_t2475 *, List_1_t100 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11875(__this, method) (( Object_t * (*) (Enumerator_t2475 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::Dispose()
#define Enumerator_Dispose_m11876(__this, method) (( void (*) (Enumerator_t2475 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::VerifyState()
#define Enumerator_VerifyState_m11877(__this, method) (( void (*) (Enumerator_t2475 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::MoveNext()
#define Enumerator_MoveNext_m11878(__this, method) (( bool (*) (Enumerator_t2475 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::get_Current()
#define Enumerator_get_Current_m11879(__this, method) (( BaseInputModule_t101 * (*) (Enumerator_t2475 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
