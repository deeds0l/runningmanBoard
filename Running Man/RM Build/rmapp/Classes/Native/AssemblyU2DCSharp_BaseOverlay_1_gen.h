﻿#pragma once
#include <stdint.h>
// UnityEngine.Coroutine
struct Coroutine_t228;
struct Coroutine_t228_marshaled;
// BaseCanvas`1<System.Object>
#include "AssemblyU2DCSharp_BaseCanvas_1_gen.h"
// BaseOverlay`1<System.Object>
struct  BaseOverlay_1_t2465  : public BaseCanvas_1_t2464
{
	// UnityEngine.Coroutine BaseOverlay`1<System.Object>::m_coroutine
	Coroutine_t228 * ___m_coroutine_5;
};
