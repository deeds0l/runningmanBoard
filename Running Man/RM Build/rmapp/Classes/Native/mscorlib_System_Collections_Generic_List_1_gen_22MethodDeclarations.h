﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t516;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t515;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GUILayoutEntry>
struct IEnumerator_1_t3155;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.GUILayoutEntry>
struct ICollection_1_t3156;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GUILayoutEntry>
struct IEnumerable_1_t3157;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.GUILayoutEntry>
struct ReadOnlyCollection_1_t2721;
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2719;
// System.Predicate`1<UnityEngine.GUILayoutEntry>
struct Predicate_1_t2722;
// System.Comparison`1<UnityEngine.GUILayoutEntry>
struct Comparison_1_t2723;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m3387(__this, method) (( void (*) (List_1_t516 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.ctor(System.Int32)
#define List_1__ctor_m15133(__this, ___capacity, method) (( void (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::.cctor()
#define List_1__cctor_m15134(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15135(__this, method) (( Object_t* (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15136(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t516 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15137(__this, method) (( Object_t * (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15138(__this, ___item, method) (( int32_t (*) (List_1_t516 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15139(__this, ___item, method) (( bool (*) (List_1_t516 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15140(__this, ___item, method) (( int32_t (*) (List_1_t516 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15141(__this, ___index, ___item, method) (( void (*) (List_1_t516 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15142(__this, ___item, method) (( void (*) (List_1_t516 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15143(__this, method) (( bool (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15144(__this, method) (( bool (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15145(__this, method) (( Object_t * (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15146(__this, method) (( bool (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15147(__this, method) (( bool (*) (List_1_t516 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15148(__this, ___index, method) (( Object_t * (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15149(__this, ___index, ___value, method) (( void (*) (List_1_t516 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(T)
#define List_1_Add_m15150(__this, ___item, method) (( void (*) (List_1_t516 *, GUILayoutEntry_t515 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15151(__this, ___newCount, method) (( void (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15152(__this, ___collection, method) (( void (*) (List_1_t516 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15153(__this, ___enumerable, method) (( void (*) (List_1_t516 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15154(__this, ___collection, method) (( void (*) (List_1_t516 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::AsReadOnly()
#define List_1_AsReadOnly_m15155(__this, method) (( ReadOnlyCollection_1_t2721 * (*) (List_1_t516 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Clear()
#define List_1_Clear_m15156(__this, method) (( void (*) (List_1_t516 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Contains(T)
#define List_1_Contains_m15157(__this, ___item, method) (( bool (*) (List_1_t516 *, GUILayoutEntry_t515 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15158(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t516 *, GUILayoutEntryU5BU5D_t2719*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Find(System.Predicate`1<T>)
#define List_1_Find_m15159(__this, ___match, method) (( GUILayoutEntry_t515 * (*) (List_1_t516 *, Predicate_1_t2722 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15160(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2722 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15161(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t516 *, int32_t, int32_t, Predicate_1_t2722 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::GetEnumerator()
#define List_1_GetEnumerator_m3384(__this, method) (( Enumerator_t733  (*) (List_1_t516 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::IndexOf(T)
#define List_1_IndexOf_m15162(__this, ___item, method) (( int32_t (*) (List_1_t516 *, GUILayoutEntry_t515 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15163(__this, ___start, ___delta, method) (( void (*) (List_1_t516 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15164(__this, ___index, method) (( void (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Insert(System.Int32,T)
#define List_1_Insert_m15165(__this, ___index, ___item, method) (( void (*) (List_1_t516 *, int32_t, GUILayoutEntry_t515 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15166(__this, ___collection, method) (( void (*) (List_1_t516 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Remove(T)
#define List_1_Remove_m15167(__this, ___item, method) (( bool (*) (List_1_t516 *, GUILayoutEntry_t515 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m15168(__this, ___match, method) (( int32_t (*) (List_1_t516 *, Predicate_1_t2722 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15169(__this, ___index, method) (( void (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Reverse()
#define List_1_Reverse_m15170(__this, method) (( void (*) (List_1_t516 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Sort()
#define List_1_Sort_m15171(__this, method) (( void (*) (List_1_t516 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15172(__this, ___comparison, method) (( void (*) (List_1_t516 *, Comparison_1_t2723 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::ToArray()
#define List_1_ToArray_m15173(__this, method) (( GUILayoutEntryU5BU5D_t2719* (*) (List_1_t516 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::TrimExcess()
#define List_1_TrimExcess_m15174(__this, method) (( void (*) (List_1_t516 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Capacity()
#define List_1_get_Capacity_m15175(__this, method) (( int32_t (*) (List_1_t516 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15176(__this, ___value, method) (( void (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count()
#define List_1_get_Count_m15177(__this, method) (( int32_t (*) (List_1_t516 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32)
#define List_1_get_Item_m15178(__this, ___index, method) (( GUILayoutEntry_t515 * (*) (List_1_t516 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m15179(__this, ___index, ___value, method) (( void (*) (List_1_t516 *, int32_t, GUILayoutEntry_t515 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
