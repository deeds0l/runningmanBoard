﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t758;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t3218;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t3219;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t631;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t757;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t3220;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t3221;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m3476(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t758 *, ThreadSafeDictionaryValueFactory_2_t757 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m16932_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16933(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16934_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m16935(__this, ___key, method) (( ConstructorDelegate_t631 * (*) (ThreadSafeDictionary_2_t758 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m16936_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m16937(__this, ___key, method) (( ConstructorDelegate_t631 * (*) (ThreadSafeDictionary_2_t758 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m16938_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m16939(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t758 *, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m16940_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m16941(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m16942_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m16943(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t758 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m16944_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m16945(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t758 *, Type_t *, ConstructorDelegate_t631 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m16946_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m16947(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m16948_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m16949(__this, ___key, method) (( ConstructorDelegate_t631 * (*) (ThreadSafeDictionary_2_t758 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m16950_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m16951(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t758 *, Type_t *, ConstructorDelegate_t631 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m16952_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m16953(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t758 *, KeyValuePair_2_t2847 , const MethodInfo*))ThreadSafeDictionary_2_Add_m16954_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m16955(__this, method) (( void (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m16956_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m16957(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t758 *, KeyValuePair_2_t2847 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m16958_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m16959(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t758 *, KeyValuePair_2U5BU5D_t3220*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m16960_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m16961(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m16962_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m16963(__this, method) (( bool (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m16964_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m16965(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t758 *, KeyValuePair_2_t2847 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m16966_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m16967(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t758 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m16968_gshared)(__this, method)
