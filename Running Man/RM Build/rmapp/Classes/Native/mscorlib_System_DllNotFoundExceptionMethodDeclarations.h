﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DllNotFoundException
struct DllNotFoundException_t1835;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.DllNotFoundException::.ctor()
extern "C" void DllNotFoundException__ctor_m9958 (DllNotFoundException_t1835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DllNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DllNotFoundException__ctor_m9959 (DllNotFoundException_t1835 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
