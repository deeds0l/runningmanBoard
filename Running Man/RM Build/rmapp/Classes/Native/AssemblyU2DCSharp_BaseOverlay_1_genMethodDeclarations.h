﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseOverlay`1<System.Object>
struct BaseOverlay_1_t2465;
// System.Collections.IEnumerator
struct IEnumerator_t28;

// System.Void BaseOverlay`1<System.Object>::.ctor()
extern "C" void BaseOverlay_1__ctor_m11751_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1__ctor_m11751(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1__ctor_m11751_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::Awake()
extern "C" void BaseOverlay_1_Awake_m11752_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_Awake_m11752(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_Awake_m11752_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::Open()
extern "C" void BaseOverlay_1_Open_m11753_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_Open_m11753(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_Open_m11753_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::Close()
extern "C" void BaseOverlay_1_Close_m11754_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_Close_m11754(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_Close_m11754_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::OnClose()
extern "C" void BaseOverlay_1_OnClose_m11755_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_OnClose_m11755(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_OnClose_m11755_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::CancelClose()
extern "C" void BaseOverlay_1_CancelClose_m11756_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_CancelClose_m11756(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_CancelClose_m11756_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::CloseWithDelay(System.Single)
extern "C" void BaseOverlay_1_CloseWithDelay_m11757_gshared (BaseOverlay_1_t2465 * __this, float ___p_delay, const MethodInfo* method);
#define BaseOverlay_1_CloseWithDelay_m11757(__this, ___p_delay, method) (( void (*) (BaseOverlay_1_t2465 *, float, const MethodInfo*))BaseOverlay_1_CloseWithDelay_m11757_gshared)(__this, ___p_delay, method)
// System.Collections.IEnumerator BaseOverlay`1<System.Object>::CloseWithDelayCoroutine(System.Single)
extern "C" Object_t * BaseOverlay_1_CloseWithDelayCoroutine_m11758_gshared (BaseOverlay_1_t2465 * __this, float ___p_delay, const MethodInfo* method);
#define BaseOverlay_1_CloseWithDelayCoroutine_m11758(__this, ___p_delay, method) (( Object_t * (*) (BaseOverlay_1_t2465 *, float, const MethodInfo*))BaseOverlay_1_CloseWithDelayCoroutine_m11758_gshared)(__this, ___p_delay, method)
// System.Void BaseOverlay`1<System.Object>::SetSortingOrder(System.Int32)
extern "C" void BaseOverlay_1_SetSortingOrder_m11759_gshared (BaseOverlay_1_t2465 * __this, int32_t ___p_order, const MethodInfo* method);
#define BaseOverlay_1_SetSortingOrder_m11759(__this, ___p_order, method) (( void (*) (BaseOverlay_1_t2465 *, int32_t, const MethodInfo*))BaseOverlay_1_SetSortingOrder_m11759_gshared)(__this, ___p_order, method)
// System.Void BaseOverlay`1<System.Object>::FadeIn()
extern "C" void BaseOverlay_1_FadeIn_m11760_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_FadeIn_m11760(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_FadeIn_m11760_gshared)(__this, method)
// System.Void BaseOverlay`1<System.Object>::FadeOut()
extern "C" void BaseOverlay_1_FadeOut_m11761_gshared (BaseOverlay_1_t2465 * __this, const MethodInfo* method);
#define BaseOverlay_1_FadeOut_m11761(__this, method) (( void (*) (BaseOverlay_1_t2465 *, const MethodInfo*))BaseOverlay_1_FadeOut_m11761_gshared)(__this, method)
// System.Collections.IEnumerator BaseOverlay`1<System.Object>::FadeInCoroutine(System.Single)
extern "C" Object_t * BaseOverlay_1_FadeInCoroutine_m11762_gshared (BaseOverlay_1_t2465 * __this, float ___duration, const MethodInfo* method);
#define BaseOverlay_1_FadeInCoroutine_m11762(__this, ___duration, method) (( Object_t * (*) (BaseOverlay_1_t2465 *, float, const MethodInfo*))BaseOverlay_1_FadeInCoroutine_m11762_gshared)(__this, ___duration, method)
// System.Collections.IEnumerator BaseOverlay`1<System.Object>::FadeOutCoroutine(System.Single)
extern "C" Object_t * BaseOverlay_1_FadeOutCoroutine_m11763_gshared (BaseOverlay_1_t2465 * __this, float ___duration, const MethodInfo* method);
#define BaseOverlay_1_FadeOutCoroutine_m11763(__this, ___duration, method) (( Object_t * (*) (BaseOverlay_1_t2465 *, float, const MethodInfo*))BaseOverlay_1_FadeOutCoroutine_m11763_gshared)(__this, ___duration, method)
