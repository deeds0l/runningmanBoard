﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.MRUList/Node
struct Node_t1090;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MRUList
struct  MRUList_t1089  : public Object_t
{
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::head
	Node_t1090 * ___head_0;
	// System.Text.RegularExpressions.MRUList/Node System.Text.RegularExpressions.MRUList::tail
	Node_t1090 * ___tail_1;
};
