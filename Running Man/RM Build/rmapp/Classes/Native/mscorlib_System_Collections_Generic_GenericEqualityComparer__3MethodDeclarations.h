﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t2550;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m12896_gshared (GenericEqualityComparer_1_t2550 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m12896(__this, method) (( void (*) (GenericEqualityComparer_1_t2550 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m12896_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m12897_gshared (GenericEqualityComparer_1_t2550 * __this, int32_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m12897(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2550 *, int32_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m12897_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m12898_gshared (GenericEqualityComparer_1_t2550 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m12898(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2550 *, int32_t, int32_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m12898_gshared)(__this, ___x, ___y, method)
