﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.String>
struct InternalEnumerator_1_t2469;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.String>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m11782(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2469 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.String>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11783(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2469 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.String>::Dispose()
#define InternalEnumerator_1_Dispose_m11784(__this, method) (( void (*) (InternalEnumerator_1_t2469 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.String>::MoveNext()
#define InternalEnumerator_1_MoveNext_m11785(__this, method) (( bool (*) (InternalEnumerator_1_t2469 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.String>::get_Current()
#define InternalEnumerator_1_get_Current_m11786(__this, method) (( String_t* (*) (InternalEnumerator_1_t2469 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
