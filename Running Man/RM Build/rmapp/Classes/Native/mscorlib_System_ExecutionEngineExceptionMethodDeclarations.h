﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ExecutionEngineException
struct ExecutionEngineException_t1846;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ExecutionEngineException::.ctor()
extern "C" void ExecutionEngineException__ctor_m9991 (ExecutionEngineException_t1846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ExecutionEngineException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ExecutionEngineException__ctor_m9992 (ExecutionEngineException_t1846 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
