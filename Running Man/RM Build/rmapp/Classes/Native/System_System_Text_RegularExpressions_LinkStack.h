﻿#pragma once
#include <stdint.h>
// System.Collections.Stack
struct Stack_t696;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t1097  : public LinkRef_t1093
{
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t696 * ___stack_0;
};
