﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t144;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t3051;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t3052;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t3053;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2497;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2493;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2501;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t103;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m1505_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1__ctor_m1505(__this, method) (( void (*) (List_1_t144 *, const MethodInfo*))List_1__ctor_m1505_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12128_gshared (List_1_t144 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12128(__this, ___capacity, method) (( void (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1__ctor_m12128_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m12129_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12129(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12129_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12130_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12130(__this, method) (( Object_t* (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12130_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12131_gshared (List_1_t144 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12131(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t144 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12131_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12132_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12132(__this, method) (( Object_t * (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12132_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12133_gshared (List_1_t144 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12133(__this, ___item, method) (( int32_t (*) (List_1_t144 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12133_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12134_gshared (List_1_t144 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12134(__this, ___item, method) (( bool (*) (List_1_t144 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12134_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12135_gshared (List_1_t144 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12135(__this, ___item, method) (( int32_t (*) (List_1_t144 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12135_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12136_gshared (List_1_t144 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12136(__this, ___index, ___item, method) (( void (*) (List_1_t144 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12136_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12137_gshared (List_1_t144 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12137(__this, ___item, method) (( void (*) (List_1_t144 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12137_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12138_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12138(__this, method) (( bool (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12139_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12139(__this, method) (( bool (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12139_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12140_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12140(__this, method) (( Object_t * (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12140_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12141_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12141(__this, method) (( bool (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12141_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12142_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12142(__this, method) (( bool (*) (List_1_t144 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12142_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12143_gshared (List_1_t144 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12143(__this, ___index, method) (( Object_t * (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12143_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12144_gshared (List_1_t144 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12144(__this, ___index, ___value, method) (( void (*) (List_1_t144 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12144_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m12145_gshared (List_1_t144 * __this, RaycastResult_t137  ___item, const MethodInfo* method);
#define List_1_Add_m12145(__this, ___item, method) (( void (*) (List_1_t144 *, RaycastResult_t137 , const MethodInfo*))List_1_Add_m12145_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12146_gshared (List_1_t144 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12146(__this, ___newCount, method) (( void (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12146_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12147_gshared (List_1_t144 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12147(__this, ___collection, method) (( void (*) (List_1_t144 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12147_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12148_gshared (List_1_t144 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12148(__this, ___enumerable, method) (( void (*) (List_1_t144 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12148_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m12149_gshared (List_1_t144 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m12149(__this, ___collection, method) (( void (*) (List_1_t144 *, Object_t*, const MethodInfo*))List_1_AddRange_m12149_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2497 * List_1_AsReadOnly_m12150_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12150(__this, method) (( ReadOnlyCollection_1_t2497 * (*) (List_1_t144 *, const MethodInfo*))List_1_AsReadOnly_m12150_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m12151_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_Clear_m12151(__this, method) (( void (*) (List_1_t144 *, const MethodInfo*))List_1_Clear_m12151_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m12152_gshared (List_1_t144 * __this, RaycastResult_t137  ___item, const MethodInfo* method);
#define List_1_Contains_m12152(__this, ___item, method) (( bool (*) (List_1_t144 *, RaycastResult_t137 , const MethodInfo*))List_1_Contains_m12152_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12153_gshared (List_1_t144 * __this, RaycastResultU5BU5D_t2493* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12153(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t144 *, RaycastResultU5BU5D_t2493*, int32_t, const MethodInfo*))List_1_CopyTo_m12153_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t137  List_1_Find_m12154_gshared (List_1_t144 * __this, Predicate_1_t2501 * ___match, const MethodInfo* method);
#define List_1_Find_m12154(__this, ___match, method) (( RaycastResult_t137  (*) (List_1_t144 *, Predicate_1_t2501 *, const MethodInfo*))List_1_Find_m12154_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12155_gshared (Object_t * __this /* static, unused */, Predicate_1_t2501 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12155(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2501 *, const MethodInfo*))List_1_CheckMatch_m12155_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12156_gshared (List_1_t144 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2501 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12156(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t144 *, int32_t, int32_t, Predicate_1_t2501 *, const MethodInfo*))List_1_GetIndex_m12156_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2495  List_1_GetEnumerator_m12157_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12157(__this, method) (( Enumerator_t2495  (*) (List_1_t144 *, const MethodInfo*))List_1_GetEnumerator_m12157_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12158_gshared (List_1_t144 * __this, RaycastResult_t137  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12158(__this, ___item, method) (( int32_t (*) (List_1_t144 *, RaycastResult_t137 , const MethodInfo*))List_1_IndexOf_m12158_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12159_gshared (List_1_t144 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12159(__this, ___start, ___delta, method) (( void (*) (List_1_t144 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12159_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12160_gshared (List_1_t144 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12160(__this, ___index, method) (( void (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12160_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12161_gshared (List_1_t144 * __this, int32_t ___index, RaycastResult_t137  ___item, const MethodInfo* method);
#define List_1_Insert_m12161(__this, ___index, ___item, method) (( void (*) (List_1_t144 *, int32_t, RaycastResult_t137 , const MethodInfo*))List_1_Insert_m12161_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12162_gshared (List_1_t144 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12162(__this, ___collection, method) (( void (*) (List_1_t144 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12162_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m12163_gshared (List_1_t144 * __this, RaycastResult_t137  ___item, const MethodInfo* method);
#define List_1_Remove_m12163(__this, ___item, method) (( bool (*) (List_1_t144 *, RaycastResult_t137 , const MethodInfo*))List_1_Remove_m12163_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12164_gshared (List_1_t144 * __this, Predicate_1_t2501 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12164(__this, ___match, method) (( int32_t (*) (List_1_t144 *, Predicate_1_t2501 *, const MethodInfo*))List_1_RemoveAll_m12164_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12165_gshared (List_1_t144 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12165(__this, ___index, method) (( void (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12165_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m12166_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_Reverse_m12166(__this, method) (( void (*) (List_1_t144 *, const MethodInfo*))List_1_Reverse_m12166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m12167_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_Sort_m12167(__this, method) (( void (*) (List_1_t144 *, const MethodInfo*))List_1_Sort_m12167_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1460_gshared (List_1_t144 * __this, Comparison_1_t103 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1460(__this, ___comparison, method) (( void (*) (List_1_t144 *, Comparison_1_t103 *, const MethodInfo*))List_1_Sort_m1460_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2493* List_1_ToArray_m12168_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_ToArray_m12168(__this, method) (( RaycastResultU5BU5D_t2493* (*) (List_1_t144 *, const MethodInfo*))List_1_ToArray_m12168_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m12169_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12169(__this, method) (( void (*) (List_1_t144 *, const MethodInfo*))List_1_TrimExcess_m12169_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12170_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12170(__this, method) (( int32_t (*) (List_1_t144 *, const MethodInfo*))List_1_get_Capacity_m12170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12171_gshared (List_1_t144 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12171(__this, ___value, method) (( void (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12171_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m12172_gshared (List_1_t144 * __this, const MethodInfo* method);
#define List_1_get_Count_m12172(__this, method) (( int32_t (*) (List_1_t144 *, const MethodInfo*))List_1_get_Count_m12172_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t137  List_1_get_Item_m12173_gshared (List_1_t144 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12173(__this, ___index, method) (( RaycastResult_t137  (*) (List_1_t144 *, int32_t, const MethodInfo*))List_1_get_Item_m12173_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12174_gshared (List_1_t144 * __this, int32_t ___index, RaycastResult_t137  ___value, const MethodInfo* method);
#define List_1_set_Item_m12174(__this, ___index, ___value, method) (( void (*) (List_1_t144 *, int32_t, RaycastResult_t137 , const MethodInfo*))List_1_set_Item_m12174_gshared)(__this, ___index, ___value, method)
