﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2963;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1083;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18363_gshared (Enumerator_t2963 * __this, Dictionary_2_t1083 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m18363(__this, ___host, method) (( void (*) (Enumerator_t2963 *, Dictionary_2_t1083 *, const MethodInfo*))Enumerator__ctor_m18363_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18364_gshared (Enumerator_t2963 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18364(__this, method) (( Object_t * (*) (Enumerator_t2963 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18364_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m18365_gshared (Enumerator_t2963 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18365(__this, method) (( void (*) (Enumerator_t2963 *, const MethodInfo*))Enumerator_Dispose_m18365_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18366_gshared (Enumerator_t2963 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18366(__this, method) (( bool (*) (Enumerator_t2963 *, const MethodInfo*))Enumerator_MoveNext_m18366_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m18367_gshared (Enumerator_t2963 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18367(__this, method) (( int32_t (*) (Enumerator_t2963 *, const MethodInfo*))Enumerator_get_Current_m18367_gshared)(__this, method)
