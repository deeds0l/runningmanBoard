﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Uri
struct Uri_t617;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t618  : public MonoBehaviour_t22
{
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t617 * ___m_BaseUri_3;
};
