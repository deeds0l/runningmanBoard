﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t985;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
struct  X509ExtensionCollection_t1034  : public Object_t
{
	// System.Collections.ArrayList System.Security.Cryptography.X509Certificates.X509ExtensionCollection::_list
	ArrayList_t985 * ____list_0;
};
