﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t581;
struct AnimationCurve_t581_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t707;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m2872 (AnimationCurve_t581 * __this, KeyframeU5BU5D_t707* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m2873 (AnimationCurve_t581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m2874 (AnimationCurve_t581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m2875 (AnimationCurve_t581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m2876 (AnimationCurve_t581 * __this, KeyframeU5BU5D_t707* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t581_marshal(const AnimationCurve_t581& unmarshaled, AnimationCurve_t581_marshaled& marshaled);
void AnimationCurve_t581_marshal_back(const AnimationCurve_t581_marshaled& marshaled, AnimationCurve_t581& unmarshaled);
void AnimationCurve_t581_marshal_cleanup(AnimationCurve_t581_marshaled& marshaled);
