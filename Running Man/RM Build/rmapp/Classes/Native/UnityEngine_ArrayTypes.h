﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.AudioClip[]
// UnityEngine.AudioClip[]
struct  AudioClipU5BU5D_t2427  : public Array_t
{
};
// UnityEngine.AudioSource[]
// UnityEngine.AudioSource[]
struct  AudioSourceU5BU5D_t2456  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t2488  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t2519  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t2524  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t347  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t351  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t2581  : public Array_t
{
};
struct FontU5BU5D_t2581_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t224  : public Array_t
{
};
struct UIVertexU5BU5D_t224_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t2620  : public Array_t
{
};
struct CanvasU5BU5D_t2620_StaticFields{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t202  : public Array_t
{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t709  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t708  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t245  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t2662  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t2684  : public Array_t
{
};
struct RectTransformU5BU5D_t2684_StaticFields{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t697  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t728  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t730  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t663  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t657  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t486  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t487  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t2703  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t699  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t729  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t700  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t731  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t703  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t2713  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t2719  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t2719_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t524  : public Array_t
{
};
struct GUIStyleU5BU5D_t524_StaticFields{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t667  : public Array_t
{
};
struct CameraU5BU5D_t667_StaticFields{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t3347  : public Array_t
{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t556  : public Array_t
{
};
struct DisplayU5BU5D_t556_StaticFields{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t2751  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t707  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct  MatchDirectConnectInfoU5BU5D_t2799  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDesc[]
// UnityEngine.Networking.Match.MatchDesc[]
struct  MatchDescU5BU5D_t2805  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkID[]
// UnityEngine.Networking.Types.NetworkID[]
struct  NetworkIDU5BU5D_t2811  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkAccessToken[]
// UnityEngine.Networking.Types.NetworkAccessToken[]
struct  NetworkAccessTokenU5BU5D_t2812  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
struct  ConstructorDelegateU5BU5D_t2844  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
struct  GetDelegateU5BU5D_t2856  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t641  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t642  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t643  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t666  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t2896  : public Array_t
{
};
struct EventU5BU5D_t2896_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t2897  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t2916  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t2921  : public Array_t
{
};
