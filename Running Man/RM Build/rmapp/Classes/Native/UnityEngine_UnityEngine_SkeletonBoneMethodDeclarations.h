﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t584;
struct SkeletonBone_t584_marshaled;

void SkeletonBone_t584_marshal(const SkeletonBone_t584& unmarshaled, SkeletonBone_t584_marshaled& marshaled);
void SkeletonBone_t584_marshal_back(const SkeletonBone_t584_marshaled& marshaled, SkeletonBone_t584& unmarshaled);
void SkeletonBone_t584_marshal_cleanup(SkeletonBone_t584_marshaled& marshaled);
