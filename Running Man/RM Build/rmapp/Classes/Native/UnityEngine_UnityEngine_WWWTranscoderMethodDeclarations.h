﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WWWTranscoder
struct WWWTranscoder_t548;
// System.Byte[]
struct ByteU5BU5D_t546;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t705;

// System.Void UnityEngine.WWWTranscoder::.cctor()
extern "C" void WWWTranscoder__cctor_m2678 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern "C" ByteU5BU5D_t546* WWWTranscoder_Byte2Hex_m2679 (Object_t * __this /* static, unused */, uint8_t ___b, ByteU5BU5D_t546* ___hexChars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern "C" ByteU5BU5D_t546* WWWTranscoder_URLEncode_m2680 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___toEncode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern "C" String_t* WWWTranscoder_QPEncode_m2681 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t705 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern "C" ByteU5BU5D_t546* WWWTranscoder_Encode_m2682 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___input, uint8_t ___escapeChar, uint8_t ___space, ByteU5BU5D_t546* ___forbidden, bool ___uppercase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C" bool WWWTranscoder_ByteArrayContains_m2683 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___array, uint8_t ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern "C" bool WWWTranscoder_SevenBitClean_m2684 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t705 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C" bool WWWTranscoder_SevenBitClean_m2685 (Object_t * __this /* static, unused */, ByteU5BU5D_t546* ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
