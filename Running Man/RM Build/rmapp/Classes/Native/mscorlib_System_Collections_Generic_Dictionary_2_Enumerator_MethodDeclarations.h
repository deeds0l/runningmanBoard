﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Enumerator_t344;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t143;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t149;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m12936(__this, ___dictionary, method) (( void (*) (Enumerator_t344 *, Dictionary_2_t149 *, const MethodInfo*))Enumerator__ctor_m12833_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12937(__this, method) (( Object_t * (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12938(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12939(__this, method) (( Object_t * (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12940(__this, method) (( Object_t * (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1532(__this, method) (( bool (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_MoveNext_m12838_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1529(__this, method) (( KeyValuePair_2_t343  (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_get_Current_m12839_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m12941(__this, method) (( int32_t (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_get_CurrentKey_m12840_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m12942(__this, method) (( PointerEventData_t143 * (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_get_CurrentValue_m12841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m12943(__this, method) (( void (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_VerifyState_m12842_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m12944(__this, method) (( void (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_VerifyCurrent_m12843_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m12945(__this, method) (( void (*) (Enumerator_t344 *, const MethodInfo*))Enumerator_Dispose_m12844_gshared)(__this, method)
