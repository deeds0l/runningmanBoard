﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$24
struct U24ArrayTypeU2424_t1889;
struct U24ArrayTypeU2424_t1889_marshaled;

void U24ArrayTypeU2424_t1889_marshal(const U24ArrayTypeU2424_t1889& unmarshaled, U24ArrayTypeU2424_t1889_marshaled& marshaled);
void U24ArrayTypeU2424_t1889_marshal_back(const U24ArrayTypeU2424_t1889_marshaled& marshaled, U24ArrayTypeU2424_t1889& unmarshaled);
void U24ArrayTypeU2424_t1889_marshal_cleanup(U24ArrayTypeU2424_t1889_marshaled& marshaled);
