﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1083;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3065;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t2958;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2962;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2532;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t3281;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3282;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t3283;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1146;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m18253_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m18253(__this, method) (( void (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2__ctor_m18253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m18254_gshared (Dictionary_2_t1083 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m18254(__this, ___comparer, method) (( void (*) (Dictionary_2_t1083 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m18254_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m18255_gshared (Dictionary_2_t1083 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m18255(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1083 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m18255_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m18256_gshared (Dictionary_2_t1083 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m18256(__this, ___capacity, method) (( void (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m18256_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m18257_gshared (Dictionary_2_t1083 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m18257(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1083 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m18257_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m18258_gshared (Dictionary_2_t1083 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m18258(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1083 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2__ctor_m18258_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18259_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18259(__this, method) (( Object_t* (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18259_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18260_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18260(__this, method) (( Object_t* (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m18261_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18261(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m18261_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m18262_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18262(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1083 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m18262_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m18263_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m18263(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1083 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m18263_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m18264_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m18264(__this, ___key, method) (( bool (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m18264_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m18265_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m18265(__this, ___key, method) (( void (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m18265_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18266_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18266(__this, method) (( bool (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18266_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18267_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18267(__this, method) (( Object_t * (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18267_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18268_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18268(__this, method) (( bool (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18269_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2_t2956  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18269(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1083 *, KeyValuePair_2_t2956 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18269_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18270_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2_t2956  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18270(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1083 *, KeyValuePair_2_t2956 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18270_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18271_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2U5BU5D_t3282* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18271(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1083 *, KeyValuePair_2U5BU5D_t3282*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18271_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18272_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2_t2956  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18272(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1083 *, KeyValuePair_2_t2956 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18272_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m18273_gshared (Dictionary_2_t1083 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18273(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1083 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m18273_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18274_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18274(__this, method) (( Object_t * (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18274_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18275_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18275(__this, method) (( Object_t* (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18275_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18276_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18276(__this, method) (( Object_t * (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18276_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m18277_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m18277(__this, method) (( int32_t (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_get_Count_m18277_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m18278_gshared (Dictionary_2_t1083 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m18278(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m18278_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m18279_gshared (Dictionary_2_t1083 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m18279(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1083 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m18279_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m18280_gshared (Dictionary_2_t1083 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m18280(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1083 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m18280_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m18281_gshared (Dictionary_2_t1083 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m18281(__this, ___size, method) (( void (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m18281_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m18282_gshared (Dictionary_2_t1083 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m18282(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1083 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m18282_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2956  Dictionary_2_make_pair_m18283_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m18283(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2956  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m18283_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m18284_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m18284(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m18284_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m18285_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m18285(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m18285_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m18286_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2U5BU5D_t3282* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m18286(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1083 *, KeyValuePair_2U5BU5D_t3282*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m18286_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m18287_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m18287(__this, method) (( void (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_Resize_m18287_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m18288_gshared (Dictionary_2_t1083 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m18288(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1083 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m18288_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m18289_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m18289(__this, method) (( void (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_Clear_m18289_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m18290_gshared (Dictionary_2_t1083 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m18290(__this, ___key, method) (( bool (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m18290_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m18291_gshared (Dictionary_2_t1083 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m18291(__this, ___value, method) (( bool (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m18291_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m18292_gshared (Dictionary_2_t1083 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m18292(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1083 *, SerializationInfo_t725 *, StreamingContext_t726 , const MethodInfo*))Dictionary_2_GetObjectData_m18292_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m18293_gshared (Dictionary_2_t1083 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m18293(__this, ___sender, method) (( void (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m18293_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m18294_gshared (Dictionary_2_t1083 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m18294(__this, ___key, method) (( bool (*) (Dictionary_2_t1083 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m18294_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m18295_gshared (Dictionary_2_t1083 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m18295(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1083 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m18295_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t2958 * Dictionary_2_get_Keys_m18296_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m18296(__this, method) (( KeyCollection_t2958 * (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_get_Keys_m18296_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t2962 * Dictionary_2_get_Values_m18297_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m18297(__this, method) (( ValueCollection_t2962 * (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_get_Values_m18297_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m18298_gshared (Dictionary_2_t1083 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m18298(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m18298_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m18299_gshared (Dictionary_2_t1083 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m18299(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1083 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m18299_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m18300_gshared (Dictionary_2_t1083 * __this, KeyValuePair_2_t2956  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m18300(__this, ___pair, method) (( bool (*) (Dictionary_2_t1083 *, KeyValuePair_2_t2956 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m18300_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2960  Dictionary_2_GetEnumerator_m18301_gshared (Dictionary_2_t1083 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m18301(__this, method) (( Enumerator_t2960  (*) (Dictionary_2_t1083 *, const MethodInfo*))Dictionary_2_GetEnumerator_m18301_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1147  Dictionary_2_U3CCopyToU3Em__0_m18302_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m18302(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1147  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m18302_gshared)(__this /* static, unused */, ___key, ___value, method)
