﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t29;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t638  : public Object_t
{
};
struct ReflectionUtils_t638_StaticFields{
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t29* ___EmptyObjects_0;
};
