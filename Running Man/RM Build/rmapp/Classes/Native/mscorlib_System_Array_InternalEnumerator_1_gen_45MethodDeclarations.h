﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>
struct InternalEnumerator_1_t2883;
// System.Object
struct Object_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17515(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2883 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17516(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2883 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m17517(__this, method) (( void (*) (InternalEnumerator_1_t2883 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17518(__this, method) (( bool (*) (InternalEnumerator_1_t2883 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m17519(__this, method) (( PropertyInfo_t * (*) (InternalEnumerator_1_t2883 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
