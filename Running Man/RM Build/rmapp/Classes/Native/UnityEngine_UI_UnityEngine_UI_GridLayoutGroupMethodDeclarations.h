﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t279;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"

// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern "C" void GridLayoutGroup__ctor_m1281 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern "C" int32_t GridLayoutGroup_get_startCorner_m1282 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern "C" void GridLayoutGroup_set_startCorner_m1283 (GridLayoutGroup_t279 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern "C" int32_t GridLayoutGroup_get_startAxis_m1284 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern "C" void GridLayoutGroup_set_startAxis_m1285 (GridLayoutGroup_t279 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern "C" Vector2_t53  GridLayoutGroup_get_cellSize_m1286 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern "C" void GridLayoutGroup_set_cellSize_m1287 (GridLayoutGroup_t279 * __this, Vector2_t53  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern "C" Vector2_t53  GridLayoutGroup_get_spacing_m1288 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern "C" void GridLayoutGroup_set_spacing_m1289 (GridLayoutGroup_t279 * __this, Vector2_t53  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern "C" int32_t GridLayoutGroup_get_constraint_m1290 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern "C" void GridLayoutGroup_set_constraint_m1291 (GridLayoutGroup_t279 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern "C" int32_t GridLayoutGroup_get_constraintCount_m1292 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern "C" void GridLayoutGroup_set_constraintCount_m1293 (GridLayoutGroup_t279 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m1294 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m1295 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m1296 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern "C" void GridLayoutGroup_SetLayoutVertical_m1297 (GridLayoutGroup_t279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m1298 (GridLayoutGroup_t279 * __this, int32_t ___axis, const MethodInfo* method) IL2CPP_METHOD_ATTR;
