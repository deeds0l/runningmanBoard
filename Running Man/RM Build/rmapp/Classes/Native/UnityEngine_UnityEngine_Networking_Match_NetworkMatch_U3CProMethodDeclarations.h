﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t2837;
// System.Object
struct Object_t;

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m16798_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m16798(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m16798_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m16799_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m16799(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m16799_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m16800_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m16800(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m16800_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m16801_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m16801(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m16801_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m16802_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m16802(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2837 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m16802_gshared)(__this, method)
