﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1168;
struct RSAParameters_t1168_marshaled;

void RSAParameters_t1168_marshal(const RSAParameters_t1168& unmarshaled, RSAParameters_t1168_marshaled& marshaled);
void RSAParameters_t1168_marshal_back(const RSAParameters_t1168_marshaled& marshaled, RSAParameters_t1168& unmarshaled);
void RSAParameters_t1168_marshal_cleanup(RSAParameters_t1168_marshaled& marshaled);
