﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
struct KeyValuePair_2_t2726;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t513;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15275(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2726 *, String_t*, GUIStyle_t513 *, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m15276(__this, method) (( String_t* (*) (KeyValuePair_2_t2726 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15277(__this, ___value, method) (( void (*) (KeyValuePair_2_t2726 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m15278(__this, method) (( GUIStyle_t513 * (*) (KeyValuePair_2_t2726 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15279(__this, ___value, method) (( void (*) (KeyValuePair_2_t2726 *, GUIStyle_t513 *, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m15280(__this, method) (( String_t* (*) (KeyValuePair_2_t2726 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
