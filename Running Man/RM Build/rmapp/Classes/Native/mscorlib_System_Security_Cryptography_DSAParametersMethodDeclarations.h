﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1170;
struct DSAParameters_t1170_marshaled;

void DSAParameters_t1170_marshal(const DSAParameters_t1170& unmarshaled, DSAParameters_t1170_marshaled& marshaled);
void DSAParameters_t1170_marshal_back(const DSAParameters_t1170_marshaled& marshaled, DSAParameters_t1170& unmarshaled);
void DSAParameters_t1170_marshal_cleanup(DSAParameters_t1170_marshaled& marshaled);
