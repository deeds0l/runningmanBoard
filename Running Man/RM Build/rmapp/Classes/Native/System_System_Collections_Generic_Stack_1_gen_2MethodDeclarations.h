﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t2608;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t3105;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t227;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m13707(__this, method) (( void (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1__ctor_m12000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m13708(__this, method) (( bool (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m12001_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m13709(__this, method) (( Object_t * (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m12002_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m13710(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2608 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m12003_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13711(__this, method) (( Object_t* (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12004_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m13712(__this, method) (( Object_t * (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m12005_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m13713(__this, method) (( List_1_t227 * (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_Peek_m12006_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m13714(__this, method) (( List_1_t227 * (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_Pop_m12007_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m13715(__this, ___t, method) (( void (*) (Stack_1_t2608 *, List_1_t227 *, const MethodInfo*))Stack_1_Push_m12008_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m13716(__this, method) (( int32_t (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_get_Count_m12009_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m13717(__this, method) (( Enumerator_t3106  (*) (Stack_1_t2608 *, const MethodInfo*))Stack_1_GetEnumerator_m12010_gshared)(__this, method)
