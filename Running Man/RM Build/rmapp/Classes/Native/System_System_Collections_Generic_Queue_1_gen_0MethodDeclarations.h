﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2461;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t78;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m11704_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1__ctor_m11704(__this, method) (( void (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1__ctor_m11704_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m11706_gshared (Queue_1_t2461 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m11706(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2461 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m11706_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708(__this, method) (( bool (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m11708_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m11710_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m11710(__this, method) (( Object_t * (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m11710_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712(__this, method) (( Object_t* (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11712_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714(__this, method) (( Object_t * (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m11714_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m11716_gshared (Queue_1_t2461 * __this, ObjectU5BU5D_t29* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m11716(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2461 *, ObjectU5BU5D_t29*, int32_t, const MethodInfo*))Queue_1_CopyTo_m11716_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C" Object_t * Queue_1_Dequeue_m11717_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m11717(__this, method) (( Object_t * (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_Dequeue_m11717_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.Object>::Peek()
extern "C" Object_t * Queue_1_Peek_m11719_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_Peek_m11719(__this, method) (( Object_t * (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_Peek_m11719_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m11720_gshared (Queue_1_t2461 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m11720(__this, ___item, method) (( void (*) (Queue_1_t2461 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m11720_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m11722_gshared (Queue_1_t2461 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m11722(__this, ___new_size, method) (( void (*) (Queue_1_t2461 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m11722_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m11724_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m11724(__this, method) (( int32_t (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_get_Count_m11724_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2462  Queue_1_GetEnumerator_m11726_gshared (Queue_1_t2461 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m11726(__this, method) (( Enumerator_t2462  (*) (Queue_1_t2461 *, const MethodInfo*))Queue_1_GetEnumerator_m11726_gshared)(__this, method)
