﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BoardController
struct BoardController_t23;

// System.Void BoardController::.ctor()
extern "C" void BoardController__ctor_m49 (BoardController_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::Awake()
extern "C" void BoardController_Awake_m50 (BoardController_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::Stop()
extern "C" void BoardController_Stop_m51 (BoardController_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardController::PlaySound(System.Int32)
extern "C" void BoardController_PlaySound_m52 (BoardController_t23 * __this, int32_t ___sound, const MethodInfo* method) IL2CPP_METHOD_ATTR;
