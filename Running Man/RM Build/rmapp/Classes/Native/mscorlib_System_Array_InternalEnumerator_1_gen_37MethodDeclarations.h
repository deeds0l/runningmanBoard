﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
struct InternalEnumerator_1_t2758;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15818_gshared (InternalEnumerator_1_t2758 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15818(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2758 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15818_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15819_gshared (InternalEnumerator_1_t2758 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15819(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2758 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15819_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15820_gshared (InternalEnumerator_1_t2758 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15820(__this, method) (( void (*) (InternalEnumerator_1_t2758 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15820_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15821_gshared (InternalEnumerator_1_t2758 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15821(__this, method) (( bool (*) (InternalEnumerator_1_t2758 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15821_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C" Keyframe_t580  InternalEnumerator_1_get_Current_m15822_gshared (InternalEnumerator_1_t2758 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15822(__this, method) (( Keyframe_t580  (*) (InternalEnumerator_1_t2758 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15822_gshared)(__this, method)
