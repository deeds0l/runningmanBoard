﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t1067;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t1142  : public MulticastDelegate_t216
{
};
