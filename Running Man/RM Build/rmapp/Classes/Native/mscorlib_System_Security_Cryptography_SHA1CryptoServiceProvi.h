﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.SHA1Internal
struct SHA1Internal_t1743;
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// System.Security.Cryptography.SHA1CryptoServiceProvider
struct  SHA1CryptoServiceProvider_t1744  : public SHA1_t1180
{
	// System.Security.Cryptography.SHA1Internal System.Security.Cryptography.SHA1CryptoServiceProvider::sha
	SHA1Internal_t1743 * ___sha_4;
};
