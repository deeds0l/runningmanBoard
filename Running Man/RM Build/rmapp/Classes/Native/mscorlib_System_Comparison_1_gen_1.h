﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t325;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct  Comparison_1_t173  : public MulticastDelegate_t216
{
};
