﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t1396;
// System.Char[]
struct CharU5BU5D_t222;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m6741 (Contraction_t1396 * __this, CharU5BU5D_t222* ___source, String_t* ___replacement, ByteU5BU5D_t546* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
