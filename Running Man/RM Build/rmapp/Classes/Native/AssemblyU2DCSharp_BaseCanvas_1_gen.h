﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t48;
// UnityEngine.CanvasGroup
struct CanvasGroup_t52;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// BaseCanvas`1<System.Object>
struct  BaseCanvas_1_t2464  : public MonoBehaviour_t22
{
	// UnityEngine.Canvas BaseCanvas`1<System.Object>::m_canvas
	Canvas_t48 * ___m_canvas_3;
	// UnityEngine.CanvasGroup BaseCanvas`1<System.Object>::m_canvasGroup
	CanvasGroup_t52 * ___m_canvasGroup_4;
};
struct BaseCanvas_1_t2464_StaticFields{
	// T BaseCanvas`1<System.Object>::s_instance
	Object_t * ___s_instance_2;
};
