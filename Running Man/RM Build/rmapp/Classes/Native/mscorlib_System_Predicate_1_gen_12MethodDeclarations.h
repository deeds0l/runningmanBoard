﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Transform>
struct Predicate_1_t2521;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t30;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m12595(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2521 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m11435_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::Invoke(T)
#define Predicate_1_Invoke_m12596(__this, ___obj, method) (( bool (*) (Predicate_1_t2521 *, Transform_t30 *, const MethodInfo*))Predicate_1_Invoke_m11436_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Transform>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m12597(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2521 *, Transform_t30 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m11437_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m12598(__this, ___result, method) (( bool (*) (Predicate_1_t2521 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m11438_gshared)(__this, ___result, method)
