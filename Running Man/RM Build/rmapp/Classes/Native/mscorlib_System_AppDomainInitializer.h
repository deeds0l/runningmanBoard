﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t45;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AppDomainInitializer
struct  AppDomainInitializer_t1818  : public MulticastDelegate_t216
{
};
