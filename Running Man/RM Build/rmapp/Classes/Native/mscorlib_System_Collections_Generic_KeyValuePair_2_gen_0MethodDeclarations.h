﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
struct KeyValuePair_2_t718;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m16815(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t718 *, String_t*, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m13480_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3454(__this, method) (( String_t* (*) (KeyValuePair_2_t718 *, const MethodInfo*))KeyValuePair_2_get_Key_m13481_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16816(__this, ___value, method) (( void (*) (KeyValuePair_2_t718 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m13482_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3455(__this, method) (( Object_t * (*) (KeyValuePair_2_t718 *, const MethodInfo*))KeyValuePair_2_get_Value_m13483_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16817(__this, ___value, method) (( void (*) (KeyValuePair_2_t718 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m13484_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::ToString()
#define KeyValuePair_2_ToString_m16818(__this, method) (( String_t* (*) (KeyValuePair_2_t718 *, const MethodInfo*))KeyValuePair_2_ToString_m13485_gshared)(__this, method)
