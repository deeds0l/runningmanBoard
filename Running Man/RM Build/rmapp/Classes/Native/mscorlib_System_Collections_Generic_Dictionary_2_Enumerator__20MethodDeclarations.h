﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
struct Enumerator_t2937;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t2932;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m18119_gshared (Enumerator_t2937 * __this, Dictionary_2_t2932 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m18119(__this, ___dictionary, method) (( void (*) (Enumerator_t2937 *, Dictionary_2_t2932 *, const MethodInfo*))Enumerator__ctor_m18119_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18120_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18120(__this, method) (( Object_t * (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18120_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1147  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18121_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122(__this, method) (( Object_t * (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18122_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123(__this, method) (( Object_t * (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18124_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18124(__this, method) (( bool (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_MoveNext_m18124_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" KeyValuePair_2_t2933  Enumerator_get_Current_m18125_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18125(__this, method) (( KeyValuePair_2_t2933  (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_get_Current_m18125_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m18126_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m18126(__this, method) (( Object_t * (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_get_CurrentKey_m18126_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::get_CurrentValue()
extern "C" uint8_t Enumerator_get_CurrentValue_m18127_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m18127(__this, method) (( uint8_t (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_get_CurrentValue_m18127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyState()
extern "C" void Enumerator_VerifyState_m18128_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18128(__this, method) (( void (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_VerifyState_m18128_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m18129_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m18129(__this, method) (( void (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_VerifyCurrent_m18129_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m18130_gshared (Enumerator_t2937 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18130(__this, method) (( void (*) (Enumerator_t2937 *, const MethodInfo*))Enumerator_Dispose_m18130_gshared)(__this, method)
