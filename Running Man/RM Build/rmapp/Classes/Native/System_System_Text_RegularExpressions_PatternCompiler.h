﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t985;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_t1098  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t985 * ___pgm_0;
};
