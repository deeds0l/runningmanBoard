﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$3132
struct U24ArrayTypeU243132_t1892;
struct U24ArrayTypeU243132_t1892_marshaled;

void U24ArrayTypeU243132_t1892_marshal(const U24ArrayTypeU243132_t1892& unmarshaled, U24ArrayTypeU243132_t1892_marshaled& marshaled);
void U24ArrayTypeU243132_t1892_marshal_back(const U24ArrayTypeU243132_t1892_marshaled& marshaled, U24ArrayTypeU243132_t1892& unmarshaled);
void U24ArrayTypeU243132_t1892_marshal_cleanup(U24ArrayTypeU243132_t1892_marshaled& marshaled);
