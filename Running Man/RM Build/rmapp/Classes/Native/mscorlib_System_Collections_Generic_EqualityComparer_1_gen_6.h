﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct EqualityComparer_1_t2830;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.UInt64>
struct  EqualityComparer_1_t2830  : public Object_t
{
};
struct EqualityComparer_1_t2830_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt64>::_default
	EqualityComparer_1_t2830 * ____default_0;
};
