﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t216;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t216  : public Delegate_t361
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t216 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t216 * ___kpm_next_10;
};
