﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Enumerator_t2638;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t48;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t366;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t193;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"
#define Enumerator__ctor_m14158(__this, ___dictionary, method) (( void (*) (Enumerator_t2638 *, Dictionary_2_t193 *, const MethodInfo*))Enumerator__ctor_m13505_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14159(__this, method) (( Object_t * (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14160(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14161(__this, method) (( Object_t * (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14162(__this, method) (( Object_t * (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m14163(__this, method) (( bool (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_MoveNext_m13510_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m14164(__this, method) (( KeyValuePair_2_t2635  (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_get_Current_m13511_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14165(__this, method) (( Canvas_t48 * (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_get_CurrentKey_m13512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14166(__this, method) (( IndexedSet_1_t366 * (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_get_CurrentValue_m13513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m14167(__this, method) (( void (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_VerifyState_m13514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14168(__this, method) (( void (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_VerifyCurrent_m13515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m14169(__this, method) (( void (*) (Enumerator_t2638 *, const MethodInfo*))Enumerator_Dispose_m13516_gshared)(__this, method)
