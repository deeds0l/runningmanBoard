﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t613;
// System.String
struct String_t;

// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor()
extern "C" void NetworkAccessToken__ctor_m3081 (NetworkAccessToken_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Types.NetworkAccessToken::GetByteString()
extern "C" String_t* NetworkAccessToken_GetByteString_m3082 (NetworkAccessToken_t613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
