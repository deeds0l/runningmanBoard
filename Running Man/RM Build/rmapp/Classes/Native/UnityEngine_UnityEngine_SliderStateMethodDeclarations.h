﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SliderState
struct SliderState_t673;

// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m3282 (SliderState_t673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
