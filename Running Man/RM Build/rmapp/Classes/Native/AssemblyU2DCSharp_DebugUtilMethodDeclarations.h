﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DebugUtil
struct DebugUtil_t24;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.String
struct String_t;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"

// System.Void DebugUtil::.ctor()
extern "C" void DebugUtil__ctor_m53 (DebugUtil_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::.cctor()
extern "C" void DebugUtil__cctor_m54 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::log(System.Object,System.Object[])
extern "C" void DebugUtil_log_m55 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::warn(System.Object,System.Object[])
extern "C" void DebugUtil_warn_m56 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::error(System.Object,System.Object[])
extern "C" void DebugUtil_error_m57 (Object_t * __this /* static, unused */, Object_t * ___format, ObjectU5BU5D_t29* ___paramList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::assert(System.Boolean)
extern "C" void DebugUtil_assert_m58 (Object_t * __this /* static, unused */, bool ___condition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::assert(System.Boolean,System.String)
extern "C" void DebugUtil_assert_m59 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___assertString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::assert(System.Boolean,System.String,System.Boolean)
extern "C" void DebugUtil_assert_m60 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___assertString, bool ___pauseOnFail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::Log(System.String)
extern "C" void DebugUtil_Log_m61 (Object_t * __this /* static, unused */, String_t* ___p_s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugUtil::LogMessage(System.String,UnityEngine.LogType)
extern "C" void DebugUtil_LogMessage_m62 (Object_t * __this /* static, unused */, String_t* ___p_s, int32_t ___p_logType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
