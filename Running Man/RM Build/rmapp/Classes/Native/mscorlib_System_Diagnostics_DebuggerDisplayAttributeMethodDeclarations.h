﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t1484;
// System.String
struct String_t;

// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
extern "C" void DebuggerDisplayAttribute__ctor_m7454 (DebuggerDisplayAttribute_t1484 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
extern "C" void DebuggerDisplayAttribute_set_Name_m7455 (DebuggerDisplayAttribute_t1484 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
