﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct Enumerator_t740;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t704;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"
#define Enumerator__ctor_m15508(__this, ___dictionary, method) (( void (*) (Enumerator_t740 *, Dictionary_2_t704 *, const MethodInfo*))Enumerator__ctor_m13505_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15509(__this, method) (( Object_t * (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15510(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15511(__this, method) (( Object_t * (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15512(__this, method) (( Object_t * (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m3410(__this, method) (( bool (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_MoveNext_m13510_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m3407(__this, method) (( KeyValuePair_2_t739  (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_get_Current_m13511_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15513(__this, method) (( String_t* (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_get_CurrentKey_m13512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15514(__this, method) (( String_t* (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_get_CurrentValue_m13513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyState()
#define Enumerator_VerifyState_m15515(__this, method) (( void (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_VerifyState_m13514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15516(__this, method) (( void (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_VerifyCurrent_m13515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m15517(__this, method) (( void (*) (Enumerator_t740 *, const MethodInfo*))Enumerator_Dispose_m13516_gshared)(__this, method)
