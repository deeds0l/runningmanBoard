﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2773;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m16093_gshared (Predicate_1_t2773 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m16093(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2773 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m16093_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m16094_gshared (Predicate_1_t2773 * __this, UILineInfo_t375  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m16094(__this, ___obj, method) (( bool (*) (Predicate_1_t2773 *, UILineInfo_t375 , const MethodInfo*))Predicate_1_Invoke_m16094_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m16095_gshared (Predicate_1_t2773 * __this, UILineInfo_t375  ___obj, AsyncCallback_t214 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m16095(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2773 *, UILineInfo_t375 , AsyncCallback_t214 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m16095_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m16096_gshared (Predicate_1_t2773 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m16096(__this, ___result, method) (( bool (*) (Predicate_1_t2773 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m16096_gshared)(__this, ___result, method)
