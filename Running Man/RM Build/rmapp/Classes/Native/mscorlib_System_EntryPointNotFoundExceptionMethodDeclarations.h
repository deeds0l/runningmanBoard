﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.EntryPointNotFoundException
struct EntryPointNotFoundException_t1837;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t725;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.EntryPointNotFoundException::.ctor()
extern "C" void EntryPointNotFoundException__ctor_m9960 (EntryPointNotFoundException_t1837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.EntryPointNotFoundException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void EntryPointNotFoundException__ctor_m9961 (EntryPointNotFoundException_t1837 * __this, SerializationInfo_t725 * ___info, StreamingContext_t726  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
