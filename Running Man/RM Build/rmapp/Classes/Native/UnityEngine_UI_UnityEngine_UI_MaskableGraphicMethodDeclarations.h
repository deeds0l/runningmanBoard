﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t204;
// UnityEngine.Material
struct Material_t180;

// System.Void UnityEngine.UI.MaskableGraphic::.ctor()
extern "C" void MaskableGraphic__ctor_m896 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.MaskableGraphic::get_maskable()
extern "C" bool MaskableGraphic_get_maskable_m897 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_maskable(System.Boolean)
extern "C" void MaskableGraphic_set_maskable_m898 (MaskableGraphic_t204 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.UI.MaskableGraphic::get_material()
extern "C" Material_t180 * MaskableGraphic_get_material_m899 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_material(UnityEngine.Material)
extern "C" void MaskableGraphic_set_material_m900 (MaskableGraphic_t204 * __this, Material_t180 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::UpdateInternalState()
extern "C" void MaskableGraphic_UpdateInternalState_m901 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnEnable()
extern "C" void MaskableGraphic_OnEnable_m902 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnDisable()
extern "C" void MaskableGraphic_OnDisable_m903 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnTransformParentChanged()
extern "C" void MaskableGraphic_OnTransformParentChanged_m904 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ParentMaskStateChanged()
extern "C" void MaskableGraphic_ParentMaskStateChanged_m905 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ClearMaskMaterial()
extern "C" void MaskableGraphic_ClearMaskMaterial_m906 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::SetMaterialDirty()
extern "C" void MaskableGraphic_SetMaterialDirty_m907 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.MaskableGraphic::GetStencilForGraphic()
extern "C" int32_t MaskableGraphic_GetStencilForGraphic_m908 (MaskableGraphic_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
