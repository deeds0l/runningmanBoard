﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2564;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t78;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.Predicate`1<System.Object>
struct Predicate_1_t2435;
// System.Comparison`1<System.Object>
struct Comparison_1_t2442;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m13063_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m13063(__this, method) (( void (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1__ctor_m13063_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065(__this, method) (( Object_t * (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13065_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m13067_gshared (IndexedSet_1_t2564 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m13067(__this, ___item, method) (( void (*) (IndexedSet_1_t2564 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m13067_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m13069_gshared (IndexedSet_1_t2564 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m13069(__this, ___item, method) (( bool (*) (IndexedSet_1_t2564 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m13069_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m13071_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m13071(__this, method) (( Object_t* (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m13071_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m13073_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m13073(__this, method) (( void (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1_Clear_m13073_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m13075_gshared (IndexedSet_1_t2564 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m13075(__this, ___item, method) (( bool (*) (IndexedSet_1_t2564 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m13075_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m13077_gshared (IndexedSet_1_t2564 * __this, ObjectU5BU5D_t29* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m13077(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2564 *, ObjectU5BU5D_t29*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m13077_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m13079_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m13079(__this, method) (( int32_t (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1_get_Count_m13079_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m13081_gshared (IndexedSet_1_t2564 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m13081(__this, method) (( bool (*) (IndexedSet_1_t2564 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m13081_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m13083_gshared (IndexedSet_1_t2564 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m13083(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2564 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m13083_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m13085_gshared (IndexedSet_1_t2564 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m13085(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2564 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m13085_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m13087_gshared (IndexedSet_1_t2564 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m13087(__this, ___index, method) (( void (*) (IndexedSet_1_t2564 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13087_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m13089_gshared (IndexedSet_1_t2564 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m13089(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2564 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13089_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m13091_gshared (IndexedSet_1_t2564 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m13091(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2564 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m13091_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m13092_gshared (IndexedSet_1_t2564 * __this, Predicate_1_t2435 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m13092(__this, ___match, method) (( void (*) (IndexedSet_1_t2564 *, Predicate_1_t2435 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13092_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m13093_gshared (IndexedSet_1_t2564 * __this, Comparison_1_t2442 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m13093(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2564 *, Comparison_1_t2442 *, const MethodInfo*))IndexedSet_1_Sort_m13093_gshared)(__this, ___sortLayoutFunction, method)
