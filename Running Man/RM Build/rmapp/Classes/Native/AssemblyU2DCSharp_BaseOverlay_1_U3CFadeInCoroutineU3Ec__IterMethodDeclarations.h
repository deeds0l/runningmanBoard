﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>
struct U3CFadeInCoroutineU3Ec__Iterator3_t2467;
// System.Object
struct Object_t;

// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::.ctor()
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3__ctor_m11770_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3__ctor_m11770(__this, method) (( void (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3__ctor_m11770_gshared)(__this, method)
// System.Object BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11771_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11771(__this, method) (( Object_t * (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m11771_gshared)(__this, method)
// System.Object BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m11772_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m11772(__this, method) (( Object_t * (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m11772_gshared)(__this, method)
// System.Boolean BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::MoveNext()
extern "C" bool U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m11773_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m11773(__this, method) (( bool (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3_MoveNext_m11773_gshared)(__this, method)
// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::Dispose()
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m11774_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m11774(__this, method) (( void (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m11774_gshared)(__this, method)
// System.Void BaseOverlay`1/<FadeInCoroutine>c__Iterator3<System.Object>::Reset()
extern "C" void U3CFadeInCoroutineU3Ec__Iterator3_Reset_m11775_gshared (U3CFadeInCoroutineU3Ec__Iterator3_t2467 * __this, const MethodInfo* method);
#define U3CFadeInCoroutineU3Ec__Iterator3_Reset_m11775(__this, method) (( void (*) (U3CFadeInCoroutineU3Ec__Iterator3_t2467 *, const MethodInfo*))U3CFadeInCoroutineU3Ec__Iterator3_Reset_m11775_gshared)(__this, method)
