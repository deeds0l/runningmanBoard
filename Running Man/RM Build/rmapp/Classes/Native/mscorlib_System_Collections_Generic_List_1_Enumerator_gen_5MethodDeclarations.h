﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>
struct Enumerator_t2452;
// System.Object
struct Object_t;
// AudioPooler/objectSource
struct objectSource_t8;
// System.Collections.Generic.List`1<AudioPooler/objectSource>
struct List_1_t21;

// System.Void System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m11595(__this, ___l, method) (( void (*) (Enumerator_t2452 *, List_1_t21 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11596(__this, method) (( Object_t * (*) (Enumerator_t2452 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::Dispose()
#define Enumerator_Dispose_m11597(__this, method) (( void (*) (Enumerator_t2452 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::VerifyState()
#define Enumerator_VerifyState_m11598(__this, method) (( void (*) (Enumerator_t2452 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::MoveNext()
#define Enumerator_MoveNext_m11599(__this, method) (( bool (*) (Enumerator_t2452 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AudioPooler/objectSource>::get_Current()
#define Enumerator_get_Current_m11600(__this, method) (( objectSource_t8 * (*) (Enumerator_t2452 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
