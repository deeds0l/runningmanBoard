﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// BaseOverlay`1<System.Object>
struct BaseOverlay_1_t2465;
// System.Object
#include "mscorlib_System_Object.h"
// BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>
struct  U3CCloseWithDelayCoroutineU3Ec__Iterator2_t2466  : public Object_t
{
	// System.Single BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::p_delay
	float ___p_delay_0;
	// System.Int32 BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::$PC
	int32_t ___U24PC_1;
	// System.Object BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::$current
	Object_t * ___U24current_2;
	// System.Single BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::<$>p_delay
	float ___U3CU24U3Ep_delay_3;
	// BaseOverlay`1<T> BaseOverlay`1/<CloseWithDelayCoroutine>c__Iterator2<System.Object>::<>f__this
	BaseOverlay_1_t2465 * ___U3CU3Ef__this_4;
};
