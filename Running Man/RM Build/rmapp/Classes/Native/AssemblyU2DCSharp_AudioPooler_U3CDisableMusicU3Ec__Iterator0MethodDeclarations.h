﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<DisableMusic>c__Iterator0
struct U3CDisableMusicU3Ec__Iterator0_t11;
// System.Object
struct Object_t;

// System.Void AudioPooler/<DisableMusic>c__Iterator0::.ctor()
extern "C" void U3CDisableMusicU3Ec__Iterator0__ctor_m8 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AudioPooler/<DisableMusic>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<DisableMusic>c__Iterator0::MoveNext()
extern "C" bool U3CDisableMusicU3Ec__Iterator0_MoveNext_m11 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Dispose()
extern "C" void U3CDisableMusicU3Ec__Iterator0_Dispose_m12 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioPooler/<DisableMusic>c__Iterator0::Reset()
extern "C" void U3CDisableMusicU3Ec__Iterator0_Reset_m13 (U3CDisableMusicU3Ec__Iterator0_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
