﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t2559;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13041_gshared (InternalEnumerator_1_t2559 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13041(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2559 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13041_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13042_gshared (InternalEnumerator_1_t2559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13042(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13042_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13043_gshared (InternalEnumerator_1_t2559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13043(__this, method) (( void (*) (InternalEnumerator_1_t2559 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13043_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13044_gshared (InternalEnumerator_1_t2559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13044(__this, method) (( bool (*) (InternalEnumerator_1_t2559 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13044_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t349  InternalEnumerator_1_get_Current_m13045_gshared (InternalEnumerator_1_t2559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13045(__this, method) (( RaycastHit2D_t349  (*) (InternalEnumerator_1_t2559 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13045_gshared)(__this, method)
