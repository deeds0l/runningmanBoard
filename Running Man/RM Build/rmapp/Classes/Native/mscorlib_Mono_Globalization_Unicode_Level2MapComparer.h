﻿#pragma once
#include <stdint.h>
// Mono.Globalization.Unicode.Level2MapComparer
struct Level2MapComparer_t1399;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.Level2MapComparer
struct  Level2MapComparer_t1399  : public Object_t
{
};
struct Level2MapComparer_t1399_StaticFields{
	// Mono.Globalization.Unicode.Level2MapComparer Mono.Globalization.Unicode.Level2MapComparer::Instance
	Level2MapComparer_t1399 * ___Instance_0;
};
