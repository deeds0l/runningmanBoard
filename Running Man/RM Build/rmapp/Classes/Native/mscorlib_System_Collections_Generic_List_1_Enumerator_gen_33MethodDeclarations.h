﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>
struct Enumerator_t3015;
// System.Object
struct Object_t;
// System.Security.Policy.StrongName
struct StrongName_t1764;
// System.Collections.Generic.List`1<System.Security.Policy.StrongName>
struct List_1_t1943;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m18680(__this, ___l, method) (( void (*) (Enumerator_t3015 *, List_1_t1943 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18681(__this, method) (( Object_t * (*) (Enumerator_t3015 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::Dispose()
#define Enumerator_Dispose_m18682(__this, method) (( void (*) (Enumerator_t3015 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::VerifyState()
#define Enumerator_VerifyState_m18683(__this, method) (( void (*) (Enumerator_t3015 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::MoveNext()
#define Enumerator_MoveNext_m18684(__this, method) (( bool (*) (Enumerator_t3015 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Security.Policy.StrongName>::get_Current()
#define Enumerator_get_Current_m18685(__this, method) (( StrongName_t1764 * (*) (Enumerator_t3015 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
