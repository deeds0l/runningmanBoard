﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern TypeInfo* RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var;
void g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t56 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t56 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m152(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t57_il2cpp_TypeInfo_var;
void AudioPooler_t10_CustomAttributesCacheGenerator_m_audioPreloadCount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t57 * tmp;
		tmp = (SerializeField_t57 *)il2cpp_codegen_object_new (SerializeField_t57_il2cpp_TypeInfo_var);
		SerializeField__ctor_m154(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void AudioPooler_t10_CustomAttributesCacheGenerator_AudioPooler_DisableMusic_m47(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void AudioPooler_t10_CustomAttributesCacheGenerator_AudioPooler_DisableSound_m48(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_Dispose_m12(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_Reset_m13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_Dispose_m18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_Reset_m19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CGetAudioSourceU3Ec__AnonStorey6_t13_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CReturnAudioSourceU3Ec__AnonStorey7_t14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CClearAudioSourceU3Ec__AnonStorey8_t15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CClearMusicU3Ec__AnonStorey9_t16_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CSearchMusicU3Ec__AnonStoreyB_t18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CSearchSoundU3Ec__AnonStoreyC_t19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// DefiniteCasinoCanvasSetup
#include "AssemblyU2DCSharp_DefiniteCasinoCanvasSetup.h"
extern const Il2CppType* DefiniteCasinoCanvasSetup_t25_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void BaseCanvas_1_t60_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefiniteCasinoCanvasSetup_t25_0_0_0_var = il2cpp_codegen_type_from_index(42);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(DefiniteCasinoCanvasSetup_t25_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_CloseWithDelayCoroutine_m176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_FadeInCoroutine_m180(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_FadeOutCoroutine_m181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_log_m55(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_ERROR"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_LOG"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_WARN"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_log_m55_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_warn_m56(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_WARN"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_ERROR"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_warn_m56_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_error_m57(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_ERROR"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t68_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_error_m57_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t68 * tmp;
		tmp = (ParamArrayAttribute_t68 *)il2cpp_codegen_object_new (ParamArrayAttribute_t68_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m201(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m58(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("UNITY_EDITOR"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_LOG"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m59(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_LOG"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("UNITY_EDITOR"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ConditionalAttribute_t67_il2cpp_TypeInfo_var;
void DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m60(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConditionalAttribute_t67_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("UNITY_EDITOR"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t67 * tmp;
		tmp = (ConditionalAttribute_t67 *)il2cpp_codegen_object_new (ConditionalAttribute_t67_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m200(tmp, il2cpp_codegen_string_new_wrapper("DEBUG_LEVEL_LOG"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster.h"
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern const Il2CppType* CanvasScaler_t50_0_0_0_var;
extern const Il2CppType* GraphicRaycaster_t51_0_0_0_var;
extern const Il2CppType* CanvasGroup_t52_0_0_0_var;
extern const Il2CppType* Canvas_t48_0_0_0_var;
extern TypeInfo* RequireComponent_t61_il2cpp_TypeInfo_var;
void DefiniteCasinoCanvasSetup_t25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasScaler_t50_0_0_0_var = il2cpp_codegen_type_from_index(31);
		GraphicRaycaster_t51_0_0_0_var = il2cpp_codegen_type_from_index(32);
		CanvasGroup_t52_0_0_0_var = il2cpp_codegen_type_from_index(33);
		Canvas_t48_0_0_0_var = il2cpp_codegen_type_from_index(30);
		RequireComponent_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(CanvasScaler_t50_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(GraphicRaycaster_t51_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(CanvasGroup_t52_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t61 * tmp;
		tmp = (RequireComponent_t61 *)il2cpp_codegen_object_new (RequireComponent_t61_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m157(tmp, il2cpp_codegen_type_get_object(Canvas_t48_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void Tools_t27_CustomAttributesCacheGenerator_Tools_WaitForRealTime_m72(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[57] = 
{
	NULL,
	g_AssemblyU2DCSharp_Assembly_CustomAttributesCacheGenerator,
	AudioPooler_t10_CustomAttributesCacheGenerator_m_audioPreloadCount,
	AudioPooler_t10_CustomAttributesCacheGenerator_AudioPooler_DisableMusic_m47,
	AudioPooler_t10_CustomAttributesCacheGenerator_AudioPooler_DisableSound_m48,
	U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator,
	U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m9,
	U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m10,
	U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_Dispose_m12,
	U3CDisableMusicU3Ec__Iterator0_t11_CustomAttributesCacheGenerator_U3CDisableMusicU3Ec__Iterator0_Reset_m13,
	U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator,
	U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15,
	U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m16,
	U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_Dispose_m18,
	U3CDisableSoundU3Ec__Iterator1_t12_CustomAttributesCacheGenerator_U3CDisableSoundU3Ec__Iterator1_Reset_m19,
	U3CGetAudioSourceU3Ec__AnonStorey6_t13_CustomAttributesCacheGenerator,
	U3CReturnAudioSourceU3Ec__AnonStorey7_t14_CustomAttributesCacheGenerator,
	U3CClearAudioSourceU3Ec__AnonStorey8_t15_CustomAttributesCacheGenerator,
	U3CClearMusicU3Ec__AnonStorey9_t16_CustomAttributesCacheGenerator,
	U3CClearSoundEffectsU3Ec__AnonStoreyA_t17_CustomAttributesCacheGenerator,
	U3CSearchMusicU3Ec__AnonStoreyB_t18_CustomAttributesCacheGenerator,
	U3CSearchSoundU3Ec__AnonStoreyC_t19_CustomAttributesCacheGenerator,
	BaseCanvas_1_t60_CustomAttributesCacheGenerator,
	BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_CloseWithDelayCoroutine_m176,
	BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_FadeInCoroutine_m180,
	BaseOverlay_1_t63_CustomAttributesCacheGenerator_BaseOverlay_1_FadeOutCoroutine_m181,
	U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator,
	U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m183,
	U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m184,
	U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_Dispose_m186,
	U3CCloseWithDelayCoroutineU3Ec__Iterator2_t64_CustomAttributesCacheGenerator_U3CCloseWithDelayCoroutineU3Ec__Iterator2_Reset_m187,
	U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator,
	U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m189,
	U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m190,
	U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_Dispose_m192,
	U3CFadeInCoroutineU3Ec__Iterator3_t65_CustomAttributesCacheGenerator_U3CFadeInCoroutineU3Ec__Iterator3_Reset_m193,
	U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator,
	U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m195,
	U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m196,
	U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_Dispose_m198,
	U3CFadeOutCoroutineU3Ec__Iterator4_t66_CustomAttributesCacheGenerator_U3CFadeOutCoroutineU3Ec__Iterator4_Reset_m199,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_log_m55,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_log_m55_Arg1_ParameterInfo,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_warn_m56,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_warn_m56_Arg1_ParameterInfo,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_error_m57,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_t24_DebugUtil_error_m57_Arg1_ParameterInfo,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m58,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m59,
	DebugUtil_t24_CustomAttributesCacheGenerator_DebugUtil_assert_m60,
	DefiniteCasinoCanvasSetup_t25_CustomAttributesCacheGenerator,
	Tools_t27_CustomAttributesCacheGenerator_Tools_WaitForRealTime_m72,
	U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator,
	U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m67,
	U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m68,
	U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_Dispose_m70,
	U3CWaitForRealTimeU3Ec__Iterator5_t26_CustomAttributesCacheGenerator_U3CWaitForRealTimeU3Ec__Iterator5_Reset_m71,
};
