﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1005;
// System.Net.WebRequest
struct WebRequest_t999;
// System.Uri
struct Uri_t617;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m3886 (HttpRequestCreator_t1005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t999 * HttpRequestCreator_Create_m3887 (HttpRequestCreator_t1005 * __this, Uri_t617 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
