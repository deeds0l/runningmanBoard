﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
struct Comparison_1_t2707;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t492;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m15001(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2707 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11465_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Invoke(T,T)
#define Comparison_1_Invoke_m15002(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2707 *, GcLeaderboard_t492 *, GcLeaderboard_t492 *, const MethodInfo*))Comparison_1_Invoke_m11466_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m15003(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2707 *, GcLeaderboard_t492 *, GcLeaderboard_t492 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11467_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m15004(__this, ___result, method) (( int32_t (*) (Comparison_1_t2707 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11468_gshared)(__this, ___result, method)
