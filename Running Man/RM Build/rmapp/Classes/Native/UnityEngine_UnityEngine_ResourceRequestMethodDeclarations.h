﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t536;
// UnityEngine.Object
struct Object_t33;
struct Object_t33_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m2627 (ResourceRequest_t536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t33 * ResourceRequest_get_asset_m2628 (ResourceRequest_t536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
