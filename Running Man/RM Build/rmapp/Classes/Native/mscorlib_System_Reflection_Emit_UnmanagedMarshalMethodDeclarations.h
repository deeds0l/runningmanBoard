﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1543;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1375;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1375 * UnmanagedMarshal_ToMarshalAsAttribute_m8183 (UnmanagedMarshal_t1543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
