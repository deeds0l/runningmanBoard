﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2772;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m16090_gshared (DefaultComparer_t2772 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16090(__this, method) (( void (*) (DefaultComparer_t2772 *, const MethodInfo*))DefaultComparer__ctor_m16090_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16091_gshared (DefaultComparer_t2772 * __this, UILineInfo_t375  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16091(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2772 *, UILineInfo_t375 , const MethodInfo*))DefaultComparer_GetHashCode_m16091_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16092_gshared (DefaultComparer_t2772 * __this, UILineInfo_t375  ___x, UILineInfo_t375  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16092(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2772 *, UILineInfo_t375 , UILineInfo_t375 , const MethodInfo*))DefaultComparer_Equals_m16092_gshared)(__this, ___x, ___y, method)
