﻿#pragma once
#include <stdint.h>
// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1305;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1174;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t1307  : public MulticastDelegate_t216
{
};
