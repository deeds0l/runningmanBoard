﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
struct Enumerator_t41;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t6;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m11696(__this, ___l, method) (( void (*) (Enumerator_t41 *, List_1_t6 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11697(__this, method) (( Object_t * (*) (Enumerator_t41 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::Dispose()
#define Enumerator_Dispose_m11698(__this, method) (( void (*) (Enumerator_t41 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::VerifyState()
#define Enumerator_VerifyState_m11699(__this, method) (( void (*) (Enumerator_t41 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::MoveNext()
#define Enumerator_MoveNext_m111(__this, method) (( bool (*) (Enumerator_t41 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::get_Current()
#define Enumerator_get_Current_m110(__this, method) (( AudioSource_t9 * (*) (Enumerator_t41 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
