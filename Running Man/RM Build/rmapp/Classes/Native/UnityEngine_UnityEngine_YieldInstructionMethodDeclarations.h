﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t478;
struct YieldInstruction_t478_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m2800 (YieldInstruction_t478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t478_marshal(const YieldInstruction_t478& unmarshaled, YieldInstruction_t478_marshaled& marshaled);
void YieldInstruction_t478_marshal_back(const YieldInstruction_t478_marshaled& marshaled, YieldInstruction_t478& unmarshaled);
void YieldInstruction_t478_marshal_cleanup(YieldInstruction_t478_marshaled& marshaled);
