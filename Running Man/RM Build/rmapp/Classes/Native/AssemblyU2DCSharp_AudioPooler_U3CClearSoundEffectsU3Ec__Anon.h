﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/<ClearSoundEffects>c__AnonStoreyA
struct  U3CClearSoundEffectsU3Ec__AnonStoreyA_t17  : public Object_t
{
	// UnityEngine.GameObject AudioPooler/<ClearSoundEffects>c__AnonStoreyA::p_audioContainer
	GameObject_t5 * ___p_audioContainer_0;
};
