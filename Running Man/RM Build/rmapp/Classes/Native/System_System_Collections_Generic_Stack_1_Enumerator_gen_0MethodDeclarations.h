﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Type>
struct Enumerator_t2888;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t779;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m17548(__this, ___t, method) (( void (*) (Enumerator_t2888 *, Stack_1_t779 *, const MethodInfo*))Enumerator__ctor_m12011_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17549(__this, method) (( Object_t * (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12012_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m17550(__this, method) (( void (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_Dispose_m12013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m17551(__this, method) (( bool (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_MoveNext_m12014_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m17552(__this, method) (( Type_t * (*) (Enumerator_t2888 *, const MethodInfo*))Enumerator_get_Current_m12015_gshared)(__this, method)
