﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<System.Boolean>
struct Action_1_t481;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Byte>
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"
#define Action_1__ctor_m14862(__this, ___object, ___method, method) (( void (*) (Action_1_t481 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14863_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Boolean>::Invoke(T)
#define Action_1_Invoke_m14864(__this, ___obj, method) (( void (*) (Action_1_t481 *, bool, const MethodInfo*))Action_1_Invoke_m14865_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m14866(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t481 *, bool, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14867_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m14868(__this, ___result, method) (( void (*) (Action_1_t481 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14869_gshared)(__this, ___result, method)
