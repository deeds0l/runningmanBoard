﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct Stack_1_t2482;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct IEnumerator_1_t3046;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t321;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_1.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m12020(__this, method) (( void (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1__ctor_m12000_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m12021(__this, method) (( bool (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m12001_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m12022(__this, method) (( Object_t * (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m12002_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m12023(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2482 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m12003_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12024(__this, method) (( Object_t* (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12004_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m12025(__this, method) (( Object_t * (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m12005_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Peek()
#define Stack_1_Peek_m12026(__this, method) (( List_1_t321 * (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_Peek_m12006_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Pop()
#define Stack_1_Pop_m12027(__this, method) (( List_1_t321 * (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_Pop_m12007_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Push(T)
#define Stack_1_Push_m12028(__this, ___t, method) (( void (*) (Stack_1_t2482 *, List_1_t321 *, const MethodInfo*))Stack_1_Push_m12008_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_Count()
#define Stack_1_get_Count_m12029(__this, method) (( int32_t (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_get_Count_m12009_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::GetEnumerator()
#define Stack_1_GetEnumerator_m12030(__this, method) (( Enumerator_t3047  (*) (Stack_1_t2482 *, const MethodInfo*))Stack_1_GetEnumerator_m12010_gshared)(__this, method)
