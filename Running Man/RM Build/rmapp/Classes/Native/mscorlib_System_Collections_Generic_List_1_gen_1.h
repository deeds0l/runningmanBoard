﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2427;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct  List_1_t1  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.AudioClip>::_items
	AudioClipU5BU5D_t2427* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::_version
	int32_t ____version_3;
};
struct List_1_t1_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.AudioClip>::EmptyArray
	AudioClipU5BU5D_t2427* ___EmptyArray_4;
};
