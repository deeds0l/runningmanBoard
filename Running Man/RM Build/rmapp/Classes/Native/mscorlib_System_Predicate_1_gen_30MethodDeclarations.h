﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Predicate_1_t2802;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t604;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m16435(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2802 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m11435_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Invoke(T)
#define Predicate_1_Invoke_m16436(__this, ___obj, method) (( bool (*) (Predicate_1_t2802 *, MatchDirectConnectInfo_t604 *, const MethodInfo*))Predicate_1_Invoke_m11436_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m16437(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2802 *, MatchDirectConnectInfo_t604 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m11437_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m16438(__this, ___result, method) (( bool (*) (Predicate_1_t2802 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m11438_gshared)(__this, ___result, method)
