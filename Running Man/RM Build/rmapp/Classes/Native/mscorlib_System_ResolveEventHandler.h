﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1164;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t1870;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t1815  : public MulticastDelegate_t216
{
};
