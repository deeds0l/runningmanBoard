﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t549;
struct CacheIndex_t549_marshaled;

void CacheIndex_t549_marshal(const CacheIndex_t549& unmarshaled, CacheIndex_t549_marshaled& marshaled);
void CacheIndex_t549_marshal_back(const CacheIndex_t549_marshaled& marshaled, CacheIndex_t549& unmarshaled);
void CacheIndex_t549_marshal_cleanup(CacheIndex_t549_marshaled& marshaled);
