﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t985;
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// System.Collections.ArrayList/ArrayListWrapper
struct  ArrayListWrapper_t1462  : public ArrayList_t985
{
	// System.Collections.ArrayList System.Collections.ArrayList/ArrayListWrapper::m_InnerArrayList
	ArrayList_t985 * ___m_InnerArrayList_5;
};
