﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider2D
struct Collider2D_t350;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t568;

// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t568 * Collider2D_get_attachedRigidbody_m2830 (Collider2D_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
