﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AudioPooler/<ReturnAudioSource>c__AnonStorey7
struct U3CReturnAudioSourceU3Ec__AnonStorey7_t14;
// AudioPooler/objectSource
struct objectSource_t8;

// System.Void AudioPooler/<ReturnAudioSource>c__AnonStorey7::.ctor()
extern "C" void U3CReturnAudioSourceU3Ec__AnonStorey7__ctor_m22 (U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AudioPooler/<ReturnAudioSource>c__AnonStorey7::<>m__1(AudioPooler/objectSource)
extern "C" bool U3CReturnAudioSourceU3Ec__AnonStorey7_U3CU3Em__1_m23 (U3CReturnAudioSourceU3Ec__AnonStorey7_t14 * __this, objectSource_t8 * ___audio, const MethodInfo* method) IL2CPP_METHOD_ATTR;
