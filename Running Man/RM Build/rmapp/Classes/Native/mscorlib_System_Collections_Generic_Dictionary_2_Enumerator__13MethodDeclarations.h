﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t2798;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t596;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12MethodDeclarations.h"
#define Enumerator__ctor_m16344(__this, ___dictionary, method) (( void (*) (Enumerator_t2798 *, Dictionary_2_t596 *, const MethodInfo*))Enumerator__ctor_m16242_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16345(__this, method) (( Object_t * (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16243_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16346(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16347(__this, method) (( Object_t * (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16348(__this, method) (( Object_t * (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m16349(__this, method) (( bool (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_MoveNext_m16247_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m16350(__this, method) (( KeyValuePair_2_t2795  (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_get_Current_m16248_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16351(__this, method) (( String_t* (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_get_CurrentKey_m16249_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16352(__this, method) (( int64_t (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_get_CurrentValue_m16250_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m16353(__this, method) (( void (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_VerifyState_m16251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16354(__this, method) (( void (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_VerifyCurrent_m16252_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m16355(__this, method) (( void (*) (Enumerator_t2798 *, const MethodInfo*))Enumerator_Dispose_m16253_gshared)(__this, method)
