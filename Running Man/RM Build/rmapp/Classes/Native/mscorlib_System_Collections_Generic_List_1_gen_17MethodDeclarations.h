﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t259;
// System.Object
struct Object_t;
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t258;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerator_1_t3133;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ICollection_1_t3134;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerable_1_t3135;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ReadOnlyCollection_1_t2670;
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t2668;
// System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Predicate_1_t2671;
// System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Comparison_1_t2673;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#define List_1__ctor_m1901(__this, method) (( void (*) (List_1_t259 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Int32)
#define List_1__ctor_m14505(__this, ___capacity, method) (( void (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.cctor()
#define List_1__cctor_m14506(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14507(__this, method) (( Object_t* (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14508(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t259 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14509(__this, method) (( Object_t * (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14510(__this, ___item, method) (( int32_t (*) (List_1_t259 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14511(__this, ___item, method) (( bool (*) (List_1_t259 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14512(__this, ___item, method) (( int32_t (*) (List_1_t259 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14513(__this, ___index, ___item, method) (( void (*) (List_1_t259 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14514(__this, ___item, method) (( void (*) (List_1_t259 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14515(__this, method) (( bool (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14516(__this, method) (( bool (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14517(__this, method) (( Object_t * (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14518(__this, method) (( bool (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14519(__this, method) (( bool (*) (List_1_t259 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14520(__this, ___index, method) (( Object_t * (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14521(__this, ___index, ___value, method) (( void (*) (List_1_t259 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Add(T)
#define List_1_Add_m14522(__this, ___item, method) (( void (*) (List_1_t259 *, MatEntry_t258 *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14523(__this, ___newCount, method) (( void (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14524(__this, ___collection, method) (( void (*) (List_1_t259 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14525(__this, ___enumerable, method) (( void (*) (List_1_t259 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14526(__this, ___collection, method) (( void (*) (List_1_t259 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AsReadOnly()
#define List_1_AsReadOnly_m14527(__this, method) (( ReadOnlyCollection_1_t2670 * (*) (List_1_t259 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Clear()
#define List_1_Clear_m14528(__this, method) (( void (*) (List_1_t259 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Contains(T)
#define List_1_Contains_m14529(__this, ___item, method) (( bool (*) (List_1_t259 *, MatEntry_t258 *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14530(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t259 *, MatEntryU5BU5D_t2668*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Find(System.Predicate`1<T>)
#define List_1_Find_m14531(__this, ___match, method) (( MatEntry_t258 * (*) (List_1_t259 *, Predicate_1_t2671 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14532(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2671 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14533(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t259 *, int32_t, int32_t, Predicate_1_t2671 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetEnumerator()
#define List_1_GetEnumerator_m14534(__this, method) (( Enumerator_t2672  (*) (List_1_t259 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::IndexOf(T)
#define List_1_IndexOf_m14535(__this, ___item, method) (( int32_t (*) (List_1_t259 *, MatEntry_t258 *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14536(__this, ___start, ___delta, method) (( void (*) (List_1_t259 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14537(__this, ___index, method) (( void (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Insert(System.Int32,T)
#define List_1_Insert_m14538(__this, ___index, ___item, method) (( void (*) (List_1_t259 *, int32_t, MatEntry_t258 *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14539(__this, ___collection, method) (( void (*) (List_1_t259 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Remove(T)
#define List_1_Remove_m14540(__this, ___item, method) (( bool (*) (List_1_t259 *, MatEntry_t258 *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14541(__this, ___match, method) (( int32_t (*) (List_1_t259 *, Predicate_1_t2671 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14542(__this, ___index, method) (( void (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Reverse()
#define List_1_Reverse_m14543(__this, method) (( void (*) (List_1_t259 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort()
#define List_1_Sort_m14544(__this, method) (( void (*) (List_1_t259 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14545(__this, ___comparison, method) (( void (*) (List_1_t259 *, Comparison_1_t2673 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::ToArray()
#define List_1_ToArray_m14546(__this, method) (( MatEntryU5BU5D_t2668* (*) (List_1_t259 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::TrimExcess()
#define List_1_TrimExcess_m14547(__this, method) (( void (*) (List_1_t259 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Capacity()
#define List_1_get_Capacity_m14548(__this, method) (( int32_t (*) (List_1_t259 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14549(__this, ___value, method) (( void (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count()
#define List_1_get_Count_m14550(__this, method) (( int32_t (*) (List_1_t259 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32)
#define List_1_get_Item_m14551(__this, ___index, method) (( MatEntry_t258 * (*) (List_1_t259 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m14552(__this, ___index, ___value, method) (( void (*) (List_1_t259 *, int32_t, MatEntry_t258 *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
