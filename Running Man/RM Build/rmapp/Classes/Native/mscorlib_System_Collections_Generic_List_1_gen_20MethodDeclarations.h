﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t336;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t78;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t28;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t716;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t36;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2430;
// System.Object[]
struct ObjectU5BU5D_t29;
// System.Predicate`1<System.Object>
struct Predicate_1_t2435;
// System.Comparison`1<System.Object>
struct Comparison_1_t2442;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m3449_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1__ctor_m3449(__this, method) (( void (*) (List_1_t336 *, const MethodInfo*))List_1__ctor_m3449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m11276_gshared (List_1_t336 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m11276(__this, ___capacity, method) (( void (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1__ctor_m11276_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m11278_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m11278(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11278_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689(__this, method) (( Object_t* (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m3672_gshared (List_1_t336 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3672(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t336 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3672_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3668(__this, method) (( Object_t * (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m3677_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3677(__this, ___item, method) (( int32_t (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m3677_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m3679_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3679(__this, ___item, method) (( bool (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3679_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m3680_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3680(__this, ___item, method) (( int32_t (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3680_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m3681_gshared (List_1_t336 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3681(__this, ___index, ___item, method) (( void (*) (List_1_t336 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3681_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m3682_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3682(__this, ___item, method) (( void (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3682_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684(__this, method) (( bool (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3684_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3670(__this, method) (( bool (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3670_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3671(__this, method) (( Object_t * (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3673(__this, method) (( bool (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3674(__this, method) (( bool (*) (List_1_t336 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3674_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m3675_gshared (List_1_t336 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3675(__this, ___index, method) (( Object_t * (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3675_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m3676_gshared (List_1_t336 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3676(__this, ___index, ___value, method) (( void (*) (List_1_t336 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3676_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m3685_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m3685(__this, ___item, method) (( void (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_Add_m3685_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m11296_gshared (List_1_t336 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m11296(__this, ___newCount, method) (( void (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11296_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m11298_gshared (List_1_t336 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m11298(__this, ___collection, method) (( void (*) (List_1_t336 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11298_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m11300_gshared (List_1_t336 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m11300(__this, ___enumerable, method) (( void (*) (List_1_t336 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11300_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m11302_gshared (List_1_t336 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m11302(__this, ___collection, method) (( void (*) (List_1_t336 *, Object_t*, const MethodInfo*))List_1_AddRange_m11302_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2430 * List_1_AsReadOnly_m11304_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m11304(__this, method) (( ReadOnlyCollection_1_t2430 * (*) (List_1_t336 *, const MethodInfo*))List_1_AsReadOnly_m11304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m3678_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_Clear_m3678(__this, method) (( void (*) (List_1_t336 *, const MethodInfo*))List_1_Clear_m3678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m3686_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m3686(__this, ___item, method) (( bool (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_Contains_m3686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m3687_gshared (List_1_t336 * __this, ObjectU5BU5D_t29* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m3687(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t336 *, ObjectU5BU5D_t29*, int32_t, const MethodInfo*))List_1_CopyTo_m3687_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m11309_gshared (List_1_t336 * __this, Predicate_1_t2435 * ___match, const MethodInfo* method);
#define List_1_Find_m11309(__this, ___match, method) (( Object_t * (*) (List_1_t336 *, Predicate_1_t2435 *, const MethodInfo*))List_1_Find_m11309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m11311_gshared (Object_t * __this /* static, unused */, Predicate_1_t2435 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m11311(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2435 *, const MethodInfo*))List_1_CheckMatch_m11311_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m11313_gshared (List_1_t336 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2435 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m11313(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t336 *, int32_t, int32_t, Predicate_1_t2435 *, const MethodInfo*))List_1_GetIndex_m11313_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2429  List_1_GetEnumerator_m11315_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m11315(__this, method) (( Enumerator_t2429  (*) (List_1_t336 *, const MethodInfo*))List_1_GetEnumerator_m11315_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m3690_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m3690(__this, ___item, method) (( int32_t (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_IndexOf_m3690_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m11318_gshared (List_1_t336 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m11318(__this, ___start, ___delta, method) (( void (*) (List_1_t336 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11318_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m11320_gshared (List_1_t336 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m11320(__this, ___index, method) (( void (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11320_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m3691_gshared (List_1_t336 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m3691(__this, ___index, ___item, method) (( void (*) (List_1_t336 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m3691_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m11323_gshared (List_1_t336 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m11323(__this, ___collection, method) (( void (*) (List_1_t336 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11323_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m3688_gshared (List_1_t336 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m3688(__this, ___item, method) (( bool (*) (List_1_t336 *, Object_t *, const MethodInfo*))List_1_Remove_m3688_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m11326_gshared (List_1_t336 * __this, Predicate_1_t2435 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m11326(__this, ___match, method) (( int32_t (*) (List_1_t336 *, Predicate_1_t2435 *, const MethodInfo*))List_1_RemoveAll_m11326_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m3683_gshared (List_1_t336 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m3683(__this, ___index, method) (( void (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3683_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m11329_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_Reverse_m11329(__this, method) (( void (*) (List_1_t336 *, const MethodInfo*))List_1_Reverse_m11329_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m11331_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_Sort_m11331(__this, method) (( void (*) (List_1_t336 *, const MethodInfo*))List_1_Sort_m11331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m11333_gshared (List_1_t336 * __this, Comparison_1_t2442 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m11333(__this, ___comparison, method) (( void (*) (List_1_t336 *, Comparison_1_t2442 *, const MethodInfo*))List_1_Sort_m11333_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t29* List_1_ToArray_m11335_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_ToArray_m11335(__this, method) (( ObjectU5BU5D_t29* (*) (List_1_t336 *, const MethodInfo*))List_1_ToArray_m11335_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m11337_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m11337(__this, method) (( void (*) (List_1_t336 *, const MethodInfo*))List_1_TrimExcess_m11337_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m11339_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m11339(__this, method) (( int32_t (*) (List_1_t336 *, const MethodInfo*))List_1_get_Capacity_m11339_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m11341_gshared (List_1_t336 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m11341(__this, ___value, method) (( void (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m3669_gshared (List_1_t336 * __this, const MethodInfo* method);
#define List_1_get_Count_m3669(__this, method) (( int32_t (*) (List_1_t336 *, const MethodInfo*))List_1_get_Count_m3669_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m3692_gshared (List_1_t336 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m3692(__this, ___index, method) (( Object_t * (*) (List_1_t336 *, int32_t, const MethodInfo*))List_1_get_Item_m3692_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m3693_gshared (List_1_t336 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m3693(__this, ___index, ___value, method) (( void (*) (List_1_t336 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m3693_gshared)(__this, ___index, ___value, method)
