﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t400_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t932_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t402_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t936_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(367);
		AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1863);
		SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1864);
		AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(365);
		AssemblyProductAttribute_t400_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1865);
		NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1866);
		RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		DebuggableAttribute_t932_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1867);
		CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1868);
		ComVisibleAttribute_t402_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1869);
		AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1870);
		CLSCompliantAttribute_t936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1871);
		AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyTitleAttribute_t403 * tmp;
		tmp = (AssemblyTitleAttribute_t403 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t403_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m1985(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t404 * tmp;
		tmp = (AssemblyFileVersionAttribute_t404 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t404_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m1986(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t928 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t928 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t928_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m3740(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t929 * tmp;
		tmp = (SatelliteContractVersionAttribute_t929 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t929_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m3741(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t401 * tmp;
		tmp = (AssemblyCopyrightAttribute_t401 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t401_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m1983(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t400 * tmp;
		tmp = (AssemblyProductAttribute_t400 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t400_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m1982(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t399 * tmp;
		tmp = (AssemblyCompanyAttribute_t399 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t399_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m1981(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t930 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t930 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t930_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m3742(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t931 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t931 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t931_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m3743(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t56 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t56 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t56_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m152(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m153(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t932 * tmp;
		tmp = (DebuggableAttribute_t932 *)il2cpp_codegen_object_new (DebuggableAttribute_t932_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m3744(tmp, 2, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t933 * tmp;
		tmp = (CompilationRelaxationsAttribute_t933 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t933_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m3745(tmp, 8, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t402 * tmp;
		tmp = (ComVisibleAttribute_t402 *)il2cpp_codegen_object_new (ComVisibleAttribute_t402_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m1984(tmp, false, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t934 * tmp;
		tmp = (AssemblyKeyFileAttribute_t934 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t934_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m3746(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t935 * tmp;
		tmp = (AssemblyDelaySignAttribute_t935 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t935_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m3747(tmp, true, NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t936 * tmp;
		tmp = (CLSCompliantAttribute_t936 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t936_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m3748(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t397 * tmp;
		tmp = (AssemblyDescriptionAttribute_t397 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t397_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m1979(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t803_il2cpp_TypeInfo_var;
void ExtensionAttribute_t792_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t803 * tmp;
		tmp = (AttributeUsageAttribute_t803 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t803_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m3590(tmp, 69, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_First_m3749(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_FirstOrDefault_m3750(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Last_m3751(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Last_m3752(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t792_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Where_m3753(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t792_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t792 * tmp;
		tmp = (ExtensionAttribute_t792 *)il2cpp_codegen_object_new (ExtensionAttribute_t792_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m3533(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m3754(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PredicateOf_1_t937_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void PredicateOf_1_t937_CustomAttributesCacheGenerator_PredicateOf_1_U3CAlwaysU3Em__76_m3756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t59 * tmp;
		tmp = (CompilerGeneratedAttribute_t59 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t59_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m156(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t58 * tmp;
		tmp = (DebuggerHiddenAttribute_t58 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t58_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m155(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[18] = 
{
	NULL,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t792_CustomAttributesCacheGenerator,
	Enumerable_t34_CustomAttributesCacheGenerator,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_First_m3749,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_FirstOrDefault_m3750,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Last_m3751,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Last_m3752,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_Where_m3753,
	Enumerable_t34_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m3754,
	PredicateOf_1_t937_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	PredicateOf_1_t937_CustomAttributesCacheGenerator_PredicateOf_1_U3CAlwaysU3Em__76_m3756,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3758,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m3759,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3760,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m3761,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t938_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m3763,
};
