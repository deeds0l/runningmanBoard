﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDeformatter
struct DSASignatureDeformatter_t1349;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1024;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t546;

// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor()
extern "C" void DSASignatureDeformatter__ctor_m8954 (DSASignatureDeformatter_t1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSASignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void DSASignatureDeformatter__ctor_m5839 (DSASignatureDeformatter_t1349 * __this, AsymmetricAlgorithm_t1024 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetHashAlgorithm(System.String)
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m8955 (DSASignatureDeformatter_t1349 * __this, String_t* ___strName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.DSASignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void DSASignatureDeformatter_SetKey_m8956 (DSASignatureDeformatter_t1349 * __this, AsymmetricAlgorithm_t1024 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.DSASignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
extern "C" bool DSASignatureDeformatter_VerifySignature_m8957 (DSASignatureDeformatter_t1349 * __this, ByteU5BU5D_t546* ___rgbHash, ByteU5BU5D_t546* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
