﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t5;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/<GetAudioSource>c__AnonStorey6
struct  U3CGetAudioSourceU3Ec__AnonStorey6_t13  : public Object_t
{
	// UnityEngine.GameObject AudioPooler/<GetAudioSource>c__AnonStorey6::p_audioContainer
	GameObject_t5 * ___p_audioContainer_0;
};
