﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>
struct Enumerator_t2629;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t188;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t190;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m13996(__this, ___l, method) (( void (*) (Enumerator_t2629 *, List_1_t190 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13997(__this, method) (( Object_t * (*) (Enumerator_t2629 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::Dispose()
#define Enumerator_Dispose_m13998(__this, method) (( void (*) (Enumerator_t2629 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::VerifyState()
#define Enumerator_VerifyState_m13999(__this, method) (( void (*) (Enumerator_t2629 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::MoveNext()
#define Enumerator_MoveNext_m14000(__this, method) (( bool (*) (Enumerator_t2629 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::get_Current()
#define Enumerator_get_Current_m14001(__this, method) (( Graphic_t188 * (*) (Enumerator_t2629 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
