﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t2655;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t2654;
// System.Object[]
struct ObjectU5BU5D_t29;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m14317_gshared (InvokableCall_1_t2655 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m14317(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2655 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m14317_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m14318_gshared (InvokableCall_1_t2655 * __this, UnityAction_1_t2654 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m14318(__this, ___callback, method) (( void (*) (InvokableCall_1_t2655 *, UnityAction_1_t2654 *, const MethodInfo*))InvokableCall_1__ctor_m14318_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m14319_gshared (InvokableCall_1_t2655 * __this, ObjectU5BU5D_t29* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m14319(__this, ___args, method) (( void (*) (InvokableCall_1_t2655 *, ObjectU5BU5D_t29*, const MethodInfo*))InvokableCall_1_Invoke_m14319_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m14320_gshared (InvokableCall_1_t2655 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m14320(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2655 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m14320_gshared)(__this, ___targetObj, ___method, method)
