﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1419;
// Mono.Math.BigInteger
struct BigInteger_t1418;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m6856 (ModulusRing_t1419 * __this, BigInteger_t1418 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m6857 (ModulusRing_t1419 * __this, BigInteger_t1418 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1418 * ModulusRing_Multiply_m6858 (ModulusRing_t1419 * __this, BigInteger_t1418 * ___a, BigInteger_t1418 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1418 * ModulusRing_Difference_m6859 (ModulusRing_t1419 * __this, BigInteger_t1418 * ___a, BigInteger_t1418 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1418 * ModulusRing_Pow_m6860 (ModulusRing_t1419 * __this, BigInteger_t1418 * ___a, BigInteger_t1418 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t1418 * ModulusRing_Pow_m6861 (ModulusRing_t1419 * __this, uint32_t ___b, BigInteger_t1418 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
