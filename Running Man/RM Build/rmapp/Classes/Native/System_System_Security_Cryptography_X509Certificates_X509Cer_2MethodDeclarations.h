﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
struct X509Certificate2Enumerator_t1039;
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t1036;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
struct X509Certificate2Collection_t1038;

// System.Void System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate2Collection)
extern "C" void X509Certificate2Enumerator__ctor_m4051 (X509Certificate2Enumerator_t1039 * __this, X509Certificate2Collection_t1038 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m4052 (X509Certificate2Enumerator_t1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::System.Collections.IEnumerator.MoveNext()
extern "C" bool X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m4053 (X509Certificate2Enumerator_t1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::get_Current()
extern "C" X509Certificate2_t1036 * X509Certificate2Enumerator_get_Current_m4054 (X509Certificate2Enumerator_t1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator::MoveNext()
extern "C" bool X509Certificate2Enumerator_MoveNext_m4055 (X509Certificate2Enumerator_t1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
