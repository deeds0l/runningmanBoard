﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t1621;

// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern "C" void ConstructionLevelActivator__ctor_m8503 (ConstructionLevelActivator_t1621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
