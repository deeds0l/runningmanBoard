﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1067;
// System.Text.StringBuilder
struct StringBuilder_t338;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t1068  : public MulticastDelegate_t216
{
};
