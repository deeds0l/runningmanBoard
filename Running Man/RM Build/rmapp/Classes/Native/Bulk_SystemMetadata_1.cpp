﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition System.Text.RegularExpressions.IMachineFactory
extern TypeInfo IMachineFactory_t1080_il2cpp_TypeInfo;
extern const Il2CppType IMachine_t1076_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.IMachineFactory::NewInstance()
extern const MethodInfo IMachineFactory_NewInstance_m4887_MethodInfo = 
{
	"NewInstance"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t1076_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1081_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.IMachineFactory::get_Mapping()
extern const MethodInfo IMachineFactory_get_Mapping_m4888_MethodInfo = 
{
	"get_Mapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1081_0_0_0;
static const ParameterInfo IMachineFactory_t1080_IMachineFactory_set_Mapping_m4889_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &IDictionary_t1081_0_0_0},
};
extern const Il2CppType Void_t71_0_0_0;
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_Mapping(System.Collections.IDictionary)
extern const MethodInfo IMachineFactory_set_Mapping_m4889_MethodInfo = 
{
	"set_Mapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IMachineFactory_t1080_IMachineFactory_set_Mapping_m4889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IMachineFactory::get_GroupCount()
extern const MethodInfo IMachineFactory_get_GroupCount_m4890_MethodInfo = 
{
	"get_GroupCount"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IMachineFactory::get_Gap()
extern const MethodInfo IMachineFactory_get_Gap_m4891_MethodInfo = 
{
	"get_Gap"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IMachineFactory_t1080_IMachineFactory_set_Gap_m4892_ParameterInfos[] = 
{
	{"value", 0, 134218215, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_Gap(System.Int32)
extern const MethodInfo IMachineFactory_set_Gap_m4892_MethodInfo = 
{
	"set_Gap"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, IMachineFactory_t1080_IMachineFactory_set_Gap_m4892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.IMachineFactory::get_NamesMapping()
extern const MethodInfo IMachineFactory_get_NamesMapping_m4893_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
static const ParameterInfo IMachineFactory_t1080_IMachineFactory_set_NamesMapping_m4894_ParameterInfos[] = 
{
	{"value", 0, 134218216, 0, &StringU5BU5D_t45_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IMachineFactory::set_NamesMapping(System.String[])
extern const MethodInfo IMachineFactory_set_NamesMapping_m4894_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, NULL/* method */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, IMachineFactory_t1080_IMachineFactory_set_NamesMapping_m4894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMachineFactory_t1080_MethodInfos[] =
{
	&IMachineFactory_NewInstance_m4887_MethodInfo,
	&IMachineFactory_get_Mapping_m4888_MethodInfo,
	&IMachineFactory_set_Mapping_m4889_MethodInfo,
	&IMachineFactory_get_GroupCount_m4890_MethodInfo,
	&IMachineFactory_get_Gap_m4891_MethodInfo,
	&IMachineFactory_set_Gap_m4892_MethodInfo,
	&IMachineFactory_get_NamesMapping_m4893_MethodInfo,
	&IMachineFactory_set_NamesMapping_m4894_MethodInfo,
	NULL
};
extern const MethodInfo IMachineFactory_get_Mapping_m4888_MethodInfo;
extern const MethodInfo IMachineFactory_set_Mapping_m4889_MethodInfo;
static const PropertyInfo IMachineFactory_t1080____Mapping_PropertyInfo = 
{
	&IMachineFactory_t1080_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &IMachineFactory_get_Mapping_m4888_MethodInfo/* get */
	, &IMachineFactory_set_Mapping_m4889_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_GroupCount_m4890_MethodInfo;
static const PropertyInfo IMachineFactory_t1080____GroupCount_PropertyInfo = 
{
	&IMachineFactory_t1080_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &IMachineFactory_get_GroupCount_m4890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_Gap_m4891_MethodInfo;
extern const MethodInfo IMachineFactory_set_Gap_m4892_MethodInfo;
static const PropertyInfo IMachineFactory_t1080____Gap_PropertyInfo = 
{
	&IMachineFactory_t1080_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &IMachineFactory_get_Gap_m4891_MethodInfo/* get */
	, &IMachineFactory_set_Gap_m4892_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IMachineFactory_get_NamesMapping_m4893_MethodInfo;
extern const MethodInfo IMachineFactory_set_NamesMapping_m4894_MethodInfo;
static const PropertyInfo IMachineFactory_t1080____NamesMapping_PropertyInfo = 
{
	&IMachineFactory_t1080_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &IMachineFactory_get_NamesMapping_m4893_MethodInfo/* get */
	, &IMachineFactory_set_NamesMapping_m4894_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IMachineFactory_t1080_PropertyInfos[] =
{
	&IMachineFactory_t1080____Mapping_PropertyInfo,
	&IMachineFactory_t1080____GroupCount_PropertyInfo,
	&IMachineFactory_t1080____Gap_PropertyInfo,
	&IMachineFactory_t1080____NamesMapping_PropertyInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachineFactory_t1080_0_0_0;
extern const Il2CppType IMachineFactory_t1080_1_0_0;
struct IMachineFactory_t1080;
const Il2CppTypeDefinitionMetadata IMachineFactory_t1080_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMachineFactory_t1080_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachineFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IMachineFactory_t1080_MethodInfos/* methods */
	, IMachineFactory_t1080_PropertyInfos/* properties */
	, NULL/* events */
	, &IMachineFactory_t1080_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachineFactory_t1080_0_0_0/* byval_arg */
	, &IMachineFactory_t1080_1_0_0/* this_arg */
	, &IMachineFactory_t1080_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_Key.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache/Key
extern TypeInfo Key_t1088_il2cpp_TypeInfo;
// System.Text.RegularExpressions.FactoryCache/Key
#include "System_System_Text_RegularExpressions_FactoryCache_KeyMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Key_t1088_Key__ctor_m4303_ParameterInfos[] = 
{
	{"pattern", 0, 134218223, 0, &String_t_0_0_0},
	{"options", 1, 134218224, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache/Key::.ctor(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Key__ctor_m4303_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Key__ctor_m4303/* method */
	, &Key_t1088_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, Key_t1088_Key__ctor_m4303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.FactoryCache/Key::GetHashCode()
extern const MethodInfo Key_GetHashCode_m4304_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Key_GetHashCode_m4304/* method */
	, &Key_t1088_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Key_t1088_Key_Equals_m4305_ParameterInfos[] = 
{
	{"o", 0, 134218225, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t72_0_0_0;
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.FactoryCache/Key::Equals(System.Object)
extern const MethodInfo Key_Equals_m4305_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Key_Equals_m4305/* method */
	, &Key_t1088_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Key_t1088_Key_Equals_m4305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.FactoryCache/Key::ToString()
extern const MethodInfo Key_ToString_m4306_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Key_ToString_m4306/* method */
	, &Key_t1088_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Key_t1088_MethodInfos[] =
{
	&Key__ctor_m4303_MethodInfo,
	&Key_GetHashCode_m4304_MethodInfo,
	&Key_Equals_m4305_MethodInfo,
	&Key_ToString_m4306_MethodInfo,
	NULL
};
extern const MethodInfo Key_Equals_m4305_MethodInfo;
extern const MethodInfo Object_Finalize_m218_MethodInfo;
extern const MethodInfo Key_GetHashCode_m4304_MethodInfo;
extern const MethodInfo Key_ToString_m4306_MethodInfo;
static const Il2CppMethodReference Key_t1088_VTable[] =
{
	&Key_Equals_m4305_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Key_GetHashCode_m4304_MethodInfo,
	&Key_ToString_m4306_MethodInfo,
};
static bool Key_t1088_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Key_t1088_0_0_0;
extern const Il2CppType Key_t1088_1_0_0;
extern TypeInfo FactoryCache_t1079_il2cpp_TypeInfo;
extern const Il2CppType FactoryCache_t1079_0_0_0;
struct Key_t1088;
const Il2CppTypeDefinitionMetadata Key_t1088_DefinitionMetadata = 
{
	&FactoryCache_t1079_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Key_t1088_VTable/* vtableMethods */
	, Key_t1088_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 498/* fieldStart */

};
TypeInfo Key_t1088_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Key"/* name */
	, ""/* namespaze */
	, Key_t1088_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Key_t1088_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Key_t1088_0_0_0/* byval_arg */
	, &Key_t1088_1_0_0/* this_arg */
	, &Key_t1088_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Key_t1088)/* instance_size */
	, sizeof (Key_t1088)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCache.h"
// Metadata Definition System.Text.RegularExpressions.FactoryCache
// System.Text.RegularExpressions.FactoryCache
#include "System_System_Text_RegularExpressions_FactoryCacheMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo FactoryCache_t1079_FactoryCache__ctor_m4307_ParameterInfos[] = 
{
	{"capacity", 0, 134218217, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::.ctor(System.Int32)
extern const MethodInfo FactoryCache__ctor_m4307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FactoryCache__ctor_m4307/* method */
	, &FactoryCache_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, FactoryCache_t1079_FactoryCache__ctor_m4307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
extern const Il2CppType IMachineFactory_t1080_0_0_0;
static const ParameterInfo FactoryCache_t1079_FactoryCache_Add_m4308_ParameterInfos[] = 
{
	{"pattern", 0, 134218218, 0, &String_t_0_0_0},
	{"options", 1, 134218219, 0, &RegexOptions_t1084_0_0_0},
	{"factory", 2, 134218220, 0, &IMachineFactory_t1080_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Add(System.String,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.IMachineFactory)
extern const MethodInfo FactoryCache_Add_m4308_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&FactoryCache_Add_m4308/* method */
	, &FactoryCache_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t/* invoker_method */
	, FactoryCache_t1079_FactoryCache_Add_m4308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.FactoryCache::Cleanup()
extern const MethodInfo FactoryCache_Cleanup_m4309_MethodInfo = 
{
	"Cleanup"/* name */
	, (methodPointerType)&FactoryCache_Cleanup_m4309/* method */
	, &FactoryCache_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo FactoryCache_t1079_FactoryCache_Lookup_m4310_ParameterInfos[] = 
{
	{"pattern", 0, 134218221, 0, &String_t_0_0_0},
	{"options", 1, 134218222, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.FactoryCache::Lookup(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo FactoryCache_Lookup_m4310_MethodInfo = 
{
	"Lookup"/* name */
	, (methodPointerType)&FactoryCache_Lookup_m4310/* method */
	, &FactoryCache_t1079_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1080_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, FactoryCache_t1079_FactoryCache_Lookup_m4310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FactoryCache_t1079_MethodInfos[] =
{
	&FactoryCache__ctor_m4307_MethodInfo,
	&FactoryCache_Add_m4308_MethodInfo,
	&FactoryCache_Cleanup_m4309_MethodInfo,
	&FactoryCache_Lookup_m4310_MethodInfo,
	NULL
};
static const Il2CppType* FactoryCache_t1079_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Key_t1088_0_0_0,
};
extern const MethodInfo Object_Equals_m244_MethodInfo;
extern const MethodInfo Object_GetHashCode_m245_MethodInfo;
extern const MethodInfo Object_ToString_m246_MethodInfo;
static const Il2CppMethodReference FactoryCache_t1079_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool FactoryCache_t1079_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FactoryCache_t1079_1_0_0;
struct FactoryCache_t1079;
const Il2CppTypeDefinitionMetadata FactoryCache_t1079_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FactoryCache_t1079_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FactoryCache_t1079_VTable/* vtableMethods */
	, FactoryCache_t1079_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 500/* fieldStart */

};
TypeInfo FactoryCache_t1079_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FactoryCache"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, FactoryCache_t1079_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FactoryCache_t1079_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FactoryCache_t1079_0_0_0/* byval_arg */
	, &FactoryCache_t1079_1_0_0/* this_arg */
	, &FactoryCache_t1079_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FactoryCache_t1079)/* instance_size */
	, sizeof (FactoryCache_t1079)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_Node.h"
// Metadata Definition System.Text.RegularExpressions.MRUList/Node
extern TypeInfo Node_t1090_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MRUList/Node
#include "System_System_Text_RegularExpressions_MRUList_NodeMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Node_t1090_Node__ctor_m4311_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList/Node::.ctor(System.Object)
extern const MethodInfo Node__ctor_m4311_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Node__ctor_m4311/* method */
	, &Node_t1090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Node_t1090_Node__ctor_m4311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Node_t1090_MethodInfos[] =
{
	&Node__ctor_m4311_MethodInfo,
	NULL
};
static const Il2CppMethodReference Node_t1090_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Node_t1090_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Node_t1090_0_0_0;
extern const Il2CppType Node_t1090_1_0_0;
extern TypeInfo MRUList_t1089_il2cpp_TypeInfo;
extern const Il2CppType MRUList_t1089_0_0_0;
struct Node_t1090;
const Il2CppTypeDefinitionMetadata Node_t1090_DefinitionMetadata = 
{
	&MRUList_t1089_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Node_t1090_VTable/* vtableMethods */
	, Node_t1090_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 503/* fieldStart */

};
TypeInfo Node_t1090_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Node"/* name */
	, ""/* namespaze */
	, Node_t1090_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Node_t1090_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Node_t1090_0_0_0/* byval_arg */
	, &Node_t1090_1_0_0/* this_arg */
	, &Node_t1090_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Node_t1090)/* instance_size */
	, sizeof (Node_t1090)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUList.h"
// Metadata Definition System.Text.RegularExpressions.MRUList
// System.Text.RegularExpressions.MRUList
#include "System_System_Text_RegularExpressions_MRUListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::.ctor()
extern const MethodInfo MRUList__ctor_m4312_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MRUList__ctor_m4312/* method */
	, &MRUList_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MRUList_t1089_MRUList_Use_m4313_ParameterInfos[] = 
{
	{"o", 0, 134218226, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MRUList::Use(System.Object)
extern const MethodInfo MRUList_Use_m4313_MethodInfo = 
{
	"Use"/* name */
	, (methodPointerType)&MRUList_Use_m4313/* method */
	, &MRUList_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, MRUList_t1089_MRUList_Use_m4313_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.MRUList::Evict()
extern const MethodInfo MRUList_Evict_m4314_MethodInfo = 
{
	"Evict"/* name */
	, (methodPointerType)&MRUList_Evict_m4314/* method */
	, &MRUList_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MRUList_t1089_MethodInfos[] =
{
	&MRUList__ctor_m4312_MethodInfo,
	&MRUList_Use_m4313_MethodInfo,
	&MRUList_Evict_m4314_MethodInfo,
	NULL
};
static const Il2CppType* MRUList_t1089_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Node_t1090_0_0_0,
};
static const Il2CppMethodReference MRUList_t1089_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool MRUList_t1089_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MRUList_t1089_1_0_0;
struct MRUList_t1089;
const Il2CppTypeDefinitionMetadata MRUList_t1089_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MRUList_t1089_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MRUList_t1089_VTable/* vtableMethods */
	, MRUList_t1089_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 506/* fieldStart */

};
TypeInfo MRUList_t1089_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MRUList"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MRUList_t1089_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MRUList_t1089_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MRUList_t1089_0_0_0/* byval_arg */
	, &MRUList_t1089_1_0_0/* this_arg */
	, &MRUList_t1089_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MRUList_t1089)/* instance_size */
	, sizeof (MRUList_t1089)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
// Metadata Definition System.Text.RegularExpressions.Category
extern TypeInfo Category_t1091_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_CategoryMethodDeclarations.h"
static const MethodInfo* Category_t1091_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m222_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m223_MethodInfo;
extern const MethodInfo Enum_ToString_m224_MethodInfo;
extern const MethodInfo Enum_ToString_m225_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m226_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m227_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m228_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m229_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m230_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m231_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m232_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m233_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m234_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m235_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m236_MethodInfo;
extern const MethodInfo Enum_ToString_m237_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m238_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m239_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m240_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m241_MethodInfo;
extern const MethodInfo Enum_CompareTo_m242_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m243_MethodInfo;
static const Il2CppMethodReference Category_t1091_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Category_t1091_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t74_0_0_0;
extern const Il2CppType IConvertible_t75_0_0_0;
extern const Il2CppType IComparable_t76_0_0_0;
static Il2CppInterfaceOffsetPair Category_t1091_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Category_t1091_1_0_0;
extern const Il2CppType Enum_t77_0_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t371_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Category_t1091_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Category_t1091_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Category_t1091_VTable/* vtableMethods */
	, Category_t1091_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 508/* fieldStart */

};
TypeInfo Category_t1091_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Category"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Category_t1091_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UInt16_t371_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Category_t1091_0_0_0/* byval_arg */
	, &Category_t1091_1_0_0/* this_arg */
	, &Category_t1091_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Category_t1091)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Category_t1091)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 146/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtils.h"
// Metadata Definition System.Text.RegularExpressions.CategoryUtils
extern TypeInfo CategoryUtils_t1092_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CategoryUtils
#include "System_System_Text_RegularExpressions_CategoryUtilsMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CategoryUtils_t1092_CategoryUtils_CategoryFromName_m4315_ParameterInfos[] = 
{
	{"name", 0, 134218228, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Category_t1091_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.CategoryUtils::CategoryFromName(System.String)
extern const MethodInfo CategoryUtils_CategoryFromName_m4315_MethodInfo = 
{
	"CategoryFromName"/* name */
	, (methodPointerType)&CategoryUtils_CategoryFromName_m4315/* method */
	, &CategoryUtils_t1092_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1091_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1091_Object_t/* invoker_method */
	, CategoryUtils_t1092_CategoryUtils_CategoryFromName_m4315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo CategoryUtils_t1092_CategoryUtils_IsCategory_m4316_ParameterInfos[] = 
{
	{"cat", 0, 134218229, 0, &Category_t1091_0_0_0},
	{"c", 1, 134218230, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_UInt16_t371_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Text.RegularExpressions.Category,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m4316_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m4316/* method */
	, &CategoryUtils_t1092_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_UInt16_t371_Int16_t448/* invoker_method */
	, CategoryUtils_t1092_CategoryUtils_IsCategory_m4316_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnicodeCategory_t1210_0_0_0;
extern const Il2CppType UnicodeCategory_t1210_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo CategoryUtils_t1092_CategoryUtils_IsCategory_m4317_ParameterInfos[] = 
{
	{"uc", 0, 134218231, 0, &UnicodeCategory_t1210_0_0_0},
	{"c", 1, 134218232, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.CategoryUtils::IsCategory(System.Globalization.UnicodeCategory,System.Char)
extern const MethodInfo CategoryUtils_IsCategory_m4317_MethodInfo = 
{
	"IsCategory"/* name */
	, (methodPointerType)&CategoryUtils_IsCategory_m4317/* method */
	, &CategoryUtils_t1092_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54_Int16_t448/* invoker_method */
	, CategoryUtils_t1092_CategoryUtils_IsCategory_m4317_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CategoryUtils_t1092_MethodInfos[] =
{
	&CategoryUtils_CategoryFromName_m4315_MethodInfo,
	&CategoryUtils_IsCategory_m4316_MethodInfo,
	&CategoryUtils_IsCategory_m4317_MethodInfo,
	NULL
};
static const Il2CppMethodReference CategoryUtils_t1092_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool CategoryUtils_t1092_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CategoryUtils_t1092_0_0_0;
extern const Il2CppType CategoryUtils_t1092_1_0_0;
struct CategoryUtils_t1092;
const Il2CppTypeDefinitionMetadata CategoryUtils_t1092_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CategoryUtils_t1092_VTable/* vtableMethods */
	, CategoryUtils_t1092_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CategoryUtils_t1092_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CategoryUtils"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, CategoryUtils_t1092_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CategoryUtils_t1092_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CategoryUtils_t1092_0_0_0/* byval_arg */
	, &CategoryUtils_t1092_1_0_0/* this_arg */
	, &CategoryUtils_t1092_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CategoryUtils_t1092)/* instance_size */
	, sizeof (CategoryUtils_t1092)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRef.h"
// Metadata Definition System.Text.RegularExpressions.LinkRef
extern TypeInfo LinkRef_t1093_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkRef
#include "System_System_Text_RegularExpressions_LinkRefMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern const MethodInfo LinkRef__ctor_m4318_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkRef__ctor_m4318/* method */
	, &LinkRef_t1093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkRef_t1093_MethodInfos[] =
{
	&LinkRef__ctor_m4318_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkRef_t1093_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool LinkRef_t1093_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkRef_t1093_0_0_0;
extern const Il2CppType LinkRef_t1093_1_0_0;
struct LinkRef_t1093;
const Il2CppTypeDefinitionMetadata LinkRef_t1093_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LinkRef_t1093_VTable/* vtableMethods */
	, LinkRef_t1093_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LinkRef_t1093_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkRef"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkRef_t1093_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkRef_t1093_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkRef_t1093_0_0_0/* byval_arg */
	, &LinkRef_t1093_1_0_0/* this_arg */
	, &LinkRef_t1093_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkRef_t1093)/* instance_size */
	, sizeof (LinkRef_t1093)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.ICompiler
extern TypeInfo ICompiler_t1156_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.ICompiler::GetMachineFactory()
extern const MethodInfo ICompiler_GetMachineFactory_m4895_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1080_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFalse()
extern const MethodInfo ICompiler_EmitFalse_m4896_MethodInfo = 
{
	"EmitFalse"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTrue()
extern const MethodInfo ICompiler_EmitTrue_m4897_MethodInfo = 
{
	"EmitTrue"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitCharacter_m4898_ParameterInfos[] = 
{
	{"c", 0, 134218233, 0, &Char_t369_0_0_0},
	{"negate", 1, 134218234, 0, &Boolean_t72_0_0_0},
	{"ignore", 2, 134218235, 0, &Boolean_t72_0_0_0},
	{"reverse", 3, 134218236, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCharacter_m4898_MethodInfo = 
{
	"EmitCharacter"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitCharacter_m4898_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitCategory_m4899_ParameterInfos[] = 
{
	{"cat", 0, 134218237, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218238, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218239, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitCategory_m4899_MethodInfo = 
{
	"EmitCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitCategory_m4899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitNotCategory_m4900_ParameterInfos[] = 
{
	{"cat", 0, 134218240, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218241, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218242, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitNotCategory_m4900_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitNotCategory_m4900_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitRange_m4901_ParameterInfos[] = 
{
	{"lo", 0, 134218243, 0, &Char_t369_0_0_0},
	{"hi", 1, 134218244, 0, &Char_t369_0_0_0},
	{"negate", 2, 134218245, 0, &Boolean_t72_0_0_0},
	{"ignore", 3, 134218246, 0, &Boolean_t72_0_0_0},
	{"reverse", 4, 134218247, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitRange_m4901_MethodInfo = 
{
	"EmitRange"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitRange_m4901_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType BitArray_t1129_0_0_0;
extern const Il2CppType BitArray_t1129_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitSet_m4902_ParameterInfos[] = 
{
	{"lo", 0, 134218248, 0, &Char_t369_0_0_0},
	{"set", 1, 134218249, 0, &BitArray_t1129_0_0_0},
	{"negate", 2, 134218250, 0, &Boolean_t72_0_0_0},
	{"ignore", 3, 134218251, 0, &Boolean_t72_0_0_0},
	{"reverse", 4, 134218252, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitSet_m4902_MethodInfo = 
{
	"EmitSet"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Object_t_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitSet_m4902_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitString_m4903_ParameterInfos[] = 
{
	{"str", 0, 134218253, 0, &String_t_0_0_0},
	{"ignore", 1, 134218254, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218255, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitString_m4903_MethodInfo = 
{
	"EmitString"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitString_m4903_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1087_0_0_0;
extern const Il2CppType Position_t1087_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitPosition_m4904_ParameterInfos[] = 
{
	{"pos", 0, 134218256, 0, &Position_t1087_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo ICompiler_EmitPosition_m4904_MethodInfo = 
{
	"EmitPosition"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitPosition_m4904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitOpen_m4905_ParameterInfos[] = 
{
	{"gid", 0, 134218257, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitOpen(System.Int32)
extern const MethodInfo ICompiler_EmitOpen_m4905_MethodInfo = 
{
	"EmitOpen"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitOpen_m4905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitClose_m4906_ParameterInfos[] = 
{
	{"gid", 0, 134218258, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitClose(System.Int32)
extern const MethodInfo ICompiler_EmitClose_m4906_MethodInfo = 
{
	"EmitClose"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitClose_m4906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitBalanceStart_m4907_ParameterInfos[] = 
{
	{"gid", 0, 134218259, 0, &Int32_t54_0_0_0},
	{"balance", 1, 134218260, 0, &Int32_t54_0_0_0},
	{"capture", 2, 134218261, 0, &Boolean_t72_0_0_0},
	{"tail", 3, 134218262, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBalanceStart_m4907_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitBalanceStart_m4907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBalance()
extern const MethodInfo ICompiler_EmitBalance_m4908_MethodInfo = 
{
	"EmitBalance"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitReference_m4909_ParameterInfos[] = 
{
	{"gid", 0, 134218263, 0, &Int32_t54_0_0_0},
	{"ignore", 1, 134218264, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218265, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo ICompiler_EmitReference_m4909_MethodInfo = 
{
	"EmitReference"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73_SByte_t73/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitReference_m4909_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitIfDefined_m4910_ParameterInfos[] = 
{
	{"gid", 0, 134218266, 0, &Int32_t54_0_0_0},
	{"tail", 1, 134218267, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIfDefined_m4910_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitIfDefined_m4910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitSub_m4911_ParameterInfos[] = 
{
	{"tail", 0, 134218268, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitSub_m4911_MethodInfo = 
{
	"EmitSub"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitSub_m4911_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitTest_m4912_ParameterInfos[] = 
{
	{"yes", 0, 134218269, 0, &LinkRef_t1093_0_0_0},
	{"tail", 1, 134218270, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitTest_m4912_MethodInfo = 
{
	"EmitTest"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitTest_m4912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitBranch_m4913_ParameterInfos[] = 
{
	{"next", 0, 134218271, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitBranch_m4913_MethodInfo = 
{
	"EmitBranch"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitBranch_m4913_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitJump_m4914_ParameterInfos[] = 
{
	{"target", 0, 134218272, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitJump_m4914_MethodInfo = 
{
	"EmitJump"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitJump_m4914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitRepeat_m4915_ParameterInfos[] = 
{
	{"min", 0, 134218273, 0, &Int32_t54_0_0_0},
	{"max", 1, 134218274, 0, &Int32_t54_0_0_0},
	{"lazy", 2, 134218275, 0, &Boolean_t72_0_0_0},
	{"until", 3, 134218276, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitRepeat_m4915_MethodInfo = 
{
	"EmitRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitRepeat_m4915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitUntil_m4916_ParameterInfos[] = 
{
	{"repeat", 0, 134218277, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitUntil_m4916_MethodInfo = 
{
	"EmitUntil"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitUntil_m4916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitIn_m4917_ParameterInfos[] = 
{
	{"tail", 0, 134218278, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitIn_m4917_MethodInfo = 
{
	"EmitIn"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitIn_m4917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitInfo_m4918_ParameterInfos[] = 
{
	{"count", 0, 134218279, 0, &Int32_t54_0_0_0},
	{"min", 1, 134218280, 0, &Int32_t54_0_0_0},
	{"max", 2, 134218281, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo ICompiler_EmitInfo_m4918_MethodInfo = 
{
	"EmitInfo"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitInfo_m4918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitFastRepeat_m4919_ParameterInfos[] = 
{
	{"min", 0, 134218282, 0, &Int32_t54_0_0_0},
	{"max", 1, 134218283, 0, &Int32_t54_0_0_0},
	{"lazy", 2, 134218284, 0, &Boolean_t72_0_0_0},
	{"tail", 3, 134218285, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitFastRepeat_m4919_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitFastRepeat_m4919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_EmitAnchor_m4920_ParameterInfos[] = 
{
	{"reverse", 0, 134218286, 0, &Boolean_t72_0_0_0},
	{"offset", 1, 134218287, 0, &Int32_t54_0_0_0},
	{"tail", 2, 134218288, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_EmitAnchor_m4920_MethodInfo = 
{
	"EmitAnchor"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_Int32_t54_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_EmitAnchor_m4920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitBranchEnd()
extern const MethodInfo ICompiler_EmitBranchEnd_m4921_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::EmitAlternationEnd()
extern const MethodInfo ICompiler_EmitAlternationEnd_m4922_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.ICompiler::NewLink()
extern const MethodInfo ICompiler_NewLink_m4923_MethodInfo = 
{
	"NewLink"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo ICompiler_t1156_ICompiler_ResolveLink_m4924_ParameterInfos[] = 
{
	{"link", 0, 134218289, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ICompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo ICompiler_ResolveLink_m4924_MethodInfo = 
{
	"ResolveLink"/* name */
	, NULL/* method */
	, &ICompiler_t1156_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ICompiler_t1156_ICompiler_ResolveLink_m4924_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICompiler_t1156_MethodInfos[] =
{
	&ICompiler_GetMachineFactory_m4895_MethodInfo,
	&ICompiler_EmitFalse_m4896_MethodInfo,
	&ICompiler_EmitTrue_m4897_MethodInfo,
	&ICompiler_EmitCharacter_m4898_MethodInfo,
	&ICompiler_EmitCategory_m4899_MethodInfo,
	&ICompiler_EmitNotCategory_m4900_MethodInfo,
	&ICompiler_EmitRange_m4901_MethodInfo,
	&ICompiler_EmitSet_m4902_MethodInfo,
	&ICompiler_EmitString_m4903_MethodInfo,
	&ICompiler_EmitPosition_m4904_MethodInfo,
	&ICompiler_EmitOpen_m4905_MethodInfo,
	&ICompiler_EmitClose_m4906_MethodInfo,
	&ICompiler_EmitBalanceStart_m4907_MethodInfo,
	&ICompiler_EmitBalance_m4908_MethodInfo,
	&ICompiler_EmitReference_m4909_MethodInfo,
	&ICompiler_EmitIfDefined_m4910_MethodInfo,
	&ICompiler_EmitSub_m4911_MethodInfo,
	&ICompiler_EmitTest_m4912_MethodInfo,
	&ICompiler_EmitBranch_m4913_MethodInfo,
	&ICompiler_EmitJump_m4914_MethodInfo,
	&ICompiler_EmitRepeat_m4915_MethodInfo,
	&ICompiler_EmitUntil_m4916_MethodInfo,
	&ICompiler_EmitIn_m4917_MethodInfo,
	&ICompiler_EmitInfo_m4918_MethodInfo,
	&ICompiler_EmitFastRepeat_m4919_MethodInfo,
	&ICompiler_EmitAnchor_m4920_MethodInfo,
	&ICompiler_EmitBranchEnd_m4921_MethodInfo,
	&ICompiler_EmitAlternationEnd_m4922_MethodInfo,
	&ICompiler_NewLink_m4923_MethodInfo,
	&ICompiler_ResolveLink_m4924_MethodInfo,
	NULL
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType ICompiler_t1156_1_0_0;
struct ICompiler_t1156;
const Il2CppTypeDefinitionMetadata ICompiler_t1156_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICompiler_t1156_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ICompiler_t1156_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICompiler_t1156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICompiler_t1156_0_0_0/* byval_arg */
	, &ICompiler_t1156_1_0_0/* this_arg */
	, &ICompiler_t1156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactory.h"
// Metadata Definition System.Text.RegularExpressions.InterpreterFactory
extern TypeInfo InterpreterFactory_t1094_il2cpp_TypeInfo;
// System.Text.RegularExpressions.InterpreterFactory
#include "System_System_Text_RegularExpressions_InterpreterFactoryMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1011_0_0_0;
extern const Il2CppType UInt16U5BU5D_t1011_0_0_0;
static const ParameterInfo InterpreterFactory_t1094_InterpreterFactory__ctor_m4319_ParameterInfos[] = 
{
	{"pattern", 0, 134218290, 0, &UInt16U5BU5D_t1011_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::.ctor(System.UInt16[])
extern const MethodInfo InterpreterFactory__ctor_m4319_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InterpreterFactory__ctor_m4319/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InterpreterFactory_t1094_InterpreterFactory__ctor_m4319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.InterpreterFactory::NewInstance()
extern const MethodInfo InterpreterFactory_NewInstance_m4320_MethodInfo = 
{
	"NewInstance"/* name */
	, (methodPointerType)&InterpreterFactory_NewInstance_m4320/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &IMachine_t1076_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_GroupCount()
extern const MethodInfo InterpreterFactory_get_GroupCount_m4321_MethodInfo = 
{
	"get_GroupCount"/* name */
	, (methodPointerType)&InterpreterFactory_get_GroupCount_m4321/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.InterpreterFactory::get_Gap()
extern const MethodInfo InterpreterFactory_get_Gap_m4322_MethodInfo = 
{
	"get_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_get_Gap_m4322/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo InterpreterFactory_t1094_InterpreterFactory_set_Gap_m4323_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Gap(System.Int32)
extern const MethodInfo InterpreterFactory_set_Gap_m4323_MethodInfo = 
{
	"set_Gap"/* name */
	, (methodPointerType)&InterpreterFactory_set_Gap_m4323/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, InterpreterFactory_t1094_InterpreterFactory_set_Gap_m4323_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IDictionary System.Text.RegularExpressions.InterpreterFactory::get_Mapping()
extern const MethodInfo InterpreterFactory_get_Mapping_m4324_MethodInfo = 
{
	"get_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_Mapping_m4324/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_t1081_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IDictionary_t1081_0_0_0;
static const ParameterInfo InterpreterFactory_t1094_InterpreterFactory_set_Mapping_m4325_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &IDictionary_t1081_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_Mapping(System.Collections.IDictionary)
extern const MethodInfo InterpreterFactory_set_Mapping_m4325_MethodInfo = 
{
	"set_Mapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_Mapping_m4325/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InterpreterFactory_t1094_InterpreterFactory_set_Mapping_m4325_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] System.Text.RegularExpressions.InterpreterFactory::get_NamesMapping()
extern const MethodInfo InterpreterFactory_get_NamesMapping_m4326_MethodInfo = 
{
	"get_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_get_NamesMapping_m4326/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t45_0_0_0;
static const ParameterInfo InterpreterFactory_t1094_InterpreterFactory_set_NamesMapping_m4327_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &StringU5BU5D_t45_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.InterpreterFactory::set_NamesMapping(System.String[])
extern const MethodInfo InterpreterFactory_set_NamesMapping_m4327_MethodInfo = 
{
	"set_NamesMapping"/* name */
	, (methodPointerType)&InterpreterFactory_set_NamesMapping_m4327/* method */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, InterpreterFactory_t1094_InterpreterFactory_set_NamesMapping_m4327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InterpreterFactory_t1094_MethodInfos[] =
{
	&InterpreterFactory__ctor_m4319_MethodInfo,
	&InterpreterFactory_NewInstance_m4320_MethodInfo,
	&InterpreterFactory_get_GroupCount_m4321_MethodInfo,
	&InterpreterFactory_get_Gap_m4322_MethodInfo,
	&InterpreterFactory_set_Gap_m4323_MethodInfo,
	&InterpreterFactory_get_Mapping_m4324_MethodInfo,
	&InterpreterFactory_set_Mapping_m4325_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m4326_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m4327_MethodInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_get_GroupCount_m4321_MethodInfo;
static const PropertyInfo InterpreterFactory_t1094____GroupCount_PropertyInfo = 
{
	&InterpreterFactory_t1094_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, &InterpreterFactory_get_GroupCount_m4321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Gap_m4322_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Gap_m4323_MethodInfo;
static const PropertyInfo InterpreterFactory_t1094____Gap_PropertyInfo = 
{
	&InterpreterFactory_t1094_il2cpp_TypeInfo/* parent */
	, "Gap"/* name */
	, &InterpreterFactory_get_Gap_m4322_MethodInfo/* get */
	, &InterpreterFactory_set_Gap_m4323_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_Mapping_m4324_MethodInfo;
extern const MethodInfo InterpreterFactory_set_Mapping_m4325_MethodInfo;
static const PropertyInfo InterpreterFactory_t1094____Mapping_PropertyInfo = 
{
	&InterpreterFactory_t1094_il2cpp_TypeInfo/* parent */
	, "Mapping"/* name */
	, &InterpreterFactory_get_Mapping_m4324_MethodInfo/* get */
	, &InterpreterFactory_set_Mapping_m4325_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo InterpreterFactory_get_NamesMapping_m4326_MethodInfo;
extern const MethodInfo InterpreterFactory_set_NamesMapping_m4327_MethodInfo;
static const PropertyInfo InterpreterFactory_t1094____NamesMapping_PropertyInfo = 
{
	&InterpreterFactory_t1094_il2cpp_TypeInfo/* parent */
	, "NamesMapping"/* name */
	, &InterpreterFactory_get_NamesMapping_m4326_MethodInfo/* get */
	, &InterpreterFactory_set_NamesMapping_m4327_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* InterpreterFactory_t1094_PropertyInfos[] =
{
	&InterpreterFactory_t1094____GroupCount_PropertyInfo,
	&InterpreterFactory_t1094____Gap_PropertyInfo,
	&InterpreterFactory_t1094____Mapping_PropertyInfo,
	&InterpreterFactory_t1094____NamesMapping_PropertyInfo,
	NULL
};
extern const MethodInfo InterpreterFactory_NewInstance_m4320_MethodInfo;
static const Il2CppMethodReference InterpreterFactory_t1094_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&InterpreterFactory_NewInstance_m4320_MethodInfo,
	&InterpreterFactory_get_Mapping_m4324_MethodInfo,
	&InterpreterFactory_set_Mapping_m4325_MethodInfo,
	&InterpreterFactory_get_GroupCount_m4321_MethodInfo,
	&InterpreterFactory_get_Gap_m4322_MethodInfo,
	&InterpreterFactory_set_Gap_m4323_MethodInfo,
	&InterpreterFactory_get_NamesMapping_m4326_MethodInfo,
	&InterpreterFactory_set_NamesMapping_m4327_MethodInfo,
};
static bool InterpreterFactory_t1094_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* InterpreterFactory_t1094_InterfacesTypeInfos[] = 
{
	&IMachineFactory_t1080_0_0_0,
};
static Il2CppInterfaceOffsetPair InterpreterFactory_t1094_InterfacesOffsets[] = 
{
	{ &IMachineFactory_t1080_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType InterpreterFactory_t1094_0_0_0;
extern const Il2CppType InterpreterFactory_t1094_1_0_0;
struct InterpreterFactory_t1094;
const Il2CppTypeDefinitionMetadata InterpreterFactory_t1094_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, InterpreterFactory_t1094_InterfacesTypeInfos/* implementedInterfaces */
	, InterpreterFactory_t1094_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InterpreterFactory_t1094_VTable/* vtableMethods */
	, InterpreterFactory_t1094_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 654/* fieldStart */

};
TypeInfo InterpreterFactory_t1094_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterpreterFactory"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, InterpreterFactory_t1094_MethodInfos/* methods */
	, InterpreterFactory_t1094_PropertyInfos/* properties */
	, NULL/* events */
	, &InterpreterFactory_t1094_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InterpreterFactory_t1094_0_0_0/* byval_arg */
	, &InterpreterFactory_t1094_1_0_0/* this_arg */
	, &InterpreterFactory_t1094_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterpreterFactory_t1094)/* instance_size */
	, sizeof (InterpreterFactory_t1094)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
extern TypeInfo Link_t1095_il2cpp_TypeInfo;
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_PatterMethodDeclarations.h"
static const MethodInfo* Link_t1095_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2116_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2117_MethodInfo;
extern const MethodInfo ValueType_ToString_m2120_MethodInfo;
static const Il2CppMethodReference Link_t1095_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool Link_t1095_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Link_t1095_0_0_0;
extern const Il2CppType Link_t1095_1_0_0;
extern const Il2CppType ValueType_t439_0_0_0;
extern TypeInfo PatternLinkStack_t1096_il2cpp_TypeInfo;
extern const Il2CppType PatternLinkStack_t1096_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1095_DefinitionMetadata = 
{
	&PatternLinkStack_t1096_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Link_t1095_VTable/* vtableMethods */
	, Link_t1095_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 658/* fieldStart */

};
TypeInfo Link_t1095_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1095_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Link_t1095_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1095_0_0_0/* byval_arg */
	, &Link_t1095_1_0_0/* this_arg */
	, &Link_t1095_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t1095)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t1095)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t1095 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::.ctor()
extern const MethodInfo PatternLinkStack__ctor_m4328_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternLinkStack__ctor_m4328/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternLinkStack_t1096_PatternLinkStack_set_BaseAddress_m4329_ParameterInfos[] = 
{
	{"value", 0, 134218364, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_BaseAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_BaseAddress_m4329_MethodInfo = 
{
	"set_BaseAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_BaseAddress_m4329/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, PatternLinkStack_t1096_PatternLinkStack_set_BaseAddress_m4329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::get_OffsetAddress()
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m4330_MethodInfo = 
{
	"get_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_get_OffsetAddress_m4330/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternLinkStack_t1096_PatternLinkStack_set_OffsetAddress_m4331_ParameterInfos[] = 
{
	{"value", 0, 134218365, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::set_OffsetAddress(System.Int32)
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m4331_MethodInfo = 
{
	"set_OffsetAddress"/* name */
	, (methodPointerType)&PatternLinkStack_set_OffsetAddress_m4331/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, PatternLinkStack_t1096_PatternLinkStack_set_OffsetAddress_m4331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternLinkStack_t1096_PatternLinkStack_GetOffset_m4332_ParameterInfos[] = 
{
	{"target_addr", 0, 134218366, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetOffset(System.Int32)
extern const MethodInfo PatternLinkStack_GetOffset_m4332_MethodInfo = 
{
	"GetOffset"/* name */
	, (methodPointerType)&PatternLinkStack_GetOffset_m4332/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, PatternLinkStack_t1096_PatternLinkStack_GetOffset_m4332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::GetCurrent()
extern const MethodInfo PatternLinkStack_GetCurrent_m4333_MethodInfo = 
{
	"GetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_GetCurrent_m4333/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PatternLinkStack_t1096_PatternLinkStack_SetCurrent_m4334_ParameterInfos[] = 
{
	{"l", 0, 134218367, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::SetCurrent(System.Object)
extern const MethodInfo PatternLinkStack_SetCurrent_m4334_MethodInfo = 
{
	"SetCurrent"/* name */
	, (methodPointerType)&PatternLinkStack_SetCurrent_m4334/* method */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternLinkStack_t1096_PatternLinkStack_SetCurrent_m4334_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternLinkStack_t1096_MethodInfos[] =
{
	&PatternLinkStack__ctor_m4328_MethodInfo,
	&PatternLinkStack_set_BaseAddress_m4329_MethodInfo,
	&PatternLinkStack_get_OffsetAddress_m4330_MethodInfo,
	&PatternLinkStack_set_OffsetAddress_m4331_MethodInfo,
	&PatternLinkStack_GetOffset_m4332_MethodInfo,
	&PatternLinkStack_GetCurrent_m4333_MethodInfo,
	&PatternLinkStack_SetCurrent_m4334_MethodInfo,
	NULL
};
extern const MethodInfo PatternLinkStack_set_BaseAddress_m4329_MethodInfo;
static const PropertyInfo PatternLinkStack_t1096____BaseAddress_PropertyInfo = 
{
	&PatternLinkStack_t1096_il2cpp_TypeInfo/* parent */
	, "BaseAddress"/* name */
	, NULL/* get */
	, &PatternLinkStack_set_BaseAddress_m4329_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PatternLinkStack_get_OffsetAddress_m4330_MethodInfo;
extern const MethodInfo PatternLinkStack_set_OffsetAddress_m4331_MethodInfo;
static const PropertyInfo PatternLinkStack_t1096____OffsetAddress_PropertyInfo = 
{
	&PatternLinkStack_t1096_il2cpp_TypeInfo/* parent */
	, "OffsetAddress"/* name */
	, &PatternLinkStack_get_OffsetAddress_m4330_MethodInfo/* get */
	, &PatternLinkStack_set_OffsetAddress_m4331_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternLinkStack_t1096_PropertyInfos[] =
{
	&PatternLinkStack_t1096____BaseAddress_PropertyInfo,
	&PatternLinkStack_t1096____OffsetAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternLinkStack_t1096_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Link_t1095_0_0_0,
};
extern const MethodInfo PatternLinkStack_GetCurrent_m4333_MethodInfo;
extern const MethodInfo PatternLinkStack_SetCurrent_m4334_MethodInfo;
static const Il2CppMethodReference PatternLinkStack_t1096_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&PatternLinkStack_GetCurrent_m4333_MethodInfo,
	&PatternLinkStack_SetCurrent_m4334_MethodInfo,
};
static bool PatternLinkStack_t1096_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternLinkStack_t1096_1_0_0;
extern const Il2CppType LinkStack_t1097_0_0_0;
extern TypeInfo PatternCompiler_t1098_il2cpp_TypeInfo;
extern const Il2CppType PatternCompiler_t1098_0_0_0;
struct PatternLinkStack_t1096;
const Il2CppTypeDefinitionMetadata PatternLinkStack_t1096_DefinitionMetadata = 
{
	&PatternCompiler_t1098_0_0_0/* declaringType */
	, PatternLinkStack_t1096_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkStack_t1097_0_0_0/* parent */
	, PatternLinkStack_t1096_VTable/* vtableMethods */
	, PatternLinkStack_t1096_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 660/* fieldStart */

};
TypeInfo PatternLinkStack_t1096_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternLinkStack"/* name */
	, ""/* namespaze */
	, PatternLinkStack_t1096_MethodInfos/* methods */
	, PatternLinkStack_t1096_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternLinkStack_t1096_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternLinkStack_t1096_0_0_0/* byval_arg */
	, &PatternLinkStack_t1096_1_0_0/* this_arg */
	, &PatternLinkStack_t1096_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternLinkStack_t1096)/* instance_size */
	, sizeof (PatternLinkStack_t1096)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompiler.h"
// Metadata Definition System.Text.RegularExpressions.PatternCompiler
// System.Text.RegularExpressions.PatternCompiler
#include "System_System_Text_RegularExpressions_PatternCompilerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::.ctor()
extern const MethodInfo PatternCompiler__ctor_m4335_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PatternCompiler__ctor_m4335/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1085_0_0_0;
extern const Il2CppType OpCode_t1085_0_0_0;
extern const Il2CppType OpFlags_t1086_0_0_0;
extern const Il2CppType OpFlags_t1086_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EncodeOp_m4336_ParameterInfos[] = 
{
	{"op", 0, 134218294, 0, &OpCode_t1085_0_0_0},
	{"flags", 1, 134218295, 0, &OpFlags_t1086_0_0_0},
};
extern const Il2CppType UInt16_t371_0_0_0;
extern void* RuntimeInvoker_UInt16_t371_UInt16_t371_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.UInt16 System.Text.RegularExpressions.PatternCompiler::EncodeOp(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_EncodeOp_m4336_MethodInfo = 
{
	"EncodeOp"/* name */
	, (methodPointerType)&PatternCompiler_EncodeOp_m4336/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &UInt16_t371_0_0_0/* return_type */
	, RuntimeInvoker_UInt16_t371_UInt16_t371_UInt16_t371/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EncodeOp_m4336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.PatternCompiler::GetMachineFactory()
extern const MethodInfo PatternCompiler_GetMachineFactory_m4337_MethodInfo = 
{
	"GetMachineFactory"/* name */
	, (methodPointerType)&PatternCompiler_GetMachineFactory_m4337/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &IMachineFactory_t1080_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFalse()
extern const MethodInfo PatternCompiler_EmitFalse_m4338_MethodInfo = 
{
	"EmitFalse"/* name */
	, (methodPointerType)&PatternCompiler_EmitFalse_m4338/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTrue()
extern const MethodInfo PatternCompiler_EmitTrue_m4339_MethodInfo = 
{
	"EmitTrue"/* name */
	, (methodPointerType)&PatternCompiler_EmitTrue_m4339/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitCount_m4340_ParameterInfos[] = 
{
	{"count", 0, 134218296, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCount(System.Int32)
extern const MethodInfo PatternCompiler_EmitCount_m4340_MethodInfo = 
{
	"EmitCount"/* name */
	, (methodPointerType)&PatternCompiler_EmitCount_m4340/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitCount_m4340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitCharacter_m4341_ParameterInfos[] = 
{
	{"c", 0, 134218297, 0, &Char_t369_0_0_0},
	{"negate", 1, 134218298, 0, &Boolean_t72_0_0_0},
	{"ignore", 2, 134218299, 0, &Boolean_t72_0_0_0},
	{"reverse", 3, 134218300, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCharacter(System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCharacter_m4341_MethodInfo = 
{
	"EmitCharacter"/* name */
	, (methodPointerType)&PatternCompiler_EmitCharacter_m4341/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitCharacter_m4341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitCategory_m4342_ParameterInfos[] = 
{
	{"cat", 0, 134218301, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218302, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218303, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitCategory_m4342_MethodInfo = 
{
	"EmitCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitCategory_m4342/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitCategory_m4342_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitNotCategory_m4343_ParameterInfos[] = 
{
	{"cat", 0, 134218304, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218305, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218306, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitNotCategory(System.Text.RegularExpressions.Category,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitNotCategory_m4343_MethodInfo = 
{
	"EmitNotCategory"/* name */
	, (methodPointerType)&PatternCompiler_EmitNotCategory_m4343/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitNotCategory_m4343_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitRange_m4344_ParameterInfos[] = 
{
	{"lo", 0, 134218307, 0, &Char_t369_0_0_0},
	{"hi", 1, 134218308, 0, &Char_t369_0_0_0},
	{"negate", 2, 134218309, 0, &Boolean_t72_0_0_0},
	{"ignore", 3, 134218310, 0, &Boolean_t72_0_0_0},
	{"reverse", 4, 134218311, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRange(System.Char,System.Char,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitRange_m4344_MethodInfo = 
{
	"EmitRange"/* name */
	, (methodPointerType)&PatternCompiler_EmitRange_m4344/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Int16_t448_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitRange_m4344_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType BitArray_t1129_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitSet_m4345_ParameterInfos[] = 
{
	{"lo", 0, 134218312, 0, &Char_t369_0_0_0},
	{"set", 1, 134218313, 0, &BitArray_t1129_0_0_0},
	{"negate", 2, 134218314, 0, &Boolean_t72_0_0_0},
	{"ignore", 3, 134218315, 0, &Boolean_t72_0_0_0},
	{"reverse", 4, 134218316, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSet(System.Char,System.Collections.BitArray,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitSet_m4345_MethodInfo = 
{
	"EmitSet"/* name */
	, (methodPointerType)&PatternCompiler_EmitSet_m4345/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Object_t_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitSet_m4345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitString_m4346_ParameterInfos[] = 
{
	{"str", 0, 134218317, 0, &String_t_0_0_0},
	{"ignore", 1, 134218318, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218319, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitString(System.String,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitString_m4346_MethodInfo = 
{
	"EmitString"/* name */
	, (methodPointerType)&PatternCompiler_EmitString_m4346/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitString_m4346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1087_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitPosition_m4347_ParameterInfos[] = 
{
	{"pos", 0, 134218320, 0, &Position_t1087_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitPosition(System.Text.RegularExpressions.Position)
extern const MethodInfo PatternCompiler_EmitPosition_m4347_MethodInfo = 
{
	"EmitPosition"/* name */
	, (methodPointerType)&PatternCompiler_EmitPosition_m4347/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitPosition_m4347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitOpen_m4348_ParameterInfos[] = 
{
	{"gid", 0, 134218321, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitOpen(System.Int32)
extern const MethodInfo PatternCompiler_EmitOpen_m4348_MethodInfo = 
{
	"EmitOpen"/* name */
	, (methodPointerType)&PatternCompiler_EmitOpen_m4348/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitOpen_m4348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitClose_m4349_ParameterInfos[] = 
{
	{"gid", 0, 134218322, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitClose(System.Int32)
extern const MethodInfo PatternCompiler_EmitClose_m4349_MethodInfo = 
{
	"EmitClose"/* name */
	, (methodPointerType)&PatternCompiler_EmitClose_m4349/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitClose_m4349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitBalanceStart_m4350_ParameterInfos[] = 
{
	{"gid", 0, 134218323, 0, &Int32_t54_0_0_0},
	{"balance", 1, 134218324, 0, &Int32_t54_0_0_0},
	{"capture", 2, 134218325, 0, &Boolean_t72_0_0_0},
	{"tail", 3, 134218326, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalanceStart(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBalanceStart_m4350_MethodInfo = 
{
	"EmitBalanceStart"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalanceStart_m4350/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitBalanceStart_m4350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBalance()
extern const MethodInfo PatternCompiler_EmitBalance_m4351_MethodInfo = 
{
	"EmitBalance"/* name */
	, (methodPointerType)&PatternCompiler_EmitBalance_m4351/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitReference_m4352_ParameterInfos[] = 
{
	{"gid", 0, 134218327, 0, &Int32_t54_0_0_0},
	{"ignore", 1, 134218328, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218329, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitReference(System.Int32,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_EmitReference_m4352_MethodInfo = 
{
	"EmitReference"/* name */
	, (methodPointerType)&PatternCompiler_EmitReference_m4352/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitReference_m4352_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitIfDefined_m4353_ParameterInfos[] = 
{
	{"gid", 0, 134218330, 0, &Int32_t54_0_0_0},
	{"tail", 1, 134218331, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIfDefined(System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIfDefined_m4353_MethodInfo = 
{
	"EmitIfDefined"/* name */
	, (methodPointerType)&PatternCompiler_EmitIfDefined_m4353/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitIfDefined_m4353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitSub_m4354_ParameterInfos[] = 
{
	{"tail", 0, 134218332, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitSub(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitSub_m4354_MethodInfo = 
{
	"EmitSub"/* name */
	, (methodPointerType)&PatternCompiler_EmitSub_m4354/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitSub_m4354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitTest_m4355_ParameterInfos[] = 
{
	{"yes", 0, 134218333, 0, &LinkRef_t1093_0_0_0},
	{"tail", 1, 134218334, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitTest(System.Text.RegularExpressions.LinkRef,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitTest_m4355_MethodInfo = 
{
	"EmitTest"/* name */
	, (methodPointerType)&PatternCompiler_EmitTest_m4355/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitTest_m4355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitBranch_m4356_ParameterInfos[] = 
{
	{"next", 0, 134218335, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranch(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitBranch_m4356_MethodInfo = 
{
	"EmitBranch"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranch_m4356/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitBranch_m4356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitJump_m4357_ParameterInfos[] = 
{
	{"target", 0, 134218336, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitJump(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitJump_m4357_MethodInfo = 
{
	"EmitJump"/* name */
	, (methodPointerType)&PatternCompiler_EmitJump_m4357/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitJump_m4357_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitRepeat_m4358_ParameterInfos[] = 
{
	{"min", 0, 134218337, 0, &Int32_t54_0_0_0},
	{"max", 1, 134218338, 0, &Int32_t54_0_0_0},
	{"lazy", 2, 134218339, 0, &Boolean_t72_0_0_0},
	{"until", 3, 134218340, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitRepeat_m4358_MethodInfo = 
{
	"EmitRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitRepeat_m4358/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitRepeat_m4358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitUntil_m4359_ParameterInfos[] = 
{
	{"repeat", 0, 134218341, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitUntil(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitUntil_m4359_MethodInfo = 
{
	"EmitUntil"/* name */
	, (methodPointerType)&PatternCompiler_EmitUntil_m4359/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitUntil_m4359_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitFastRepeat_m4360_ParameterInfos[] = 
{
	{"min", 0, 134218342, 0, &Int32_t54_0_0_0},
	{"max", 1, 134218343, 0, &Int32_t54_0_0_0},
	{"lazy", 2, 134218344, 0, &Boolean_t72_0_0_0},
	{"tail", 3, 134218345, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitFastRepeat(System.Int32,System.Int32,System.Boolean,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitFastRepeat_m4360_MethodInfo = 
{
	"EmitFastRepeat"/* name */
	, (methodPointerType)&PatternCompiler_EmitFastRepeat_m4360/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitFastRepeat_m4360_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitIn_m4361_ParameterInfos[] = 
{
	{"tail", 0, 134218346, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitIn(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitIn_m4361_MethodInfo = 
{
	"EmitIn"/* name */
	, (methodPointerType)&PatternCompiler_EmitIn_m4361/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitIn_m4361_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitAnchor_m4362_ParameterInfos[] = 
{
	{"reverse", 0, 134218347, 0, &Boolean_t72_0_0_0},
	{"offset", 1, 134218348, 0, &Int32_t54_0_0_0},
	{"tail", 2, 134218349, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAnchor(System.Boolean,System.Int32,System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitAnchor_m4362_MethodInfo = 
{
	"EmitAnchor"/* name */
	, (methodPointerType)&PatternCompiler_EmitAnchor_m4362/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_Int32_t54_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitAnchor_m4362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitInfo_m4363_ParameterInfos[] = 
{
	{"count", 0, 134218350, 0, &Int32_t54_0_0_0},
	{"min", 1, 134218351, 0, &Int32_t54_0_0_0},
	{"max", 2, 134218352, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitInfo(System.Int32,System.Int32,System.Int32)
extern const MethodInfo PatternCompiler_EmitInfo_m4363_MethodInfo = 
{
	"EmitInfo"/* name */
	, (methodPointerType)&PatternCompiler_EmitInfo_m4363/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitInfo_m4363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.LinkRef System.Text.RegularExpressions.PatternCompiler::NewLink()
extern const MethodInfo PatternCompiler_NewLink_m4364_MethodInfo = 
{
	"NewLink"/* name */
	, (methodPointerType)&PatternCompiler_NewLink_m4364/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &LinkRef_t1093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_ResolveLink_m4365_ParameterInfos[] = 
{
	{"lref", 0, 134218353, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::ResolveLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_ResolveLink_m4365_MethodInfo = 
{
	"ResolveLink"/* name */
	, (methodPointerType)&PatternCompiler_ResolveLink_m4365/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_ResolveLink_m4365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitBranchEnd()
extern const MethodInfo PatternCompiler_EmitBranchEnd_m4366_MethodInfo = 
{
	"EmitBranchEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitBranchEnd_m4366/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitAlternationEnd()
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m4367_MethodInfo = 
{
	"EmitAlternationEnd"/* name */
	, (methodPointerType)&PatternCompiler_EmitAlternationEnd_m4367/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_MakeFlags_m4368_ParameterInfos[] = 
{
	{"negate", 0, 134218354, 0, &Boolean_t72_0_0_0},
	{"ignore", 1, 134218355, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218356, 0, &Boolean_t72_0_0_0},
	{"lazy", 3, 134218357, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_OpFlags_t1086_SByte_t73_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.OpFlags System.Text.RegularExpressions.PatternCompiler::MakeFlags(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo PatternCompiler_MakeFlags_m4368_MethodInfo = 
{
	"MakeFlags"/* name */
	, (methodPointerType)&PatternCompiler_MakeFlags_m4368/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &OpFlags_t1086_0_0_0/* return_type */
	, RuntimeInvoker_OpFlags_t1086_SByte_t73_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_MakeFlags_m4368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1085_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_Emit_m4369_ParameterInfos[] = 
{
	{"op", 0, 134218358, 0, &OpCode_t1085_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode)
extern const MethodInfo PatternCompiler_Emit_m4369_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m4369/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_Emit_m4369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OpCode_t1085_0_0_0;
extern const Il2CppType OpFlags_t1086_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_Emit_m4370_ParameterInfos[] = 
{
	{"op", 0, 134218359, 0, &OpCode_t1085_0_0_0},
	{"flags", 1, 134218360, 0, &OpFlags_t1086_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.Text.RegularExpressions.OpCode,System.Text.RegularExpressions.OpFlags)
extern const MethodInfo PatternCompiler_Emit_m4370_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m4370/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_UInt16_t371/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_Emit_m4370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt16_t371_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_Emit_m4371_ParameterInfos[] = 
{
	{"word", 0, 134218361, 0, &UInt16_t371_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::Emit(System.UInt16)
extern const MethodInfo PatternCompiler_Emit_m4371_MethodInfo = 
{
	"Emit"/* name */
	, (methodPointerType)&PatternCompiler_Emit_m4371/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_Emit_m4371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.PatternCompiler::get_CurrentAddress()
extern const MethodInfo PatternCompiler_get_CurrentAddress_m4372_MethodInfo = 
{
	"get_CurrentAddress"/* name */
	, (methodPointerType)&PatternCompiler_get_CurrentAddress_m4372/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_BeginLink_m4373_ParameterInfos[] = 
{
	{"lref", 0, 134218362, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::BeginLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_BeginLink_m4373_MethodInfo = 
{
	"BeginLink"/* name */
	, (methodPointerType)&PatternCompiler_BeginLink_m4373/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_BeginLink_m4373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LinkRef_t1093_0_0_0;
static const ParameterInfo PatternCompiler_t1098_PatternCompiler_EmitLink_m4374_ParameterInfos[] = 
{
	{"lref", 0, 134218363, 0, &LinkRef_t1093_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.PatternCompiler::EmitLink(System.Text.RegularExpressions.LinkRef)
extern const MethodInfo PatternCompiler_EmitLink_m4374_MethodInfo = 
{
	"EmitLink"/* name */
	, (methodPointerType)&PatternCompiler_EmitLink_m4374/* method */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, PatternCompiler_t1098_PatternCompiler_EmitLink_m4374_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PatternCompiler_t1098_MethodInfos[] =
{
	&PatternCompiler__ctor_m4335_MethodInfo,
	&PatternCompiler_EncodeOp_m4336_MethodInfo,
	&PatternCompiler_GetMachineFactory_m4337_MethodInfo,
	&PatternCompiler_EmitFalse_m4338_MethodInfo,
	&PatternCompiler_EmitTrue_m4339_MethodInfo,
	&PatternCompiler_EmitCount_m4340_MethodInfo,
	&PatternCompiler_EmitCharacter_m4341_MethodInfo,
	&PatternCompiler_EmitCategory_m4342_MethodInfo,
	&PatternCompiler_EmitNotCategory_m4343_MethodInfo,
	&PatternCompiler_EmitRange_m4344_MethodInfo,
	&PatternCompiler_EmitSet_m4345_MethodInfo,
	&PatternCompiler_EmitString_m4346_MethodInfo,
	&PatternCompiler_EmitPosition_m4347_MethodInfo,
	&PatternCompiler_EmitOpen_m4348_MethodInfo,
	&PatternCompiler_EmitClose_m4349_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m4350_MethodInfo,
	&PatternCompiler_EmitBalance_m4351_MethodInfo,
	&PatternCompiler_EmitReference_m4352_MethodInfo,
	&PatternCompiler_EmitIfDefined_m4353_MethodInfo,
	&PatternCompiler_EmitSub_m4354_MethodInfo,
	&PatternCompiler_EmitTest_m4355_MethodInfo,
	&PatternCompiler_EmitBranch_m4356_MethodInfo,
	&PatternCompiler_EmitJump_m4357_MethodInfo,
	&PatternCompiler_EmitRepeat_m4358_MethodInfo,
	&PatternCompiler_EmitUntil_m4359_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m4360_MethodInfo,
	&PatternCompiler_EmitIn_m4361_MethodInfo,
	&PatternCompiler_EmitAnchor_m4362_MethodInfo,
	&PatternCompiler_EmitInfo_m4363_MethodInfo,
	&PatternCompiler_NewLink_m4364_MethodInfo,
	&PatternCompiler_ResolveLink_m4365_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m4366_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m4367_MethodInfo,
	&PatternCompiler_MakeFlags_m4368_MethodInfo,
	&PatternCompiler_Emit_m4369_MethodInfo,
	&PatternCompiler_Emit_m4370_MethodInfo,
	&PatternCompiler_Emit_m4371_MethodInfo,
	&PatternCompiler_get_CurrentAddress_m4372_MethodInfo,
	&PatternCompiler_BeginLink_m4373_MethodInfo,
	&PatternCompiler_EmitLink_m4374_MethodInfo,
	NULL
};
extern const MethodInfo PatternCompiler_get_CurrentAddress_m4372_MethodInfo;
static const PropertyInfo PatternCompiler_t1098____CurrentAddress_PropertyInfo = 
{
	&PatternCompiler_t1098_il2cpp_TypeInfo/* parent */
	, "CurrentAddress"/* name */
	, &PatternCompiler_get_CurrentAddress_m4372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PatternCompiler_t1098_PropertyInfos[] =
{
	&PatternCompiler_t1098____CurrentAddress_PropertyInfo,
	NULL
};
static const Il2CppType* PatternCompiler_t1098_il2cpp_TypeInfo__nestedTypes[1] =
{
	&PatternLinkStack_t1096_0_0_0,
};
extern const MethodInfo PatternCompiler_GetMachineFactory_m4337_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFalse_m4338_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTrue_m4339_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCharacter_m4341_MethodInfo;
extern const MethodInfo PatternCompiler_EmitCategory_m4342_MethodInfo;
extern const MethodInfo PatternCompiler_EmitNotCategory_m4343_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRange_m4344_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSet_m4345_MethodInfo;
extern const MethodInfo PatternCompiler_EmitString_m4346_MethodInfo;
extern const MethodInfo PatternCompiler_EmitPosition_m4347_MethodInfo;
extern const MethodInfo PatternCompiler_EmitOpen_m4348_MethodInfo;
extern const MethodInfo PatternCompiler_EmitClose_m4349_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalanceStart_m4350_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBalance_m4351_MethodInfo;
extern const MethodInfo PatternCompiler_EmitReference_m4352_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIfDefined_m4353_MethodInfo;
extern const MethodInfo PatternCompiler_EmitSub_m4354_MethodInfo;
extern const MethodInfo PatternCompiler_EmitTest_m4355_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranch_m4356_MethodInfo;
extern const MethodInfo PatternCompiler_EmitJump_m4357_MethodInfo;
extern const MethodInfo PatternCompiler_EmitRepeat_m4358_MethodInfo;
extern const MethodInfo PatternCompiler_EmitUntil_m4359_MethodInfo;
extern const MethodInfo PatternCompiler_EmitIn_m4361_MethodInfo;
extern const MethodInfo PatternCompiler_EmitInfo_m4363_MethodInfo;
extern const MethodInfo PatternCompiler_EmitFastRepeat_m4360_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAnchor_m4362_MethodInfo;
extern const MethodInfo PatternCompiler_EmitBranchEnd_m4366_MethodInfo;
extern const MethodInfo PatternCompiler_EmitAlternationEnd_m4367_MethodInfo;
extern const MethodInfo PatternCompiler_NewLink_m4364_MethodInfo;
extern const MethodInfo PatternCompiler_ResolveLink_m4365_MethodInfo;
static const Il2CppMethodReference PatternCompiler_t1098_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&PatternCompiler_GetMachineFactory_m4337_MethodInfo,
	&PatternCompiler_EmitFalse_m4338_MethodInfo,
	&PatternCompiler_EmitTrue_m4339_MethodInfo,
	&PatternCompiler_EmitCharacter_m4341_MethodInfo,
	&PatternCompiler_EmitCategory_m4342_MethodInfo,
	&PatternCompiler_EmitNotCategory_m4343_MethodInfo,
	&PatternCompiler_EmitRange_m4344_MethodInfo,
	&PatternCompiler_EmitSet_m4345_MethodInfo,
	&PatternCompiler_EmitString_m4346_MethodInfo,
	&PatternCompiler_EmitPosition_m4347_MethodInfo,
	&PatternCompiler_EmitOpen_m4348_MethodInfo,
	&PatternCompiler_EmitClose_m4349_MethodInfo,
	&PatternCompiler_EmitBalanceStart_m4350_MethodInfo,
	&PatternCompiler_EmitBalance_m4351_MethodInfo,
	&PatternCompiler_EmitReference_m4352_MethodInfo,
	&PatternCompiler_EmitIfDefined_m4353_MethodInfo,
	&PatternCompiler_EmitSub_m4354_MethodInfo,
	&PatternCompiler_EmitTest_m4355_MethodInfo,
	&PatternCompiler_EmitBranch_m4356_MethodInfo,
	&PatternCompiler_EmitJump_m4357_MethodInfo,
	&PatternCompiler_EmitRepeat_m4358_MethodInfo,
	&PatternCompiler_EmitUntil_m4359_MethodInfo,
	&PatternCompiler_EmitIn_m4361_MethodInfo,
	&PatternCompiler_EmitInfo_m4363_MethodInfo,
	&PatternCompiler_EmitFastRepeat_m4360_MethodInfo,
	&PatternCompiler_EmitAnchor_m4362_MethodInfo,
	&PatternCompiler_EmitBranchEnd_m4366_MethodInfo,
	&PatternCompiler_EmitAlternationEnd_m4367_MethodInfo,
	&PatternCompiler_NewLink_m4364_MethodInfo,
	&PatternCompiler_ResolveLink_m4365_MethodInfo,
};
static bool PatternCompiler_t1098_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* PatternCompiler_t1098_InterfacesTypeInfos[] = 
{
	&ICompiler_t1156_0_0_0,
};
static Il2CppInterfaceOffsetPair PatternCompiler_t1098_InterfacesOffsets[] = 
{
	{ &ICompiler_t1156_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PatternCompiler_t1098_1_0_0;
struct PatternCompiler_t1098;
const Il2CppTypeDefinitionMetadata PatternCompiler_t1098_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PatternCompiler_t1098_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PatternCompiler_t1098_InterfacesTypeInfos/* implementedInterfaces */
	, PatternCompiler_t1098_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PatternCompiler_t1098_VTable/* vtableMethods */
	, PatternCompiler_t1098_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 661/* fieldStart */

};
TypeInfo PatternCompiler_t1098_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PatternCompiler"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, PatternCompiler_t1098_MethodInfos/* methods */
	, PatternCompiler_t1098_PropertyInfos/* properties */
	, NULL/* events */
	, &PatternCompiler_t1098_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PatternCompiler_t1098_0_0_0/* byval_arg */
	, &PatternCompiler_t1098_1_0_0/* this_arg */
	, &PatternCompiler_t1098_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PatternCompiler_t1098)/* instance_size */
	, sizeof (PatternCompiler_t1098)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// Metadata Definition System.Text.RegularExpressions.LinkStack
extern TypeInfo LinkStack_t1097_il2cpp_TypeInfo;
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
extern const MethodInfo LinkStack__ctor_m4375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkStack__ctor_m4375/* method */
	, &LinkStack_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::Push()
extern const MethodInfo LinkStack_Push_m4376_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&LinkStack_Push_m4376/* method */
	, &LinkStack_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
extern const MethodInfo LinkStack_Pop_m4377_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&LinkStack_Pop_m4377/* method */
	, &LinkStack_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
extern const MethodInfo LinkStack_GetCurrent_m4925_MethodInfo = 
{
	"GetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo LinkStack_t1097_LinkStack_SetCurrent_m4926_ParameterInfos[] = 
{
	{"l", 0, 134218368, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
extern const MethodInfo LinkStack_SetCurrent_m4926_MethodInfo = 
{
	"SetCurrent"/* name */
	, NULL/* method */
	, &LinkStack_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, LinkStack_t1097_LinkStack_SetCurrent_m4926_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LinkStack_t1097_MethodInfos[] =
{
	&LinkStack__ctor_m4375_MethodInfo,
	&LinkStack_Push_m4376_MethodInfo,
	&LinkStack_Pop_m4377_MethodInfo,
	&LinkStack_GetCurrent_m4925_MethodInfo,
	&LinkStack_SetCurrent_m4926_MethodInfo,
	NULL
};
static const Il2CppMethodReference LinkStack_t1097_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
};
static bool LinkStack_t1097_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType LinkStack_t1097_1_0_0;
struct LinkStack_t1097;
const Il2CppTypeDefinitionMetadata LinkStack_t1097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &LinkRef_t1093_0_0_0/* parent */
	, LinkStack_t1097_VTable/* vtableMethods */
	, LinkStack_t1097_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 662/* fieldStart */

};
TypeInfo LinkStack_t1097_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkStack"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, LinkStack_t1097_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LinkStack_t1097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LinkStack_t1097_0_0_0/* byval_arg */
	, &LinkStack_t1097_1_0_0/* this_arg */
	, &LinkStack_t1097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkStack_t1097)/* instance_size */
	, sizeof (LinkStack_t1097)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
// Metadata Definition System.Text.RegularExpressions.Mark
extern TypeInfo Mark_t1099_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_MarkMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
extern const MethodInfo Mark_get_IsDefined_m4378_MethodInfo = 
{
	"get_IsDefined"/* name */
	, (methodPointerType)&Mark_get_IsDefined_m4378/* method */
	, &Mark_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
extern const MethodInfo Mark_get_Index_m4379_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&Mark_get_Index_m4379/* method */
	, &Mark_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
extern const MethodInfo Mark_get_Length_m4380_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&Mark_get_Length_m4380/* method */
	, &Mark_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mark_t1099_MethodInfos[] =
{
	&Mark_get_IsDefined_m4378_MethodInfo,
	&Mark_get_Index_m4379_MethodInfo,
	&Mark_get_Length_m4380_MethodInfo,
	NULL
};
extern const MethodInfo Mark_get_IsDefined_m4378_MethodInfo;
static const PropertyInfo Mark_t1099____IsDefined_PropertyInfo = 
{
	&Mark_t1099_il2cpp_TypeInfo/* parent */
	, "IsDefined"/* name */
	, &Mark_get_IsDefined_m4378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Index_m4379_MethodInfo;
static const PropertyInfo Mark_t1099____Index_PropertyInfo = 
{
	&Mark_t1099_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &Mark_get_Index_m4379_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mark_get_Length_m4380_MethodInfo;
static const PropertyInfo Mark_t1099____Length_PropertyInfo = 
{
	&Mark_t1099_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &Mark_get_Length_m4380_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mark_t1099_PropertyInfos[] =
{
	&Mark_t1099____IsDefined_PropertyInfo,
	&Mark_t1099____Index_PropertyInfo,
	&Mark_t1099____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference Mark_t1099_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool Mark_t1099_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mark_t1099_0_0_0;
extern const Il2CppType Mark_t1099_1_0_0;
const Il2CppTypeDefinitionMetadata Mark_t1099_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Mark_t1099_VTable/* vtableMethods */
	, Mark_t1099_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 663/* fieldStart */

};
TypeInfo Mark_t1099_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mark"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Mark_t1099_MethodInfos/* methods */
	, Mark_t1099_PropertyInfos/* properties */
	, NULL/* events */
	, &Mark_t1099_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mark_t1099_0_0_0/* byval_arg */
	, &Mark_t1099_1_0_0/* this_arg */
	, &Mark_t1099_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mark_t1099)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mark_t1099)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mark_t1099 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStack.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/IntStack
extern TypeInfo IntStack_t1100_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/IntStack
#include "System_System_Text_RegularExpressions_Interpreter_IntStackMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::Pop()
extern const MethodInfo IntStack_Pop_m4381_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&IntStack_Pop_m4381/* method */
	, &IntStack_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IntStack_t1100_IntStack_Push_m4382_ParameterInfos[] = 
{
	{"value", 0, 134218406, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::Push(System.Int32)
extern const MethodInfo IntStack_Push_m4382_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&IntStack_Push_m4382/* method */
	, &IntStack_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, IntStack_t1100_IntStack_Push_m4382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/IntStack::get_Count()
extern const MethodInfo IntStack_get_Count_m4383_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntStack_get_Count_m4383/* method */
	, &IntStack_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IntStack_t1100_IntStack_set_Count_m4384_ParameterInfos[] = 
{
	{"value", 0, 134218407, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/IntStack::set_Count(System.Int32)
extern const MethodInfo IntStack_set_Count_m4384_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&IntStack_set_Count_m4384/* method */
	, &IntStack_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, IntStack_t1100_IntStack_set_Count_m4384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntStack_t1100_MethodInfos[] =
{
	&IntStack_Pop_m4381_MethodInfo,
	&IntStack_Push_m4382_MethodInfo,
	&IntStack_get_Count_m4383_MethodInfo,
	&IntStack_set_Count_m4384_MethodInfo,
	NULL
};
extern const MethodInfo IntStack_get_Count_m4383_MethodInfo;
extern const MethodInfo IntStack_set_Count_m4384_MethodInfo;
static const PropertyInfo IntStack_t1100____Count_PropertyInfo = 
{
	&IntStack_t1100_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntStack_get_Count_m4383_MethodInfo/* get */
	, &IntStack_set_Count_m4384_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntStack_t1100_PropertyInfos[] =
{
	&IntStack_t1100____Count_PropertyInfo,
	NULL
};
static const Il2CppMethodReference IntStack_t1100_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool IntStack_t1100_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntStack_t1100_0_0_0;
extern const Il2CppType IntStack_t1100_1_0_0;
extern TypeInfo Interpreter_t1105_il2cpp_TypeInfo;
extern const Il2CppType Interpreter_t1105_0_0_0;
const Il2CppTypeDefinitionMetadata IntStack_t1100_DefinitionMetadata = 
{
	&Interpreter_t1105_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, IntStack_t1100_VTable/* vtableMethods */
	, IntStack_t1100_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 666/* fieldStart */

};
TypeInfo IntStack_t1100_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntStack"/* name */
	, ""/* namespaze */
	, IntStack_t1100_MethodInfos/* methods */
	, IntStack_t1100_PropertyInfos/* properties */
	, NULL/* events */
	, &IntStack_t1100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntStack_t1100_0_0_0/* byval_arg */
	, &IntStack_t1100_1_0_0/* this_arg */
	, &IntStack_t1100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)IntStack_t1100_marshal/* marshal_to_native_func */
	, (methodPointerType)IntStack_t1100_marshal_back/* marshal_from_native_func */
	, (methodPointerType)IntStack_t1100_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (IntStack_t1100)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntStack_t1100)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntStack_t1100_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatCont.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/RepeatContext
extern TypeInfo RepeatContext_t1101_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/RepeatContext
#include "System_System_Text_RegularExpressions_Interpreter_RepeatContMethodDeclarations.h"
extern const Il2CppType RepeatContext_t1101_0_0_0;
extern const Il2CppType RepeatContext_t1101_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo RepeatContext_t1101_RepeatContext__ctor_m4385_ParameterInfos[] = 
{
	{"previous", 0, 134218408, 0, &RepeatContext_t1101_0_0_0},
	{"min", 1, 134218409, 0, &Int32_t54_0_0_0},
	{"max", 2, 134218410, 0, &Int32_t54_0_0_0},
	{"lazy", 3, 134218411, 0, &Boolean_t72_0_0_0},
	{"expr_pc", 4, 134218412, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::.ctor(System.Text.RegularExpressions.Interpreter/RepeatContext,System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo RepeatContext__ctor_m4385_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RepeatContext__ctor_m4385/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_SByte_t73_Int32_t54/* invoker_method */
	, RepeatContext_t1101_RepeatContext__ctor_m4385_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Count()
extern const MethodInfo RepeatContext_get_Count_m4386_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&RepeatContext_get_Count_m4386/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo RepeatContext_t1101_RepeatContext_set_Count_m4387_ParameterInfos[] = 
{
	{"value", 0, 134218413, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Count(System.Int32)
extern const MethodInfo RepeatContext_set_Count_m4387_MethodInfo = 
{
	"set_Count"/* name */
	, (methodPointerType)&RepeatContext_set_Count_m4387/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, RepeatContext_t1101_RepeatContext_set_Count_m4387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Start()
extern const MethodInfo RepeatContext_get_Start_m4388_MethodInfo = 
{
	"get_Start"/* name */
	, (methodPointerType)&RepeatContext_get_Start_m4388/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo RepeatContext_t1101_RepeatContext_set_Start_m4389_ParameterInfos[] = 
{
	{"value", 0, 134218414, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter/RepeatContext::set_Start(System.Int32)
extern const MethodInfo RepeatContext_set_Start_m4389_MethodInfo = 
{
	"set_Start"/* name */
	, (methodPointerType)&RepeatContext_set_Start_m4389/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, RepeatContext_t1101_RepeatContext_set_Start_m4389_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMinimum()
extern const MethodInfo RepeatContext_get_IsMinimum_m4390_MethodInfo = 
{
	"get_IsMinimum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMinimum_m4390/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsMaximum()
extern const MethodInfo RepeatContext_get_IsMaximum_m4391_MethodInfo = 
{
	"get_IsMaximum"/* name */
	, (methodPointerType)&RepeatContext_get_IsMaximum_m4391/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter/RepeatContext::get_IsLazy()
extern const MethodInfo RepeatContext_get_IsLazy_m4392_MethodInfo = 
{
	"get_IsLazy"/* name */
	, (methodPointerType)&RepeatContext_get_IsLazy_m4392/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter/RepeatContext::get_Expression()
extern const MethodInfo RepeatContext_get_Expression_m4393_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&RepeatContext_get_Expression_m4393/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interpreter/RepeatContext System.Text.RegularExpressions.Interpreter/RepeatContext::get_Previous()
extern const MethodInfo RepeatContext_get_Previous_m4394_MethodInfo = 
{
	"get_Previous"/* name */
	, (methodPointerType)&RepeatContext_get_Previous_m4394/* method */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* declaring_type */
	, &RepeatContext_t1101_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RepeatContext_t1101_MethodInfos[] =
{
	&RepeatContext__ctor_m4385_MethodInfo,
	&RepeatContext_get_Count_m4386_MethodInfo,
	&RepeatContext_set_Count_m4387_MethodInfo,
	&RepeatContext_get_Start_m4388_MethodInfo,
	&RepeatContext_set_Start_m4389_MethodInfo,
	&RepeatContext_get_IsMinimum_m4390_MethodInfo,
	&RepeatContext_get_IsMaximum_m4391_MethodInfo,
	&RepeatContext_get_IsLazy_m4392_MethodInfo,
	&RepeatContext_get_Expression_m4393_MethodInfo,
	&RepeatContext_get_Previous_m4394_MethodInfo,
	NULL
};
extern const MethodInfo RepeatContext_get_Count_m4386_MethodInfo;
extern const MethodInfo RepeatContext_set_Count_m4387_MethodInfo;
static const PropertyInfo RepeatContext_t1101____Count_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &RepeatContext_get_Count_m4386_MethodInfo/* get */
	, &RepeatContext_set_Count_m4387_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Start_m4388_MethodInfo;
extern const MethodInfo RepeatContext_set_Start_m4389_MethodInfo;
static const PropertyInfo RepeatContext_t1101____Start_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "Start"/* name */
	, &RepeatContext_get_Start_m4388_MethodInfo/* get */
	, &RepeatContext_set_Start_m4389_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMinimum_m4390_MethodInfo;
static const PropertyInfo RepeatContext_t1101____IsMinimum_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "IsMinimum"/* name */
	, &RepeatContext_get_IsMinimum_m4390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsMaximum_m4391_MethodInfo;
static const PropertyInfo RepeatContext_t1101____IsMaximum_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "IsMaximum"/* name */
	, &RepeatContext_get_IsMaximum_m4391_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_IsLazy_m4392_MethodInfo;
static const PropertyInfo RepeatContext_t1101____IsLazy_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "IsLazy"/* name */
	, &RepeatContext_get_IsLazy_m4392_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Expression_m4393_MethodInfo;
static const PropertyInfo RepeatContext_t1101____Expression_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &RepeatContext_get_Expression_m4393_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo RepeatContext_get_Previous_m4394_MethodInfo;
static const PropertyInfo RepeatContext_t1101____Previous_PropertyInfo = 
{
	&RepeatContext_t1101_il2cpp_TypeInfo/* parent */
	, "Previous"/* name */
	, &RepeatContext_get_Previous_m4394_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RepeatContext_t1101_PropertyInfos[] =
{
	&RepeatContext_t1101____Count_PropertyInfo,
	&RepeatContext_t1101____Start_PropertyInfo,
	&RepeatContext_t1101____IsMinimum_PropertyInfo,
	&RepeatContext_t1101____IsMaximum_PropertyInfo,
	&RepeatContext_t1101____IsLazy_PropertyInfo,
	&RepeatContext_t1101____Expression_PropertyInfo,
	&RepeatContext_t1101____Previous_PropertyInfo,
	NULL
};
static const Il2CppMethodReference RepeatContext_t1101_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool RepeatContext_t1101_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RepeatContext_t1101_1_0_0;
struct RepeatContext_t1101;
const Il2CppTypeDefinitionMetadata RepeatContext_t1101_DefinitionMetadata = 
{
	&Interpreter_t1105_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RepeatContext_t1101_VTable/* vtableMethods */
	, RepeatContext_t1101_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 668/* fieldStart */

};
TypeInfo RepeatContext_t1101_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RepeatContext"/* name */
	, ""/* namespaze */
	, RepeatContext_t1101_MethodInfos/* methods */
	, RepeatContext_t1101_PropertyInfos/* properties */
	, NULL/* events */
	, &RepeatContext_t1101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RepeatContext_t1101_0_0_0/* byval_arg */
	, &RepeatContext_t1101_1_0_0/* this_arg */
	, &RepeatContext_t1101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RepeatContext_t1101)/* instance_size */
	, sizeof (RepeatContext_t1101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_Mode.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter/Mode
extern TypeInfo Mode_t1102_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interpreter/Mode
#include "System_System_Text_RegularExpressions_Interpreter_ModeMethodDeclarations.h"
static const MethodInfo* Mode_t1102_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Mode_t1102_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool Mode_t1102_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Mode_t1102_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Mode_t1102_0_0_0;
extern const Il2CppType Mode_t1102_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t54_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Mode_t1102_DefinitionMetadata = 
{
	&Interpreter_t1105_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t1102_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, Mode_t1102_VTable/* vtableMethods */
	, Mode_t1102_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 675/* fieldStart */

};
TypeInfo Mode_t1102_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, Mode_t1102_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mode_t1102_0_0_0/* byval_arg */
	, &Mode_t1102_1_0_0/* this_arg */
	, &Mode_t1102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t1102)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t1102)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_Interpreter.h"
// Metadata Definition System.Text.RegularExpressions.Interpreter
// System.Text.RegularExpressions.Interpreter
#include "System_System_Text_RegularExpressions_InterpreterMethodDeclarations.h"
extern const Il2CppType UInt16U5BU5D_t1011_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter__ctor_m4395_ParameterInfos[] = 
{
	{"program", 0, 134218369, 0, &UInt16U5BU5D_t1011_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::.ctor(System.UInt16[])
extern const MethodInfo Interpreter__ctor_m4395_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interpreter__ctor_m4395/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Interpreter_t1105_Interpreter__ctor_m4395_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_ReadProgramCount_m4396_ParameterInfos[] = 
{
	{"ptr", 0, 134218370, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::ReadProgramCount(System.Int32)
extern const MethodInfo Interpreter_ReadProgramCount_m4396_MethodInfo = 
{
	"ReadProgramCount"/* name */
	, (methodPointerType)&Interpreter_ReadProgramCount_m4396/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_ReadProgramCount_m4396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t783_0_0_0;
extern const Il2CppType Regex_t783_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Scan_m4397_ParameterInfos[] = 
{
	{"regex", 0, 134218371, 0, &Regex_t783_0_0_0},
	{"text", 1, 134218372, 0, &String_t_0_0_0},
	{"start", 2, 134218373, 0, &Int32_t54_0_0_0},
	{"end", 3, 134218374, 0, &Int32_t54_0_0_0},
};
extern const Il2CppType Match_t1067_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::Scan(System.Text.RegularExpressions.Regex,System.String,System.Int32,System.Int32)
extern const MethodInfo Interpreter_Scan_m4397_MethodInfo = 
{
	"Scan"/* name */
	, (methodPointerType)&Interpreter_Scan_m4397/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Match_t1067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Scan_m4397_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Reset()
extern const MethodInfo Interpreter_Reset_m4398_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Interpreter_Reset_m4398/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1102_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Eval_m4399_ParameterInfos[] = 
{
	{"mode", 0, 134218375, 0, &Mode_t1102_0_0_0},
	{"ref_ptr", 1, 134218376, 0, &Int32_t54_1_0_0},
	{"pc", 2, 134218377, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Eval(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32)
extern const MethodInfo Interpreter_Eval_m4399_MethodInfo = 
{
	"Eval"/* name */
	, (methodPointerType)&Interpreter_Eval_m4399/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Eval_m4399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Mode_t1102_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_EvalChar_m4400_ParameterInfos[] = 
{
	{"mode", 0, 134218378, 0, &Mode_t1102_0_0_0},
	{"ptr", 1, 134218379, 0, &Int32_t54_1_0_0},
	{"pc", 2, 134218380, 0, &Int32_t54_1_0_0},
	{"multi", 3, 134218381, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32U26_t449_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::EvalChar(System.Text.RegularExpressions.Interpreter/Mode,System.Int32&,System.Int32&,System.Boolean)
extern const MethodInfo Interpreter_EvalChar_m4400_MethodInfo = 
{
	"EvalChar"/* name */
	, (methodPointerType)&Interpreter_EvalChar_m4400/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54_Int32U26_t449_Int32U26_t449_SByte_t73/* invoker_method */
	, Interpreter_t1105_Interpreter_EvalChar_m4400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_TryMatch_m4401_ParameterInfos[] = 
{
	{"ref_ptr", 0, 134218382, 0, &Int32_t54_1_0_0},
	{"pc", 1, 134218383, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::TryMatch(System.Int32&,System.Int32)
extern const MethodInfo Interpreter_TryMatch_m4401_MethodInfo = 
{
	"TryMatch"/* name */
	, (methodPointerType)&Interpreter_TryMatch_m4401/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_TryMatch_m4401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Position_t1087_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_IsPosition_m4402_ParameterInfos[] = 
{
	{"pos", 0, 134218384, 0, &Position_t1087_0_0_0},
	{"ptr", 1, 134218385, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_UInt16_t371_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsPosition(System.Text.RegularExpressions.Position,System.Int32)
extern const MethodInfo Interpreter_IsPosition_m4402_MethodInfo = 
{
	"IsPosition"/* name */
	, (methodPointerType)&Interpreter_IsPosition_m4402/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_UInt16_t371_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_IsPosition_m4402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_IsWordChar_m4403_ParameterInfos[] = 
{
	{"c", 0, 134218386, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::IsWordChar(System.Char)
extern const MethodInfo Interpreter_IsWordChar_m4403_MethodInfo = 
{
	"IsWordChar"/* name */
	, (methodPointerType)&Interpreter_IsWordChar_m4403/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int16_t448/* invoker_method */
	, Interpreter_t1105_Interpreter_IsWordChar_m4403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_GetString_m4404_ParameterInfos[] = 
{
	{"pc", 0, 134218387, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Interpreter::GetString(System.Int32)
extern const MethodInfo Interpreter_GetString_m4404_MethodInfo = 
{
	"GetString"/* name */
	, (methodPointerType)&Interpreter_GetString_m4404/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_GetString_m4404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Open_m4405_ParameterInfos[] = 
{
	{"gid", 0, 134218388, 0, &Int32_t54_0_0_0},
	{"ptr", 1, 134218389, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Open(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Open_m4405_MethodInfo = 
{
	"Open"/* name */
	, (methodPointerType)&Interpreter_Open_m4405/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Open_m4405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Close_m4406_ParameterInfos[] = 
{
	{"gid", 0, 134218390, 0, &Int32_t54_0_0_0},
	{"ptr", 1, 134218391, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Close(System.Int32,System.Int32)
extern const MethodInfo Interpreter_Close_m4406_MethodInfo = 
{
	"Close"/* name */
	, (methodPointerType)&Interpreter_Close_m4406/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Close_m4406_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Balance_m4407_ParameterInfos[] = 
{
	{"gid", 0, 134218392, 0, &Int32_t54_0_0_0},
	{"balance_gid", 1, 134218393, 0, &Int32_t54_0_0_0},
	{"capture", 2, 134218394, 0, &Boolean_t72_0_0_0},
	{"ptr", 3, 134218395, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54_SByte_t73_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interpreter::Balance(System.Int32,System.Int32,System.Boolean,System.Int32)
extern const MethodInfo Interpreter_Balance_m4407_MethodInfo = 
{
	"Balance"/* name */
	, (methodPointerType)&Interpreter_Balance_m4407/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54_Int32_t54_SByte_t73_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Balance_m4407_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::Checkpoint()
extern const MethodInfo Interpreter_Checkpoint_m4408_MethodInfo = 
{
	"Checkpoint"/* name */
	, (methodPointerType)&Interpreter_Checkpoint_m4408/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_Backtrack_m4409_ParameterInfos[] = 
{
	{"cp", 0, 134218396, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::Backtrack(System.Int32)
extern const MethodInfo Interpreter_Backtrack_m4409_MethodInfo = 
{
	"Backtrack"/* name */
	, (methodPointerType)&Interpreter_Backtrack_m4409/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_Backtrack_m4409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::ResetGroups()
extern const MethodInfo Interpreter_ResetGroups_m4410_MethodInfo = 
{
	"ResetGroups"/* name */
	, (methodPointerType)&Interpreter_ResetGroups_m4410/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_GetLastDefined_m4411_ParameterInfos[] = 
{
	{"gid", 0, 134218397, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::GetLastDefined(System.Int32)
extern const MethodInfo Interpreter_GetLastDefined_m4411_MethodInfo = 
{
	"GetLastDefined"/* name */
	, (methodPointerType)&Interpreter_GetLastDefined_m4411/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_GetLastDefined_m4411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_CreateMark_m4412_ParameterInfos[] = 
{
	{"previous", 0, 134218398, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interpreter::CreateMark(System.Int32)
extern const MethodInfo Interpreter_CreateMark_m4412_MethodInfo = 
{
	"CreateMark"/* name */
	, (methodPointerType)&Interpreter_CreateMark_m4412/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_CreateMark_m4412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Interpreter_t1105_Interpreter_GetGroupInfo_m4413_ParameterInfos[] = 
{
	{"gid", 0, 134218399, 0, &Int32_t54_0_0_0},
	{"first_mark_index", 1, 134218400, 0, &Int32_t54_1_0_2},
	{"n_caps", 2, 134218401, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::GetGroupInfo(System.Int32,System.Int32&,System.Int32&)
extern const MethodInfo Interpreter_GetGroupInfo_m4413_MethodInfo = 
{
	"GetGroupInfo"/* name */
	, (methodPointerType)&Interpreter_GetGroupInfo_m4413/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Interpreter_t1105_Interpreter_GetGroupInfo_m4413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1073_0_0_0;
extern const Il2CppType Group_t1073_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_PopulateGroup_m4414_ParameterInfos[] = 
{
	{"g", 0, 134218402, 0, &Group_t1073_0_0_0},
	{"first_mark_index", 1, 134218403, 0, &Int32_t54_0_0_0},
	{"n_caps", 2, 134218404, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interpreter::PopulateGroup(System.Text.RegularExpressions.Group,System.Int32,System.Int32)
extern const MethodInfo Interpreter_PopulateGroup_m4414_MethodInfo = 
{
	"PopulateGroup"/* name */
	, (methodPointerType)&Interpreter_PopulateGroup_m4414/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, Interpreter_t1105_Interpreter_PopulateGroup_m4414_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Regex_t783_0_0_0;
static const ParameterInfo Interpreter_t1105_Interpreter_GenerateMatch_m4415_ParameterInfos[] = 
{
	{"regex", 0, 134218405, 0, &Regex_t783_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Interpreter::GenerateMatch(System.Text.RegularExpressions.Regex)
extern const MethodInfo Interpreter_GenerateMatch_m4415_MethodInfo = 
{
	"GenerateMatch"/* name */
	, (methodPointerType)&Interpreter_GenerateMatch_m4415/* method */
	, &Interpreter_t1105_il2cpp_TypeInfo/* declaring_type */
	, &Match_t1067_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Interpreter_t1105_Interpreter_GenerateMatch_m4415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interpreter_t1105_MethodInfos[] =
{
	&Interpreter__ctor_m4395_MethodInfo,
	&Interpreter_ReadProgramCount_m4396_MethodInfo,
	&Interpreter_Scan_m4397_MethodInfo,
	&Interpreter_Reset_m4398_MethodInfo,
	&Interpreter_Eval_m4399_MethodInfo,
	&Interpreter_EvalChar_m4400_MethodInfo,
	&Interpreter_TryMatch_m4401_MethodInfo,
	&Interpreter_IsPosition_m4402_MethodInfo,
	&Interpreter_IsWordChar_m4403_MethodInfo,
	&Interpreter_GetString_m4404_MethodInfo,
	&Interpreter_Open_m4405_MethodInfo,
	&Interpreter_Close_m4406_MethodInfo,
	&Interpreter_Balance_m4407_MethodInfo,
	&Interpreter_Checkpoint_m4408_MethodInfo,
	&Interpreter_Backtrack_m4409_MethodInfo,
	&Interpreter_ResetGroups_m4410_MethodInfo,
	&Interpreter_GetLastDefined_m4411_MethodInfo,
	&Interpreter_CreateMark_m4412_MethodInfo,
	&Interpreter_GetGroupInfo_m4413_MethodInfo,
	&Interpreter_PopulateGroup_m4414_MethodInfo,
	&Interpreter_GenerateMatch_m4415_MethodInfo,
	NULL
};
static const Il2CppType* Interpreter_t1105_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IntStack_t1100_0_0_0,
	&RepeatContext_t1101_0_0_0,
	&Mode_t1102_0_0_0,
};
extern const MethodInfo Interpreter_Scan_m4397_MethodInfo;
extern const MethodInfo BaseMachine_Replace_m4222_MethodInfo;
static const Il2CppMethodReference Interpreter_t1105_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Interpreter_Scan_m4397_MethodInfo,
	&BaseMachine_Replace_m4222_MethodInfo,
	&BaseMachine_Replace_m4222_MethodInfo,
	&Interpreter_Scan_m4397_MethodInfo,
};
static bool Interpreter_t1105_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Interpreter_t1105_InterfacesOffsets[] = 
{
	{ &IMachine_t1076_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interpreter_t1105_1_0_0;
extern const Il2CppType BaseMachine_t1069_0_0_0;
struct Interpreter_t1105;
const Il2CppTypeDefinitionMetadata Interpreter_t1105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Interpreter_t1105_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Interpreter_t1105_InterfacesOffsets/* interfaceOffsets */
	, &BaseMachine_t1069_0_0_0/* parent */
	, Interpreter_t1105_VTable/* vtableMethods */
	, Interpreter_t1105_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 679/* fieldStart */

};
TypeInfo Interpreter_t1105_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interpreter"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interpreter_t1105_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Interpreter_t1105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interpreter_t1105_0_0_0/* byval_arg */
	, &Interpreter_t1105_1_0_0/* this_arg */
	, &Interpreter_t1105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interpreter_t1105)/* instance_size */
	, sizeof (Interpreter_t1105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
// Metadata Definition System.Text.RegularExpressions.Interval
extern TypeInfo Interval_t1106_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_IntervalMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interval_t1106_Interval__ctor_m4416_ParameterInfos[] = 
{
	{"low", 0, 134218415, 0, &Int32_t54_0_0_0},
	{"high", 1, 134218416, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::.ctor(System.Int32,System.Int32)
extern const MethodInfo Interval__ctor_m4416_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Interval__ctor_m4416/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, Interval_t1106_Interval__ctor_m4416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
extern void* RuntimeInvoker_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Interval::get_Empty()
extern const MethodInfo Interval_get_Empty_m4417_MethodInfo = 
{
	"get_Empty"/* name */
	, (methodPointerType)&Interval_get_Empty_m4417/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1106_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1106/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsDiscontiguous()
extern const MethodInfo Interval_get_IsDiscontiguous_m4418_MethodInfo = 
{
	"get_IsDiscontiguous"/* name */
	, (methodPointerType)&Interval_get_IsDiscontiguous_m4418/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsSingleton()
extern const MethodInfo Interval_get_IsSingleton_m4419_MethodInfo = 
{
	"get_IsSingleton"/* name */
	, (methodPointerType)&Interval_get_IsSingleton_m4419/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::get_IsEmpty()
extern const MethodInfo Interval_get_IsEmpty_m4420_MethodInfo = 
{
	"get_IsEmpty"/* name */
	, (methodPointerType)&Interval_get_IsEmpty_m4420/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::get_Size()
extern const MethodInfo Interval_get_Size_m4421_MethodInfo = 
{
	"get_Size"/* name */
	, (methodPointerType)&Interval_get_Size_m4421/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo Interval_t1106_Interval_IsDisjoint_m4422_ParameterInfos[] = 
{
	{"i", 0, 134218417, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsDisjoint(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsDisjoint_m4422_MethodInfo = 
{
	"IsDisjoint"/* name */
	, (methodPointerType)&Interval_IsDisjoint_m4422/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Interval_t1106/* invoker_method */
	, Interval_t1106_Interval_IsDisjoint_m4422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo Interval_t1106_Interval_IsAdjacent_m4423_ParameterInfos[] = 
{
	{"i", 0, 134218418, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::IsAdjacent(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_IsAdjacent_m4423_MethodInfo = 
{
	"IsAdjacent"/* name */
	, (methodPointerType)&Interval_IsAdjacent_m4423/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Interval_t1106/* invoker_method */
	, Interval_t1106_Interval_IsAdjacent_m4423_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo Interval_t1106_Interval_Contains_m4424_ParameterInfos[] = 
{
	{"i", 0, 134218419, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Contains_m4424_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m4424/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Interval_t1106/* invoker_method */
	, Interval_t1106_Interval_Contains_m4424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Interval_t1106_Interval_Contains_m4425_ParameterInfos[] = 
{
	{"i", 0, 134218420, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Contains(System.Int32)
extern const MethodInfo Interval_Contains_m4425_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&Interval_Contains_m4425/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Interval_t1106_Interval_Contains_m4425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo Interval_t1106_Interval_Intersects_m4426_ParameterInfos[] = 
{
	{"i", 0, 134218421, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Interval::Intersects(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Intersects_m4426_MethodInfo = 
{
	"Intersects"/* name */
	, (methodPointerType)&Interval_Intersects_m4426/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Interval_t1106/* invoker_method */
	, Interval_t1106_Interval_Intersects_m4426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo Interval_t1106_Interval_Merge_m4427_ParameterInfos[] = 
{
	{"i", 0, 134218422, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Interval::Merge(System.Text.RegularExpressions.Interval)
extern const MethodInfo Interval_Merge_m4427_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Interval_Merge_m4427/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Interval_t1106/* invoker_method */
	, Interval_t1106_Interval_Merge_m4427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Interval_t1106_Interval_CompareTo_m4428_ParameterInfos[] = 
{
	{"o", 0, 134218423, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Interval::CompareTo(System.Object)
extern const MethodInfo Interval_CompareTo_m4428_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Interval_CompareTo_m4428/* method */
	, &Interval_t1106_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Interval_t1106_Interval_CompareTo_m4428_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Interval_t1106_MethodInfos[] =
{
	&Interval__ctor_m4416_MethodInfo,
	&Interval_get_Empty_m4417_MethodInfo,
	&Interval_get_IsDiscontiguous_m4418_MethodInfo,
	&Interval_get_IsSingleton_m4419_MethodInfo,
	&Interval_get_IsEmpty_m4420_MethodInfo,
	&Interval_get_Size_m4421_MethodInfo,
	&Interval_IsDisjoint_m4422_MethodInfo,
	&Interval_IsAdjacent_m4423_MethodInfo,
	&Interval_Contains_m4424_MethodInfo,
	&Interval_Contains_m4425_MethodInfo,
	&Interval_Intersects_m4426_MethodInfo,
	&Interval_Merge_m4427_MethodInfo,
	&Interval_CompareTo_m4428_MethodInfo,
	NULL
};
extern const MethodInfo Interval_get_Empty_m4417_MethodInfo;
static const PropertyInfo Interval_t1106____Empty_PropertyInfo = 
{
	&Interval_t1106_il2cpp_TypeInfo/* parent */
	, "Empty"/* name */
	, &Interval_get_Empty_m4417_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsDiscontiguous_m4418_MethodInfo;
static const PropertyInfo Interval_t1106____IsDiscontiguous_PropertyInfo = 
{
	&Interval_t1106_il2cpp_TypeInfo/* parent */
	, "IsDiscontiguous"/* name */
	, &Interval_get_IsDiscontiguous_m4418_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsSingleton_m4419_MethodInfo;
static const PropertyInfo Interval_t1106____IsSingleton_PropertyInfo = 
{
	&Interval_t1106_il2cpp_TypeInfo/* parent */
	, "IsSingleton"/* name */
	, &Interval_get_IsSingleton_m4419_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_IsEmpty_m4420_MethodInfo;
static const PropertyInfo Interval_t1106____IsEmpty_PropertyInfo = 
{
	&Interval_t1106_il2cpp_TypeInfo/* parent */
	, "IsEmpty"/* name */
	, &Interval_get_IsEmpty_m4420_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Interval_get_Size_m4421_MethodInfo;
static const PropertyInfo Interval_t1106____Size_PropertyInfo = 
{
	&Interval_t1106_il2cpp_TypeInfo/* parent */
	, "Size"/* name */
	, &Interval_get_Size_m4421_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Interval_t1106_PropertyInfos[] =
{
	&Interval_t1106____Empty_PropertyInfo,
	&Interval_t1106____IsDiscontiguous_PropertyInfo,
	&Interval_t1106____IsSingleton_PropertyInfo,
	&Interval_t1106____IsEmpty_PropertyInfo,
	&Interval_t1106____Size_PropertyInfo,
	NULL
};
extern const MethodInfo Interval_CompareTo_m4428_MethodInfo;
static const Il2CppMethodReference Interval_t1106_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
	&Interval_CompareTo_m4428_MethodInfo,
};
static bool Interval_t1106_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Interval_t1106_InterfacesTypeInfos[] = 
{
	&IComparable_t76_0_0_0,
};
static Il2CppInterfaceOffsetPair Interval_t1106_InterfacesOffsets[] = 
{
	{ &IComparable_t76_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Interval_t1106_1_0_0;
const Il2CppTypeDefinitionMetadata Interval_t1106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Interval_t1106_InterfacesTypeInfos/* implementedInterfaces */
	, Interval_t1106_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, Interval_t1106_VTable/* vtableMethods */
	, Interval_t1106_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 695/* fieldStart */

};
TypeInfo Interval_t1106_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interval"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, Interval_t1106_MethodInfos/* methods */
	, Interval_t1106_PropertyInfos/* properties */
	, NULL/* events */
	, &Interval_t1106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interval_t1106_0_0_0/* byval_arg */
	, &Interval_t1106_1_0_0/* this_arg */
	, &Interval_t1106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Interval_t1106_marshal/* marshal_to_native_func */
	, (methodPointerType)Interval_t1106_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Interval_t1106_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Interval_t1106)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Interval_t1106)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Interval_t1106_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_Enu.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/Enumerator
extern TypeInfo Enumerator_t1107_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/Enumerator
#include "System_System_Text_RegularExpressions_IntervalCollection_EnuMethodDeclarations.h"
extern const Il2CppType IList_t861_0_0_0;
extern const Il2CppType IList_t861_0_0_0;
static const ParameterInfo Enumerator_t1107_Enumerator__ctor_m4429_ParameterInfos[] = 
{
	{"list", 0, 134218433, 0, &IList_t861_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
extern const MethodInfo Enumerator__ctor_m4429_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m4429/* method */
	, &Enumerator_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Enumerator_t1107_Enumerator__ctor_m4429_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
extern const MethodInfo Enumerator_get_Current_m4430_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m4430/* method */
	, &Enumerator_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
extern const MethodInfo Enumerator_MoveNext_m4431_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m4431/* method */
	, &Enumerator_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
extern const MethodInfo Enumerator_Reset_m4432_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Enumerator_Reset_m4432/* method */
	, &Enumerator_t1107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Enumerator_t1107_MethodInfos[] =
{
	&Enumerator__ctor_m4429_MethodInfo,
	&Enumerator_get_Current_m4430_MethodInfo,
	&Enumerator_MoveNext_m4431_MethodInfo,
	&Enumerator_Reset_m4432_MethodInfo,
	NULL
};
extern const MethodInfo Enumerator_get_Current_m4430_MethodInfo;
static const PropertyInfo Enumerator_t1107____Current_PropertyInfo = 
{
	&Enumerator_t1107_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m4430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Enumerator_t1107_PropertyInfos[] =
{
	&Enumerator_t1107____Current_PropertyInfo,
	NULL
};
extern const MethodInfo Enumerator_MoveNext_m4431_MethodInfo;
extern const MethodInfo Enumerator_Reset_m4432_MethodInfo;
static const Il2CppMethodReference Enumerator_t1107_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Enumerator_get_Current_m4430_MethodInfo,
	&Enumerator_MoveNext_m4431_MethodInfo,
	&Enumerator_Reset_m4432_MethodInfo,
};
static bool Enumerator_t1107_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t28_0_0_0;
static const Il2CppType* Enumerator_t1107_InterfacesTypeInfos[] = 
{
	&IEnumerator_t28_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1107_InterfacesOffsets[] = 
{
	{ &IEnumerator_t28_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t1107_0_0_0;
extern const Il2CppType Enumerator_t1107_1_0_0;
extern TypeInfo IntervalCollection_t1109_il2cpp_TypeInfo;
extern const Il2CppType IntervalCollection_t1109_0_0_0;
struct Enumerator_t1107;
const Il2CppTypeDefinitionMetadata Enumerator_t1107_DefinitionMetadata = 
{
	&IntervalCollection_t1109_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1107_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1107_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1107_VTable/* vtableMethods */
	, Enumerator_t1107_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 698/* fieldStart */

};
TypeInfo Enumerator_t1107_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1107_MethodInfos/* methods */
	, Enumerator_t1107_PropertyInfos/* properties */
	, NULL/* events */
	, &Enumerator_t1107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1107_0_0_0/* byval_arg */
	, &Enumerator_t1107_1_0_0/* this_arg */
	, &Enumerator_t1107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1107)/* instance_size */
	, sizeof (Enumerator_t1107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_Cos.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection/CostDelegate
extern TypeInfo CostDelegate_t1108_il2cpp_TypeInfo;
// System.Text.RegularExpressions.IntervalCollection/CostDelegate
#include "System_System_Text_RegularExpressions_IntervalCollection_CosMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CostDelegate_t1108_CostDelegate__ctor_m4433_ParameterInfos[] = 
{
	{"object", 0, 134218434, 0, &Object_t_0_0_0},
	{"method", 1, 134218435, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection/CostDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CostDelegate__ctor_m4433_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CostDelegate__ctor_m4433/* method */
	, &CostDelegate_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, CostDelegate_t1108_CostDelegate__ctor_m4433_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo CostDelegate_t1108_CostDelegate_Invoke_m4434_ParameterInfos[] = 
{
	{"i", 0, 134218436, 0, &Interval_t1106_0_0_0},
};
extern const Il2CppType Double_t752_0_0_0;
extern void* RuntimeInvoker_Double_t752_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::Invoke(System.Text.RegularExpressions.Interval)
extern const MethodInfo CostDelegate_Invoke_m4434_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CostDelegate_Invoke_m4434/* method */
	, &CostDelegate_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Interval_t1106/* invoker_method */
	, CostDelegate_t1108_CostDelegate_Invoke_m4434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CostDelegate_t1108_CostDelegate_BeginInvoke_m4435_ParameterInfos[] = 
{
	{"i", 0, 134218437, 0, &Interval_t1106_0_0_0},
	{"callback", 1, 134218438, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134218439, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t213_0_0_0;
extern void* RuntimeInvoker_Object_t_Interval_t1106_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.IntervalCollection/CostDelegate::BeginInvoke(System.Text.RegularExpressions.Interval,System.AsyncCallback,System.Object)
extern const MethodInfo CostDelegate_BeginInvoke_m4435_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CostDelegate_BeginInvoke_m4435/* method */
	, &CostDelegate_t1108_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Interval_t1106_Object_t_Object_t/* invoker_method */
	, CostDelegate_t1108_CostDelegate_BeginInvoke_m4435_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo CostDelegate_t1108_CostDelegate_EndInvoke_m4436_ParameterInfos[] = 
{
	{"result", 0, 134218440, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.IntervalCollection/CostDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo CostDelegate_EndInvoke_m4436_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CostDelegate_EndInvoke_m4436/* method */
	, &CostDelegate_t1108_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Object_t/* invoker_method */
	, CostDelegate_t1108_CostDelegate_EndInvoke_m4436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CostDelegate_t1108_MethodInfos[] =
{
	&CostDelegate__ctor_m4433_MethodInfo,
	&CostDelegate_Invoke_m4434_MethodInfo,
	&CostDelegate_BeginInvoke_m4435_MethodInfo,
	&CostDelegate_EndInvoke_m4436_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2103_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2104_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2105_MethodInfo;
extern const MethodInfo Delegate_Clone_m2106_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2107_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2108_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2109_MethodInfo;
extern const MethodInfo CostDelegate_Invoke_m4434_MethodInfo;
extern const MethodInfo CostDelegate_BeginInvoke_m4435_MethodInfo;
extern const MethodInfo CostDelegate_EndInvoke_m4436_MethodInfo;
static const Il2CppMethodReference CostDelegate_t1108_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&CostDelegate_Invoke_m4434_MethodInfo,
	&CostDelegate_BeginInvoke_m4435_MethodInfo,
	&CostDelegate_EndInvoke_m4436_MethodInfo,
};
static bool CostDelegate_t1108_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t427_0_0_0;
extern const Il2CppType ISerializable_t428_0_0_0;
static Il2CppInterfaceOffsetPair CostDelegate_t1108_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CostDelegate_t1108_0_0_0;
extern const Il2CppType CostDelegate_t1108_1_0_0;
extern const Il2CppType MulticastDelegate_t216_0_0_0;
struct CostDelegate_t1108;
const Il2CppTypeDefinitionMetadata CostDelegate_t1108_DefinitionMetadata = 
{
	&IntervalCollection_t1109_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CostDelegate_t1108_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, CostDelegate_t1108_VTable/* vtableMethods */
	, CostDelegate_t1108_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CostDelegate_t1108_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CostDelegate"/* name */
	, ""/* namespaze */
	, CostDelegate_t1108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CostDelegate_t1108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CostDelegate_t1108_0_0_0/* byval_arg */
	, &CostDelegate_t1108_1_0_0/* this_arg */
	, &CostDelegate_t1108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CostDelegate_t1108/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CostDelegate_t1108)/* instance_size */
	, sizeof (CostDelegate_t1108)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollection.h"
// Metadata Definition System.Text.RegularExpressions.IntervalCollection
// System.Text.RegularExpressions.IntervalCollection
#include "System_System_Text_RegularExpressions_IntervalCollectionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::.ctor()
extern const MethodInfo IntervalCollection__ctor_m4437_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntervalCollection__ctor_m4437/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IntervalCollection_t1109_IntervalCollection_get_Item_m4438_ParameterInfos[] = 
{
	{"i", 0, 134218424, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1106_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.IntervalCollection::get_Item(System.Int32)
extern const MethodInfo IntervalCollection_get_Item_m4438_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&IntervalCollection_get_Item_m4438/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1106_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1106_Int32_t54/* invoker_method */
	, IntervalCollection_t1109_IntervalCollection_get_Item_m4438_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo IntervalCollection_t1109_IntervalCollection_Add_m4439_ParameterInfos[] = 
{
	{"i", 0, 134218425, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Add(System.Text.RegularExpressions.Interval)
extern const MethodInfo IntervalCollection_Add_m4439_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&IntervalCollection_Add_m4439/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Interval_t1106/* invoker_method */
	, IntervalCollection_t1109_IntervalCollection_Add_m4439_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Normalize()
extern const MethodInfo IntervalCollection_Normalize_m4440_MethodInfo = 
{
	"Normalize"/* name */
	, (methodPointerType)&IntervalCollection_Normalize_m4440/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CostDelegate_t1108_0_0_0;
static const ParameterInfo IntervalCollection_t1109_IntervalCollection_GetMetaCollection_m4441_ParameterInfos[] = 
{
	{"cost_del", 0, 134218426, 0, &CostDelegate_t1108_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.IntervalCollection System.Text.RegularExpressions.IntervalCollection::GetMetaCollection(System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_GetMetaCollection_m4441_MethodInfo = 
{
	"GetMetaCollection"/* name */
	, (methodPointerType)&IntervalCollection_GetMetaCollection_m4441/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &IntervalCollection_t1109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1109_IntervalCollection_GetMetaCollection_m4441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType IntervalCollection_t1109_0_0_0;
extern const Il2CppType CostDelegate_t1108_0_0_0;
static const ParameterInfo IntervalCollection_t1109_IntervalCollection_Optimize_m4442_ParameterInfos[] = 
{
	{"begin", 0, 134218427, 0, &Int32_t54_0_0_0},
	{"end", 1, 134218428, 0, &Int32_t54_0_0_0},
	{"meta", 2, 134218429, 0, &IntervalCollection_t1109_0_0_0},
	{"cost_del", 3, 134218430, 0, &CostDelegate_t1108_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::Optimize(System.Int32,System.Int32,System.Text.RegularExpressions.IntervalCollection,System.Text.RegularExpressions.IntervalCollection/CostDelegate)
extern const MethodInfo IntervalCollection_Optimize_m4442_MethodInfo = 
{
	"Optimize"/* name */
	, (methodPointerType)&IntervalCollection_Optimize_m4442/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_Object_t_Object_t/* invoker_method */
	, IntervalCollection_t1109_IntervalCollection_Optimize_m4442_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.IntervalCollection::get_Count()
extern const MethodInfo IntervalCollection_get_Count_m4443_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&IntervalCollection_get_Count_m4443/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.IntervalCollection::get_IsSynchronized()
extern const MethodInfo IntervalCollection_get_IsSynchronized_m4444_MethodInfo = 
{
	"get_IsSynchronized"/* name */
	, (methodPointerType)&IntervalCollection_get_IsSynchronized_m4444/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Text.RegularExpressions.IntervalCollection::get_SyncRoot()
extern const MethodInfo IntervalCollection_get_SyncRoot_m4445_MethodInfo = 
{
	"get_SyncRoot"/* name */
	, (methodPointerType)&IntervalCollection_get_SyncRoot_m4445/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo IntervalCollection_t1109_IntervalCollection_CopyTo_m4446_ParameterInfos[] = 
{
	{"array", 0, 134218431, 0, &Array_t_0_0_0},
	{"index", 1, 134218432, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.IntervalCollection::CopyTo(System.Array,System.Int32)
extern const MethodInfo IntervalCollection_CopyTo_m4446_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&IntervalCollection_CopyTo_m4446/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, IntervalCollection_t1109_IntervalCollection_CopyTo_m4446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator System.Text.RegularExpressions.IntervalCollection::GetEnumerator()
extern const MethodInfo IntervalCollection_GetEnumerator_m4447_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&IntervalCollection_GetEnumerator_m4447/* method */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntervalCollection_t1109_MethodInfos[] =
{
	&IntervalCollection__ctor_m4437_MethodInfo,
	&IntervalCollection_get_Item_m4438_MethodInfo,
	&IntervalCollection_Add_m4439_MethodInfo,
	&IntervalCollection_Normalize_m4440_MethodInfo,
	&IntervalCollection_GetMetaCollection_m4441_MethodInfo,
	&IntervalCollection_Optimize_m4442_MethodInfo,
	&IntervalCollection_get_Count_m4443_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m4444_MethodInfo,
	&IntervalCollection_get_SyncRoot_m4445_MethodInfo,
	&IntervalCollection_CopyTo_m4446_MethodInfo,
	&IntervalCollection_GetEnumerator_m4447_MethodInfo,
	NULL
};
extern const MethodInfo IntervalCollection_get_Item_m4438_MethodInfo;
static const PropertyInfo IntervalCollection_t1109____Item_PropertyInfo = 
{
	&IntervalCollection_t1109_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IntervalCollection_get_Item_m4438_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_Count_m4443_MethodInfo;
static const PropertyInfo IntervalCollection_t1109____Count_PropertyInfo = 
{
	&IntervalCollection_t1109_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IntervalCollection_get_Count_m4443_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_IsSynchronized_m4444_MethodInfo;
static const PropertyInfo IntervalCollection_t1109____IsSynchronized_PropertyInfo = 
{
	&IntervalCollection_t1109_il2cpp_TypeInfo/* parent */
	, "IsSynchronized"/* name */
	, &IntervalCollection_get_IsSynchronized_m4444_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IntervalCollection_get_SyncRoot_m4445_MethodInfo;
static const PropertyInfo IntervalCollection_t1109____SyncRoot_PropertyInfo = 
{
	&IntervalCollection_t1109_il2cpp_TypeInfo/* parent */
	, "SyncRoot"/* name */
	, &IntervalCollection_get_SyncRoot_m4445_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IntervalCollection_t1109_PropertyInfos[] =
{
	&IntervalCollection_t1109____Item_PropertyInfo,
	&IntervalCollection_t1109____Count_PropertyInfo,
	&IntervalCollection_t1109____IsSynchronized_PropertyInfo,
	&IntervalCollection_t1109____SyncRoot_PropertyInfo,
	NULL
};
static const Il2CppType* IntervalCollection_t1109_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Enumerator_t1107_0_0_0,
	&CostDelegate_t1108_0_0_0,
};
extern const MethodInfo IntervalCollection_CopyTo_m4446_MethodInfo;
extern const MethodInfo IntervalCollection_GetEnumerator_m4447_MethodInfo;
static const Il2CppMethodReference IntervalCollection_t1109_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&IntervalCollection_get_Count_m4443_MethodInfo,
	&IntervalCollection_get_IsSynchronized_m4444_MethodInfo,
	&IntervalCollection_get_SyncRoot_m4445_MethodInfo,
	&IntervalCollection_CopyTo_m4446_MethodInfo,
	&IntervalCollection_GetEnumerator_m4447_MethodInfo,
};
static bool IntervalCollection_t1109_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_t860_0_0_0;
extern const Il2CppType IEnumerable_t464_0_0_0;
static const Il2CppType* IntervalCollection_t1109_InterfacesTypeInfos[] = 
{
	&ICollection_t860_0_0_0,
	&IEnumerable_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair IntervalCollection_t1109_InterfacesOffsets[] = 
{
	{ &ICollection_t860_0_0_0, 4},
	{ &IEnumerable_t464_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IntervalCollection_t1109_1_0_0;
struct IntervalCollection_t1109;
const Il2CppTypeDefinitionMetadata IntervalCollection_t1109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, IntervalCollection_t1109_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, IntervalCollection_t1109_InterfacesTypeInfos/* implementedInterfaces */
	, IntervalCollection_t1109_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntervalCollection_t1109_VTable/* vtableMethods */
	, IntervalCollection_t1109_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 700/* fieldStart */

};
TypeInfo IntervalCollection_t1109_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntervalCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, IntervalCollection_t1109_MethodInfos/* methods */
	, IntervalCollection_t1109_PropertyInfos/* properties */
	, NULL/* events */
	, &IntervalCollection_t1109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 63/* custom_attributes_cache */
	, &IntervalCollection_t1109_0_0_0/* byval_arg */
	, &IntervalCollection_t1109_1_0_0/* this_arg */
	, &IntervalCollection_t1109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntervalCollection_t1109)/* instance_size */
	, sizeof (IntervalCollection_t1109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_Parser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Parser
extern TypeInfo Parser_t1110_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Parser
#include "System_System_Text_RegularExpressions_Syntax_ParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::.ctor()
extern const MethodInfo Parser__ctor_m4448_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Parser__ctor_m4448/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseDecimal_m4449_ParameterInfos[] = 
{
	{"str", 0, 134218441, 0, &String_t_0_0_0},
	{"ptr", 1, 134218442, 0, &Int32_t54_1_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDecimal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseDecimal_m4449_MethodInfo = 
{
	"ParseDecimal"/* name */
	, (methodPointerType)&Parser_ParseDecimal_m4449/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449/* invoker_method */
	, Parser_t1110_Parser_ParseDecimal_m4449_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseOctal_m4450_ParameterInfos[] = 
{
	{"str", 0, 134218443, 0, &String_t_0_0_0},
	{"ptr", 1, 134218444, 0, &Int32_t54_1_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseOctal(System.String,System.Int32&)
extern const MethodInfo Parser_ParseOctal_m4450_MethodInfo = 
{
	"ParseOctal"/* name */
	, (methodPointerType)&Parser_ParseOctal_m4450/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449/* invoker_method */
	, Parser_t1110_Parser_ParseOctal_m4450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseHex_m4451_ParameterInfos[] = 
{
	{"str", 0, 134218445, 0, &String_t_0_0_0},
	{"ptr", 1, 134218446, 0, &Int32_t54_1_0_0},
	{"digits", 2, 134218447, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseHex(System.String,System.Int32&,System.Int32)
extern const MethodInfo Parser_ParseHex_m4451_MethodInfo = 
{
	"ParseHex"/* name */
	, (methodPointerType)&Parser_ParseHex_m4451/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseHex_m4451_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseNumber_m4452_ParameterInfos[] = 
{
	{"str", 0, 134218448, 0, &String_t_0_0_0},
	{"ptr", 1, 134218449, 0, &Int32_t54_1_0_0},
	{"b", 2, 134218450, 0, &Int32_t54_0_0_0},
	{"min", 3, 134218451, 0, &Int32_t54_0_0_0},
	{"max", 4, 134218452, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.String,System.Int32&,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m4452_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m4452/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32U26_t449_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseNumber_m4452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseName_m4453_ParameterInfos[] = 
{
	{"str", 0, 134218453, 0, &String_t_0_0_0},
	{"ptr", 1, 134218454, 0, &Int32_t54_1_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName(System.String,System.Int32&)
extern const MethodInfo Parser_ParseName_m4453_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m4453/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32U26_t449/* invoker_method */
	, Parser_t1110_Parser_ParseName_m4453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseRegularExpression_m4454_ParameterInfos[] = 
{
	{"pattern", 0, 134218455, 0, &String_t_0_0_0},
	{"options", 1, 134218456, 0, &RegexOptions_t1084_0_0_0},
};
extern const Il2CppType RegularExpression_t1116_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.RegularExpression System.Text.RegularExpressions.Syntax.Parser::ParseRegularExpression(System.String,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRegularExpression_m4454_MethodInfo = 
{
	"ParseRegularExpression"/* name */
	, (methodPointerType)&Parser_ParseRegularExpression_m4454/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &RegularExpression_t1116_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseRegularExpression_m4454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t975_0_0_0;
extern const Il2CppType Hashtable_t975_0_0_0;
static const ParameterInfo Parser_t1110_Parser_GetMapping_m4455_ParameterInfos[] = 
{
	{"mapping", 0, 134218457, 0, &Hashtable_t975_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::GetMapping(System.Collections.Hashtable)
extern const MethodInfo Parser_GetMapping_m4455_MethodInfo = 
{
	"GetMapping"/* name */
	, (methodPointerType)&Parser_GetMapping_m4455/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Parser_t1110_Parser_GetMapping_m4455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Group_t1115_0_0_0;
extern const Il2CppType Group_t1115_0_0_0;
extern const Il2CppType RegexOptions_t1084_0_0_0;
extern const Il2CppType Assertion_t1121_0_0_0;
extern const Il2CppType Assertion_t1121_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseGroup_m4456_ParameterInfos[] = 
{
	{"group", 0, 134218458, 0, &Group_t1115_0_0_0},
	{"options", 1, 134218459, 0, &RegexOptions_t1084_0_0_0},
	{"assertion", 2, 134218460, 0, &Assertion_t1121_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseGroup(System.Text.RegularExpressions.Syntax.Group,System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.Syntax.Assertion)
extern const MethodInfo Parser_ParseGroup_m4456_MethodInfo = 
{
	"ParseGroup"/* name */
	, (methodPointerType)&Parser_ParseGroup_m4456/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Object_t/* invoker_method */
	, Parser_t1110_Parser_ParseGroup_m4456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_1_0_0;
extern const Il2CppType RegexOptions_t1084_1_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseGroupingConstruct_m4457_ParameterInfos[] = 
{
	{"options", 0, 134218461, 0, &RegexOptions_t1084_1_0_0},
};
extern const Il2CppType Expression_t1113_0_0_0;
extern void* RuntimeInvoker_Object_t_RegexOptionsU26_t1211 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseGroupingConstruct(System.Text.RegularExpressions.RegexOptions&)
extern const MethodInfo Parser_ParseGroupingConstruct_m4457_MethodInfo = 
{
	"ParseGroupingConstruct"/* name */
	, (methodPointerType)&Parser_ParseGroupingConstruct_m4457/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_RegexOptionsU26_t1211/* invoker_method */
	, Parser_t1110_Parser_ParseGroupingConstruct_m4457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ExpressionAssertion_t1122_0_0_0;
extern const Il2CppType ExpressionAssertion_t1122_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseAssertionType_m4458_ParameterInfos[] = 
{
	{"assertion", 0, 134218462, 0, &ExpressionAssertion_t1122_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseAssertionType(System.Text.RegularExpressions.Syntax.ExpressionAssertion)
extern const MethodInfo Parser_ParseAssertionType_m4458_MethodInfo = 
{
	"ParseAssertionType"/* name */
	, (methodPointerType)&Parser_ParseAssertionType_m4458/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Parser_t1110_Parser_ParseAssertionType_m4458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_1_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseOptions_m4459_ParameterInfos[] = 
{
	{"options", 0, 134218463, 0, &RegexOptions_t1084_1_0_0},
	{"negate", 1, 134218464, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_RegexOptionsU26_t1211_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ParseOptions(System.Text.RegularExpressions.RegexOptions&,System.Boolean)
extern const MethodInfo Parser_ParseOptions_m4459_MethodInfo = 
{
	"ParseOptions"/* name */
	, (methodPointerType)&Parser_ParseOptions_m4459/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_RegexOptionsU26_t1211_SByte_t73/* invoker_method */
	, Parser_t1110_Parser_ParseOptions_m4459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseCharacterClass_m4460_ParameterInfos[] = 
{
	{"options", 0, 134218465, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseCharacterClass(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseCharacterClass_m4460_MethodInfo = 
{
	"ParseCharacterClass"/* name */
	, (methodPointerType)&Parser_ParseCharacterClass_m4460/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseCharacterClass_m4460_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseRepetitionBounds_m4461_ParameterInfos[] = 
{
	{"min", 0, 134218466, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218467, 0, &Int32_t54_1_0_2},
	{"options", 2, 134218468, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::ParseRepetitionBounds(System.Int32&,System.Int32&,System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseRepetitionBounds_m4461_MethodInfo = 
{
	"ParseRepetitionBounds"/* name */
	, (methodPointerType)&Parser_ParseRepetitionBounds_m4461/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32U26_t449_Int32U26_t449_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseRepetitionBounds_m4461_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Category_t1091 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Category System.Text.RegularExpressions.Syntax.Parser::ParseUnicodeCategory()
extern const MethodInfo Parser_ParseUnicodeCategory_m4462_MethodInfo = 
{
	"ParseUnicodeCategory"/* name */
	, (methodPointerType)&Parser_ParseUnicodeCategory_m4462/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Category_t1091_0_0_0/* return_type */
	, RuntimeInvoker_Category_t1091/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseSpecial_m4463_ParameterInfos[] = 
{
	{"options", 0, 134218469, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Parser::ParseSpecial(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_ParseSpecial_m4463_MethodInfo = 
{
	"ParseSpecial"/* name */
	, (methodPointerType)&Parser_ParseSpecial_m4463/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseSpecial_m4463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseEscape()
extern const MethodInfo Parser_ParseEscape_m4464_MethodInfo = 
{
	"ParseEscape"/* name */
	, (methodPointerType)&Parser_ParseEscape_m4464/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.Parser::ParseName()
extern const MethodInfo Parser_ParseName_m4465_MethodInfo = 
{
	"ParseName"/* name */
	, (methodPointerType)&Parser_ParseName_m4465/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsNameChar_m4466_ParameterInfos[] = 
{
	{"c", 0, 134218470, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsNameChar(System.Char)
extern const MethodInfo Parser_IsNameChar_m4466_MethodInfo = 
{
	"IsNameChar"/* name */
	, (methodPointerType)&Parser_IsNameChar_m4466/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int16_t448/* invoker_method */
	, Parser_t1110_Parser_IsNameChar_m4466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseNumber_m4467_ParameterInfos[] = 
{
	{"b", 0, 134218471, 0, &Int32_t54_0_0_0},
	{"min", 1, 134218472, 0, &Int32_t54_0_0_0},
	{"max", 2, 134218473, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseNumber(System.Int32,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseNumber_m4467_MethodInfo = 
{
	"ParseNumber"/* name */
	, (methodPointerType)&Parser_ParseNumber_m4467/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32_t54_Int32_t54_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseNumber_m4467_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ParseDigit_m4468_ParameterInfos[] = 
{
	{"c", 0, 134218474, 0, &Char_t369_0_0_0},
	{"b", 1, 134218475, 0, &Int32_t54_0_0_0},
	{"n", 2, 134218476, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int16_t448_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ParseDigit(System.Char,System.Int32,System.Int32)
extern const MethodInfo Parser_ParseDigit_m4468_MethodInfo = 
{
	"ParseDigit"/* name */
	, (methodPointerType)&Parser_ParseDigit_m4468/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int16_t448_Int32_t54_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_ParseDigit_m4468_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Parser_t1110_Parser_ConsumeWhitespace_m4469_ParameterInfos[] = 
{
	{"ignore", 0, 134218477, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ConsumeWhitespace(System.Boolean)
extern const MethodInfo Parser_ConsumeWhitespace_m4469_MethodInfo = 
{
	"ConsumeWhitespace"/* name */
	, (methodPointerType)&Parser_ConsumeWhitespace_m4469/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Parser_t1110_Parser_ConsumeWhitespace_m4469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::ResolveReferences()
extern const MethodInfo Parser_ResolveReferences_m4470_MethodInfo = 
{
	"ResolveReferences"/* name */
	, (methodPointerType)&Parser_ResolveReferences_m4470/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ArrayList_t985_0_0_0;
extern const Il2CppType ArrayList_t985_0_0_0;
static const ParameterInfo Parser_t1110_Parser_HandleExplicitNumericGroups_m4471_ParameterInfos[] = 
{
	{"explicit_numeric_groups", 0, 134218478, 0, &ArrayList_t985_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Parser::HandleExplicitNumericGroups(System.Collections.ArrayList)
extern const MethodInfo Parser_HandleExplicitNumericGroups_m4471_MethodInfo = 
{
	"HandleExplicitNumericGroups"/* name */
	, (methodPointerType)&Parser_HandleExplicitNumericGroups_m4471/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Parser_t1110_Parser_HandleExplicitNumericGroups_m4471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsIgnoreCase_m4472_ParameterInfos[] = 
{
	{"options", 0, 134218479, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnoreCase(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnoreCase_m4472_MethodInfo = 
{
	"IsIgnoreCase"/* name */
	, (methodPointerType)&Parser_IsIgnoreCase_m4472/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsIgnoreCase_m4472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsMultiline_m4473_ParameterInfos[] = 
{
	{"options", 0, 134218480, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsMultiline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsMultiline_m4473_MethodInfo = 
{
	"IsMultiline"/* name */
	, (methodPointerType)&Parser_IsMultiline_m4473/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsMultiline_m4473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsExplicitCapture_m4474_ParameterInfos[] = 
{
	{"options", 0, 134218481, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsExplicitCapture(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsExplicitCapture_m4474_MethodInfo = 
{
	"IsExplicitCapture"/* name */
	, (methodPointerType)&Parser_IsExplicitCapture_m4474/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsExplicitCapture_m4474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsSingleline_m4475_ParameterInfos[] = 
{
	{"options", 0, 134218482, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsSingleline(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsSingleline_m4475_MethodInfo = 
{
	"IsSingleline"/* name */
	, (methodPointerType)&Parser_IsSingleline_m4475/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsSingleline_m4475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsIgnorePatternWhitespace_m4476_ParameterInfos[] = 
{
	{"options", 0, 134218483, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsIgnorePatternWhitespace(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsIgnorePatternWhitespace_m4476_MethodInfo = 
{
	"IsIgnorePatternWhitespace"/* name */
	, (methodPointerType)&Parser_IsIgnorePatternWhitespace_m4476/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsIgnorePatternWhitespace_m4476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RegexOptions_t1084_0_0_0;
static const ParameterInfo Parser_t1110_Parser_IsECMAScript_m4477_ParameterInfos[] = 
{
	{"options", 0, 134218484, 0, &RegexOptions_t1084_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Parser::IsECMAScript(System.Text.RegularExpressions.RegexOptions)
extern const MethodInfo Parser_IsECMAScript_m4477_MethodInfo = 
{
	"IsECMAScript"/* name */
	, (methodPointerType)&Parser_IsECMAScript_m4477/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int32_t54/* invoker_method */
	, Parser_t1110_Parser_IsECMAScript_m4477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Parser_t1110_Parser_NewParseException_m4478_ParameterInfos[] = 
{
	{"msg", 0, 134218485, 0, &String_t_0_0_0},
};
extern const Il2CppType ArgumentException_t387_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.ArgumentException System.Text.RegularExpressions.Syntax.Parser::NewParseException(System.String)
extern const MethodInfo Parser_NewParseException_m4478_MethodInfo = 
{
	"NewParseException"/* name */
	, (methodPointerType)&Parser_NewParseException_m4478/* method */
	, &Parser_t1110_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentException_t387_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Parser_t1110_Parser_NewParseException_m4478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Parser_t1110_MethodInfos[] =
{
	&Parser__ctor_m4448_MethodInfo,
	&Parser_ParseDecimal_m4449_MethodInfo,
	&Parser_ParseOctal_m4450_MethodInfo,
	&Parser_ParseHex_m4451_MethodInfo,
	&Parser_ParseNumber_m4452_MethodInfo,
	&Parser_ParseName_m4453_MethodInfo,
	&Parser_ParseRegularExpression_m4454_MethodInfo,
	&Parser_GetMapping_m4455_MethodInfo,
	&Parser_ParseGroup_m4456_MethodInfo,
	&Parser_ParseGroupingConstruct_m4457_MethodInfo,
	&Parser_ParseAssertionType_m4458_MethodInfo,
	&Parser_ParseOptions_m4459_MethodInfo,
	&Parser_ParseCharacterClass_m4460_MethodInfo,
	&Parser_ParseRepetitionBounds_m4461_MethodInfo,
	&Parser_ParseUnicodeCategory_m4462_MethodInfo,
	&Parser_ParseSpecial_m4463_MethodInfo,
	&Parser_ParseEscape_m4464_MethodInfo,
	&Parser_ParseName_m4465_MethodInfo,
	&Parser_IsNameChar_m4466_MethodInfo,
	&Parser_ParseNumber_m4467_MethodInfo,
	&Parser_ParseDigit_m4468_MethodInfo,
	&Parser_ConsumeWhitespace_m4469_MethodInfo,
	&Parser_ResolveReferences_m4470_MethodInfo,
	&Parser_HandleExplicitNumericGroups_m4471_MethodInfo,
	&Parser_IsIgnoreCase_m4472_MethodInfo,
	&Parser_IsMultiline_m4473_MethodInfo,
	&Parser_IsExplicitCapture_m4474_MethodInfo,
	&Parser_IsSingleline_m4475_MethodInfo,
	&Parser_IsIgnorePatternWhitespace_m4476_MethodInfo,
	&Parser_IsECMAScript_m4477_MethodInfo,
	&Parser_NewParseException_m4478_MethodInfo,
	NULL
};
static const Il2CppMethodReference Parser_t1110_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool Parser_t1110_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Parser_t1110_0_0_0;
extern const Il2CppType Parser_t1110_1_0_0;
struct Parser_t1110;
const Il2CppTypeDefinitionMetadata Parser_t1110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Parser_t1110_VTable/* vtableMethods */
	, Parser_t1110_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 701/* fieldStart */

};
TypeInfo Parser_t1110_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Parser"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Parser_t1110_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Parser_t1110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Parser_t1110_0_0_0/* byval_arg */
	, &Parser_t1110_1_0_0/* this_arg */
	, &Parser_t1110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Parser_t1110)/* instance_size */
	, sizeof (Parser_t1110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearch.h"
// Metadata Definition System.Text.RegularExpressions.QuickSearch
extern TypeInfo QuickSearch_t1103_il2cpp_TypeInfo;
// System.Text.RegularExpressions.QuickSearch
#include "System_System_Text_RegularExpressions_QuickSearchMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo QuickSearch_t1103_QuickSearch__ctor_m4479_ParameterInfos[] = 
{
	{"str", 0, 134218486, 0, &String_t_0_0_0},
	{"ignore", 1, 134218487, 0, &Boolean_t72_0_0_0},
	{"reverse", 2, 134218488, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.ctor(System.String,System.Boolean,System.Boolean)
extern const MethodInfo QuickSearch__ctor_m4479_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuickSearch__ctor_m4479/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, QuickSearch_t1103_QuickSearch__ctor_m4479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::.cctor()
extern const MethodInfo QuickSearch__cctor_m4480_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QuickSearch__cctor_m4480/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::get_Length()
extern const MethodInfo QuickSearch_get_Length_m4481_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&QuickSearch_get_Length_m4481/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo QuickSearch_t1103_QuickSearch_Search_m4482_ParameterInfos[] = 
{
	{"text", 0, 134218489, 0, &String_t_0_0_0},
	{"start", 1, 134218490, 0, &Int32_t54_0_0_0},
	{"end", 2, 134218491, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::Search(System.String,System.Int32,System.Int32)
extern const MethodInfo QuickSearch_Search_m4482_MethodInfo = 
{
	"Search"/* name */
	, (methodPointerType)&QuickSearch_Search_m4482/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t_Int32_t54_Int32_t54/* invoker_method */
	, QuickSearch_t1103_QuickSearch_Search_m4482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.QuickSearch::SetupShiftTable()
extern const MethodInfo QuickSearch_SetupShiftTable_m4483_MethodInfo = 
{
	"SetupShiftTable"/* name */
	, (methodPointerType)&QuickSearch_SetupShiftTable_m4483/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo QuickSearch_t1103_QuickSearch_GetShiftDistance_m4484_ParameterInfos[] = 
{
	{"c", 0, 134218492, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.QuickSearch::GetShiftDistance(System.Char)
extern const MethodInfo QuickSearch_GetShiftDistance_m4484_MethodInfo = 
{
	"GetShiftDistance"/* name */
	, (methodPointerType)&QuickSearch_GetShiftDistance_m4484/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int16_t448/* invoker_method */
	, QuickSearch_t1103_QuickSearch_GetShiftDistance_m4484_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo QuickSearch_t1103_QuickSearch_GetChar_m4485_ParameterInfos[] = 
{
	{"c", 0, 134218493, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Char_t369_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Text.RegularExpressions.QuickSearch::GetChar(System.Char)
extern const MethodInfo QuickSearch_GetChar_m4485_MethodInfo = 
{
	"GetChar"/* name */
	, (methodPointerType)&QuickSearch_GetChar_m4485/* method */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* declaring_type */
	, &Char_t369_0_0_0/* return_type */
	, RuntimeInvoker_Char_t369_Int16_t448/* invoker_method */
	, QuickSearch_t1103_QuickSearch_GetChar_m4485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QuickSearch_t1103_MethodInfos[] =
{
	&QuickSearch__ctor_m4479_MethodInfo,
	&QuickSearch__cctor_m4480_MethodInfo,
	&QuickSearch_get_Length_m4481_MethodInfo,
	&QuickSearch_Search_m4482_MethodInfo,
	&QuickSearch_SetupShiftTable_m4483_MethodInfo,
	&QuickSearch_GetShiftDistance_m4484_MethodInfo,
	&QuickSearch_GetChar_m4485_MethodInfo,
	NULL
};
extern const MethodInfo QuickSearch_get_Length_m4481_MethodInfo;
static const PropertyInfo QuickSearch_t1103____Length_PropertyInfo = 
{
	&QuickSearch_t1103_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &QuickSearch_get_Length_m4481_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* QuickSearch_t1103_PropertyInfos[] =
{
	&QuickSearch_t1103____Length_PropertyInfo,
	NULL
};
static const Il2CppMethodReference QuickSearch_t1103_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool QuickSearch_t1103_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType QuickSearch_t1103_0_0_0;
extern const Il2CppType QuickSearch_t1103_1_0_0;
struct QuickSearch_t1103;
const Il2CppTypeDefinitionMetadata QuickSearch_t1103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QuickSearch_t1103_VTable/* vtableMethods */
	, QuickSearch_t1103_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 707/* fieldStart */

};
TypeInfo QuickSearch_t1103_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuickSearch"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, QuickSearch_t1103_MethodInfos/* methods */
	, QuickSearch_t1103_PropertyInfos/* properties */
	, NULL/* events */
	, &QuickSearch_t1103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuickSearch_t1103_0_0_0/* byval_arg */
	, &QuickSearch_t1103_1_0_0/* this_arg */
	, &QuickSearch_t1103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuickSearch_t1103)/* instance_size */
	, sizeof (QuickSearch_t1103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QuickSearch_t1103_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.ReplacementEvaluator
extern TypeInfo ReplacementEvaluator_t1111_il2cpp_TypeInfo;
// System.Text.RegularExpressions.ReplacementEvaluator
#include "System_System_Text_RegularExpressions_ReplacementEvaluatorMethodDeclarations.h"
extern const Il2CppType Regex_t783_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator__ctor_m4486_ParameterInfos[] = 
{
	{"regex", 0, 134218494, 0, &Regex_t783_0_0_0},
	{"replacement", 1, 134218495, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::.ctor(System.Text.RegularExpressions.Regex,System.String)
extern const MethodInfo ReplacementEvaluator__ctor_m4486_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReplacementEvaluator__ctor_m4486/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator__ctor_m4486_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1067_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_Evaluate_m4487_ParameterInfos[] = 
{
	{"match", 0, 134218496, 0, &Match_t1067_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.ReplacementEvaluator::Evaluate(System.Text.RegularExpressions.Match)
extern const MethodInfo ReplacementEvaluator_Evaluate_m4487_MethodInfo = 
{
	"Evaluate"/* name */
	, (methodPointerType)&ReplacementEvaluator_Evaluate_m4487/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_Evaluate_m4487_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1067_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
extern const Il2CppType StringBuilder_t338_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_EvaluateAppend_m4488_ParameterInfos[] = 
{
	{"match", 0, 134218497, 0, &Match_t1067_0_0_0},
	{"sb", 1, 134218498, 0, &StringBuilder_t338_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::EvaluateAppend(System.Text.RegularExpressions.Match,System.Text.StringBuilder)
extern const MethodInfo ReplacementEvaluator_EvaluateAppend_m4488_MethodInfo = 
{
	"EvaluateAppend"/* name */
	, (methodPointerType)&ReplacementEvaluator_EvaluateAppend_m4488/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_EvaluateAppend_m4488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.ReplacementEvaluator::get_NeedsGroupsOrCaptures()
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4489_MethodInfo = 
{
	"get_NeedsGroupsOrCaptures"/* name */
	, (methodPointerType)&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4489/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_Ensure_m4490_ParameterInfos[] = 
{
	{"size", 0, 134218499, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Ensure(System.Int32)
extern const MethodInfo ReplacementEvaluator_Ensure_m4490_MethodInfo = 
{
	"Ensure"/* name */
	, (methodPointerType)&ReplacementEvaluator_Ensure_m4490/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_Ensure_m4490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_AddFromReplacement_m4491_ParameterInfos[] = 
{
	{"start", 0, 134218500, 0, &Int32_t54_0_0_0},
	{"end", 1, 134218501, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddFromReplacement(System.Int32,System.Int32)
extern const MethodInfo ReplacementEvaluator_AddFromReplacement_m4491_MethodInfo = 
{
	"AddFromReplacement"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddFromReplacement_m4491/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_AddFromReplacement_m4491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_AddInt_m4492_ParameterInfos[] = 
{
	{"i", 0, 134218502, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::AddInt(System.Int32)
extern const MethodInfo ReplacementEvaluator_AddInt_m4492_MethodInfo = 
{
	"AddInt"/* name */
	, (methodPointerType)&ReplacementEvaluator_AddInt_m4492/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_AddInt_m4492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.ReplacementEvaluator::Compile()
extern const MethodInfo ReplacementEvaluator_Compile_m4493_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ReplacementEvaluator_Compile_m4493/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_0;
static const ParameterInfo ReplacementEvaluator_t1111_ReplacementEvaluator_CompileTerm_m4494_ParameterInfos[] = 
{
	{"ptr", 0, 134218503, 0, &Int32_t54_1_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.ReplacementEvaluator::CompileTerm(System.Int32&)
extern const MethodInfo ReplacementEvaluator_CompileTerm_m4494_MethodInfo = 
{
	"CompileTerm"/* name */
	, (methodPointerType)&ReplacementEvaluator_CompileTerm_m4494/* method */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int32U26_t449/* invoker_method */
	, ReplacementEvaluator_t1111_ReplacementEvaluator_CompileTerm_m4494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReplacementEvaluator_t1111_MethodInfos[] =
{
	&ReplacementEvaluator__ctor_m4486_MethodInfo,
	&ReplacementEvaluator_Evaluate_m4487_MethodInfo,
	&ReplacementEvaluator_EvaluateAppend_m4488_MethodInfo,
	&ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4489_MethodInfo,
	&ReplacementEvaluator_Ensure_m4490_MethodInfo,
	&ReplacementEvaluator_AddFromReplacement_m4491_MethodInfo,
	&ReplacementEvaluator_AddInt_m4492_MethodInfo,
	&ReplacementEvaluator_Compile_m4493_MethodInfo,
	&ReplacementEvaluator_CompileTerm_m4494_MethodInfo,
	NULL
};
extern const MethodInfo ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4489_MethodInfo;
static const PropertyInfo ReplacementEvaluator_t1111____NeedsGroupsOrCaptures_PropertyInfo = 
{
	&ReplacementEvaluator_t1111_il2cpp_TypeInfo/* parent */
	, "NeedsGroupsOrCaptures"/* name */
	, &ReplacementEvaluator_get_NeedsGroupsOrCaptures_m4489_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ReplacementEvaluator_t1111_PropertyInfos[] =
{
	&ReplacementEvaluator_t1111____NeedsGroupsOrCaptures_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ReplacementEvaluator_t1111_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool ReplacementEvaluator_t1111_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ReplacementEvaluator_t1111_0_0_0;
extern const Il2CppType ReplacementEvaluator_t1111_1_0_0;
struct ReplacementEvaluator_t1111;
const Il2CppTypeDefinitionMetadata ReplacementEvaluator_t1111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReplacementEvaluator_t1111_VTable/* vtableMethods */
	, ReplacementEvaluator_t1111_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 714/* fieldStart */

};
TypeInfo ReplacementEvaluator_t1111_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReplacementEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, ReplacementEvaluator_t1111_MethodInfos/* methods */
	, ReplacementEvaluator_t1111_PropertyInfos/* properties */
	, NULL/* events */
	, &ReplacementEvaluator_t1111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReplacementEvaluator_t1111_0_0_0/* byval_arg */
	, &ReplacementEvaluator_t1111_1_0_0/* this_arg */
	, &ReplacementEvaluator_t1111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReplacementEvaluator_t1111)/* instance_size */
	, sizeof (ReplacementEvaluator_t1111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColle.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionCollection
extern TypeInfo ExpressionCollection_t1112_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
#include "System_System_Text_RegularExpressions_Syntax_ExpressionColleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
extern const MethodInfo ExpressionCollection__ctor_m4495_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionCollection__ctor_m4495/* method */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo ExpressionCollection_t1112_ExpressionCollection_Add_m4496_ParameterInfos[] = 
{
	{"e", 0, 134218504, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_Add_m4496_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&ExpressionCollection_Add_m4496/* method */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ExpressionCollection_t1112_ExpressionCollection_Add_m4496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo ExpressionCollection_t1112_ExpressionCollection_get_Item_m4497_ParameterInfos[] = 
{
	{"i", 0, 134218505, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
extern const MethodInfo ExpressionCollection_get_Item_m4497_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&ExpressionCollection_get_Item_m4497/* method */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, ExpressionCollection_t1112_ExpressionCollection_get_Item_m4497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo ExpressionCollection_t1112_ExpressionCollection_set_Item_m4498_ParameterInfos[] = 
{
	{"i", 0, 134218506, 0, &Int32_t54_0_0_0},
	{"value", 1, 134218507, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionCollection_set_Item_m4498_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&ExpressionCollection_set_Item_m4498/* method */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, ExpressionCollection_t1112_ExpressionCollection_set_Item_m4498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ExpressionCollection_t1112_ExpressionCollection_OnValidate_m4499_ParameterInfos[] = 
{
	{"o", 0, 134218508, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
extern const MethodInfo ExpressionCollection_OnValidate_m4499_MethodInfo = 
{
	"OnValidate"/* name */
	, (methodPointerType)&ExpressionCollection_OnValidate_m4499/* method */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ExpressionCollection_t1112_ExpressionCollection_OnValidate_m4499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionCollection_t1112_MethodInfos[] =
{
	&ExpressionCollection__ctor_m4495_MethodInfo,
	&ExpressionCollection_Add_m4496_MethodInfo,
	&ExpressionCollection_get_Item_m4497_MethodInfo,
	&ExpressionCollection_set_Item_m4498_MethodInfo,
	&ExpressionCollection_OnValidate_m4499_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionCollection_get_Item_m4497_MethodInfo;
extern const MethodInfo ExpressionCollection_set_Item_m4498_MethodInfo;
static const PropertyInfo ExpressionCollection_t1112____Item_PropertyInfo = 
{
	&ExpressionCollection_t1112_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ExpressionCollection_get_Item_m4497_MethodInfo/* get */
	, &ExpressionCollection_set_Item_m4498_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionCollection_t1112_PropertyInfos[] =
{
	&ExpressionCollection_t1112____Item_PropertyInfo,
	NULL
};
extern const MethodInfo CollectionBase_GetEnumerator_m4953_MethodInfo;
extern const MethodInfo CollectionBase_get_Count_m4954_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_IsSynchronized_m4955_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_get_SyncRoot_m4956_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_ICollection_CopyTo_m4957_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsFixedSize_m4958_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_IsReadOnly_m4959_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_get_Item_m4960_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_set_Item_m4961_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Add_m4962_MethodInfo;
extern const MethodInfo CollectionBase_Clear_m4963_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Contains_m4964_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_IndexOf_m4965_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Insert_m4966_MethodInfo;
extern const MethodInfo CollectionBase_System_Collections_IList_Remove_m4967_MethodInfo;
extern const MethodInfo CollectionBase_RemoveAt_m4968_MethodInfo;
extern const MethodInfo CollectionBase_OnClear_m4969_MethodInfo;
extern const MethodInfo CollectionBase_OnClearComplete_m4970_MethodInfo;
extern const MethodInfo CollectionBase_OnInsert_m4971_MethodInfo;
extern const MethodInfo CollectionBase_OnInsertComplete_m4972_MethodInfo;
extern const MethodInfo CollectionBase_OnRemove_m4973_MethodInfo;
extern const MethodInfo CollectionBase_OnRemoveComplete_m4974_MethodInfo;
extern const MethodInfo CollectionBase_OnSet_m4975_MethodInfo;
extern const MethodInfo CollectionBase_OnSetComplete_m4976_MethodInfo;
extern const MethodInfo ExpressionCollection_OnValidate_m4499_MethodInfo;
static const Il2CppMethodReference ExpressionCollection_t1112_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CollectionBase_GetEnumerator_m4953_MethodInfo,
	&CollectionBase_get_Count_m4954_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_IsSynchronized_m4955_MethodInfo,
	&CollectionBase_System_Collections_ICollection_get_SyncRoot_m4956_MethodInfo,
	&CollectionBase_System_Collections_ICollection_CopyTo_m4957_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsFixedSize_m4958_MethodInfo,
	&CollectionBase_System_Collections_IList_get_IsReadOnly_m4959_MethodInfo,
	&CollectionBase_System_Collections_IList_get_Item_m4960_MethodInfo,
	&CollectionBase_System_Collections_IList_set_Item_m4961_MethodInfo,
	&CollectionBase_System_Collections_IList_Add_m4962_MethodInfo,
	&CollectionBase_Clear_m4963_MethodInfo,
	&CollectionBase_System_Collections_IList_Contains_m4964_MethodInfo,
	&CollectionBase_System_Collections_IList_IndexOf_m4965_MethodInfo,
	&CollectionBase_System_Collections_IList_Insert_m4966_MethodInfo,
	&CollectionBase_System_Collections_IList_Remove_m4967_MethodInfo,
	&CollectionBase_RemoveAt_m4968_MethodInfo,
	&CollectionBase_OnClear_m4969_MethodInfo,
	&CollectionBase_OnClearComplete_m4970_MethodInfo,
	&CollectionBase_OnInsert_m4971_MethodInfo,
	&CollectionBase_OnInsertComplete_m4972_MethodInfo,
	&CollectionBase_OnRemove_m4973_MethodInfo,
	&CollectionBase_OnRemoveComplete_m4974_MethodInfo,
	&CollectionBase_OnSet_m4975_MethodInfo,
	&CollectionBase_OnSetComplete_m4976_MethodInfo,
	&ExpressionCollection_OnValidate_m4499_MethodInfo,
};
static bool ExpressionCollection_t1112_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExpressionCollection_t1112_InterfacesOffsets[] = 
{
	{ &IEnumerable_t464_0_0_0, 4},
	{ &ICollection_t860_0_0_0, 5},
	{ &IList_t861_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionCollection_t1112_0_0_0;
extern const Il2CppType ExpressionCollection_t1112_1_0_0;
extern const Il2CppType CollectionBase_t1041_0_0_0;
struct ExpressionCollection_t1112;
const Il2CppTypeDefinitionMetadata ExpressionCollection_t1112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExpressionCollection_t1112_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1041_0_0_0/* parent */
	, ExpressionCollection_t1112_VTable/* vtableMethods */
	, ExpressionCollection_t1112_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExpressionCollection_t1112_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionCollection"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionCollection_t1112_MethodInfos/* methods */
	, ExpressionCollection_t1112_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionCollection_t1112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 64/* custom_attributes_cache */
	, &ExpressionCollection_t1112_0_0_0/* byval_arg */
	, &ExpressionCollection_t1112_1_0_0/* this_arg */
	, &ExpressionCollection_t1112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionCollection_t1112)/* instance_size */
	, sizeof (ExpressionCollection_t1112)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Expression
extern TypeInfo Expression_t1113_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_ExpressionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::.ctor()
extern const MethodInfo Expression__ctor_m4500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Expression__ctor_m4500/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Expression_t1113_Expression_Compile_m4927_ParameterInfos[] = 
{
	{"cmp", 0, 134218509, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218510, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Expression_Compile_m4927_MethodInfo = 
{
	"Compile"/* name */
	, NULL/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Expression_t1113_Expression_Compile_m4927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Expression_t1113_Expression_GetWidth_m4928_ParameterInfos[] = 
{
	{"min", 0, 134218511, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218512, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Expression::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Expression_GetWidth_m4928_MethodInfo = 
{
	"GetWidth"/* name */
	, NULL/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Expression_t1113_Expression_GetWidth_m4928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Expression::GetFixedWidth()
extern const MethodInfo Expression_GetFixedWidth_m4501_MethodInfo = 
{
	"GetFixedWidth"/* name */
	, (methodPointerType)&Expression_GetFixedWidth_m4501/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Expression_t1113_Expression_GetAnchorInfo_m4502_ParameterInfos[] = 
{
	{"reverse", 0, 134218513, 0, &Boolean_t72_0_0_0},
};
extern const Il2CppType AnchorInfo_t1131_0_0_0;
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Expression::GetAnchorInfo(System.Boolean)
extern const MethodInfo Expression_GetAnchorInfo_m4502_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Expression_GetAnchorInfo_m4502/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, Expression_t1113_Expression_GetAnchorInfo_m4502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Expression::IsComplex()
extern const MethodInfo Expression_IsComplex_m4929_MethodInfo = 
{
	"IsComplex"/* name */
	, NULL/* method */
	, &Expression_t1113_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Expression_t1113_MethodInfos[] =
{
	&Expression__ctor_m4500_MethodInfo,
	&Expression_Compile_m4927_MethodInfo,
	&Expression_GetWidth_m4928_MethodInfo,
	&Expression_GetFixedWidth_m4501_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&Expression_IsComplex_m4929_MethodInfo,
	NULL
};
extern const MethodInfo Expression_GetAnchorInfo_m4502_MethodInfo;
static const Il2CppMethodReference Expression_t1113_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	NULL,
};
static bool Expression_t1113_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Expression_t1113_1_0_0;
struct Expression_t1113;
const Il2CppTypeDefinitionMetadata Expression_t1113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Expression_t1113_VTable/* vtableMethods */
	, Expression_t1113_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Expression_t1113_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Expression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Expression_t1113_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Expression_t1113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Expression_t1113_0_0_0/* byval_arg */
	, &Expression_t1113_1_0_0/* this_arg */
	, &Expression_t1113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Expression_t1113)/* instance_size */
	, sizeof (Expression_t1113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpres.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CompositeExpression
extern TypeInfo CompositeExpression_t1114_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CompositeExpression
#include "System_System_Text_RegularExpressions_Syntax_CompositeExpresMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::.ctor()
extern const MethodInfo CompositeExpression__ctor_m4503_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CompositeExpression__ctor_m4503/* method */
	, &CompositeExpression_t1114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::get_Expressions()
extern const MethodInfo CompositeExpression_get_Expressions_m4504_MethodInfo = 
{
	"get_Expressions"/* name */
	, (methodPointerType)&CompositeExpression_get_Expressions_m4504/* method */
	, &CompositeExpression_t1114_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo CompositeExpression_t1114_CompositeExpression_GetWidth_m4505_ParameterInfos[] = 
{
	{"min", 0, 134218514, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218515, 0, &Int32_t54_1_0_2},
	{"count", 2, 134218516, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CompositeExpression::GetWidth(System.Int32&,System.Int32&,System.Int32)
extern const MethodInfo CompositeExpression_GetWidth_m4505_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CompositeExpression_GetWidth_m4505/* method */
	, &CompositeExpression_t1114_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449_Int32_t54/* invoker_method */
	, CompositeExpression_t1114_CompositeExpression_GetWidth_m4505_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CompositeExpression::IsComplex()
extern const MethodInfo CompositeExpression_IsComplex_m4506_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CompositeExpression_IsComplex_m4506/* method */
	, &CompositeExpression_t1114_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CompositeExpression_t1114_MethodInfos[] =
{
	&CompositeExpression__ctor_m4503_MethodInfo,
	&CompositeExpression_get_Expressions_m4504_MethodInfo,
	&CompositeExpression_GetWidth_m4505_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
	NULL
};
extern const MethodInfo CompositeExpression_get_Expressions_m4504_MethodInfo;
static const PropertyInfo CompositeExpression_t1114____Expressions_PropertyInfo = 
{
	&CompositeExpression_t1114_il2cpp_TypeInfo/* parent */
	, "Expressions"/* name */
	, &CompositeExpression_get_Expressions_m4504_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CompositeExpression_t1114_PropertyInfos[] =
{
	&CompositeExpression_t1114____Expressions_PropertyInfo,
	NULL
};
extern const MethodInfo CompositeExpression_IsComplex_m4506_MethodInfo;
static const Il2CppMethodReference CompositeExpression_t1114_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	NULL,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool CompositeExpression_t1114_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CompositeExpression_t1114_0_0_0;
extern const Il2CppType CompositeExpression_t1114_1_0_0;
struct CompositeExpression_t1114;
const Il2CppTypeDefinitionMetadata CompositeExpression_t1114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1113_0_0_0/* parent */
	, CompositeExpression_t1114_VTable/* vtableMethods */
	, CompositeExpression_t1114_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 718/* fieldStart */

};
TypeInfo CompositeExpression_t1114_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompositeExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CompositeExpression_t1114_MethodInfos/* methods */
	, CompositeExpression_t1114_PropertyInfos/* properties */
	, NULL/* events */
	, &CompositeExpression_t1114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompositeExpression_t1114_0_0_0/* byval_arg */
	, &CompositeExpression_t1114_1_0_0/* this_arg */
	, &CompositeExpression_t1114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompositeExpression_t1114)/* instance_size */
	, sizeof (CompositeExpression_t1114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_Group.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Group
extern TypeInfo Group_t1115_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Group
#include "System_System_Text_RegularExpressions_Syntax_GroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::.ctor()
extern const MethodInfo Group__ctor_m4507_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Group__ctor_m4507/* method */
	, &Group_t1115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo Group_t1115_Group_AppendExpression_m4508_ParameterInfos[] = 
{
	{"e", 0, 134218517, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::AppendExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Group_AppendExpression_m4508_MethodInfo = 
{
	"AppendExpression"/* name */
	, (methodPointerType)&Group_AppendExpression_m4508/* method */
	, &Group_t1115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Group_t1115_Group_AppendExpression_m4508_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Group_t1115_Group_Compile_m4509_ParameterInfos[] = 
{
	{"cmp", 0, 134218518, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218519, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Group_Compile_m4509_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Group_Compile_m4509/* method */
	, &Group_t1115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Group_t1115_Group_Compile_m4509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Group_t1115_Group_GetWidth_m4510_ParameterInfos[] = 
{
	{"min", 0, 134218520, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218521, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Group::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Group_GetWidth_m4510_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Group_GetWidth_m4510/* method */
	, &Group_t1115_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Group_t1115_Group_GetWidth_m4510_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Group_t1115_Group_GetAnchorInfo_m4511_ParameterInfos[] = 
{
	{"reverse", 0, 134218522, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Group::GetAnchorInfo(System.Boolean)
extern const MethodInfo Group_GetAnchorInfo_m4511_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Group_GetAnchorInfo_m4511/* method */
	, &Group_t1115_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, Group_t1115_Group_GetAnchorInfo_m4511_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Group_t1115_MethodInfos[] =
{
	&Group__ctor_m4507_MethodInfo,
	&Group_AppendExpression_m4508_MethodInfo,
	&Group_Compile_m4509_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	NULL
};
extern const MethodInfo Group_Compile_m4509_MethodInfo;
extern const MethodInfo Group_GetWidth_m4510_MethodInfo;
extern const MethodInfo Group_GetAnchorInfo_m4511_MethodInfo;
static const Il2CppMethodReference Group_t1115_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Group_Compile_m4509_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool Group_t1115_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t1115_1_0_0;
struct Group_t1115;
const Il2CppTypeDefinitionMetadata Group_t1115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1114_0_0_0/* parent */
	, Group_t1115_VTable/* vtableMethods */
	, Group_t1115_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Group_t1115_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Group_t1115_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Group_t1115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t1115_0_0_0/* byval_arg */
	, &Group_t1115_1_0_0/* this_arg */
	, &Group_t1115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t1115)/* instance_size */
	, sizeof (Group_t1115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressi.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.RegularExpression
extern TypeInfo RegularExpression_t1116_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.RegularExpression
#include "System_System_Text_RegularExpressions_Syntax_RegularExpressiMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::.ctor()
extern const MethodInfo RegularExpression__ctor_m4512_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RegularExpression__ctor_m4512/* method */
	, &RegularExpression_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo RegularExpression_t1116_RegularExpression_set_GroupCount_m4513_ParameterInfos[] = 
{
	{"value", 0, 134218523, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::set_GroupCount(System.Int32)
extern const MethodInfo RegularExpression_set_GroupCount_m4513_MethodInfo = 
{
	"set_GroupCount"/* name */
	, (methodPointerType)&RegularExpression_set_GroupCount_m4513/* method */
	, &RegularExpression_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, RegularExpression_t1116_RegularExpression_set_GroupCount_m4513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo RegularExpression_t1116_RegularExpression_Compile_m4514_ParameterInfos[] = 
{
	{"cmp", 0, 134218524, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218525, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.RegularExpression::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo RegularExpression_Compile_m4514_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&RegularExpression_Compile_m4514/* method */
	, &RegularExpression_t1116_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, RegularExpression_t1116_RegularExpression_Compile_m4514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RegularExpression_t1116_MethodInfos[] =
{
	&RegularExpression__ctor_m4512_MethodInfo,
	&RegularExpression_set_GroupCount_m4513_MethodInfo,
	&RegularExpression_Compile_m4514_MethodInfo,
	NULL
};
extern const MethodInfo RegularExpression_set_GroupCount_m4513_MethodInfo;
static const PropertyInfo RegularExpression_t1116____GroupCount_PropertyInfo = 
{
	&RegularExpression_t1116_il2cpp_TypeInfo/* parent */
	, "GroupCount"/* name */
	, NULL/* get */
	, &RegularExpression_set_GroupCount_m4513_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RegularExpression_t1116_PropertyInfos[] =
{
	&RegularExpression_t1116____GroupCount_PropertyInfo,
	NULL
};
extern const MethodInfo RegularExpression_Compile_m4514_MethodInfo;
static const Il2CppMethodReference RegularExpression_t1116_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&RegularExpression_Compile_m4514_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool RegularExpression_t1116_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegularExpression_t1116_1_0_0;
struct RegularExpression_t1116;
const Il2CppTypeDefinitionMetadata RegularExpression_t1116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1115_0_0_0/* parent */
	, RegularExpression_t1116_VTable/* vtableMethods */
	, RegularExpression_t1116_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 719/* fieldStart */

};
TypeInfo RegularExpression_t1116_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegularExpression"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, RegularExpression_t1116_MethodInfos/* methods */
	, RegularExpression_t1116_PropertyInfos/* properties */
	, NULL/* events */
	, &RegularExpression_t1116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RegularExpression_t1116_0_0_0/* byval_arg */
	, &RegularExpression_t1116_1_0_0/* this_arg */
	, &RegularExpression_t1116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegularExpression_t1116)/* instance_size */
	, sizeof (RegularExpression_t1116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CapturingGroup
extern TypeInfo CapturingGroup_t1117_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CapturingGroup
#include "System_System_Text_RegularExpressions_Syntax_CapturingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::.ctor()
extern const MethodInfo CapturingGroup__ctor_m4515_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CapturingGroup__ctor_m4515/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::get_Index()
extern const MethodInfo CapturingGroup_get_Index_m4516_MethodInfo = 
{
	"get_Index"/* name */
	, (methodPointerType)&CapturingGroup_get_Index_m4516/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo CapturingGroup_t1117_CapturingGroup_set_Index_m4517_ParameterInfos[] = 
{
	{"value", 0, 134218526, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Index(System.Int32)
extern const MethodInfo CapturingGroup_set_Index_m4517_MethodInfo = 
{
	"set_Index"/* name */
	, (methodPointerType)&CapturingGroup_set_Index_m4517/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, CapturingGroup_t1117_CapturingGroup_set_Index_m4517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::get_Name()
extern const MethodInfo CapturingGroup_get_Name_m4518_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&CapturingGroup_get_Name_m4518/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CapturingGroup_t1117_CapturingGroup_set_Name_m4519_ParameterInfos[] = 
{
	{"value", 0, 134218527, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Name(System.String)
extern const MethodInfo CapturingGroup_set_Name_m4519_MethodInfo = 
{
	"set_Name"/* name */
	, (methodPointerType)&CapturingGroup_set_Name_m4519/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CapturingGroup_t1117_CapturingGroup_set_Name_m4519_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::get_IsNamed()
extern const MethodInfo CapturingGroup_get_IsNamed_m4520_MethodInfo = 
{
	"get_IsNamed"/* name */
	, (methodPointerType)&CapturingGroup_get_IsNamed_m4520/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CapturingGroup_t1117_CapturingGroup_Compile_m4521_ParameterInfos[] = 
{
	{"cmp", 0, 134218528, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218529, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CapturingGroup_Compile_m4521_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CapturingGroup_Compile_m4521/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, CapturingGroup_t1117_CapturingGroup_Compile_m4521_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::IsComplex()
extern const MethodInfo CapturingGroup_IsComplex_m4522_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CapturingGroup_IsComplex_m4522/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CapturingGroup_t1117_CapturingGroup_CompareTo_m4523_ParameterInfos[] = 
{
	{"other", 0, 134218530, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::CompareTo(System.Object)
extern const MethodInfo CapturingGroup_CompareTo_m4523_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&CapturingGroup_CompareTo_m4523/* method */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, CapturingGroup_t1117_CapturingGroup_CompareTo_m4523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CapturingGroup_t1117_MethodInfos[] =
{
	&CapturingGroup__ctor_m4515_MethodInfo,
	&CapturingGroup_get_Index_m4516_MethodInfo,
	&CapturingGroup_set_Index_m4517_MethodInfo,
	&CapturingGroup_get_Name_m4518_MethodInfo,
	&CapturingGroup_set_Name_m4519_MethodInfo,
	&CapturingGroup_get_IsNamed_m4520_MethodInfo,
	&CapturingGroup_Compile_m4521_MethodInfo,
	&CapturingGroup_IsComplex_m4522_MethodInfo,
	&CapturingGroup_CompareTo_m4523_MethodInfo,
	NULL
};
extern const MethodInfo CapturingGroup_get_Index_m4516_MethodInfo;
extern const MethodInfo CapturingGroup_set_Index_m4517_MethodInfo;
static const PropertyInfo CapturingGroup_t1117____Index_PropertyInfo = 
{
	&CapturingGroup_t1117_il2cpp_TypeInfo/* parent */
	, "Index"/* name */
	, &CapturingGroup_get_Index_m4516_MethodInfo/* get */
	, &CapturingGroup_set_Index_m4517_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_Name_m4518_MethodInfo;
extern const MethodInfo CapturingGroup_set_Name_m4519_MethodInfo;
static const PropertyInfo CapturingGroup_t1117____Name_PropertyInfo = 
{
	&CapturingGroup_t1117_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &CapturingGroup_get_Name_m4518_MethodInfo/* get */
	, &CapturingGroup_set_Name_m4519_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CapturingGroup_get_IsNamed_m4520_MethodInfo;
static const PropertyInfo CapturingGroup_t1117____IsNamed_PropertyInfo = 
{
	&CapturingGroup_t1117_il2cpp_TypeInfo/* parent */
	, "IsNamed"/* name */
	, &CapturingGroup_get_IsNamed_m4520_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CapturingGroup_t1117_PropertyInfos[] =
{
	&CapturingGroup_t1117____Index_PropertyInfo,
	&CapturingGroup_t1117____Name_PropertyInfo,
	&CapturingGroup_t1117____IsNamed_PropertyInfo,
	NULL
};
extern const MethodInfo CapturingGroup_Compile_m4521_MethodInfo;
extern const MethodInfo CapturingGroup_IsComplex_m4522_MethodInfo;
extern const MethodInfo CapturingGroup_CompareTo_m4523_MethodInfo;
static const Il2CppMethodReference CapturingGroup_t1117_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CapturingGroup_Compile_m4521_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	&CapturingGroup_IsComplex_m4522_MethodInfo,
	&CapturingGroup_CompareTo_m4523_MethodInfo,
};
static bool CapturingGroup_t1117_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* CapturingGroup_t1117_InterfacesTypeInfos[] = 
{
	&IComparable_t76_0_0_0,
};
static Il2CppInterfaceOffsetPair CapturingGroup_t1117_InterfacesOffsets[] = 
{
	{ &IComparable_t76_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CapturingGroup_t1117_0_0_0;
extern const Il2CppType CapturingGroup_t1117_1_0_0;
struct CapturingGroup_t1117;
const Il2CppTypeDefinitionMetadata CapturingGroup_t1117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CapturingGroup_t1117_InterfacesTypeInfos/* implementedInterfaces */
	, CapturingGroup_t1117_InterfacesOffsets/* interfaceOffsets */
	, &Group_t1115_0_0_0/* parent */
	, CapturingGroup_t1117_VTable/* vtableMethods */
	, CapturingGroup_t1117_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 720/* fieldStart */

};
TypeInfo CapturingGroup_t1117_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapturingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CapturingGroup_t1117_MethodInfos/* methods */
	, CapturingGroup_t1117_PropertyInfos/* properties */
	, NULL/* events */
	, &CapturingGroup_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapturingGroup_t1117_0_0_0/* byval_arg */
	, &CapturingGroup_t1117_1_0_0/* this_arg */
	, &CapturingGroup_t1117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapturingGroup_t1117)/* instance_size */
	, sizeof (CapturingGroup_t1117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroup.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BalancingGroup
extern TypeInfo BalancingGroup_t1118_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BalancingGroup
#include "System_System_Text_RegularExpressions_Syntax_BalancingGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::.ctor()
extern const MethodInfo BalancingGroup__ctor_m4524_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BalancingGroup__ctor_m4524/* method */
	, &BalancingGroup_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1117_0_0_0;
static const ParameterInfo BalancingGroup_t1118_BalancingGroup_set_Balance_m4525_ParameterInfos[] = 
{
	{"value", 0, 134218531, 0, &CapturingGroup_t1117_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::set_Balance(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo BalancingGroup_set_Balance_m4525_MethodInfo = 
{
	"set_Balance"/* name */
	, (methodPointerType)&BalancingGroup_set_Balance_m4525/* method */
	, &BalancingGroup_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, BalancingGroup_t1118_BalancingGroup_set_Balance_m4525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo BalancingGroup_t1118_BalancingGroup_Compile_m4526_ParameterInfos[] = 
{
	{"cmp", 0, 134218532, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218533, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BalancingGroup_Compile_m4526_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BalancingGroup_Compile_m4526/* method */
	, &BalancingGroup_t1118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, BalancingGroup_t1118_BalancingGroup_Compile_m4526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BalancingGroup_t1118_MethodInfos[] =
{
	&BalancingGroup__ctor_m4524_MethodInfo,
	&BalancingGroup_set_Balance_m4525_MethodInfo,
	&BalancingGroup_Compile_m4526_MethodInfo,
	NULL
};
extern const MethodInfo BalancingGroup_set_Balance_m4525_MethodInfo;
static const PropertyInfo BalancingGroup_t1118____Balance_PropertyInfo = 
{
	&BalancingGroup_t1118_il2cpp_TypeInfo/* parent */
	, "Balance"/* name */
	, NULL/* get */
	, &BalancingGroup_set_Balance_m4525_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BalancingGroup_t1118_PropertyInfos[] =
{
	&BalancingGroup_t1118____Balance_PropertyInfo,
	NULL
};
extern const MethodInfo BalancingGroup_Compile_m4526_MethodInfo;
static const Il2CppMethodReference BalancingGroup_t1118_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&BalancingGroup_Compile_m4526_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	&CapturingGroup_IsComplex_m4522_MethodInfo,
	&CapturingGroup_CompareTo_m4523_MethodInfo,
};
static bool BalancingGroup_t1118_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair BalancingGroup_t1118_InterfacesOffsets[] = 
{
	{ &IComparable_t76_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BalancingGroup_t1118_0_0_0;
extern const Il2CppType BalancingGroup_t1118_1_0_0;
struct BalancingGroup_t1118;
const Il2CppTypeDefinitionMetadata BalancingGroup_t1118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BalancingGroup_t1118_InterfacesOffsets/* interfaceOffsets */
	, &CapturingGroup_t1117_0_0_0/* parent */
	, BalancingGroup_t1118_VTable/* vtableMethods */
	, BalancingGroup_t1118_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 722/* fieldStart */

};
TypeInfo BalancingGroup_t1118_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BalancingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BalancingGroup_t1118_MethodInfos/* methods */
	, BalancingGroup_t1118_PropertyInfos/* properties */
	, NULL/* events */
	, &BalancingGroup_t1118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BalancingGroup_t1118_0_0_0/* byval_arg */
	, &BalancingGroup_t1118_1_0_0/* this_arg */
	, &BalancingGroup_t1118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BalancingGroup_t1118)/* instance_size */
	, sizeof (BalancingGroup_t1118)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktracking.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
extern TypeInfo NonBacktrackingGroup_t1119_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
#include "System_System_Text_RegularExpressions_Syntax_NonBacktrackingMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::.ctor()
extern const MethodInfo NonBacktrackingGroup__ctor_m4527_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonBacktrackingGroup__ctor_m4527/* method */
	, &NonBacktrackingGroup_t1119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo NonBacktrackingGroup_t1119_NonBacktrackingGroup_Compile_m4528_ParameterInfos[] = 
{
	{"cmp", 0, 134218534, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218535, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo NonBacktrackingGroup_Compile_m4528_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&NonBacktrackingGroup_Compile_m4528/* method */
	, &NonBacktrackingGroup_t1119_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, NonBacktrackingGroup_t1119_NonBacktrackingGroup_Compile_m4528_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.NonBacktrackingGroup::IsComplex()
extern const MethodInfo NonBacktrackingGroup_IsComplex_m4529_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&NonBacktrackingGroup_IsComplex_m4529/* method */
	, &NonBacktrackingGroup_t1119_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonBacktrackingGroup_t1119_MethodInfos[] =
{
	&NonBacktrackingGroup__ctor_m4527_MethodInfo,
	&NonBacktrackingGroup_Compile_m4528_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m4529_MethodInfo,
	NULL
};
extern const MethodInfo NonBacktrackingGroup_Compile_m4528_MethodInfo;
extern const MethodInfo NonBacktrackingGroup_IsComplex_m4529_MethodInfo;
static const Il2CppMethodReference NonBacktrackingGroup_t1119_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&NonBacktrackingGroup_Compile_m4528_MethodInfo,
	&Group_GetWidth_m4510_MethodInfo,
	&Group_GetAnchorInfo_m4511_MethodInfo,
	&NonBacktrackingGroup_IsComplex_m4529_MethodInfo,
};
static bool NonBacktrackingGroup_t1119_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NonBacktrackingGroup_t1119_0_0_0;
extern const Il2CppType NonBacktrackingGroup_t1119_1_0_0;
struct NonBacktrackingGroup_t1119;
const Il2CppTypeDefinitionMetadata NonBacktrackingGroup_t1119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1115_0_0_0/* parent */
	, NonBacktrackingGroup_t1119_VTable/* vtableMethods */
	, NonBacktrackingGroup_t1119_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonBacktrackingGroup_t1119_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonBacktrackingGroup"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, NonBacktrackingGroup_t1119_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonBacktrackingGroup_t1119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NonBacktrackingGroup_t1119_0_0_0/* byval_arg */
	, &NonBacktrackingGroup_t1119_1_0_0/* this_arg */
	, &NonBacktrackingGroup_t1119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonBacktrackingGroup_t1119)/* instance_size */
	, sizeof (NonBacktrackingGroup_t1119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_Repetition.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Repetition
extern TypeInfo Repetition_t1120_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Repetition
#include "System_System_Text_RegularExpressions_Syntax_RepetitionMethodDeclarations.h"
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Repetition_t1120_Repetition__ctor_m4530_ParameterInfos[] = 
{
	{"min", 0, 134218536, 0, &Int32_t54_0_0_0},
	{"max", 1, 134218537, 0, &Int32_t54_0_0_0},
	{"lazy", 2, 134218538, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::.ctor(System.Int32,System.Int32,System.Boolean)
extern const MethodInfo Repetition__ctor_m4530_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Repetition__ctor_m4530/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Int32_t54_SByte_t73/* invoker_method */
	, Repetition_t1120_Repetition__ctor_m4530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Repetition::get_Expression()
extern const MethodInfo Repetition_get_Expression_m4531_MethodInfo = 
{
	"get_Expression"/* name */
	, (methodPointerType)&Repetition_get_Expression_m4531/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo Repetition_t1120_Repetition_set_Expression_m4532_ParameterInfos[] = 
{
	{"value", 0, 134218539, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::set_Expression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Repetition_set_Expression_m4532_MethodInfo = 
{
	"set_Expression"/* name */
	, (methodPointerType)&Repetition_set_Expression_m4532/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Repetition_t1120_Repetition_set_Expression_m4532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::get_Minimum()
extern const MethodInfo Repetition_get_Minimum_m4533_MethodInfo = 
{
	"get_Minimum"/* name */
	, (methodPointerType)&Repetition_get_Minimum_m4533/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Repetition_t1120_Repetition_Compile_m4534_ParameterInfos[] = 
{
	{"cmp", 0, 134218540, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218541, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Repetition_Compile_m4534_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Repetition_Compile_m4534/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Repetition_t1120_Repetition_Compile_m4534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Repetition_t1120_Repetition_GetWidth_m4535_ParameterInfos[] = 
{
	{"min", 0, 134218542, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218543, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Repetition::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Repetition_GetWidth_m4535_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Repetition_GetWidth_m4535/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Repetition_t1120_Repetition_GetWidth_m4535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Repetition_t1120_Repetition_GetAnchorInfo_m4536_ParameterInfos[] = 
{
	{"reverse", 0, 134218544, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Repetition::GetAnchorInfo(System.Boolean)
extern const MethodInfo Repetition_GetAnchorInfo_m4536_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Repetition_GetAnchorInfo_m4536/* method */
	, &Repetition_t1120_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, Repetition_t1120_Repetition_GetAnchorInfo_m4536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Repetition_t1120_MethodInfos[] =
{
	&Repetition__ctor_m4530_MethodInfo,
	&Repetition_get_Expression_m4531_MethodInfo,
	&Repetition_set_Expression_m4532_MethodInfo,
	&Repetition_get_Minimum_m4533_MethodInfo,
	&Repetition_Compile_m4534_MethodInfo,
	&Repetition_GetWidth_m4535_MethodInfo,
	&Repetition_GetAnchorInfo_m4536_MethodInfo,
	NULL
};
extern const MethodInfo Repetition_get_Expression_m4531_MethodInfo;
extern const MethodInfo Repetition_set_Expression_m4532_MethodInfo;
static const PropertyInfo Repetition_t1120____Expression_PropertyInfo = 
{
	&Repetition_t1120_il2cpp_TypeInfo/* parent */
	, "Expression"/* name */
	, &Repetition_get_Expression_m4531_MethodInfo/* get */
	, &Repetition_set_Expression_m4532_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Repetition_get_Minimum_m4533_MethodInfo;
static const PropertyInfo Repetition_t1120____Minimum_PropertyInfo = 
{
	&Repetition_t1120_il2cpp_TypeInfo/* parent */
	, "Minimum"/* name */
	, &Repetition_get_Minimum_m4533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Repetition_t1120_PropertyInfos[] =
{
	&Repetition_t1120____Expression_PropertyInfo,
	&Repetition_t1120____Minimum_PropertyInfo,
	NULL
};
extern const MethodInfo Repetition_Compile_m4534_MethodInfo;
extern const MethodInfo Repetition_GetWidth_m4535_MethodInfo;
extern const MethodInfo Repetition_GetAnchorInfo_m4536_MethodInfo;
static const Il2CppMethodReference Repetition_t1120_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Repetition_Compile_m4534_MethodInfo,
	&Repetition_GetWidth_m4535_MethodInfo,
	&Repetition_GetAnchorInfo_m4536_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool Repetition_t1120_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Repetition_t1120_0_0_0;
extern const Il2CppType Repetition_t1120_1_0_0;
struct Repetition_t1120;
const Il2CppTypeDefinitionMetadata Repetition_t1120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1114_0_0_0/* parent */
	, Repetition_t1120_VTable/* vtableMethods */
	, Repetition_t1120_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 723/* fieldStart */

};
TypeInfo Repetition_t1120_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Repetition"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Repetition_t1120_MethodInfos/* methods */
	, Repetition_t1120_PropertyInfos/* properties */
	, NULL/* events */
	, &Repetition_t1120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Repetition_t1120_0_0_0/* byval_arg */
	, &Repetition_t1120_1_0_0/* this_arg */
	, &Repetition_t1120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Repetition_t1120)/* instance_size */
	, sizeof (Repetition_t1120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_Assertion.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Assertion
extern TypeInfo Assertion_t1121_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Assertion
#include "System_System_Text_RegularExpressions_Syntax_AssertionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::.ctor()
extern const MethodInfo Assertion__ctor_m4537_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Assertion__ctor_m4537/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_TrueExpression()
extern const MethodInfo Assertion_get_TrueExpression_m4538_MethodInfo = 
{
	"get_TrueExpression"/* name */
	, (methodPointerType)&Assertion_get_TrueExpression_m4538/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo Assertion_t1121_Assertion_set_TrueExpression_m4539_ParameterInfos[] = 
{
	{"value", 0, 134218545, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_TrueExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_TrueExpression_m4539_MethodInfo = 
{
	"set_TrueExpression"/* name */
	, (methodPointerType)&Assertion_set_TrueExpression_m4539/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Assertion_t1121_Assertion_set_TrueExpression_m4539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.Assertion::get_FalseExpression()
extern const MethodInfo Assertion_get_FalseExpression_m4540_MethodInfo = 
{
	"get_FalseExpression"/* name */
	, (methodPointerType)&Assertion_get_FalseExpression_m4540/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo Assertion_t1121_Assertion_set_FalseExpression_m4541_ParameterInfos[] = 
{
	{"value", 0, 134218546, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::set_FalseExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Assertion_set_FalseExpression_m4541_MethodInfo = 
{
	"set_FalseExpression"/* name */
	, (methodPointerType)&Assertion_set_FalseExpression_m4541/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Assertion_t1121_Assertion_set_FalseExpression_m4541_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Assertion_t1121_Assertion_GetWidth_m4542_ParameterInfos[] = 
{
	{"min", 0, 134218547, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218548, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Assertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Assertion_GetWidth_m4542_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Assertion_GetWidth_m4542/* method */
	, &Assertion_t1121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Assertion_t1121_Assertion_GetWidth_m4542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Assertion_t1121_MethodInfos[] =
{
	&Assertion__ctor_m4537_MethodInfo,
	&Assertion_get_TrueExpression_m4538_MethodInfo,
	&Assertion_set_TrueExpression_m4539_MethodInfo,
	&Assertion_get_FalseExpression_m4540_MethodInfo,
	&Assertion_set_FalseExpression_m4541_MethodInfo,
	&Assertion_GetWidth_m4542_MethodInfo,
	NULL
};
extern const MethodInfo Assertion_get_TrueExpression_m4538_MethodInfo;
extern const MethodInfo Assertion_set_TrueExpression_m4539_MethodInfo;
static const PropertyInfo Assertion_t1121____TrueExpression_PropertyInfo = 
{
	&Assertion_t1121_il2cpp_TypeInfo/* parent */
	, "TrueExpression"/* name */
	, &Assertion_get_TrueExpression_m4538_MethodInfo/* get */
	, &Assertion_set_TrueExpression_m4539_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Assertion_get_FalseExpression_m4540_MethodInfo;
extern const MethodInfo Assertion_set_FalseExpression_m4541_MethodInfo;
static const PropertyInfo Assertion_t1121____FalseExpression_PropertyInfo = 
{
	&Assertion_t1121_il2cpp_TypeInfo/* parent */
	, "FalseExpression"/* name */
	, &Assertion_get_FalseExpression_m4540_MethodInfo/* get */
	, &Assertion_set_FalseExpression_m4541_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Assertion_t1121_PropertyInfos[] =
{
	&Assertion_t1121____TrueExpression_PropertyInfo,
	&Assertion_t1121____FalseExpression_PropertyInfo,
	NULL
};
extern const MethodInfo Assertion_GetWidth_m4542_MethodInfo;
static const Il2CppMethodReference Assertion_t1121_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	NULL,
	&Assertion_GetWidth_m4542_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool Assertion_t1121_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Assertion_t1121_1_0_0;
struct Assertion_t1121;
const Il2CppTypeDefinitionMetadata Assertion_t1121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1114_0_0_0/* parent */
	, Assertion_t1121_VTable/* vtableMethods */
	, Assertion_t1121_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Assertion_t1121_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Assertion_t1121_MethodInfos/* methods */
	, Assertion_t1121_PropertyInfos/* properties */
	, NULL/* events */
	, &Assertion_t1121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Assertion_t1121_0_0_0/* byval_arg */
	, &Assertion_t1121_1_0_0/* this_arg */
	, &Assertion_t1121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assertion_t1121)/* instance_size */
	, sizeof (Assertion_t1121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertio.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CaptureAssertion
extern TypeInfo CaptureAssertion_t1124_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CaptureAssertion
#include "System_System_Text_RegularExpressions_Syntax_CaptureAssertioMethodDeclarations.h"
extern const Il2CppType Literal_t1123_0_0_0;
extern const Il2CppType Literal_t1123_0_0_0;
static const ParameterInfo CaptureAssertion_t1124_CaptureAssertion__ctor_m4543_ParameterInfos[] = 
{
	{"l", 0, 134218549, 0, &Literal_t1123_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::.ctor(System.Text.RegularExpressions.Syntax.Literal)
extern const MethodInfo CaptureAssertion__ctor_m4543_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CaptureAssertion__ctor_m4543/* method */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CaptureAssertion_t1124_CaptureAssertion__ctor_m4543_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1117_0_0_0;
static const ParameterInfo CaptureAssertion_t1124_CaptureAssertion_set_CapturingGroup_m4544_ParameterInfos[] = 
{
	{"value", 0, 134218550, 0, &CapturingGroup_t1117_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m4544_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&CaptureAssertion_set_CapturingGroup_m4544/* method */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, CaptureAssertion_t1124_CaptureAssertion_set_CapturingGroup_m4544_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CaptureAssertion_t1124_CaptureAssertion_Compile_m4545_ParameterInfos[] = 
{
	{"cmp", 0, 134218551, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218552, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CaptureAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CaptureAssertion_Compile_m4545_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CaptureAssertion_Compile_m4545/* method */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, CaptureAssertion_t1124_CaptureAssertion_Compile_m4545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CaptureAssertion::IsComplex()
extern const MethodInfo CaptureAssertion_IsComplex_m4546_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CaptureAssertion_IsComplex_m4546/* method */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::get_Alternate()
extern const MethodInfo CaptureAssertion_get_Alternate_m4547_MethodInfo = 
{
	"get_Alternate"/* name */
	, (methodPointerType)&CaptureAssertion_get_Alternate_m4547/* method */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionAssertion_t1122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CaptureAssertion_t1124_MethodInfos[] =
{
	&CaptureAssertion__ctor_m4543_MethodInfo,
	&CaptureAssertion_set_CapturingGroup_m4544_MethodInfo,
	&CaptureAssertion_Compile_m4545_MethodInfo,
	&CaptureAssertion_IsComplex_m4546_MethodInfo,
	&CaptureAssertion_get_Alternate_m4547_MethodInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_set_CapturingGroup_m4544_MethodInfo;
static const PropertyInfo CaptureAssertion_t1124____CapturingGroup_PropertyInfo = 
{
	&CaptureAssertion_t1124_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, NULL/* get */
	, &CaptureAssertion_set_CapturingGroup_m4544_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CaptureAssertion_get_Alternate_m4547_MethodInfo;
static const PropertyInfo CaptureAssertion_t1124____Alternate_PropertyInfo = 
{
	&CaptureAssertion_t1124_il2cpp_TypeInfo/* parent */
	, "Alternate"/* name */
	, &CaptureAssertion_get_Alternate_m4547_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CaptureAssertion_t1124_PropertyInfos[] =
{
	&CaptureAssertion_t1124____CapturingGroup_PropertyInfo,
	&CaptureAssertion_t1124____Alternate_PropertyInfo,
	NULL
};
extern const MethodInfo CaptureAssertion_Compile_m4545_MethodInfo;
extern const MethodInfo CaptureAssertion_IsComplex_m4546_MethodInfo;
static const Il2CppMethodReference CaptureAssertion_t1124_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CaptureAssertion_Compile_m4545_MethodInfo,
	&Assertion_GetWidth_m4542_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&CaptureAssertion_IsComplex_m4546_MethodInfo,
};
static bool CaptureAssertion_t1124_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureAssertion_t1124_0_0_0;
extern const Il2CppType CaptureAssertion_t1124_1_0_0;
struct CaptureAssertion_t1124;
const Il2CppTypeDefinitionMetadata CaptureAssertion_t1124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1121_0_0_0/* parent */
	, CaptureAssertion_t1124_VTable/* vtableMethods */
	, CaptureAssertion_t1124_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 726/* fieldStart */

};
TypeInfo CaptureAssertion_t1124_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CaptureAssertion_t1124_MethodInfos/* methods */
	, CaptureAssertion_t1124_PropertyInfos/* properties */
	, NULL/* events */
	, &CaptureAssertion_t1124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CaptureAssertion_t1124_0_0_0/* byval_arg */
	, &CaptureAssertion_t1124_1_0_0/* this_arg */
	, &CaptureAssertion_t1124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureAssertion_t1124)/* instance_size */
	, sizeof (CaptureAssertion_t1124)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsser.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.ExpressionAssertion
extern TypeInfo ExpressionAssertion_t1122_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
#include "System_System_Text_RegularExpressions_Syntax_ExpressionAsserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::.ctor()
extern const MethodInfo ExpressionAssertion__ctor_m4548_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExpressionAssertion__ctor_m4548/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ExpressionAssertion_t1122_ExpressionAssertion_set_Reverse_m4549_ParameterInfos[] = 
{
	{"value", 0, 134218553, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Reverse(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Reverse_m4549_MethodInfo = 
{
	"set_Reverse"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Reverse_m4549/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ExpressionAssertion_t1122_ExpressionAssertion_set_Reverse_m4549_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ExpressionAssertion_t1122_ExpressionAssertion_set_Negate_m4550_ParameterInfos[] = 
{
	{"value", 0, 134218554, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_Negate(System.Boolean)
extern const MethodInfo ExpressionAssertion_set_Negate_m4550_MethodInfo = 
{
	"set_Negate"/* name */
	, (methodPointerType)&ExpressionAssertion_set_Negate_m4550/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, ExpressionAssertion_t1122_ExpressionAssertion_set_Negate_m4550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionAssertion::get_TestExpression()
extern const MethodInfo ExpressionAssertion_get_TestExpression_m4551_MethodInfo = 
{
	"get_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_get_TestExpression_m4551/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Expression_t1113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo ExpressionAssertion_t1122_ExpressionAssertion_set_TestExpression_m4552_ParameterInfos[] = 
{
	{"value", 0, 134218555, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::set_TestExpression(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo ExpressionAssertion_set_TestExpression_m4552_MethodInfo = 
{
	"set_TestExpression"/* name */
	, (methodPointerType)&ExpressionAssertion_set_TestExpression_m4552/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, ExpressionAssertion_t1122_ExpressionAssertion_set_TestExpression_m4552_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo ExpressionAssertion_t1122_ExpressionAssertion_Compile_m4553_ParameterInfos[] = 
{
	{"cmp", 0, 134218556, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218557, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.ExpressionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo ExpressionAssertion_Compile_m4553_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&ExpressionAssertion_Compile_m4553/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, ExpressionAssertion_t1122_ExpressionAssertion_Compile_m4553_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::IsComplex()
extern const MethodInfo ExpressionAssertion_IsComplex_m4554_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&ExpressionAssertion_IsComplex_m4554/* method */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExpressionAssertion_t1122_MethodInfos[] =
{
	&ExpressionAssertion__ctor_m4548_MethodInfo,
	&ExpressionAssertion_set_Reverse_m4549_MethodInfo,
	&ExpressionAssertion_set_Negate_m4550_MethodInfo,
	&ExpressionAssertion_get_TestExpression_m4551_MethodInfo,
	&ExpressionAssertion_set_TestExpression_m4552_MethodInfo,
	&ExpressionAssertion_Compile_m4553_MethodInfo,
	&ExpressionAssertion_IsComplex_m4554_MethodInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_set_Reverse_m4549_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1122____Reverse_PropertyInfo = 
{
	&ExpressionAssertion_t1122_il2cpp_TypeInfo/* parent */
	, "Reverse"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Reverse_m4549_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_set_Negate_m4550_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1122____Negate_PropertyInfo = 
{
	&ExpressionAssertion_t1122_il2cpp_TypeInfo/* parent */
	, "Negate"/* name */
	, NULL/* get */
	, &ExpressionAssertion_set_Negate_m4550_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ExpressionAssertion_get_TestExpression_m4551_MethodInfo;
extern const MethodInfo ExpressionAssertion_set_TestExpression_m4552_MethodInfo;
static const PropertyInfo ExpressionAssertion_t1122____TestExpression_PropertyInfo = 
{
	&ExpressionAssertion_t1122_il2cpp_TypeInfo/* parent */
	, "TestExpression"/* name */
	, &ExpressionAssertion_get_TestExpression_m4551_MethodInfo/* get */
	, &ExpressionAssertion_set_TestExpression_m4552_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ExpressionAssertion_t1122_PropertyInfos[] =
{
	&ExpressionAssertion_t1122____Reverse_PropertyInfo,
	&ExpressionAssertion_t1122____Negate_PropertyInfo,
	&ExpressionAssertion_t1122____TestExpression_PropertyInfo,
	NULL
};
extern const MethodInfo ExpressionAssertion_Compile_m4553_MethodInfo;
extern const MethodInfo ExpressionAssertion_IsComplex_m4554_MethodInfo;
static const Il2CppMethodReference ExpressionAssertion_t1122_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&ExpressionAssertion_Compile_m4553_MethodInfo,
	&Assertion_GetWidth_m4542_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&ExpressionAssertion_IsComplex_m4554_MethodInfo,
};
static bool ExpressionAssertion_t1122_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ExpressionAssertion_t1122_1_0_0;
struct ExpressionAssertion_t1122;
const Il2CppTypeDefinitionMetadata ExpressionAssertion_t1122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Assertion_t1121_0_0_0/* parent */
	, ExpressionAssertion_t1122_VTable/* vtableMethods */
	, ExpressionAssertion_t1122_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 729/* fieldStart */

};
TypeInfo ExpressionAssertion_t1122_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExpressionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, ExpressionAssertion_t1122_MethodInfos/* methods */
	, ExpressionAssertion_t1122_PropertyInfos/* properties */
	, NULL/* events */
	, &ExpressionAssertion_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExpressionAssertion_t1122_0_0_0/* byval_arg */
	, &ExpressionAssertion_t1122_1_0_0/* this_arg */
	, &ExpressionAssertion_t1122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExpressionAssertion_t1122)/* instance_size */
	, sizeof (ExpressionAssertion_t1122)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_Alternation.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Alternation
extern TypeInfo Alternation_t1125_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Alternation
#include "System_System_Text_RegularExpressions_Syntax_AlternationMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::.ctor()
extern const MethodInfo Alternation__ctor_m4555_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Alternation__ctor_m4555/* method */
	, &Alternation_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.Alternation::get_Alternatives()
extern const MethodInfo Alternation_get_Alternatives_m4556_MethodInfo = 
{
	"get_Alternatives"/* name */
	, (methodPointerType)&Alternation_get_Alternatives_m4556/* method */
	, &Alternation_t1125_il2cpp_TypeInfo/* declaring_type */
	, &ExpressionCollection_t1112_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
static const ParameterInfo Alternation_t1125_Alternation_AddAlternative_m4557_ParameterInfos[] = 
{
	{"e", 0, 134218558, 0, &Expression_t1113_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::AddAlternative(System.Text.RegularExpressions.Syntax.Expression)
extern const MethodInfo Alternation_AddAlternative_m4557_MethodInfo = 
{
	"AddAlternative"/* name */
	, (methodPointerType)&Alternation_AddAlternative_m4557/* method */
	, &Alternation_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Alternation_t1125_Alternation_AddAlternative_m4557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Alternation_t1125_Alternation_Compile_m4558_ParameterInfos[] = 
{
	{"cmp", 0, 134218559, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218560, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Alternation_Compile_m4558_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Alternation_Compile_m4558/* method */
	, &Alternation_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Alternation_t1125_Alternation_Compile_m4558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Alternation_t1125_Alternation_GetWidth_m4559_ParameterInfos[] = 
{
	{"min", 0, 134218561, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218562, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Alternation::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Alternation_GetWidth_m4559_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Alternation_GetWidth_m4559/* method */
	, &Alternation_t1125_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Alternation_t1125_Alternation_GetWidth_m4559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Alternation_t1125_MethodInfos[] =
{
	&Alternation__ctor_m4555_MethodInfo,
	&Alternation_get_Alternatives_m4556_MethodInfo,
	&Alternation_AddAlternative_m4557_MethodInfo,
	&Alternation_Compile_m4558_MethodInfo,
	&Alternation_GetWidth_m4559_MethodInfo,
	NULL
};
extern const MethodInfo Alternation_get_Alternatives_m4556_MethodInfo;
static const PropertyInfo Alternation_t1125____Alternatives_PropertyInfo = 
{
	&Alternation_t1125_il2cpp_TypeInfo/* parent */
	, "Alternatives"/* name */
	, &Alternation_get_Alternatives_m4556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Alternation_t1125_PropertyInfos[] =
{
	&Alternation_t1125____Alternatives_PropertyInfo,
	NULL
};
extern const MethodInfo Alternation_Compile_m4558_MethodInfo;
extern const MethodInfo Alternation_GetWidth_m4559_MethodInfo;
static const Il2CppMethodReference Alternation_t1125_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Alternation_Compile_m4558_MethodInfo,
	&Alternation_GetWidth_m4559_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&CompositeExpression_IsComplex_m4506_MethodInfo,
};
static bool Alternation_t1125_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Alternation_t1125_0_0_0;
extern const Il2CppType Alternation_t1125_1_0_0;
struct Alternation_t1125;
const Il2CppTypeDefinitionMetadata Alternation_t1125_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CompositeExpression_t1114_0_0_0/* parent */
	, Alternation_t1125_VTable/* vtableMethods */
	, Alternation_t1125_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Alternation_t1125_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alternation"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Alternation_t1125_MethodInfos/* methods */
	, Alternation_t1125_PropertyInfos/* properties */
	, NULL/* events */
	, &Alternation_t1125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alternation_t1125_0_0_0/* byval_arg */
	, &Alternation_t1125_1_0_0/* this_arg */
	, &Alternation_t1125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alternation_t1125)/* instance_size */
	, sizeof (Alternation_t1125)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_Literal.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Literal
extern TypeInfo Literal_t1123_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Literal
#include "System_System_Text_RegularExpressions_Syntax_LiteralMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Literal_t1123_Literal__ctor_m4560_ParameterInfos[] = 
{
	{"str", 0, 134218563, 0, &String_t_0_0_0},
	{"ignore", 1, 134218564, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::.ctor(System.String,System.Boolean)
extern const MethodInfo Literal__ctor_m4560_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Literal__ctor_m4560/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Literal_t1123_Literal__ctor_m4560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Literal_t1123_Literal_CompileLiteral_m4561_ParameterInfos[] = 
{
	{"str", 0, 134218565, 0, &String_t_0_0_0},
	{"cmp", 1, 134218566, 0, &ICompiler_t1156_0_0_0},
	{"ignore", 2, 134218567, 0, &Boolean_t72_0_0_0},
	{"reverse", 3, 134218568, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::CompileLiteral(System.String,System.Text.RegularExpressions.ICompiler,System.Boolean,System.Boolean)
extern const MethodInfo Literal_CompileLiteral_m4561_MethodInfo = 
{
	"CompileLiteral"/* name */
	, (methodPointerType)&Literal_CompileLiteral_m4561/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_SByte_t73_SByte_t73/* invoker_method */
	, Literal_t1123_Literal_CompileLiteral_m4561_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Literal_t1123_Literal_Compile_m4562_ParameterInfos[] = 
{
	{"cmp", 0, 134218569, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218570, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Literal_Compile_m4562_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Literal_Compile_m4562/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Literal_t1123_Literal_Compile_m4562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Literal_t1123_Literal_GetWidth_m4563_ParameterInfos[] = 
{
	{"min", 0, 134218571, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218572, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Literal::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Literal_GetWidth_m4563_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Literal_GetWidth_m4563/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Literal_t1123_Literal_GetWidth_m4563_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Literal_t1123_Literal_GetAnchorInfo_m4564_ParameterInfos[] = 
{
	{"reverse", 0, 134218573, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.Literal::GetAnchorInfo(System.Boolean)
extern const MethodInfo Literal_GetAnchorInfo_m4564_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&Literal_GetAnchorInfo_m4564/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, Literal_t1123_Literal_GetAnchorInfo_m4564_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Literal::IsComplex()
extern const MethodInfo Literal_IsComplex_m4565_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Literal_IsComplex_m4565/* method */
	, &Literal_t1123_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Literal_t1123_MethodInfos[] =
{
	&Literal__ctor_m4560_MethodInfo,
	&Literal_CompileLiteral_m4561_MethodInfo,
	&Literal_Compile_m4562_MethodInfo,
	&Literal_GetWidth_m4563_MethodInfo,
	&Literal_GetAnchorInfo_m4564_MethodInfo,
	&Literal_IsComplex_m4565_MethodInfo,
	NULL
};
extern const MethodInfo Literal_Compile_m4562_MethodInfo;
extern const MethodInfo Literal_GetWidth_m4563_MethodInfo;
extern const MethodInfo Literal_GetAnchorInfo_m4564_MethodInfo;
extern const MethodInfo Literal_IsComplex_m4565_MethodInfo;
static const Il2CppMethodReference Literal_t1123_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Literal_Compile_m4562_MethodInfo,
	&Literal_GetWidth_m4563_MethodInfo,
	&Literal_GetAnchorInfo_m4564_MethodInfo,
	&Literal_IsComplex_m4565_MethodInfo,
};
static bool Literal_t1123_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Literal_t1123_1_0_0;
struct Literal_t1123;
const Il2CppTypeDefinitionMetadata Literal_t1123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1113_0_0_0/* parent */
	, Literal_t1123_VTable/* vtableMethods */
	, Literal_t1123_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 731/* fieldStart */

};
TypeInfo Literal_t1123_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Literal"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Literal_t1123_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Literal_t1123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Literal_t1123_0_0_0/* byval_arg */
	, &Literal_t1123_1_0_0/* this_arg */
	, &Literal_t1123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Literal_t1123)/* instance_size */
	, sizeof (Literal_t1123)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAsserti.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.PositionAssertion
extern TypeInfo PositionAssertion_t1126_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.PositionAssertion
#include "System_System_Text_RegularExpressions_Syntax_PositionAssertiMethodDeclarations.h"
extern const Il2CppType Position_t1087_0_0_0;
static const ParameterInfo PositionAssertion_t1126_PositionAssertion__ctor_m4566_ParameterInfos[] = 
{
	{"pos", 0, 134218574, 0, &Position_t1087_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::.ctor(System.Text.RegularExpressions.Position)
extern const MethodInfo PositionAssertion__ctor_m4566_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAssertion__ctor_m4566/* method */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371/* invoker_method */
	, PositionAssertion_t1126_PositionAssertion__ctor_m4566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PositionAssertion_t1126_PositionAssertion_Compile_m4567_ParameterInfos[] = 
{
	{"cmp", 0, 134218575, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218576, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo PositionAssertion_Compile_m4567_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&PositionAssertion_Compile_m4567/* method */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, PositionAssertion_t1126_PositionAssertion_Compile_m4567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo PositionAssertion_t1126_PositionAssertion_GetWidth_m4568_ParameterInfos[] = 
{
	{"min", 0, 134218577, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218578, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.PositionAssertion::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo PositionAssertion_GetWidth_m4568_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&PositionAssertion_GetWidth_m4568/* method */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, PositionAssertion_t1126_PositionAssertion_GetWidth_m4568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.PositionAssertion::IsComplex()
extern const MethodInfo PositionAssertion_IsComplex_m4569_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&PositionAssertion_IsComplex_m4569/* method */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo PositionAssertion_t1126_PositionAssertion_GetAnchorInfo_m4570_ParameterInfos[] = 
{
	{"revers", 0, 134218579, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.AnchorInfo System.Text.RegularExpressions.Syntax.PositionAssertion::GetAnchorInfo(System.Boolean)
extern const MethodInfo PositionAssertion_GetAnchorInfo_m4570_MethodInfo = 
{
	"GetAnchorInfo"/* name */
	, (methodPointerType)&PositionAssertion_GetAnchorInfo_m4570/* method */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* declaring_type */
	, &AnchorInfo_t1131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t73/* invoker_method */
	, PositionAssertion_t1126_PositionAssertion_GetAnchorInfo_m4570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAssertion_t1126_MethodInfos[] =
{
	&PositionAssertion__ctor_m4566_MethodInfo,
	&PositionAssertion_Compile_m4567_MethodInfo,
	&PositionAssertion_GetWidth_m4568_MethodInfo,
	&PositionAssertion_IsComplex_m4569_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m4570_MethodInfo,
	NULL
};
extern const MethodInfo PositionAssertion_Compile_m4567_MethodInfo;
extern const MethodInfo PositionAssertion_GetWidth_m4568_MethodInfo;
extern const MethodInfo PositionAssertion_GetAnchorInfo_m4570_MethodInfo;
extern const MethodInfo PositionAssertion_IsComplex_m4569_MethodInfo;
static const Il2CppMethodReference PositionAssertion_t1126_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&PositionAssertion_Compile_m4567_MethodInfo,
	&PositionAssertion_GetWidth_m4568_MethodInfo,
	&PositionAssertion_GetAnchorInfo_m4570_MethodInfo,
	&PositionAssertion_IsComplex_m4569_MethodInfo,
};
static bool PositionAssertion_t1126_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PositionAssertion_t1126_0_0_0;
extern const Il2CppType PositionAssertion_t1126_1_0_0;
struct PositionAssertion_t1126;
const Il2CppTypeDefinitionMetadata PositionAssertion_t1126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1113_0_0_0/* parent */
	, PositionAssertion_t1126_VTable/* vtableMethods */
	, PositionAssertion_t1126_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 733/* fieldStart */

};
TypeInfo PositionAssertion_t1126_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAssertion"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, PositionAssertion_t1126_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAssertion_t1126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PositionAssertion_t1126_0_0_0/* byval_arg */
	, &PositionAssertion_t1126_1_0_0/* this_arg */
	, &PositionAssertion_t1126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAssertion_t1126)/* instance_size */
	, sizeof (PositionAssertion_t1126)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_Reference.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.Reference
extern TypeInfo Reference_t1127_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.Reference
#include "System_System_Text_RegularExpressions_Syntax_ReferenceMethodDeclarations.h"
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Reference_t1127_Reference__ctor_m4571_ParameterInfos[] = 
{
	{"ignore", 0, 134218580, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::.ctor(System.Boolean)
extern const MethodInfo Reference__ctor_m4571_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Reference__ctor_m4571/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73/* invoker_method */
	, Reference_t1127_Reference__ctor_m4571_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::get_CapturingGroup()
extern const MethodInfo Reference_get_CapturingGroup_m4572_MethodInfo = 
{
	"get_CapturingGroup"/* name */
	, (methodPointerType)&Reference_get_CapturingGroup_m4572/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &CapturingGroup_t1117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapturingGroup_t1117_0_0_0;
static const ParameterInfo Reference_t1127_Reference_set_CapturingGroup_m4573_ParameterInfos[] = 
{
	{"value", 0, 134218581, 0, &CapturingGroup_t1117_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::set_CapturingGroup(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern const MethodInfo Reference_set_CapturingGroup_m4573_MethodInfo = 
{
	"set_CapturingGroup"/* name */
	, (methodPointerType)&Reference_set_CapturingGroup_m4573/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Reference_t1127_Reference_set_CapturingGroup_m4573_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::get_IgnoreCase()
extern const MethodInfo Reference_get_IgnoreCase_m4574_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&Reference_get_IgnoreCase_m4574/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Reference_t1127_Reference_Compile_m4575_ParameterInfos[] = 
{
	{"cmp", 0, 134218582, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218583, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo Reference_Compile_m4575_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&Reference_Compile_m4575/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Reference_t1127_Reference_Compile_m4575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo Reference_t1127_Reference_GetWidth_m4576_ParameterInfos[] = 
{
	{"min", 0, 134218584, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218585, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.Reference::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo Reference_GetWidth_m4576_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&Reference_GetWidth_m4576/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, Reference_t1127_Reference_GetWidth_m4576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.Reference::IsComplex()
extern const MethodInfo Reference_IsComplex_m4577_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&Reference_IsComplex_m4577/* method */
	, &Reference_t1127_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Reference_t1127_MethodInfos[] =
{
	&Reference__ctor_m4571_MethodInfo,
	&Reference_get_CapturingGroup_m4572_MethodInfo,
	&Reference_set_CapturingGroup_m4573_MethodInfo,
	&Reference_get_IgnoreCase_m4574_MethodInfo,
	&Reference_Compile_m4575_MethodInfo,
	&Reference_GetWidth_m4576_MethodInfo,
	&Reference_IsComplex_m4577_MethodInfo,
	NULL
};
extern const MethodInfo Reference_get_CapturingGroup_m4572_MethodInfo;
extern const MethodInfo Reference_set_CapturingGroup_m4573_MethodInfo;
static const PropertyInfo Reference_t1127____CapturingGroup_PropertyInfo = 
{
	&Reference_t1127_il2cpp_TypeInfo/* parent */
	, "CapturingGroup"/* name */
	, &Reference_get_CapturingGroup_m4572_MethodInfo/* get */
	, &Reference_set_CapturingGroup_m4573_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Reference_get_IgnoreCase_m4574_MethodInfo;
static const PropertyInfo Reference_t1127____IgnoreCase_PropertyInfo = 
{
	&Reference_t1127_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &Reference_get_IgnoreCase_m4574_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Reference_t1127_PropertyInfos[] =
{
	&Reference_t1127____CapturingGroup_PropertyInfo,
	&Reference_t1127____IgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo Reference_Compile_m4575_MethodInfo;
extern const MethodInfo Reference_GetWidth_m4576_MethodInfo;
extern const MethodInfo Reference_IsComplex_m4577_MethodInfo;
static const Il2CppMethodReference Reference_t1127_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&Reference_Compile_m4575_MethodInfo,
	&Reference_GetWidth_m4576_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&Reference_IsComplex_m4577_MethodInfo,
};
static bool Reference_t1127_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Reference_t1127_0_0_0;
extern const Il2CppType Reference_t1127_1_0_0;
struct Reference_t1127;
const Il2CppTypeDefinitionMetadata Reference_t1127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1113_0_0_0/* parent */
	, Reference_t1127_VTable/* vtableMethods */
	, Reference_t1127_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 734/* fieldStart */

};
TypeInfo Reference_t1127_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reference"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, Reference_t1127_MethodInfos/* methods */
	, Reference_t1127_PropertyInfos/* properties */
	, NULL/* events */
	, &Reference_t1127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reference_t1127_0_0_0/* byval_arg */
	, &Reference_t1127_1_0_0/* this_arg */
	, &Reference_t1127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reference_t1127)/* instance_size */
	, sizeof (Reference_t1127)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumber.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.BackslashNumber
extern TypeInfo BackslashNumber_t1128_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.BackslashNumber
#include "System_System_Text_RegularExpressions_Syntax_BackslashNumberMethodDeclarations.h"
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo BackslashNumber_t1128_BackslashNumber__ctor_m4578_ParameterInfos[] = 
{
	{"ignore", 0, 134218586, 0, &Boolean_t72_0_0_0},
	{"ecma", 1, 134218587, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo BackslashNumber__ctor_m4578_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackslashNumber__ctor_m4578/* method */
	, &BackslashNumber_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_SByte_t73/* invoker_method */
	, BackslashNumber_t1128_BackslashNumber__ctor_m4578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Hashtable_t975_0_0_0;
static const ParameterInfo BackslashNumber_t1128_BackslashNumber_ResolveReference_m4579_ParameterInfos[] = 
{
	{"num_str", 0, 134218588, 0, &String_t_0_0_0},
	{"groups", 1, 134218589, 0, &Hashtable_t975_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
extern const MethodInfo BackslashNumber_ResolveReference_m4579_MethodInfo = 
{
	"ResolveReference"/* name */
	, (methodPointerType)&BackslashNumber_ResolveReference_m4579/* method */
	, &BackslashNumber_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, BackslashNumber_t1128_BackslashNumber_ResolveReference_m4579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo BackslashNumber_t1128_BackslashNumber_Compile_m4580_ParameterInfos[] = 
{
	{"cmp", 0, 134218590, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218591, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo BackslashNumber_Compile_m4580_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&BackslashNumber_Compile_m4580/* method */
	, &BackslashNumber_t1128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, BackslashNumber_t1128_BackslashNumber_Compile_m4580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BackslashNumber_t1128_MethodInfos[] =
{
	&BackslashNumber__ctor_m4578_MethodInfo,
	&BackslashNumber_ResolveReference_m4579_MethodInfo,
	&BackslashNumber_Compile_m4580_MethodInfo,
	NULL
};
extern const MethodInfo BackslashNumber_Compile_m4580_MethodInfo;
static const Il2CppMethodReference BackslashNumber_t1128_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&BackslashNumber_Compile_m4580_MethodInfo,
	&Reference_GetWidth_m4576_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&Reference_IsComplex_m4577_MethodInfo,
};
static bool BackslashNumber_t1128_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BackslashNumber_t1128_0_0_0;
extern const Il2CppType BackslashNumber_t1128_1_0_0;
struct BackslashNumber_t1128;
const Il2CppTypeDefinitionMetadata BackslashNumber_t1128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Reference_t1127_0_0_0/* parent */
	, BackslashNumber_t1128_VTable/* vtableMethods */
	, BackslashNumber_t1128_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 736/* fieldStart */

};
TypeInfo BackslashNumber_t1128_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackslashNumber"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, BackslashNumber_t1128_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BackslashNumber_t1128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackslashNumber_t1128_0_0_0/* byval_arg */
	, &BackslashNumber_t1128_1_0_0/* this_arg */
	, &BackslashNumber_t1128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackslashNumber_t1128)/* instance_size */
	, sizeof (BackslashNumber_t1128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClass.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.CharacterClass
extern TypeInfo CharacterClass_t1130_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.CharacterClass
#include "System_System_Text_RegularExpressions_Syntax_CharacterClassMethodDeclarations.h"
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass__ctor_m4581_ParameterInfos[] = 
{
	{"negate", 0, 134218592, 0, &Boolean_t72_0_0_0},
	{"ignore", 1, 134218593, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Boolean,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m4581_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m4581/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_SByte_t73_SByte_t73/* invoker_method */
	, CharacterClass_t1130_CharacterClass__ctor_m4581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass__ctor_m4582_ParameterInfos[] = 
{
	{"cat", 0, 134218594, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218595, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.ctor(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass__ctor_m4582_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CharacterClass__ctor_m4582/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73/* invoker_method */
	, CharacterClass_t1130_CharacterClass__ctor_m4582_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::.cctor()
extern const MethodInfo CharacterClass__cctor_m4583_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CharacterClass__cctor_m4583/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Category_t1091_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass_AddCategory_m4584_ParameterInfos[] = 
{
	{"cat", 0, 134218596, 0, &Category_t1091_0_0_0},
	{"negate", 1, 134218597, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCategory(System.Text.RegularExpressions.Category,System.Boolean)
extern const MethodInfo CharacterClass_AddCategory_m4584_MethodInfo = 
{
	"AddCategory"/* name */
	, (methodPointerType)&CharacterClass_AddCategory_m4584/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_UInt16_t371_SByte_t73/* invoker_method */
	, CharacterClass_t1130_CharacterClass_AddCategory_m4584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass_AddCharacter_m4585_ParameterInfos[] = 
{
	{"c", 0, 134218598, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddCharacter(System.Char)
extern const MethodInfo CharacterClass_AddCharacter_m4585_MethodInfo = 
{
	"AddCharacter"/* name */
	, (methodPointerType)&CharacterClass_AddCharacter_m4585/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448/* invoker_method */
	, CharacterClass_t1130_CharacterClass_AddCharacter_m4585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass_AddRange_m4586_ParameterInfos[] = 
{
	{"lo", 0, 134218599, 0, &Char_t369_0_0_0},
	{"hi", 1, 134218600, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int16_t448_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::AddRange(System.Char,System.Char)
extern const MethodInfo CharacterClass_AddRange_m4586_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&CharacterClass_AddRange_m4586/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int16_t448_Int16_t448/* invoker_method */
	, CharacterClass_t1130_CharacterClass_AddRange_m4586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICompiler_t1156_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass_Compile_m4587_ParameterInfos[] = 
{
	{"cmp", 0, 134218601, 0, &ICompiler_t1156_0_0_0},
	{"reverse", 1, 134218602, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern const MethodInfo CharacterClass_Compile_m4587_MethodInfo = 
{
	"Compile"/* name */
	, (methodPointerType)&CharacterClass_Compile_m4587/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, CharacterClass_t1130_CharacterClass_Compile_m4587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_1_0_2;
extern const Il2CppType Int32_t54_1_0_2;
static const ParameterInfo CharacterClass_t1130_CharacterClass_GetWidth_m4588_ParameterInfos[] = 
{
	{"min", 0, 134218603, 0, &Int32_t54_1_0_2},
	{"max", 1, 134218604, 0, &Int32_t54_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.CharacterClass::GetWidth(System.Int32&,System.Int32&)
extern const MethodInfo CharacterClass_GetWidth_m4588_MethodInfo = 
{
	"GetWidth"/* name */
	, (methodPointerType)&CharacterClass_GetWidth_m4588/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32U26_t449_Int32U26_t449/* invoker_method */
	, CharacterClass_t1130_CharacterClass_GetWidth_m4588_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::IsComplex()
extern const MethodInfo CharacterClass_IsComplex_m4589_MethodInfo = 
{
	"IsComplex"/* name */
	, (methodPointerType)&CharacterClass_IsComplex_m4589/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Interval_t1106_0_0_0;
static const ParameterInfo CharacterClass_t1130_CharacterClass_GetIntervalCost_m4590_ParameterInfos[] = 
{
	{"i", 0, 134218605, 0, &Interval_t1106_0_0_0},
};
extern void* RuntimeInvoker_Double_t752_Interval_t1106 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Text.RegularExpressions.Syntax.CharacterClass::GetIntervalCost(System.Text.RegularExpressions.Interval)
extern const MethodInfo CharacterClass_GetIntervalCost_m4590_MethodInfo = 
{
	"GetIntervalCost"/* name */
	, (methodPointerType)&CharacterClass_GetIntervalCost_m4590/* method */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* declaring_type */
	, &Double_t752_0_0_0/* return_type */
	, RuntimeInvoker_Double_t752_Interval_t1106/* invoker_method */
	, CharacterClass_t1130_CharacterClass_GetIntervalCost_m4590_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CharacterClass_t1130_MethodInfos[] =
{
	&CharacterClass__ctor_m4581_MethodInfo,
	&CharacterClass__ctor_m4582_MethodInfo,
	&CharacterClass__cctor_m4583_MethodInfo,
	&CharacterClass_AddCategory_m4584_MethodInfo,
	&CharacterClass_AddCharacter_m4585_MethodInfo,
	&CharacterClass_AddRange_m4586_MethodInfo,
	&CharacterClass_Compile_m4587_MethodInfo,
	&CharacterClass_GetWidth_m4588_MethodInfo,
	&CharacterClass_IsComplex_m4589_MethodInfo,
	&CharacterClass_GetIntervalCost_m4590_MethodInfo,
	NULL
};
extern const MethodInfo CharacterClass_Compile_m4587_MethodInfo;
extern const MethodInfo CharacterClass_GetWidth_m4588_MethodInfo;
extern const MethodInfo CharacterClass_IsComplex_m4589_MethodInfo;
static const Il2CppMethodReference CharacterClass_t1130_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&CharacterClass_Compile_m4587_MethodInfo,
	&CharacterClass_GetWidth_m4588_MethodInfo,
	&Expression_GetAnchorInfo_m4502_MethodInfo,
	&CharacterClass_IsComplex_m4589_MethodInfo,
};
static bool CharacterClass_t1130_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CharacterClass_t1130_0_0_0;
extern const Il2CppType CharacterClass_t1130_1_0_0;
struct CharacterClass_t1130;
const Il2CppTypeDefinitionMetadata CharacterClass_t1130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Expression_t1113_0_0_0/* parent */
	, CharacterClass_t1130_VTable/* vtableMethods */
	, CharacterClass_t1130_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 738/* fieldStart */

};
TypeInfo CharacterClass_t1130_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterClass"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, CharacterClass_t1130_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CharacterClass_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterClass_t1130_0_0_0/* byval_arg */
	, &CharacterClass_t1130_1_0_0/* this_arg */
	, &CharacterClass_t1130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterClass_t1130)/* instance_size */
	, sizeof (CharacterClass_t1130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CharacterClass_t1130_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfo.h"
// Metadata Definition System.Text.RegularExpressions.Syntax.AnchorInfo
extern TypeInfo AnchorInfo_t1131_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Syntax.AnchorInfo
#include "System_System_Text_RegularExpressions_Syntax_AnchorInfoMethodDeclarations.h"
extern const Il2CppType Expression_t1113_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AnchorInfo_t1131_AnchorInfo__ctor_m4591_ParameterInfos[] = 
{
	{"expr", 0, 134218606, 0, &Expression_t1113_0_0_0},
	{"width", 1, 134218607, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32)
extern const MethodInfo AnchorInfo__ctor_m4591_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m4591/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, AnchorInfo_t1131_AnchorInfo__ctor_m4591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo AnchorInfo_t1131_AnchorInfo__ctor_m4592_ParameterInfos[] = 
{
	{"expr", 0, 134218608, 0, &Expression_t1113_0_0_0},
	{"offset", 1, 134218609, 0, &Int32_t54_0_0_0},
	{"width", 2, 134218610, 0, &Int32_t54_0_0_0},
	{"str", 3, 134218611, 0, &String_t_0_0_0},
	{"ignore", 4, 134218612, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.String,System.Boolean)
extern const MethodInfo AnchorInfo__ctor_m4592_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m4592/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_Object_t_SByte_t73/* invoker_method */
	, AnchorInfo_t1131_AnchorInfo__ctor_m4592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Expression_t1113_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
extern const Il2CppType Position_t1087_0_0_0;
static const ParameterInfo AnchorInfo_t1131_AnchorInfo__ctor_m4593_ParameterInfos[] = 
{
	{"expr", 0, 134218613, 0, &Expression_t1113_0_0_0},
	{"offset", 1, 134218614, 0, &Int32_t54_0_0_0},
	{"width", 2, 134218615, 0, &Int32_t54_0_0_0},
	{"pos", 3, 134218616, 0, &Position_t1087_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_UInt16_t371 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.Syntax.AnchorInfo::.ctor(System.Text.RegularExpressions.Syntax.Expression,System.Int32,System.Int32,System.Text.RegularExpressions.Position)
extern const MethodInfo AnchorInfo__ctor_m4593_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnchorInfo__ctor_m4593/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54_Int32_t54_UInt16_t371/* invoker_method */
	, AnchorInfo_t1131_AnchorInfo__ctor_m4593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Offset()
extern const MethodInfo AnchorInfo_get_Offset_m4594_MethodInfo = 
{
	"get_Offset"/* name */
	, (methodPointerType)&AnchorInfo_get_Offset_m4594/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Width()
extern const MethodInfo AnchorInfo_get_Width_m4595_MethodInfo = 
{
	"get_Width"/* name */
	, (methodPointerType)&AnchorInfo_get_Width_m4595/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::get_Length()
extern const MethodInfo AnchorInfo_get_Length_m4596_MethodInfo = 
{
	"get_Length"/* name */
	, (methodPointerType)&AnchorInfo_get_Length_m4596/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsUnknownWidth()
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m4597_MethodInfo = 
{
	"get_IsUnknownWidth"/* name */
	, (methodPointerType)&AnchorInfo_get_IsUnknownWidth_m4597/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsComplete()
extern const MethodInfo AnchorInfo_get_IsComplete_m4598_MethodInfo = 
{
	"get_IsComplete"/* name */
	, (methodPointerType)&AnchorInfo_get_IsComplete_m4598/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::get_Substring()
extern const MethodInfo AnchorInfo_get_Substring_m4599_MethodInfo = 
{
	"get_Substring"/* name */
	, (methodPointerType)&AnchorInfo_get_Substring_m4599/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IgnoreCase()
extern const MethodInfo AnchorInfo_get_IgnoreCase_m4600_MethodInfo = 
{
	"get_IgnoreCase"/* name */
	, (methodPointerType)&AnchorInfo_get_IgnoreCase_m4600/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Position_t1087 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::get_Position()
extern const MethodInfo AnchorInfo_get_Position_m4601_MethodInfo = 
{
	"get_Position"/* name */
	, (methodPointerType)&AnchorInfo_get_Position_m4601/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Position_t1087_0_0_0/* return_type */
	, RuntimeInvoker_Position_t1087/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsSubstring()
extern const MethodInfo AnchorInfo_get_IsSubstring_m4602_MethodInfo = 
{
	"get_IsSubstring"/* name */
	, (methodPointerType)&AnchorInfo_get_IsSubstring_m4602/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::get_IsPosition()
extern const MethodInfo AnchorInfo_get_IsPosition_m4603_MethodInfo = 
{
	"get_IsPosition"/* name */
	, (methodPointerType)&AnchorInfo_get_IsPosition_m4603/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo AnchorInfo_t1131_AnchorInfo_GetInterval_m4604_ParameterInfos[] = 
{
	{"start", 0, 134218617, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Interval_t1106_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.AnchorInfo::GetInterval(System.Int32)
extern const MethodInfo AnchorInfo_GetInterval_m4604_MethodInfo = 
{
	"GetInterval"/* name */
	, (methodPointerType)&AnchorInfo_GetInterval_m4604/* method */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* declaring_type */
	, &Interval_t1106_0_0_0/* return_type */
	, RuntimeInvoker_Interval_t1106_Int32_t54/* invoker_method */
	, AnchorInfo_t1131_AnchorInfo_GetInterval_m4604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AnchorInfo_t1131_MethodInfos[] =
{
	&AnchorInfo__ctor_m4591_MethodInfo,
	&AnchorInfo__ctor_m4592_MethodInfo,
	&AnchorInfo__ctor_m4593_MethodInfo,
	&AnchorInfo_get_Offset_m4594_MethodInfo,
	&AnchorInfo_get_Width_m4595_MethodInfo,
	&AnchorInfo_get_Length_m4596_MethodInfo,
	&AnchorInfo_get_IsUnknownWidth_m4597_MethodInfo,
	&AnchorInfo_get_IsComplete_m4598_MethodInfo,
	&AnchorInfo_get_Substring_m4599_MethodInfo,
	&AnchorInfo_get_IgnoreCase_m4600_MethodInfo,
	&AnchorInfo_get_Position_m4601_MethodInfo,
	&AnchorInfo_get_IsSubstring_m4602_MethodInfo,
	&AnchorInfo_get_IsPosition_m4603_MethodInfo,
	&AnchorInfo_GetInterval_m4604_MethodInfo,
	NULL
};
extern const MethodInfo AnchorInfo_get_Offset_m4594_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____Offset_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "Offset"/* name */
	, &AnchorInfo_get_Offset_m4594_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Width_m4595_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____Width_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "Width"/* name */
	, &AnchorInfo_get_Width_m4595_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Length_m4596_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____Length_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "Length"/* name */
	, &AnchorInfo_get_Length_m4596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsUnknownWidth_m4597_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____IsUnknownWidth_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "IsUnknownWidth"/* name */
	, &AnchorInfo_get_IsUnknownWidth_m4597_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsComplete_m4598_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____IsComplete_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "IsComplete"/* name */
	, &AnchorInfo_get_IsComplete_m4598_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Substring_m4599_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____Substring_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "Substring"/* name */
	, &AnchorInfo_get_Substring_m4599_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IgnoreCase_m4600_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____IgnoreCase_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "IgnoreCase"/* name */
	, &AnchorInfo_get_IgnoreCase_m4600_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_Position_m4601_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____Position_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "Position"/* name */
	, &AnchorInfo_get_Position_m4601_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsSubstring_m4602_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____IsSubstring_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "IsSubstring"/* name */
	, &AnchorInfo_get_IsSubstring_m4602_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AnchorInfo_get_IsPosition_m4603_MethodInfo;
static const PropertyInfo AnchorInfo_t1131____IsPosition_PropertyInfo = 
{
	&AnchorInfo_t1131_il2cpp_TypeInfo/* parent */
	, "IsPosition"/* name */
	, &AnchorInfo_get_IsPosition_m4603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AnchorInfo_t1131_PropertyInfos[] =
{
	&AnchorInfo_t1131____Offset_PropertyInfo,
	&AnchorInfo_t1131____Width_PropertyInfo,
	&AnchorInfo_t1131____Length_PropertyInfo,
	&AnchorInfo_t1131____IsUnknownWidth_PropertyInfo,
	&AnchorInfo_t1131____IsComplete_PropertyInfo,
	&AnchorInfo_t1131____Substring_PropertyInfo,
	&AnchorInfo_t1131____IgnoreCase_PropertyInfo,
	&AnchorInfo_t1131____Position_PropertyInfo,
	&AnchorInfo_t1131____IsSubstring_PropertyInfo,
	&AnchorInfo_t1131____IsPosition_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AnchorInfo_t1131_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool AnchorInfo_t1131_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AnchorInfo_t1131_1_0_0;
struct AnchorInfo_t1131;
const Il2CppTypeDefinitionMetadata AnchorInfo_t1131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnchorInfo_t1131_VTable/* vtableMethods */
	, AnchorInfo_t1131_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 744/* fieldStart */

};
TypeInfo AnchorInfo_t1131_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnchorInfo"/* name */
	, "System.Text.RegularExpressions.Syntax"/* namespaze */
	, AnchorInfo_t1131_MethodInfos/* methods */
	, AnchorInfo_t1131_PropertyInfos/* properties */
	, NULL/* events */
	, &AnchorInfo_t1131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnchorInfo_t1131_0_0_0/* byval_arg */
	, &AnchorInfo_t1131_1_0_0/* this_arg */
	, &AnchorInfo_t1131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnchorInfo_t1131)/* instance_size */
	, sizeof (AnchorInfo_t1131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 10/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DefaultUriParser
#include "System_System_DefaultUriParser.h"
// Metadata Definition System.DefaultUriParser
extern TypeInfo DefaultUriParser_t1132_il2cpp_TypeInfo;
// System.DefaultUriParser
#include "System_System_DefaultUriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor()
extern const MethodInfo DefaultUriParser__ctor_m4605_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m4605/* method */
	, &DefaultUriParser_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultUriParser_t1132_DefaultUriParser__ctor_m4606_ParameterInfos[] = 
{
	{"scheme", 0, 134218618, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.DefaultUriParser::.ctor(System.String)
extern const MethodInfo DefaultUriParser__ctor_m4606_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultUriParser__ctor_m4606/* method */
	, &DefaultUriParser_t1132_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, DefaultUriParser_t1132_DefaultUriParser__ctor_m4606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultUriParser_t1132_MethodInfos[] =
{
	&DefaultUriParser__ctor_m4605_MethodInfo,
	&DefaultUriParser__ctor_m4606_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_InitializeAndValidate_m4662_MethodInfo;
extern const MethodInfo UriParser_OnRegister_m4663_MethodInfo;
static const Il2CppMethodReference DefaultUriParser_t1132_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&UriParser_InitializeAndValidate_m4662_MethodInfo,
	&UriParser_OnRegister_m4663_MethodInfo,
};
static bool DefaultUriParser_t1132_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultUriParser_t1132_0_0_0;
extern const Il2CppType DefaultUriParser_t1132_1_0_0;
extern const Il2CppType UriParser_t1133_0_0_0;
struct DefaultUriParser_t1132;
const Il2CppTypeDefinitionMetadata DefaultUriParser_t1132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1133_0_0_0/* parent */
	, DefaultUriParser_t1132_VTable/* vtableMethods */
	, DefaultUriParser_t1132_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DefaultUriParser_t1132_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultUriParser"/* name */
	, "System"/* namespaze */
	, DefaultUriParser_t1132_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DefaultUriParser_t1132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultUriParser_t1132_0_0_0/* byval_arg */
	, &DefaultUriParser_t1132_1_0_0/* this_arg */
	, &DefaultUriParser_t1132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultUriParser_t1132)/* instance_size */
	, sizeof (DefaultUriParser_t1132)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.GenericUriParser
#include "System_System_GenericUriParser.h"
// Metadata Definition System.GenericUriParser
extern TypeInfo GenericUriParser_t1134_il2cpp_TypeInfo;
// System.GenericUriParser
#include "System_System_GenericUriParserMethodDeclarations.h"
static const MethodInfo* GenericUriParser_t1134_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference GenericUriParser_t1134_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&UriParser_InitializeAndValidate_m4662_MethodInfo,
	&UriParser_OnRegister_m4663_MethodInfo,
};
static bool GenericUriParser_t1134_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GenericUriParser_t1134_0_0_0;
extern const Il2CppType GenericUriParser_t1134_1_0_0;
struct GenericUriParser_t1134;
const Il2CppTypeDefinitionMetadata GenericUriParser_t1134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UriParser_t1133_0_0_0/* parent */
	, GenericUriParser_t1134_VTable/* vtableMethods */
	, GenericUriParser_t1134_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericUriParser_t1134_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericUriParser"/* name */
	, "System"/* namespaze */
	, GenericUriParser_t1134_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericUriParser_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericUriParser_t1134_0_0_0/* byval_arg */
	, &GenericUriParser_t1134_1_0_0/* this_arg */
	, &GenericUriParser_t1134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericUriParser_t1134)/* instance_size */
	, sizeof (GenericUriParser_t1134)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
// Metadata Definition System.Uri/UriScheme
extern TypeInfo UriScheme_t1135_il2cpp_TypeInfo;
// System.Uri/UriScheme
#include "System_System_Uri_UriSchemeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo UriScheme_t1135_UriScheme__ctor_m4607_ParameterInfos[] = 
{
	{"s", 0, 134218671, 0, &String_t_0_0_0},
	{"d", 1, 134218672, 0, &String_t_0_0_0},
	{"p", 2, 134218673, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri/UriScheme::.ctor(System.String,System.String,System.Int32)
extern const MethodInfo UriScheme__ctor_m4607_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriScheme__ctor_m4607/* method */
	, &UriScheme_t1135_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Int32_t54/* invoker_method */
	, UriScheme_t1135_UriScheme__ctor_m4607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriScheme_t1135_MethodInfos[] =
{
	&UriScheme__ctor_m4607_MethodInfo,
	NULL
};
static const Il2CppMethodReference UriScheme_t1135_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool UriScheme_t1135_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriScheme_t1135_0_0_0;
extern const Il2CppType UriScheme_t1135_1_0_0;
extern TypeInfo Uri_t617_il2cpp_TypeInfo;
extern const Il2CppType Uri_t617_0_0_0;
const Il2CppTypeDefinitionMetadata UriScheme_t1135_DefinitionMetadata = 
{
	&Uri_t617_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, UriScheme_t1135_VTable/* vtableMethods */
	, UriScheme_t1135_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 750/* fieldStart */

};
TypeInfo UriScheme_t1135_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriScheme"/* name */
	, ""/* namespaze */
	, UriScheme_t1135_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriScheme_t1135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriScheme_t1135_0_0_0/* byval_arg */
	, &UriScheme_t1135_1_0_0/* this_arg */
	, &UriScheme_t1135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)UriScheme_t1135_marshal/* marshal_to_native_func */
	, (methodPointerType)UriScheme_t1135_marshal_back/* marshal_from_native_func */
	, (methodPointerType)UriScheme_t1135_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (UriScheme_t1135)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriScheme_t1135)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UriScheme_t1135_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Uri
#include "System_System_Uri.h"
// Metadata Definition System.Uri
// System.Uri
#include "System_System_UriMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri__ctor_m3444_ParameterInfos[] = 
{
	{"uriString", 0, 134218619, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String)
extern const MethodInfo Uri__ctor_m3444_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m3444/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Uri_t617_Uri__ctor_m3444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo Uri_t617_Uri__ctor_m4608_ParameterInfos[] = 
{
	{"serializationInfo", 0, 134218620, 0, &SerializationInfo_t725_0_0_0},
	{"streamingContext", 1, 134218621, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri__ctor_m4608_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m4608/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, Uri_t617_Uri__ctor_m4608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Uri_t617_Uri__ctor_m4609_ParameterInfos[] = 
{
	{"uriString", 0, 134218622, 0, &String_t_0_0_0},
	{"dontEscape", 1, 134218623, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.String,System.Boolean)
extern const MethodInfo Uri__ctor_m4609_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m4609/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_SByte_t73/* invoker_method */
	, Uri_t617_Uri__ctor_m4609_ParameterInfos/* parameters */
	, 71/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t617_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri__ctor_m3446_ParameterInfos[] = 
{
	{"baseUri", 0, 134218624, 0, &Uri_t617_0_0_0},
	{"relativeUri", 1, 134218625, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.ctor(System.Uri,System.String)
extern const MethodInfo Uri__ctor_m3446_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Uri__ctor_m3446/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri__ctor_m3446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::.cctor()
extern const MethodInfo Uri__cctor_m4610_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Uri__cctor_m4610/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo Uri_t617_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_ParameterInfos[] = 
{
	{"info", 0, 134218626, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134218627, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, Uri_t617_Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t617_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_Merge_m4612_ParameterInfos[] = 
{
	{"baseUri", 0, 134218628, 0, &Uri_t617_0_0_0},
	{"relativeUri", 1, 134218629, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Merge(System.Uri,System.String)
extern const MethodInfo Uri_Merge_m4612_MethodInfo = 
{
	"Merge"/* name */
	, (methodPointerType)&Uri_Merge_m4612/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_Merge_m4612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_AbsoluteUri()
extern const MethodInfo Uri_get_AbsoluteUri_m4613_MethodInfo = 
{
	"get_AbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_AbsoluteUri_m4613/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Authority()
extern const MethodInfo Uri_get_Authority_m4614_MethodInfo = 
{
	"get_Authority"/* name */
	, (methodPointerType)&Uri_get_Authority_m4614/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Host()
extern const MethodInfo Uri_get_Host_m4615_MethodInfo = 
{
	"get_Host"/* name */
	, (methodPointerType)&Uri_get_Host_m4615/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsFile()
extern const MethodInfo Uri_get_IsFile_m4616_MethodInfo = 
{
	"get_IsFile"/* name */
	, (methodPointerType)&Uri_get_IsFile_m4616/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsLoopback()
extern const MethodInfo Uri_get_IsLoopback_m4617_MethodInfo = 
{
	"get_IsLoopback"/* name */
	, (methodPointerType)&Uri_get_IsLoopback_m4617/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsUnc()
extern const MethodInfo Uri_get_IsUnc_m4618_MethodInfo = 
{
	"get_IsUnc"/* name */
	, (methodPointerType)&Uri_get_IsUnc_m4618/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::get_Scheme()
extern const MethodInfo Uri_get_Scheme_m4619_MethodInfo = 
{
	"get_Scheme"/* name */
	, (methodPointerType)&Uri_get_Scheme_m4619/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t72 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::get_IsAbsoluteUri()
extern const MethodInfo Uri_get_IsAbsoluteUri_m4620_MethodInfo = 
{
	"get_IsAbsoluteUri"/* name */
	, (methodPointerType)&Uri_get_IsAbsoluteUri_m4620/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_CheckHostName_m4621_ParameterInfos[] = 
{
	{"name", 0, 134218630, 0, &String_t_0_0_0},
};
extern const Il2CppType UriHostNameType_t1138_0_0_0;
extern void* RuntimeInvoker_UriHostNameType_t1138_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriHostNameType System.Uri::CheckHostName(System.String)
extern const MethodInfo Uri_CheckHostName_m4621_MethodInfo = 
{
	"CheckHostName"/* name */
	, (methodPointerType)&Uri_CheckHostName_m4621/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &UriHostNameType_t1138_0_0_0/* return_type */
	, RuntimeInvoker_UriHostNameType_t1138_Object_t/* invoker_method */
	, Uri_t617_Uri_CheckHostName_m4621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsIPv4Address_m4622_ParameterInfos[] = 
{
	{"name", 0, 134218631, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsIPv4Address(System.String)
extern const MethodInfo Uri_IsIPv4Address_m4622_MethodInfo = 
{
	"IsIPv4Address"/* name */
	, (methodPointerType)&Uri_IsIPv4Address_m4622/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_IsIPv4Address_m4622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsDomainAddress_m4623_ParameterInfos[] = 
{
	{"name", 0, 134218632, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsDomainAddress(System.String)
extern const MethodInfo Uri_IsDomainAddress_m4623_MethodInfo = 
{
	"IsDomainAddress"/* name */
	, (methodPointerType)&Uri_IsDomainAddress_m4623/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_IsDomainAddress_m4623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_CheckSchemeName_m4624_ParameterInfos[] = 
{
	{"schemeName", 0, 134218633, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CheckSchemeName(System.String)
extern const MethodInfo Uri_CheckSchemeName_m4624_MethodInfo = 
{
	"CheckSchemeName"/* name */
	, (methodPointerType)&Uri_CheckSchemeName_m4624/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_CheckSchemeName_m4624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsAlpha_m4625_ParameterInfos[] = 
{
	{"c", 0, 134218634, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsAlpha(System.Char)
extern const MethodInfo Uri_IsAlpha_m4625_MethodInfo = 
{
	"IsAlpha"/* name */
	, (methodPointerType)&Uri_IsAlpha_m4625/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int16_t448/* invoker_method */
	, Uri_t617_Uri_IsAlpha_m4625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_Equals_m4626_ParameterInfos[] = 
{
	{"comparant", 0, 134218635, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::Equals(System.Object)
extern const MethodInfo Uri_Equals_m4626_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Uri_Equals_m4626/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_Equals_m4626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t617_0_0_0;
static const ParameterInfo Uri_t617_Uri_InternalEquals_m4627_ParameterInfos[] = 
{
	{"uri", 0, 134218636, 0, &Uri_t617_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::InternalEquals(System.Uri)
extern const MethodInfo Uri_InternalEquals_m4627_MethodInfo = 
{
	"InternalEquals"/* name */
	, (methodPointerType)&Uri_InternalEquals_m4627/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_InternalEquals_m4627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetHashCode()
extern const MethodInfo Uri_GetHashCode_m4628_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Uri_GetHashCode_m4628/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriPartial_t1140_0_0_0;
extern const Il2CppType UriPartial_t1140_0_0_0;
static const ParameterInfo Uri_t617_Uri_GetLeftPart_m4629_ParameterInfos[] = 
{
	{"part", 0, 134218637, 0, &UriPartial_t1140_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetLeftPart(System.UriPartial)
extern const MethodInfo Uri_GetLeftPart_m4629_MethodInfo = 
{
	"GetLeftPart"/* name */
	, (methodPointerType)&Uri_GetLeftPart_m4629/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54/* invoker_method */
	, Uri_t617_Uri_GetLeftPart_m4629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Uri_t617_Uri_FromHex_m4630_ParameterInfos[] = 
{
	{"digit", 0, 134218638, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::FromHex(System.Char)
extern const MethodInfo Uri_FromHex_m4630_MethodInfo = 
{
	"FromHex"/* name */
	, (methodPointerType)&Uri_FromHex_m4630/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Int16_t448/* invoker_method */
	, Uri_t617_Uri_FromHex_m4630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Uri_t617_Uri_HexEscape_m4631_ParameterInfos[] = 
{
	{"character", 0, 134218639, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::HexEscape(System.Char)
extern const MethodInfo Uri_HexEscape_m4631_MethodInfo = 
{
	"HexEscape"/* name */
	, (methodPointerType)&Uri_HexEscape_m4631/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int16_t448/* invoker_method */
	, Uri_t617_Uri_HexEscape_m4631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t369_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsHexDigit_m4632_ParameterInfos[] = 
{
	{"digit", 0, 134218640, 0, &Char_t369_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Int16_t448 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexDigit(System.Char)
extern const MethodInfo Uri_IsHexDigit_m4632_MethodInfo = 
{
	"IsHexDigit"/* name */
	, (methodPointerType)&Uri_IsHexDigit_m4632/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Int16_t448/* invoker_method */
	, Uri_t617_Uri_IsHexDigit_m4632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsHexEncoding_m4633_ParameterInfos[] = 
{
	{"pattern", 0, 134218641, 0, &String_t_0_0_0},
	{"index", 1, 134218642, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsHexEncoding(System.String,System.Int32)
extern const MethodInfo Uri_IsHexEncoding_m4633_MethodInfo = 
{
	"IsHexEncoding"/* name */
	, (methodPointerType)&Uri_IsHexEncoding_m4633/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Int32_t54/* invoker_method */
	, Uri_t617_Uri_IsHexEncoding_m4633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_0;
static const ParameterInfo Uri_t617_Uri_AppendQueryAndFragment_m4634_ParameterInfos[] = 
{
	{"result", 0, 134218643, 0, &String_t_1_0_0},
};
extern void* RuntimeInvoker_Void_t71_StringU26_t882 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::AppendQueryAndFragment(System.String&)
extern const MethodInfo Uri_AppendQueryAndFragment_m4634_MethodInfo = 
{
	"AppendQueryAndFragment"/* name */
	, (methodPointerType)&Uri_AppendQueryAndFragment_m4634/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_StringU26_t882/* invoker_method */
	, Uri_t617_Uri_AppendQueryAndFragment_m4634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ToString()
extern const MethodInfo Uri_ToString_m4635_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Uri_ToString_m4635/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_EscapeString_m4636_ParameterInfos[] = 
{
	{"str", 0, 134218644, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String)
extern const MethodInfo Uri_EscapeString_m4636_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m4636/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_EscapeString_m4636_ParameterInfos/* parameters */
	, 72/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Uri_t617_Uri_EscapeString_m4637_ParameterInfos[] = 
{
	{"str", 0, 134218645, 0, &String_t_0_0_0},
	{"escapeReserved", 1, 134218646, 0, &Boolean_t72_0_0_0},
	{"escapeHex", 2, 134218647, 0, &Boolean_t72_0_0_0},
	{"escapeBrackets", 3, 134218648, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::EscapeString(System.String,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Uri_EscapeString_m4637_MethodInfo = 
{
	"EscapeString"/* name */
	, (methodPointerType)&Uri_EscapeString_m4637/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73_SByte_t73_SByte_t73/* invoker_method */
	, Uri_t617_Uri_EscapeString_m4637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1139_0_0_0;
extern const Il2CppType UriKind_t1139_0_0_0;
static const ParameterInfo Uri_t617_Uri_ParseUri_m4638_ParameterInfos[] = 
{
	{"kind", 0, 134218649, 0, &UriKind_t1139_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseUri(System.UriKind)
extern const MethodInfo Uri_ParseUri_m4638_MethodInfo = 
{
	"ParseUri"/* name */
	, (methodPointerType)&Uri_ParseUri_m4638/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, Uri_t617_Uri_ParseUri_m4638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_Unescape_m4639_ParameterInfos[] = 
{
	{"str", 0, 134218650, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String)
extern const MethodInfo Uri_Unescape_m4639_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m4639/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_Unescape_m4639_ParameterInfos/* parameters */
	, 73/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Uri_t617_Uri_Unescape_m4640_ParameterInfos[] = 
{
	{"str", 0, 134218651, 0, &String_t_0_0_0},
	{"excludeSpecial", 1, 134218652, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Unescape(System.String,System.Boolean)
extern const MethodInfo Uri_Unescape_m4640_MethodInfo = 
{
	"Unescape"/* name */
	, (methodPointerType)&Uri_Unescape_m4640/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, Uri_t617_Uri_Unescape_m4640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_ParseAsWindowsUNC_m4641_ParameterInfos[] = 
{
	{"uriString", 0, 134218653, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsWindowsUNC(System.String)
extern const MethodInfo Uri_ParseAsWindowsUNC_m4641_MethodInfo = 
{
	"ParseAsWindowsUNC"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsUNC_m4641/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Uri_t617_Uri_ParseAsWindowsUNC_m4641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_ParseAsWindowsAbsoluteFilePath_m4642_ParameterInfos[] = 
{
	{"uriString", 0, 134218654, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseAsWindowsAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsWindowsAbsoluteFilePath_m4642_MethodInfo = 
{
	"ParseAsWindowsAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsWindowsAbsoluteFilePath_m4642/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_ParseAsWindowsAbsoluteFilePath_m4642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_ParseAsUnixAbsoluteFilePath_m4643_ParameterInfos[] = 
{
	{"uriString", 0, 134218655, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::ParseAsUnixAbsoluteFilePath(System.String)
extern const MethodInfo Uri_ParseAsUnixAbsoluteFilePath_m4643_MethodInfo = 
{
	"ParseAsUnixAbsoluteFilePath"/* name */
	, (methodPointerType)&Uri_ParseAsUnixAbsoluteFilePath_m4643/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, Uri_t617_Uri_ParseAsUnixAbsoluteFilePath_m4643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1139_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_Parse_m4644_ParameterInfos[] = 
{
	{"kind", 0, 134218656, 0, &UriKind_t1139_0_0_0},
	{"uriString", 1, 134218657, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::Parse(System.UriKind,System.String)
extern const MethodInfo Uri_Parse_m4644_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&Uri_Parse_m4644/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54_Object_t/* invoker_method */
	, Uri_t617_Uri_Parse_m4644_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UriKind_t1139_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_ParseNoExceptions_m4645_ParameterInfos[] = 
{
	{"kind", 0, 134218658, 0, &UriKind_t1139_0_0_0},
	{"uriString", 1, 134218659, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::ParseNoExceptions(System.UriKind,System.String)
extern const MethodInfo Uri_ParseNoExceptions_m4645_MethodInfo = 
{
	"ParseNoExceptions"/* name */
	, (methodPointerType)&Uri_ParseNoExceptions_m4645/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t54_Object_t/* invoker_method */
	, Uri_t617_Uri_ParseNoExceptions_m4645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_CompactEscaped_m4646_ParameterInfos[] = 
{
	{"scheme", 0, 134218660, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::CompactEscaped(System.String)
extern const MethodInfo Uri_CompactEscaped_m4646_MethodInfo = 
{
	"CompactEscaped"/* name */
	, (methodPointerType)&Uri_CompactEscaped_m4646/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_CompactEscaped_m4646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t72_0_0_0;
static const ParameterInfo Uri_t617_Uri_Reduce_m4647_ParameterInfos[] = 
{
	{"path", 0, 134218661, 0, &String_t_0_0_0},
	{"compact_escaped", 1, 134218662, 0, &Boolean_t72_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t73 (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::Reduce(System.String,System.Boolean)
extern const MethodInfo Uri_Reduce_m4647_MethodInfo = 
{
	"Reduce"/* name */
	, (methodPointerType)&Uri_Reduce_m4647/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t73/* invoker_method */
	, Uri_t617_Uri_Reduce_m4647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_1_0_0;
extern const Il2CppType Char_t369_1_0_2;
extern const Il2CppType Char_t369_1_0_0;
static const ParameterInfo Uri_t617_Uri_HexUnescapeMultiByte_m4648_ParameterInfos[] = 
{
	{"pattern", 0, 134218663, 0, &String_t_0_0_0},
	{"index", 1, 134218664, 0, &Int32_t54_1_0_0},
	{"surrogate", 2, 134218665, 0, &Char_t369_1_0_2},
};
extern void* RuntimeInvoker_Char_t369_Object_t_Int32U26_t449_CharU26_t1212 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Uri::HexUnescapeMultiByte(System.String,System.Int32&,System.Char&)
extern const MethodInfo Uri_HexUnescapeMultiByte_m4648_MethodInfo = 
{
	"HexUnescapeMultiByte"/* name */
	, (methodPointerType)&Uri_HexUnescapeMultiByte_m4648/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Char_t369_0_0_0/* return_type */
	, RuntimeInvoker_Char_t369_Object_t_Int32U26_t449_CharU26_t1212/* invoker_method */
	, Uri_t617_Uri_HexUnescapeMultiByte_m4648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_GetSchemeDelimiter_m4649_ParameterInfos[] = 
{
	{"scheme", 0, 134218666, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetSchemeDelimiter(System.String)
extern const MethodInfo Uri_GetSchemeDelimiter_m4649_MethodInfo = 
{
	"GetSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetSchemeDelimiter_m4649/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_GetSchemeDelimiter_m4649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_GetDefaultPort_m4650_ParameterInfos[] = 
{
	{"scheme", 0, 134218667, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t54_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Uri::GetDefaultPort(System.String)
extern const MethodInfo Uri_GetDefaultPort_m4650_MethodInfo = 
{
	"GetDefaultPort"/* name */
	, (methodPointerType)&Uri_GetDefaultPort_m4650/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54_Object_t/* invoker_method */
	, Uri_t617_Uri_GetDefaultPort_m4650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Uri::GetOpaqueWiseSchemeDelimiter()
extern const MethodInfo Uri_GetOpaqueWiseSchemeDelimiter_m4651_MethodInfo = 
{
	"GetOpaqueWiseSchemeDelimiter"/* name */
	, (methodPointerType)&Uri_GetOpaqueWiseSchemeDelimiter_m4651/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Uri_t617_Uri_IsPredefinedScheme_m4652_ParameterInfos[] = 
{
	{"scheme", 0, 134218668, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::IsPredefinedScheme(System.String)
extern const MethodInfo Uri_IsPredefinedScheme_m4652_MethodInfo = 
{
	"IsPredefinedScheme"/* name */
	, (methodPointerType)&Uri_IsPredefinedScheme_m4652/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, Uri_t617_Uri_IsPredefinedScheme_m4652_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.Uri::get_Parser()
extern const MethodInfo Uri_get_Parser_m4653_MethodInfo = 
{
	"get_Parser"/* name */
	, (methodPointerType)&Uri_get_Parser_m4653/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1133_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Uri::EnsureAbsoluteUri()
extern const MethodInfo Uri_EnsureAbsoluteUri_m4654_MethodInfo = 
{
	"EnsureAbsoluteUri"/* name */
	, (methodPointerType)&Uri_EnsureAbsoluteUri_m4654/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t617_0_0_0;
extern const Il2CppType Uri_t617_0_0_0;
static const ParameterInfo Uri_t617_Uri_op_Equality_m4655_ParameterInfos[] = 
{
	{"u1", 0, 134218669, 0, &Uri_t617_0_0_0},
	{"u2", 1, 134218670, 0, &Uri_t617_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Uri::op_Equality(System.Uri,System.Uri)
extern const MethodInfo Uri_op_Equality_m4655_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Uri_op_Equality_m4655/* method */
	, &Uri_t617_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t/* invoker_method */
	, Uri_t617_Uri_op_Equality_m4655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Uri_t617_MethodInfos[] =
{
	&Uri__ctor_m3444_MethodInfo,
	&Uri__ctor_m4608_MethodInfo,
	&Uri__ctor_m4609_MethodInfo,
	&Uri__ctor_m3446_MethodInfo,
	&Uri__cctor_m4610_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_MethodInfo,
	&Uri_Merge_m4612_MethodInfo,
	&Uri_get_AbsoluteUri_m4613_MethodInfo,
	&Uri_get_Authority_m4614_MethodInfo,
	&Uri_get_Host_m4615_MethodInfo,
	&Uri_get_IsFile_m4616_MethodInfo,
	&Uri_get_IsLoopback_m4617_MethodInfo,
	&Uri_get_IsUnc_m4618_MethodInfo,
	&Uri_get_Scheme_m4619_MethodInfo,
	&Uri_get_IsAbsoluteUri_m4620_MethodInfo,
	&Uri_CheckHostName_m4621_MethodInfo,
	&Uri_IsIPv4Address_m4622_MethodInfo,
	&Uri_IsDomainAddress_m4623_MethodInfo,
	&Uri_CheckSchemeName_m4624_MethodInfo,
	&Uri_IsAlpha_m4625_MethodInfo,
	&Uri_Equals_m4626_MethodInfo,
	&Uri_InternalEquals_m4627_MethodInfo,
	&Uri_GetHashCode_m4628_MethodInfo,
	&Uri_GetLeftPart_m4629_MethodInfo,
	&Uri_FromHex_m4630_MethodInfo,
	&Uri_HexEscape_m4631_MethodInfo,
	&Uri_IsHexDigit_m4632_MethodInfo,
	&Uri_IsHexEncoding_m4633_MethodInfo,
	&Uri_AppendQueryAndFragment_m4634_MethodInfo,
	&Uri_ToString_m4635_MethodInfo,
	&Uri_EscapeString_m4636_MethodInfo,
	&Uri_EscapeString_m4637_MethodInfo,
	&Uri_ParseUri_m4638_MethodInfo,
	&Uri_Unescape_m4639_MethodInfo,
	&Uri_Unescape_m4640_MethodInfo,
	&Uri_ParseAsWindowsUNC_m4641_MethodInfo,
	&Uri_ParseAsWindowsAbsoluteFilePath_m4642_MethodInfo,
	&Uri_ParseAsUnixAbsoluteFilePath_m4643_MethodInfo,
	&Uri_Parse_m4644_MethodInfo,
	&Uri_ParseNoExceptions_m4645_MethodInfo,
	&Uri_CompactEscaped_m4646_MethodInfo,
	&Uri_Reduce_m4647_MethodInfo,
	&Uri_HexUnescapeMultiByte_m4648_MethodInfo,
	&Uri_GetSchemeDelimiter_m4649_MethodInfo,
	&Uri_GetDefaultPort_m4650_MethodInfo,
	&Uri_GetOpaqueWiseSchemeDelimiter_m4651_MethodInfo,
	&Uri_IsPredefinedScheme_m4652_MethodInfo,
	&Uri_get_Parser_m4653_MethodInfo,
	&Uri_EnsureAbsoluteUri_m4654_MethodInfo,
	&Uri_op_Equality_m4655_MethodInfo,
	NULL
};
extern const MethodInfo Uri_get_AbsoluteUri_m4613_MethodInfo;
static const PropertyInfo Uri_t617____AbsoluteUri_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "AbsoluteUri"/* name */
	, &Uri_get_AbsoluteUri_m4613_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Authority_m4614_MethodInfo;
static const PropertyInfo Uri_t617____Authority_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "Authority"/* name */
	, &Uri_get_Authority_m4614_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Host_m4615_MethodInfo;
static const PropertyInfo Uri_t617____Host_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "Host"/* name */
	, &Uri_get_Host_m4615_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsFile_m4616_MethodInfo;
static const PropertyInfo Uri_t617____IsFile_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "IsFile"/* name */
	, &Uri_get_IsFile_m4616_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsLoopback_m4617_MethodInfo;
static const PropertyInfo Uri_t617____IsLoopback_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "IsLoopback"/* name */
	, &Uri_get_IsLoopback_m4617_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsUnc_m4618_MethodInfo;
static const PropertyInfo Uri_t617____IsUnc_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "IsUnc"/* name */
	, &Uri_get_IsUnc_m4618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Scheme_m4619_MethodInfo;
static const PropertyInfo Uri_t617____Scheme_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "Scheme"/* name */
	, &Uri_get_Scheme_m4619_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_IsAbsoluteUri_m4620_MethodInfo;
static const PropertyInfo Uri_t617____IsAbsoluteUri_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "IsAbsoluteUri"/* name */
	, &Uri_get_IsAbsoluteUri_m4620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Uri_get_Parser_m4653_MethodInfo;
static const PropertyInfo Uri_t617____Parser_PropertyInfo = 
{
	&Uri_t617_il2cpp_TypeInfo/* parent */
	, "Parser"/* name */
	, &Uri_get_Parser_m4653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Uri_t617_PropertyInfos[] =
{
	&Uri_t617____AbsoluteUri_PropertyInfo,
	&Uri_t617____Authority_PropertyInfo,
	&Uri_t617____Host_PropertyInfo,
	&Uri_t617____IsFile_PropertyInfo,
	&Uri_t617____IsLoopback_PropertyInfo,
	&Uri_t617____IsUnc_PropertyInfo,
	&Uri_t617____Scheme_PropertyInfo,
	&Uri_t617____IsAbsoluteUri_PropertyInfo,
	&Uri_t617____Parser_PropertyInfo,
	NULL
};
static const Il2CppType* Uri_t617_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UriScheme_t1135_0_0_0,
};
extern const MethodInfo Uri_Equals_m4626_MethodInfo;
extern const MethodInfo Uri_GetHashCode_m4628_MethodInfo;
extern const MethodInfo Uri_ToString_m4635_MethodInfo;
extern const MethodInfo Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_MethodInfo;
extern const MethodInfo Uri_Unescape_m4639_MethodInfo;
static const Il2CppMethodReference Uri_t617_VTable[] =
{
	&Uri_Equals_m4626_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Uri_GetHashCode_m4628_MethodInfo,
	&Uri_ToString_m4635_MethodInfo,
	&Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m4611_MethodInfo,
	&Uri_Unescape_m4639_MethodInfo,
};
static bool Uri_t617_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Uri_t617_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
static Il2CppInterfaceOffsetPair Uri_t617_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Uri_t617_1_0_0;
struct Uri_t617;
const Il2CppTypeDefinitionMetadata Uri_t617_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Uri_t617_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Uri_t617_InterfacesTypeInfos/* implementedInterfaces */
	, Uri_t617_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Uri_t617_VTable/* vtableMethods */
	, Uri_t617_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 753/* fieldStart */

};
TypeInfo Uri_t617_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Uri"/* name */
	, "System"/* namespaze */
	, Uri_t617_MethodInfos/* methods */
	, Uri_t617_PropertyInfos/* properties */
	, NULL/* events */
	, &Uri_t617_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 65/* custom_attributes_cache */
	, &Uri_t617_0_0_0/* byval_arg */
	, &Uri_t617_1_0_0/* this_arg */
	, &Uri_t617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Uri_t617)/* instance_size */
	, sizeof (Uri_t617)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Uri_t617_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 9/* property_count */
	, 38/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UriFormatException
#include "System_System_UriFormatException.h"
// Metadata Definition System.UriFormatException
extern TypeInfo UriFormatException_t1137_il2cpp_TypeInfo;
// System.UriFormatException
#include "System_System_UriFormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor()
extern const MethodInfo UriFormatException__ctor_m4656_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m4656/* method */
	, &UriFormatException_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriFormatException_t1137_UriFormatException__ctor_m4657_ParameterInfos[] = 
{
	{"message", 0, 134218674, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.String)
extern const MethodInfo UriFormatException__ctor_m4657_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m4657/* method */
	, &UriFormatException_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UriFormatException_t1137_UriFormatException__ctor_m4657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UriFormatException_t1137_UriFormatException__ctor_m4658_ParameterInfos[] = 
{
	{"info", 0, 134218675, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134218676, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException__ctor_m4658_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriFormatException__ctor_m4658/* method */
	, &UriFormatException_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UriFormatException_t1137_UriFormatException__ctor_m4658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t725_0_0_0;
extern const Il2CppType StreamingContext_t726_0_0_0;
static const ParameterInfo UriFormatException_t1137_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_ParameterInfos[] = 
{
	{"info", 0, 134218677, 0, &SerializationInfo_t725_0_0_0},
	{"context", 1, 134218678, 0, &StreamingContext_t726_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriFormatException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_MethodInfo = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData"/* name */
	, (methodPointerType)&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659/* method */
	, &UriFormatException_t1137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_StreamingContext_t726/* invoker_method */
	, UriFormatException_t1137_UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriFormatException_t1137_MethodInfos[] =
{
	&UriFormatException__ctor_m4656_MethodInfo,
	&UriFormatException__ctor_m4657_MethodInfo,
	&UriFormatException__ctor_m4658_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m3652_MethodInfo;
extern const MethodInfo UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m3654_MethodInfo;
extern const MethodInfo Exception_get_Message_m3655_MethodInfo;
extern const MethodInfo Exception_get_Source_m3656_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m3657_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m3653_MethodInfo;
extern const MethodInfo Exception_GetType_m3658_MethodInfo;
static const Il2CppMethodReference UriFormatException_t1137_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Exception_ToString_m3652_MethodInfo,
	&UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m4659_MethodInfo,
	&Exception_get_InnerException_m3654_MethodInfo,
	&Exception_get_Message_m3655_MethodInfo,
	&Exception_get_Source_m3656_MethodInfo,
	&Exception_get_StackTrace_m3657_MethodInfo,
	&Exception_GetObjectData_m3653_MethodInfo,
	&Exception_GetType_m3658_MethodInfo,
};
static bool UriFormatException_t1137_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UriFormatException_t1137_InterfacesTypeInfos[] = 
{
	&ISerializable_t428_0_0_0,
};
extern const Il2CppType _Exception_t824_0_0_0;
static Il2CppInterfaceOffsetPair UriFormatException_t1137_InterfacesOffsets[] = 
{
	{ &ISerializable_t428_0_0_0, 4},
	{ &_Exception_t824_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriFormatException_t1137_0_0_0;
extern const Il2CppType UriFormatException_t1137_1_0_0;
extern const Il2CppType FormatException_t743_0_0_0;
struct UriFormatException_t1137;
const Il2CppTypeDefinitionMetadata UriFormatException_t1137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UriFormatException_t1137_InterfacesTypeInfos/* implementedInterfaces */
	, UriFormatException_t1137_InterfacesOffsets/* interfaceOffsets */
	, &FormatException_t743_0_0_0/* parent */
	, UriFormatException_t1137_VTable/* vtableMethods */
	, UriFormatException_t1137_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriFormatException_t1137_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriFormatException"/* name */
	, "System"/* namespaze */
	, UriFormatException_t1137_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriFormatException_t1137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriFormatException_t1137_0_0_0/* byval_arg */
	, &UriFormatException_t1137_1_0_0/* this_arg */
	, &UriFormatException_t1137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriFormatException_t1137)/* instance_size */
	, sizeof (UriFormatException_t1137)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
// Metadata Definition System.UriHostNameType
extern TypeInfo UriHostNameType_t1138_il2cpp_TypeInfo;
// System.UriHostNameType
#include "System_System_UriHostNameTypeMethodDeclarations.h"
static const MethodInfo* UriHostNameType_t1138_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriHostNameType_t1138_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UriHostNameType_t1138_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriHostNameType_t1138_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriHostNameType_t1138_1_0_0;
const Il2CppTypeDefinitionMetadata UriHostNameType_t1138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriHostNameType_t1138_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UriHostNameType_t1138_VTable/* vtableMethods */
	, UriHostNameType_t1138_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 791/* fieldStart */

};
TypeInfo UriHostNameType_t1138_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriHostNameType"/* name */
	, "System"/* namespaze */
	, UriHostNameType_t1138_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriHostNameType_t1138_0_0_0/* byval_arg */
	, &UriHostNameType_t1138_1_0_0/* this_arg */
	, &UriHostNameType_t1138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriHostNameType_t1138)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriHostNameType_t1138)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriKind
#include "System_System_UriKind.h"
// Metadata Definition System.UriKind
extern TypeInfo UriKind_t1139_il2cpp_TypeInfo;
// System.UriKind
#include "System_System_UriKindMethodDeclarations.h"
static const MethodInfo* UriKind_t1139_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriKind_t1139_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UriKind_t1139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriKind_t1139_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriKind_t1139_1_0_0;
const Il2CppTypeDefinitionMetadata UriKind_t1139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriKind_t1139_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UriKind_t1139_VTable/* vtableMethods */
	, UriKind_t1139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 797/* fieldStart */

};
TypeInfo UriKind_t1139_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriKind"/* name */
	, "System"/* namespaze */
	, UriKind_t1139_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriKind_t1139_0_0_0/* byval_arg */
	, &UriKind_t1139_1_0_0/* this_arg */
	, &UriKind_t1139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriKind_t1139)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriKind_t1139)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriParser
#include "System_System_UriParser.h"
// Metadata Definition System.UriParser
extern TypeInfo UriParser_t1133_il2cpp_TypeInfo;
// System.UriParser
#include "System_System_UriParserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.ctor()
extern const MethodInfo UriParser__ctor_m4660_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UriParser__ctor_m4660/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::.cctor()
extern const MethodInfo UriParser__cctor_m4661_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&UriParser__cctor_m4661/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Uri_t617_0_0_0;
extern const Il2CppType UriFormatException_t1137_1_0_2;
static const ParameterInfo UriParser_t1133_UriParser_InitializeAndValidate_m4662_ParameterInfos[] = 
{
	{"uri", 0, 134218679, 0, &Uri_t617_0_0_0},
	{"parsingError", 1, 134218680, 0, &UriFormatException_t1137_1_0_2},
};
extern void* RuntimeInvoker_Void_t71_Object_t_UriFormatExceptionU26_t1213 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern const MethodInfo UriParser_InitializeAndValidate_m4662_MethodInfo = 
{
	"InitializeAndValidate"/* name */
	, (methodPointerType)&UriParser_InitializeAndValidate_m4662/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_UriFormatExceptionU26_t1213/* invoker_method */
	, UriParser_t1133_UriParser_InitializeAndValidate_m4662_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 453/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo UriParser_t1133_UriParser_OnRegister_m4663_ParameterInfos[] = 
{
	{"schemeName", 0, 134218681, 0, &String_t_0_0_0},
	{"defaultPort", 1, 134218682, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
extern const MethodInfo UriParser_OnRegister_m4663_MethodInfo = 
{
	"OnRegister"/* name */
	, (methodPointerType)&UriParser_OnRegister_m4663/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Int32_t54/* invoker_method */
	, UriParser_t1133_UriParser_OnRegister_m4663_ParameterInfos/* parameters */
	, 74/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1133_UriParser_set_SchemeName_m4664_ParameterInfos[] = 
{
	{"value", 0, 134218683, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_SchemeName(System.String)
extern const MethodInfo UriParser_set_SchemeName_m4664_MethodInfo = 
{
	"set_SchemeName"/* name */
	, (methodPointerType)&UriParser_set_SchemeName_m4664/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t/* invoker_method */
	, UriParser_t1133_UriParser_set_SchemeName_m4664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.UriParser::get_DefaultPort()
extern const MethodInfo UriParser_get_DefaultPort_m4665_MethodInfo = 
{
	"get_DefaultPort"/* name */
	, (methodPointerType)&UriParser_get_DefaultPort_m4665/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t54_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t54/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo UriParser_t1133_UriParser_set_DefaultPort_m4666_ParameterInfos[] = 
{
	{"value", 0, 134218684, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::set_DefaultPort(System.Int32)
extern const MethodInfo UriParser_set_DefaultPort_m4666_MethodInfo = 
{
	"set_DefaultPort"/* name */
	, (methodPointerType)&UriParser_set_DefaultPort_m4666/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Int32_t54/* invoker_method */
	, UriParser_t1133_UriParser_set_DefaultPort_m4666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t71 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::CreateDefaults()
extern const MethodInfo UriParser_CreateDefaults_m4667_MethodInfo = 
{
	"CreateDefaults"/* name */
	, (methodPointerType)&UriParser_CreateDefaults_m4667/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Hashtable_t975_0_0_0;
extern const Il2CppType UriParser_t1133_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t54_0_0_0;
static const ParameterInfo UriParser_t1133_UriParser_InternalRegister_m4668_ParameterInfos[] = 
{
	{"table", 0, 134218685, 0, &Hashtable_t975_0_0_0},
	{"uriParser", 1, 134218686, 0, &UriParser_t1133_0_0_0},
	{"schemeName", 2, 134218687, 0, &String_t_0_0_0},
	{"defaultPort", 3, 134218688, 0, &Int32_t54_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
extern const MethodInfo UriParser_InternalRegister_m4668_MethodInfo = 
{
	"InternalRegister"/* name */
	, (methodPointerType)&UriParser_InternalRegister_m4668/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_Object_t_Object_t_Int32_t54/* invoker_method */
	, UriParser_t1133_UriParser_InternalRegister_m4668_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UriParser_t1133_UriParser_GetParser_m4669_ParameterInfos[] = 
{
	{"schemeName", 0, 134218689, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.UriParser System.UriParser::GetParser(System.String)
extern const MethodInfo UriParser_GetParser_m4669_MethodInfo = 
{
	"GetParser"/* name */
	, (methodPointerType)&UriParser_GetParser_m4669/* method */
	, &UriParser_t1133_il2cpp_TypeInfo/* declaring_type */
	, &UriParser_t1133_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UriParser_t1133_UriParser_GetParser_m4669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UriParser_t1133_MethodInfos[] =
{
	&UriParser__ctor_m4660_MethodInfo,
	&UriParser__cctor_m4661_MethodInfo,
	&UriParser_InitializeAndValidate_m4662_MethodInfo,
	&UriParser_OnRegister_m4663_MethodInfo,
	&UriParser_set_SchemeName_m4664_MethodInfo,
	&UriParser_get_DefaultPort_m4665_MethodInfo,
	&UriParser_set_DefaultPort_m4666_MethodInfo,
	&UriParser_CreateDefaults_m4667_MethodInfo,
	&UriParser_InternalRegister_m4668_MethodInfo,
	&UriParser_GetParser_m4669_MethodInfo,
	NULL
};
extern const MethodInfo UriParser_set_SchemeName_m4664_MethodInfo;
static const PropertyInfo UriParser_t1133____SchemeName_PropertyInfo = 
{
	&UriParser_t1133_il2cpp_TypeInfo/* parent */
	, "SchemeName"/* name */
	, NULL/* get */
	, &UriParser_set_SchemeName_m4664_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UriParser_get_DefaultPort_m4665_MethodInfo;
extern const MethodInfo UriParser_set_DefaultPort_m4666_MethodInfo;
static const PropertyInfo UriParser_t1133____DefaultPort_PropertyInfo = 
{
	&UriParser_t1133_il2cpp_TypeInfo/* parent */
	, "DefaultPort"/* name */
	, &UriParser_get_DefaultPort_m4665_MethodInfo/* get */
	, &UriParser_set_DefaultPort_m4666_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UriParser_t1133_PropertyInfos[] =
{
	&UriParser_t1133____SchemeName_PropertyInfo,
	&UriParser_t1133____DefaultPort_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UriParser_t1133_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&UriParser_InitializeAndValidate_m4662_MethodInfo,
	&UriParser_OnRegister_m4663_MethodInfo,
};
static bool UriParser_t1133_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriParser_t1133_1_0_0;
struct UriParser_t1133;
const Il2CppTypeDefinitionMetadata UriParser_t1133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UriParser_t1133_VTable/* vtableMethods */
	, UriParser_t1133_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 801/* fieldStart */

};
TypeInfo UriParser_t1133_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriParser"/* name */
	, "System"/* namespaze */
	, UriParser_t1133_MethodInfos/* methods */
	, UriParser_t1133_PropertyInfos/* properties */
	, NULL/* events */
	, &UriParser_t1133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriParser_t1133_0_0_0/* byval_arg */
	, &UriParser_t1133_1_0_0/* this_arg */
	, &UriParser_t1133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriParser_t1133)/* instance_size */
	, sizeof (UriParser_t1133)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UriParser_t1133_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UriPartial
#include "System_System_UriPartial.h"
// Metadata Definition System.UriPartial
extern TypeInfo UriPartial_t1140_il2cpp_TypeInfo;
// System.UriPartial
#include "System_System_UriPartialMethodDeclarations.h"
static const MethodInfo* UriPartial_t1140_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriPartial_t1140_VTable[] =
{
	&Enum_Equals_m222_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Enum_GetHashCode_m223_MethodInfo,
	&Enum_ToString_m224_MethodInfo,
	&Enum_ToString_m225_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m226_MethodInfo,
	&Enum_System_IConvertible_ToByte_m227_MethodInfo,
	&Enum_System_IConvertible_ToChar_m228_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m229_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m230_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m231_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m232_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m233_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m234_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m235_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m236_MethodInfo,
	&Enum_ToString_m237_MethodInfo,
	&Enum_System_IConvertible_ToType_m238_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m239_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m240_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m241_MethodInfo,
	&Enum_CompareTo_m242_MethodInfo,
	&Enum_GetTypeCode_m243_MethodInfo,
};
static bool UriPartial_t1140_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UriPartial_t1140_InterfacesOffsets[] = 
{
	{ &IFormattable_t74_0_0_0, 4},
	{ &IConvertible_t75_0_0_0, 5},
	{ &IComparable_t76_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriPartial_t1140_1_0_0;
const Il2CppTypeDefinitionMetadata UriPartial_t1140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UriPartial_t1140_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t77_0_0_0/* parent */
	, UriPartial_t1140_VTable/* vtableMethods */
	, UriPartial_t1140_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 807/* fieldStart */

};
TypeInfo UriPartial_t1140_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriPartial"/* name */
	, "System"/* namespaze */
	, UriPartial_t1140_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriPartial_t1140_0_0_0/* byval_arg */
	, &UriPartial_t1140_1_0_0/* this_arg */
	, &UriPartial_t1140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriPartial_t1140)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UriPartial_t1140)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UriTypeConverter
#include "System_System_UriTypeConverter.h"
// Metadata Definition System.UriTypeConverter
extern TypeInfo UriTypeConverter_t1141_il2cpp_TypeInfo;
// System.UriTypeConverter
#include "System_System_UriTypeConverterMethodDeclarations.h"
static const MethodInfo* UriTypeConverter_t1141_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UriTypeConverter_t1141_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool UriTypeConverter_t1141_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType UriTypeConverter_t1141_0_0_0;
extern const Il2CppType UriTypeConverter_t1141_1_0_0;
extern const Il2CppType TypeConverter_t990_0_0_0;
struct UriTypeConverter_t1141;
const Il2CppTypeDefinitionMetadata UriTypeConverter_t1141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeConverter_t990_0_0_0/* parent */
	, UriTypeConverter_t1141_VTable/* vtableMethods */
	, UriTypeConverter_t1141_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UriTypeConverter_t1141_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "UriTypeConverter"/* name */
	, "System"/* namespaze */
	, UriTypeConverter_t1141_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UriTypeConverter_t1141_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UriTypeConverter_t1141_0_0_0/* byval_arg */
	, &UriTypeConverter_t1141_1_0_0/* this_arg */
	, &UriTypeConverter_t1141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UriTypeConverter_t1141)/* instance_size */
	, sizeof (UriTypeConverter_t1141)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallba.h"
// Metadata Definition System.Net.Security.RemoteCertificateValidationCallback
extern TypeInfo RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo;
// System.Net.Security.RemoteCertificateValidationCallback
#include "System_System_Net_Security_RemoteCertificateValidationCallbaMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback__ctor_m4670_ParameterInfos[] = 
{
	{"object", 0, 134218690, 0, &Object_t_0_0_0},
	{"method", 1, 134218691, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Net.Security.RemoteCertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo RemoteCertificateValidationCallback__ctor_m4670_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback__ctor_m4670/* method */
	, &RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback__ctor_m4670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType X509Chain_t1048_0_0_0;
extern const Il2CppType X509Chain_t1048_0_0_0;
extern const Il2CppType SslPolicyErrors_t993_0_0_0;
extern const Il2CppType SslPolicyErrors_t993_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_Invoke_m4671_ParameterInfos[] = 
{
	{"sender", 0, 134218692, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218693, 0, &X509Certificate_t1037_0_0_0},
	{"chain", 2, 134218694, 0, &X509Chain_t1048_0_0_0},
	{"sslPolicyErrors", 3, 134218695, 0, &SslPolicyErrors_t993_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Int32_t54 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::Invoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m4671_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_Invoke_m4671/* method */
	, &RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t_Object_t_Object_t_Int32_t54/* invoker_method */
	, RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_Invoke_m4671_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType X509Certificate_t1037_0_0_0;
extern const Il2CppType X509Chain_t1048_0_0_0;
extern const Il2CppType SslPolicyErrors_t993_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_BeginInvoke_m4672_ParameterInfos[] = 
{
	{"sender", 0, 134218696, 0, &Object_t_0_0_0},
	{"certificate", 1, 134218697, 0, &X509Certificate_t1037_0_0_0},
	{"chain", 2, 134218698, 0, &X509Chain_t1048_0_0_0},
	{"sslPolicyErrors", 3, 134218699, 0, &SslPolicyErrors_t993_0_0_0},
	{"callback", 4, 134218700, 0, &AsyncCallback_t214_0_0_0},
	{"object", 5, 134218701, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Net.Security.RemoteCertificateValidationCallback::BeginInvoke(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors,System.AsyncCallback,System.Object)
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m4672_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_BeginInvoke_m4672/* method */
	, &RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t54_Object_t_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_BeginInvoke_m4672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_EndInvoke_m4673_ParameterInfos[] = 
{
	{"result", 0, 134218702, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t72_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Net.Security.RemoteCertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m4673_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&RemoteCertificateValidationCallback_EndInvoke_m4673/* method */
	, &RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t72_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t72_Object_t/* invoker_method */
	, RemoteCertificateValidationCallback_t1002_RemoteCertificateValidationCallback_EndInvoke_m4673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RemoteCertificateValidationCallback_t1002_MethodInfos[] =
{
	&RemoteCertificateValidationCallback__ctor_m4670_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m4671_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m4672_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m4673_MethodInfo,
	NULL
};
extern const MethodInfo RemoteCertificateValidationCallback_Invoke_m4671_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_BeginInvoke_m4672_MethodInfo;
extern const MethodInfo RemoteCertificateValidationCallback_EndInvoke_m4673_MethodInfo;
static const Il2CppMethodReference RemoteCertificateValidationCallback_t1002_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&RemoteCertificateValidationCallback_Invoke_m4671_MethodInfo,
	&RemoteCertificateValidationCallback_BeginInvoke_m4672_MethodInfo,
	&RemoteCertificateValidationCallback_EndInvoke_m4673_MethodInfo,
};
static bool RemoteCertificateValidationCallback_t1002_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RemoteCertificateValidationCallback_t1002_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RemoteCertificateValidationCallback_t1002_0_0_0;
extern const Il2CppType RemoteCertificateValidationCallback_t1002_1_0_0;
struct RemoteCertificateValidationCallback_t1002;
const Il2CppTypeDefinitionMetadata RemoteCertificateValidationCallback_t1002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemoteCertificateValidationCallback_t1002_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, RemoteCertificateValidationCallback_t1002_VTable/* vtableMethods */
	, RemoteCertificateValidationCallback_t1002_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteCertificateValidationCallback"/* name */
	, "System.Net.Security"/* namespaze */
	, RemoteCertificateValidationCallback_t1002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RemoteCertificateValidationCallback_t1002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteCertificateValidationCallback_t1002_0_0_0/* byval_arg */
	, &RemoteCertificateValidationCallback_t1002_1_0_0/* this_arg */
	, &RemoteCertificateValidationCallback_t1002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t1002/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteCertificateValidationCallback_t1002)/* instance_size */
	, sizeof (RemoteCertificateValidationCallback_t1002)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluator.h"
// Metadata Definition System.Text.RegularExpressions.MatchEvaluator
extern TypeInfo MatchEvaluator_t1142_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchEvaluator
#include "System_System_Text_RegularExpressions_MatchEvaluatorMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1142_MatchEvaluator__ctor_m4674_ParameterInfos[] = 
{
	{"object", 0, 134218703, 0, &Object_t_0_0_0},
	{"method", 1, 134218704, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t71_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Text.RegularExpressions.MatchEvaluator::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MatchEvaluator__ctor_m4674_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatchEvaluator__ctor_m4674/* method */
	, &MatchEvaluator_t1142_il2cpp_TypeInfo/* declaring_type */
	, &Void_t71_0_0_0/* return_type */
	, RuntimeInvoker_Void_t71_Object_t_IntPtr_t/* invoker_method */
	, MatchEvaluator_t1142_MatchEvaluator__ctor_m4674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1067_0_0_0;
static const ParameterInfo MatchEvaluator_t1142_MatchEvaluator_Invoke_m4675_ParameterInfos[] = 
{
	{"match", 0, 134218705, 0, &Match_t1067_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::Invoke(System.Text.RegularExpressions.Match)
extern const MethodInfo MatchEvaluator_Invoke_m4675_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MatchEvaluator_Invoke_m4675/* method */
	, &MatchEvaluator_t1142_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1142_MatchEvaluator_Invoke_m4675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Match_t1067_0_0_0;
extern const Il2CppType AsyncCallback_t214_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MatchEvaluator_t1142_MatchEvaluator_BeginInvoke_m4676_ParameterInfos[] = 
{
	{"match", 0, 134218706, 0, &Match_t1067_0_0_0},
	{"callback", 1, 134218707, 0, &AsyncCallback_t214_0_0_0},
	{"object", 2, 134218708, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Text.RegularExpressions.MatchEvaluator::BeginInvoke(System.Text.RegularExpressions.Match,System.AsyncCallback,System.Object)
extern const MethodInfo MatchEvaluator_BeginInvoke_m4676_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_BeginInvoke_m4676/* method */
	, &MatchEvaluator_t1142_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t213_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1142_MatchEvaluator_BeginInvoke_m4676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t213_0_0_0;
static const ParameterInfo MatchEvaluator_t1142_MatchEvaluator_EndInvoke_m4677_ParameterInfos[] = 
{
	{"result", 0, 134218709, 0, &IAsyncResult_t213_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Text.RegularExpressions.MatchEvaluator::EndInvoke(System.IAsyncResult)
extern const MethodInfo MatchEvaluator_EndInvoke_m4677_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MatchEvaluator_EndInvoke_m4677/* method */
	, &MatchEvaluator_t1142_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MatchEvaluator_t1142_MatchEvaluator_EndInvoke_m4677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatchEvaluator_t1142_MethodInfos[] =
{
	&MatchEvaluator__ctor_m4674_MethodInfo,
	&MatchEvaluator_Invoke_m4675_MethodInfo,
	&MatchEvaluator_BeginInvoke_m4676_MethodInfo,
	&MatchEvaluator_EndInvoke_m4677_MethodInfo,
	NULL
};
extern const MethodInfo MatchEvaluator_Invoke_m4675_MethodInfo;
extern const MethodInfo MatchEvaluator_BeginInvoke_m4676_MethodInfo;
extern const MethodInfo MatchEvaluator_EndInvoke_m4677_MethodInfo;
static const Il2CppMethodReference MatchEvaluator_t1142_VTable[] =
{
	&MulticastDelegate_Equals_m2103_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&MulticastDelegate_GetHashCode_m2104_MethodInfo,
	&Object_ToString_m246_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&Delegate_Clone_m2106_MethodInfo,
	&MulticastDelegate_GetObjectData_m2105_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2107_MethodInfo,
	&MulticastDelegate_CombineImpl_m2108_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2109_MethodInfo,
	&MatchEvaluator_Invoke_m4675_MethodInfo,
	&MatchEvaluator_BeginInvoke_m4676_MethodInfo,
	&MatchEvaluator_EndInvoke_m4677_MethodInfo,
};
static bool MatchEvaluator_t1142_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MatchEvaluator_t1142_InterfacesOffsets[] = 
{
	{ &ICloneable_t427_0_0_0, 4},
	{ &ISerializable_t428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchEvaluator_t1142_0_0_0;
extern const Il2CppType MatchEvaluator_t1142_1_0_0;
struct MatchEvaluator_t1142;
const Il2CppTypeDefinitionMetadata MatchEvaluator_t1142_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchEvaluator_t1142_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t216_0_0_0/* parent */
	, MatchEvaluator_t1142_VTable/* vtableMethods */
	, MatchEvaluator_t1142_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MatchEvaluator_t1142_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchEvaluator"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, MatchEvaluator_t1142_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatchEvaluator_t1142_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchEvaluator_t1142_0_0_0/* byval_arg */
	, &MatchEvaluator_t1142_1_0_0/* this_arg */
	, &MatchEvaluator_t1142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchEvaluator_t1142/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchEvaluator_t1142)/* instance_size */
	, sizeof (MatchEvaluator_t1142)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t1143_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU24128MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t1143_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t1143_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU24128_t1143_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t1143_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t1143_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1145_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1145_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t1143_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1145_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU24128_t1143_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t1143_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t1143_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t1143_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t1143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t1143_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t1143_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t1143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t1143_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1143_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t1143_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t1143)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t1143)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t1143_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1144_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU2412MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1144_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1144_VTable[] =
{
	&ValueType_Equals_m2116_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&ValueType_GetHashCode_m2117_MethodInfo,
	&ValueType_ToString_m2120_MethodInfo,
};
static bool U24ArrayTypeU2412_t1144_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1144_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1144_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1144_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1145_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t439_0_0_0/* parent */
	, U24ArrayTypeU2412_t1144_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1144_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1144_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1144_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1144_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1144_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1144_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1144_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1144_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1144_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1144)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1144)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1144_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1145_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1145_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U24ArrayTypeU24128_t1143_0_0_0,
	&U24ArrayTypeU2412_t1144_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1145_VTable[] =
{
	&Object_Equals_m244_MethodInfo,
	&Object_Finalize_m218_MethodInfo,
	&Object_GetHashCode_m245_MethodInfo,
	&Object_ToString_m246_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1145_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1145_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1145;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1145_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1145_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1145_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 812/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1145_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1145_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1145_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 75/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1145_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1145_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1145)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1145)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1145_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
