﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t48;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Canvas>
struct  Comparison_1_t2625  : public MulticastDelegate_t216
{
};
