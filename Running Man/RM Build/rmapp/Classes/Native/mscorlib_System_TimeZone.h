﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t1877;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t1877  : public Object_t
{
};
struct TimeZone_t1877_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t1877 * ___currentTimeZone_0;
};
