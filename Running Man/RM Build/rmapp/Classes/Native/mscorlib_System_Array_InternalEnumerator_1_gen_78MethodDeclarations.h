﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
struct InternalEnumerator_1_t2996;
// System.Object
struct Object_t;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1550;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"
#define InternalEnumerator_1__ctor_m18520(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2996 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11345_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18521(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2996 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m18522(__this, method) (( void (*) (InternalEnumerator_1_t2996 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18523(__this, method) (( bool (*) (InternalEnumerator_1_t2996 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m18524(__this, method) (( ParameterBuilder_t1550 * (*) (InternalEnumerator_1_t2996 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11349_gshared)(__this, method)
