﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t2495;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t144;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m12180_gshared (Enumerator_t2495 * __this, List_1_t144 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m12180(__this, ___l, method) (( void (*) (Enumerator_t2495 *, List_1_t144 *, const MethodInfo*))Enumerator__ctor_m12180_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12181_gshared (Enumerator_t2495 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12181(__this, method) (( Object_t * (*) (Enumerator_t2495 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12181_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m12182_gshared (Enumerator_t2495 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12182(__this, method) (( void (*) (Enumerator_t2495 *, const MethodInfo*))Enumerator_Dispose_m12182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m12183_gshared (Enumerator_t2495 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m12183(__this, method) (( void (*) (Enumerator_t2495 *, const MethodInfo*))Enumerator_VerifyState_m12183_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12184_gshared (Enumerator_t2495 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12184(__this, method) (( bool (*) (Enumerator_t2495 *, const MethodInfo*))Enumerator_MoveNext_m12184_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t137  Enumerator_get_Current_m12185_gshared (Enumerator_t2495 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12185(__this, method) (( RaycastResult_t137  (*) (Enumerator_t2495 *, const MethodInfo*))Enumerator_get_Current_m12185_gshared)(__this, method)
