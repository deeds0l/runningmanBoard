﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2914;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t2677;
// System.Object[]
struct ObjectU5BU5D_t29;

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3MethodDeclarations.h"
#define InvokableCall_1__ctor_m17795(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2914 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m14611_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m17796(__this, ___callback, method) (( void (*) (InvokableCall_1_t2914 *, UnityAction_1_t2677 *, const MethodInfo*))InvokableCall_1__ctor_m14612_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m17797(__this, ___args, method) (( void (*) (InvokableCall_1_t2914 *, ObjectU5BU5D_t29*, const MethodInfo*))InvokableCall_1_Invoke_m14613_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m17798(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2914 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m14614_gshared)(__this, ___targetObj, ___method, method)
