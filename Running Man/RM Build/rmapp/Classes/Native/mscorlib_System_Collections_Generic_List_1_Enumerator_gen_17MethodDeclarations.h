﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
struct Enumerator_t2624;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t48;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t334;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m13905(__this, ___l, method) (( void (*) (Enumerator_t2624 *, List_1_t334 *, const MethodInfo*))Enumerator__ctor_m11350_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13906(__this, method) (( Object_t * (*) (Enumerator_t2624 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11351_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::Dispose()
#define Enumerator_Dispose_m13907(__this, method) (( void (*) (Enumerator_t2624 *, const MethodInfo*))Enumerator_Dispose_m11352_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::VerifyState()
#define Enumerator_VerifyState_m13908(__this, method) (( void (*) (Enumerator_t2624 *, const MethodInfo*))Enumerator_VerifyState_m11353_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::MoveNext()
#define Enumerator_MoveNext_m13909(__this, method) (( bool (*) (Enumerator_t2624 *, const MethodInfo*))Enumerator_MoveNext_m11354_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::get_Current()
#define Enumerator_get_Current_m13910(__this, method) (( Canvas_t48 * (*) (Enumerator_t2624 *, const MethodInfo*))Enumerator_get_Current_m11355_gshared)(__this, method)
