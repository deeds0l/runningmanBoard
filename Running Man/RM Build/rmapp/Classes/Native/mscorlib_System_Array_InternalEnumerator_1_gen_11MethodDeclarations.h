﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>
struct InternalEnumerator_1_t2560;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13049_gshared (InternalEnumerator_1_t2560 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13049(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2560 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13049_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13050_gshared (InternalEnumerator_1_t2560 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13050(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2560 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13050_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13051_gshared (InternalEnumerator_1_t2560 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13051(__this, method) (( void (*) (InternalEnumerator_1_t2560 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13051_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13052_gshared (InternalEnumerator_1_t2560 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13052(__this, method) (( bool (*) (InternalEnumerator_1_t2560 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13052_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C" RaycastHit_t323  InternalEnumerator_1_get_Current_m13053_gshared (InternalEnumerator_1_t2560 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13053(__this, method) (( RaycastHit_t323  (*) (InternalEnumerator_1_t2560 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13053_gshared)(__this, method)
