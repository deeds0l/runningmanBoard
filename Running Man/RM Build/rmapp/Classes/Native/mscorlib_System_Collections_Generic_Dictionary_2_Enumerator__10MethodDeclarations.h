﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t2728;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t513;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t525;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"
#define Enumerator__ctor_m15308(__this, ___dictionary, method) (( void (*) (Enumerator_t2728 *, Dictionary_2_t525 *, const MethodInfo*))Enumerator__ctor_m13505_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15309(__this, method) (( Object_t * (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13506_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15310(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13507_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15311(__this, method) (( Object_t * (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13508_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15312(__this, method) (( Object_t * (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13509_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m15313(__this, method) (( bool (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_MoveNext_m13510_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m15314(__this, method) (( KeyValuePair_2_t2726  (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_get_Current_m13511_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15315(__this, method) (( String_t* (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_get_CurrentKey_m13512_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15316(__this, method) (( GUIStyle_t513 * (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_get_CurrentValue_m13513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m15317(__this, method) (( void (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_VerifyState_m13514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15318(__this, method) (( void (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_VerifyCurrent_m13515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m15319(__this, method) (( void (*) (Enumerator_t2728 *, const MethodInfo*))Enumerator_Dispose_m13516_gshared)(__this, method)
