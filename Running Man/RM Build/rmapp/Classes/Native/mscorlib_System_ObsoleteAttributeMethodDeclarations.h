﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ObsoleteAttribute
struct ObsoleteAttribute_t410;
// System.String
struct String_t;

// System.Void System.ObsoleteAttribute::.ctor()
extern "C" void ObsoleteAttribute__ctor_m4930 (ObsoleteAttribute_t410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
extern "C" void ObsoleteAttribute__ctor_m2008 (ObsoleteAttribute_t410 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
extern "C" void ObsoleteAttribute__ctor_m2009 (ObsoleteAttribute_t410 * __this, String_t* ___message, bool ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
