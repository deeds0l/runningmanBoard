﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Enumerator_t2718;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t511;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct Dictionary_2_t512;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"
#define Enumerator__ctor_m15117(__this, ___dictionary, method) (( void (*) (Enumerator_t2718 *, Dictionary_2_t512 *, const MethodInfo*))Enumerator__ctor_m12833_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15118(__this, method) (( Object_t * (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12834_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15119(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12835_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15120(__this, method) (( Object_t * (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12836_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15121(__this, method) (( Object_t * (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12837_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m15122(__this, method) (( bool (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_MoveNext_m12838_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m15123(__this, method) (( KeyValuePair_2_t2715  (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_get_Current_m12839_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15124(__this, method) (( int32_t (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_get_CurrentKey_m12840_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15125(__this, method) (( LayoutCache_t511 * (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_get_CurrentValue_m12841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m15126(__this, method) (( void (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_VerifyState_m12842_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15127(__this, method) (( void (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_VerifyCurrent_m12843_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m15128(__this, method) (( void (*) (Enumerator_t2718 *, const MethodInfo*))Enumerator_Dispose_m12844_gshared)(__this, method)
