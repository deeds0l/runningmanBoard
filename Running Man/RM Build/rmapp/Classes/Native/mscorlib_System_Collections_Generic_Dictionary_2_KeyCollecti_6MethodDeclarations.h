﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t2596;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2592;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m13500_gshared (Enumerator_t2596 * __this, Dictionary_2_t2592 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m13500(__this, ___host, method) (( void (*) (Enumerator_t2596 *, Dictionary_2_t2592 *, const MethodInfo*))Enumerator__ctor_m13500_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13501_gshared (Enumerator_t2596 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13501(__this, method) (( Object_t * (*) (Enumerator_t2596 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13501_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13502_gshared (Enumerator_t2596 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13502(__this, method) (( void (*) (Enumerator_t2596 *, const MethodInfo*))Enumerator_Dispose_m13502_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13503_gshared (Enumerator_t2596 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13503(__this, method) (( bool (*) (Enumerator_t2596 *, const MethodInfo*))Enumerator_MoveNext_m13503_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13504_gshared (Enumerator_t2596 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13504(__this, method) (( Object_t * (*) (Enumerator_t2596 *, const MethodInfo*))Enumerator_get_Current_m13504_gshared)(__this, method)
