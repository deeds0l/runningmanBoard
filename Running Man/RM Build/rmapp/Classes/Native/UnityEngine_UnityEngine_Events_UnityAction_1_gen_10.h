﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Byte>
struct  UnityAction_1_t2675  : public MulticastDelegate_t216
{
};
