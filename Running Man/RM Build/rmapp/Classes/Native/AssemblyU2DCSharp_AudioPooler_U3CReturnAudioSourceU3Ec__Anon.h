﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t9;
// System.Object
#include "mscorlib_System_Object.h"
// AudioPooler/<ReturnAudioSource>c__AnonStorey7
struct  U3CReturnAudioSourceU3Ec__AnonStorey7_t14  : public Object_t
{
	// UnityEngine.AudioSource AudioPooler/<ReturnAudioSource>c__AnonStorey7::p_currentAudioSource
	AudioSource_t9 * ___p_currentAudioSource_0;
};
