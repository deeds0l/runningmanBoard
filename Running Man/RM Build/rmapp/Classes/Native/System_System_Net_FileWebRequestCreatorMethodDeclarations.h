﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1000;
// System.Net.WebRequest
struct WebRequest_t999;
// System.Uri
struct Uri_t617;

// System.Void System.Net.FileWebRequestCreator::.ctor()
extern "C" void FileWebRequestCreator__ctor_m3878 (FileWebRequestCreator_t1000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
extern "C" WebRequest_t999 * FileWebRequestCreator_Create_m3879 (FileWebRequestCreator_t1000 * __this, Uri_t617 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
