﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseCanvas`1<System.Object>
struct BaseCanvas_1_t2464;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t30;

// System.Void BaseCanvas`1<System.Object>::.ctor()
extern "C" void BaseCanvas_1__ctor_m11743_gshared (BaseCanvas_1_t2464 * __this, const MethodInfo* method);
#define BaseCanvas_1__ctor_m11743(__this, method) (( void (*) (BaseCanvas_1_t2464 *, const MethodInfo*))BaseCanvas_1__ctor_m11743_gshared)(__this, method)
// System.Void BaseCanvas`1<System.Object>::.cctor()
extern "C" void BaseCanvas_1__cctor_m11744_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define BaseCanvas_1__cctor_m11744(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))BaseCanvas_1__cctor_m11744_gshared)(__this /* static, unused */, method)
// T BaseCanvas`1<System.Object>::get_Instance()
extern "C" Object_t * BaseCanvas_1_get_Instance_m11745_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define BaseCanvas_1_get_Instance_m11745(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))BaseCanvas_1_get_Instance_m11745_gshared)(__this /* static, unused */, method)
// System.Void BaseCanvas`1<System.Object>::Create(UnityEngine.Transform)
extern "C" void BaseCanvas_1_Create_m11746_gshared (Object_t * __this /* static, unused */, Transform_t30 * ___p_parent, const MethodInfo* method);
#define BaseCanvas_1_Create_m11746(__this /* static, unused */, ___p_parent, method) (( void (*) (Object_t * /* static, unused */, Transform_t30 *, const MethodInfo*))BaseCanvas_1_Create_m11746_gshared)(__this /* static, unused */, ___p_parent, method)
// System.Void BaseCanvas`1<System.Object>::Awake()
extern "C" void BaseCanvas_1_Awake_m11747_gshared (BaseCanvas_1_t2464 * __this, const MethodInfo* method);
#define BaseCanvas_1_Awake_m11747(__this, method) (( void (*) (BaseCanvas_1_t2464 *, const MethodInfo*))BaseCanvas_1_Awake_m11747_gshared)(__this, method)
// System.Void BaseCanvas`1<System.Object>::OnDestroy()
extern "C" void BaseCanvas_1_OnDestroy_m11748_gshared (BaseCanvas_1_t2464 * __this, const MethodInfo* method);
#define BaseCanvas_1_OnDestroy_m11748(__this, method) (( void (*) (BaseCanvas_1_t2464 *, const MethodInfo*))BaseCanvas_1_OnDestroy_m11748_gshared)(__this, method)
// System.Void BaseCanvas`1<System.Object>::Open()
extern "C" void BaseCanvas_1_Open_m11749_gshared (BaseCanvas_1_t2464 * __this, const MethodInfo* method);
#define BaseCanvas_1_Open_m11749(__this, method) (( void (*) (BaseCanvas_1_t2464 *, const MethodInfo*))BaseCanvas_1_Open_m11749_gshared)(__this, method)
// System.Void BaseCanvas`1<System.Object>::Close()
extern "C" void BaseCanvas_1_Close_m11750_gshared (BaseCanvas_1_t2464 * __this, const MethodInfo* method);
#define BaseCanvas_1_Close_m11750(__this, method) (( void (*) (BaseCanvas_1_t2464 *, const MethodInfo*))BaseCanvas_1_Close_m11750_gshared)(__this, method)
