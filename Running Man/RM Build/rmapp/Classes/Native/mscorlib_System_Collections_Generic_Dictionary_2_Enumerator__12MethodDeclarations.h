﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
struct Enumerator_t2785;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2779;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16242_gshared (Enumerator_t2785 * __this, Dictionary_2_t2779 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16242(__this, ___dictionary, method) (( void (*) (Enumerator_t2785 *, Dictionary_2_t2779 *, const MethodInfo*))Enumerator__ctor_m16242_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16243_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16243(__this, method) (( Object_t * (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16243_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1147  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244(__this, method) (( DictionaryEntry_t1147  (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16244_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245(__this, method) (( Object_t * (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16245_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246(__this, method) (( Object_t * (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16246_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16247_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16247(__this, method) (( bool (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_MoveNext_m16247_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" KeyValuePair_2_t2780  Enumerator_get_Current_m16248_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16248(__this, method) (( KeyValuePair_2_t2780  (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_get_Current_m16248_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m16249_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16249(__this, method) (( Object_t * (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_get_CurrentKey_m16249_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C" int64_t Enumerator_get_CurrentValue_m16250_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16250(__this, method) (( int64_t (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_get_CurrentValue_m16250_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C" void Enumerator_VerifyState_m16251_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16251(__this, method) (( void (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_VerifyState_m16251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16252_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16252(__this, method) (( void (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_VerifyCurrent_m16252_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m16253_gshared (Enumerator_t2785 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16253(__this, method) (( void (*) (Enumerator_t2785 *, const MethodInfo*))Enumerator_Dispose_m16253_gshared)(__this, method)
