﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t407;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.IEventSystemHandler>
struct  Predicate_1_t2479  : public MulticastDelegate_t216
{
};
