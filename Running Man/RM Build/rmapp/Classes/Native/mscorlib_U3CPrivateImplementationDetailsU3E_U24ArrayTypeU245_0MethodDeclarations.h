﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct U24ArrayTypeU2452_t1907;
struct U24ArrayTypeU2452_t1907_marshaled;

void U24ArrayTypeU2452_t1907_marshal(const U24ArrayTypeU2452_t1907& unmarshaled, U24ArrayTypeU2452_t1907_marshaled& marshaled);
void U24ArrayTypeU2452_t1907_marshal_back(const U24ArrayTypeU2452_t1907_marshaled& marshaled, U24ArrayTypeU2452_t1907& unmarshaled);
void U24ArrayTypeU2452_t1907_marshal_cleanup(U24ArrayTypeU2452_t1907_marshaled& marshaled);
