﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t2765;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Comparer_1__ctor_m15955_gshared (Comparer_1_t2765 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m15955(__this, method) (( void (*) (Comparer_1_t2765 *, const MethodInfo*))Comparer_1__ctor_m15955_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void Comparer_1__cctor_m15956_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m15956(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m15956_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m15957_gshared (Comparer_1_t2765 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m15957(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2765 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m15957_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C" Comparer_1_t2765 * Comparer_1_get_Default_m15958_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m15958(__this /* static, unused */, method) (( Comparer_1_t2765 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m15958_gshared)(__this /* static, unused */, method)
