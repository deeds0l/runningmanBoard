﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct  ScriptableObject_t480  : public Object_t33
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t480_marshaled
{
};
