﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t233;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t691  : public MonoBehaviour_t22
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t220  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t233 * ___warningIcon_5;
};
