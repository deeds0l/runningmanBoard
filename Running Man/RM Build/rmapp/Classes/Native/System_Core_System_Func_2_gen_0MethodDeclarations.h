﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<UnityEngine.AudioClip,System.Boolean>
struct Func_2_t40;
// System.Object
struct Object_t;
// UnityEngine.AudioClip
struct AudioClip_t20;
// System.IAsyncResult
struct IAsyncResult_t213;
// System.AsyncCallback
struct AsyncCallback_t214;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<UnityEngine.AudioClip,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Byte>
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
#define Func_2__ctor_m113(__this, ___object, ___method, method) (( void (*) (Func_2_t40 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m11608_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.AudioClip,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m11740(__this, ___arg1, method) (( bool (*) (Func_2_t40 *, AudioClip_t20 *, const MethodInfo*))Func_2_Invoke_m11610_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.AudioClip,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m11741(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t40 *, AudioClip_t20 *, AsyncCallback_t214 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m11612_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.AudioClip,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m11742(__this, ___result, method) (( bool (*) (Func_2_t40 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m11614_gshared)(__this, ___result, method)
